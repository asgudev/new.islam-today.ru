<?php
namespace SC\FileStorageBundle\File;

use SC\FileStorageBundle\Entity\File;

class Service
{
    protected $container;


    public function __construct($container) {
        $this->container = $container;
    }

    public function createObjFromLocalFile($fullFileName, $itemId, $moduleId, $modTypeId = null, $modSubTypeId = null)
    {
        /**
         * FIXME: похорошему тут проверять надо - вдруг файл с таким именем уже есть и надо его же и возвращать!
         */

        // тут нет проверок на валидность имени файла!

        $file = new File();
        $file->setFileName($this->container->get('sc_file_storage_module.service')->getRelativeFileName($moduleId, $itemId, $fullFileName));
        $file->setItemId($itemId);
        $file->setModuleId($moduleId);
        $file->setOriginHostId(1);

        if (!is_null($modTypeId)) {
            $file->setModTypeId($modTypeId);
        }

        if (!is_null($modSubTypeId)) {
            $file->setModSubTypeId($modSubTypeId);
        }

        //$file->setCreatedAt(filectime($fullFileName));
        $file->setCreatedAt(new \DateTime());

        $file->setSize(filesize($fullFileName));

        $fileType = new \finfo(FILEINFO_MIME_TYPE);
        $fileType = $fileType->file($fullFileName);

        if (strpos($fileType, 'image') !== false) {
            $metadata = new \SC\ArticleAdminBundle\Media\Image\MetaData();
            $metadata->setFileName($fullFileName);
            $file->setMediaData(
                array(
                    'width' => $metadata->getWidth(),
                    'height' => $metadata->getHeight()
                ));
            $file->setMediaTypeId(\SC\FileStorageBundle\Service\MediaTypes::IMAGE);
        } else {
            // для остальных файлов мы просто не знаем что указывать в медиаданных
            $file->setMediaData(array());
            $file->setMediaTypeId(\SC\FileStorageBundle\Service\MediaTypes::UNKNOWN);
        }

        return $file;
    }
}