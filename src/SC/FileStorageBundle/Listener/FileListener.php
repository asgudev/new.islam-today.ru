<?php
namespace SC\FileStorageBundle\Listener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use SC\FileStorageBundle\Entity\File;

class FileListener
{
    protected $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    public function postLoad(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();


        if ($entity instanceof File) {

            if (!is_null($entity->getModuleId())) {
                $entity->setUrl($this->container->get('sc_file_storage_module.service')->getFileUrl($entity->getModuleId(), $entity));
            }
        }

        // этот метод проксирует в сервисный слой,
        // на самом деле конечное решение о том как строить
        // урл до файла принимает сам модуль
        //return $this->FSModuleService->getFileUrl($this->getModuleId(), $this);
    }
}