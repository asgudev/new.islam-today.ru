<?php
namespace SC\FileStorageBundle\Module;

class Service
{
    protected static $instance = null;
    protected $modulesServiceClasses = array();
    protected $modulesSettings = array();
    const RELATED_FILE_NAME_SUBSTR = '-INNERRESIZED600-';

    protected $container;

    public function __construct($container)
    {
        //foreach (\G2\Registry::getConfig()->{'G2\FileStorage'}->modules->toArray() as $moduleClassName => $moduleSettings) {
        //    $this->modulesServiceClasses[$moduleClassName::getFileStorageModuleId()] = $moduleClassName;
        //    $this->modulesSettings[$moduleClassName::getFileStorageModuleId()] = $moduleSettings;
        //}
        $this->container = $container;
        $articleModuleId = 'islam_russia_news';
        $this->modulesServiceClasses[$articleModuleId] = '\SC\ArticleBundle\FS\Module';
        $this->modulesSettings[$articleModuleId] = array();
    }

    public function getHostsForNotification($moduleId)
    {
        if (isset($this->modulesSettings[$moduleId]['notifyHosts'])) {
            return $this->modulesSettings[$moduleId]['notifyHosts'];
        } else {
            return array();
        }
    }

    public function getLocalListeners($moduleId)
    {
        if (isset($this->modulesSettings[$moduleId]['localListeners'])) {
            return $this->modulesSettings[$moduleId]['localListeners'];
        } else {
            return array();
        }
    }

    public function notifyListenersFileChanged(\G2\FileStorage\File $file)
    {
        $moduleId = $file->getModuleId();
        if (isset($this->modulesSettings[$moduleId]['notifyHosts'])) {
            \G2\FileStorage\Notify\Service::saveForNotification($file);
        }
    }

    /**
     * Подразумевается, что модуль может по собственным признакам
     * каким-то образом связывать файлы между собой
     * например когда в статью мы вставляем файлы с уменьшением до определенного размера - эти файлы мы так же храним в сторидже
     * а при удалении оригинала надо бы так же удалить и ресайзы
     */
    public function getRelatedFiles($moduleId, $allFiles, $fileId)
    {
        $moduleClassName = $this->_getModuleClassName($moduleId);
        return $moduleClassName::getRelatedFiles($allFiles, $fileId);
    }

    public function getHRModuleName($moduleId)
    {
        $moduleClassName = $this->_getModuleClassName($moduleId);
        return $moduleClassName::getHRModuleName($moduleId);
    }

    public function getLocalHostName($moduleId)
    {
        $settings = $this->_getModuleSettings($moduleId);
        return $settings['localHost'];
    }

    public function getKnownModulesIds()
    {
        return array_keys($this->modulesServiceClasses);
    }

    public function getModuleBackendsSettings($moduleId)
    {
        $settings = $this->_getModuleSettings($moduleId);
        return $settings['backends'];
    }

    public function getFileUrl($moduleId, $file)
    {
        // каждый модуль сам реализует просчет пути до файла
        $moduleClassName = $this->_getModuleClassName($moduleId);
        return (new $moduleClassName($this->container))->getFileUrl($file, $this->_getModuleSettings($moduleId));
    }

    public static function getRelatedFileName($file, $postFix)
    {
        return $file->getId() . static::RELATED_FILE_NAME_SUBSTR . $postFix
        . '-' . $file->getFileName();
    }

    /**
     * Отдает имя файла вместе с вложенными папками, если таковые имеются
     * например
     * /itemId/file/fileName.ext вернет file/fileName.ext
     * /itemId/fileName.ext вернет fileName.ext
     */
    public function getRelativeFileName($moduleId, $itemId, $fullFileName)
    {
        $itemPath = $this->getModuleLocalUploadPath($moduleId, $itemId);
        return str_replace($itemPath . '/', '', $fullFileName);
    }

    public function getModuleLocalUploadPath($moduleId, $itemId)
    {
        // каждый модуль сам реализует просчет пути до файла
        $moduleClassName = $this->_getModuleClassName($moduleId);
        return $moduleClassName::getItemLocalFilePath($itemId);
    }

    public function getItemRelativeWebPath($moduleId, $itemId)
    {
        // каждый модуль сам реализует просчет пути до файла
        $moduleClassName = $this->_getModuleClassName($moduleId);
        return $moduleClassName::getItemRelativeWebPath($itemId);
    }

    public function getModuleTableId($moduleId, $itemId)
    {
        // каждый модуль сам решает по сколько своих элементов хранить внутри одной таблице файлов
        $moduleClassName = $this->_getModuleClassName($moduleId);
        return $moduleClassName::getTableId($itemId);
    }

    protected function _getModuleClassName($moduleId)
    {
        if (!isset($this->modulesServiceClasses[$moduleId])) {
            throw new \Exception('FileStorage module:' . $moduleId . ' not found');
        }
        return $this->modulesServiceClasses[$moduleId];
    }

    protected function _getModuleSettings($moduleId)
    {
        if (!isset($this->modulesSettings[$moduleId])) {
            throw new \Exception('FileStorage module:' . $moduleId . ' not found');
        }

        return $this->modulesSettings[$moduleId];
    }
}