<?php

namespace SC\CategoryBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="sc_category")
 */
class Category
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=250)
     */
    protected $title = '';


    /**
     * @ORM\Column(type="string", length=250)
     */
    protected $path = '';

    /**
     * @ORM\Column(type="boolean", name="is_primary")
     */
    protected $isPrimary = 0;

    /**
     * @ORM\Column(type="integer")
     */
    protected $menuOrder = -1;

    /**
     * @ORM\Column(type="integer")
     */
    protected $level = 1;

    /**
     * @ORM\Column(type="integer")
     */
    protected $parentId = 0;

    /**
     * @ORM\OneToOne(targetEntity="CategoryMeta", cascade={"persist", "remove"})
     */
    protected $meta;

    /**
     * @ORM\Column(type="text", name="descr")
     */
    protected $descr = '';


    protected $picUrl = '';

    /**
     * @ORM\Column(type="boolean", name="has_pic")
     */
    protected $hasPic = false;

    /**
     * @param string $picUrl
     */
    public function setPicUrl($picUrl)
    {
        $this->picUrl = $picUrl;
    }

    /**
     * @return string
     */
    public function getPicUrl()
    {
        return $this->picUrl;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Category
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set path
     *
     * @param string $path
     * @return Category
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set isPrimary
     *
     * @param boolean $isPrimary
     * @return Category
     */
    public function setIsPrimary($isPrimary)
    {
        $this->isPrimary = $isPrimary;

        return $this;
    }

    /**
     * Get isPrimary
     *
     * @return boolean
     */
    public function getIsPrimary()
    {
        return $this->isPrimary;
    }

    /**
     * Set menuOrder
     *
     * @param integer $menuOrder
     * @return Category
     */
    public function setMenuOrder($menuOrder)
    {
        $this->menuOrder = $menuOrder;

        return $this;
    }

    /**
     * Get menuOrder
     *
     * @return integer
     */
    public function getMenuOrder()
    {
        return $this->menuOrder;
    }

    /**
     * Set level
     *
     * @param integer $level
     * @return Category
     */
    public function setLevel($level)
    {
        $this->level = $level;
    
        return $this;
    }

    /**
     * Get level
     *
     * @return integer 
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set parentId
     *
     * @param integer $parentId
     * @return Category
     */
    public function setParentId($parentId)
    {
        $this->parentId = $parentId;
    
        return $this;
    }

    /**
     * Get parentId
     *
     * @return integer 
     */
    public function getParentId()
    {
        return $this->parentId;
    }

    /**
     * Set meta
     *
     * @param \SC\CategoryBundle\Entity\CategoryMeta $meta
     * @return Category
     */
    public function setMeta(\SC\CategoryBundle\Entity\CategoryMeta $meta = null)
    {
        $this->meta = $meta;
    
        return $this;
    }

    /**
     * Get meta
     *
     * @return \SC\CategoryBundle\Entity\CategoryMeta 
     */
    public function getMeta()
    {
        return $this->meta;
    }

    /**
     * Set descr
     *
     * @param string $descr
     * @return Category
     */
    public function setDescr($descr)
    {
        $this->descr = $descr;
    
        return $this;
    }

    /**
     * Get descr
     *
     * @return string 
     */
    public function getDescr()
    {
        return $this->descr;
    }

    /**
     * Set hasPic
     *
     * @param boolean $hasPic
     * @return Category
     */
    public function setHasPic($hasPic)
    {
        $this->hasPic = $hasPic;
    
        return $this;
    }

    /**
     * Get hasPic
     *
     * @return boolean 
     */
    public function getHasPic()
    {
        return $this->hasPic;
    }
}