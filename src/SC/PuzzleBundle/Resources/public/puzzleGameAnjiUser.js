	var puzzleApp = angular.module('puzzle', []);

	puzzleApp.factory('myService', function() {
		return {
			foo: function($scope) {
				$scope.trNumbersGoo = [];

				$scope.mozaicBgImage = document.getElementById("mozaicBgImage");

				$scope.imgWidthIx = Math.floor($scope.mozaicBgImage.width / $scope.hor);
				$scope.imgHeightIx = Math.floor($scope.mozaicBgImage.height / $scope.ver);

				var countOut = 0;
				for (var d = 0; d < $scope.ver; d++) {
					var innerGrid = [];
					for (var i = 0; i < $scope.hor; i++) {
						innerGrid = innerGrid.concat(countOut++);
					}
					$scope.trNumbersGoo.push(innerGrid);
				}
				$scope.trNumbersGooOrig = angular.copy($scope.trNumbersGoo);
				var outer = [];
				for (q=0;q<$scope.trNumbersGoo.length;q++) {
					var inner = [];
					for (x=0;x<$scope.trNumbersGoo[q].length;x++) {
						inner = inner.concat($scope.trNumbersGoo[q][x]);
					}
					outer = outer.concat(inner);
				}
				for(var j, x, i = outer.length; i; j = parseInt(Math.random() * i), x = outer[--i], outer[i] = outer[j], outer[j] = x);
				var trArr = [];
				var trArrInner = [];
				var counter = 0;
				for (x=0;x<outer.length;x++){
					trArrInner = trArrInner.concat(outer[x]);
					if (x % $scope.hor == $scope.hor - 1){
						trArr[counter] = trArrInner;
						counter++;
						trArrInner = [];
					}
				}
				$scope.trNumbersGoo = angular.copy(trArr);


				$scope.getStyle = function(inData){
				return {
						width:  $scope.imgWidthIx + "px",
						height: $scope.imgHeightIx + "px",
						background: "url(" + $scope.mozaicBgImage.src + ") -"
						+ (inData % $scope.hor)*$scope.imgWidthIx
						+ "px -"
						+ ((inData - (inData % $scope.hor)) / $scope.hor)*$scope.imgHeightIx
						+ "px no-repeat"
					}
				}

			}
		};
	});

	puzzleApp.run(function($rootScope, myService) {
		$rootScope.appData = myService;
	});

	puzzleApp.controller('puzzleCtrl', ['$scope', function($scope){
		$scope.check = [0,1,2];
		$scope.prev = null;
		$scope.trNumbersGoo = [];
		$scope.trNumbersGooOrig = [];
		$scope.checked = false;
		$scope.finale = false;

		$scope.moveNumber = 0;
		var countMoveNumber = document.getElementById("countMoveNumber");
		$scope.congrat = "U vas poluchilos!";

		$scope.hor = hor;
		$scope.ver = ver;

		$scope.appData.foo($scope);
		$scope.reWrite = function() {
			$scope.finale = false;
			$scope.moveNumber = 0;
			countMoveNumber.innerHTML = $scope.moveNumber;
			$scope.appData.foo($scope);
		}

		$scope.clickMy = function(pIndex, index, x) {
			$scope.check[0] = angular.copy(pIndex);
			$scope.check[1] = angular.copy(index);
			$scope.check[2] = angular.copy(x);
			if ($scope.prev == null) {
				$scope.prev = angular.copy($scope.check);
				return;
			} else {
				countMoveNumber.innerHTML = ++$scope.moveNumber;
				$scope.trNumbersGoo[pIndex][index] = $scope.prev[2];
				$scope.trNumbersGoo[$scope.prev[0]][$scope.prev[1]] = x;
				$scope.prev = null;
			}
			if (angular.equals($scope.trNumbersGoo, $scope.trNumbersGooOrig)) {
				$scope.finale = true;
			} else {
				$scope.finale = false;
			}
		}
	}]);
