<?php

namespace SC\PuzzleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Puzzle
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Puzzle
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="text")
     */
    private $text;


    /**
     * @var \SC\ArticleBundle\Entity\Article
     *
     * @ORM\OneToOne(targetEntity="\SC\ArticleBundle\Entity\Article", inversedBy="puzzle")
     * @ORM\JoinColumn(name="article_id", referencedColumnName="id")
     **/
    private $article;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set text
     *
     * @param string $text
     * @return Puzzle
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string 
     */
    public function getText()
    {
        return $this->text;
    }



    /**
     * Set article
     *
     * @param \SC\ArticleBundle\Entity\Article $article
     * @return Puzzle
     */
    public function setArticle(\SC\ArticleBundle\Entity\Article $article = null)
    {
        $this->article = $article;

        return $this;
    }

    /**
     * Get article
     *
     * @return \SC\ArticleBundle\Entity\Article 
     */
    public function getArticle()
    {
        return $this->article;
    }
}
