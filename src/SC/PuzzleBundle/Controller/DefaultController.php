<?php

namespace SC\PuzzleBundle\Controller;

use SC\PuzzleBundle\Entity\Puzzle;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use SC\ArticleBundle\Entity\Article;
use SC\ArticleBundle\Entity\ArticleFlatContent;
use SC\ArticleBundle\Entity\ArticleMeta;
use SC\ArticleBundle\Entity\ArticleStats;
use SC\ArticleBundle\Entity\ArticleContent;

class DefaultController extends Controller
{
    /**
     * @Route("/puzzle/", name="sc_puzzle")
     * @Template()
     */
    public function indexAction()
    {
        return [];
    }

    /**
     * @Route("/puzzle/fileUpload/")
     */
    public function fileUploadAction(Request $request)
    {
        if (!$this->get('security.context')->isGranted('ROLE_USER')) {
            throw new AuthenticationException();
        }
        $result = ['status' => 'error'];
        if($request->isMethod('POST')){
            if($request->files){
                $file = $request->files->get('file');
                if($file->isValid()){
                    $ext = substr($file->getClientOriginalName(), strrpos($file->getClientOriginalName(), '.'));
                    $path = '/files/puzzles/';
                    $fileName = md5(time() + rand(0,100)) . $ext;
                    try {
                        $file->move(WEB_DIRECTORY . $path, $fileName);
                        $result['image'] = $path . $fileName;
                        $result['status'] = 'success';
                    }catch(FileException $e){
                    }
                }
            }
        }
        $response = new JsonResponse();
        $response->setData($result);

        return $response;
    }

    /**
     * @Route("/puzzle/save/")
     */
    public function saveAction(Request $request)
    {
        $text = $request->request->get('text');

        $em = $this->get('doctrine.orm.default_entity_manager');

        $article = new Article();
        $article->setAuthorId($this->get('security.context')->getToken()->getUser()->getId());
        $article->setAuthor($this->get('security.context')->getToken()->getUser());
        $article->setCreatedAt(new \DateTime());
        $article->setPublishedAt(new \DateTime());
        $article->setCommentOutPutStyle(1);
        $article->setIsPublished(false);
        $article->setTitle('Пазл');
        $article->setShortAnounce('');
        $article->setLongAnounce('');
        $article->setPath('');
        $article->setFlatContent((new ArticleFlatContent())->setArticle($article));

        $articleMeta = new ArticleMeta();
        $article->setMeta($articleMeta);

        $article->setViewCnt((new ArticleStats())->setCnt(0)->setArticle($article));

        $content = new ArticleContent();
        $content->setContent("");
        $content->setArticle($article);
        $article->setContent($content);

        $this->container->get('article.service')->save($article);

        $puzzle = new Puzzle();
        $puzzle->setText($text);
        $puzzle->setArticle($article);

        $em->persist($puzzle);
        $em->flush();

        return $this->redirect($this->generateUrl('sc_article_edit', ['articleId' => $article->getId()]));

    }


    public function getPuzzleAction(Article $article)
    {
        $puzzle = $article->getPuzzle();
        $puzzleText = $puzzle ? $puzzle->getText() : '';

        return $this->render("@SCPuzzle/Default/getPuzzle.html.twig", [
            'puzzle' => $puzzleText
            ]);
    }
}
