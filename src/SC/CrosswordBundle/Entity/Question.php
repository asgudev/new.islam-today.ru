<?php

namespace SC\CrosswordBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Question
 *
 * @ORM\Table(name="crossword_question")
 * @ORM\Entity
 */
class Question
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="string", length=1000)
     */
    private $text;

    /**
     * @var string
     *
     * @ORM\Column(name="number", type="integer")
     */
    private $number;

    /**
     * @var Crossword
     *
     * @ORM\ManyToOne(targetEntity="Crossword", inversedBy="questions")
     * @ORM\JoinColumn(name="crossword_id", referencedColumnName="id")
     **/
    private $crossword;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set text
     *
     * @param string $text
     * @return Question
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string 
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param string $number
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * @return Crossword
     */
    public function getCrossword()
    {
        return $this->crossword;
    }

    /**
     * @param Crossword $crossword
     */
    public function setCrossword($crossword)
    {
        $this->crossword = $crossword;

        return $this;
    }


}
