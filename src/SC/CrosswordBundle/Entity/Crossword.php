<?php

namespace SC\CrosswordBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use \DateTime;
use SC\CrosswordBundle\Word;

/**
 * Crossword
 *
 * @ORM\Table(name="crossword")
 * @ORM\Entity
 */
class Crossword
{
    const MAX_WIDTH = 25;
    const MAX_HEIGHT = 40;


    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="width", type="integer")
     */
    private $width;

    /**
     * @var integer
     *
     * @ORM\Column(name="height", type="integer")
     */
    private $height;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var string
     *
     * @ORM\Column(name="user_id", type="string", length=255)
     */
    private $user = 1;

    /**
     * @var Letter[]
     *
     * @ORM\OneToMany(targetEntity="Letter", mappedBy="crossword")
     **/
    private $letters;

    /**
     * @var Question[]
     *
     * @ORM\OneToMany(targetEntity="Question", mappedBy="crossword")
     **/
    private $questions;

    /**
     * @var \SC\ArticleBundle\Entity\Article
     *
     * @ORM\OneToOne(targetEntity="\SC\ArticleBundle\Entity\Article", inversedBy="crossword")
     * @ORM\JoinColumn(name="article_id", referencedColumnName="id")
     **/
    private $article;

    /**
     * @var array
     */
    private $matrix = [];

    /**
     * @var array
     */
    private $words = [];

    /**
     * @var array
     */
    private $wordsCoord;

    public function __construct()
    {
        $this->createdAt = new DateTime();
        $this->letters   = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Crossword
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set width
     *
     * @param integer $width
     * @return Crossword
     */
    public function setWidth($width)
    {
        $this->width = $width <= self::MAX_WIDTH ? $width : self::MAX_WIDTH;

        return $this;
    }

    /**
     * Get width
     *
     * @return integer 
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * Set height
     *
     * @param integer $height
     * @return Crossword
     */
    public function setHeight($height)
    {
        $this->height = $height <= self::MAX_HEIGHT ? $height : self::MAX_HEIGHT;

        return $this;
    }

    /**
     * Get height
     *
     * @return integer 
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Crossword
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set user
     *
     * @param string $user
     * @return Crossword
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return string 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return Letter[]
     */
    public function getLetters()
    {
        return $this->letters;
    }

    /**
     * @return Article
     */
    public function getArticle()
    {
        return $this->article;
    }

    /**
     * @param $article
     *
     * @return $this
     */
    public function setArticle($article)
    {
        $this->article = $article;

        return $this;
    }

    /**
     * @return array
     */
    public function getMatrix()
    {
        if (count($this->matrix) == 0) {
            $letters = $this->getLetters();
            foreach ($letters as $letter) {
                $this->matrix[$letter->getX()][$letter->getY()] = $letter->getLetter();
            }
        }

        return $this->matrix;
    }

    /**
     * @param $x
     * @param $y
     *
     * @return string
     */
    public function getLetterByCoord($x, $y)
    {
        $matrix = $this->getMatrix();

        return isset($matrix[$x][$y]) ? $matrix[$x][$y] : "";
    }

    /**
     * @param $x
     * @param $y
     *
     * @return string
     */
    public function getHasLetterByCoord($x, $y)
    {
        return (bool) strlen($this->getLetterByCoord($x, $y));
    }

    /**
     * @return Word[]
     */
    public function getWords()
    {
        /** @var $word Word */
        if (count($this->words)) {
            return $this->words;
        }

        $this->words = [];

        $words  = [];
        $matrix =  $this->getMatrix();

        $invert = [];
        foreach ($matrix as $x => $row) {
            foreach ($row as $y => $char) {
                $invert[$y][$x] = $char;
            }
        }

        $words = array_merge($words, $this->detectWords($matrix, Word::VERTICAL));
        $words = array_merge($words, $this->detectWords($invert, Word::HORIZONTAL));

        $orderedWords = [];
        foreach ($words as $word) {
            $orderedWords[$word->getOrder()][] = $word;
        }
        ksort($orderedWords);

        $numberedWords = [];
        foreach ($orderedWords as $order => $wordsGroup) {
            foreach ($wordsGroup as $key => $word) {
                $numberedWords[] = $word;
            }
        }

        foreach (array_values($numberedWords) as $number => $word) {
            $word->setNumber($number + 1);
            $this->words[] = $word;
        }

        foreach ($this->words as $word) {
            $this->wordsCoord[$word->getStartX()][$word->getStartY()][] = $word;
        }

        return $this->words;
    }

    /**
     * @param $x
     * @param $y
     *
     * @return bool
     */
    public function getIsWordStart($x, $y)
    {
        $this->getWords();

        return (isset($this->wordsCoord[$x][$y]) && count($this->wordsCoord[$x][$y]) > 0);
    }

    /**
     * @param $x
     * @param $y
     *
     * @return array|null
     */
    public function getWordNumbers($x, $y)
    {
        $this->getWords();

        if (!isset($this->wordsCoord[$x][$y])) {
            return null;
        }

        $numbers = [];
        foreach ($this->wordsCoord[$x][$y] as $word) {
            $numbers[] = $word->getNumber();
        }

        return $numbers;
    }

    /**
     * @param $number
     *
     * @return Word
     */
    public function getWordByNumber($number)
    {
        foreach ($this->getWords() as $word) {
            if ($word->getNumber() == $number) {
                return $word;
            }
        }

        return null;
    }

    /**
     * @return int
     */
    public function getWordCount()
    {
       return count($this->getWords());
    }

    /**
     * @param $matrix
     * @param $type
     *
     * @return Word[]
     */
    private function detectWords($matrix, $type)
    {
        /** @var Word $word */
        $words = [];

        foreach ($matrix as $x => $row) {
            ksort($row);
            $previousY = 0;
            unset($word);

            $counter = 1;
            foreach ($row as $y => $char) {
                $delta = $y - $previousY;
                if ($delta > 1 || ($y == 1 && $delta == 1)) {
                    if (isset($word) and $word->getLength() > 1) {
                        $words[] = $word;
                    }
                    $word = new Word($x, $y, $char);
                    $word->setType($type);
                } elseif ($delta == 1 && isset($word)) {
                    $word->addLetter($x, $y, $char);
                }

                if (isset($word) and $word->getLength() > 1 && count($row) == $counter) {
                    $words[] = $word;
                }

                $previousY = $y;
                $counter++;
            }
        }

        return $words;
    }

    /**
     * @return Question[]
     */
    public function getQuestions()
    {
        return $this->questions;
    }

    /**
     * @return Question[]
     */
    public function getVerticalQuestions()
    {
        $words     = $this->getWords();
        $questions = $this->getQuestions();

        $selectedQuestions  = [];
        $questionsNumerated = [];

        foreach ($questions as $question) {
            $questionsNumerated[$question->getNumber()] = $question;
        }

        foreach ($words as $word) {
            if ($word->isVertical() && isset($questionsNumerated[$word->getNumber()])) {
                $selectedQuestions[] = $questionsNumerated[$word->getNumber()];
            }
        }

        return $selectedQuestions;
    }

    /**
     * @return Question[]
     */
    public function getHorizontalQuestions()
    {
        $words     = $this->getWords();
        $questions = $this->getQuestions();

        $selectedQuestions  = [];
        $questionsNumerated = [];

        foreach ($questions as $question) {
            $questionsNumerated[$question->getNumber()] = $question;
        }

        foreach ($words as $word) {
            if ($word->isHorizontal() && isset($questionsNumerated[$word->getNumber()])) {
                $selectedQuestions[] = $questionsNumerated[$word->getNumber()];
            }
        }

        return $selectedQuestions;
    }

    /**
     * @param $number
     *
     * @return string
     */
    public function getQuestionByNumber($number)
    {
        foreach ($this->getQuestions() as $question) {
            if ($question->getNumber() == $number) {
                return $question->getText();
            }
        }

        return "";
    }
}
