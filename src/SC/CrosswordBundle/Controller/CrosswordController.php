<?php
namespace SC\CrosswordBundle\Controller;

use SC\ArticleBundle\Entity\Article;
use SC\CrosswordBundle\Entity\Crossword;
use SC\CrosswordBundle\Entity\Letter;
use SC\CrosswordBundle\Entity\Question;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class CrosswordController extends Controller
{

    /**
     * @param Crossword $crossword
     *
     * @Template
     * @ParamConverter("crossword", class="CrosswordBundle:Crossword")
     *
     * @return array
     */
    public function pageAction(Crossword $crossword)
    {
        return [
            'crossword' => $crossword
        ];
    }

    /**
     * @param Article $article
     *
     * @return Response
     */
    public function getCrosswordAction(Article $article)
    {
        $crossword = $article->getCrossword();
        if (!$crossword) {
            return new Response();
        }

        return $this->render('@Crossword/Crossword/crossword.html.twig', ['crossword' => $crossword]);
    }

    /**
     * @param Crossword $crossword
     *
     * @ParamConverter("crossword", class="CrosswordBundle:Crossword")
     *
     * @return JsonResponse
     */
    public function getWordCoordinatesAction(Crossword $crossword)
    {
        $wordsCoordinates = [];
        foreach ($crossword->getWords() as $word) {
            $wordCoords = [];
            foreach ($word->getCoordinates() as $coord) {
                $wordCoords[] = ['x' => $coord[0], 'y' => $coord[1]];
            }
            $wordsCoordinates[$word->getNumber()] = $wordCoords;
        }

        return new JsonResponse(['success' => true, 'coordinates' => $wordsCoordinates]);
    }


    /**
     * @param Request   $request
     * @param Crossword $crossword
     *
     * @ParamConverter("crossword", class="CrosswordBundle:Crossword")
     *
     * @return Response
     */
    public function checkWordAction(Crossword $crossword, Request $request)
    {
        $question = $this->getDoctrine()
                         ->getRepository('CrosswordBundle:Question')
                         ->findOneBy([
                             'number' => $request->get('questionId'),
                             'crossword' => $crossword
                            ]);
        if (!$question) {
            return new JsonResponse(['success' => false, 'error' => 'QUESTION_UNDEFINED']);
        }

        $word = $crossword->getWordByNumber($question->getNumber());
        if (!$word) {
            return new JsonResponse(['success' => false, 'error' => 'WORD_UNDEFINED']);
        }

        if ($word->getWord() != $request->get('word')) {
            return new JsonResponse(['success' => false, 'error' => 'WORD_INCORRECT']);
        }

        return new JsonResponse(['success' => true]);
    }
}
