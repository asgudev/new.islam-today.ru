<?php
namespace SC\CrosswordBundle;

class Word
{
    const HORIZONTAL = 1;

    const VERTICAL   = 2;

    private $startCoord = [];

    private $word = "";

    private $number;

    private $type;

    public function __construct($x, $y, $char)
    {
        $this->startCoord = [$x, $y];
        $this->word       .= $char;
    }

    public function setType($type)
    {
        $this->type = $type;

        if ($type == self::HORIZONTAL) {
            $this->startCoord = [$this->startCoord[1], $this->startCoord[0]];
        }
    }

    /**
     * @return string
     */
    public function getWord()
    {
        return $this->word;
    }

    public function getStartX()
    {
        return $this->startCoord[0];
    }

    public function getStartY()
    {
        return $this->startCoord[1];
    }

    public function addLetter($x, $y, $char)
    {
        $this->word .= $char;
    }

    /**
     * @return array
     */
    public function getCoordinates()
    {
        $coords = [];

        if ($this->type == self::VERTICAL) {
            $yCoords = range($this->startCoord[1], $this->startCoord[1] + $this->getLength() - 1);
            foreach ($yCoords as $yCoord) {
                $coords[] = [$this->startCoord[0], $yCoord];
            }
        }

        if ($this->type == self::HORIZONTAL) {
            $xCoords = range($this->startCoord[0], $this->startCoord[0] + $this->getLength() - 1);
            foreach ($xCoords as $xCoord) {
                $coords[] = [$xCoord, $this->startCoord[1]];
            }
        }

        return $coords;
    }

    public function getLength()
    {
        return mb_strlen($this->word, 'utf-8');
    }

    public function getOrder()
    {
        return $this->startCoord[0] + $this->startCoord[1];
    }

    public function setNumber($number)
    {
        $this->number = $number;
    }

    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @return bool
     */
    public function isVertical()
    {
        return $this->type == self::VERTICAL;
    }

    /**
     * @return bool
     */
    public function isHorizontal()
    {
        return $this->type == self::HORIZONTAL;
    }
}
