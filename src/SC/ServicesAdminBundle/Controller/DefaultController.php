<?php

namespace SC\ServicesAdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('SCServicesAdminBundle:Default:index.html.twig', array('name' => $name));
    }
}
