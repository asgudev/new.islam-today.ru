<?php

namespace SC\ArticleBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class IntegrationController extends Controller
{
    public function fileWithResizeAction($file, $linkTo, $options, $article, $alt)
    {
        $fileMediaData = $file->getMediaData();

        switch ($options['align']) {
            case 'left':
                $alignStyle = 'style="text-align: left;"';
                break;
            case 'none':
                $alignStyle = "";
                break;
            case 'center':
            default:
                // тут работаем как с центром
                $alignStyle = 'style="display: block;margin-left: auto;margin-right: auto;"';
        }

        if (strlen($alt) == 0) {
            $title = $article->getTitle();
        } else {
            $title = $alt;
        }

        $href = '<img ' . $alignStyle . ' title="' . $title . '" alt="' . $title . '" src="' . $file->getUrl() . '" width="' . $fileMediaData['width'] . '"
    height="' . $fileMediaData['height'] . '" border="0" />';

        if (strlen($alt) > 0) {
            $href .= '<p ' . $alignStyle . ' class="img-description">' . $alt . '</p>';
        }

        return new Response($href);

    }

    public function href($href)
    {
        /**
         * FIXME: перейти на http://ru.php.net/manual/en/function.http-build-url.php
         */
        $url = parse_url($href);
        $url['path'] = implode('/',
            array_map(
                function ($v) {
                    if (strpos($v, '?', 0) !== false) {
                        return $v;
                    } else {
                        return rawurlencode($v);
                    }
                },
                explode('/', $url['path'])));

        $outPut = '';
        if (isset($url['scheme'])) {
            $outPut = $url['scheme'] . '://';
        }

        if (isset($url['host'])) {
            $outPut .= $url['host'];
        }
        if (isset($url['path'])) {
            $outPut .= $url['path'];
        }
        if (isset($url['query'])) {
            $outPut .= '?' . $url['query'];
        }

        return $outPut;
    }
}