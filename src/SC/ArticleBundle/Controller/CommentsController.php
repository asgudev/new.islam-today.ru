<?php

namespace SC\ArticleBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use SC\ArticleBundle\Entity\Comment;

class CommentsController extends Controller
{
    public function handlerAction($pageId)
    {

        $apiSecret = $this->container->getParameter('vkontakte_app_secret');
        $date = $this->getRequest()->request->get('date');
        $commentsNum = $this->getRequest()->request->get('num');
        $lastComment = $this->getRequest()->request->get('last_comment');
        $sign = $this->getRequest()->request->get('sign');


        $hash = md5($apiSecret . $date . $commentsNum . iconv('utf-8', 'windows-1251', $lastComment));
        if (strcmp($hash, $sign) !== 0) {
            return new Response('Bad Sign');
        }

        if (!preg_match('/(\d+)$/', $pageId, $matches)) {
            return new Response('Bad Page Id');
        }
        $objectId = $matches[1];


        $em = $this->getDoctrine()->getManager();
        $articleRepository = $em->getRepository('SCArticleBundle:Article');
        $article = $articleRepository->findOneById($objectId);
        if (!$article) {
            throw $this->createNotFoundException('article.not.found');
        }

        $article->setCommentsCount($commentsNum);
        $em->persist($article);
        $em->flush();

        if ($commentsNum > 0) {
            $comment = new Comment(new \DateTime($date), $lastComment, $article);
            $em->persist($comment);
            $em->flush();
        }

        return new Response();
    }

    public function deleteAction($pageId)
    {
        $apiSecret = $this->container->getParameter('vkontakte_app_secret');
        $date = $this->getRequest()->request->get('date');
        $commentsNum = $this->getRequest()->request->get('num');
        $lastComment = $this->getRequest()->request->get('last_comment');
        $sign = $this->getRequest()->request->get('sign');


        $hash = md5($apiSecret . $date . $commentsNum . iconv('utf-8', 'windows-1251', $lastComment));
        if (strcmp($hash, $sign) !== 0) {
            return new Response('Bad Sign');
        }

        if (!preg_match('/(\d+)$/', $pageId, $matches)) {
            return new Response('Bad Page Id');
        }
        $objectId = $matches[1];


        $em = $this->getDoctrine()->getManager();
        $articleRepository = $em->getRepository('SCArticleBundle:Article');
        $article = $articleRepository->findOneById($objectId);
        if (!$article) {
            throw $this->createNotFoundException('article.not.found');
        }

        $article->setCommentsCount($commentsNum);
        $em->persist($article);
        $em->flush();

        //TODO: надо реализовать удаление самих комментариев из базы
        // но я не нашел как это реализовать(вконтакт присылает на событие "удаление" какую-то фигню

        return new Response();
    }


    public function recentAction($max = 10)
    {
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('SCArticleBundle:Comment');

        $criteria = array('locale' => 'ru');
        $orderBy = array('created_at' => 'DESC');

        $comments = $repo->findBy($criteria, $orderBy, $max);

        $articlesIds = array();
        foreach ($comments as $comment) {
            $articlesIds[$comment->getArticleId()] = $comment->getArticleId();
        }


        if (!empty($articlesIds)) {
            $articles = $em->getRepository('SCArticleBundle:Article')->findById($articlesIds);

            foreach ($comments as $comment) {
                foreach ($articles as $article) {
                    if ($comment->getArticleId() == $article->getId()) {
                        $comment->setArticle($article);
                    }
                }
            }
        }
        return $this->render('SCArticleBundle:Comments:recent.html.twig', array('comments' => $comments));
    }
}