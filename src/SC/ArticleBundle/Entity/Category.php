<?php

namespace SC\ArticleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Category
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Category
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="alias", type="string", length=255)
     */
    private $alias;

    /**
     * @var boolean
     *
     * @ORM\Column(name="primary_category", type="boolean", nullable=true)
     */
    private $primary_category = false;

    /**
     * @var integer
     *
     * @ORM\Column(name="menu_order", type="smallint", nullable=true)
     */
    private $order = 99;

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getAlias()."";
    }

    /**
     * @ORM\OneToMany(targetEntity="Article", mappedBy="primary_category")
     */
    protected $primary_articles;

    /**
     * @ORM\ManyToMany(targetEntity="Article", mappedBy="secondary_categories")
     */
    protected $secondary_articles;

    public function __construct()
    {
        $this->primary_articles = new ArrayCollection();
        $this->secondary_articles = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set alias
     *
     * @param string $alias
     * @return Category
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;
    
        return $this;
    }

    /**
     * Get alias
     *
     * @return string 
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * Set primary_category
     *
     * @param boolean $primaryCategory
     * @return Category
     */
    public function setPrimaryCategory($primaryCategory)
    {
        $this->primary_category = $primaryCategory;
    
        return $this;
    }

    /**
     * Get primary_category
     *
     * @return boolean 
     */
    public function getPrimaryCategory()
    {
        return $this->primary_category;
    }

    /**
     * Add primary_articles
     *
     * @param \SC\ArticleBundle\Entity\Article $primaryArticles
     * @return Category
     */
    public function addPrimaryArticle(\SC\ArticleBundle\Entity\Article $primaryArticles)
    {
        $this->primary_articles[] = $primaryArticles;
    
        return $this;
    }

    /**
     * Remove primary_articles
     *
     * @param \SC\ArticleBundle\Entity\Article $primaryArticles
     */
    public function removePrimaryArticle(\SC\ArticleBundle\Entity\Article $primaryArticles)
    {
        $this->primary_articles->removeElement($primaryArticles);
    }

    /**
     * Get primary_articles
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPrimaryArticles()
    {
        return $this->primary_articles;
    }

    /**
     * Add secondary_articles
     *
     * @param \SC\ArticleBundle\Entity\Article $secondaryArticles
     * @return Category
     */
    public function addSecondaryArticle(\SC\ArticleBundle\Entity\Article $secondaryArticles)
    {
        $this->secondary_articles[] = $secondaryArticles;
    
        return $this;
    }

    /**
     * Remove secondary_articles
     *
     * @param \SC\ArticleBundle\Entity\Article $secondaryArticles
     */
    public function removeSecondaryArticle(\SC\ArticleBundle\Entity\Article $secondaryArticles)
    {
        $this->secondary_articles->removeElement($secondaryArticles);
    }

    /**
     * Get secondary_articles
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSecondaryArticles()
    {
        return $this->secondary_articles;
    }

    /**
     * Set order
     *
     * @param integer $order
     * @return Category
     */
    public function setOrder($order)
    {
        $this->order = $order;
    
        return $this;
    }

    /**
     * Get order
     *
     * @return integer 
     */
    public function getOrder()
    {
        return $this->order;
    }
}