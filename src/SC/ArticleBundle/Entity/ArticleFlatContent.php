<?php

namespace SC\ArticleBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="sc_article_flat_content")
 */
class ArticleFlatContent
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * @ORM\OneToOne(targetEntity="Article", inversedBy="flatContent")
     * @ORM\JoinColumn(name="article", referencedColumnName="id")
     */
    protected $article;

    /**
     * @ORM\Column(type="text", name="flat_content")
     */
    protected $flatContent = '';

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set flatContent
     *
     * @param string $flatContent
     * @return ArticleFlatContent
     */
    public function setFlatContent($flatContent)
    {
        $this->flatContent = $flatContent;
    
        return $this;
    }

    /**
     * Get flatContent
     *
     * @return string 
     */
    public function getFlatContent()
    {
        return $this->flatContent;
    }

    /**
     * Set article
     *
     * @param \SC\ArticleBundle\Entity\Article $article
     * @return ArticleFlatContent
     */
    public function setArticle(\SC\ArticleBundle\Entity\Article $article = null)
    {
        $this->article = $article;
    
        return $this;
    }

    /**
     * Get article
     *
     * @return \SC\ArticleBundle\Entity\Article 
     */
    public function getArticle()
    {
        return $this->article;
    }
}