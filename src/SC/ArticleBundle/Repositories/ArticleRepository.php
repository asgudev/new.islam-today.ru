<?php

namespace SC\ArticleBundle\Repositories;

use Doctrine\ORM\EntityRepository;

class ArticleRepository extends EntityRepository
{

    //["superMain" => 1], ['publishedAt' => "DESC"],

    public function findSliderArticles($limit = 9)
    {
        return $this->createQueryBuilder('a')
            ->addSelect('avc')
            ->andWhere('a.isPublished = 1')
            ->andWhere('a.publishedAt <= :now')
            ->andWhere('a.superMain = 1')
            ->setParameters([
                'now' => new \DateTime(),
            ])
            ->leftJoin('a.viewCnt', 'avc')
            ->orderBy('a.publishedAt', "DESC")
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }

    public function findCatNews($category, $limit)
    {
        return $this->createQueryBuilder('a')
            ->addSelect('ac')
            ->addSelect('afc')
            ->where('a.isPublished = 1')
            ->andWhere('a.primary_category = :cat')
            ->andWhere('a.publishedAt <= :now')
            ->leftJoin('a.content' ,'ac')
            ->leftJoin('a.flatContent', 'afc')
            ->setParameters([
                'now' => new \DateTime(),
                'cat' => $category
            ])
            ->orderBy('a.publishedAt', "DESC")
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }

    public function findLastInCategory($category)
    {
        return $this->createQueryBuilder('a')
            ->where('a.isPublished = 1')
            ->andWhere('a.primary_category = :cat')
            ->setParameters([
                'cat' => $category
            ])
            ->orderBy('a.publishedAt', "DESC")
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }


    public function findRelatedToArticle($id, $limit = 3, $ids = [])
    {
        return $this->createQueryBuilder('a')
            ->addSelect('v')
            ->where('a.primary_category NOT IN (:cats)')
            ->andWhere('a.isPublished = 1')
            ->andWhere('v.c')
            ->leftJoin('a.s', 'v')
            ->setParameters([
                'cats' => [38, 62, 75, 116, 117, 122],
            ])
            ->orderBy('a.publishedAt', "DESC")
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }

    public function findRelatedArticles($limit = 3)
    {
        return $this->createQueryBuilder('a')
            ->addSelect('v')
            ->where('a.primary_category NOT IN (:cats)')
            ->andWhere('a.isPublished = 1')
            ->andWhere('v.c')
            ->leftJoin('a.s', 'v')
            ->setParameters([
                'cats' => [38, 62, 75, 116, 117, 122],
            ])
            ->orderBy('a.publishedAt', "DESC")
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }

    public function findRecentArticles($limit = 9)
    {
        return $this->createQueryBuilder('a')
            ->addSelect('avc')
            ->where('a.primary_category NOT IN (:cats)')
            ->andWhere('a.isPublished = 1')
            ->andWhere('a.publishedAt <= :now')
            ->setParameters([
                'now' => new \DateTime(),
                'cats' => [38, 62, 39, 84, 56, 59, 75, 116, 117, 122],
            ])
            ->leftJoin('a.viewCnt', 'avc')
            ->orderBy('a.publishedAt', "DESC")
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }

    public function findBySecondaryCategories(array $categories)
    {
        return $this->createQueryBuilder('b')
            ->join('b.secondary_categories', 'p')
            ->where('p.id in (:secondary_categories) and b.isPublished = 1 ORDER BY b.publishedAt DESC')
            ->setParameter('secondary_categories', $categories)
            ->getQuery()
            ->getResult();
    }


    public function findByTags(array $tags)
    {
        return $this->createQueryBuilder('b')
            ->join('b.tags', 'p')
            ->where('p.id in (:tags) and b.isPublished = 1 ORDER BY b.publishedAt DESC')
            ->setParameter('tags', $tags)
            ->getQuery()
            ->getResult();
    }

    public function findByFiles(array $files)
    {
        return $this->createQueryBuilder('b')
            ->join('b.files', 'p')
            ->where('p.id in (:files) and b.isPublished = 1 ORDER BY b.publishedAt DESC')
            ->setParameter('files', $files)
            ->getQuery()
            ->getResult();
    }

    public function findPostsToRss($ids, $viewCnt = 0)
    {
        $posts = $this->createQueryBuilder('a')
            ->where("a.id IN(:ids)")
            ->join("a.viewCnt", 'v')
            ->andWhere('v.cnt > :views')
            ->setParameters([
                'ids' => $ids,
                'views' => $viewCnt,
            ])
            ->orderBy('a.publishedAt', "DESC")
            ->getQuery()
            ->getResult();
        return $posts;
    }

    public function findByIds($ids, $order= false)
    {
        $posts = $this->createQueryBuilder('a')
            ->addSelect('v')
            ->addSelect("ac")
            ->addSelect("ap")
            ->where("a.id IN(:ids)")
            ->leftJoin("a.viewCnt", 'v')
            ->leftJoin("a.crossword", "ac")
            ->leftJoin("a.puzzle", "ap")
            ->setParameters([
                'ids' => $ids,
            ]);

        if ($order) {
            $posts->addOrderBy($order['field'], $order['order']);
        }

        $posts = $posts->getQuery()->getResult();

        return $posts;
    }


    public function findPublishedFetvy($cat)
    {

        return $this->createQueryBuilder('a')
            ->where('a.primary_category = :cat')
            ->andWhere('a.isPublished = true')
            ->andWhere('a.publishedAt < :date')
            ->setParameters([
                'cat' => $cat,
                'date' => (new \DateTime())
            ])
            ->orderBy('a.publishedAt', 'DESC')
            ->getQuery()
            ->getResult();

    }

    public function findRecommendedFetv()
    {
        return $this->createQueryBuilder('a')
            ->where('a.primary_category = :cat')
            ->andWhere('a.isPublished = true')
            ->andWhere('a.publishedAt < :date')
            ->andWhere('a.isRecommeded = 1')
            ->setParameters([
                'cat' => 39,
                'date' => (new \DateTime())
            ])
            ->orderBy('a.publishedAt', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();

    }
}
