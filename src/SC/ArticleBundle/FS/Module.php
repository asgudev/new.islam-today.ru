<?php
namespace SC\ArticleBundle\FS;

class Module extends \SC\FileStorageBundle\Module\ModuleAbstract implements \SC\FileStorageBundle\Module\ModuleInterface
{
    const FILE_STORAGE_MODULE_ID = 'islam_russia_news';
    const FILE_STORAGE_HR_MODULE_NAME = 'Islam Russia Статьи';

    const FILE_TYPE_PREVIEW = 1;
    const FILE_TYPE_OTHER = 2;
    const FILE_TYPE_RESIZES = 3;
    const RELATED_FILE_NAME_SUBSTR = '-INNERRESIZED600-';

    protected $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    /**
     * Возвращает полный путь до файлов итема на текущем файл-хосте
     */
    public static function getItemLocalFilePath($itemId)
    {
        return WEB_DIRECTORY . '/files/news/part_' . (floor($itemId / 10000)) . '/' . $itemId;
    }

    public static function getItemRelativeWebPath($itemId)
    {
        return '/files/news/part_' . (floor($itemId / 10000)) . '/' . $itemId . '/';
    }

    public static function getTableId($itemId)
    {
        /**
         * select CAST(id / 20000 as UNSIGNED) as cnt, count(id), min(id), max(id) from afamily_article group by cnt;
         * так как у нас есть значительное уплотнение в первой сотне тысяч элементов - мы их расфигачиваем по шардам вручную
         *
         * все более поздние элементы у нас хранятся уже по понятному правилу
         *
         * ВАЖНО: эта логика не может менятся никогда! Если что-то менять в этих вычислениях, то таблицы с файлами необходимо переконвертировать
         */
        switch (true) {
            case $itemId > 0 && $itemId <= 10000:
                return 0;
            case $itemId > 10000 && $itemId <= 20000:
                return 1;
            case $itemId > 20000 && $itemId <= 40000:
                return 2;
            case $itemId > 40000 && $itemId <= 60000:
                return 3;
            case $itemId > 60000 && $itemId <= 80000:
                return 4;
            case $itemId > 80000 && $itemId <= 100000:
                return 5;
            case $itemId > 100000:
                return 6 + (floor($itemId / (500000))); // примерно после 100 000 идентификатора плотность уменьшается в 50 раз за счет циркулярной репликации (автоинкремент +50)
            default:
                throw new \Exception($itemId . ' - is not valid itemId');
        }
    }

    public static function getRelatedFileName(\SC\FileStorageBundle\Entity\File $file, $postFix)
    {
        return $file->getId() . static::RELATED_FILE_NAME_SUBSTR . $postFix . '-' . $file->getFileName();
    }

    public static function getRelatedFiles($allFiles, $fileId)
    {
        $original = $allFiles[$fileId];
        $originalFileName = $original->getFileName();

        $relatedFiles = array();
        foreach ($allFiles as $fileId => $fileObj) {
            if (strpos($fileObj->getFileName(), $original->getId() . static::RELATED_FILE_NAME_SUBSTR) === 0) {
                $relatedFiles[$fileId] = $fileObj;
            }
        }

        return $relatedFiles;
    }
}