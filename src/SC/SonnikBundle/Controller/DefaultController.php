<?php

namespace SC\SonnikBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('SCSonnikBundle:Default:index.html.twig', array('name' => $name));
    }
}
