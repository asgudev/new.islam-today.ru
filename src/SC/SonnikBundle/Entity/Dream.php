<?php

namespace SC\SonnikBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Dream
 *
 * @ORM\Table(name="sc_sonnik_dreams")
 * @ORM\Entity
 */
class Dream
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @ORM\Column(type="datetime", name="created_at")
     *
     * @var DateTime $createdAt
     */
    protected $createdAt;

    /**
     * @ORM\Column(type="datetime", name="updated_at")
     *
     * @var DateTime $updatedAt
     */
    protected $updatedAt;

    /**
     * @ORM\Column(type="boolean", name="is_published")
     */
    protected $isPublished = 0;

    /**
     * @ORM\Column(type="string", length=250)
     */
    protected $title;

    /**
     * @ORM\Column(type="string", length=250)
     */
    protected $path = '';

    /**
     * @ORM\Column(type="text", name="descr")
     */
    protected $descr;

    /**
     * @ORM\Column(type="integer")
     */
    protected $letter = 0;

    /**
     * @ORM\ManyToOne(targetEntity="\SC\SonnikBundle\Entity\Theme", inversedBy="dreams", cascade={"persist"})
     */
    protected $theme;

    /**
     * @ORM\OneToOne(targetEntity="\SC\SonnikBundle\Entity\DreamMeta", cascade={"persist", "remove"})
     */
    protected $meta;


    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Dream
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Dream
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }


    /**
     * Set path
     *
     * @param string $path
     * @return Dream
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }


    /**
     * Set isPublished
     *
     * @param boolean $isPublished
     * @return Dream
     */
    public function setIsPublished($isPublished)
    {
        $this->isPublished = $isPublished;

        return $this;
    }

    /**
     * Get isPublished
     *
     * @return boolean 
     */
    public function getIsPublished()
    {
        return $this->isPublished;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Dream
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set descr
     *
     * @param string $descr
     * @return Dream
     */
    public function setDescr($descr)
    {
        $this->descr = $descr;

        return $this;
    }

    /**
     * Get descr
     *
     * @return string 
     */
    public function getDescr()
    {
        return $this->descr;
    }

    /**
     * Set letter
     *
     * @return Dream
     */
    public function setLetter()
    {
        $this->letter = $this->ordutf8(mb_substr($this->title, 0, 1, 'utf8'));

        return $this;
    }

    /**
     * Get letter
     *
     * @return integer
     */
    public function getLetter()
    {
        return $this->letter;
    }

    protected function ordutf8($string, $offset = 0)
    {
        $code = ord(substr($string, $offset, 1));
        if ($code >= 128) { //otherwise 0xxxxxxx
            if ($code < 224) $bytesnumber = 2; //110xxxxx
            else if ($code < 240) $bytesnumber = 3; //1110xxxx
            else if ($code < 248) $bytesnumber = 4; //11110xxx
            $codetemp = $code - 192 - ($bytesnumber > 2 ? 32 : 0) - ($bytesnumber > 3 ? 16 : 0);
            for ($i = 2; $i <= $bytesnumber; $i++) {
                $offset++;
                $code2 = ord(substr($string, $offset, 1)) - 128; //10xxxxxx
                $codetemp = $codetemp * 64 + $code2;
            }
            $code = $codetemp;
        }
        $offset += 1;
        if ($offset >= strlen($string)) $offset = -1;
        return $code;
    }


    /**
     * Set theme
     *
     * @param \SC\SonnikBundle\Entity\Theme $theme
     * @return Dream
     */
    public function setTheme(\SC\SonnikBundle\Entity\Theme $theme = null)
    {
        $this->theme = $theme;

        return $this;
    }

    /**
     * Get theme
     *
     * @return \SC\SonnikBundle\Entity\Theme 
     */
    public function getTheme()
    {
        return $this->theme;
    }

    /**
     * Set meta
     *
     * @param \SC\SonnikBundle\Entity\DreamMeta $meta
     * @return Dream
     */
    public function setMeta(\SC\SonnikBundle\Entity\DreamMeta $meta = null)
    {
        $this->meta = $meta;

        return $this;
    }

    /**
     * Get meta
     *
     * @return \SC\SonnikBundle\Entity\DreamMeta 
     */
    public function getMeta()
    {
        return $this->meta;
    }
}
