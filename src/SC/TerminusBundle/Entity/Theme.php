<?php

namespace SC\TerminusBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Theme
 *
 * @ORM\Table(name="sc_terms_themes")
 * @ORM\Entity
 */
class Theme
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @ORM\Column(type="string", length=250)
     */
    protected $title = '';


    /**
     * @ORM\Column(type="string", length=250)
     */
    protected $path = '';

    /**
     * @ORM\Column(type="integer")
     */
    protected $menuOrder = -1;

    /**
     * @ORM\Column(type="integer")
     */
    protected $level = 1;

    /**
     * @ORM\Column(type="integer")
     */
    protected $parentId = 0;

    /**
     * @ORM\OneToOne(targetEntity="\SC\TerminusBundle\Entity\ThemeMeta", cascade={"persist", "remove"})
     */
    protected $meta;

    /**
     * @ORM\Column(type="text", name="descr")
     */
    protected $descr = '';


    /**
     * Set title
     *
     * @param string $title
     * @return Theme
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set path
     *
     * @param string $path
     * @return Theme
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set menuOrder
     *
     * @param integer $menuOrder
     * @return Theme
     */
    public function setMenuOrder($menuOrder)
    {
        $this->menuOrder = $menuOrder;

        return $this;
    }

    /**
     * Get menuOrder
     *
     * @return integer 
     */
    public function getMenuOrder()
    {
        return $this->menuOrder;
    }

    /**
     * Set level
     *
     * @param integer $level
     * @return Theme
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Get level
     *
     * @return integer 
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set parentId
     *
     * @param integer $parentId
     * @return Theme
     */
    public function setParentId($parentId)
    {
        $this->parentId = $parentId;

        return $this;
    }

    /**
     * Get parentId
     *
     * @return integer 
     */
    public function getParentId()
    {
        return $this->parentId;
    }

    /**
     * Set descr
     *
     * @param string $descr
     * @return Theme
     */
    public function setDescr($descr)
    {
        $this->descr = $descr;

        return $this;
    }

    /**
     * Get descr
     *
     * @return string 
     */
    public function getDescr()
    {
        return $this->descr;
    }

    /**
     * Set meta
     *
     * @param \SC\TerminusBundle\Entity\ThemeMeta $meta
     * @return Theme
     */
    public function setMeta(\SC\TerminusBundle\Entity\ThemeMeta $meta = null)
    {
        $this->meta = $meta;

        return $this;
    }

    /**
     * Get meta
     *
     * @return \SC\TerminusBundle\Entity\ThemeMeta
     */
    public function getMeta()
    {
        return $this->meta;
    }
}
