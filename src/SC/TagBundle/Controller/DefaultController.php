<?php

namespace SC\TagBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class DefaultController extends Controller
{
    public function addAction()
    {
        return $this->render('SCTagBundle:Default:index.html.twig');
    }

    public function searchAction()
    {
        $q = $this->getRequest()->get('q');

        $query = $this->getDoctrine()->getManager()->createQuery('SELECT a FROM SC\TagBundle\Entity\Tag a WHERE a.title LIKE ?1');
        $query->setParameter(1, '%' . $q . '%');
        $tags = $query->getResult();

        $results = array();
        foreach ($tags as $tag) {
            $results[] = array('id' => $tag->getTitle(), 'text' => $tag->getTitle());
        }

        return new JsonResponse($results);
    }


}
