<?php

namespace SC\BannersBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BannerType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("name", TextType::class, [
                    'attr' => [
                        'class' => "form-control"
                    ]
                ]
            )
            ->add("link", TextType::class, [
                    'attr' => [
                        'class' => "form-control"
                    ]
                ]
            )
            ->add("dateFrom", DateTimeType::class, [
                'widget' => 'single_text',
                "format" =>'dd.MM.yyyy',
                'attr' => [
                    'class' => "form-control"
                ]
            ])
            ->add("dateTo", DateTimeType::class, [
                'widget' => 'single_text',
                "format" =>'dd.MM.yyyy',
                'attr' => [
                    'class' => "form-control"
                ]
            ])
            ->add("place", EntityType::class, [
                'choice_label' => 'name',
                'class' => "SC\\BannersBundle\\Entity\\BannerPlace",
                'expanded' => false,
                "multiple" => false,
                'attr' => [
                    'class' => "form-control"
                ]
            ])
            ->add("isMainpage", CheckboxType::class, [
                'label' => "На главной",
            ])
            ->add("isNewspage", CheckboxType::class, [
                'label' => "На страницах новостей",
            ])
            ->add("isOtherpage", CheckboxType::class, [
                'label' => "На остальных",
            ])
            ->add("isActive", CheckboxType::class, [
                'label' => "Активно",
            ])
            ->add("showSubtitle", CheckboxType::class, [
                'label' => "Подпись \"Реклама\"",
            ])
            ->add("text", TextareaType::class, [
                'attr' => [
                    'class' => "form-control",
                    "rows" => 20,
                ]
            ])
            ->add("file")
            ->add("city", EntityType::class, [
                'class' => "Asgu\\GeoipBundle\\Entity\\City",
                'choice_label' => "nameRu",
                'empty_value' => "Все города",
                'expanded' => false,
                'multiple' => false,
                'required' => false,
                'query_builder' => function(EntityRepository $er){
                    return $er->createQueryBuilder("c")
                        ->orderBy("c.name_ru")
                        ;
                }
            ])
            ->add("submit", SubmitType::class, [
                'label' => "Сохранить",
                'attr' => [
                    'class' => 'btn btn-success'
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'SC\BannersBundle\Entity\Banner'
        ]);
    }
}