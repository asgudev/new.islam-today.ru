<?php

namespace SC\BannersBundle\Controller;

use SC\BannersBundle\Entity\Banner;
use SC\BannersBundle\Entity\BannerPlace;
use SC\BannersBundle\Form\Type\BannerType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/banners/",name="sc_banners")
     * @Template()
     * @Security("has_role('ROLE_EDITOR_IN_CHIEF')")
     */
    public function indexAction(Request $request)
    {
        $full = $request->query->get('full');
        if ($full) {
            $banners = $this->getDoctrine()->getRepository('SCBannersBundle:Banner')->findAll();
        } else {
            $banners = $this->getDoctrine()->getRepository('SCBannersBundle:Banner')->findActual();
        };

        return array(
            'banners' => $banners,
            'full' => $full
        );
    }

    /**
     * @Route("/banners/edit/{id}",name="sc_banners_edit")
     * @Security("has_role('ROLE_EDITOR_IN_CHIEF')")
     */
    public function editAction(Request $request, Banner $banner)
    {
        if (!$banner) {
            return $this->redirectToRoute('sc_banners');
        }

        $form = $this->createForm(new BannerType(), $banner);

        $form->handleRequest($request);
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($banner);
            $em->flush();

            return $this->redirectToRoute('sc_banners');
        }

        return $this->render("SCBannersBundle:Default:form.html.twig", [
            'banner' => $banner,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/banners/add",name="sc_banners_add")
     * @Security("has_role('ROLE_EDITOR_IN_CHIEF')")
     */
    public function addAction(Request $request)
    {
        $banner = new Banner();
        $form = $this->createForm(new BannerType(), $banner);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($banner);
            $em->flush();

            return $this->redirectToRoute('sc_banners_edit', ['id' => $banner->getId()]);
        }

        return $this->render("SCBannersBundle:Default:form.html.twig", [
            'banner' => $banner,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/banners/{id}/test",name="sc_banners_test")
     */
    public function getBannerAction(BannerPlace $bannerPlace, $style = null, $class = null, $width = null, $height = null, $onlyInnerPages = false)
    {
        $city = $this->get('session')->get('userCity');
        if ($city && isset($city['id'])) {
//            $city = $this->getDoctrine()->getRepository("IslamTodayApplicationBundle:City")->find($city['id']);
            $city = $this->getDoctrine()->getEntityManager()->getReference("IslamTodayApplicationBundle:City", $city['id']);
        }

        $now = new \DateTime('now');
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();
        $query = $qb->select(array('b'))
            ->from('SCBannersBundle:Banner', 'b')
            ->where('b.isActive = 1')
            ->andWhere('b.place = :place')
            ->andWhere('b.dateFrom < :dateNow')
            ->andWhere('b.dateTo > :dateNow')
            ->setParameters(
                [
                    'dateNow' => $now,
                    'place' => $bannerPlace,
                ]);


        $isMainPage = false;
        $isNewsPage = false;
        $isOtherPage = false;
        if ($_SERVER['REQUEST_URI'] == '/') {
            $isMainPage = true;
        };
        if (substr($_SERVER['REQUEST_URI'], 0, 17) == '/svetskie_novosti') {
            $isNewsPage = true;
        };
        if (!$isMainPage && !$isNewsPage) {
            $isOtherPage = true;
        }
        if ($isMainPage) {
            $query->andWhere('b.isMainpage = 1');
        };
        if ($isNewsPage) {
            $query->andWhere('b.isNewspage = 1');
        };
        if ($isOtherPage) {
            $query->andWhere('b.isOtherpage = 1');
        };
        if($city) {
            $query->andWhere('(b.city = :city OR b.city IS NULL)');
            $query->setParameter('city', $city);
        }else {
            $query->andWhere("b.city IS NULL");
        };

        if ($isMainPage && $onlyInnerPages) {
            // Если шаблон запрашивает баннер только для внутренних страниц
            return $this->render("@SCBanners/Default/getBanner.html.twig", ['banner' => null]);
        };

        $query = $query->getQuery();

        $bannersRes = $query->getResult();

        $banners = [];

        $sessionQueue = $this->get('session')->get('banners_queue');

        if (!$sessionQueue)
            $sessionQueue = [];

            if (!array_key_exists($bannerPlace->getId(), $sessionQueue))
                $sessionQueue[$bannerPlace->getId()] = [];

        foreach ($bannersRes as $banner) {

            if (!in_array($banner->getId(), $sessionQueue[$bannerPlace->getId()]))
                $sessionQueue[$bannerPlace->getId()][$banner->getShowOrder()] = $banner->getId();

            $banners[$banner->getId()] = $banner;
        }
        ksort($sessionQueue[$bannerPlace->getId()]);


        $showBannerId = array_shift($sessionQueue[$bannerPlace->getId()]);
        $sessionQueue[$bannerPlace->getId()][] = $showBannerId;

        $this->get('session')->set('banners_queue', $sessionQueue);

        $banner = @$banners[$showBannerId];

        return $this->render("@SCBanners/Default/getBanner.html.twig", [
            'banner' => $banner,
            'style' => $style,
            'class' => $class,
            'width' => $width,
            'height' => $height
        ]);
    }
}
