<?php

namespace SC\BannersBundle\Entity;

use Doctrine\ORM\EntityRepository;


class BannerRepository extends  EntityRepository
{
    public function findActual()
    {
        $query = $this
            ->createQueryBuilder('b')
            ->where("b.dateTo > :dateTo")
            ->setParameter('dateTo', new \DateTime())
            ->orderBy("b.id", "DESC")
            ->getQuery()
            ->getResult()
        ;

        return $query;
    }


    /**
     * @return Banner[]
     */
    public function findPartners()
    {
        $query = $this
            ->createQueryBuilder('b')
            ->where("b.dateTo > :dateTo")
            ->andWhere('p.id in (:p_ids)')
            ->leftJoin('b.place', 'p')
            ->setParameters([
                'p_ids' => [201,202,203,204,205,206,207,208],
                'dateTo' => new \DateTime(),
            ])
            ->orderBy("p.id", "ASC")
            ->getQuery()
            ->getResult()
        ;

        return $query;
    }
//        $q = $em->createQuery('SELECT b FROM SCBannersBundle:Banner b WHERE b.dateTo > :dateTo ORDER BY b.id DESC')

}