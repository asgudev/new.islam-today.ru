<?php

namespace SC\BannersBundle\Entity;

use IslamToday\ApplicationBundle\Entity\City;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Banner
 *
 * @ORM\Table()
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity(repositoryClass="SC\BannersBundle\Entity\BannerRepository")
 */
class Banner
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="link", type="string", length=255)
     */
    private $link;

    /**
     * @var UploadedFile
     *
     * @Assert\File(maxSize="6000000")
     */
    private $file;

    /**
     * @var string
     *
     * @ORM\Column(name="filepath", type="string", length=255, nullable=true)
     */
    private $filepath;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_swf", type="boolean")
     */
    private $isSwf = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_mainpage", type="boolean")
     */
    private $isMainpage;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_newspage", type="boolean")
     */
    private $isNewspage;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_otherpage", type="boolean")
     */
    private $isOtherpage;

    /**
     * @var boolean
     *
     * @ORM\Column(name="show_subtitle", type="boolean")
     */
    private $showSubtitle;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_from", type="datetime")
     */
    private $dateFrom;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_to", type="datetime")
     */
    private $dateTo;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="text")
     */
    private $text;

    /**
     * @var string
     *
     * @ORM\Column(name="show_order", type="integer")
     */
    private $showOrder;

    /**
     * @ORM\ManyToOne(targetEntity="IslamToday\ApplicationBundle\Entity\City", inversedBy="banners")
     */
    private $city;

    /**
     * @ORM\ManyToOne(targetEntity="BannerPlace", inversedBy="banners")
     */
    protected $place;

    public function __construct()
    {
        $this->isMainpage = true;
        $this->isNewspage = true;
        $this->isOtherpage = true;
        $this->isActive = true;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Banner
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    private $temp;

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
        // check if we have an old image path
        if (isset($this->filepath)) {
            // store the old name to delete after the update
            $this->temp = $this->filepath;
            $this->filepath = null;
        } else {
            $this->filepath = 'initial';
        }
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFilepath()
    {
        return $this->filepath;
    }

    /**
     * Set isSwf
     *
     * @param boolean $isSwf
     * @return Banner
     */
    public function setIsSwf($isSwf)
    {
        $this->isSwf = $isSwf;

        return $this;
    }

    /**
     * Get isSwf
     *
     * @return boolean 
     */
    public function getIsSwf()
    {
        return $this->isSwf;
    }

    /**
     * Set dateFrom
     *
     * @param \DateTime $dateFrom
     * @return Banner
     */
    public function setDateFrom($dateFrom)
    {
        $this->dateFrom = $dateFrom;

        return $this;
    }

    /**
     * Get dateFrom
     *
     * @return \DateTime 
     */
    public function getDateFrom()
    {
        return $this->dateFrom;
    }

    /**
     * Set dateTo
     *
     * @param \DateTime $dateTo
     * @return Banner
     */
    public function setDateTo($dateTo)
    {
        $this->dateTo = $dateTo;

        return $this;
    }

    /**
     * Get dateTo
     *
     * @return \DateTime 
     */
    public function getDateTo()
    {
        return $this->dateTo;
    }

    /**
     * Set place
     *
     * @param \SC\BannersBundle\Entity\BannerPlace $place
     * @return Banner
     */
    public function setPlace(\SC\BannersBundle\Entity\BannerPlace $place = null)
    {
        $this->place = $place;

        return $this;
    }

    /**
     * Get place
     *
     * @return \SC\BannersBundle\Entity\BannerPlace 
     */
    public function getPlace()
    {
        return $this->place;
    }

    /**
     * Set isMainpage
     *
     * @param boolean $isMainpage
     * @return Banner
     */
    public function setIsMainpage($isMainpage)
    {
        $this->isMainpage = $isMainpage;

        return $this;
    }

    /**
     * Get isMainpage
     *
     * @return boolean 
     */
    public function getIsMainpage()
    {
        return $this->isMainpage;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return Banner
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set link
     *
     * @param string $link
     * @return Banner
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link
     *
     * @return string 
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set isNewspage
     *
     * @param boolean $isNewspage
     * @return Banner
     */
    public function setIsNewspage($isNewspage)
    {
        $this->isNewspage = $isNewspage;

        return $this;
    }

    /**
     * Get isNewspage
     *
     * @return boolean 
     */
    public function getIsNewspage()
    {
        return $this->isNewspage;
    }

    /**
     * Set isOtherpage
     *
     * @param boolean $isOtherpage
     * @return Banner
     */
    public function setIsOtherpage($isOtherpage)
    {
        $this->isOtherpage = $isOtherpage;

        return $this;
    }

    /**
     * Get isOtherpage
     *
     * @return boolean 
     */
    public function getIsOtherpage()
    {
        return $this->isOtherpage;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText($text)
    {
        $this->text = $text;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity(City $city)
    {
        $this->city = $city;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->getFile()) {
            // do whatever you want to generate a unique name
            $filename = sha1(uniqid(mt_rand(), true));
            $this->filepath = $filename.'.'.$this->getFile()->guessExtension();
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
        if (null === $this->getFile()) {
            return;
        }

        // if there is an error when moving the file, an exception will
        // be automatically thrown by move(). This will properly prevent
        // the entity from being persisted to the database on error
        $this->getFile()->move($this->getUploadRootDir(), $this->filepath);

        // check if we have an old image
        if (isset($this->temp)) {
            // delete the old image
            unlink($this->getUploadRootDir().'/'.$this->temp);
            // clear the temp image path
            $this->temp = null;
        }
        $this->file = null;
    }

    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
        $file = $this->getAbsolutePath();
        if ($file) {
            unlink($file);
        }
    }

    public function getAbsolutePath()
    {
        return null === $this->filepath
            ? null
            : $this->getUploadRootDir().'/'.$this->filepath;
    }

    public function getWebPath()
    {
        return null === $this->filepath
            ? null
            : $this->getUploadDir().'/'.$this->filepath;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'files/banners';
    }

    /**
     * Set filepath
     *
     * @param string $filepath
     *
     * @return Banner
     */
    public function setFilepath($filepath)
    {
        $this->filepath = $filepath;

        return $this;
    }

    /**
     * Set showSubtitle
     *
     * @param boolean $showSubtitle
     *
     * @return Banner
     */
    public function setShowSubtitle($showSubtitle)
    {
        $this->showSubtitle = $showSubtitle;

        return $this;
    }

    /**
     * Get showSubtitle
     *
     * @return boolean
     */
    public function getShowSubtitle()
    {
        return $this->showSubtitle;
    }

    /**
     * @return integer
     */
    public function getShowOrder()
    {
        return $this->showOrder;
    }

    /**
     * @param integer $showOrder
     * @return Banner
     */
    public function setShowOrder($showOrder)
    {
        $this->showOrder = $showOrder;
        return $this;
    }

}
