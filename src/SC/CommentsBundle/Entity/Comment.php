<?php

namespace SC\CommentsBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="sc_comments")
 */
class Comment
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="datetime", name="created_at")
     *
     * @var DateTime $createdAt
     */
    protected $createdAt;

    /**
     * @ORM\Column(type="string", length=250)
     */
    protected $moduleId;

    /**
     * @ORM\Column(type="integer")
     */
    protected $itemId;

    /**
     * @ORM\Column(type="string", length=250)
     */
    protected $username;

    /**
     * @ORM\ManyToOne(targetEntity="\SC\UserBundle\Entity\BaseUser", inversedBy="comment")
     */
    protected $user;

    /**
     * @ORM\Column(type="string", length=250)
     */
    protected $email = '';

    /**
     * @ORM\Column(type="text")
     */
    protected $text = '';

    /**
     * @ORM\Column(type="integer")
     */
    protected $ip;

    /**
     * @ORM\Column(type="string", length=250)
     */
    protected $extId;

    /**
     * @ORM\Column(type="integer")
     */
    protected $parentId = 0;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Comment
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set moduleId
     *
     * @param integer $moduleId
     * @return Comment
     */
    public function setModuleId($moduleId)
    {
        $this->moduleId = $moduleId;
    
        return $this;
    }

    /**
     * Get moduleId
     *
     * @return integer 
     */
    public function getModuleId()
    {
        return $this->moduleId;
    }

    /**
     * Set itemId
     *
     * @param integer $itemId
     * @return Comment
     */
    public function setItemId($itemId)
    {
        $this->itemId = $itemId;
    
        return $this;
    }

    /**
     * Get itemId
     *
     * @return integer 
     */
    public function getItemId()
    {
        return $this->itemId;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return Comment
     */
    public function setUsername($username)
    {
        $this->username = $username;
    
        return $this;
    }

    /**
     * Get username
     *
     * @return string 
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Comment
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set text
     *
     * @param string $text
     * @return Comment
     */
    public function setText($text)
    {
        $this->text = $text;
    
        return $this;
    }

    /**
     * Get text
     *
     * @return string 
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set ip
     *
     * @param integer $ip
     * @return Comment
     */
    public function setIp($ip)
    {
        $this->ip = ip2long($ip);
    
        return $this;
    }

    /**
     * Get ip
     *
     * @return integer 
     */
    public function getIp()
    {
        return long2ip($this->ip);
    }

    /**
     * Set user
     *
     * @param \SC\UserBundle\Entity\BaseUser $user
     * @return Comment
     */
    public function setUser(\SC\UserBundle\Entity\BaseUser $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \SC\UserBundle\Entity\BaseUser 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set extId
     *
     * @param string $extId
     * @return Comment
     */
    public function setExtId($extId)
    {
        $this->extId = $extId;
    
        return $this;
    }

    /**
     * Get extId
     *
     * @return string 
     */
    public function getExtId()
    {
        return $this->extId;
    }

    /**
     * Set parentId
     *
     * @param integer $parentId
     * @return Comment
     */
    public function setParentId($parentId)
    {
        $this->parentId = $parentId;
    
        return $this;
    }

    /**
     * Get parentId
     *
     * @return integer 
     */
    public function getParentId()
    {
        return $this->parentId;
    }
}