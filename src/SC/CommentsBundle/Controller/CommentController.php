<?php

namespace SC\CommentsBundle\Controller;

use SC\CommentsBundle\Entity\Comment;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class CommentController extends Controller
{
    public function addAction()
    {
//      todo: здесь надо заебенить проверку, чтобы приходили все нужные нам данные

        $params = $this->getRequest()->get('params');
        $itemId = $this->getRequest()->get('itemId');
        $moduleId = $this->getRequest()->get('moduleId');
        $user = $this->getRequest()->get('user');


        if (empty($params) || is_null($itemId) || is_null($moduleId)) {
            return new JsonResponse(['code' => 'error']);
        }

        $comment = new Comment();
        $comment->setCreatedAt(new \DateTime($params['time']));
        $comment->setEmail(isset($params['email']) ? $params['email'] : '');
        $comment->setIp($params['ip']);
        $comment->setItemId($itemId);
        $comment->setModuleId($moduleId);
        $comment->setText($params['text']);
        $comment->setUser(!is_null($user) ? $user : null);
        $comment->setUsername($params['nick']);
        $comment->setExtId($params['id']);

        if (isset($params['parent_id']) && $params['parent_id'] != '') {
            $parentComment = $this->getDoctrine()->getRepository('SCCommentsBundle:Comment')->findOneBy(['extId' => $params['parent_id']]);

            if (!is_null($parentComment)) {
                $comment->setParentId($parentComment->getId());
            }
        }

        $this->getDoctrine()->getManager()->persist($comment);

        switch ($moduleId) {
            case 'articles':
                $article = $this->getDoctrine()->getRepository('SCArticleBundle:Article')->findOneById($itemId);
                $article->setCommentsCount($article->getCommentsCount() + 1);
                break;
            default:
                break;

        }

        $this->getDoctrine()->getManager()->flush();


        return new JsonResponse(['code' => 'ok']);
    }

    public function deleteAction()
    {
        $params = $this->getRequest()->get('params');
        $itemId = $this->getRequest()->get('itemId');
        $moduleId = $this->getRequest()->get('moduleId');

        if (empty($params) || !isset($params['id']) || is_null($itemId) || is_null($moduleId)) {
            return new JsonResponse(['code' => 'error']);
        }

        $comment = $this->getDoctrine()->getRepository('SCCommentsBundle:Comment')->findOneByExtId($params['id']);

        if (is_null($comment)) {
            return new JsonResponse(['code' => 'error']);
        }

        $this->getDoctrine()->getManager()->remove($comment);

        switch ($moduleId) {
            case 'articles':
                $article = $this->getDoctrine()->getRepository('SCArticleBundle:Article')->findOneById($itemId);
                $article->setCommentsCount($article->getCommentsCount() - 1);
                break;
            default:
                break;

        }

        $this->getDoctrine()->getManager()->flush();

        return new JsonResponse(['code' => 'ok']);
    }

    public function updateAction()
    {
        $params = $this->getRequest()->get('params');

        if (empty($params) || !isset($params['id'])) {
            return new JsonResponse(['code' => 'error']);
        }

        $comment = $this->getDoctrine()->getRepository('SCCommentsBundle:Comment')->findOneByExtId($params['id']);

        if (is_null($comment)) {
            return new JsonResponse(['code' => 'error']);
        }

        $comment->setText($params['text']);
        $this->getDoctrine()->getManager()->persist($comment);
        $this->getDoctrine()->getManager()->flush();

        return new JsonResponse(['code' => 'ok']);
    }
}
