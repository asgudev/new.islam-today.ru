<?php

namespace SC\KoranBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('SCKoranBundle:Default:index.html.twig', array('name' => $name));
    }
}
