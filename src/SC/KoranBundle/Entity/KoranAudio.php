<?php

namespace SC\KoranBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Dream
 *
 * @ORM\Table(name="sc_koran_audio")
 * @ORM\Entity
 */
class KoranAudio
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @ORM\Column(type="string", length=250)
     */
    protected $filename = '';

    /**
     * @ORM\ManyToOne(targetEntity="\SC\KoranBundle\Entity\Koran", cascade={"persist"})
     */
    protected $koran;

    /**
     * @ORM\ManyToOne(targetEntity="\SC\KoranBundle\Entity\KoranAudioAuthor", cascade={"persist"})
     */
    protected $author;


    /**
     * Set filename
     *
     * @param string $filename
     * @return KoranAudio
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;

        return $this;
    }

    /**
     * Get filename
     *
     * @return string 
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * Set koran
     *
     * @param \SC\KoranBundle\Entity\Koran $koran
     * @return KoranAudio
     */
    public function setKoran(\SC\KoranBundle\Entity\Koran $koran = null)
    {
        $this->koran = $koran;

        return $this;
    }

    /**
     * Get koran
     *
     * @return \SC\KoranBundle\Entity\Koran 
     */
    public function getKoran()
    {
        return $this->koran;
    }

    /**
     * Set author
     *
     * @param \SC\KoranBundle\Entity\KoranAudioAuthor $author
     * @return KoranAudio
     */
    public function setAuthor(\SC\KoranBundle\Entity\KoranAudioAuthor $author = null)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return \SC\KoranBundle\Entity\KoranAudioAuthor 
     */
    public function getAuthor()
    {
        return $this->author;
    }
}
