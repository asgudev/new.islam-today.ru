<?php

namespace SC\KoranBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Dream
 *
 * @ORM\Table(name="sc_koran_surah_ayah")
 * @ORM\Entity
 */
class KoranSurahAyah
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @ORM\Column(type="string", length=250)
     */
    protected $ayah;


    /**
     * @ORM\OneToMany(targetEntity="\SC\KoranBundle\Entity\KoranSurah", mappedBy="ayah", cascade={"persist"})
     */
    protected $surah;



    /**
     * Set ayah
     *
     * @param string $ayah
     * @return KoranSurahAyah
     */
    public function setAyah($ayah)
    {
        $this->ayah = $ayah;

        return $this;
    }

    /**
     * Get ayah
     *
     * @return string 
     */
    public function getAyah()
    {
        return $this->ayah;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
    }


    /**
     * Set surah
     *
     * @param \SC\KoranBundle\Entity\KoranSurah $surah
     * @return KoranSurahAyah
     */
    public function setSurah(\SC\KoranBundle\Entity\KoranSurah $surah = null)
    {
        $this->surah = $surah;

        return $this;
    }

    /**
     * Get surah
     *
     * @return \SC\KoranBundle\Entity\KoranSurah 
     */
    public function getSurah()
    {
        return $this->surah;
    }

    /**
     * Add surah
     *
     * @param \SC\KoranBundle\Entity\KoranSurah $surah
     * @return KoranSurahAyah
     */
    public function addSurah(\SC\KoranBundle\Entity\KoranSurah $surah)
    {
        $this->surah[] = $surah;

        return $this;
    }

    /**
     * Remove surah
     *
     * @param \SC\KoranBundle\Entity\KoranSurah $surah
     */
    public function removeSurah(\SC\KoranBundle\Entity\KoranSurah $surah)
    {
        $this->surah->removeElement($surah);
    }
}
