<?php

namespace SC\KoranBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Dream
 *
 * @ORM\Table(name="sc_koran")
 * @ORM\Entity
 */
class Koran
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @ORM\Column(type="datetime", name="created_at")
     *
     * @var DateTime $createdAt
     */
    protected $createdAt;

    /**
     * @ORM\Column(type="datetime", name="updated_at")
     *
     * @var DateTime $updatedAt
     */
    protected $updatedAt;

    /**
     * @ORM\Column(type="boolean", name="is_published")
     */
    protected $isPublished = 0;

    /**
     * @ORM\ManyToMany(targetEntity="\SC\KoranBundle\Entity\KoranSurah", inversedBy="koran", cascade={"persist"})
     */
    protected $surah;

    /**
     * @ORM\Column(type="string", length=250)
     */
    protected $path = '';

    /**
     * @ORM\ManyToOne(targetEntity="\SC\KoranBundle\Entity\KoranComment", inversedBy="comments", cascade={"persist"})
     */
    protected $comment;

    /**
     * @ORM\ManyToMany(targetEntity="\SC\KoranBundle\Entity\KoranAudio", inversedBy="audios", cascade={"persist"})
     */
    protected $audio;

    /**
     * @ORM\OneToOne(targetEntity="\SC\KoranBundle\Entity\KoranMeta", cascade={"persist", "remove"})
     */
    protected $meta;

    /**
     * @ORM\ManyToOne(targetEntity="\SC\KoranBundle\Entity\KoranAuthor", inversedBy="authors", cascade={"persist"})
     */
    protected $author;


    /**
     * @ORM\Column(type="integer")
     */
    protected $page;

    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->audio = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Koran
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Koran
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set isPublished
     *
     * @param boolean $isPublished
     * @return Koran
     */
    public function setIsPublished($isPublished)
    {
        $this->isPublished = $isPublished;

        return $this;
    }

    /**
     * Get isPublished
     *
     * @return boolean 
     */
    public function getIsPublished()
    {
        return $this->isPublished;
    }

    /**
     * Set path
     *
     * @param string $path
     * @return Koran
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set page
     *
     * @param integer $page
     * @return Koran
     */
    public function setPage($page)
    {
        $this->page = $page;

        return $this;
    }

    /**
     * Get page
     *
     * @return integer 
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * Set comment
     *
     * @param \SC\KoranBundle\Entity\KoranComment $comment
     * @return Koran
     */
    public function setComment(\SC\KoranBundle\Entity\KoranComment $comment = null)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return \SC\KoranBundle\Entity\KoranComment 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Add audio
     *
     * @param \SC\KoranBundle\Entity\KoranAudio $audio
     * @return Koran
     */
    public function addAudio(\SC\KoranBundle\Entity\KoranAudio $audio)
    {
        $this->audio[] = $audio;

        return $this;
    }

    /**
     * Remove audio
     *
     * @param \SC\KoranBundle\Entity\KoranAudio $audio
     */
    public function removeAudio(\SC\KoranBundle\Entity\KoranAudio $audio)
    {
        $this->audio->removeElement($audio);
    }

    /**
     * Get audio
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAudio()
    {
        return $this->audio;
    }

    /**
     * Set meta
     *
     * @param \SC\KoranBundle\Entity\KoranMeta $meta
     * @return Koran
     */
    public function setMeta(\SC\KoranBundle\Entity\KoranMeta $meta = null)
    {
        $this->meta = $meta;

        return $this;
    }

    /**
     * Get meta
     *
     * @return \SC\KoranBundle\Entity\KoranMeta 
     */
    public function getMeta()
    {
        return $this->meta;
    }

    /**
     * Set author
     *
     * @param \SC\KoranBundle\Entity\KoranAuthor $author
     * @return Koran
     */
    public function setAuthor(\SC\KoranBundle\Entity\KoranAuthor $author = null)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return \SC\KoranBundle\Entity\KoranAuthor 
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Add surah
     *
     * @param \SC\KoranBundle\Entity\KoranSurah $surah
     * @return Koran
     */
    public function addSurah(\SC\KoranBundle\Entity\KoranSurah $surah)
    {
        $this->surah[] = $surah;

        return $this;
    }

    /**
     * Remove surah
     *
     * @param \SC\KoranBundle\Entity\KoranSurah $surah
     */
    public function removeSurah(\SC\KoranBundle\Entity\KoranSurah $surah)
    {
        $this->surah->removeElement($surah);
    }

    /**
     * Get surah
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSurah()
    {
        return $this->surah;
    }

    /**
     * Set surah
     *
     * @param \SC\KoranBundle\Entity\KoranSurah $surah
     * @return Koran
     */
    public function setSurah(\SC\KoranBundle\Entity\KoranSurah $surah = null)
    {
        $this->surah = $surah;

        return $this;
    }
}
