<?php

namespace SC\KoranBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Dream
 *
 * @ORM\Table(name="sc_koran_file")
 * @ORM\Entity
 */
class KoranFile
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @ORM\Column(type="string", length=250)
     */
    protected $filename;


    /**
     * @ORM\ManyToOne(targetEntity="\SC\KoranBundle\Entity\KoranAuthor", inversedBy="authors", cascade={"persist"})
     */
    protected $author;


    /**
     * @ORM\Column(type="integer")
     */
    protected $page;

    /**
     * Set filename
     *
     * @param string $filename
     * @return KoranFile
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;

        return $this;
    }

    /**
     * Get filename
     *
     * @return string 
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * Set author
     *
     * @param \SC\KoranBundle\Entity\KoranAuthor $author
     * @return KoranFile
     */
    public function setAuthor(\SC\KoranBundle\Entity\KoranAuthor $author = null)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return \SC\KoranBundle\Entity\KoranAuthor 
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set page
     *
     * @param integer $page
     * @return KoranFile
     */
    public function setPage($page)
    {
        $this->page = $page;

        return $this;
    }

    /**
     * Get page
     *
     * @return integer 
     */
    public function getPage()
    {
        return $this->page;
    }
}
