<?php

namespace SC\KoranBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Dream
 *
 * @ORM\Table(name="sc_koran_surah")
 * @ORM\Entity
 */
class KoranSurah
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @ORM\Column(type="string", length=250)
     */
    protected $title;


    /**
     * @ORM\ManyToMany(targetEntity="\SC\KoranBundle\Entity\KoranSurahAyah", inversedBy="surah", cascade={"persist"})
     */
    protected $ayah;


    /**
     * @ORM\OneToMany(targetEntity="\SC\KoranBundle\Entity\Koran", mappedBy="surah", cascade={"persist"})
     */
    protected $koran;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->ayah = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set title
     *
     * @param string $title
     * @return KoranSurah
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Add ayah
     *
     * @param \SC\KoranBundle\Entity\KoranSurahAyah $ayah
     * @return KoranSurah
     */
    public function addAyah(\SC\KoranBundle\Entity\KoranSurahAyah $ayah)
    {
        $this->ayah[] = $ayah;

        return $this;
    }

    /**
     * Remove ayah
     *
     * @param \SC\KoranBundle\Entity\KoranSurahAyah $ayah
     */
    public function removeAyah(\SC\KoranBundle\Entity\KoranSurahAyah $ayah)
    {
        $this->ayah->removeElement($ayah);
    }

    /**
     * Get ayah
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAyah()
    {
        return $this->ayah;
    }

    /**
     * Add koran
     *
     * @param \SC\KoranBundle\Entity\Koran $koran
     * @return KoranSurah
     */
    public function addKoran(\SC\KoranBundle\Entity\Koran $koran)
    {
        $this->koran[] = $koran;

        return $this;
    }

    /**
     * Remove koran
     *
     * @param \SC\KoranBundle\Entity\Koran $koran
     */
    public function removeKoran(\SC\KoranBundle\Entity\Koran $koran)
    {
        $this->koran->removeElement($koran);
    }

    /**
     * Get koran
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getKoran()
    {
        return $this->koran;
    }
}
