<?php

namespace SC\KoranBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Dream
 *
 * @ORM\Table(name="sc_koran_comment")
 * @ORM\Entity
 */
class KoranComment
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @ORM\Column(type="text")
     */
    protected $comment = '';

    /**
     * @ORM\Column(type="integer")
     */
    protected $page;

    /**
     * @ORM\ManyToOne(targetEntity="\SC\KoranBundle\Entity\KoranCommentAuthor", cascade={"persist"})
     */
    protected $author;

    /**
     * Set author
     *
     * @param \SC\KoranBundle\Entity\KoranCommentAuthor $author
     * @return KoranComment
     */
    public function setAuthor(\SC\KoranBundle\Entity\KoranCommentAuthor $author = null)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return \SC\KoranBundle\Entity\KoranCommentAuthor
     */
    public function getAuthor()
    {
        return $this->author;
    }



    /**
     * Set comment
     *
     * @param string $comment
     * @return KoranComment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set page
     *
     * @param integer $page
     * @return KoranComment
     */
    public function setPage($page)
    {
        $this->page = $page;

        return $this;
    }

    /**
     * Get page
     *
     * @return integer
     */
    public function getPage()
    {
        return $this->page;
    }
}
