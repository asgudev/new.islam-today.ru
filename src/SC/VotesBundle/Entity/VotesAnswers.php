<?php

namespace SC\VotesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="sc_votes_answers")
 */
class VotesAnswers
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=250)
     */
    protected $answer;

    /**
     * @ORM\Column(type="integer")
     */
    protected $count;


    /**
     * @ORM\ManyToOne(targetEntity="Votes", inversedBy="answers")
     */
    protected $vote;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set answer
     *
     * @param string $answer
     * @return VotesAnswers
     */
    public function setAnswer($answer)
    {
        $this->answer = $answer;
    
        return $this;
    }

    /**
     * Get answer
     *
     * @return string 
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * Set count
     *
     * @param integer $count
     * @return VotesAnswers
     */
    public function setCount($count)
    {
        $this->count = $count;
    
        return $this;
    }

    /**
     * Get count
     *
     * @return integer 
     */
    public function getCount()
    {
        return $this->count;
    }


    /**
     * Set vote
     *
     * @param \SC\VotesBundle\Entity\Votes $vote
     * @return VotesAnswers
     */
    public function setVote(\SC\VotesBundle\Entity\Votes $vote = null)
    {
        $this->vote = $vote;
    
        return $this;
    }

    /**
     * Get vote
     *
     * @return \SC\VotesBundle\Entity\Votes 
     */
    public function getVote()
    {
        return $this->vote;
    }
}