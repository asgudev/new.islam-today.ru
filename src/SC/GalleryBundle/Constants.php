<?php

namespace SC\GalleryBundle;

class Constants
{
    const TYPE_IMAGE = 'image';
    const TYPE_VIDEO_YOUTUBE = 'youtube';

    static $types = array(self::TYPE_IMAGE, self::TYPE_VIDEO_YOUTUBE);


    public static function isValidType($type)
    {
        if (false !== array_search($type, self::$types)) {
            return true;
        }

        return false;
    }
}