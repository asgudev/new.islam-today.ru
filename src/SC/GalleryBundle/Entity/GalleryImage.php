<?php

namespace SC\GalleryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="sc_gallery_image")
 */
class GalleryImage
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="datetime", name="created_at")
     *
     * @var DateTime $createdAt
     */
    protected $createdAt;

    /**
     * @ORM\Column(type="datetime", name="lastmodyfied_at")
     *
     * @var DateTime $lastModyfiedAt
     */
    protected $lastModyfiedAt;

    /**
     * @ORM\Column(type="integer")
     */
    protected $authorId;

    /**
     * @ORM\Column(type="boolean", name="is_published")
     */
    protected $isPublished;

    /**
     * @ORM\Column(type="string", length=250)
     */
    protected $title;

    /**
     * @ORM\Column(type="text", name="descr")
     */
    protected $descr;

    /**
     * @ORM\Column(type="string", length=250)
     */
    protected $filename;

    protected $smallResize;
    protected $bigResize;

    /**
     * @param mixed $smallResize
     */
    public function setSmallResize($smallResize)
    {
        $this->smallResize = $smallResize;
    }

    /**
     * @return mixed
     */
    public function getSmallResize()
    {
        return $this->smallResize;
    }

    /**
     * @param mixed $bigResize
     */
    public function setBigResize($bigResize)
    {
        $this->bigResize = $bigResize;
    }

    /**
     * @return mixed
     */
    public function getBigResize()
    {
        return $this->bigResize;
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return GalleryImage
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set lastModyfiedAt
     *
     * @param \DateTime $lastModyfiedAt
     * @return GalleryImage
     */
    public function setLastModyfiedAt($lastModyfiedAt)
    {
        $this->lastModyfiedAt = $lastModyfiedAt;
    
        return $this;
    }

    /**
     * Get lastModyfiedAt
     *
     * @return \DateTime 
     */
    public function getLastModyfiedAt()
    {
        return $this->lastModyfiedAt;
    }

    /**
     * Set authorId
     *
     * @param integer $authorId
     * @return GalleryImage
     */
    public function setAuthorId($authorId)
    {
        $this->authorId = $authorId;
    
        return $this;
    }

    /**
     * Get authorId
     *
     * @return integer 
     */
    public function getAuthorId()
    {
        return $this->authorId;
    }

    /**
     * Set isPublished
     *
     * @param boolean $isPublished
     * @return GalleryImage
     */
    public function setIsPublished($isPublished)
    {
        $this->isPublished = $isPublished;
    
        return $this;
    }

    /**
     * Get isPublished
     *
     * @return boolean 
     */
    public function getIsPublished()
    {
        return $this->isPublished;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return GalleryImage
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set descr
     *
     * @param string $descr
     * @return GalleryImage
     */
    public function setDescr($descr)
    {
        $this->descr = $descr;
    
        return $this;
    }

    /**
     * Get descr
     *
     * @return string 
     */
    public function getDescr()
    {
        return $this->descr;
    }

    /**
     * Set filename
     *
     * @param string $filename
     * @return GalleryImage
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;
    
        return $this;
    }

    /**
     * Get filename
     *
     * @return string 
     */
    public function getFilename()
    {
        return $this->filename;
    }
}