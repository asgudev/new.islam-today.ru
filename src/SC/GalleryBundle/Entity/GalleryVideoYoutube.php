<?php

namespace SC\GalleryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="sc_gallery_video_youtube")
 */
class GalleryVideoYoutube
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="datetime", name="created_at")
     *
     * @var DateTime $createdAt
     */
    protected $createdAt;

    /**
     * @ORM\Column(type="datetime", name="lastmodyfied_at")
     *
     * @var DateTime $lastModyfiedAt
     */
    protected $lastModyfiedAt;

    /**
     * @ORM\Column(type="integer")
     */
    protected $authorId;

    /**
     * @ORM\Column(type="boolean", name="is_published")
     */
    protected $isPublished;

    /**
     * @ORM\Column(type="string", length=250)
     */
    protected $title;

    /**
     * @ORM\Column(type="text", name="descr")
     */
    protected $descr;

    /**
     * @ORM\Column(type="string", length=250)
     */
    protected $videoId;


    /**
     * @ORM\ManyToOne(targetEntity="\SC\GalleryBundle\Entity\GallerySection", inversedBy="gallery", cascade={"persist"})
     */
    protected $section;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return GalleryVideoYoutube
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set lastModyfiedAt
     *
     * @param \DateTime $lastModyfiedAt
     * @return GalleryVideoYoutube
     */
    public function setLastModyfiedAt($lastModyfiedAt)
    {
        $this->lastModyfiedAt = $lastModyfiedAt;
    
        return $this;
    }

    /**
     * Get lastModyfiedAt
     *
     * @return \DateTime 
     */
    public function getLastModyfiedAt()
    {
        return $this->lastModyfiedAt;
    }

    /**
     * Set authorId
     *
     * @param integer $authorId
     * @return GalleryVideoYoutube
     */
    public function setAuthorId($authorId)
    {
        $this->authorId = $authorId;
    
        return $this;
    }

    /**
     * Get authorId
     *
     * @return integer 
     */
    public function getAuthorId()
    {
        return $this->authorId;
    }

    /**
     * Set isPublished
     *
     * @param boolean $isPublished
     * @return GalleryVideoYoutube
     */
    public function setIsPublished($isPublished)
    {
        $this->isPublished = $isPublished;
    
        return $this;
    }

    /**
     * Get isPublished
     *
     * @return boolean 
     */
    public function getIsPublished()
    {
        return $this->isPublished;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return GalleryVideoYoutube
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set descr
     *
     * @param string $descr
     * @return GalleryVideoYoutube
     */
    public function setDescr($descr)
    {
        $this->descr = $descr;
    
        return $this;
    }

    /**
     * Get descr
     *
     * @return string 
     */
    public function getDescr()
    {
        return $this->descr;
    }

    /**
     * Set video id
     *
     * @param string $videoId
     * @return GalleryVideoYoutube
     */
    public function setVideoId($videoId)
    {
        $this->videoId = $videoId;
    
        return $this;
    }

    /**
     * Get video id
     *
     * @return string 
     */
    public function getVideoId()
    {
        return $this->videoId;
    }

    /**
     * Set section
     *
     * @param \SC\GalleryBundle\Entity\GallerySection $section
     * @return GalleryVideoYoutube
     */
    public function setSection(\SC\GalleryBundle\Entity\GallerySection $section = null)
    {
        $this->section = $section;
    
        return $this;
    }

    /**
     * Get section
     *
     * @return \SC\GalleryBundle\Entity\GallerySection 
     */
    public function getSection()
    {
        return $this->section;
    }
}