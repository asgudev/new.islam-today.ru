<?php

namespace SC\GalleryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

class GalleryPersistent
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="datetime", name="created_at")
     *
     * @var DateTime $createdAt
     */
    protected $createdAt;

    /**
     * @ORM\Column(type="datetime", name="lastmodyfied_at")
     *
     * @var DateTime $lastModyfiedAt
     */
    protected $lastModyfiedAt;

    /**
     * @ORM\Column(type="integer")
     */
    protected $authorId;

    /**
     * @ORM\Column(type="boolean", name="is_published")
     */
    protected $isPublished;

    /**
     * @ORM\Column(type="string", length=250)
     */
    protected $title;

    /**
     * @ORM\Column(type="text", name="descr")
     */
    protected $descr;

}