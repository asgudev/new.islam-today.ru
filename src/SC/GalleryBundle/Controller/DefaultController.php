<?php

namespace SC\GalleryBundle\Controller;

use SC\VotesBundle\Entity\Votes;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('SCMediaBundle:Default:index.html.twig');
    }

    public function imageListingAction()
    {
        return $this->render('SCMediaBundle:Default:image_listing.html.twig');
    }

    public function deleteAction()
    {

    }
}
