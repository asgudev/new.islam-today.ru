<?php

namespace SC\GalleryBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ListsController extends Controller
{
    public function photoAction($max = 20)
    {
        $images = $this->getDoctrine()->getRepository('SCGalleryBundle:GalleryImage')->findAll();

        foreach ($images as $image) {
            $bigResizeUrl = '/files/gallery/part_' . (floor($image->getId() / 10000)) . '/' . $image->getId() . '/previews/preview-600.jpg';
            $smallResizeUrl = '/files/gallery/part_' . (floor($image->getId() / 10000)) . '/' . $image->getId() . '/previews/preview-120.jpg';
            $image->setSmallResize($smallResizeUrl);
            $image->setBigResize($bigResizeUrl);
        }

        return $this->render('SCGalleryBundle:Lists:photo.html.twig', array('images' => $images));

    }

    public function videoAction($max = 20)
    {
        $videos = $this->getDoctrine()->getRepository('SCGalleryBundle:GalleryVideoYoutube')->findAll();

        return $this->render('SCGalleryBundle:Lists:video.html.twig', array('videos' => $videos));
    }
}
