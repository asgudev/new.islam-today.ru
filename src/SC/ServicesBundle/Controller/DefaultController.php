<?php

namespace SC\ServicesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('SCServicesBundle:Default:index.html.twig', array('name' => $name));
    }
}
