<?php
namespace SC\UserBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="sc_user_social")
 */
class SocialUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $socialId;

    /**
     * @ORM\Column(type="integer")
     */
    protected $socialType;


    /**
     * @ORM\ManyToOne(targetEntity="BaseUser", inversedBy="socialUser")
     */
    protected $user;

    /**
     * @param mixed $socialId
     */
    public function setSocialId($socialId)
    {
        $this->socialId = $socialId;
    }

    /**
     * @return mixed
     */
    public function getSocialId()
    {
        return $this->socialId;
    }

    /**
     * @param mixed $socialType
     */
    public function setSocialType($socialType)
    {
        $this->socialType = $socialType;
    }

    /**
     * @return mixed
     */
    public function getSocialType()
    {
        return $this->socialType;
    }


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->user = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param \SC\UserBundle\Entity\BaseUser $user
     * @return SocialUser
     */
    public function setUser(\SC\UserBundle\Entity\BaseUser $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \SC\UserBundle\Entity\BaseUser 
     */
    public function getUser()
    {
        return $this->user;
    }
}