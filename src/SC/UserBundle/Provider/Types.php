<?php

namespace SC\UserBundle\Provider;

class Types
{
    const PROV_NATIVE = 0;
    const PROV_TWITTER = 1;
    const PROV_GOOGLEOAUTH = 2;
    const PROV_FACEBOOK = 3;
    const PROV_VKONTAKTE = 5;
    const PROV_COOKIE = 6;
    const PROV_IP = 7;

    protected static $providerNames = array(
        self::PROV_FACEBOOK => 'facebook',
        self::PROV_TWITTER => 'twitter',
        self::PROV_VKONTAKTE => 'vk',
        self::PROV_GOOGLEOAUTH => 'google',
        self::PROV_NATIVE => 'native',
    );

    public static function getProviderId($provName)
    {
        $provId = array_search($provName, self::$providerNames);

        if ($provId !== false) {
            return $provId;
        } else {
            return null;
        }

    }
}