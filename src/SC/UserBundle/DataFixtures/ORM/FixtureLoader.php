<?php

namespace SC\UserBundle\DataFixtures\ORM;


use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use SC\UserBundle\Entity\BaseUser as User;
use SC\UserBundle\Entity\Role;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;

class FixtureLoader implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        return;
        // создание роли ROLE_ADMIN
        $role = new Role();
        $role->setName('ROLE_ADMIN');

        $manager->persist($role);


        // создание пользователя
        $user = new User();	
        //$user->setFirstName('Namaz');
        //$user->setLastName('Time');
        $user->setEmail('namaztime@namaztime.com');
        $user->setUsername('namaztime@namaztime.com');
        $user->setSalt(md5(time()));

        // шифрует и устанавливает пароль для пользователя,
        // эти настройки совпадают с конфигурационными файлами
        $encoder = new MessageDigestPasswordEncoder('sha512', true, 10);
        $password = $encoder->encodePassword('namaz-time-93ed', $user->getSalt());
        $user->setPassword($password);

        $user->getUserRoles()->add($role);

        //$manager->persist($user);

       // $manager->flush();



        // создание роли ROLE_EDITOR
        $role = new Role();
        $role->setName('ROLE_EDITOR');

        $manager->persist($role);


        // создание роли ROLE_EDITOR_IN_CHIEF
        $role = new Role();
        $role->setName('ROLE_EDITOR_IN_CHIEF');

        $manager->persist($role);

        // создание пользователя
        $user = new User();
        //$user->setFirstName('John');
        //$user->setLastName('Doe');
        $user->setEmail('guzel.maksudy@gmail.com');
        $user->setUsername('guzel.maksudy');
        $user->setSalt(md5(time()));

        // шифрует и устанавливает пароль для пользователя,
        // эти настройки совпадают с конфигурационными файлами
        $encoder = new MessageDigestPasswordEncoder('sha512', true, 10);
        $password = $encoder->encodePassword('islam_russia_512', $user->getSalt());
        $user->setPassword($password);

        $user->getUserRoles()->add($role);

       // $manager->persist($user);

        //$manager->flush();


        // ...

    }
}