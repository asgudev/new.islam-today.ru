<?php

namespace SC\AdvancedGalleryBundle\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="sc_adv_gallery_stats")
 */
class GalleryStats
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToOne(targetEntity="Gallery", inversedBy="viewCnt")
     */
    protected $gallery;

    /**
     * @ORM\Column(type="integer")
     */
    protected $cnt = 0;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cnt
     *
     * @param integer $cnt
     * @return GalleryViewCnt
     */
    public function setCnt($cnt)
    {
        $this->cnt = $cnt;

        return $this;
    }

    /**
     * Get cnt
     *
     * @return integer
     */
    public function getCnt()
    {
        return $this->cnt;
    }

    /**
     * Set gallery
     *
     * @param \SC\AdvancedGalleryBundle\Entity\Gallery $gallery
     * @return GalleryViewCnt
     */
    public function setGallery(\SC\AdvancedGalleryBundle\Entity\Gallery $gallery = null)
    {
        $this->gallery = $gallery;

        return $this;
    }

    /**
     * Get gallery
     *
     * @return \SC\AdvancedGalleryBundle\Entity\Gallery
     */
    public function getGallery()
    {
        return $this->gallery;
    }
}