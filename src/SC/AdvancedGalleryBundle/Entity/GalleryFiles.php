<?php

namespace SC\AdvancedGalleryBundle\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="sc_adv_gallery_files")
 */
class GalleryFiles
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=250)
     */
    protected $filename;

    /**
     * @ORM\Column(type="datetime", name="created_at")
     *
     * @var DateTime $createdAt
     */
    protected $createdAt;

    /**
     * @ORM\Column(type="integer")
     */
    protected $mediaTypeId = '';

    /**
     * @ORM\Column(type="integer")
     */
    protected $size = 0;

    /**
     * @ORM\Column(type="string", length=250)
     */
    protected $author = '';

    /**
     * @ORM\Column(type="string", length=250)
     */
    protected $title = '';

    /**
     * @ORM\Column(type="text")
     */
    protected $descr = '';

    /**
     * @ORM\Column(type="integer")
     */
    protected $downloadCnt = 0;

    protected $localUploadPath = '';

    /**
     * @param string $localUploadPath
     */
    public function setLocalUploadPath($localUploadPath)
    {
        $this->localUploadPath = $localUploadPath;
    }

    /**
     * @return string
     */
    public function getLocalUploadPath()
    {
        return $this->localUploadPath;
    }


    public function getPreviewUrl($type = 'medium')
    {
        // todo: это полный пиздец, надо с этим что-то делать

        //$sizes = ['small' => [104, 70], 'medium' => [200, 134], 'big' => [940, 627]];
        switch ($type) {
            case 'small':
                $url = $this->getLocalUploadPath() . '/preview_' . $this->id . '_104x70.jpg';
                break;
            case 'medium':
                $url = $this->getLocalUploadPath() . '/preview_' . $this->id . '_200x134.jpg';
                break;
            case 'big':
                $url = $this->getLocalUploadPath() . '/preview_' . $this->id . '_940x627.jpg';
                break;
            case 'original':
                $url = $this->getLocalUploadPath() . '/orig/' . $this->filename;
                break;
            default:
                $url = $this->getLocalUploadPath() . '/preview_' . $this->id . '_200x134.jpg';
                break;
        }

        return $url;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set filename
     *
     * @param string $filename
     * @return GalleryFiles
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;

        return $this;
    }

    /**
     * Get filename
     *
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return GalleryFiles
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set mediaTypeId
     *
     * @param integer $mediaTypeId
     * @return GalleryFiles
     */
    public function setMediaTypeId($mediaTypeId)
    {
        $this->mediaTypeId = $mediaTypeId;

        return $this;
    }

    /**
     * Get mediaTypeId
     *
     * @return integer
     */
    public function getMediaTypeId()
    {
        return $this->mediaTypeId;
    }

    /**
     * Set size
     *
     * @param integer $size
     * @return GalleryFiles
     */
    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    /**
     * Get size
     *
     * @return integer
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Set author
     *
     * @param string $author
     * @return GalleryFiles
     */
    public function setAuthor($author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return GalleryFiles
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set descr
     *
     * @param string $descr
     * @return GalleryFiles
     */
    public function setDescr($descr)
    {
        $this->descr = $descr;

        return $this;
    }

    /**
     * Get descr
     *
     * @return string
     */
    public function getDescr()
    {
        return $this->descr;
    }

    /**
     * Set downloadCnt
     *
     * @param integer $downloadCnt
     * @return GalleryFiles
     */
    public function setDownloadCnt($downloadCnt)
    {
        $this->downloadCnt = $downloadCnt;

        return $this;
    }

    /**
     * Get downloadCnt
     *
     * @return integer
     */
    public function getDownloadCnt()
    {
        return $this->downloadCnt;
    }
}