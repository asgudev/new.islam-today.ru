<?php

namespace SC\AdvancedGalleryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="sc_adv_gallery")
 */
class Gallery
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="datetime", name="created_at")
     *
     * @var DateTime $createdAt
     */
    protected $createdAt;

    /**
     * @ORM\Column(type="datetime", name="lastmodyfied_at")
     *
     * @var DateTime $lastModyfiedAt
     */
    protected $lastModyfiedAt;

    /**
     * @ORM\Column(type="datetime", name="published_at")
     *
     * @var DateTime $publishedAt
     */
    protected $publishedAt;

    /**
     * @ORM\ManyToOne(targetEntity="\SC\UserBundle\Entity\BaseUser", inversedBy="gallery")
     */
    protected $author;

    /**
     * @ORM\Column(type="boolean", name="is_published")
     */
    protected $isPublished = 0;

    /**
     * @ORM\Column(type="boolean", name="is_recommended")
     */
    protected $isRecommended = 0;

    /**
     * @ORM\Column(type="string", length=250)
     */
    protected $title = '';

    /**
     * @ORM\Column(type="text", name="short_descr")
     */
    protected $shortDescr = '';

    /**
     * @ORM\Column(type="text", name="long_descr")
     */
    protected $longDescr = '';

    /**
     * @ORM\OneToOne(targetEntity="GalleryStats", mappedBy="gallery", cascade={"persist", "remove"})
     */
    protected $viewCnt;

    /**
     * @ORM\ManyToMany(targetEntity="GalleryFiles", inversedBy="gallery_files",cascade={"persist", "remove"})
     */
    protected $files;

    /**
     * @ORM\Column(type="integer")
     */
    protected $type = 0;

    /**
     * @ORM\Column(type="string", length=250)
     */
    protected $path = '';

    /**
     * @ORM\OneToOne(targetEntity="GalleryMeta", cascade={"persist", "remove"})
     */
    protected $meta;

    /**
     * @var int
     *
     * @ORM\Column(name="comments_count", type="integer", nullable=true)
     */
    protected $commentsCount = 0;

    /**
     * Set commentsCount
     *
     * @param integer $commentsCount
     * @return Gallery
     */
    public function setCommentsCount($commentsCount)
    {
        $this->commentsCount = $commentsCount;

        return $this;
    }

    /**
     * Get commentsCount
     *
     * @return integer
     */
    public function getCommentsCount()
    {
        return $this->commentsCount;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->files = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Gallery
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set lastModyfiedAt
     *
     * @param \DateTime $lastModyfiedAt
     * @return Gallery
     */
    public function setLastModyfiedAt($lastModyfiedAt)
    {
        $this->lastModyfiedAt = $lastModyfiedAt;

        return $this;
    }

    /**
     * Get lastModyfiedAt
     *
     * @return \DateTime
     */
    public function getLastModyfiedAt()
    {
        return $this->lastModyfiedAt;
    }

    /**
     * Set publishedAt
     *
     * @param \DateTime $publishedAt
     * @return Gallery
     */
    public function setPublishedAt($publishedAt)
    {
        $this->publishedAt = $publishedAt;

        return $this;
    }

    /**
     * Get publishedAt
     *
     * @return \DateTime
     */
    public function getPublishedAt()
    {
        return $this->publishedAt;
    }

    /**
     * Set isPublished
     *
     * @param boolean $isPublished
     * @return Gallery
     */
    public function setIsPublished($isPublished)
    {
        $this->isPublished = $isPublished;

        return $this;
    }

    /**
     * Get isPublished
     *
     * @return boolean
     */
    public function getIsPublished()
    {
        return $this->isPublished;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Gallery
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set shortDescr
     *
     * @param string $shortDescr
     * @return Gallery
     */
    public function setShortDescr($shortDescr)
    {
        $this->shortDescr = $shortDescr;

        return $this;
    }

    /**
     * Get shortDescr
     *
     * @return string
     */
    public function getShortDescr()
    {
        return $this->shortDescr;
    }

    /**
     * Set longDescr
     *
     * @param string $longDescr
     * @return Gallery
     */
    public function setLongDescr($longDescr)
    {
        $this->longDescr = $longDescr;

        return $this;
    }

    /**
     * Get longDescr
     *
     * @return string
     */
    public function getLongDescr()
    {
        return $this->longDescr;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return Gallery
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set path
     *
     * @param string $path
     * @return Gallery
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set author
     *
     * @param \SC\UserBundle\Entity\BaseUser $author
     * @return Gallery
     */
    public function setAuthor(\SC\UserBundle\Entity\BaseUser $author = null)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return \SC\UserBundle\Entity\BaseUser
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set viewCnt
     *
     * @param \SC\AdvancedGalleryBundle\Entity\GalleryStats $viewCnt
     * @return Gallery
     */
    public function setViewCnt(\SC\AdvancedGalleryBundle\Entity\GalleryStats $viewCnt = null)
    {
        $this->viewCnt = $viewCnt;

        return $this;
    }

    /**
     * Get viewCnt
     *
     * @return \SC\AdvancedGalleryBundle\Entity\GalleryStats
     */
    public function getViewCnt()
    {
        return $this->viewCnt;
    }

    /**
     * Add files
     *
     * @param \SC\AdvancedGalleryBundle\Entity\GalleryFiles $files
     * @return Gallery
     */
    public function addFile(\SC\AdvancedGalleryBundle\Entity\GalleryFiles $files)
    {
        $this->files[] = $files;

        return $this;
    }

    /**
     * Remove files
     *
     * @param \SC\AdvancedGalleryBundle\Entity\GalleryFiles $files
     */
    public function removeFile(\SC\AdvancedGalleryBundle\Entity\GalleryFiles $files)
    {
        $this->files->removeElement($files);
    }

    /**
     * Get files
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * Set meta
     *
     * @param \SC\AdvancedGalleryBundle\Entity\GalleryMeta $meta
     * @return Gallery
     */
    public function setMeta(\SC\AdvancedGalleryBundle\Entity\GalleryMeta $meta = null)
    {
        $this->meta = $meta;

        return $this;
    }

    /**
     * Get meta
     *
     * @return \SC\AdvancedGalleryBundle\Entity\GalleryMeta
     */
    public function getMeta()
    {
        return $this->meta;
    }

    /**
     * Set isRecommended
     *
     * @param boolean $isRecommended
     * @return Gallery
     */
    public function setIsRecommended($isRecommended)
    {
        $this->isRecommended = $isRecommended;

        return $this;
    }

    /**
     * Get isRecommended
     *
     * @return boolean
     */
    public function getIsRecommended()
    {
        return $this->isRecommended;
    }
}