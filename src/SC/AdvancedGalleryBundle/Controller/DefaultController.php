<?php

namespace SC\AdvancedGalleryBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('SCAdvancedGalleryBundle:Default:index.html.twig', array('name' => $name));
    }
}
