<?php

namespace SC\PollBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Question
 *
 * @ORM\Entity
 * @ORM\Table(name="sc_poll_questions")
 */
class Question
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="text")
     */
    private $text;

    /**
     * @var string
     *
     * @ORM\Column(name="is_test", type="boolean")
     */
    private $test = false;

    /**
     * @var array
     *
     * @ORM\OneToOne(targetEntity="SC\PollBundle\Entity\Answer", cascade={"persist"}, orphanRemoval=true)
     * @ORM\JoinColumn(name="valid", referencedColumnName="id", onDelete="CASCADE")
     */
    private $valid;

    /**
     * @ORM\OneToMany(targetEntity="SC\PollBundle\Entity\Answer", mappedBy="question", cascade={"persist"}, orphanRemoval=true)
     * @ORM\JoinColumn(name="id", referencedColumnName="question_id")
     */
    private $answers;

    /**
     * @var Article
     *
     * @ORM\ManyToOne(targetEntity="SC\ArticleBundle\Entity\Article", inversedBy="questions")
     */
    private $article;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return Question
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->answers = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add answer
     *
     * @param \SC\PollBundle\Entity\Answer $answer
     *
     * @return Question
     */
    public function addAnswer(\SC\PollBundle\Entity\Answer $answer)
    {
        $answer->setQuestion($this);
        $this->answers[] = $answer;

        return $this;
    }

    /**
     * Remove answer
     *
     * @param \SC\PollBundle\Entity\Answer $answer
     */
    public function removeAnswer(\SC\PollBundle\Entity\Answer $answer)
    {
        $this->answers->removeElement($answer);
    }

    /**
     * Get answers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAnswers()
    {
        return $this->answers;
    }

    public function setAnswers(\Doctrine\Common\Collections\Collection $answers)
    {
        foreach ($answers as $answer) {
            $answer->setQuestion($this);
        }

        $this->answers = $answers;
    }

    /**
     * Set article
     *
     * @param \SC\ArticleBundle\Entity\Article $article
     *
     * @return Question
     */
    public function setArticle(\SC\ArticleBundle\Entity\Article $article = null)
    {
        $this->article = $article;

        return $this;
    }

    /**
     * Get article
     *
     * @return \SC\ArticleBundle\Entity\Article
     */
    public function getArticle()
    {
        return $this->article;
    }


    /**
     * Set test
     *
     * @param boolean $test
     *
     * @return Question
     */
    public function setTest($test)
    {
        $this->test = $test;

        return $this;
    }

    /**
     * Get test
     *
     * @return boolean
     */
    public function getTest()
    {
        return $this->test;
    }

    /**
     * Set valid
     *
     * @param \SC\PollBundle\Entity\Answer $valid
     *
     * @return Question
     */
    public function setValid(\SC\PollBundle\Entity\Answer $valid = null)
    {
        $this->valid = $valid;

        return $this;
    }

    /**
     * Get valid
     *
     * @return \SC\PollBundle\Entity\Answer
     */
    public function getValid()
    {
        return $this->valid;
    }
}
