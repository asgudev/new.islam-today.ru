<?php

namespace SC\PollBundle\Controller;

use SC\PollBundle\Entity\Question;
use SC\PollBundle\Form\AnswerType;
use SC\PollBundle\Form\QuestionType;
use SC\PollBundle\Form\VoteType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

class DefaultController extends Controller
{
    public function indexAction()
    {

        /*$poll = $this->getDoctrine()
            ->getRepository('SCPollBundle:Question')
            ->find($id);*/
        $article = $this->getDoctrine()->getRepository('SCArticleBundle:Article')->find(1);


        $poll = null;

        return $this->render('SCPollBundle:Default:index.html.twig', array('poll' => $poll, "article" => $article));
    }


    public function testResultAction(Request $request, $id)
    {
        $hasTested = $this->hasTested($request, $id);
        $answer_id = false;
        if ($hasTested) {
            $answer_id = $hasTested;
        } else {
            if ($request->isMethod('post')) {
                $answer_id = ($request->get("answer"));
            }
        }

        $question = $this->getDoctrine()->getManager()->getRepository('SCPollBundle:Question')->find($id);

/*        if ($answer != null) {
            $answer = $this->getDoctrine()->getManager()->getRepository('SCPollBundle:Answer')->find($answer_id);
            if ($answer) {
                $answer->setClick($answer->getClick() + 1);

                $em = $this->getDoctrine()->getManager();
                $em->persist($answer);
                $em->flush();
            }
        }*/

        $response = $this->render('SCPollBundle:Default:test_result.html.twig', array('question' => $question, "my_answer" => $answer_id));
        $session = $this->get("session");
        $answer = $session->set("test_" . $id, $answer_id);

//        $response->headers->setCookie(new Cookie('test_' . $id, $answer_id, time() + 3600 * 24 * 365));

        return $response;
    }

    public function resetAction(Request $request, $id)
    {
        $session = $this->get("session");
        $session->clear();
        return new JsonResponse();
    }

    public function testAction(Request $request, $id)
    {
        $question = $this->getDoctrine()->getManager()->getRepository('SCPollBundle:Question')->find($id);

        if (!$question) {
            throw $this->createNotFoundException("This poll doesn't exist or has been closed.");
        }

        $hasTested = $this->hasTested($request, $id);
        if ($hasTested !== false) {
            return $this->forward('SCPollBundle:Default:testResult', array('id' => $id, 'hasTested' => $hasTested));
        }

        $answers = array();
        foreach ($question->getAnswers() as $answer) {
            $answers[$answer->getId()] = $answer;
        }
//        shuffle($answers);
        $form = $this->container->get('form.factory')->createNamed(
            'test',
            VoteType::class,
            null,
            array(
                'answers' => $answers,
                'action' => $this->generateUrl('sc_test_vote_result', array('id' => $question->getId())),
            ));

        return $this->render('SCPollBundle:Default:test.html.twig', array('question' => $question, "form" => $form->createView()));

    }


    public function voteResultAction(Request $request, $id)
    {
        $question = $this->getDoctrine()->getManager()->getRepository('SCPollBundle:Question')->find($id);

        $answer_id = $request->get("poll")["answers"];
        if ($answer_id != null) {
            $answer = $this->getDoctrine()->getManager()->getRepository('SCPollBundle:Answer')->find($answer_id);
            $answer->setClick($answer->getClick() + 1);

            $em = $this->getDoctrine()->getManager();
            $em->persist($answer);
            $em->flush();
        }

        $total = 0;
        foreach ($question->getAnswers() as $answer) {
            $total += $answer->getClick();
        }

        $response = $this->render('SCPollBundle:Default:vote_result.html.twig', array('question' => $question, "total" => $total));
        $response->headers->setCookie(new Cookie('poll_' . $id, true, time() + 3600 * 24 * 365));


        return $response;
    }


    public function voteAction(Request $request, $id)
    {
        $question = $this->getDoctrine()->getManager()->getRepository('SCPollBundle:Question')->find($id);

        if (!$question) {
            throw $this->createNotFoundException("This poll doesn't exist or has been closed.");
        }
        $hasTested = $this->hasTested($request, $id);

        if ($hasTested !== false) {
            return $this->forward('SCPollBundle:Default:testResult', array('id' => $id, 'hasTested' => true));
        }

        if ($this->hasVoted($request, $id)) {
            return $this->forward('SCPollBundle:Default:voteResult', array('id' => $id, 'hasVoted' => true));
        }

        $answers = array();
        foreach ($question->getAnswers() as $answer) {
            $answers[$answer->getId()] = $answer;
        }
        //shuffle($answers);

        $type = $question->getTest();
        if ($type) {

/*            $form = $this->get('form.factory')->createNamed(
                'test',
                VoteType::class,
                null,
                array(
                    'answers' => $answers,
                    'action' => $this->generateUrl('sc_test_vote_result', array('id' => $question->getId())),
                ));*/


            //return $this->render('SCPollBundle:Default:test.html.twig', array('question' => $question, "form" => $form->createView()));
            return $this->render('SCPollBundle:Default:test.html.twig', ['question' => $question, 'answers' => $answers]);
        } else {
            $form = $this->container->get('form.factory')->createNamed(
                'poll',
                VoteType::class,
                null,
                array(
                    'answers' => $answers,
                    'action' => $this->generateUrl('sc_poll_vote_result', array('id' => $question->getId())),
                ));

            return $this->render('SCPollBundle:Default:vote.html.twig', array('question' => $question, "form" => $form->createView()));
        }
    }

    protected function hasTested(Request $request, $id)
    {
        $session = $this->get("session");
        $answer = $session->get("test_" . $id);
        if ($answer) {
            return $answer ;
        }
        return false;

        $cookies = $request->cookies;
        if ($cookies->has('test_' . $id)) {
            return $cookies->get('test_' . $id);
        }

        return false;
    }

    protected function hasVoted(Request $request, $id)
    {
        $cookies = $request->cookies;
        if ($cookies->has('poll_' . $id)) {
            return true;
        }

        return false;
    }

    public function editAction(Request $request, $id)
    {
        $question = $this->getDoctrine()->getManager()->getRepository('SCPollBundle:Question')->find($id);

        if (!$question) {
            $question = new Question();
        }

        $form = $this->createCreateForm($question);



        $form->handleRequest($request);

        if ($form->isValid()) {

            $article = $this->getDoctrine()->getManager()->getRepository('SCArticleBundle:Article')->find($request->get('article'));

            $question->setText($request->get('sc_pollbundle_question_text'));

            if ($article != null) 
                $question->setArticle($article);

/*            foreach ($question->getAnswers() as $answer) {
                if ($answer->getValid()) {
                    $question->setTest(true);
                    $question->setValid($answer);
                    break;
                } else {
                    $question->setValid(null);
                    $question->setTest(false);
                }
            }*/

            $em = $this->getDoctrine()->getManager();
            $em->persist($question);

            $em->flush();

            $response = array(
                "code" => "OK",
                "data" => array(
                    "status" => "Обновлено",
                    "time" => date("H:i:s", time()),
                    "question_id" => $question->getId(),
                    "answers" => count($question->getAnswers())
                )
            );

            return new Response(json_encode($response, JSON_UNESCAPED_UNICODE));
        }

        $fileStorageClassName = "\SC\PollBundle\FS\Module";
        return $this->render('SCPollBundle:Default:form.html.twig', array(
            "form" => $form->createView(),
            "question" => $question,
            'previewModTypeId' => $fileStorageClassName::FILE_TYPE_PREVIEW,
            'fileStorageModTypeId' => $fileStorageClassName::FILE_TYPE_OTHER,
            'fsModuleId' => $fileStorageClassName::FILE_STORAGE_MODULE_ID,
        ));
    }

    public function deleteAction($id)
    {
        $question = $this->getDoctrine()->getManager()->getRepository('SCPollBundle:Question')->find($id);

        if ($question != null) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($question);
            $em->flush();
        }
        return new Response("deleted");
    }


    public function createCreateForm(Question $question)
    {
        $form = $this->createForm(new QuestionType(), $question,
            array(
                'action' => $this->generateUrl('sc_poll_edit', array('id' => $question->getId())),
                'answer_form' => new AnswerType(),
                'method' => 'POST',
            ));

        return $form;
    }

    public function getIncludesAction($id)
    {
        $article = $this->getDoctrine()->getRepository("SCArticleBundle:Article")->find($id);
        return $this->render("SCPollBundle:Default:includes.html.twig", array(
            "article" => $article,
        ));
    }

    public function getIncludesFrontAction($id)
    {
        $article = $this->getDoctrine()->getRepository("SCArticleBundle:Article")->find($id);
        return $this->render("SCPollBundle:Default:includes_front.html.twig", array(
            "article" => $article,
        ));
    }


    public function saveAction()
    {

    }
}
