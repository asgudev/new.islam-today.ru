<?php


namespace SC\PollBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AnswerType extends AbstractType
{

    /**
     * Build Form
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('text', "textarea", array(
            "attr" => array(
                "class" => "form-control",
                "rows" => "2"
            )
        ))
            ->add('valid', 'checkbox', array(
                'required' => false,
                'label' => 'Правильный ответ',
                'attr' => array(

                ),
                'label_attr' => array(
                    'for' => null
                )
            ));
    }

    /**
     * Get Name
     *
     * @return string
     */
    public function getName()
    {
        return 'answer';
    }

    /**
     * Set Default Options
     *
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SC\PollBundle\Entity\Answer',
        ));
    }

}