<?php

namespace SC\PollBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class QuestionType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('text', "textarea",
                  array(
                      'label' => "Вопрос",
                      'attr'  => array(
                          'class' => 'form-control',
                          'rows' => '5'
                      )
                  ))
            ->add('answers', 'collection', array(
                'label' => 'Варианты ответов',
                'type' => new $options['answer_form'],
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false
            ))
            ->add("save", "button", array(
                    "label" => "Сохранить",
                    "attr" => array(
                        "class" => "btn btn-block btn-success submit_poll"
                    )
                ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SC\PollBundle\Entity\Question',
            'answer_form' => 'SC\PollBundle\Form\AnswerType',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sc_pollbundle_question';
    }
}
