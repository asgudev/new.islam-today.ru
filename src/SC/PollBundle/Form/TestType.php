<?php

namespace SC\PollBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Choice;

class TestType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('answers', 'choice', array(
                    'choices' => $options['answers'],
                    'multiple' => false,
                    "expanded" => true,
                    'constraints' => array(
                        new NotNull(array('message' => "Please select a choice.")),
                        new Choice(array('choices' => array_keys($options['answers'])))
                    )
                )
            )
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SC\PollBundle\Entity\Question',
            'answer_form' => 'SC\PollBundle\Form\AnswerType',
        ));
        $resolver->setRequired(array('answers'));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sc_pollbundle_test';
    }
}
