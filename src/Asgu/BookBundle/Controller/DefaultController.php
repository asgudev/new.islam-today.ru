<?php

namespace Asgu\BookBundle\Controller;

use Asgu\BookBundle\Entity\Book;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class DefaultController
 * @package Asgu\BookBundle\Controller
 * @Route("/book")
 */
class DefaultController extends Controller
{

    /**
     * @param Request $request
     * @param Book $book
     * @return array
     * @Route("/{id}/show", name="book_show")
     */
    public function showBookAction(Request $request, Book $book)
    {
        if (!$book) {
            return null;
        }
        $dir = $book->getDirectory()."/*.jpg";

        $pages = glob($dir, GLOB_BRACE);
        
        return $this->render("@Book/Default/showBook.html.twig", [
            'book' => $book,
            'pages' => $pages
        ]);
    }

    /**
     * @param Request $request
     * @param Book $book
     * @return array
     * @Route("/{id}/shownew", name="book_show_new")
     */
    public function showBookNewAction(Request $request, Book $book)
    {
        if (!$book) {
            return null;
        }
        $dir = $book->getDirectory()."/*.jpg";

        $pages = glob($dir, GLOB_BRACE);

        return $this->render("@Book/Default/showBookNew.html.twig", [
            'book' => $book,
            'pages' => $pages
        ]);
    }

    private function isTrustedTablet($headers)
    {
        if (array_key_exists("asgumobileclient", $headers) && ($headers["asgumobileclient"][0] == true)) {
            return true;
        }

        return false;
    }


    /**
     * @param Request $request
     * @Route("/list", name="books_list")
     */
    public  function booksList(Request $request)
    {
        if ($this->isTrustedTablet($request->headers->all())) {
            throw $this->createNotFoundException('нет такого раздела');
        }

        $books = $this->getDoctrine()->getRepository("BookBundle:Book")->findAll();

        return $this->render("@Book/Default/bookList.html.twig", [
            'books' => $books
        ]);
    }
}
