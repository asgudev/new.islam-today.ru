<?php

namespace Asgu\GeoipBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

use Asgu\GeoipBundle\Entity\CityIp;

/**
 *
 * @ORM\Table(name="net_city")
 * @ORM\Entity
 */
class City
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name_ru", type="string")
     */
    private $name_ru;

    /**
     * @var string
     *
     * @ORM\Column(name="name_en", type="string")
     */
    private $name_en;

    /**
     * @ORM\OneToMany(targetEntity="SC\BannersBundle\Entity\Banner", mappedBy="city")
     */
    private $banners;

    /**
     * @ORM\OneToMany(targetEntity="Asgu\GeoipBundle\Entity\CityIp", mappedBy="city")
     */
    private $ip;

    public function __construct()
    {
        $this->ip = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getNameRu()
    {
        return $this->name_ru;
    }

    /**
     * @param string $name_ru
     */
    public function setNameRu($name_ru)
    {
        $this->name_ru = $name_ru;
        return $this;
    }

    /**
     * @return string
     */
    public function getNameEn()
    {
        return $this->name_en;
    }

    /**
     * @param string $name_en
     */
    public function setNameEn($name_en)
    {
        $this->name_en = $name_en;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * @param mixed $ip
     */
    public function addIp(CityIp $cityIp)
    {
        $this->ip[] = $cityIp;
    }

}
