<?php

namespace Asgu\GeoipBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Asgu\GeoipBundle\Entity\City;

/**
 * @ORM\Table(name="net_city_ip")
 * @ORM\Entity(repositoryClass="Asgu\GeoipBundle\Entity\CityIpRepository")
 */
class CityIp
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="begin_ip", type="bigint")
     */
    private $beginIp;

    /**
     * @var string
     *
     * @ORM\Column(name="end_ip", type="bigint")
     */
    private $endIp;

    /**
     * @ORM\ManyToOne(targetEntity="Asgu\GeoipBundle\Entity\City", inversedBy="ip")
     */
    private $city;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getBeginIp()
    {
        return $this->beginIp;
    }

    /**
     * @param string $beginIp
     */
    public function setBeginIp($beginIp)
    {
        $this->beginIp = $beginIp;
    }

    /**
     * @return string
     */
    public function getEndIp()
    {
        return $this->endIp;
    }

    /**
     * @param string $end_ip
     */
    public function setEndIp($endIp)
    {
        $this->endIp = $endIp;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity(City $city)
    {
        $this->city = $city;
    }

}
