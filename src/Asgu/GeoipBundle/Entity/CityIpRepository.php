<?php

namespace Asgu\GeoipBundle\Entity;

use Doctrine\ORM\EntityRepository;

class CityIpRepository extends EntityRepository
{
    /**
     * @param $id
     *
     * @return City
     */
    public function findCityByIp($ip)
    {

        $int_ip = sprintf("%u", ip2long($ip));

        $cityIp = $this
            ->createQueryBuilder('cip')
            ->where(":ip BETWEEN cip.beginIp AND cip.endIp ")
            ->setParameter("ip", $int_ip)
            ->getQuery()
            ->getOneOrNullResult()
        ;

        if ($cityIp != null) {
            $city = $cityIp->getCity();
        } else {
            $city = null;
        }

        return $city;

    }
}