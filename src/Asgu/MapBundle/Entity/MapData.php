<?php

namespace Asgu\MapBundle\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 *
 * @ORM\Table(name="map_data")
 * @ORM\Entity
 */
class MapData
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @ORM\ManyToOne(targetEntity="Asgu\MapBundle\Entity\Map", inversedBy="objects")
     */
    private $map;
    
    /**
     * @ORM\Column(type="integer", name="region", nullable=true)
     */
    private $region;

    /**
     * @ORM\Column(type="text", name="content", nullable=true)
     */
    private $content;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set map
     *
     * @param \Asgu\MapBundle\Entity\Map $map
     *
     * @return MapData
     */
    public function setMap(\Asgu\MapBundle\Entity\Map $map = null)
    {
        $this->map = $map;

        return $this;
    }

    /**
     * Get map
     *
     * @return \Asgu\MapBundle\Entity\Map
     */
    public function getMap()
    {
        return $this->map;
    }

    /**
     * Set region
     *
     * @param integer $region
     *
     * @return MapData
     */
    public function setRegion($region)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return integer
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return MapData
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }
}
