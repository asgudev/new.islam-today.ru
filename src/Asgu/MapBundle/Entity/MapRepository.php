<?php

namespace Asgu\MapBundle\Entity;


use Doctrine\ORM\EntityRepository;

class MapRepository extends EntityRepository
{
    /**
     * @param Map $map
     * @return Map
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findMapWithData(Map $map)
    {
        return $this->createQueryBuilder('m')
            ->where('m = :map')
            ->addSelect('mo')
            ->leftJoin('m.objects', 'mo')
            ->setParameters([
                'map' => $map
            ])
            ->getQuery()
            ->getOneOrNullResult();
    }
}