<?php
/**
 * Created by IntelliJ IDEA.
 * User: iProtoss
 * Date: 27.07.2016
 * Time: 12:48
 */

namespace Asgu\MapBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MapDataType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('content', TextareaType::class, [
                'required' => false,
            ])
            ->add('region', HiddenType::class)
            ->add('submit', SubmitType::class, [
                'label' => 'Сохранить информацию о регионе'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Asgu\MapBundle\Entity\MapData',
        ));
    }
}