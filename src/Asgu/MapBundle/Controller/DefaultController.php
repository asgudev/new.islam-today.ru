<?php

namespace Asgu\MapBundle\Controller;

use Asgu\MapBundle\Entity\Map;
use Asgu\MapBundle\Entity\MapData;
use Asgu\MapBundle\Form\MapDataType;
use Asgu\MapBundle\Form\MapType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @Route("/map")
 */
class DefaultController extends Controller
{
    /**
     * @Route("/list", name="maps_list")
     */
    public function mapsListAction(Request $request)
    {
        $maps = $this->getDoctrine()->getRepository('MapBundle:Map')->findAll();

        return $this->render('@Map/Default/mapList.html.twig', [
            'maps' => $maps,
        ]);
    }

    /**
     * @param Request $request
     * @Route("/create", name="map_create")
     */
    public function createMapAction(Request $request)
    {
        $map = new Map();

        $em = $this->getDoctrine()->getManager();
        $em->persist($map);
        $em->flush();


        return $this->redirectToRoute('map_edit', [
            'id' => $map->getId(),
        ]);
    }

    /**
     * @param Request $request
     * @Route("/edit/{id}", name="map_edit")
     */
    public function editMapAction(Request $request, Map $map)
    {
        $map = $this->getDoctrine()->getRepository('MapBundle:Map')->findMapWithData($map);

        $mapData = [];

        /**
         * @var MapData $data
         */
        foreach ($map->getObjects() as $data) {
            $mapData[$data->getRegion()] = [
                'id' => $data->getId(),
                'region' => $data->getRegion(),
            ];
        }

        $form = $this->createForm(MapType::class, $map);

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($map);
                $em->flush();
                return new JsonResponse([
                    'status' => true,
                ]);
            }
        }

        return $this->render('@Map/Default/mapEdit.html.twig', [
            'map' => $map,
            'form' => $form->createView(),
            'data' => $mapData,
        ]);
    }

    /**
     * @param Request $request
     * @Route("/edit/{id}/create", name="map_data_create")
     */
    public function createMapDataAction(Request $request, Map $map)
    {


        $mapData = new MapData();
        $mapData->setMap($map);
        $mapData->setRegion($request->get('region'));


        $form = $this->createForm(MapDataType::class, $mapData, [
            'action' => $this->generateUrl('map_data_create', [
                'id' => $map->getId(),
            ])
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($mapData);
                $em->flush();
                return new JsonResponse([
                    'status' => true,
                    'type' => 'region',
                    'region' => $mapData->getRegion(),
                    'id' => $mapData->getId(),
                ]);
            } else {
                dump($form->getErrors());
                die();
            }
        } else {
            $mapData->setRegion($request->get('region'));
        }

        return $this->render('@Map/Default/mapDataEdit.html.twig', [
            'mapData' => $mapData,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param Request $request
     * @Route("/data/{id}/edit", name="map_data_edit")
     */
    public function editMapDataAction(Request $request, MapData $mapData)
    {
        //$mapData = $this->getDoctrine()->getRepository("MapBundle:MapData")->find($request->get('region'));

        if ((!$mapData) or ($this->getUser() === null)) {
            throw new NotFoundHttpException();
        }

        $form = $this->createForm(MapDataType::class, $mapData, [
            'action' => $this->generateUrl('map_data_edit', [
                'id' => $mapData->getId(),
            ])
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($mapData);
                $em->flush();
                return new JsonResponse([
                    'status' => true,
                    'type' => 'region',
                    'region' => $mapData->getRegion(),
                    'id' => $mapData->getId(),
                ]);
            } else {
                dump($form->getErrors());
                die();
            }
        } else {
            $mapData->setRegion($request->get('region'));
        }

        return $this->render('@Map/Default/mapDataEdit.html.twig', [
            'mapData' => $mapData,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param Request $request
     * @Route("/data/{id}/show", name="map_data_show")
     */
    public function showMapDataAction(Request $request, MapData $mapData)
    {
        //$mapData = $this->getDoctrine()->getRepository("MapBundle:MapData")->find($request->get('region'));

        if (!$mapData) {
            throw new NotFoundHttpException();
        }

        return new JsonResponse([
            'status' => true,
            'data' => $mapData->getContent(),
        ]);


    }

    /**
     * @param Request $request
     * @Route("/data/{id}/delete", name="map_data_delete")
     */
    public function deleteMapDataAction(Request $request, MapData $mapData)
    {
        //$mapData = $this->getDoctrine()->getRepository("MapBundle:MapData")->find($request->get('region'));

        if ((!$mapData) or ($this->getUser() === null)) {
            throw new NotFoundHttpException();
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($mapData);
        $em->flush();

        return new JsonResponse([
            'status' => true,
            'region' => $mapData->getRegion(),
        ]);
    }

    /**
     * @param Request $request
     * @Route("/show/{id}", name="show_map")
     */
    public function showMapAction(Request $request, Map $map)
    {
        if (!$map) {
            throw new NotFoundHttpException();
        }

        $mapData = [];

        /**
         * @var MapData $data
         */
        foreach ($map->getObjects() as $data) {
            $mapData[$data->getRegion()] = [
                'id' => $data->getId(),
                'region' => $data->getRegion(),
            ];
        }

        return $this->render('@Map/Default/mapShow.html.twig', [
            'map' => $map,
            'data' => $mapData,
        ]);
    }
}
