<?php
namespace IslamToday\ApplicationBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\ArrayInput;

use IslamToday\ApplicationBundle\Entity\Times;
use IslamToday\ApplicationBundle\Entity\City;

/**
 * Class for AbstractCommand
 */
class ParseNamazCommand extends ContainerAwareCommand
{
    /**
     * Configure the command.
     */
    protected function configure()
    {
        $this
            ->setName('namaz2:parse')
            ->setDescription('Add namaz times from csv-file')
            ->addArgument(
                'file',
                InputArgument::OPTIONAL,
                'Who do you want to greet?'
            )
        ;

    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $file = $input->getArgument('file');
//        $lines = file($file);
        $lines = explode("\r", file($file)[0]);

        $citiesAll = $this->getContainer()->get('doctrine')
            ->getRepository('IslamTodayApplicationBundle:City')->findAll();
        $citiesArr = [];
        foreach ($citiesAll as $key => $city) {
            $citiesArr[$city->getTitle()] = $city;
        };
        $citiesStart = [];
        $citiesEnd = [];

$i =0;
        foreach($lines as $lineNum => $line){
                $columns = explode(';', $line);
//var_dump($columns);die();
                if(isset($citiesArr[$columns[0]])){
                    $city = $citiesArr[$columns[0]];
                }else{
                    $city = new City();
                    $city->setTitle($columns[0]);
                    $city->setLat('');
                    $city->setLng('');
                    $city->setTimezone(0);
                    $city->setPath('');
                    $this->getContainer()->get('doctrine')->getManager()->persist($city);
                    $this->getContainer()->get('doctrine')->getManager()->flush(); 
var_dump('new_City '. $columns[0]);die();
                    exit;
                }

                if(!isset($citiesStart[$city->getId()])){
                    $maxTimeObj = $this->getContainer()->get('doctrine')
                        ->getRepository('IslamTodayApplicationBundle:Times')->findOneBy(['city' => $city], ['date' => 'desc']);
                    if($maxTimeObj){
                        $citiesStart[$city->getId()] = $maxTimeObj->getDate();
                    }else{
                        $citiesStart[$city->getId()] = new \DateTime('2016-12-31');
                    }
                    //$citiesStart[$city->getId()] = new \DateTime('2016-12-31');
                    $citiesEnd[$city->getId()] = new \DateTime('2020-01-01');
                };

                $currentRowDate = new \DateTime($columns[1]);
//dump($currentRowDate);
                if (($currentRowDate <= $citiesStart[$city->getId()]) || ($currentRowDate >= $citiesEnd[$city->getId()])){
                    continue;
                };
                $newTime = new Times();
                $newTime->setCity($city);
                $newTime->setDate($currentRowDate);
                $newTime->setFajr(new \DateTime($columns[1].' '.$columns[3]));
                $newTime->setTulu(new \DateTime($columns[1].' '.$columns[4]));
                $newTime->setZuhr(new \DateTime($columns[1].' '.$columns[6]));
                $newTime->setAsr(new \DateTime($columns[1].' '.$columns[7]));
                $newTime->setMaghrib(new \DateTime($columns[1].' '.$columns[8]));
                $newTime->setIsha(new \DateTime($columns[1].' '.$columns[9]));
//dump($newTime);die();
                $this->getContainer()->get('doctrine')->getManager()->persist($newTime);
                $this->getContainer()->get('doctrine')->getManager()->flush();  

dump($i);

if ($i == 300) exit;
$i += 1;
//die();
                // print_r($currentRowDate);
                // exit;
        }
    }
}
