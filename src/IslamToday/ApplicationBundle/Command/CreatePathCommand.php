<?php
namespace IslamToday\ApplicationBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\ArrayInput;
use Doctrine\Bundle\DoctrineBundle\Registry;

/**
 * Class for AbstractCommand
 */
class CreatePathCommand extends ContainerAwareCommand
{
    /**
     * Configure the command.
     */
    protected function configure()
    {
        $this
            ->setName('city:createpath')
            ->setDescription('Convert old address schema');

    }

    /**
     * Executes the command.
     *
     * @param InputInterface $input  The input
     * @param OutputInterface $output The output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $cities = $this->getContainer()->get('doctrine')
            ->getRepository('IslamTodayApplicationBundle:City')->findAll();


        foreach ($cities as $city) {
            $city->setPath(self::createPath($city->getTitle()));
            $this->getContainer()->get('doctrine')->getManager()->persist($city);
            $this->getContainer()->get('doctrine')->getManager()->flush();
        }
     }

    public static function createPath($title)
    {

        /*$path = transliterate($title,
            array("cyrillic_lowercase", "latin_lowercase",
                'han_transliterate', 'diacritical_remove',
                'cyrillic_transliterate'), 'utf-8', 'utf-8');*/


        $t = \Transliterator::create("latin; NFKD; [^\u0000-\u007E] Remove; NFC");
        $path = $t->transliterate($title);
        $path = strtolower($path);

        $path = preg_replace('/[\'"]+/i', '', $path);
        $path = preg_replace('/[^a-z0-9_]+/i', '-', $path);
        $path = trim($path, '-');

        return $path;
    }
}
