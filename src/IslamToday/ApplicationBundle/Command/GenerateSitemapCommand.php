<?php

namespace IslamToday\ApplicationBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\ArrayInput;
use Doctrine\Bundle\DoctrineBundle\Registry;

/**
 * Class for AbstractCommand
 */
class GenerateSitemapCommand extends ContainerAwareCommand
{
    /**
     * Configure the command.
     */
    protected function configure()
    {
        $this
            ->setName('sitemap:generate')
            ->setDescription('Convert old address schema');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $webDir = $this->getContainer()->get('kernel')->getProjectDir() . "/web";


        $url = $this->getContainer()->getParameter('sc_article.hostname.url');
        // 1. общая карта
        $sitemap = $this->getContainer()->get('templating')->renderResponse('IslamTodayApplicationBundle:Sitemap:sitemap.xml.twig', ['url' => $url]);

        file_put_contents($webDir . '/sitemap.xml', $sitemap->getContent());


        // 2. карта категорий
        $categories = $this->getContainer()->get('doctrine')->getRepository('SCCategoryBundle:Category')->findAll();
        $fetvyCat = $this->getContainer()->get('doctrine')->getRepository('SCCategoryBundle:Category')->findOneById([39]);

        $ignoreCats = [76, 71, 77, 78, 79, 80, 81, 82];

        $sorted = array();
        foreach ($categories as $cat) {
            if (strlen($cat->getTitle()) == 0 || $cat->getId() == 39 || in_array($cat->getId(), $ignoreCats) !== false) {
                continue;
            }
            if ($cat->getLevel() == 1) {
                $sorted[$cat->getId()]['parent'] = $cat;
                if ($cat->getPath() == 'veroucenie') {
                    $sorted[$cat->getId()]['subs'][] = $fetvyCat;
                }

                foreach ($categories as $subCat) {
                    if (strlen($subCat->getTitle()) == 0) {
                        continue;
                    }
                    if ($cat->getId() == $subCat->getParentId()) {
                        $sorted[$cat->getId()]['subs'][] = $subCat;
                    }
                }
            }
        }

//        foreach ($categories as $k => $cat) {
//            if (array_search($cat->getId(), $ignoreCats) !== false) {
//                unset($categories[$k]);
//            }
//        }

        $sitemap_category = $this->getContainer()->get('templating')->renderResponse('IslamTodayApplicationBundle:Sitemap:sitemap_category.xml.twig',
            ['url' => $url, 'categories' => $sorted]);

        file_put_contents($webDir . '/sitemap_category.xml', $sitemap_category->getContent());

        //3. карта новостей
        $news = $this->getContainer()->get('doctrine')->getRepository('SCArticleBundle:Article')
            ->findBy(['primary_category' => 38, 'isPublished' => 1], ['publishedAt' => 'asc']);

        $secularNews = $this->getContainer()->get('doctrine')->getRepository('SCArticleBundle:Article')
            ->findBy(['primary_category' => 62, 'isPublished' => 1], ['publishedAt' => 'asc']);

        $sitemap_news = $this->getContainer()->get('templating')->renderResponse('IslamTodayApplicationBundle:Sitemap:sitemap_news.xml.twig',
            ['url' => $url, 'all_news' => array_merge($news, $secularNews)]);

        file_put_contents($webDir . '/sitemap_news.xml', $sitemap_news->getContent());


        //4. карта статей

        $dql = "SELECT a FROM SCArticleBundle:Article a WHERE a.isPublished = 1 and a.primary_category not in(76, 38, 62) ORDER BY a.publishedAt ASC";
        $query = $this->getContainer()->get('doctrine')->getManager()->createQuery($dql);
        $query->setMaxResults(300);


        $sitemap_articles = $this->getContainer()->get('templating')->renderResponse('IslamTodayApplicationBundle:Sitemap:sitemap_articles.xml.twig',
            ['url' => $url, 'articles' => $query->getResult()]);

        file_put_contents($webDir . '/sitemap_articles.xml', $sitemap_articles->getContent());

    }
}

