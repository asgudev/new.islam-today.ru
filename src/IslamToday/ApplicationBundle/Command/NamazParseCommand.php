<?php
namespace IslamToday\ApplicationBundle\Command;

use IslamToday\ApplicationBundle\Entity\City;
use IslamToday\ApplicationBundle\Entity\RamadanTimes;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\ArrayInput;
use Doctrine\Bundle\DoctrineBundle\Registry;

/**
 * Class for AbstractCommand
 */
class NamazParseCommand extends ContainerAwareCommand
{
    /**
     * Configure the command.
     */
    protected function configure()
    {
        $this
            ->setName('namaz:parse')
            ->setDescription('Import csv-file')
            ->addArgument('file', InputArgument::REQUIRED, 'CSV-file');
    }
//TRUNCATE `times_ramadan`
//UPDATE `times_ramadan` SET `time4`=concat(date(`time4`), ' 12:00:00') WHERE `city`=1;

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $doctrine = $this->getContainer()->get('doctrine');
        $em = $doctrine->getManager();
        $cityRepo = $this->getContainer()->get('doctrine')
            ->getRepository('IslamTodayApplicationBundle:City');
        $ramadanTimesRepo = $doctrine->getRepository('IslamTodayApplicationBundle:RamadanTimes');
        $file = $input->getArgument('file');
        $rows = file($file);
        $cities = [];
        for ($i = 0; $i < count($rows); $i++) {
            $row = explode(';', $rows[$i]);


            if (!isset($cities[$row[0]])) {
                $city = $cityRepo->findOneByTitle(trim($row[0]));
            };
            if ($city) {
                $cities[$row[0]] = $city;
            } else {
                $city = new City();
                $city->setTitle($row[0]);
                $em->persist($city);

                $em->flush();
            };

            $date = new \DateTime($row[1]);
            if ($ramadanTimesRepo->findBy(['city' => $city, 'date' => $date])) {
                continue;
            };

/*
            $time = new RamadanTimes();
            $time->setCity($city);

            $time->setDate($date);

            $datetime = clone $date;
            $datetime->add(new \DateInterval('PT' . str_replace(':', 'H', $row[2]) . 'M'));
            $time->setTime1($datetime);

            $datetime = clone $date;
            $datetime->add(new \DateInterval('PT' . str_replace(':', 'H', $row[3]) . 'M'));
            $time->setTime2($datetime);

            $datetime = clone $date;
            $datetime->add(new \DateInterval('PT' . str_replace(':', 'H', $row[4]) . 'M'));
            $time->setTime3($datetime);

            $datetime = clone $date;
            $datetime->add(new \DateInterval('PT' . str_replace(':', 'H', $row[5]) . 'M'));
            $time->setTime4($datetime);

            $datetime = clone $date;
            $datetime->add(new \DateInterval('PT' . str_replace(':', 'H', $row[6]) . 'M'));
            $time->setTime5($datetime);

            $datetime = clone $date;
            $datetime->add(new \DateInterval('PT' . str_replace(':', 'H', $row[7]) . 'M'));
            $time->setTime6($datetime);

            $datetime = clone $date;
            $datetime->add(new \DateInterval('PT' . str_replace(':', 'H', $row[8]) . 'M'));
            $time->setTime7($datetime);

            $datetime = clone $date;
            $datetime->add(new \DateInterval('PT' . str_replace(':', 'H', trim($row[9])) . 'M'));
            $time->setTime8($datetime);
dump($time);die();*/


            $time = new RamadanTimes();
            $time->setCity($city);

            $time->setDate($date);

            $datetime = clone $date;
            $datetime = (new \DateTime($row[2]));
            $time->setTime1($datetime);

            $datetime = clone $date;
            $datetime = (new \DateTime($row[3]));
            $time->setTime2($datetime);

            $datetime = clone $date;
            $datetime = (new \DateTime($row[4]));
            $time->setTime3($datetime);

            $datetime = clone $date;
            $datetime = (new \DateTime($row[5]));
            $time->setTime4($datetime);

            $datetime = clone $date;
            $datetime = (new \DateTime($row[6]));
            $time->setTime5($datetime);

            $datetime = clone $date;
            $datetime = (new \DateTime($row[7]));
            $time->setTime6($datetime);

            $datetime = clone $date;
            $datetime = (new \DateTime($row[8]));
            $time->setTime7($datetime);
/*
            $datetime = clone $date;
            $datetime = (new \DateTime($row[10]));
            $time->setTime8($datetime);*/
//            dump($time);die();


            $em->persist($time);

        }
        $em->flush();
    }

}
