<?php
ini_set('display_errors', "On");
error_reporting(E_ALL + E_STRICT);
require_once('src/Google_Client.php');
require_once('src/contrib/Google_AnalyticsService.php');
 
$client = new Google_Client();
$client->setApplicationName('My cool application');
 
$client->setAssertionCredentials(
    new Google_AssertionCredentials(  
    '886249913276-vu21envbv74rquea7um089m28obihops@developer.gserviceaccount.com',
    array('https://www.googleapis.com/auth/analytics.readonly'),
    file_get_contents('src/568cfbdccaa0095ed814d7bd084dee690c578563-privatekey.p12')
));
 
// Client ID со страницы API Access
$client->setClientId('886249913276-vu21envbv74rquea7um089m28obihops.apps.googleusercontent.com');
$client->setAccessType('offline_access');
 
$service = new Google_AnalyticsService($client);


$data = $service->data_ga->get('ga:61524664', date('Y-m-d'), date('Y-m-d'), 'ga:visits', array(
    
));
$visits = $data['rows'][0][0];
//header("Content-type: image/png");

$string = $visits;
$im     = imagecreatefrompng("counter_bg.png");
$orange = imagecolorallocate($im, 201, 96, 82);
$px     = (imagesx($im) - 7.5 * strlen($string)) / 2;
$font = "PTC75F-webfont.ttf";
$fontSize = 12;
$tb = imagettfbbox($fontSize, 0, $font, $string);
$x = ceil((112 - $tb[2]) / 2)+23;
imagettftext ($im, $fontSize, 0, $x, 28, $orange, $font, $string);
imagealphablending( $im, false );
imagesavealpha( $im, true );
//imagepng($im);

ob_start();
imagepng($im);
$data = ob_get_clean();
imagedestroy($im);
file_put_contents('g_counter.png',$data);
?>