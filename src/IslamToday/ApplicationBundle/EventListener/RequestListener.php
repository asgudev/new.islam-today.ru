<?php

namespace IslamToday\ApplicationBundle\EventListener;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use MaxMind\Db\Reader;

class RequestListener
{
    private $rootDir;
    private $em;

    public function __construct($rootDir, EntityManager $em)
    {
        $this->rootDir = $rootDir;
        $this->em = $em;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        $sessionCity = $event->getRequest()->getSession()->get('userCity');
        if ($event->getRequest()->get('changeCity')) {
            $city = $this->em->getRepository('IslamTodayApplicationBundle:City')
                ->find($event->getRequest()->get('changeCity'));
            if ($city) {
                $sessionCity = ['id' => $city->getId(), 'title' => $city->getTitle()];
            }
        } elseif (!$sessionCity) {
            $databaseFile = $this->rootDir . '/../GeoIP2-City.mmdb';
            $sessionCity = ['id' => null];
            if (is_file($databaseFile)) {
                $reader = new Reader($databaseFile);

                $userIp = $event->getRequest()->getClientIp();
                $userIpInfo = $reader->get($userIp);
                if (isset($userIpInfo['city'])) {
                    $city = $this->em->getRepository('IslamTodayApplicationBundle:City')
                        ->findOneByTitle($userIpInfo['city']['names']['ru']);
                    if ($city) {
                        $sessionCity = ['id' => $city->getId(), 'title' => $city->getTitle()];
                    };
                } else {
                    $sessionCity = ['id' => 1, 'title' => 'Казань'];
                }
            };
        }
        if ($sessionCity) {
            $event->getRequest()->getSession()->set('userCity', $sessionCity);
        }
    }
}