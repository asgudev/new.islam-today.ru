<?php

namespace IslamToday\ApplicationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="times_ramadan")
 * @ORM\Entity
 */
class RamadanTimes
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="City", inversedBy="times")
     * @ORM\JoinColumn(name="city", referencedColumnName="id")
     */
    protected $city;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date", nullable=false)
     */
    private $date;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="time1", type="datetime", nullable=false)
     */
    private $time1;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="time2", type="datetime", nullable=false)
     */
    private $time2;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="time3", type="datetime", nullable=false)
     */
    private $time3;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="time4", type="datetime", nullable=false)
     */
    private $time4;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="time5", type="datetime", nullable=false)
     */
    private $time5;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="time6", type="datetime", nullable=false)
     */
    private $time6;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="time7", type="datetime", nullable=false)
     */
    private $time7;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="time8", type="datetime", nullable=false)
     */
    private $time8;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Times
     */
    public function setDate($date)
    {
        $this->date = $date;
    
        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set city
     *
     * @param \IslamToday\ApplicationBundle\Entity\City $city
     * @return Times
     */
    public function setCity(\IslamToday\ApplicationBundle\Entity\City $city = null)
    {
        $this->city = $city;
    
        return $this;
    }

    /**
     * Get city
     *
     * @return \IslamToday\ApplicationBundle\Entity\City
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set time1
     *
     * @param \DateTime $time1
     * @return RamadanTimes
     */
    public function setTime1($time1)
    {
        $this->time1 = $time1;

        return $this;
    }

    /**
     * Get time1
     *
     * @return \DateTime 
     */
    public function getTime1()
    {
        return $this->time1;
    }

    /**
     * Set time2
     *
     * @param \DateTime $time2
     * @return RamadanTimes
     */
    public function setTime2($time2)
    {
        $this->time2 = $time2;

        return $this;
    }

    /**
     * Get time2
     *
     * @return \DateTime 
     */
    public function getTime2()
    {
        return $this->time2;
    }

    /**
     * Set time3
     *
     * @param \DateTime $time3
     * @return RamadanTimes
     */
    public function setTime3($time3)
    {
        $this->time3 = $time3;

        return $this;
    }

    /**
     * Get time3
     *
     * @return \DateTime 
     */
    public function getTime3()
    {
        return $this->time3;
    }

    /**
     * Set time4
     *
     * @param \DateTime $time4
     * @return RamadanTimes
     */
    public function setTime4($time4)
    {
        $this->time4 = $time4;

        return $this;
    }

    /**
     * Get time4
     *
     * @return \DateTime 
     */
    public function getTime4()
    {
        return $this->time4;
    }

    /**
     * Set time5
     *
     * @param \DateTime $time5
     * @return RamadanTimes
     */
    public function setTime5($time5)
    {
        $this->time5 = $time5;

        return $this;
    }

    /**
     * Get time5
     *
     * @return \DateTime 
     */
    public function getTime5()
    {
        return $this->time5;
    }

    /**
     * Set time6
     *
     * @param \DateTime $time6
     * @return RamadanTimes
     */
    public function setTime6($time6)
    {
        $this->time6 = $time6;

        return $this;
    }

    /**
     * Get time6
     *
     * @return \DateTime 
     */
    public function getTime6()
    {
        return $this->time6;
    }

    /**
     * Set time7
     *
     * @param \DateTime $time7
     * @return RamadanTimes
     */
    public function setTime7($time7)
    {
        $this->time7 = $time7;

        return $this;
    }

    /**
     * Get time7
     *
     * @return \DateTime 
     */
    public function getTime7()
    {
        return $this->time7;
    }

    /**
     * Set time8
     *
     * @param \DateTime $time8
     * @return RamadanTimes
     */
    public function setTime8($time8)
    {
        $this->time8 = $time8;

        return $this;
    }

    /**
     * Get time8
     *
     * @return \DateTime 
     */
    public function getTime8()
    {
        return $this->time8;
    }
}
