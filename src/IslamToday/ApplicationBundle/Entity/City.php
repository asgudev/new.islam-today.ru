<?php

namespace IslamToday\ApplicationBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="city")
 * @ORM\Entity(repositoryClass="IslamToday\ApplicationBundle\Repositories\CityRepository")
 */
class City
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=250)
     */
    private $title;


    /**
     * @ORM\Column(type="string", length=250)
     */
    private $lat;


    /**
     * @ORM\Column(type="string", length=250)
     */
    private $lng;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private $timezone;

    /**
     * @ORM\Column(type="string", length=250)
     */
    private $path;


    /**
     * @ORM\OneToMany(targetEntity="Times", mappedBy="city")
     */
    protected $times;

    /**
     * @ORM\OneToMany(targetEntity="RamadanTimes", mappedBy="city")
     */
    protected $ramadanTimes;

    public function __construct()
    {
        $this->times = new ArrayCollection();
        $this->lat = '';
        $this->lng = '';
        $this->timezone = '';
        $this->path = '';
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return City
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set lat
     *
     * @param string $lat
     * @return City
     */
    public function setLat($lat)
    {
        $this->lat = $lat;
    
        return $this;
    }

    /**
     * Get lat
     *
     * @return string 
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * Set lng
     *
     * @param string $lng
     * @return City
     */
    public function setLng($lng)
    {
        $this->lng = $lng;
    
        return $this;
    }

    /**
     * Get lng
     *
     * @return string 
     */
    public function getLng()
    {
        return $this->lng;
    }

    /**
     * Set timezone
     *
     * @param integer $timezone
     * @return City
     */
    public function setTimezone($timezone)
    {
        $this->timezone = $timezone;
    
        return $this;
    }

    /**
     * Get timezone
     *
     * @return integer 
     */
    public function getTimezone()
    {
        return $this->timezone;
    }

    /**
     * Set path
     *
     * @param string $path
     * @return City
     */
    public function setPath($path)
    {
        $this->path = $path;
    
        return $this;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Add times
     *
     * @param \IslamToday\ApplicationBundle\Entity\Times $times
     * @return City
     */
    public function addTime(\IslamToday\ApplicationBundle\Entity\Times $times)
    {
        $this->times[] = $times;
    
        return $this;
    }

    /**
     * Remove times
     *
     * @param \IslamToday\ApplicationBundle\Entity\Times $times
     */
    public function removeTime(\IslamToday\ApplicationBundle\Entity\Times $times)
    {
        $this->times->removeElement($times);
    }

    /**
     * Get times
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTimes()
    {
        return $this->times;
    }

    /**
     * Add ramadanTimes
     *
     * @param \IslamToday\ApplicationBundle\Entity\RamadanTimes $ramadanTimes
     * @return City
     */
    public function addRamadanTime(\IslamToday\ApplicationBundle\Entity\RamadanTimes $ramadanTimes)
    {
        $this->ramadanTimes[] = $ramadanTimes;

        return $this;
    }

    /**
     * Remove ramadanTimes
     *
     * @param \IslamToday\ApplicationBundle\Entity\RamadanTimes $ramadanTimes
     */
    public function removeRamadanTime(\IslamToday\ApplicationBundle\Entity\RamadanTimes $ramadanTimes)
    {
        $this->ramadanTimes->removeElement($ramadanTimes);
    }

    /**
     * Get ramadanTimes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRamadanTimes()
    {
        return $this->ramadanTimes;
    }
}
