<?php

namespace IslamToday\ApplicationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="sc_article_views")
 * @ORM\Entity(repositoryClass="IslamToday\ApplicationBundle\Repositories\ArticleViewsRepository")
 */
class ArticleViews
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToOne(targetEntity="\SC\ArticleBundle\Entity\Article")
     */
    protected $article;

    /**
     * @ORM\Column(type="integer")
     */
    protected $ip = 0;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $date;

    
    public function __construct() 
    {
        $this->date = new \DateTime();
    }
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ip
     *
     * @param integer $ip
     */
    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip
     *
     * @return integer
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set date
     *
     * @param integer $date
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return integer
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set article
     *
     * @param \SC\ArticleBundle\Entity\Article $article
     * @return ArticleStats
     */
    public function setArticle(\SC\ArticleBundle\Entity\Article $article = null)
    {
        $this->article = $article;
    
        return $this;
    }

    /**
     * Get article
     *
     * @return \SC\ArticleBundle\Entity\Article 
     */
    public function getArticle()
    {
        return $this->article;
    }
}
