<?php

namespace IslamToday\ApplicationBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="arab_alphabets")
 * @ORM\Entity
 */
class ArabAlphabet
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=250)
     */
    protected $cyrillic;

    /**
     * @ORM\Column(type="string", length=250)
     */
    protected $latin;

    /**
     * @ORM\Column(type="string", length=250)
     */
    protected $isolated;

    /**
     * @ORM\Column(type="string", length=250)
     */
    protected $beginning;

    /**
     * @ORM\Column(type="string", length=250)
     */
    protected $middle;

    /**
     * @ORM\Column(type="string", length=250)
     */
    protected $end;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    protected $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    protected $updatedAt;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cyrillic
     *
     * @param string $cyrillic
     * @return ArabAlphabet
     */
    public function setCyrillic($cyrillic)
    {
        $this->cyrillic = $cyrillic;
    
        return $this;
    }

    /**
     * Get cyrillic
     *
     * @return string 
     */
    public function getCyrillic()
    {
        return $this->cyrillic;
    }

    /**
     * Set latin
     *
     * @param string $latin
     * @return ArabAlphabet
     */
    public function setLatin($latin)
    {
        $this->latin = $latin;
    
        return $this;
    }

    /**
     * Get latin
     *
     * @return string 
     */
    public function getLatin()
    {
        return $this->latin;
    }

    /**
     * Set isolated
     *
     * @param string $isolated
     * @return ArabAlphabet
     */
    public function setIsolated($isolated)
    {
        $this->isolated = $isolated;
    
        return $this;
    }

    /**
     * Get isolated
     *
     * @return string 
     */
    public function getIsolated()
    {
        return $this->isolated;
    }

    /**
     * Set beginning
     *
     * @param string $beginning
     * @return ArabAlphabet
     */
    public function setBeginning($beginning)
    {
        $this->beginning = $beginning;
    
        return $this;
    }

    /**
     * Get beginning
     *
     * @return string 
     */
    public function getBeginning()
    {
        return $this->beginning;
    }

    /**
     * Set middle
     *
     * @param string $middle
     * @return ArabAlphabet
     */
    public function setMiddle($middle)
    {
        $this->middle = $middle;
    
        return $this;
    }

    /**
     * Get middle
     *
     * @return string 
     */
    public function getMiddle()
    {
        return $this->middle;
    }

    /**
     * Set end
     *
     * @param string $end
     * @return ArabAlphabet
     */
    public function setEnd($end)
    {
        $this->end = $end;
    
        return $this;
    }

    /**
     * Get end
     *
     * @return string 
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return ArabAlphabet
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return ArabAlphabet
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    
        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}
