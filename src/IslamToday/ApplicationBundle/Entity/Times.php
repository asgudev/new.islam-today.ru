<?php

namespace IslamToday\ApplicationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="times")
 * @ORM\Entity
 */
class Times
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="City", inversedBy="times")
     * @ORM\JoinColumn(name="city", referencedColumnName="id")
     */
    protected $city;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date", nullable=false)
     */
    private $date;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fajr", type="datetime", nullable=false)
     */
    private $fajr;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="tulu", type="datetime", nullable=false)
     */
    private $tulu;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="zuhr", type="datetime", nullable=false)
     */
    private $zuhr;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="asr", type="datetime", nullable=false)
     */
    private $asr;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="maghrib", type="datetime", nullable=false)
     */
    private $maghrib;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="isha", type="datetime", nullable=false)
     */
    private $isha;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Times
     */
    public function setDate($date)
    {
        $this->date = $date;
    
        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set fajr
     *
     * @param \DateTime $fajr
     * @return Times
     */
    public function setFajr($fajr)
    {
        $this->fajr = $fajr;
    
        return $this;
    }

    /**
     * Get fajr
     *
     * @return \DateTime 
     */
    public function getFajr()
    {
        return $this->fajr;
    }

    /**
     * Set tulu
     *
     * @param \DateTime $tulu
     * @return Times
     */
    public function setTulu($tulu)
    {
        $this->tulu = $tulu;
    
        return $this;
    }

    /**
     * Get tulu
     *
     * @return \DateTime 
     */
    public function getTulu()
    {
        return $this->tulu;
    }

    /**
     * Set zuhr
     *
     * @param \DateTime $zuhr
     * @return Times
     */
    public function setZuhr($zuhr)
    {
        $this->zuhr = $zuhr;
    
        return $this;
    }

    /**
     * Get zuhr
     *
     * @return \DateTime 
     */
    public function getZuhr()
    {
        return $this->zuhr;
    }

    /**
     * Set asr
     *
     * @param \DateTime $asr
     * @return Times
     */
    public function setAsr($asr)
    {
        $this->asr = $asr;
    
        return $this;
    }

    /**
     * Get asr
     *
     * @return \DateTime 
     */
    public function getAsr()
    {
        return $this->asr;
    }

    /**
     * Set maghrib
     *
     * @param \DateTime $maghrib
     * @return Times
     */
    public function setMaghrib($maghrib)
    {
        $this->maghrib = $maghrib;
    
        return $this;
    }

    /**
     * Get maghrib
     *
     * @return \DateTime 
     */
    public function getMaghrib()
    {
        return $this->maghrib;
    }

    /**
     * Set isha
     *
     * @param \DateTime $isha
     * @return Times
     */
    public function setIsha($isha)
    {
        $this->isha = $isha;
    
        return $this;
    }

    /**
     * Get isha
     *
     * @return \DateTime 
     */
    public function getIsha()
    {
        return $this->isha;
    }

    /**
     * Set city
     *
     * @param \IslamToday\ApplicationBundle\Entity\City $city
     * @return Times
     */
    public function setCity(\IslamToday\ApplicationBundle\Entity\City $city = null)
    {
        $this->city = $city;
    
        return $this;
    }

    /**
     * Get city
     *
     * @return \IslamToday\ApplicationBundle\Entity\City
     */
    public function getCity()
    {
        return $this->city;
    }
}
