<?php

namespace IslamToday\ApplicationBundle\Repositories;

use Doctrine\ORM\EntityRepository;
use IslamToday\ApplicationBundle\Entity\ArticleViews;
use SC\ArticleBundle\Entity\Article;

class CityRepository extends EntityRepository
{
    public function findByNamaz()
    {
        $dql = 'SELECT c FROM IslamTodayApplicationBundle:City c '
            .'INNER JOIN c.times t '
            .'GROUP BY c.id';
        $query = $this->getEntityManager()->createQuery($dql);
        $result = $query->getResult();

        return $result;
    }

    public function findByRamadan()
    {
        $dql = 'SELECT c FROM IslamTodayApplicationBundle:City c '
            .'INNER JOIN c.ramadanTimes t '
            .'GROUP BY c.id';
        $query = $this->getEntityManager()->createQuery($dql);
        $result = $query->getResult();

        return $result;
    }
}
