<?php

namespace IslamToday\ApplicationBundle\Repositories;

use Doctrine\ORM\EntityRepository;
use IslamToday\ApplicationBundle\Entity\ArticleViews;
use SC\ArticleBundle\Entity\Article;

class ArticleViewsRepository extends EntityRepository
{
    public function checkViewByIpToday(Article $article, $ip)
    {
        if(!is_numeric($ip)){
            $ip = ip2long($ip);
        };
        $isViewedToday = $this->createQueryBuilder('v')
            ->where('v.article = :article AND v.ip = :ip AND v.date > :date')
            ->setParameters([
                'article' => $article,
                'ip' => $ip,
                'date' => date('Y-m-d')
            ])
            ->getQuery()
            ->getResult();
        if(!$isViewedToday){
            $articleViews = new ArticleViews();
            $articleViews->setIp($ip);
            $articleViews->setArticle($article);
            $this->_em->persist($articleViews);
            $this->_em->flush();
        };
        return (bool)$isViewedToday;
    }
}
