<?php

namespace IslamToday\ApplicationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class RadioController extends Controller
{
    public function promoAction()
    {
        //dev
        //$radioProgramSchedule = $this->getDoctrine()->getRepository('SCArticleBundle:Article')->findOneById(31689);
        //$radioBroadcasts = $this->getDoctrine()->getRepository('SCArticleBundle:Article')->findOneById(31690);

        //prod
        $radioProgramSchedule = $this->getDoctrine()->getRepository('SCArticleBundle:Article')->findOneById(32302);
        $radioBroadcasts = $this->getDoctrine()->getRepository('SCArticleBundle:Article')->findOneById(32304);

        $this->container->get('sc_article_item.service')->hydratePreviews([$radioProgramSchedule, $radioBroadcasts]);
        $this->container->get('sc_article_item.service')->hydrateFiles([$radioProgramSchedule, $radioBroadcasts]);

        if (mb_strlen($radioProgramSchedule->getFlatContent()->getFlatContent(), 'utf-8') < 10) {
            $this->container->get('sc_article_item.service')->generateFlatContent($radioProgramSchedule);
        }

        if (mb_strlen($radioBroadcasts->getFlatContent()->getFlatContent(), 'utf-8') < 10) {
            $this->container->get('sc_article_item.service')->generateFlatContent($radioBroadcasts);
        }

        return $this->render('IslamTodayApplicationBundle:Radio:promo.html.twig', ['programSchedule' => $radioProgramSchedule,
            'broadcasts' => $radioBroadcasts]);
    }
}