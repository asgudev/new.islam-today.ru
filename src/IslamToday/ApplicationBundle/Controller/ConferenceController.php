<?php

namespace IslamToday\ApplicationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class ConferenceController extends Controller
{
    public function allAction()
    {
        $category = $this->getDoctrine()->getRepository('SCCategoryBundle:Category')->findOneBy(['path' => 'online']);
        $conferences = $this->getDoctrine()->getRepository('SCArticleBundle:Article')->findBy(['primary_category' => $category, 'isPublished' => 1], ['publishedAt' => 'Desc']);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $conferences,
            $this->get('request')->query->get('page', 1),
            $limit = 40
        );

        $data['pagination'] = $pagination;
        $data['category'] = $category;

        $this->container->get('sc_article_item.service')->hydratePreviews($conferences);

        return $this->render('IslamTodayApplicationBundle:Conference:all.html.twig', $data);
    }

    public function singleAction($title_path)
    {
        $conf = $this->getDoctrine()->getRepository('SCArticleBundle:Article')->findOneBy(['path' => $title_path, 'primary_category' => 56]);

        if (is_null($conf)) {
            throw $this->createNotFoundException('нет такой конфы');
        }

        $this->container->get('sc_article_item.service')->hydrateContent(array($conf));
        $this->container->get('sc_article_item.service')->hydrateFlatContent(array($conf));

        $this->container->get('sc_article_item.service')->hydratePreviews(array($conf));
        $this->container->get('sc_article_item.service')->hydrateFiles(array($conf));

        if (mb_strlen($conf->getFlatContent()->getFlatContent(), 'utf-8') < 10) {
            $this->container->get('sc_article_item.service')->generateFlatContent($conf);
        }

        /*
         * todo: это очень плохая логика
         * надо написать другой сборщик статистики
         * например, на memcached
         */

        $viewCnt = $this->getDoctrine()->getRepository('SCArticleBundle:ArticleStats')->findOneByArticle($conf);
        $viewCnt->setCnt($viewCnt->getCnt() + 1);
        $this->getDoctrine()->getManager()->persist($viewCnt);
        $this->getDoctrine()->getManager()->flush();


        return $this->render('IslamTodayApplicationBundle:Conference:single.html.twig', ['conf' => $conf]);

    }
}