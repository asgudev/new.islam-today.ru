<?php

namespace IslamToday\ApplicationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class NewsController extends BaseController
{
    const SEARCH_EPOCH_START = 631141200; // 1990 год

    public function widgetAction()
    {

    }

    public function allAction(Request $request)
    {
        $sphinx = $this->get('scsphinx.client');
        $indexes = $this->container->getParameter('sc_article.indexes.main') . ' ' . $this->container->getParameter('sc_article.indexes.delta');

        $sphinx->setIndexes($indexes);
        $sphinx->setLimit(1000, 0);
        $sphinx->sortByDesc('published_at');
        $sphinx->setFilter('primary_category_id', array(38));
        $sphinx->setFilter('is_published', array(1));
        $sphinx->SetFilterRange('published_at', self::SEARCH_EPOCH_START, time());

        $sphinx->q('');

        $dql = '';
        if (sizeof($sphinx->getFoundIds()) > 0) {
            $dql = "SELECT a FROM SCArticleBundle:Article a WHERE a.id in(" . implode(',', $sphinx->getFoundIds()) . ") ORDER BY a.publishedAt DESC";
        }

        $articles = $this->getDoctrine()->getManager()->createQuery($dql)->getResult();

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $articles,
            $request->query->get('page', 1),
            $limit = 15
        );

        $this->container->get('sc_article_item.service')->hydratePreviews($articles);

        $data = array('pagination' => $pagination);
        $newsCategory = $this->getDoctrine()->getRepository('SCCategoryBundle:Category')->findOneById(38);
        $data['category'] = $newsCategory;

        return $this->render('@IslamTodayApplication/Articles/category.html.twig', $data);
    }

    public function singleAction(Request $request, $news_path, $year = null, $month = null, $day = null)
    {
        $news = $this->getDoctrine()->getRepository('SCArticleBundle:Article')->findOneBy(['path' => $news_path, 'isPublished' => 1, 'primary_category' => 38]);

        if (is_null($news)) {
            throw $this->createNotFoundException('нет такой новости');
        }

        if (is_null($year) || is_null($month) || is_null($day)) {
            return $this
                ->redirect(
                    $this
                        ->generateUrl('news_single_with_date',
                            array('news_path' => $news->getPath(), 'year' => $news->getPublishedAt()->format('Y'),
                                'month' => $news->getPublishedAt()->format('m'),
                                'day' => $news->getPublishedAt()->format('d'))));
        }


        $response = new Response();
        $response->setExpires(new \DateTime());
        $response->setLastModified($news->getLastModyfiedAt());
        // Set response as public. Otherwise it will be private by default.
        $response->setPublic();

//        if ($response->isNotModified($request)) {
//            return $response;
//        }

        $this->container->get('sc_article_item.service')->hydratePreviews(array($news));
        $this->container->get('sc_article_item.service')->hydrateFiles(array($news));

        if (mb_strlen($news->getFlatContent()->getFlatContent(), 'utf-8') < 10) {
            $this->container->get('sc_article_item.service')->generateFlatContent($news);
        }
        $articleImage = isset($news->getPreviews()[440]) ? $news->getPreviews()[440]->getUrl() : '';
        $articleImageBig = isset($news->getPreviews()[540]) ? $news->getPreviews()[540]->getUrl() : '';
        /*
         * todo: это очень плохая логика
         * надо написать другой сборщик статистики
         * например, на memcached
         */

        $viewCnt = $this->getDoctrine()->getRepository('SCArticleBundle:ArticleStats')->findOneByArticle($news);
        $viewCnt->setCnt($viewCnt->getCnt() + 1);
        $this->getDoctrine()->getManager()->persist($viewCnt);
        $this->getDoctrine()->getManager()->flush();

        $newsCategory = $this->getDoctrine()->getRepository('SCCategoryBundle:Category')->findOneById(38);

        return $this->render('@IslamTodayApplication/Articles/single.html.twig', [
            'article' => $news,
            'category' => $newsCategory,
            'articleImage' => $articleImage,
            'articleImageBig' => $articleImageBig
        ],
            $response);
    }

    public function rssAction()
    {
        $sphinx = $this->get('scsphinx.client');
        $indexes = $this->container->getParameter('sc_article.indexes.main') . ' ' . $this->container->getParameter('sc_article.indexes.delta');

        $sphinx->setIndexes($indexes);
        $sphinx->setLimit(25, 0);
        $sphinx->sortByDesc('published_at');

        $sphinx->setFilter('primary_category_id', [38]);
        $sphinx->setFilter('is_published', array(1));
        $sphinx->SetFilterRange('published_at', self::SEARCH_EPOCH_START, time());

        $sphinx->q('');


        $articles = array();

        if (sizeof($sphinx->getFoundIds()) > 0) {
            $parameters = array('id' => $sphinx->getFoundIds());

            $articles = $this->getDoctrine()
                ->getRepository('SCArticleBundle:Article')
                ->findBy($parameters, ['publishedAt' => 'desc']);
        }

        $this->container->get('sc_article_item.service')->hydratePreviews($articles);

        $url = $this->container->getParameter('sc_article.hostname.url');

        $response = new Response();
        $response->headers->set('Content-Type', 'text/xml');

        return $this->render('IslamTodayApplicationBundle:News:rss.html.twig', ['articles' => $articles, 'url' => $url],
            $response);
    }
}
