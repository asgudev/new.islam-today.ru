<?php

namespace IslamToday\ApplicationBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class WidgetController extends Controller
{
    public function widgetListAction(Request $request)
    {
        $namaz_widget_code =
            "<script>" .
            "var widget_times = {width: __width__,  color1: '__color1__', color2: '__color2__', city: '__city__'};" .
            "document.write('<script type=\"text/javascript\" src=\"https://islam-today.ru/static/javascripts/namaztime.js\"><\/script>');" .
            "</script>";

        $arabname_widget_code =
            "<script>" .
            "var widget_name = { color1: '__color1__'};" .
            "document.write('<script type=\"text/javascript\" src=\"https://islam-today.ru/static/javascripts/arabname.js\"><\/script>');" .
            "</script>";

	$cities = $this->getDoctrine()->getRepository('IslamTodayApplicationBundle:City')->findAll();

        //$namaz_widget_code = 'document.write(\'<script type="text / javascript" src="http://islam-today.ru/static/javascripts/namaztime.js"></script>\')';

        return $this->render('@IslamTodayApplication/Widget/widgetList.html.twig', [
            'namaz_widget' => $namaz_widget_code,
            'names_widget' => $arabname_widget_code,
            'cities' => $cities,
        ]);
    }
}
