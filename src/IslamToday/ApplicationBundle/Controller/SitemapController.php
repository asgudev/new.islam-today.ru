<?php

namespace IslamToday\ApplicationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SitemapController extends BaseController
{
    public function indexAction(Request $request)
    {
        $menuCatsPaths = array('islam_v_mire', 'islam_v_rossii', 'ekonomika', 'obsestvo', 'zhenshhina_v_islame', 'veroucenie', 'istoria', 'sport');
        $categories = $this->getDoctrine()->getRepository('SCCategoryBundle:Category')->findAll();

        $sorted = array();
        foreach ($categories as $cat) {
            if (strlen($cat->getTitle()) == 0 || !in_array($cat->getPath(), $menuCatsPaths)) {
                continue;
            }
            if ($cat->getLevel() == 1) {
                $sorted[$cat->getId()]['parent'] = $cat;
                foreach ($categories as $subCat) {
                    if ($cat->getId() == $subCat->getParentId()) {
                        $sorted[$cat->getId()]['subs'][] = $subCat;
                    }
                }
            }
        }

        $sortFunc = function ($a, $b) {
            if ($a['parent']->getMenuOrder() == $b['parent']->getMenuOrder()) {
                return 0;
            }
            return ($a['parent']->getMenuOrder() < $b['parent']->getMenuOrder()) ? -1 : 1;
        };

        //uasort($sorted, $sortFunc);

        return $this->render('IslamTodayApplicationBundle:Sitemap:sitemap.html.twig', ['map' => $sorted]);
    }
}