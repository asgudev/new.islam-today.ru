<?php

namespace IslamToday\ApplicationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class CommonController extends Controller
{
    public function socialShareAction()
    {
        return $this->render('IslamTodayApplicationBundle:Common:socialShare.html.twig');
    }
}