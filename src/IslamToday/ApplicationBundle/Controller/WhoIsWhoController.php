<?php

namespace IslamToday\ApplicationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class WhoIsWhoController extends Controller
{
    public function allAction()
    {
        $category = $this->getDoctrine()->getRepository('SCCategoryBundle:Category')->findOneById(59);
        $persones = $this->getDoctrine()->getRepository('SCArticleBundle:Article')->findBy(['primary_category' => 59, 'isPublished' => 1], ['publishedAt' => 'Desc']);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $persones,
            $this->get('request')->query->get('page', 1),
            $limit = 40
        );

        $this->container->get('sc_article_item.service')->hydratePreviews($persones);

        $data = array('pagination' => $pagination);
        $data['category'] = $category;

        return $this->render('IslamTodayApplicationBundle:WhoIsWho:all.html.twig', $data);
    }

    public function singleAction($title_path)
    {
        $persona = $this->getDoctrine()->getRepository('SCArticleBundle:Article')->findOneBy(['path' => $title_path, 'isPublished' => 1, 'primary_category' => 59]);

        if (is_null($persona)) {
            throw $this->createNotFoundException('нет такой персоны');
        }

        $this->container->get('sc_article_item.service')->hydratePreviews(array($persona));
        $this->container->get('sc_article_item.service')->hydrateFiles(array($persona));

        if (mb_strlen($persona->getFlatContent()->getFlatContent(), 'utf-8') < 10) {
            $this->container->get('sc_article_item.service')->generateFlatContent($persona);
        }

        /*
         * todo: это очень плохая логика
         * надо написать другой сборщик статистики
         * например, на memcached
         */

        $viewCnt = $this->getDoctrine()->getRepository('SCArticleBundle:ArticleStats')->findOneByArticle($persona);
        $viewCnt->setCnt($viewCnt->getCnt() + 1);
        $this->getDoctrine()->getManager()->persist($viewCnt);
        $this->getDoctrine()->getManager()->flush();

        return $this->render('IslamTodayApplicationBundle:WhoIsWho:single.html.twig', ['persona' => $persona]);
    }
}