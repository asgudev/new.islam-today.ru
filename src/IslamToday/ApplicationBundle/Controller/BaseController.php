<?php

namespace IslamToday\ApplicationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class BaseController extends Controller
{
    public function render($view, array $parameters = array(), Response $response = null)
    {
        $sidebarNews = $this->getDoctrine()->getRepository('SCArticleBundle:Article')->findCatNews(38,20);
        $sidebarCatNews = $this->getDoctrine()->getRepository('SCArticleBundle:Article')->findCatNews(62,20);;

        if (!array_key_exists('sidebarNews', $parameters))
            $parameters['sidebarNews'] = $sidebarNews;

        if (!array_key_exists('sidebarCatNews', $parameters))
            $parameters['sidebarCatNews'] = $sidebarCatNews;

        if (!array_key_exists('nisab', $parameters)) 
           $parameters['nisab'] = $this->get("redis.helper")->get("nisab");

        return parent::render($view, $parameters, $response);
    }
}
