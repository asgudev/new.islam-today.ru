<?php

namespace IslamToday\ApplicationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;

class ServiceController extends BaseController
{
    public function getNamazTimeWidgetAction(Request $request)
    {
        $city = $request->get('city');

        $city = $this->getDoctrine()->getRepository('IslamTodayApplicationBundle:City')->findOneByPath($city);

        if (is_null($city)) {
            $city = $this->getDoctrine()->getRepository('IslamTodayApplicationBundle:City')->findOneByPath('kazan');
        }

        $today = new \DateTime();

        $todayTime = $this->getDoctrine()->getRepository('IslamTodayApplicationBundle:Times')->findOneBy(array('date' => $today, 'city' => $city));

        $tomorrow = clone $today;
        $tomorrow = $tomorrow->modify('+1 day');
        $tomorrowTime = $this->getDoctrine()->getRepository('IslamTodayApplicationBundle:Times')->findOneBy(array('date' => $tomorrow, 'city' => $city));

        $cities = $this->getDoctrine()->getRepository('IslamTodayApplicationBundle:City')->findByNamaz();

        return $this->render('IslamTodayApplicationBundle:Service:namaztime_widget.html.twig', [
            'todayTime' => $todayTime,
            'today' => $today,
            'tomorrowTime' => $tomorrowTime,
            'city' => $city,
            'cities' => $cities,
        ]);
    }

    public function getNamazTimeWidgetNewAction(Request $request)
    {
        $city = $this->get('session')->get('userCity');

        if (is_null($city['id'])) {
            $city = $this->getDoctrine()->getRepository('IslamTodayApplicationBundle:City')->findOneByPath('kazan');
        } else {
            $city = $this->getDoctrine()->getRepository('IslamTodayApplicationBundle:City')->find($city['id']);
        }

        $today = new \DateTime();

        $todayTime = $this->getDoctrine()->getRepository('IslamTodayApplicationBundle:Times')->findOneBy(array('date' => $today, 'city' => $city));

        $tomorrow = clone $today;
        $tomorrow = $tomorrow->modify('+1 day');
        $tomorrowTime = $this->getDoctrine()->getRepository('IslamTodayApplicationBundle:Times')->findOneBy(array('date' => $tomorrow, 'city' => $city));

        $cities = $this->getDoctrine()->getRepository('IslamTodayApplicationBundle:City')->findByNamaz();

        return $this->render('IslamTodayApplicationBundle:Service:namaztime_widget_new.html.twig', [
            'todayTime' => $todayTime,
            'today' => $today,
            'tomorrowTime' => $tomorrowTime,
            'city' => $city,
            'cities' => $cities,
        ]);
    }

    public function getNamazTimeExternalWidgetAction(Request $request)
    {
        $city = $request->get('city');

        $city = $this->getDoctrine()->getRepository('IslamTodayApplicationBundle:City')->findOneByPath($city);

        if (is_null($city)) {
            $city = $this->getDoctrine()->getRepository('IslamTodayApplicationBundle:City')->findOneByPath('kazan');
        }

        $today = new \DateTime();

        $todayTime = $this->getDoctrine()->getRepository('IslamTodayApplicationBundle:Times')->findOneBy(array('date' => $today, 'city' => $city));
        /*
                $todayData['fajr'] = $todayTime->getFajr()->format('U') * 1000;
                $todayData['tulu'] = $todayTime->getTulu()->format('U') * 1000;
                $todayData['zuhr'] = $todayTime->getZuhr()->format('U') * 1000;
                $todayData['asr'] = $todayTime->getAsr()->format('U') * 1000;
                $todayData['maghrib'] = $todayTime->getMaghrib()->format('U') * 1000;
                $todayData['isha'] = $todayTime->getIsha()->format('U') * 1000;*/

        $todayData[] = $todayTime->getFajr()->format('U') * 1000;
        $todayData[] = $todayTime->getTulu()->format('U') * 1000;
        $todayData[] = $todayTime->getZuhr()->format('U') * 1000;
        $todayData[] = $todayTime->getAsr()->format('U') * 1000;
        $todayData[] = $todayTime->getMaghrib()->format('U') * 1000;
        $todayData[] = $todayTime->getIsha()->format('U') * 1000;

        $tomorrow = clone $today;
        $tomorrow = $tomorrow->modify('+1 day');
        $tomorrowTime = $this->getDoctrine()->getRepository('IslamTodayApplicationBundle:Times')->findOneBy(array('date' => $tomorrow, 'city' => $city));

        $tomorrowData[] = $tomorrowTime->getFajr()->format('U') * 1000;
        $tomorrowData[] = $tomorrowTime->getTulu()->format('U') * 1000;
        $tomorrowData[] = $tomorrowTime->getZuhr()->format('U') * 1000;
        $tomorrowData[] = $tomorrowTime->getAsr()->format('U') * 1000;
        $tomorrowData[] = $tomorrowTime->getMaghrib()->format('U') * 1000;
        $tomorrowData[] = $tomorrowTime->getIsha()->format('U') * 1000;

        $cities = $this->getDoctrine()->getRepository('IslamTodayApplicationBundle:City')->findByNamaz();

        return $this->render('IslamTodayApplicationBundle:Service:namaztime_exteranal.html.twig', [
            'todayTime' => $todayTime,
            'today' => $today,
            'tomorrowTime' => $tomorrowTime,
            'city' => $city,
            'cities' => $cities,
            'todayData' => $todayData,
            'tomorrowData' => $tomorrowData,
        ]);
    }

    public function namazPageAction(Request $request)
    {
        $cityPath = $request->get('city');
        $period = $request->get('period');

        if (is_null($cityPath)) {
            $city = 'kazan';
        }

        if ($period == 'week') {
            return $this->redirect($this
                ->generateUrl('namaztime', ['city' => $cityPath]));

        }

        if (is_null($period)) {
            $period = 'week';
        }

        $city = $this->getDoctrine()->getRepository('IslamTodayApplicationBundle:City')->findOneByPath($cityPath);

        if (is_null($city)) {
            $city = $this->getDoctrine()->getRepository('IslamTodayApplicationBundle:City')->findOneByPath('kazan');
        }

        $today = new \DateTime();

        $q = $this->getDoctrine()->getManager()
                    ->createQuery("select t from IslamTodayApplicationBundle:Times t where t.date BETWEEN DATE(NOW()) AND :end and t.city = :city")
                    ->setParameters(array('city' => $city, 'end' => (new \DateTime())->modify("+30 days") ));
        $times = $q->getResult();


        $cities = $this->getDoctrine()->getRepository('IslamTodayApplicationBundle:City')->findByNamaz();

        return $this->render(
            'IslamTodayApplicationBundle:Service:namazs_page.html.twig', [
            'cities' => $cities,
            'city' => $city,
            'times' => $times,
            'period' => $period
        ]);
    }

    public function namazRamadanAction(Request $request)
    {
        $cityPath = $request->get('city');
        $period = $request->get('period');

        if (is_null($cityPath)) {
            $city = 'kazan';
        }

        if ($period == 'week') {
            return $this->redirect($this
                ->generateUrl('namazramadan', ['city' => $cityPath]));

        }

        if (is_null($period)) {
            $period = 'week';
        }

        $city = $this->getDoctrine()->getRepository('IslamTodayApplicationBundle:City')->findOneByPath($cityPath);

        if (is_null($city)) {
            $city = $this->getDoctrine()->getRepository('IslamTodayApplicationBundle:City')->findOneByPath('kazan');
        }
        $today = new \DateTime(date('Y-m-d 0:0:0'));

        $times = $this->getDoctrine()->getRepository('IslamTodayApplicationBundle:RamadanTimes')->findByCity($city);
//        echo count($times);
//        exit;

        $cities = $this->getDoctrine()->getRepository('IslamTodayApplicationBundle:City')->findByRamadan();

        return $this->render('IslamTodayApplicationBundle:Service:ramadannamazs_page.html.twig', [
            'cities' => $cities,
            'city' => $city,
            'times' => $times,
            'period' => $period
        ]);
    }

    public function getArabNameWidgetAction()
    {
        return $this->render('IslamTodayApplicationBundle:Service:arab_name_widget.html.twig');
    }

    public function arabNamePageAction()
    {
        return $this->render('IslamTodayApplicationBundle:Service:arab_name_page.html.twig');
    }

    public function getArabNameExternalAction(Request $request)
    {
        return $this->render('IslamTodayApplicationBundle:Service:arabname_external.html.twig', [

        ]);
    }

    public function getArabNameAction(Request $request)
    {
        $name = $request->get('name');

        if (strlen($name) < 3 || strlen($name) > 30) {
            return new Response();
        }

        $name = trim($name);

        $letters = $this->mbStringToArray($name);
        $totalLetters = count($letters);

        $arabAlpabet = $this->getDoctrine()->getRepository('IslamTodayApplicationBundle:ArabAlphabet')->findBy(['cyrillic' => $letters]);

        $arabName = [];
        foreach ($letters as $inx => $letter) {
            foreach ($arabAlpabet as $arabLetter) {
                if ($letter == ' ') {
                    $arabName[$inx] = $letter;
                }

                if ($inx == 0) {
                    if ($arabLetter->getCyrillic() == $letter) {
                        $arabName[$inx] = $arabLetter->getBeginning();
                    }
                }

                if ($arabLetter->getCyrillic() == $letter) {
                    $arabName[$inx] = $arabLetter->getMiddle();
                }

                if ($inx == ($totalLetters - 1)) {
                    if ($arabLetter->getCyrillic() == $letter) {
                        $arabName[$inx] = $arabLetter->getEnd();
                    }
                }
            }
        }

        return new Response(implode('', $arabName));
    }

    public function namesAction(Request $request)
    {
        $q = $request->get('q');

        $names = [];
        if (!is_null($q)) {
            $query = $this->getDoctrine()->getManager()->createQuery("select t from SCServicesBundle:Name t where t.name LIKE ?1")
                ->setParameter(1, '%' . $q . '%')->setMaxResults(10);
            $names = $query->getResult();
        }

        return $this->render('IslamTodayApplicationBundle:Service:names.html.twig', ['names' => $names, 'q' => $q]);
    }

    public function namesByLettersAction(Request $request)
    {
        $letter = $request->get('letter');

        if (is_null($letter)) {
            return $this
                ->redirect(
                    $this
                        ->generateUrl('names_by_letters',
                            array('letter' => 'А')));
        }

        $names = $this->getDoctrine()->getRepository('SCServicesBundle:Name')->findByLetter(array('letter' => $this->ordutf8($letter)));

        foreach (range(176, 176 + 31) as $i) {
            $letters[$i] = iconv('ISO-8859-5', 'UTF-8', chr($i));
        }

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $names,
            $this->get('request')->query->get('page', 1),
            $limit = 40
        );

        $lastNames15 = [];
        $count = 0;
        foreach ($names as $name) {
            $lastNames15[] = $name->getName();
            if ($count == 15) {
                break;
            }
            $count++;
        }

        $data['last15names'] = implode(',', $lastNames15);
        $data['pagination'] = $pagination;
        $data['letters'] = $letters;
        $data['letter'] = $letter;

        return $this->render('IslamTodayApplicationBundle:Service:names_by_letters.html.twig', $data);
    }

    public function prayersAction(Request $request)
    {
        $prayers = $this->getDoctrine()->getRepository('SCArticleBundle:Article')->findBy(['primary_category' => 84]);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $prayers,
            $request->query->get('page', 1),
            $limit = 40
        );

        $data['pagination'] = $pagination;

        return $this->render('IslamTodayApplicationBundle:Service:prayers.html.twig', $data);
    }

    public function prayersSingleAction($path)
    {
        $prayer = $this->getDoctrine()->getRepository('SCArticleBundle:Article')->findOneBy(['path' => $path, 'isPublished' => 1], ['publishedAt' => 'desc']);

        if (is_null($prayer)) {
            throw $this->createNotFoundException('нет такой молитвы');
        }

        $this->container->get('sc_article_item.service')->hydratePreviews(array($prayer));
        $this->container->get('sc_article_item.service')->hydrateFiles(array($prayer));

        if (mb_strlen($prayer->getFlatContent()->getFlatContent(), 'utf-8') < 10) {
            $this->container->get('sc_article_item.service')->generateFlatContent($prayer);
        }

        $prayers = $this->getDoctrine()->getRepository('SCArticleBundle:Article')->findBy(['primary_category' => 84], ['publishedAt' => 'desc'], 5);

        return $this->render('IslamTodayApplicationBundle:Service:prayers_single.html.twig', ['prayer' => $prayer, 'prayers' => $prayers]);
    }

    public function getPrayersWidgetAction()
    {
        $prayer = $this->getDoctrine()->getRepository('SCArticleBundle:Article')->findOneBy(['primary_category' => 84, 'isPublished' => 1], ['publishedAt' => 'desc']);

        return $this->render('IslamTodayApplicationBundle:Service:prayer_widget.html.twig', ['prayer' => $prayer]);
    }

    protected function uchr($codes)
    {
        if (is_scalar($codes)) $codes = func_get_args();
        $str = '';
        foreach ($codes as $code) $str .= html_entity_decode('&#' . $code . ';', ENT_NOQUOTES, 'UTF-8');
        return $str;
    }

    protected function ordutf8($string, $offset = 0)
    {
        $code = ord(substr($string, $offset, 1));
        if ($code >= 128) { //otherwise 0xxxxxxx
            if ($code < 224) $bytesnumber = 2; //110xxxxx
            else if ($code < 240) $bytesnumber = 3; //1110xxxx
            else if ($code < 248) $bytesnumber = 4; //11110xxx
            $codetemp = $code - 192 - ($bytesnumber > 2 ? 32 : 0) - ($bytesnumber > 3 ? 16 : 0);
            for ($i = 2; $i <= $bytesnumber; $i++) {
                $offset++;
                $code2 = ord(substr($string, $offset, 1)) - 128; //10xxxxxx
                $codetemp = $codetemp * 64 + $code2;
            }
            $code = $codetemp;
        }
        $offset += 1;
        if ($offset >= strlen($string)) $offset = -1;
        return $code;
    }


    private function mbStringToArray($string)
    {
        $strlen = mb_strlen($string);
        $string = mb_strtolower($string, 'UTF-8');
        while ($strlen) {
            $array[] = mb_substr($string, 0, 1, "UTF-8");
            $string = mb_substr($string, 1, $strlen, "UTF-8");
            $strlen = mb_strlen($string);

        }
        return $array;
    }
}
