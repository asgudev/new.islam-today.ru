<?php

namespace IslamToday\ApplicationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class TermsController extends Controller
{
    public function indexAction(Request $request)
    {
        $q = $request->get('q');

        $items = [];
        if (!is_null($q)) {
            $query = $this->getDoctrine()->getManager()->createQuery("select t from SCTerminusBundle:Term t where t.title LIKE ?1")
                ->setParameter(1, '%' . $q . '%')->setMaxResults(10);
            $items = $query->getResult();
        }

        return $this->render('IslamTodayApplicationBundle:Terms:index.html.twig', ['items' => $items, 'q' => $q]);
    }

    public function singleAction($path)
    {
        $item = $this->getDoctrine()->getRepository('SCTerminusBundle:Term')->findOneByPath($path);

        if (is_null($item)) {
            throw $this->createNotFoundException('нет такого термина');
        }

        $othersItems = $this->getDoctrine()->getRepository('SCTerminusBundle:Term')->findBy(['theme' => $item->getTheme()], null, 5);

        foreach ($othersItems as $id => $othersItem) {
            if ($othersItem->getId() == $item->getId()) {
                unset($othersItems[$id]);
                break;
            }
        }

        return $this->render('IslamTodayApplicationBundle:Terms:single.html.twig', ['item' => $item,
            'otherItems' => $othersItems, 'metaDescr' => explode('.', $item->getDescr())[0]]);
    }

    public function byletterAction(Request $request)
    {
        $letter = $request->get('letter');

        if (is_null($letter)) {
            return $this
                ->redirect(
                    $this
                        ->generateUrl('terms_byletter',
                            array('letter' => 'А')));
        }

        $items = $this->getDoctrine()->getRepository('SCTerminusBundle:Term')->findByLetter(array('letter' => $this->ordutf8($letter)), ['title' => 'asc']);

        foreach (range(176, 176 + 31) as $i) {
            $letters[$i] = iconv('ISO-8859-5', 'UTF-8', chr($i));
        }


//        $lastNames15 = [];
//        $count = 0;
//        foreach ($names as $name) {
//            $lastNames15[] = $name->getName();
//            if ($count == 15) {
//                break;
//            }
//            $count++;
//        }

        //$data['last15names'] = implode(',', $lastNames15);
        $data['items'] = $items;
        $data['letters'] = $letters;
        $data['letter'] = $letter;
        $data['firstItem'] = reset($items);
        $data['lastItem'] = end($items);

        return $this->render('IslamTodayApplicationBundle:Terms:byletter.html.twig', $data);
    }

    public function themesAction()
    {
        $themes = $this->getDoctrine()->getRepository('SCTerminusBundle:Theme')->findAll();

        $sorted = array();
        foreach ($themes as $theme) {
            if ($theme->getTitle() == '') {
                continue;
            }
            if ($theme->getLevel() == 1) {
                $sorted[$theme->getId()]['parent'] = $theme;
                foreach ($themes as $subCat) {
                    if ($subCat->getTitle() == '') {
                        continue;
                    }
                    if ($theme->getId() == $subCat->getParentId()) {
                        $sorted[$theme->getId()]['subs'][] = $subCat;
                    }
                }
            }
        }

        return $this->render('IslamTodayApplicationBundle:Terms:themes.html.twig', ['themes' => $sorted]);
    }

    public function themeSingleAction($path)
    {
        $theme = $this->getDoctrine()->getRepository('SCTerminusBundle:Theme')->findOneByPath($path);

        if (is_null($theme)) {
            throw $this->createNotFoundException('нет такой темы термина');
        }

        $items = $this->getDoctrine()->getRepository('SCTerminusBundle:Term')->findBy(['theme' => $theme], ['title' => 'asc']);

        return $this->render('IslamTodayApplicationBundle:Terms:theme_single.html.twig', ['theme' => $theme, 'items' => $items]);
    }

    protected function ordutf8($string, $offset = 0)
    {
        $code = ord(substr($string, $offset, 1));
        if ($code >= 128) { //otherwise 0xxxxxxx
            if ($code < 224) $bytesnumber = 2; //110xxxxx
            else if ($code < 240) $bytesnumber = 3; //1110xxxx
            else if ($code < 248) $bytesnumber = 4; //11110xxx
            $codetemp = $code - 192 - ($bytesnumber > 2 ? 32 : 0) - ($bytesnumber > 3 ? 16 : 0);
            for ($i = 2; $i <= $bytesnumber; $i++) {
                $offset++;
                $code2 = ord(substr($string, $offset, 1)) - 128; //10xxxxxx
                $codetemp = $codetemp * 64 + $code2;
            }
            $code = $codetemp;
        }
        $offset += 1;
        if ($offset >= strlen($string)) $offset = -1;
        return $code;
    }
}