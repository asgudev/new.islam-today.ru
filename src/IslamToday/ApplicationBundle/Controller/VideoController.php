<?php

namespace IslamToday\ApplicationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class VideoController extends Controller
{
    public function allAction()
    {
        $galleries = $this->getDoctrine()->getRepository('SCGalleryBundle:GalleryVideoYoutube')->findBy(['isPublished' => 1], ['lastModyfiedAt' => 'desc']);

        $sections = $this->getDoctrine()->getRepository('SCGalleryBundle:GallerySection')->findAll();
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $galleries,
            $this->get('request')->query->get('page', 1),
            $limit = 40
        );


        $data = array('pagination' => $pagination);
        $data['sections'] = $sections;

        return $this->render('IslamTodayApplicationBundle:Video:all.html.twig', $data);
    }

    public function sectionAction($section_path)
    {
        $section = $this->getDoctrine()->getRepository('SCGalleryBundle:GallerySection')->findOneByPath($section_path);

        if (is_null($section)) {
            throw $this->createNotFoundException('нет такого раздела');
        }

        $galleries = $this->getDoctrine()->getRepository('SCGalleryBundle:GalleryVideoYoutube')->findBy(['isPublished' => 1, 'section' => $section], ['lastModyfiedAt' => 'desc']);

        $sections = $this->getDoctrine()->getRepository('SCGalleryBundle:GallerySection')->findAll();
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $galleries,
            $this->get('request')->query->get('page', 1),
            $limit = 40
        );


        $data = array('pagination' => $pagination);
        $data['sections'] = $sections;
        $data['currentSection'] = $section;

        return $this->render('IslamTodayApplicationBundle:Video:section.html.twig', $data);
    }
}
