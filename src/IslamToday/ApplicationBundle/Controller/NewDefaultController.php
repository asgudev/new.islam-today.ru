<?php

namespace IslamToday\ApplicationBundle\Controller;

use IslamToday\ApplicationBundle\Repositories\CityRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class NewDefaultController extends BaseController
{
    protected static $mainArticles = [];
    const SEARCH_EPOCH_START = 631141200; // 1990 год

    private function findRelatedArticles()
    {
        $sql = "SELECT a.id, s.cnt FROM `sc_article` a
                INNER JOIN sc_article_stats s ON s.article_id = a.id AND s.cnt > 15000
                WHERE a.primary_category_id NOT IN (38, 62, 75, 116, 117, 122) AND a.is_published = 1 
                ";
        $stmt = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();
        $ids = $stmt->fetchAll();
        shuffle($ids);
        $ids = array_slice($ids, 0 ,3);
        $idsStr = '';
        $_ids = [];
        if ($ids) {
            foreach ($ids as $articleId) {
                $idsStr .= ', ' . $articleId['id'];
                $_ids[] = $articleId['id'];
            };
            $idsStr = substr($idsStr, 2);
        };

/*        $dql = "SELECT a FROM SCArticleBundle:Article a JOIN a.viewCnt s " .
//        "WHERE s.cnt > 6000 and a.primary_category not in (38, 62, 84) ".$excludeWhere ." ORDER BY a.publishedAt DESC";
            "WHERE a.id IN ($idsStr)";
        $query = $this->getDoctrine()->getManager()->createQuery($dql);
        $query->setMaxResults(3);*/
        $articles = $this->getDoctrine()->getRepository('SCArticleBundle:Article')->findByIds($_ids);


//        return $query->getResult();
        return $articles;
    }

    public function indexAction(Request $request)
    {
        /*
        $sphinx = $this->get('scsphinx.client');
        $indexes = $this->container->getParameter('sc_article.indexes.main') . ' ' . $this->container->getParameter('sc_article.indexes.delta');

        $sphinx->setIndexes($indexes);
        $sphinx->setLimit(20, 0);
        $sphinx->sortByDesc('published_at');
        $sphinx->setFilter('primary_category_id', array(38));
        $sphinx->setFilter('is_published', array(1));
        $sphinx->SetFilterRange('published_at', self::SEARCH_EPOCH_START, time());

        $sphinx->q('');

        $news = array();
        if (sizeof($sphinx->getFoundIds()) > 0) {
            $parameters = array('id' => $sphinx->getFoundIds());
            $news = $this->getDoctrine()
                ->getRepository('SCArticleBundle:Article')
                ->findBy($parameters, ['publishedAt' => 'desc']);
            $this->container->get('sc_article_item.service')->hydratePreviews($news);
        }

        $new = $news[0];*/

//        $sidebarNews = $this->getDoctrine()->getRepository('SCArticleBundle:Article')->findBy(['primary_category' => 38], ['publishedAt' => "DESC"], 20);

        $sliderNews = $this->getDoctrine()->getRepository('SCArticleBundle:Article')->findSliderArticles(4);
        $this->container->get('sc_article_item.service')->hydratePreviews($sliderNews);

        $recentArticles = $this->getDoctrine()->getRepository('SCArticleBundle:Article')->findRecentArticles(9);
        $this->container->get('sc_article_item.service')->hydratePreviews($recentArticles);

//        $relatedArticles = $this->getDoctrine()->getRepository('SCArticleBundle:Article')->findRelatedArticles(3);
        $relatedArticles = $this->findRelatedArticles();
        $this->container->get('sc_article_item.service')->hydratePreviews($relatedArticles);

        $photo = $this->getDoctrine()->getRepository('SCArticleBundle:Article')->findLastInCategory(99);
        $this->container->get('sc_article_item.service')->hydratePreviews([$photo]);


        if (!is_null($photo)) {
            $localUploadPath = '/files/advgallery/part_' . (floor($photo->getId() / 10000)) . '/' . $photo->getId();
            if ($photo->getFiles()) {
                foreach ($photo->getFiles() as $file) {
                    $file->setLocalUploadPath($localUploadPath);
                }
	    }
        }

        $video = $this->getDoctrine()->getRepository('SCArticleBundle:Article')->findLastInCategory(98);
        $this->container->get('sc_article_item.service')->hydratePreviews([$video]);

        $date = 1590278400;
        $days = ceil(($date - time()) / 86400);
//var_dump($days);
        switch ($days) {
            case in_array($days, [1, 21, 31, 41, 51, 61, 71, 81, 91]):
                $text = 'день';
                break;
            case in_array($days, [2, 3, 4, 22, 23, 24, 32, 33, 34, 42, 43, 44, 52, 53, 54]):
                $text = 'дня';
                break;
            default:
                $text = 'дней';
        }

        return $this->render('IslamTodayApplicationBundle:New/Default:index.html.twig', [
            'sliderNews' => $sliderNews,
            'recentArticles' => $recentArticles,
            'relatedArticles' => $relatedArticles,
            'photo' => $photo,
            'video' => $video,
            'ramadan' => [
                'days' => $days,
                'text' => $text,

            ]
        ]);
    }


//
    public function townSelectAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var CityRepository $cityRepository */
        $cityRepository = $em->getRepository('IslamTodayApplicationBundle:City');

        $cities = $cityRepository->findBy([], ['title' => 'ASC']);
        $userCityArr = $request->getSession()->get('userCity');

        return $this->render('@IslamTodayApplication/New/Default/townSelect.html.twig', [
            'cities' => $cities,
            'userCity' => $userCityArr
        ]);
    }
//
//    public function recentNewsAction()
//    {
//        $sphinx = $this->get('scsphinx.client');
//        $indexes = $this->container->getParameter('sc_article.indexes.main') . ' ' . $this->container->getParameter('sc_article.indexes.delta');
//
//        $sphinx->setIndexes($indexes);
//        $sphinx->setLimit(20, 0);
//        $sphinx->sortByDesc('published_at');
//        $sphinx->setFilter('primary_category_id', array(38));
//        $sphinx->setFilter('is_published', array(1));
//        $sphinx->SetFilterRange('published_at', self::SEARCH_EPOCH_START, time());
//
//        $sphinx->q('');
//
//        $news = array();
//        if (sizeof($sphinx->getFoundIds()) > 0) {
//            $parameters = array('id' => $sphinx->getFoundIds());
//            $news = $this->getDoctrine()
//                ->getRepository('SCArticleBundle:Article')
//                ->findBy($parameters, ['publishedAt' => 'desc']);
//            $this->container->get('sc_article_item.service')->hydratePreviews($news);
//        }
//
//        $new = $news[0];
//
//        return $this->render('IslamTodayApplicationBundle:Default:news.html.twig', ['news' => $news]);
//    }
//
//    public function blogsAction()
//    {
//
//        $sphinx = $this->get('scsphinx.client');
//
//        $indexes = $this->container->getParameter('sc_article.indexes.main') . ' ' . $this->container->getParameter('sc_article.indexes.delta');
//
//        $sphinx->setIndexes($indexes);
//        $sphinx->setLimit(2000, 0);
//
//        //      $sphinx->setFilter('primary_category_id', array(37));
//        $sphinx->setFilter('secondcat', array(65, 73, 63,   120, 123, 124, 126));
//
//        $sphinx->setFilter('is_published', array(1));
//        $sphinx->SetFilterRange('published_at', 1499347380, time());
//
//        $sphinx->q('');
////error_log(implode(",", $sphinx->getFoundIds()));
//        $blogs = array();
//        if (sizeof($sphinx->getFoundIds()) > 0) {
//            $parameters = array('id' => $sphinx->getFoundIds());
//            $blogs = $this->getDoctrine()
//                ->getRepository('SCArticleBundle:Article')
//                ->findBy($parameters, ['publishedAt' => 'desc']);
//        }
//
//
//        // todo: что-то надо делать с запросами, так не пойдет
////        $blogs = $this->getDoctrine()->getRepository('SCArticleBundle:Article')->findBy(['primary_category' => 37, 'isPublished' => 1], ['publishedAt' => 'Desc'], 300);
//
////        $blogs = $this->getDoctrine()->getRepository('SCArticleBundle:Article')->findBySecondaryCategories([65, 73, 63, 90, 100, 108]);
///*        	$em = $this->getDoctrine()->getManager();
//                $blogs = $em->createQuery('SELECT p FROM SCArticleBundle:Category p WHERE p.id IN (:ids)')
//                    ->setParameter('ids', [65, 73, 63, 90, 100])
//                    ->getResult();*/
//
//        $sortedBlogs = [];
//
//        foreach ($blogs as $blog) {
//            if (!isset($blog->getSecondaryCategories()[0])) {
//                continue;
//            }
//            $secondaryCat = $blog->getSecondaryCategories()[0];
//            $secondaryCatId = $secondaryCat->getId();
//
//            // убираем из выборки сергея маркуса
//            if ($secondaryCatId == 83 || $secondaryCatId == 88 || $secondaryCatId == 93 || $secondaryCatId == 64) {
//                continue;
//            }
//            if (!isset($sortedBlogs[$secondaryCatId])) {
//                $sortedBlogs[$secondaryCatId] = $blog;
//                $picUrl = '/files/categories/part_' . (floor($secondaryCatId / 10000)) . '/' . $secondaryCatId . '/' . $secondaryCatId . '.jpg';
//
//                if (file_exists(WEB_DIRECTORY . $picUrl)) {
//                    $secondaryCat->setPicUrl($picUrl);
//                } else {
//                    $secondaryCat->setPicUrl('/static/images/FACE-NOT.png');
//                }
////                    $secondaryCat->setPicUrl($picUrl);
//            } else {
//                continue;
//            }
//
//        }
//
//        $sortFunc = function ($a, $b) {
//            if ($a->getSecondaryCategories()[0]->getMenuOrder() == $b->getSecondaryCategories()[0]->getMenuOrder()) {
//                return 0;
//            }
//            return ($a->getSecondaryCategories()[0]->getMenuOrder() < $b->getSecondaryCategories()[0]->getMenuOrder()) ? -1 : 1;
//        };
//
//        uasort($sortedBlogs, $sortFunc);
//
//        return $this->render('IslamTodayApplicationBundle:Default:blogs.html.twig', ['blogs' => $sortedBlogs]);
//    }
//
//    public function secularNewsAction()
//    {
//        $sphinx = $this->get('scsphinx.client');
//        $indexes = $this->container->getParameter('sc_article.indexes.main') . ' ' . $this->container->getParameter('sc_article.indexes.delta');
//
//        $sphinx->setIndexes($indexes);
//        $sphinx->setLimit(1, 0);
//        $sphinx->sortByDesc('published_at');
//        $sphinx->setFilter('primary_category_id', array(62));
//        $sphinx->setFilter('is_published', array(1));
//        $sphinx->SetFilterRange('published_at', self::SEARCH_EPOCH_START, time());
//
//        $sphinx->q('');
//
//        $news = array();
//        if (sizeof($sphinx->getFoundIds()) > 0) {
//            $parameters = array('id' => $sphinx->getFoundIds());
//            $news = $this->getDoctrine()
//                ->getRepository('SCArticleBundle:Article')
//                ->findBy($parameters, ['publishedAt' => 'desc']);
//            $this->container->get('sc_article_item.service')->hydratePreviews($news);
//        }
//
//        return $this->render('IslamTodayApplicationBundle:Default:secular_news.html.twig', ['news' => $news]);
//    }
//
//    public function categoryNewsAction($category, $limit = 1)
//    {
//        $sphinx = $this->get('scsphinx.client');
//        $indexes = $this->container->getParameter('sc_article.indexes.main') . ' ' . $this->container->getParameter('sc_article.indexes.delta');
//
//        $sphinx->setIndexes($indexes);
//        $sphinx->setLimit($limit, 0);
//        $sphinx->sortByDesc('published_at');
//        $sphinx->setFilter('primary_category_id', array($category));
//        $sphinx->setFilter('is_published', array(1));
//        $sphinx->SetFilterRange('published_at', 1451606400, time());
//
//        $sphinx->q('');
//
//        $news = array();
//        if (sizeof($sphinx->getFoundIds()) > 0) {
//            $parameters = array('id' => $sphinx->getFoundIds());
//            $news = $this->getDoctrine()
//                ->getRepository('SCArticleBundle:Article')
//                ->findBy($parameters, ['publishedAt' => 'desc']);
//            $this->container->get('sc_article_item.service')->hydratePreviews($news);
//        }
//
//        return $this->render('IslamTodayApplicationBundle:Default:category_news.html.twig', ['news' => $news]);
//    }
//
//    public function fetvyAction()
//    {
//        /*
//        $recommendedFetv = $this->getDoctrine()->getRepository('SCArticleBundle:Article')->findOneBy([
//            'primary_category' => 39,
//            'isPublished' => 1,
//            'isRecommeded' => 1
//        ], ['publishedAt' => 'Desc']);
//        */
//
//        $recommendedFetv = $this->getDoctrine()->getRepository('SCArticleBundle:Article')->findRecommendedFetv();
//
//        if ($recommendedFetv == null) {
//            return new Response();
//        }
//
//        $sphinx = $this->get('scsphinx.client');
//
//        $indexes = $this->container->getParameter('sc_article.indexes.main') . ' ' . $this->container->getParameter('sc_article.indexes.delta');
//
//        $sphinx->setIndexes($indexes);
//        $sphinx->setLimit(3, 0);
//        $sphinx->sortByDesc('published_at');
//
//
//        $sphinx->setFilter('primary_category_id', [39]);
//        $sphinx->setFilter('is_published', [1]);
//        $sphinx->setFilter('idt', [$recommendedFetv->getId()], true);
//        $sphinx->setFilterRange('published_at', strtotime('01-01-1970'), strtotime((new \DateTime())->format('d-m-Y')));
//
//        $sphinx->q('');
//
//        $fetvy = array();
//        if (sizeof($sphinx->getFoundIds()) > 0) {
//            $parameters = array('id' => $sphinx->getFoundIds());
//            $fetvy = $this->getDoctrine()
//                ->getRepository('SCArticleBundle:Article')
//                ->findBy($parameters, ['publishedAt' => 'desc']);
//            $this->container->get('sc_article_item.service')->hydratePreviews($fetvy);
//        }
//
//        $this->container->get('sc_article_item.service')->hydratePreviews([$recommendedFetv]);
//
//        return $this->render('IslamTodayApplicationBundle:Default:fetvy.html.twig', ['fetvy' => $fetvy, 'recommendedFetv' => $recommendedFetv]);
//    }
//
//    public function mainArticlesAction()
//    {
//        $sphinx = $this->get('scsphinx.client');
//
//        $indexes = $this->container->getParameter('sc_article.indexes.main') . ' ' . $this->container->getParameter('sc_article.indexes.delta');
//
//        $sphinx->setIndexes($indexes);
//        $sphinx->setLimit(3, 0);
//
//        $articlesIds = [];
//        $sphinx->setSelect("*, super_main = 1 and is_published = 1 as main");
//        $sphinx->SetFilterRange('published_at', self::SEARCH_EPOCH_START, time());
//        $sphinx->sortByDesc('lastmodyfied_at');
//
//        $sphinx->setFilter('main', array(
//            1
//        ));
//
//        $sphinx->q('');
//
//        $articlesIds = $sphinx->getFoundIds();
//
//        $sphinx->setLimit(1, 0);
//
//        $sphinx->setSelect("*, main_left = 1 and is_published = 1 as main");
//        $sphinx->SetFilterRange('published_at', self::SEARCH_EPOCH_START, time());
//        $sphinx->sortByDesc('lastmodyfied_at');
//
//        $sphinx->setFilter('main', array(
//            1
//        ));
//
//        $sphinx->q('');
//        $articlesIds[] = $sphinx->getFoundIds()[0];
//
//        $sphinx->setLimit(1, 0);
//
//        $sphinx->setSelect("*, main_right = 1 and is_published = 1 as main");
//        $sphinx->SetFilterRange('published_at', self::SEARCH_EPOCH_START, time());
//        $sphinx->sortByDesc('lastmodyfied_at');
//
//        $sphinx->setFilter('main', array(
//            1
//        ));
//
//        $sphinx->q('');
//        $articlesIds[] = $sphinx->getFoundIds()[0];
//
//
//        static::$mainArticles = $articlesIds;
//        $articles = array();
//        if (sizeof($articlesIds) > 0) {
//            $articles = $this->getDoctrine()
//                ->getRepository('SCArticleBundle:Article')
//                ->findBy(['id' => $articlesIds], ['lastModyfiedAt' => 'desc']);
//
//            $this->container->get('sc_article_item.service')->hydratePreviews($articles);
//        }
//
//
//        return $this->render('IslamTodayApplicationBundle:Default:main_articles.html.twig', ['articles' => $articles]);
//    }
//
//    public function recentArticlesAction()
//    {
//        $sphinx = $this->get('scsphinx.client');
//        $indexes = $this->container->getParameter('sc_article.indexes.main') . ' ' . $this->container->getParameter('sc_article.indexes.delta');
//
//        $sphinx->setIndexes($indexes);
//        $sphinx->setLimit(11, 0);
//        $sphinx->sortByDesc('published_at');
//
//        $sphinx->setFilter('primary_category_id', array(38, 62, 39, 84, 56, 59, 75, 116, 117, 122), true);
//        $sphinx->setFilter('idt', static::$mainArticles, true);
//        $sphinx->SetFilterRange('published_at', self::SEARCH_EPOCH_START, time());
//
//        $sphinx->setFilter('is_published', array(1));
//
//        $sphinx->q('');
//
//        $articles = array();
//        if (sizeof($sphinx->getFoundIds()) > 0) {
//            $parameters = array('id' => $sphinx->getFoundIds());
//            $articles = $this->getDoctrine()
//                ->getRepository('SCArticleBundle:Article')
//                ->findBy($parameters, ['publishedAt' => 'desc']);
//            $this->container->get('sc_article_item.service')->hydratePreviews($articles);
//        }
//
//        return $this->render('IslamTodayApplicationBundle:Default:recentArticles.html.twig', ['articles' => $articles]);
//    }
//
//    public function onlineAction()
//    {
//        $conference = $this->getDoctrine()->getRepository('SCArticleBundle:Article')->findOneBy(['primary_category' => 56, 'isPublished' => 1, 'isRecommeded' => 1], ['lastModyfiedAt' => 'Desc']);
//
//        if (is_null($conference)) {
//            $conference = $this->getDoctrine()->getRepository('SCArticleBundle:Article')->findOneBy(['primary_category' => 56, 'isPublished' => 1], ['lastModyfiedAt' => 'Desc']);
//        }
//
//        if (!is_null($conference)) {
//            $this->container->get('sc_article_item.service')->hydratePreviews(array($conference));
//        }
//
//        return $this->render('IslamTodayApplicationBundle:Default:online.html.twig', ['conf' => $conference]);
//    }
//
    public function searchAction(Request $request)
    {
        $q = $request->get('q');
        $date = $request->get('date');
        $show = $request->get('show');
        $sort = $request->get('sort');
        $order = $request->get('order') ?: 'asc';

        $sphinx = $this->get('scsphinx.client');

        $indexes = $this->container->getParameter('sc_article.indexes.main') . ' ' . $this->container->getParameter('sc_article.indexes.delta');

        $sphinx->setIndexes($indexes);
        $sphinx->setLimit(3000, 0);


        $data = [];
        switch ($show) {
            case 'news':
                $sphinx->setFilter('primary_category_id', array(38, 62, 117));
//                $sphinx->setFilter('primary_category_id', array(117));
                $data['news'] = true;
                $data['section'] = 'news';
                break;
            case 'articles':
                $sphinx->setFilter('primary_category_id', array(38, 62, 117), true);
//                $sphinx->setFilter('primary_category_id', array(62), true);
//                $sphinx->setFilter('primary_category_id', array(117), true);
                $data['articles'] = true;
                $data['section'] = 'articles';

                break;
            default:
                $sphinx->setFilter('primary_category_id', array(38, 62, 117), true);
//                $sphinx->setFilter('primary_category_id', array(62), true);
//                $sphinx->setFilter('primary_category_id', array(117), true);
//                $data['news'] = true;
                $data['articles'] = true;
                $data['section'] = 'articles';
                break;
        }


        $sphinx->setFilter('is_published', array(1));
        if (!is_null($date)) {
            $startDate = new \DateTime($date . ' 0:00');

            if ($date == date('d.m.Y')) {
                $endDate = new \DateTime($date . date('H:i'));
            } else {
                $endDate = new \DateTime($date . '24:00');
            }

            $sphinx->SetFilterRange('published_at', $startDate->getTimestamp(), $endDate->getTimestamp());
            $sphinx->q('');
        } else {
            $sphinx->SetFilterRange('published_at', self::SEARCH_EPOCH_START, time());
            $sphinx->q($q);
        }
        switch ($sort) {
            case 'date':
                $sort = [
                    'field' => 'a.publishedAt',
                    'order' => $order,
                ];
                break;
            case 'rate':
                $sort = [
                    'field' => 'v.cnt',
                    'order' => $order,
                ];
                break;
            case 'name':
                $sort = [
                    'field' => 'a.title',
                    'order' => $order,
                ];
                break;
            default:
                $sort = [
                    'field' => 'a.publishedAt',
                    'order' => 'desc',
                ];
                break;
        }

        $articles = array();
        if (sizeof($sphinx->getFoundIds()) > 0) {
            $articles = $this->getDoctrine()
                ->getRepository('SCArticleBundle:Article')
                ->findByIds($sphinx->getFoundIds(), $sort);
        }


        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $articles,
            $request->query->get('page', 1),
            $limit = 10
        );


        $this->container->get('sc_article_item.service')->hydratePreviews($articles);
        if ($q == '') $q = $date;
        $data['pagination'] = $pagination;
        $data['q'] = $q;
        $data['a'] = $articles;
        if (empty($articles)) {

            $sphinx->resetFilters();
            $sphinx->setLimit(8, 0);
            $sphinx->sortByDesc('published_at');

            $sphinx->setFilter('primary_category_id', [38, 62, 39, 84, 56, 59], true);
            $sphinx->SetFilterRange('published_at', self::SEARCH_EPOCH_START, time());

            $sphinx->setFilter('is_published', [1]);

            $sphinx->q('');

            $otherArticles = [];
            if (sizeof($sphinx->getFoundIds()) > 0) {
                $parameters = array('id' => $sphinx->getFoundIds());
                $otherArticles = $this->getDoctrine()
                    ->getRepository('SCArticleBundle:Article')
                    ->findBy($parameters, ['publishedAt' => 'desc']);
                $this->container->get('sc_article_item.service')->hydratePreviews($otherArticles);
            }

            $data['otherArticles'] = $otherArticles;
        }

        return $this->render('@IslamTodayApplication/New/Default/search.html.twig', $data);
    }
//
//    public function searchAutocompleterAction(Request $request)
//    {
//        $q = $request->get('q');
//
//        $sphinx = $this->get('scsphinx.client');
//
//        $indexes = $this->container->getParameter('sc_article.indexes.main') . ' ' . $this->container->getParameter('sc_article.indexes.delta');
//
//        $sphinx->setIndexes($indexes);
//        $sphinx->setLimit(5, 0);
//
//
//        $sphinx->setFilter('is_published', [1]);
//        $sphinx->SetFilterRange('published_at', self::SEARCH_EPOCH_START, time());
//        $sphinx->q($q);
//
//
//        $articles = [];
//        if (sizeof($sphinx->getFoundIds()) > 0) {
//            $articles = $this->getDoctrine()
//                ->getRepository('SCArticleBundle:Article')
//                ->findBy(['id' => $sphinx->getFoundIds()], ['publishedAt' => 'desc']);
//        }
//
//
//        $return = [];
//        foreach ($articles as $article) {
//            $return[] = $article->getTitle();
//        }
//
//        return new JsonResponse($return);
//    }
//
//    public function servicesAction()
//    {
//        return $this->render('IslamTodayApplicationBundle:Default:services.html.twig');
//    }
//
//    public function rssAction(Request $request)
//    {
//        $sphinx = $this->get('scsphinx.client');
//        $indexes = $this->container->getParameter('sc_article.indexes.main') . ' ' . $this->container->getParameter('sc_article.indexes.delta');
//
//        $sphinx->setIndexes($indexes);
//        $sphinx->setLimit(25, 0);
//        $sphinx->sortByDesc('published_at');
//
//        $sphinx->setFilter('primary_category_id', array(38, 62, 117), true);
//        $sphinx->setFilter('is_published', array(1));
//        $sphinx->SetFilterRange('published_at', self::SEARCH_EPOCH_START, time());
//
//        $sphinx->q('');
//
//
//        $articles = array();
//
//        if (sizeof($sphinx->getFoundIds()) > 0) {
//            $parameters = array('id' => $sphinx->getFoundIds());
//
//            $articles = $this->getDoctrine()
//                ->getRepository('SCArticleBundle:Article')
//                ->findBy($parameters, ['publishedAt' => 'desc']);
//        }
//
//        $this->container->get('sc_article_item.service')->hydratePreviews($articles);
//
//        $url = $this->container->getParameter('sc_article.hostname.url');
//
//        $response = new Response();
//        $response->headers->set('Content-Type', 'text/xml');
//
//        return $this->render('IslamTodayApplicationBundle:Default:rss.html.twig', ['articles' => $articles, 'url' => $url],
//            $response);
//    }
//
//    public function rssTatarAction(Request $request)
//    {
//        $sphinx = $this->get('scsphinx.client');
//        $indexes = $this->container->getParameter('sc_article.indexes.main') . ' ' . $this->container->getParameter('sc_article.indexes.delta');
//
//        $sphinx->setIndexes($indexes);
//        $sphinx->setLimit(25, 0);
//        $sphinx->sortByDesc('published_at');
//
//        $sphinx->setFilter('primary_category_id', array(117));
//        $sphinx->setFilter('is_published', array(1));
//        $sphinx->SetFilterRange('published_at', self::SEARCH_EPOCH_START, time());
//
//        $sphinx->q('');
//
//
//        $articles = array();
//
//        if (sizeof($sphinx->getFoundIds()) > 0) {
//            $parameters = array('id' => $sphinx->getFoundIds());
//
//            $articles = $this->getDoctrine()
//                ->getRepository('SCArticleBundle:Article')
//                ->findBy($parameters, ['publishedAt' => 'desc']);
//        }
//
//        $this->container->get('sc_article_item.service')->hydratePreviews($articles);
//
//        $url = $this->container->getParameter('sc_article.hostname.url');
//
//        $response = new Response();
//        $response->headers->set('Content-Type', 'text/xml');
//
//        return $this->render('IslamTodayApplicationBundle:Default:rss.html.twig', ['articles' => $articles, 'url' => $url],
//            $response);
//    }
//
//
//    public function rssBotAction(Request $request)
//    {
//
//        $sphinx = $this->get('scsphinx.client');
//        $indexes = $this->container->getParameter('sc_article.indexes.main') . ' ' . $this->container->getParameter('sc_article.indexes.delta');
//
//        $sphinx->setIndexes($indexes);
//        $sphinx->setLimit(25, 0);
//        $sphinx->sortByDesc('published_at');
//
//        $sphinx->setFilter('secondcat', array(17,18,19,65,73,71,90,119,124,22,23,24,21,84,39,27,28,29,31,87,99));
////        $sphinx->setFilter('primary_category_id', array(26));
//        $sphinx->setFilter('primary_category_id', array(62,38), true);
//
//        $sphinx->setFilter('is_published', array(1));
//        $sphinx->SetFilterRange('published_at', self::SEARCH_EPOCH_START, time());
//
//        $sphinx->q('');
//
//        $articles = array();
//
//        if (sizeof($sphinx->getFoundIds()) > 0) {
//            $ids = $sphinx->getFoundIds();
//        }
//
//        $s2 = $this->get('scsphinx.client');
//        $indexes2 = $this->container->getParameter('sc_article.indexes.main') . ' ' . $this->container->getParameter('sc_article.indexes.delta');
//
//        $s2->setIndexes($indexes2);
//        $s2->setLimit(25, 0);
//        $s2->sortByDesc('published_at');
//$s2->resetFilters();
//        $s2->setFilter('secondcat', array(17, 18, 19, 65, 73, 71, 90, 119, 124, 22, 23, 24, 21, 84, 39, 27, 28, 29, 31, 87, 99), true);
//        $s2->setFilter('primary_category_id', array(26));
//
//        $s2->setFilter('is_published', array(1));
//        $s2->SetFilterRange('published_at', self::SEARCH_EPOCH_START, time());
//
//        $s2->q('');
//        $a2 = [];
//        if (sizeof($s2->getFoundIds()) > 0) {
//            $ids2 = $s2->getFoundIds();
//            $a2 = $this->getDoctrine()
//                ->getRepository('SCArticleBundle:Article')
//                ->findPostsToRss($ids);
//        }
//
//        $aids = array_unique(array_merge($ids, $ids2));
//
//        $articles = $this->getDoctrine()
//                ->getRepository('SCArticleBundle:Article')
//                ->findPostsToRss($aids);
//
//
//        $this->container->get('sc_article_item.service')->hydratePreviews($articles);
//
//        $url = $this->container->getParameter('sc_article.hostname.url');
//
//        $response = new Response();
//        $response->headers->set('Content-Type', 'text/xml');
//
//        return $this->render('IslamTodayApplicationBundle:Default:rss.html.twig', ['articles' => $articles, 'url' => $url],
//            $response);
//    }
//
//    public function rssFeedAction(Request $request)
//    {
//        $sphinx = $this->get('scsphinx.client');
//        $indexes = $this->container->getParameter('sc_article.indexes.main') . ' ' . $this->container->getParameter('sc_article.indexes.delta');
//
//        $sphinx->setIndexes($indexes);
//        $sphinx->setLimit(25, 0);
//        $sphinx->sortByDesc('published_at');
//
//        $sphinx->setFilter('primary_category_id', array($request->get('cat')));
//        $sphinx->setFilter('is_published', array(1));
//        $sphinx->SetFilterRange('published_at', self::SEARCH_EPOCH_START, time());
//
//        $sphinx->q('');
//
//        $articles = array();
//
//        if (sizeof($sphinx->getFoundIds()) > 0) {
//            $ids = $sphinx->getFoundIds();
//
//            $articles = $this->getDoctrine()
//                ->getRepository('SCArticleBundle:Article')
//                ->findPostsToRss($ids);
//        }
//
//        $this->container->get('sc_article_item.service')->hydratePreviews($articles);
//
//        $url = $this->container->getParameter('sc_article.hostname.url');
//
//        $response = new Response();
//        $response->headers->set('Content-Type', 'text/xml');
//
//        return $this->render('IslamTodayApplicationBundle:Default:rss.html.twig', ['articles' => $articles, 'url' => $url],
//            $response);
//    }
//
//
//    public function yandexRssAction()
//    {
//        $sphinx = $this->get('scsphinx.client');
//
//        $indexes = $this->container->getParameter('sc_article.indexes.main') . ' ' . $this->container->getParameter('sc_article.indexes.delta');
//
//        $sphinx->setIndexes($indexes);
//        $sphinx->setLimit(300, 0);
//
//        $sphinx->setFilter('is_for_yandex', [1]);
//        $sphinx->setFilter('is_published', array(1));
//
//        $startDate = new \DateTime('NOW' . ' 1:00');
//        $endDate = new \DateTime('NOW');
//        $sphinx->SetFilterRange('published_at', $startDate->getTimestamp(), $endDate->getTimestamp());
//        $sphinx->q('');
//
//        $articles = array();
//        if (sizeof($sphinx->getFoundIds()) > 0) {
//            $parameters = array('id' => $sphinx->getFoundIds());
//
//            $articles = $this->getDoctrine()
//                ->getRepository('SCArticleBundle:Article')
//                ->findBy($parameters, ['publishedAt' => 'asc']);
//        }
//
//        $url = $this->container->getParameter('sc_article.hostname.url');
//
//        $response = new Response();
//        $response->headers->set('Content-Type', 'text/xml');
//
//        return $this->render('IslamTodayApplicationBundle:Default:yandex_rss.html.twig', ['articles' => $articles,
//            'url' => $url],
//            $response);
//    }
//
//    public function whoIsWhoAction()
//    {
//        $persona = $this->getDoctrine()->getRepository('SCArticleBundle:Article')->findOneBy(['primary_category' => 59, 'isPublished' => 1], ['createdAt' => 'Desc']);
//
//        $this->container->get('sc_article_item.service')->hydratePreviews([$persona]);
//        return $this->render('IslamTodayApplicationBundle:Default:whoIsWho.html.twig', ['persona' => $persona]);
//    }
//
//    public function photoAction()
//    {
//        $gallery = $this->getDoctrine()->getRepository('SCAdvancedGalleryBundle:Gallery')->findOneBy(['isPublished' => 1], ['publishedAt' => 'desc']);
//
//        if (!is_null($gallery)) {
//            $localUploadPath = '/files/advgallery/part_' . (floor($gallery->getId() / 10000)) . '/' . $gallery->getId();
//            foreach ($gallery->getFiles() as $file) {
//                $file->setLocalUploadPath($localUploadPath);
//            }
//        }
//
//        return $this->render('IslamTodayApplicationBundle:Default:photo.html.twig', ['gallery' => $gallery]);
//    }
//
//    public function videoAction()
//    {
//        $video = $this->getDoctrine()->getRepository('SCGalleryBundle:GalleryVideoYoutube')->findOneBy(['isPublished' => 1], ['lastModyfiedAt' => 'desc']);
//
//        return $this->render('IslamTodayApplicationBundle:Default:video.html.twig', ['video' => $video]);
//    }
//
//    public function relatedMainAction()
//    {
//        $sql = "SELECT a.id, s.cnt FROM `sc_article` a
//                INNER JOIN sc_article_stats s ON s.article_id = a.id
//                WHERE s.cnt > 15000 AND a.primary_category_id NOT IN (38, 62, 75, 116, 117, 122) AND is_published = 1
//                ORDER BY RAND()
//                LIMIT 6";
//        $stmt = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
//        $stmt->execute();
//        $ids = $stmt->fetchAll();
//        $idsStr = '';
//        if ($ids) {
//            foreach ($ids as $articleId) {
//                $idsStr .= ', ' . $articleId['id'];
//            };
//            $idsStr = substr($idsStr, 2);
//        };
//        $dql = "SELECT a FROM SCArticleBundle:Article a JOIN a.viewCnt s " .
////        "WHERE s.cnt > 6000 and a.primary_category not in (38, 62, 84) ".$excludeWhere ." ORDER BY a.publishedAt DESC";
//            "WHERE a.id IN ($idsStr)";
//        $query = $this->getDoctrine()->getManager()->createQuery($dql);
//        $query->setMaxResults(8);
//
//        $articles = $query->getResult();
//
//        $this->container->get('sc_article_item.service')->hydratePreviews($articles);
//        return $this->render('IslamTodayApplicationBundle:Default:related.html.twig',
//            [
//                'articles' => $articles,
//            ]);
//    }
//
//    public function prMainAction()
//    {
//        $sql = "SELECT a.id, s.cnt FROM `sc_article` a
//                INNER JOIN sc_article_stats s ON s.article_id = a.id
//                WHERE a.primary_category_id = 121 AND is_published = 1
//                ORDER BY a.published_at DESC
//                LIMIT 3";
//        $stmt = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
//        $stmt->execute();
//        $ids = $stmt->fetchAll();
//        $idsStr = '';
//        if ($ids) {
//            foreach ($ids as $articleId) {
//                $idsStr .= ', ' . $articleId['id'];
//            };
//            $idsStr = substr($idsStr, 2);
//        };
//        $dql = "SELECT a FROM SCArticleBundle:Article a JOIN a.viewCnt s " .
////        "WHERE s.cnt > 6000 and a.primary_category not in (38, 62, 84) ".$excludeWhere ." ORDER BY a.publishedAt DESC";
//            "WHERE a.id IN ($idsStr) ORDER BY a.publishedAt DESC";
//        $query = $this->getDoctrine()->getManager()->createQuery($dql);
//        $query->setMaxResults(8);
//
//        $articles = $query->getResult();
//
//        $this->container->get('sc_article_item.service')->hydratePreviews($articles);
//        return $this->render('IslamTodayApplicationBundle:Default:related.html.twig',
//            [
//                'articles' => $articles,
//            ]);
//    }
    /*


            "php": ">=7.2",
            "symfony/symfony": "3.4.*",
            "doctrine/orm": "^2.5",
            "doctrine/doctrine-bundle": "^1.6",
            "doctrine/doctrine-cache-bundle": "^1.2",
            "symfony/swiftmailer-bundle": "^2.3",
            "symfony/monolog-bundle": "^2.8",
            "sensio/distribution-bundle": "^5.0",
            "sensio/framework-extra-bundle": "^3.0.2",
            "symfony/assetic-bundle": "^2.8",
            "leafo/scssphp": "^0.6.6",
            "incenteev/composer-parameter-handler": "^2.0",
            "friendsofsymfony/user-bundle": "2.0.x-dev",
            "jms/translation-bundle": "^1.2",
            "jms/di-extra-bundle": "^1.7",
            "jms/i18n-routing-bundle": "^2.0",
            "beberlei/DoctrineExtensions": "^1.0",
            "stof/doctrine-extensions-bundle": "^1.2",
            "liip/imagine-bundle": "^1.6",
            "patchwork/jsqueeze": "^2.0",
            "chillerlan/php-qrcode": "dev-master",
            "longman/telegram-bot": "^0.55.1"


     */
}
