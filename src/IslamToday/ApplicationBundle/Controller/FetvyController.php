<?php

namespace IslamToday\ApplicationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class FetvyController extends BaseController
{
    public function allAction(Request $request)
    {
        // так как у нас фетвы являются корневой категорией, но в меню располагаются в категории "вероучения"
        // приходиться вот так вот изъебываться
        $fetvyCategory = $this->getDoctrine()->getRepository('SCCategoryBundle:Category')->findOneById(39);

        // вероучения
        $category = $this->getDoctrine()->getRepository('SCCategoryBundle:Category')->findOneById(26);

        $data['sub_category'] = $fetvyCategory;
        $data['category'] = $category;


        $articles = $this->getDoctrine()->getRepository('SCArticleBundle:Article')->findPublishedFetvy($fetvyCategory);


        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $articles,
            $request->query->get('page', 1),
            $limit = 15
        );

        $this->container->get('sc_article_item.service')->hydratePreviews($articles);

        $data['pagination'] = $pagination;

        return $this->render('@IslamTodayApplication/Articles/category.html.twig', $data);
    }

    public function singleAction(Request $request, $title_path)
    {
        $fetva = $this->getDoctrine()->getRepository('SCArticleBundle:Article')->findOneBy(['path' => $title_path, 'isPublished' => 1, 'primary_category' => 39]);

        if (is_null($fetva)) {
            throw $this->createNotFoundException('нет такой фетвы');
        }

        $this->container->get('sc_article_item.service')->hydrateContent(array($fetva));
        $this->container->get('sc_article_item.service')->hydrateFlatContent(array($fetva));

        $this->container->get('sc_article_item.service')->hydratePreviews(array($fetva));
        $this->container->get('sc_article_item.service')->hydrateFiles(array($fetva));

        if (mb_strlen($fetva->getFlatContent()->getFlatContent(), 'utf-8') < 10) {
            /**
             * Метод, который занимается генерацией плоского контента должен жить где-то в рамках текущего проекта видимо
             */
            $this->container->get('sc_article_item.service')->generateFlatContent($fetva);
        }

        // вероучения
        $category = $this->getDoctrine()->getRepository('SCCategoryBundle:Category')->findOneById(26);

        /*
         * todo: это очень плохая логика
         * надо написать другой сборщик статистики
         * например, на memcached
         */

        $isArticleViewed = $this->getDoctrine()->getRepository('IslamTodayApplicationBundle:ArticleViews')
            ->checkViewByIpToday($fetva, $request->getClientIp());
        if (!$isArticleViewed) {
            $viewCnt = $this->getDoctrine()->getRepository('SCArticleBundle:ArticleStats')->findOneByArticle($fetva);
            $viewCnt->setCnt($viewCnt->getCnt() + 1);
            $this->getDoctrine()->getManager()->persist($viewCnt);
            $this->getDoctrine()->getManager()->flush();
        };

        $articleImage = isset($fetva->getPreviews()[440]) ? $fetva->getPreviews()[440]->getUrl() : '';
        $articleImageBig = isset($fetva->getPreviews()[540]) ? $fetva->getPreviews()[540]->getUrl() : '';

        return $this->render('@IslamTodayApplication/Articles/single.html.twig', [
            'article' => $fetva,
            'category' => $category,
            'sub_category' => $fetva->getPrimaryCategory(),
            'articleImage' => $articleImage,
            'articleImageBig' => $articleImageBig
        ]);
    }

    public function askAction(Request $request)
    {
        $name = $request->get('name');
        $email = $request->get('email');
        $message = $request->get('message');

        if (empty($name) || empty($email) || empty($message)) {
            return new JsonResponse(['code' => 'error']);
        }

        // отправляем письмо
        $message = \Swift_Message::newInstance()
            ->setSubject('Новый вопрос по фетве')
            ->setFrom('islamtoday116.ru@gmail.com')
            ->setTo('islamtoday116.ru@gmail.com')
            ->setBody(
                $this->renderView(
                    'IslamTodayApplicationBundle:Fetvy:email.txt.twig',
                    array('name' => $name, 'email' => $email, 'message' => $message)
                )
            );

        $this->get('mailer')->send($message);

        return new JsonResponse(['code' => 'ok']);

    }
}
