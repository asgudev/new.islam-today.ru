<?php

namespace IslamToday\ApplicationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PhotoController extends BaseController
{
    public function allAction(Request $request)
    {
        $galleries = $this->getDoctrine()->getRepository('SCAdvancedGalleryBundle:Gallery')->findBy(['isPublished' => 1], ['publishedAt' => 'desc']);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $galleries,
            $request->query->get('page', 1),
            $limit = 40
        );

        foreach ($galleries as $gallery) {
            $localUploadPath = '/files/advgallery/part_' . (floor($gallery->getId() / 10000)) . '/' . $gallery->getId();
            foreach ($gallery->getFiles() as $file) {
                $file->setLocalUploadPath($localUploadPath);
            }
        }

        $data = array('pagination' => $pagination);

        return $this->render('IslamTodayApplicationBundle:Photo:all.html.twig', $data);
    }

    public function singleAction(Request $request, $title_path)
    {
        $gallery = $this->getDoctrine()->getRepository('SCAdvancedGalleryBundle:Gallery')->findOneByPath($title_path);

        if (is_null($gallery)) {
            throw $this->createNotFoundException('нет такой галереи');
        }

        $localUploadPath = '/files/advgallery/part_' . (floor($gallery->getId() / 10000)) . '/' . $gallery->getId();
        foreach ($gallery->getFiles() as $file) {
            $file->setLocalUploadPath($localUploadPath);
        }

        /*
         * todo: это очень плохая логика
         * надо написать другой сборщик статистики
         * например, на memcached
         */

        $viewCnt = $this->getDoctrine()->getRepository('SCAdvancedGalleryBundle:GalleryStats')->findOneByGallery($gallery);
        $viewCnt->setCnt($viewCnt->getCnt() + 1);
        $this->getDoctrine()->getManager()->persist($viewCnt);
        $this->getDoctrine()->getManager()->flush();

        return $this->render('IslamTodayApplicationBundle:Photo:single.html.twig', ['gallery' => $gallery]);
    }
}