<?php

namespace IslamToday\ApplicationBundle\Controller;

use SC\ArticleBundle\Entity\Article;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


/*
 * TODO: переделать на хелпер к твигу
 */
class MistypeController extends Controller
{
    public function handlerAction(Request $request)
    {

        $data = [
            'ref' => $request->get('ref'),
            'c_pre' => $request->get('c_pre'),
            'c_sel' => $request->get('c_sel'),
            'c_suf' => $request->get('c_suf'),
            'comment' => $request->get('comment'),

        ];

        $message = \Swift_Message::newInstance()
            ->setSubject('Орфографическая ошибка')
            ->setFrom('islamtoday116.ru@gmail.com')
            ->setTo([
//                'iprotoss@bk.ru',
                'islam_today@mail.ru',
            ])
            ->setBody(
                 $this->renderView('IslamTodayApplicationBundle:Default:errorMail.html.twig', [
                    'data' => $data
                 ]),
                 'text/html'
            );


        $this->get('mailer')->send($message);
        
        return new Response();
    }
}
