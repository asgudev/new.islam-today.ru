<?php

namespace IslamToday\ApplicationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class SEOController extends BaseController
{
    public function indexRedirectAction()
    {
        return $this
            ->redirect(
                $this
                    ->generateUrl('islam_today_homepage'));

    }

    public function singleRedirectAction($article_path)
    {
        return $this->forward('IslamTodayApplicationBundle:SEO:single', ['article_path' => $article_path]);
    }

    public function singleAction($article_path)
    {
        $article = $this->getDoctrine()->getRepository('SCArticleBundle:Article')->findOneBy(['path' => $article_path, 'primary_category' => 75]);
        if (is_null($article)) {
            return new Response();
        }

        $this->container->get('sc_article_item.service')->hydrateContent(array($article));
        $this->container->get('sc_article_item.service')->hydrateFlatContent(array($article));
        $this->container->get('sc_article_item.service')->hydratePreviews(array($article));
        $this->container->get('sc_article_item.service')->hydrateFiles(array($article));

        if (mb_strlen($article->getFlatContent()->getFlatContent(), 'utf-8') < 10) {
            /**
             * Метод, который занимается генерацией плоского контента должен жить где-то в рамках текущего проекта видимо
             */
            $this->container->get('sc_article_item.service')->generateFlatContent($article);
        }

        return $this->render('@IslamTodayApplication/Articles/single.html.twig', ['article' => $article]);
    }
}
