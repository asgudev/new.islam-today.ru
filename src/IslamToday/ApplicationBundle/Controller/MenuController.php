<?php

namespace IslamToday\ApplicationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class MenuController extends Controller
{
    public function headerAction($category = null, $sub_category = null)
    {
        //$menuCatsPaths = array('islam_v_mire', 'islam_v_rossii', 'ekonomika', 'obsestvo', 'zhenshhina_v_islame', 'veroucenie', 'istoria', 'sport');
        // Вероучение / Вопрос-ответ / Женщина в Исламе / В мире / Общество / Фото / Видео / История
        $menuCatsPaths = array('veroucenie', 'vopros-otvet', 'zhenshhina_v_islame', 'islam_v_mire', 'obsestvo', 'istoria');
        $menuCategories = $this->getDoctrine()->getRepository('SCCategoryBundle:Category')->findByPath($menuCatsPaths, ['menuOrder' => 'asc']);
        $allCategories = $this->getDoctrine()->getRepository('SCCategoryBundle:Category')->findAll();

        foreach ($allCategories as $__cat) {
            switch ($__cat->getPath()) {
                case 'fetvy':
                    $fetvyCat = $__cat;
                    break;
                case 'islam_v_rossii' :
                    $inRussiaCat = $__cat;
                    break;
                case 'ekonomika' :
                    $economCat = $__cat;
                    break;
                case 'sport' :
                    $sportCat = $__cat;
                    break;
                default:
                    break;
            }
        }

        //$fetvyCat = $this->getDoctrine()->getRepository('SCCategoryBundle:Category')->findOneBy(['path' => 'fetvy']);

        $sorted = array();
        foreach ($menuCategories as $cat) {
            //if (strlen($cat->getTitle()) == 0 || !in_array($cat->getPath(), $menuCatsPaths)) {
            //    continue;
            //}
            if ($cat->getLevel() == 1) {
                $sorted[$cat->getId()]['parent'] = $cat;

                // В России-Экономика-Спорт суем в общество.
                if ($cat->getPath() == 'obsestvo') {
                    $sorted[$cat->getId()]['subs'][] = $inRussiaCat;
                    $sorted[$cat->getId()]['subs'][] = $economCat;
                    $sorted[$cat->getId()]['subs'][] = $sportCat;
                }

                if ($cat->getPath() == 'veroucenie') {
                    $sorted[$cat->getId()]['subs'][] = $fetvyCat;
                }
                foreach ($allCategories as $subCat) {
                    if (strlen($subCat->getTitle()) == 0) {
                        continue;
                    }

                    if ($subCat->getPath() == 'vopros-otvet') {
                        continue;
                    }

                    if ($subCat->getPath() == 'h-festival-musulmanskogo-kino') {
                        continue;
                    }
                    if ($cat->getId() == $subCat->getParentId()) {
                        $sorted[$cat->getId()]['subs'][] = $subCat;
                    }
                }
            } else {
                $sorted[$cat->getId()]['parent'] = $cat;
            }
        }

        $sortFunc = function ($a, $b) {
            if ($a['parent']->getMenuOrder() == $b['parent']->getMenuOrder()) {
                return 0;
            }
            return ($a['parent']->getMenuOrder() < $b['parent']->getMenuOrder()) ? -1 : 1;
        };

        uasort($sorted, $sortFunc);

        if (!is_null($sub_category) && $sub_category->getPath() == 'vopros-otvet') {
            $category = $sub_category;
            $sub_category = null;
        }

        //dump($sorted); die();


        return $this->render('IslamTodayApplicationBundle:Menu:header.html.twig', ['menu' => $sorted, 'category' => $category,
            'sub_category' => $sub_category]);
    }

    public function extraAction()
    {
        $menu = $this->getDoctrine()->getRepository('SCExtraMenuBundle:ExtraMenu')->findBy(['isPublished' => 1], ['updatedAt' => 'desc']);

        return $this->render('IslamTodayApplicationBundle:Menu:extra.html.twig', ['menu' => $menu]);
    }

    public function partnersAction()
    {
        $partners = $this->getDoctrine()->getRepository('SCBannersBundle:Banner')->findPartners();

        return $this->render('IslamTodayApplicationBundle:Menu:partners.html.twig', [
            'partners' => $partners
        ]);

    }

    public function footerAction()
    {
        $menuCatsPaths = array('islam_v_mire', 'islam_v_rossii', 'ekonomika', 'obsestvo', 'zhenshhina_v_islame', 'veroucenie', 'istoria', 'sport',);
        $ignore = ['lzehadisy', 'h-festival-musulmanskogo-kino'];

        $categories = $this->getDoctrine()->getRepository('SCCategoryBundle:Category')->findAll();

        $sorted = array();
        foreach ($categories as $cat) {
            if (strlen($cat->getTitle()) == 0 || !in_array($cat->getPath(), $menuCatsPaths)) {
                continue;
            }
            if ($cat->getLevel() == 1) {
                $sorted[$cat->getId()]['parent'] = $cat;
                foreach ($categories as $subCat) {
                    //                   if (in_array($subcat->getPath(), $subcatIgonre)) continue;
                    if ($subCat != null && in_array($subCat->getPath(), $ignore)) continue;
                    if ($cat->getId() == $subCat->getParentId()) {
                        $sorted[$cat->getId()]['subs'][] = $subCat;
                    }
                }
            }
        }

        return $this->render('IslamTodayApplicationBundle:Menu:footer.html.twig', ['map' => $sorted]);
    }

    public function seoLinksAction()
    {

        return $this->render('IslamTodayApplicationBundle:Menu:seo_links.html.twig');
    }
}
