<?php

namespace IslamToday\ApplicationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class BannersController extends Controller
{
    public function mainTopAction()
    {
        return $this->render('IslamTodayApplicationBundle:Banners:mainTop.html.twig');
    }

    public function right1Action()
    {
        return $this->render('IslamTodayApplicationBundle:Banners:right1.html.twig', ['mainPage' => $_SERVER['REQUEST_URI'] == '/']);
    }

    public function right2Action()
    {
        return $this->render('IslamTodayApplicationBundle:Banners:right2.html.twig');
    }

    public function right3Action()
    {
        return $this->render('IslamTodayApplicationBundle:Banners:right3.html.twig');
    }

    public function right6Action()
    {
        return $this->render('IslamTodayApplicationBundle:Banners:right6.html.twig');
    }

    public function testAction()
    {

        $databaseFile = $this->get('kernel')->getRootDir() . '/../GeoIP2-City.mmdb';
        if (is_file($databaseFile)) {
            $reader = new \MaxMind\Db\Reader($databaseFile);

            $userIp = $this->getRequest()->getClientIp();
if ($this->getRequest()->get('ip')) $userIp = $this->getRequest()->get('ip');
var_dump($userIp);
            $userIpInfo = $reader->get($userIp);
            var_dump($userIpInfo['city']['names']);
            die();
        };
        return $this->render('@IslamTodayApplication/Banners/test.html.twig');
    }

    public function holidayAction($date = 1526539987, $image = 'ramadan.png')
    {

        $days = ceil(($date - time()) / 86400);

        switch ($days) {
            case in_array($days, [1, 21, 31, 41, 51, 61, 71, 81, 91]):
                $text = 'день';
                break;
            case in_array($days, [2, 3, 4, 22, 23, 24, 32, 33, 34, 42, 43, 44, 52, 53, 54]):
                $text = 'дня';
                break;
            default:
                $text = 'дней';
        }

        return $this->render('IslamTodayApplicationBundle:Banners:holiday.html.twig', [
            'days' => $days,
            'text' => $text,
            'image' => $image,
        ]);
    }


}
