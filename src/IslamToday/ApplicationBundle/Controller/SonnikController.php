<?php

namespace IslamToday\ApplicationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class SonnikController extends Controller
{
    public function indexAction(Request $request)
    {
        $q = $request->get('q');

        $dreams = [];
        if (!is_null($q)) {
            $query = $this->getDoctrine()->getManager()->createQuery("select t from SCSonnikBundle:Dream t where t.title LIKE ?1")
                ->setParameter(1, '%' . $q . '%')->setMaxResults(10);
            $dreams = $query->getResult();
        }

        return $this->render('IslamTodayApplicationBundle:Sonnik:index.html.twig', ['dreams' => $dreams, 'q' => $q]);
    }

    public function singleAction($path)
    {
        $dream = $this->getDoctrine()->getRepository('SCSonnikBundle:Dream')->findOneByPath($path);

        if (is_null($dream)) {
            throw $this->createNotFoundException('нет такого сна');
        }

        $othersDreams = $this->getDoctrine()->getRepository('SCSonnikBundle:Dream')->findBy(['theme' => $dream->getTheme()], null, 5);

        foreach ($othersDreams as $id => $othersDream) {
            if ($othersDream->getId() == $dream->getId()) {
                unset($othersDreams[$id]);
                break;
            }
        }


        return $this->render('IslamTodayApplicationBundle:Sonnik:single.html.twig', ['dream' => $dream,
            'otherDreams' => $othersDreams]);
    }

    public function byletterAction(Request $request)
    {
        $letter = $request->get('letter');

        if (is_null($letter)) {
            return $this
                ->redirect(
                    $this
                        ->generateUrl('sonnik_byletter',
                            array('letter' => 'А')));
        }

        $dreams = $this->getDoctrine()->getRepository('SCSonnikBundle:Dream')->findByLetter(array('letter' => $this->ordutf8($letter)), ['title' => 'asc']);

        foreach (range(176, 176 + 31) as $i) {
            $letters[$i] = iconv('ISO-8859-5', 'UTF-8', chr($i));
        }


//        $lastNames15 = [];
//        $count = 0;
//        foreach ($names as $name) {
//            $lastNames15[] = $name->getName();
//            if ($count == 15) {
//                break;
//            }
//            $count++;
//        }

        //$data['last15names'] = implode(',', $lastNames15);
        $data['dreams'] = $dreams;
        $data['letters'] = $letters;
        $data['letter'] = $letter;
        $data['firstDream'] = reset($dreams);
        $data['lastDream'] = end($dreams);

        return $this->render('IslamTodayApplicationBundle:Sonnik:byletter.html.twig', $data);
    }

    public function themesAction()
    {
        $themes = $this->getDoctrine()->getRepository('SCSonnikBundle:Theme')->findAll();

        $sorted = array();
        foreach ($themes as $theme) {
            if ($theme->getTitle() == '') {
                continue;
            }
            if ($theme->getLevel() == 1) {
                $sorted[$theme->getId()]['parent'] = $theme;
                foreach ($themes as $subCat) {
                    if ($subCat->getTitle() == '') {
                        continue;
                    }
                    if ($theme->getId() == $subCat->getParentId()) {
                        $sorted[$theme->getId()]['subs'][] = $subCat;
                    }
                }
            }
        }

        return $this->render('IslamTodayApplicationBundle:Sonnik:themes.html.twig', ['themes' => $sorted]);
    }

    public function themeSingleAction($path)
    {
        $theme = $this->getDoctrine()->getRepository('SCSonnikBundle:Theme')->findOneByPath($path);

        if (is_null($theme)) {
            throw $this->createNotFoundException('нет такой темы сна');
        }

        $dreams = $this->getDoctrine()->getRepository('SCSonnikBundle:Dream')->findBy(['theme' => $theme], ['title' => 'asc']);

        return $this->render('IslamTodayApplicationBundle:Sonnik:theme_single.html.twig', ['theme' => $theme, 'dreams' => $dreams]);
    }

    protected function ordutf8($string, $offset = 0)
    {
        $code = ord(substr($string, $offset, 1));
        if ($code >= 128) { //otherwise 0xxxxxxx
            if ($code < 224) $bytesnumber = 2; //110xxxxx
            else if ($code < 240) $bytesnumber = 3; //1110xxxx
            else if ($code < 248) $bytesnumber = 4; //11110xxx
            $codetemp = $code - 192 - ($bytesnumber > 2 ? 32 : 0) - ($bytesnumber > 3 ? 16 : 0);
            for ($i = 2; $i <= $bytesnumber; $i++) {
                $offset++;
                $code2 = ord(substr($string, $offset, 1)) - 128; //10xxxxxx
                $codetemp = $codetemp * 64 + $code2;
            }
            $code = $codetemp;
        }
        $offset += 1;
        if ($offset >= strlen($string)) $offset = -1;
        return $code;
    }
}