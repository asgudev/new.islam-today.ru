<?php

namespace IslamToday\ApplicationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class BlogsController extends BaseController
{
    const SEARCH_EPOCH_START = 631141200; // 1990 год

    private function getSlide($data)
    {
        preg_match_all("#(.+)/\d+-INNERRESIZED\d+-\d+-(.*)#",$data['src'],  $matches);
//      var_dump($matches);
        return '<li data-thumb="'.($matches[1][0]."/".$matches[2][0]).'"><img src="'.$data['src'].'" /></li>';
/*        return '
            <div class="main__item" style="background-image: url(' . $data['src'] . ')">
                <div class="container">
                    <div class="main__item__inner"></div>
                </div>
                <div class="main__item__overlay"></div>
            </div>
        ';*/
    }

    public function allAction(Request $request)
    {
        $blogsCategory = $this->getDoctrine()->getRepository('SCCategoryBundle:Category')->findBy(['path' => 'blogi']);
        $blogs = $this->getDoctrine()->getRepository('SCArticleBundle:Article')->findBy(['primary_category' => $blogsCategory, 'isPublished' => 1], ['publishedAt' => 'Desc']);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $blogs,
            $request->query->get('page', 1),
            $limit = 15
        );

        $this->container->get('sc_article_item.service')->hydratePreviews($blogs);

        foreach ($blogs as $blog) {
            $picUrl = '/files/categories/part_' . (floor($blog->getSecondaryCategories()[0]->getId() / 10000)) . '/' . $blog->getSecondaryCategories()[0]->getId() . '/' . $blog->getSecondaryCategories()[0]->getId() . '.jpg';

            if (file_exists(WEB_DIRECTORY . $picUrl)) {
                $blog->getSecondaryCategories()[0]->setPicUrl($picUrl);
            } else {
                $blog->getSecondaryCategories()[0]->setPicUrl('/static/images/FACE-NOT.png');
            }
        }

        $data = array('pagination' => $pagination);

        return $this->render('IslamTodayApplicationBundle:Blogs:all_blogs.html.twig', $data);
    }

    public function allAuthorsAction()
    {
        $authors = $this->getDoctrine()->getRepository('SCCategoryBundle:Category')->findById([65, 73, 63, 64, 83,  100, 109,  119, 120, 123, 124, 125, 126 ]);

        $parentCat = $this->getDoctrine()->getRepository('SCCategoryBundle:Category')->findOneById(37);
        foreach ($authors as $author) {
            $picUrl = '/files/categories/part_' . (floor($author->getId() / 10000)) . '/' . $author->getId() . '/' . $author->getId() . '.jpg';
            if (file_exists(WEB_DIRECTORY . $picUrl)) {
                $author->setPicUrl($picUrl);
            } else {
                $author->setPicUrl('/static/images/FACE-NOT.png');
            }
        }

        $sortFunc = function ($a, $b) {
            if ($a->getMenuOrder() == $b->getMenuOrder()) {
                return 0;
            }
            return ($a->getMenuOrder() < $b->getMenuOrder()) ? -1 : 1;
        };

        uasort($authors, $sortFunc);

        return $this->render('IslamTodayApplicationBundle:Blogs:all_authors.html.twig', ['authors' => $authors, 'category' => $parentCat]);
    }

    public function singleAction(Request $request, $author_path, $title_path)
    {
        $blog = $this->getDoctrine()->getRepository('SCArticleBundle:Article')->findOneBy(['path' => $title_path, 'isPublished' => 1, 'primary_category' => 37]);

        if (is_null($blog)) {
            throw $this->createNotFoundException('нет такого блога');
        }

        $response = new Response();
        $response->setExpires(new \DateTime());
        $response->setLastModified($blog->getLastModyfiedAt());
        // Set response as public. Otherwise it will be private by default.
        $response->setPublic();

//        if ($response->isNotModified($request)) {
//            return $response;
//        }

        $this->container->get('sc_article_item.service')->hydratePreviews(array($blog));
        $this->container->get('sc_article_item.service')->hydrateFiles(array($blog));

        if (mb_strlen($blog->getFlatContent()->getFlatContent(), 'utf-8') < 10) {
            $this->container->get('sc_article_item.service')->generateFlatContent($blog);
        }




        /*
         * todo: это очень плохая логика
         * надо написать другой сборщик статистики
         * например, на memcached
         */

        $isArticleViewed = $this->getDoctrine()->getRepository('IslamTodayApplicationBundle:ArticleViews')
            ->checkViewByIpToday($blog, $request->getClientIp());
        if (!$isArticleViewed) {
            $viewCnt = $this->getDoctrine()->getRepository('SCArticleBundle:ArticleStats')->findOneByArticle($blog);
            $viewCnt->setCnt($viewCnt->getCnt() + 1);
            $this->getDoctrine()->getManager()->persist($viewCnt);
            $this->getDoctrine()->getManager()->flush();
        };

        $picUrl = '/files/categories/part_' . (floor($blog->getSecondaryCategories()[0]->getId() / 10000)) . '/' . $blog->getSecondaryCategories()[0]->getId() . '/' . $blog->getSecondaryCategories()[0]->getId() . '.jpg';

        $articleImage = isset($blog->getPreviews()[440]) ? $blog->getPreviews()[440]->getUrl() : '';

        if (file_exists(WEB_DIRECTORY . $picUrl)) {
            $blog->getSecondaryCategories()[0]->setPicUrl($picUrl);
        } else {
            $blog->getSecondaryCategories()[0]->setPicUrl('/static/images/FACE-NOT.png');
        }


        $__content = explode("\n", $blog->getFlatContent()->getFlatContent());
        $__sliderStartIndex = 0;
        $__contentWithSlider = [];
	$__slides = [];
        foreach ($__content as $__rn => $__row) {
            $_r = $__content[$__rn];

            if (preg_match("#\[slider\]#", $_r, $matches)) {
                $__sliderStartIndex = $__rn;
                $__contentWithSlider[] = "<div class=\"slider\"><ul class='slides'>";
                continue;
            }
            if ($__sliderStartIndex > 0) {
                if (preg_match_all("#<img.+title=\"(.*)\".+src=\"(.*)\"#U", $_r, $matches)) {
                    $__contentWithSlider[] = $this->getSlide([
                        'src' => $matches[2][0],
                        'title' => $matches[1][0],
                    ]);
                    $__slides[] = $this->getSlide([
                        'src' => $matches[2][0],
                        'title' => $matches[1][0],
                    ]);

                    continue;
                }
                if (preg_match("#.+height.+#", $_r)) {
                    continue;
                }
                if (preg_match("#\[/slider\]#", $_r, $matches)) {
                    $__contentWithSlider[] = "</ul></div>";
                    $__contentWithSlider[] = "<div class='slider-nav'><ul class='slides'>";
		    foreach ($__slides as $slide) {
                        $__contentWithSlider[] = $slide;
                    }
                    $__contentWithSlider[] = "</ul></div>";

                    $__sliderStartIndex = 0;
                }
                continue;
            }

            $__contentWithSlider[] = $__row;
        }
        $blog->getFlatContent()->setFlatContent(implode("\n", $__contentWithSlider));




        return $this->render('@IslamTodayApplication/Articles/single.html.twig', [
            'article' => $blog,
            'articleImage' => $articleImage
        ], $response);
    }

    public function authorAction(Request $request, $author_path)
    {
        $authorCat = $this->getDoctrine()->getRepository('SCCategoryBundle:Category')->findOneBy(['path' => $author_path]);

        if (is_null($authorCat)) {
            throw $this->createNotFoundException('нет такого блога');
        }

        $blogs = $this->getDoctrine()->getRepository('SCArticleBundle:Article')->findBySecondaryCategories(array($authorCat));

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $blogs,
            $request->query->get('page', 1),
            $limit = 15
        );

        $this->container->get('sc_article_item.service')->hydratePreviews($blogs);

        $data = array('pagination' => $pagination);
        $data['category'] = $authorCat;

        $picUrl = '/files/categories/part_' . (floor($authorCat->getId() / 10000)) . '/' . $authorCat->getId() . '/' . $authorCat->getId() . '.jpg';
        if (file_exists(WEB_DIRECTORY . $picUrl)) {
            $authorCat->setPicUrl($picUrl);
        } else {
            $authorCat->setPicUrl('/static/images/FACE-NOT.png');
        }

        return $this->render('@IslamTodayApplication/Articles/category.html.twig', $data);
    }
}
