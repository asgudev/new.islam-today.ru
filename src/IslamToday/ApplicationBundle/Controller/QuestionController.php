<?php

namespace IslamToday\ApplicationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class QuestionController extends BaseController
{
    const SEARCH_EPOCH_START = 631141200; // 1990 год

    public function allAction(Request $request)
    {
        $questionCategory = $this->getDoctrine()->getRepository('SCCategoryBundle:Category')->findOneById(87);
        $data['category'] = $questionCategory;
        $data['sub_category'] = null;

        $sphinx = $this->get('scsphinx.client');
        $indexes = $this->container->getParameter('sc_article.indexes.main') . ' ' . $this->container->getParameter('sc_article.indexes.delta');

        $sphinx->setIndexes($indexes);
        $sphinx->setFilter('secondcat', [$questionCategory->getId()]);
        $sphinx->setFilter('is_published', array(1));
        $sphinx->sortByDesc('published_at');
        $sphinx->SetFilterRange('published_at', self::SEARCH_EPOCH_START, time());
        $sphinx->setLimit(1000, 0);
        $sphinx->q('');

        $articles = [];
        if (sizeof($sphinx->getFoundIds()) > 0) {
            $parameters = array('id' => $sphinx->getFoundIds());

            $articles = $this->getDoctrine()
                ->getRepository('SCArticleBundle:Article')
                ->findBy($parameters, ['publishedAt' => 'desc']);
            $this->container->get('sc_article_item.service')->hydratePreviews($articles);
        }

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $articles,
            $request->query->get('page', 1),
            $limit = 15
        );

        $data['pagination'] = $pagination;

//        return $this->render('IslamTodayApplicationBundle:Question:question_list.html.twig', $data);
        return $this->render('IslamTodayApplicationBundle:Articles:category.html.twig', $data);
    }

    public function singleAction($title_path)
    {
        $article = $this->getDoctrine()->getRepository('SCArticleBundle:Article')->findOneBy(['path' => $title_path, 'isPublished' => 1]);

        if (is_null($article)) {
            throw $this->createNotFoundException('нет такой статьи');
        }

        /*        if ($article->getSecondaryCategories()[0]->getId() != 87) {
                    throw $this->createNotFoundException('нет такой статьи');
                }*/

        $this->container->get('sc_article_item.service')->hydratePreviews(array($article));
        $this->container->get('sc_article_item.service')->hydrateFiles(array($article));

        if (mb_strlen($article->getFlatContent()->getFlatContent(), 'utf-8') < 10) {
            $this->container->get('sc_article_item.service')->generateFlatContent($article);
        }

        // вопрос ответ
        $category = $this->getDoctrine()->getRepository('SCCategoryBundle:Category')->findOneById(87);

        /*
         * todo: это очень плохая логика
         * надо написать другой сборщик статистики
         * например, на memcached
         */

        $viewCnt = $this->getDoctrine()->getRepository('SCArticleBundle:ArticleStats')->findOneByArticle($article);
        $viewCnt->setCnt($viewCnt->getCnt() + 1);
        $this->getDoctrine()->getManager()->persist($viewCnt);
        $this->getDoctrine()->getManager()->flush();

        $articleImage = isset($article->getPreviews()[440]) ? $article->getPreviews()[440]->getUrl() : '';
        $articleImageBig = isset($article->getPreviews()[540]) ? $article->getPreviews()[540]->getUrl() : '';
        return $this->render('@IslamTodayApplication/Articles/single.html.twig', [
            'article' => $article,
            'category' => $category,
            'sub_category' => null,
            'articleImage' => $articleImage,
            'articleImageBig' => $articleImageBig
        ]);
    }

    public function askAction(Request $request)
    {
        $name = $request->get('name');
        $email = $request->get('email');
        $message = $request->get('message');

        if (empty($name) || empty($email) || empty($message)) {
            return new JsonResponse(['code' => 'error']);
        }

        // отправляем письмо
        $message = \Swift_Message::newInstance()
            ->setSubject('Новый вопрос в рубрику вопрос-ответ')
            ->setFrom('islamtoday116.ru@gmail.com')
            ->setTo('islamtoday116.ru@gmail.com')
            ->setBody(
                $this->renderView(
                    'IslamTodayApplicationBundle:Question:email.txt.twig',
                    array('name' => $name, 'email' => $email, 'message' => $message)
                )
            );

        $this->get('mailer')->send($message);

        return new JsonResponse(['code' => 'ok']);

    }
}
