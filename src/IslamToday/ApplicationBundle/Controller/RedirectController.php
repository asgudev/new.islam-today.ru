<?php

namespace IslamToday\ApplicationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class RedirectController extends Controller
{
    public function redirectOuterAction($target)
    {
	switch($target) {
           case "twitter":
               return $this->redirect("https://twitter.com/huzurtvvideo");
               break;
           case "vk":
               return $this->redirect("https://vk.com/huzurtv");
               break;
           case "facebook":
               return $this->redirect("https://www.facebook.com/huzurtv.huzur");
               break;
           case "instagram":
               return $this->redirect("https://www.instagram.com/huzurtvhuzur/");
               break;
           default:
               return $this->redirect("/");
               break;
        }
        var_dump($target);die();
    }
    
    public function redirectAction($new_url = '', $article_path = '')
    {
        if (is_null($new_url) || $new_url == '') {
            return $this
                ->redirect('/', 301);
        }

        if (!is_null($article_path) && $article_path != '') {
            return $this->redirect($new_url . $article_path, 301);
        }

        return $this
            ->redirect($new_url, 301);
    }

    public function redirectRadioAction()
    {
        return $this
            ->redirect('http://radioazan.ru', 301);
    }

    public function prayersAction($article_path)
    {
        return $this->redirect($this->generateUrl('prayers_single', ['path' => $article_path]));
    }

    public function oldArtIdAction()
    {
        $oldArtId = (int)$this->getRequest()->get('art_id');

        $article = $this->getDoctrine()->getRepository('SCArticleBundle:Article')->findOneByOldArticleId($oldArtId);

        if (is_null($article)) {
            return $this
                ->redirect('/', 301);
        }

        if (!is_null($article->getSecondaryCategories()[0])) {
            $url = $this
                ->generateUrl('islam_today_sub_category_article',
                    array('category_path' => $article->getPrimaryCategory()->getPath(),
                        'sub_category_path' => $article->getSecondaryCategories()[0]->getPath(),
                        'article_path' => $article->getPath()));

        } else {
            $url = $this
                ->generateUrl('islam_today_category_article',
                    array('category_path' => $article->getPrimaryCategory()->getPath(),
                        'article_path' => $article->getPath()));
        }

        return $this
            ->redirect($url, 301);
    }

}
