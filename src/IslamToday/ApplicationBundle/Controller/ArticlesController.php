<?php

namespace IslamToday\ApplicationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ArticlesController extends BaseController
{
    const SEARCH_EPOCH_START = 631141200; // 1990 год

    public function inCategoryAction(Request $request, $category_path, $sub_category_path = null)
    {
        $data = [];

        $sphinx = $this->get('scsphinx.client');
        $indexes = $this->container->getParameter('sc_article.indexes.main') . ' ' . $this->container->getParameter('sc_article.indexes.delta');

        $sphinx->setIndexes($indexes);

        $articles = array();

        if (!is_null($sub_category_path)) {
            $subCategory = $this->getDoctrine()->getRepository('SCCategoryBundle:Category')->findOneBy(['path' => $sub_category_path]);

            if (is_null($subCategory)) {
                throw $this->createNotFoundException('нет такого раздела');
            }

            $category = $this->getDoctrine()->getRepository('SCCategoryBundle:Category')->findOneById($subCategory->getParentId());
            $data['sub_category'] = $subCategory;
            $data['category'] = $category;

            $sphinx->setFilter('secondcat', array($subCategory->getId()));
            $sphinx->setFilter('is_published', array(1));
            $sphinx->sortByDesc('published_at');
            $sphinx->SetFilterRange('published_at', self::SEARCH_EPOCH_START, time());
            $sphinx->setLimit(1000, 0);
            $sphinx->q('');

            if (sizeof($sphinx->getFoundIds()) > 0) {
                $parameters = array('id' => $sphinx->getFoundIds());

                $articles = $this->getDoctrine()
                    ->getRepository('SCArticleBundle:Article')
                    ->findBy($parameters, ['publishedAt' => 'desc']);
                $this->container->get('sc_article_item.service')->hydratePreviews($articles);
            }
        } else {
            $category = $this->getDoctrine()->getRepository('SCCategoryBundle:Category')->findOneBy(['path' => $category_path]);

            if (is_null($category)) {
                //seo
                $seoContent = $this->forward('IslamTodayApplicationBundle:SEO:single', ['article_path' => $category_path]);

                if (strlen($seoContent->getContent()) > 0) {
                    return $seoContent;
                }
                //пытаемся найти тэг
                return $this->forward('IslamTodayApplicationBundle:Articles:tags', ['tag_path' => $category_path, 'page' => $request->query->get('page', 1)]);
            }

            $data['category'] = $category;
            $data['sub_category'] = null;



            $sphinx->setFilter('primary_category_id', array($category->getId()));
            $sphinx->setFilter('is_published', array(1));
            
            $n = floor($request->get('page') / 25);
            $sphinx->setLimit(1000 + (1000 * $n), (1000 * $n));
            $sphinx->sortByDesc('published_at');
            $sphinx->SetFilterRange('published_at', self::SEARCH_EPOCH_START, time());
            $sphinx->q('');

            if (sizeof($sphinx->getFoundIds()) > 0) {
//                $dql = "SELECT a FROM SCArticleBundle:Article a WHERE a.id IN (?1) ORDER BY a.publishedAt DESC";
//                $query = $this->getDoctrine()->getManager()->createQuery($dql);
//                $query->setParameter(1, $sphinx->getFoundIds());


                $parameters = array('id' => $sphinx->getFoundIds());

                $articles = $this->getDoctrine()
                    ->getRepository('SCArticleBundle:Article')
                    ->findBy($parameters, ['publishedAt' => 'desc']);
                $this->container->get('sc_article_item.service')->hydratePreviews($articles);
            }

        }

//        if (!is_null($request->get('page'))) {
//            $page = $request->get('page');
//        }

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $articles,
            $request->get('page', 1),
            $limit = 15
        );

        $data['pagination'] = $pagination;
        $data['page'] = $request->get('page');

        return $this->render('IslamTodayApplicationBundle:Articles:category.html.twig', $data);
    }

    public function tagsAction($tag_path, $page = 1)
    {
        $data = [];

        $tag = $this->getDoctrine()->getRepository('SCTagBundle:Tag')->findOneByPath($tag_path);


        if (is_null($tag)) {
            throw $this->createNotFoundException('нет такого раздела');
        }

        $sphinx = $this->get('scsphinx.client');
        $indexes = $this->container->getParameter('sc_article.indexes.main') . ' ' . $this->container->getParameter('sc_article.indexes.delta');

        $sphinx->setIndexes($indexes);

        $articles = [];
        $sphinx->setFilter('tag', [$tag->getId()]);
        $sphinx->setFilter('is_published', array(1));
        $sphinx->setLimit(1000, 0);
        $sphinx->sortByDesc('published_at');
        $sphinx->SetFilterRange('published_at', self::SEARCH_EPOCH_START, time());
        $sphinx->q('');

        if (sizeof($sphinx->getFoundIds()) > 0) {
            $parameters = array('id' => $sphinx->getFoundIds());

            $articles = $this->getDoctrine()
                ->getRepository('SCArticleBundle:Article')
                ->findBy($parameters, ['publishedAt' => 'desc']);
            $this->container->get('sc_article_item.service')->hydratePreviews($articles);
        }

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $articles,
            $page,
            $limit = 40
        );

        $data['tag'] = $tag;

        $data['pagination'] = $pagination;
        return $this->render('IslamTodayApplicationBundle:Articles:tags.html.twig', $data);
    }
/*
    public function singleAction(Request $request, $category_path, $sub_category_path = null, $article_path)
    {
        $article = $this->getDoctrine()->getRepository('SCArticleBundle:Article')->findOneBy(['path' => $article_path, 'isPublished' => 1]);

        if (is_null($article)) {
            return $this->forward('IslamTodayApplicationBundle:Articles:inCategory', ['category_path' => $category_path,
                'sub_category_path' => $article_path, 'page' => $request->query->get('page', 1)]);
        }

        if ($article->getPrimaryCategory()->getPath() !== $category_path) {
            throw $this->createNotFoundException('не совпадает категория');
        }


        $response = new Response();
        $response->setExpires(new \DateTime());
        $response->setLastModified($article->getLastModyfiedAt());
        // Set response as public. Otherwise it will be private by default.
        $response->setPublic();

//        if ($response->isNotModified($request)) {
//            return $response;
//        }

        $this->container->get('sc_article_item.service')->hydratePreviews(array($article));
        $this->container->get('sc_article_item.service')->hydrateFiles(array($article));

        if (mb_strlen($article->getFlatContent()->getFlatContent(), 'utf-8') < 10) {
            $this->container->get('sc_article_item.service')->generateFlatContent($article);
        }
        $articleImage = isset($article->getPreviews()[440]) ? $article->getPreviews()[440]->getUrl() : '';
        $articleImageBig = isset($article->getPreviews()[540]) ? $article->getPreviews()[540]->getUrl() : '';


        $isArticleViewed = $this->getDoctrine()->getRepository('IslamTodayApplicationBundle:ArticleViews')
            ->checkViewByIpToday($article, $request->getClientIp());
        
        if ((!$isArticleViewed) && (!in_array($article->getId(), [24019, 23414, 23361, 23444, 23371, 23464, 23507, 23283, 23386, 21795, 24522, 19690]))) {

            $viewCnt = $this->getDoctrine()->getRepository('SCArticleBundle:ArticleStats')->findOneByArticle($article);
            $viewCnt->setCnt($viewCnt->getCnt() + 1);
            $this->getDoctrine()->getManager()->persist($viewCnt);
            $this->getDoctrine()->getManager()->flush();
        };

        return $this->render('IslamTodayApplicationBundle:Articles:single.html.twig', [
            'article' => $article,
            'category' => $article->getPrimaryCategory(), 'sub_category' => $article->getSecondaryCategories()[0],
            'articleImage' => $articleImage,
            'articleImageBig' => $articleImageBig
        ],
            $response);
    }
*/

    public function singleAction(Request $request, $category_path, $sub_category_path = null, $article_path)
    {
        $article = $this->getDoctrine()->getRepository('SCArticleBundle:Article')->findOneBy(['path' => $article_path, 'isPublished' => 1]);

        if (is_null($article)) {
            return $this->forward('IslamTodayApplicationBundle:Articles:inCategory', ['category_path' => $category_path,
                'sub_category_path' => $article_path, 'page' => $request->query->get('page', 1)]);
        }

        if ($article->getPrimaryCategory()->getPath() !== $category_path) {
            throw $this->createNotFoundException('не совпадает категория');
        }


        $response = new Response();
        $response->setExpires(new \DateTime());
        $response->setLastModified($article->getLastModyfiedAt());
        // Set response as public. Otherwise it will be private by default.
        $response->setPublic();

//        if ($response->isNotModified($request)) {
//            return $response;
//        }

        $this->container->get('sc_article_item.service')->hydratePreviews(array($article));
        $this->container->get('sc_article_item.service')->hydrateFiles(array($article));

        if (mb_strlen($article->getFlatContent()->getFlatContent(), 'utf-8') < 10) {
            $this->container->get('sc_article_item.service')->generateFlatContent($article);
        }
        $articleImage = isset($article->getPreviews()[440]) ? $article->getPreviews()[440]->getUrl() : '';
        $articleImageBig = isset($article->getPreviews()[540]) ? $article->getPreviews()[540]->getUrl() : '';

        /*
         * todo: это очень плохая логика
         * надо написать другой сборщик статистики
         * например, на memcached
         */

        $isArticleViewed = $this->getDoctrine()->getRepository('IslamTodayApplicationBundle:ArticleViews')
            ->checkViewByIpToday($article, $request->getClientIp());

        if ((!$isArticleViewed) && (!in_array($article->getId(), [24019, 23414, 23361, 23444, 23371, 23464, 23507, 23283, 23386, 21795, 24522, 19690]))) {

            $viewCnt = $this->getDoctrine()->getRepository('SCArticleBundle:ArticleStats')->findOneByArticle($article);
            $viewCnt->setCnt($viewCnt->getCnt() + 1);
            $this->getDoctrine()->getManager()->persist($viewCnt);
            $this->getDoctrine()->getManager()->flush();
        };

        $__content = explode("\n", $article->getFlatContent()->getFlatContent());
        $__sliderStartIndex = 0;
        $__contentWithSlider = [];
	$__slides = [];
        foreach ($__content as $__rn => $__row) {
            $_r = $__content[$__rn];

            if (preg_match("#\[slider\]#", $_r, $matches)) {
                $__sliderStartIndex = $__rn;
                $__contentWithSlider[] = "<div class=\"slider\"><ul class='slides'>";
                continue;
            }
            if ($__sliderStartIndex > 0) {
                if (preg_match_all("#<img.+title=\"(.*)\".+src=\"(.*)\"#U", $_r, $matches)) {
                    $__contentWithSlider[] = $this->getSlide([
                        'src' => $matches[2][0],
                        'title' => $matches[1][0],
                    ]);
                    $__slides[] = $this->getSlide([
                        'src' => $matches[2][0],
                        'title' => $matches[1][0],
                    ]);

                    continue;
                }
                if (preg_match("#.+height.+#", $_r)) {
                    continue;
                }
                if (preg_match("#\[/slider\]#", $_r, $matches)) {
                    $__contentWithSlider[] = "</ul></div>";
                    $__contentWithSlider[] = "<div class='slider-nav'><ul class='slides'>";
		    foreach ($__slides as $slide) {
                        $__contentWithSlider[] = $slide;
                    }
                    $__contentWithSlider[] = "</ul></div>";

                    $__sliderStartIndex = 0;
                }
                continue;
            }

            $__contentWithSlider[] = $__row;
        }
        $article->getFlatContent()->setFlatContent(implode("\n", $__contentWithSlider));

        return $this->render('IslamTodayApplicationBundle:Articles:single.html.twig', [
            'article' => $article,
            'category' => $article->getPrimaryCategory(), 'sub_category' => $article->getSecondaryCategories()[0],
            'articleImage' => $articleImage,
            'articleImageBig' => $articleImageBig
        ],
            $response);
    }

    private function getSlide($data)
    {
	preg_match_all("#(.+)/\d+-INNERRESIZED\d+-\d+-(.*)#",$data['src'],  $matches);
//	var_dump($matches);
	return '<li data-thumb="'.($matches[1][0]."/".$matches[2][0]).'"><img src="'.$data['src'].'" /></li>';
/*        return '
            <div class="main__item" style="background-image: url(' . $data['src'] . ')">
                <div class="container">
                    <div class="main__item__inner"></div>
                </div>
                <div class="main__item__overlay"></div>
            </div>
        ';*/
    }


    public function relatedTagsAction($tags = [])
    {
        $tagsArticles = [];
        foreach ($tags as $tag) {
            $tagsIds[] = $tag->getId();
        }

        if (!empty($tagsIds)) {

            $sphinx = $this->get('scsphinx.client');
            $indexes = $this->container->getParameter('sc_article.indexes.main') . ' ' . $this->container->getParameter('sc_article.indexes.delta');

            $sphinx->setIndexes($indexes);
            $sphinx->setLimit(4, 0);
            $sphinx->sortByDesc('tag');

            //$sphinx->setFilter('primary_category_id', array(38, 62, 84), true);
            $sphinx->setFilter('is_published', array(1));
            $sphinx->SetFilterRange('published_at', self::SEARCH_EPOCH_START, time());
            $sphinx->setFilter('tag', $tagsIds);

            $sphinx->q('');

            if (sizeof($sphinx->getFoundIds()) > 0) {
                $tagsArticles = $this->getDoctrine()
                    ->getRepository('SCArticleBundle:Article')
                    ->findBy(['id' => $sphinx->getFoundIds()]);
            }
        }

        return $this->render('IslamTodayApplicationBundle:Articles:relatedTags.html.twig', ['articles' => $tagsArticles]);

    }

    public function relatedAction($excludeId = null)
    {
        $excludeWhere = $excludeId ? ' AND a.id != ' . $excludeId : '';
        $sql = "SELECT a.id, s.cnt FROM `sc_article` a 
                INNER JOIN sc_article_stats s ON s.article_id = a.id
                WHERE s.cnt > 15000 AND a.primary_category_id NOT IN (38, 62, 75, 116, 117, 122) AND is_published = 1  $excludeWhere
                ";
        $stmt = $this->getDoctrine()->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();
        $ids = $stmt->fetchAll();
        shuffle($ids);
        $ids = array_slice($ids, 0, 5);
        $_ids = [];
$idsStr = "";
        if ($ids) {
            foreach ($ids as $articleId) {
                $idsStr .= ', ' . $articleId['id'];
                $_ids[] = $articleId['id'];
            };
            $idsStr = substr($idsStr, 2);
        };
/*
        $dql = "SELECT a FROM SCArticleBundle:Article a JOIN a.viewCnt s " .
//        "WHERE s.cnt > 6000 and a.primary_category not in (38, 62, 84) ".$excludeWhere ." ORDER BY a.publishedAt DESC";
            "WHERE a.id IN ($idsStr)";
        $query = $this->getDoctrine()->getManager()->createQuery($dql);
        $query->setMaxResults(8);

        $articles = $query->getResult();
*/
        $articles = $this->getDoctrine()->getRepository('SCArticleBundle:Article')->findByIds($_ids);

        $this->container->get('sc_article_item.service')->hydratePreviews($articles);
        return $this->render('IslamTodayApplicationBundle:Articles:related_articles.html.twig',
            [
                'articles' => $articles,
            ]);
    }

    public function popularAction()
    {
        $sphinx = $this->get('scsphinx.client');
        $indexes = $this->container->getParameter('sc_article.indexes.main') . ' ' . $this->container->getParameter('sc_article.indexes.delta');

        $sphinx->setIndexes($indexes);
        $sphinx->setLimit(5, 0);
        $sphinx->sortByDesc('published_at');

        $sphinx->setFilter('primary_category_id', array(38, 62, 84), true);
        $sphinx->setFilter('is_published', array(1));
        $sphinx->setFilter('is_recommended', array(1));
        $sphinx->SetFilterRange('published_at', self::SEARCH_EPOCH_START, time());

        $sphinx->q('');

        $articles = array();
        if (sizeof($sphinx->getFoundIds()) > 0) {
            $parameters = array('id' => $sphinx->getFoundIds());

            $articles = $this->getDoctrine()
                ->getRepository('SCArticleBundle:Article')
                ->findBy($parameters, ['publishedAt' => 'desc']);
            $this->container->get('sc_article_item.service')->hydratePreviews($articles);
        }

        return $this->render('IslamTodayApplicationBundle:Articles:popular_articles.html.twig', ['articles' => $articles]);
    }

}
