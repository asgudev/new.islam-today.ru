<?php

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = [
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new IslamToday\ApplicationBundle\IslamTodayApplicationBundle(),

            new SC\ArticleBundle\SCArticleBundle(),
            new SC\FileStorageBundle\SCFileStorageBundle(),
//            new Doctrine\Bundle\FixturesBundle\DoctrineFixturesBundle(),
            new SC\SphinxBundle\SCSphinxBundle(),

            // Pagination
            new Knp\Bundle\PaginatorBundle\KnpPaginatorBundle(),

            // DateTime internatialization
            new Sonata\IntlBundle\SonataIntlBundle(),

            new SC\CategoryBundle\SCCategoryBundle(),

            //голосовалка
            new SC\VotesBundle\SCVotesBundle(),

            // tags
            new SC\TagBundle\SCTagBundle(),

            // comments
            new SC\CommentsBundle\SCCommentsBundle(),

            // users
            new SC\UserBundle\SCUserBundle(),

            //фото галерея
            new SC\AdvancedGalleryBundle\SCAdvancedGalleryBundle(),

            // видео галерея
            new SC\GalleryBundle\SCGalleryBundle(),

            new SC\ServicesBundle\SCServicesBundle(),
            new SC\ServicesAdminBundle\SCServicesAdminBundle(),

            new SC\ExtraMenuBundle\SCExtraMenuBundle(),

            //сонник
            new SC\SonnikBundle\SCSonnikBundle(),

            //исламские термины
            new SC\TerminusBundle\SCTerminusBundle(),

            // сервис коран
            new SC\KoranBundle\SCKoranBundle(),

            new SC\BannersBundle\SCBannersBundle(),
            new SC\CrosswordBundle\CrosswordBundle(),
            new SC\PuzzleBundle\SCPuzzleBundle(),

            new SC\PollBundle\SCPollBundle(),

            new Liip\ImagineBundle\LiipImagineBundle(),

            new Asgu\GeoipBundle\GeoipBundle(),
            new Asgu\BookBundle\BookBundle(),
            new Asgu\MapBundle\MapBundle(),
        ];

        if (in_array($this->getEnvironment(), ['dev', 'test'], true)) {
            $bundles[] = new Symfony\Bundle\DebugBundle\DebugBundle();
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();

            if ('dev' === $this->getEnvironment()) {
                $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
                $bundles[] = new Symfony\Bundle\WebServerBundle\WebServerBundle();
            }
        }

        return $bundles;
    }

    public function getRootDir()
    {
        return __DIR__;
    }

    public function getCacheDir()
    {
        return dirname(__DIR__).'/var/cache/'.$this->getEnvironment();
    }

    public function getLogDir()
    {
        return dirname(__DIR__).'/var/logs';
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(function (ContainerBuilder $container) {
            $container->setParameter('container.autowiring.strict_mode', true);
            $container->setParameter('container.dumper.inline_class_loader', true);

            $container->addObjectResource($this);
        });
        $loader->load($this->getRootDir().'/config/config_'.$this->getEnvironment().'.yml');
    }
}
