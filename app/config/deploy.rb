############# test dev##########
#set :application, "islam-today.ru"
#set :domain,      "solidcube.ru"
#set :deploy_to,   "/home/web/deploy/#{application}"
############# end test dev ############

############## prod ###################
set :application, "islam-today.ru"
set :domain,      "#{application}"
set :deploy_to,   "/home/web/projects/#{domain}"
############## end prod ###############

set :app_path,    "app"

set :repository,  "git@bitbucket.org:skirow/islam-today.ru.git"
set :scm,         :git

set :model_manager, "doctrine"
# Or: `propel`

#role :web,        "solidcube.ru", "islam-today.ru"                         # Your HTTP server, Apache/etc
#role :app,        "solidcube.ru", "islam-today.ru", :primary => true       # This may be the same as your `Web` server
role :web,        "huzur1", "huzur2", "huzur3"
role :app,        "huzur1", "huzur2", "huzur3", :primary => true       # This may be the same as your `Web` server

set  :keep_releases,  3

# Be more verbose by uncommenting the following line
logger.level = Logger::MAX_LEVEL

set  :user,       "web"
# sudo отключили. деплоим из под владельца
set  :use_sudo,   false

# файлы которые хранятся в папке shared и копируются при каждом релизе
# например, в ./shared/app/config/parameters.ini хранятся настройки доступа к БД
# и пароль к админке
set :shared_files,      ["app/config/parameters.yml"]
# папки которые хранятся в папке shared и копируются при каждом релизе
# например, логи, папка для загрузки файлов, папка с библиотеками вендоров
#set :shared_children,     [app_path + "/logs", web_path + "/files", web_path + "/bundles"]
# запускает команду ./app/cache assetic:dump
set :dump_assetic_assets, true

set :deploy_via, :rsync_with_remote_cache

if variables.include?(:with_composer)
    set :shared_children,     [app_path + "/logs", web_path + "/files", web_path + "/bundles", "vendor"]
    set :use_composer, true
    set :update_vendors, true
    set :copy_vendors, true
else
    set :shared_children,     [app_path + "/logs", web_path + "/files", web_path + "/bundles", "vendor"]
    set :use_composer, true
    set :copy_vendors, true
end

