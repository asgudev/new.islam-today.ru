$(document).ready(function () {

    $( ".calendar" ).datepicker({
        onSelect: function(date){
            location.href="/search/?date="+date+"&show=articles";
        },
        dateFormat: "yy-mm-dd",
        maxDate: new Date()
    });

    $( ".calendar" ).datepicker( "option", $.datepicker.regional['ru']);

    var player = $("#radio")[0];
    if (player) {
        let isPlaying = false;
        $(document).on("click", "#play", function () {
            if (player.paused) {
                player.play();
                $("#play").addClass('paused');
            } else {
                player.pause();
                $("#play").removeClass('paused');
            }
        });
        function rangeSlider(id, onDrag) {
            var range = document.getElementById(id),
                dragger = range.children[0],
                down = false,
                rangeWidth, rangeLeft;

            range.addEventListener("mousedown", function (e) {
                rangeWidth = this.offsetWidth;
                rangeLeft = e.layerX;
                down = true;
                updateDragger(rangeWidth, rangeLeft);
                return false;
            });

            range.addEventListener("mousemove", function (e) {
                if (down) {
                    rangeWidth = this.offsetWidth;
                    rangeLeft = e.layerX;
                    updateDragger(rangeWidth, rangeLeft);
                }
            });

            range.addEventListener("mouseup", function () {
                down = false;
            });

            function updateDragger(rangeWidth, rangeLeft) {
                dragger.style.width = rangeLeft + 'px';
                if (typeof onDrag == "function") {
                    onDrag(rangeLeft / rangeWidth);
                }
            }
        }

        rangeSlider('volume', function (value) {
            console.log(value);
            player.volume = value;
            // document.getElementById('test-result').innerHTML = value + '%';
        });

        if (Hls.isSupported()) {
            var hls = new Hls();
            hls.loadSource('https://hls-iptv.tattelecom.ru/hls/huzur/radio/azan.m3u8');
            hls.attachMedia(player);
        } else if (player.canPlayType('application/vnd.apple.mpegurl')) {
            player.src = 'https://hls-iptv.tattelecom.ru/hls/huzur/radio/azan.m3u8';
        }
    }

    $(".side-menu .button").click(function () {
        $('.side-menu').toggleClass('active');
        return false;
    });
    var initLastNewsCarousel = function () {
        var $el = $('#last-news'),
            w_height = $('#main-articles').height() - $el.parents('.panel_bordered').height() - $('#secular-articles').height(),
            magic = 62;
        $el.removeClass('hide')
            .jcarousel({
                vertical: true,
                scroll: 2
            });
        if (w_height > 0) {
            $el.parents('.jcarousel-clip').height(w_height - magic);
        }
    };

    // Flexslider
    //------------------------------
    var mainArticles = $('#mainSlider ').flexslider({
        touch: false,
        start: initLastNewsCarousel,
        slideshowSpeed: 5000
    });
    //  if ($('#mainSlider').length == 0) initLastNewsCarousel();
    var articleSlider = $('.slider').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
	touch: true,
	sync: ".slider-nav",
    });

    $('.slider-nav').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: true,
        slideshow: false,
        itemWidth: 210,
        itemHeight: 158,
        touch: true,
        itemMargin: 5,
        asNavFor: '.slider'
  });
                $(".toggle-comments").click((e) => {
                    $(".comments").css({"height":"auto"});
                    return false;
                });

    $(document).on("click", ".slider .flex-active-slide", (e) => {
	window.open($(e.target).parent().data('thumb'), '_blank');
    });

function addLink() {
	if ((document.activeElement.tagName == 'TEXTAREA') || (document.activeElement.tagName == 'INPUT')) return;
	var body_element = document.getElementsByTagName('body')[0];
	var selection;
	selection = window.getSelection();
	var pagelink = "<br /><br /> Подробнее: <a href='"+document.location.href+"'>"+document.location.href+"</a>";
	var copytext = selection + pagelink;
	var newdiv = document.createElement('div');
	newdiv.style.position='absolute';
	newdiv.style.left='-99999px';
	body_element.appendChild(newdiv);
	newdiv.innerHTML = copytext;
	selection.selectAllChildren(newdiv);
	window.setTimeout(function() {
		body_element.removeChild(newdiv);
	},0);
}
document.oncopy = addLink;
});
