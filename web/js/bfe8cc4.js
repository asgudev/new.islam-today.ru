/*! jQuery v1.12.4 | (c) jQuery Foundation | jquery.org/license */
!function(a,b){"object"==typeof module&&"object"==typeof module.exports?module.exports=a.document?b(a,!0):function(a){if(!a.document)throw new Error("jQuery requires a window with a document");return b(a)}:b(a)}("undefined"!=typeof window?window:this,function(a,b){var c=[],d=a.document,e=c.slice,f=c.concat,g=c.push,h=c.indexOf,i={},j=i.toString,k=i.hasOwnProperty,l={},m="1.12.4",n=function(a,b){return new n.fn.init(a,b)},o=/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,p=/^-ms-/,q=/-([\da-z])/gi,r=function(a,b){return b.toUpperCase()};n.fn=n.prototype={jquery:m,constructor:n,selector:"",length:0,toArray:function(){return e.call(this)},get:function(a){return null!=a?0>a?this[a+this.length]:this[a]:e.call(this)},pushStack:function(a){var b=n.merge(this.constructor(),a);return b.prevObject=this,b.context=this.context,b},each:function(a){return n.each(this,a)},map:function(a){return this.pushStack(n.map(this,function(b,c){return a.call(b,c,b)}))},slice:function(){return this.pushStack(e.apply(this,arguments))},first:function(){return this.eq(0)},last:function(){return this.eq(-1)},eq:function(a){var b=this.length,c=+a+(0>a?b:0);return this.pushStack(c>=0&&b>c?[this[c]]:[])},end:function(){return this.prevObject||this.constructor()},push:g,sort:c.sort,splice:c.splice},n.extend=n.fn.extend=function(){var a,b,c,d,e,f,g=arguments[0]||{},h=1,i=arguments.length,j=!1;for("boolean"==typeof g&&(j=g,g=arguments[h]||{},h++),"object"==typeof g||n.isFunction(g)||(g={}),h===i&&(g=this,h--);i>h;h++)if(null!=(e=arguments[h]))for(d in e)a=g[d],c=e[d],g!==c&&(j&&c&&(n.isPlainObject(c)||(b=n.isArray(c)))?(b?(b=!1,f=a&&n.isArray(a)?a:[]):f=a&&n.isPlainObject(a)?a:{},g[d]=n.extend(j,f,c)):void 0!==c&&(g[d]=c));return g},n.extend({expando:"jQuery"+(m+Math.random()).replace(/\D/g,""),isReady:!0,error:function(a){throw new Error(a)},noop:function(){},isFunction:function(a){return"function"===n.type(a)},isArray:Array.isArray||function(a){return"array"===n.type(a)},isWindow:function(a){return null!=a&&a==a.window},isNumeric:function(a){var b=a&&a.toString();return!n.isArray(a)&&b-parseFloat(b)+1>=0},isEmptyObject:function(a){var b;for(b in a)return!1;return!0},isPlainObject:function(a){var b;if(!a||"object"!==n.type(a)||a.nodeType||n.isWindow(a))return!1;try{if(a.constructor&&!k.call(a,"constructor")&&!k.call(a.constructor.prototype,"isPrototypeOf"))return!1}catch(c){return!1}if(!l.ownFirst)for(b in a)return k.call(a,b);for(b in a);return void 0===b||k.call(a,b)},type:function(a){return null==a?a+"":"object"==typeof a||"function"==typeof a?i[j.call(a)]||"object":typeof a},globalEval:function(b){b&&n.trim(b)&&(a.execScript||function(b){a.eval.call(a,b)})(b)},camelCase:function(a){return a.replace(p,"ms-").replace(q,r)},nodeName:function(a,b){return a.nodeName&&a.nodeName.toLowerCase()===b.toLowerCase()},each:function(a,b){var c,d=0;if(s(a)){for(c=a.length;c>d;d++)if(b.call(a[d],d,a[d])===!1)break}else for(d in a)if(b.call(a[d],d,a[d])===!1)break;return a},trim:function(a){return null==a?"":(a+"").replace(o,"")},makeArray:function(a,b){var c=b||[];return null!=a&&(s(Object(a))?n.merge(c,"string"==typeof a?[a]:a):g.call(c,a)),c},inArray:function(a,b,c){var d;if(b){if(h)return h.call(b,a,c);for(d=b.length,c=c?0>c?Math.max(0,d+c):c:0;d>c;c++)if(c in b&&b[c]===a)return c}return-1},merge:function(a,b){var c=+b.length,d=0,e=a.length;while(c>d)a[e++]=b[d++];if(c!==c)while(void 0!==b[d])a[e++]=b[d++];return a.length=e,a},grep:function(a,b,c){for(var d,e=[],f=0,g=a.length,h=!c;g>f;f++)d=!b(a[f],f),d!==h&&e.push(a[f]);return e},map:function(a,b,c){var d,e,g=0,h=[];if(s(a))for(d=a.length;d>g;g++)e=b(a[g],g,c),null!=e&&h.push(e);else for(g in a)e=b(a[g],g,c),null!=e&&h.push(e);return f.apply([],h)},guid:1,proxy:function(a,b){var c,d,f;return"string"==typeof b&&(f=a[b],b=a,a=f),n.isFunction(a)?(c=e.call(arguments,2),d=function(){return a.apply(b||this,c.concat(e.call(arguments)))},d.guid=a.guid=a.guid||n.guid++,d):void 0},now:function(){return+new Date},support:l}),"function"==typeof Symbol&&(n.fn[Symbol.iterator]=c[Symbol.iterator]),n.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "),function(a,b){i["[object "+b+"]"]=b.toLowerCase()});function s(a){var b=!!a&&"length"in a&&a.length,c=n.type(a);return"function"===c||n.isWindow(a)?!1:"array"===c||0===b||"number"==typeof b&&b>0&&b-1 in a}var t=function(a){var b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u="sizzle"+1*new Date,v=a.document,w=0,x=0,y=ga(),z=ga(),A=ga(),B=function(a,b){return a===b&&(l=!0),0},C=1<<31,D={}.hasOwnProperty,E=[],F=E.pop,G=E.push,H=E.push,I=E.slice,J=function(a,b){for(var c=0,d=a.length;d>c;c++)if(a[c]===b)return c;return-1},K="checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",L="[\\x20\\t\\r\\n\\f]",M="(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+",N="\\["+L+"*("+M+")(?:"+L+"*([*^$|!~]?=)"+L+"*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|("+M+"))|)"+L+"*\\]",O=":("+M+")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|"+N+")*)|.*)\\)|)",P=new RegExp(L+"+","g"),Q=new RegExp("^"+L+"+|((?:^|[^\\\\])(?:\\\\.)*)"+L+"+$","g"),R=new RegExp("^"+L+"*,"+L+"*"),S=new RegExp("^"+L+"*([>+~]|"+L+")"+L+"*"),T=new RegExp("="+L+"*([^\\]'\"]*?)"+L+"*\\]","g"),U=new RegExp(O),V=new RegExp("^"+M+"$"),W={ID:new RegExp("^#("+M+")"),CLASS:new RegExp("^\\.("+M+")"),TAG:new RegExp("^("+M+"|[*])"),ATTR:new RegExp("^"+N),PSEUDO:new RegExp("^"+O),CHILD:new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\("+L+"*(even|odd|(([+-]|)(\\d*)n|)"+L+"*(?:([+-]|)"+L+"*(\\d+)|))"+L+"*\\)|)","i"),bool:new RegExp("^(?:"+K+")$","i"),needsContext:new RegExp("^"+L+"*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\("+L+"*((?:-\\d)?\\d*)"+L+"*\\)|)(?=[^-]|$)","i")},X=/^(?:input|select|textarea|button)$/i,Y=/^h\d$/i,Z=/^[^{]+\{\s*\[native \w/,$=/^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,_=/[+~]/,aa=/'|\\/g,ba=new RegExp("\\\\([\\da-f]{1,6}"+L+"?|("+L+")|.)","ig"),ca=function(a,b,c){var d="0x"+b-65536;return d!==d||c?b:0>d?String.fromCharCode(d+65536):String.fromCharCode(d>>10|55296,1023&d|56320)},da=function(){m()};try{H.apply(E=I.call(v.childNodes),v.childNodes),E[v.childNodes.length].nodeType}catch(ea){H={apply:E.length?function(a,b){G.apply(a,I.call(b))}:function(a,b){var c=a.length,d=0;while(a[c++]=b[d++]);a.length=c-1}}}function fa(a,b,d,e){var f,h,j,k,l,o,r,s,w=b&&b.ownerDocument,x=b?b.nodeType:9;if(d=d||[],"string"!=typeof a||!a||1!==x&&9!==x&&11!==x)return d;if(!e&&((b?b.ownerDocument||b:v)!==n&&m(b),b=b||n,p)){if(11!==x&&(o=$.exec(a)))if(f=o[1]){if(9===x){if(!(j=b.getElementById(f)))return d;if(j.id===f)return d.push(j),d}else if(w&&(j=w.getElementById(f))&&t(b,j)&&j.id===f)return d.push(j),d}else{if(o[2])return H.apply(d,b.getElementsByTagName(a)),d;if((f=o[3])&&c.getElementsByClassName&&b.getElementsByClassName)return H.apply(d,b.getElementsByClassName(f)),d}if(c.qsa&&!A[a+" "]&&(!q||!q.test(a))){if(1!==x)w=b,s=a;else if("object"!==b.nodeName.toLowerCase()){(k=b.getAttribute("id"))?k=k.replace(aa,"\\$&"):b.setAttribute("id",k=u),r=g(a),h=r.length,l=V.test(k)?"#"+k:"[id='"+k+"']";while(h--)r[h]=l+" "+qa(r[h]);s=r.join(","),w=_.test(a)&&oa(b.parentNode)||b}if(s)try{return H.apply(d,w.querySelectorAll(s)),d}catch(y){}finally{k===u&&b.removeAttribute("id")}}}return i(a.replace(Q,"$1"),b,d,e)}function ga(){var a=[];function b(c,e){return a.push(c+" ")>d.cacheLength&&delete b[a.shift()],b[c+" "]=e}return b}function ha(a){return a[u]=!0,a}function ia(a){var b=n.createElement("div");try{return!!a(b)}catch(c){return!1}finally{b.parentNode&&b.parentNode.removeChild(b),b=null}}function ja(a,b){var c=a.split("|"),e=c.length;while(e--)d.attrHandle[c[e]]=b}function ka(a,b){var c=b&&a,d=c&&1===a.nodeType&&1===b.nodeType&&(~b.sourceIndex||C)-(~a.sourceIndex||C);if(d)return d;if(c)while(c=c.nextSibling)if(c===b)return-1;return a?1:-1}function la(a){return function(b){var c=b.nodeName.toLowerCase();return"input"===c&&b.type===a}}function ma(a){return function(b){var c=b.nodeName.toLowerCase();return("input"===c||"button"===c)&&b.type===a}}function na(a){return ha(function(b){return b=+b,ha(function(c,d){var e,f=a([],c.length,b),g=f.length;while(g--)c[e=f[g]]&&(c[e]=!(d[e]=c[e]))})})}function oa(a){return a&&"undefined"!=typeof a.getElementsByTagName&&a}c=fa.support={},f=fa.isXML=function(a){var b=a&&(a.ownerDocument||a).documentElement;return b?"HTML"!==b.nodeName:!1},m=fa.setDocument=function(a){var b,e,g=a?a.ownerDocument||a:v;return g!==n&&9===g.nodeType&&g.documentElement?(n=g,o=n.documentElement,p=!f(n),(e=n.defaultView)&&e.top!==e&&(e.addEventListener?e.addEventListener("unload",da,!1):e.attachEvent&&e.attachEvent("onunload",da)),c.attributes=ia(function(a){return a.className="i",!a.getAttribute("className")}),c.getElementsByTagName=ia(function(a){return a.appendChild(n.createComment("")),!a.getElementsByTagName("*").length}),c.getElementsByClassName=Z.test(n.getElementsByClassName),c.getById=ia(function(a){return o.appendChild(a).id=u,!n.getElementsByName||!n.getElementsByName(u).length}),c.getById?(d.find.ID=function(a,b){if("undefined"!=typeof b.getElementById&&p){var c=b.getElementById(a);return c?[c]:[]}},d.filter.ID=function(a){var b=a.replace(ba,ca);return function(a){return a.getAttribute("id")===b}}):(delete d.find.ID,d.filter.ID=function(a){var b=a.replace(ba,ca);return function(a){var c="undefined"!=typeof a.getAttributeNode&&a.getAttributeNode("id");return c&&c.value===b}}),d.find.TAG=c.getElementsByTagName?function(a,b){return"undefined"!=typeof b.getElementsByTagName?b.getElementsByTagName(a):c.qsa?b.querySelectorAll(a):void 0}:function(a,b){var c,d=[],e=0,f=b.getElementsByTagName(a);if("*"===a){while(c=f[e++])1===c.nodeType&&d.push(c);return d}return f},d.find.CLASS=c.getElementsByClassName&&function(a,b){return"undefined"!=typeof b.getElementsByClassName&&p?b.getElementsByClassName(a):void 0},r=[],q=[],(c.qsa=Z.test(n.querySelectorAll))&&(ia(function(a){o.appendChild(a).innerHTML="<a id='"+u+"'></a><select id='"+u+"-\r\\' msallowcapture=''><option selected=''></option></select>",a.querySelectorAll("[msallowcapture^='']").length&&q.push("[*^$]="+L+"*(?:''|\"\")"),a.querySelectorAll("[selected]").length||q.push("\\["+L+"*(?:value|"+K+")"),a.querySelectorAll("[id~="+u+"-]").length||q.push("~="),a.querySelectorAll(":checked").length||q.push(":checked"),a.querySelectorAll("a#"+u+"+*").length||q.push(".#.+[+~]")}),ia(function(a){var b=n.createElement("input");b.setAttribute("type","hidden"),a.appendChild(b).setAttribute("name","D"),a.querySelectorAll("[name=d]").length&&q.push("name"+L+"*[*^$|!~]?="),a.querySelectorAll(":enabled").length||q.push(":enabled",":disabled"),a.querySelectorAll("*,:x"),q.push(",.*:")})),(c.matchesSelector=Z.test(s=o.matches||o.webkitMatchesSelector||o.mozMatchesSelector||o.oMatchesSelector||o.msMatchesSelector))&&ia(function(a){c.disconnectedMatch=s.call(a,"div"),s.call(a,"[s!='']:x"),r.push("!=",O)}),q=q.length&&new RegExp(q.join("|")),r=r.length&&new RegExp(r.join("|")),b=Z.test(o.compareDocumentPosition),t=b||Z.test(o.contains)?function(a,b){var c=9===a.nodeType?a.documentElement:a,d=b&&b.parentNode;return a===d||!(!d||1!==d.nodeType||!(c.contains?c.contains(d):a.compareDocumentPosition&&16&a.compareDocumentPosition(d)))}:function(a,b){if(b)while(b=b.parentNode)if(b===a)return!0;return!1},B=b?function(a,b){if(a===b)return l=!0,0;var d=!a.compareDocumentPosition-!b.compareDocumentPosition;return d?d:(d=(a.ownerDocument||a)===(b.ownerDocument||b)?a.compareDocumentPosition(b):1,1&d||!c.sortDetached&&b.compareDocumentPosition(a)===d?a===n||a.ownerDocument===v&&t(v,a)?-1:b===n||b.ownerDocument===v&&t(v,b)?1:k?J(k,a)-J(k,b):0:4&d?-1:1)}:function(a,b){if(a===b)return l=!0,0;var c,d=0,e=a.parentNode,f=b.parentNode,g=[a],h=[b];if(!e||!f)return a===n?-1:b===n?1:e?-1:f?1:k?J(k,a)-J(k,b):0;if(e===f)return ka(a,b);c=a;while(c=c.parentNode)g.unshift(c);c=b;while(c=c.parentNode)h.unshift(c);while(g[d]===h[d])d++;return d?ka(g[d],h[d]):g[d]===v?-1:h[d]===v?1:0},n):n},fa.matches=function(a,b){return fa(a,null,null,b)},fa.matchesSelector=function(a,b){if((a.ownerDocument||a)!==n&&m(a),b=b.replace(T,"='$1']"),c.matchesSelector&&p&&!A[b+" "]&&(!r||!r.test(b))&&(!q||!q.test(b)))try{var d=s.call(a,b);if(d||c.disconnectedMatch||a.document&&11!==a.document.nodeType)return d}catch(e){}return fa(b,n,null,[a]).length>0},fa.contains=function(a,b){return(a.ownerDocument||a)!==n&&m(a),t(a,b)},fa.attr=function(a,b){(a.ownerDocument||a)!==n&&m(a);var e=d.attrHandle[b.toLowerCase()],f=e&&D.call(d.attrHandle,b.toLowerCase())?e(a,b,!p):void 0;return void 0!==f?f:c.attributes||!p?a.getAttribute(b):(f=a.getAttributeNode(b))&&f.specified?f.value:null},fa.error=function(a){throw new Error("Syntax error, unrecognized expression: "+a)},fa.uniqueSort=function(a){var b,d=[],e=0,f=0;if(l=!c.detectDuplicates,k=!c.sortStable&&a.slice(0),a.sort(B),l){while(b=a[f++])b===a[f]&&(e=d.push(f));while(e--)a.splice(d[e],1)}return k=null,a},e=fa.getText=function(a){var b,c="",d=0,f=a.nodeType;if(f){if(1===f||9===f||11===f){if("string"==typeof a.textContent)return a.textContent;for(a=a.firstChild;a;a=a.nextSibling)c+=e(a)}else if(3===f||4===f)return a.nodeValue}else while(b=a[d++])c+=e(b);return c},d=fa.selectors={cacheLength:50,createPseudo:ha,match:W,attrHandle:{},find:{},relative:{">":{dir:"parentNode",first:!0}," ":{dir:"parentNode"},"+":{dir:"previousSibling",first:!0},"~":{dir:"previousSibling"}},preFilter:{ATTR:function(a){return a[1]=a[1].replace(ba,ca),a[3]=(a[3]||a[4]||a[5]||"").replace(ba,ca),"~="===a[2]&&(a[3]=" "+a[3]+" "),a.slice(0,4)},CHILD:function(a){return a[1]=a[1].toLowerCase(),"nth"===a[1].slice(0,3)?(a[3]||fa.error(a[0]),a[4]=+(a[4]?a[5]+(a[6]||1):2*("even"===a[3]||"odd"===a[3])),a[5]=+(a[7]+a[8]||"odd"===a[3])):a[3]&&fa.error(a[0]),a},PSEUDO:function(a){var b,c=!a[6]&&a[2];return W.CHILD.test(a[0])?null:(a[3]?a[2]=a[4]||a[5]||"":c&&U.test(c)&&(b=g(c,!0))&&(b=c.indexOf(")",c.length-b)-c.length)&&(a[0]=a[0].slice(0,b),a[2]=c.slice(0,b)),a.slice(0,3))}},filter:{TAG:function(a){var b=a.replace(ba,ca).toLowerCase();return"*"===a?function(){return!0}:function(a){return a.nodeName&&a.nodeName.toLowerCase()===b}},CLASS:function(a){var b=y[a+" "];return b||(b=new RegExp("(^|"+L+")"+a+"("+L+"|$)"))&&y(a,function(a){return b.test("string"==typeof a.className&&a.className||"undefined"!=typeof a.getAttribute&&a.getAttribute("class")||"")})},ATTR:function(a,b,c){return function(d){var e=fa.attr(d,a);return null==e?"!="===b:b?(e+="","="===b?e===c:"!="===b?e!==c:"^="===b?c&&0===e.indexOf(c):"*="===b?c&&e.indexOf(c)>-1:"$="===b?c&&e.slice(-c.length)===c:"~="===b?(" "+e.replace(P," ")+" ").indexOf(c)>-1:"|="===b?e===c||e.slice(0,c.length+1)===c+"-":!1):!0}},CHILD:function(a,b,c,d,e){var f="nth"!==a.slice(0,3),g="last"!==a.slice(-4),h="of-type"===b;return 1===d&&0===e?function(a){return!!a.parentNode}:function(b,c,i){var j,k,l,m,n,o,p=f!==g?"nextSibling":"previousSibling",q=b.parentNode,r=h&&b.nodeName.toLowerCase(),s=!i&&!h,t=!1;if(q){if(f){while(p){m=b;while(m=m[p])if(h?m.nodeName.toLowerCase()===r:1===m.nodeType)return!1;o=p="only"===a&&!o&&"nextSibling"}return!0}if(o=[g?q.firstChild:q.lastChild],g&&s){m=q,l=m[u]||(m[u]={}),k=l[m.uniqueID]||(l[m.uniqueID]={}),j=k[a]||[],n=j[0]===w&&j[1],t=n&&j[2],m=n&&q.childNodes[n];while(m=++n&&m&&m[p]||(t=n=0)||o.pop())if(1===m.nodeType&&++t&&m===b){k[a]=[w,n,t];break}}else if(s&&(m=b,l=m[u]||(m[u]={}),k=l[m.uniqueID]||(l[m.uniqueID]={}),j=k[a]||[],n=j[0]===w&&j[1],t=n),t===!1)while(m=++n&&m&&m[p]||(t=n=0)||o.pop())if((h?m.nodeName.toLowerCase()===r:1===m.nodeType)&&++t&&(s&&(l=m[u]||(m[u]={}),k=l[m.uniqueID]||(l[m.uniqueID]={}),k[a]=[w,t]),m===b))break;return t-=e,t===d||t%d===0&&t/d>=0}}},PSEUDO:function(a,b){var c,e=d.pseudos[a]||d.setFilters[a.toLowerCase()]||fa.error("unsupported pseudo: "+a);return e[u]?e(b):e.length>1?(c=[a,a,"",b],d.setFilters.hasOwnProperty(a.toLowerCase())?ha(function(a,c){var d,f=e(a,b),g=f.length;while(g--)d=J(a,f[g]),a[d]=!(c[d]=f[g])}):function(a){return e(a,0,c)}):e}},pseudos:{not:ha(function(a){var b=[],c=[],d=h(a.replace(Q,"$1"));return d[u]?ha(function(a,b,c,e){var f,g=d(a,null,e,[]),h=a.length;while(h--)(f=g[h])&&(a[h]=!(b[h]=f))}):function(a,e,f){return b[0]=a,d(b,null,f,c),b[0]=null,!c.pop()}}),has:ha(function(a){return function(b){return fa(a,b).length>0}}),contains:ha(function(a){return a=a.replace(ba,ca),function(b){return(b.textContent||b.innerText||e(b)).indexOf(a)>-1}}),lang:ha(function(a){return V.test(a||"")||fa.error("unsupported lang: "+a),a=a.replace(ba,ca).toLowerCase(),function(b){var c;do if(c=p?b.lang:b.getAttribute("xml:lang")||b.getAttribute("lang"))return c=c.toLowerCase(),c===a||0===c.indexOf(a+"-");while((b=b.parentNode)&&1===b.nodeType);return!1}}),target:function(b){var c=a.location&&a.location.hash;return c&&c.slice(1)===b.id},root:function(a){return a===o},focus:function(a){return a===n.activeElement&&(!n.hasFocus||n.hasFocus())&&!!(a.type||a.href||~a.tabIndex)},enabled:function(a){return a.disabled===!1},disabled:function(a){return a.disabled===!0},checked:function(a){var b=a.nodeName.toLowerCase();return"input"===b&&!!a.checked||"option"===b&&!!a.selected},selected:function(a){return a.parentNode&&a.parentNode.selectedIndex,a.selected===!0},empty:function(a){for(a=a.firstChild;a;a=a.nextSibling)if(a.nodeType<6)return!1;return!0},parent:function(a){return!d.pseudos.empty(a)},header:function(a){return Y.test(a.nodeName)},input:function(a){return X.test(a.nodeName)},button:function(a){var b=a.nodeName.toLowerCase();return"input"===b&&"button"===a.type||"button"===b},text:function(a){var b;return"input"===a.nodeName.toLowerCase()&&"text"===a.type&&(null==(b=a.getAttribute("type"))||"text"===b.toLowerCase())},first:na(function(){return[0]}),last:na(function(a,b){return[b-1]}),eq:na(function(a,b,c){return[0>c?c+b:c]}),even:na(function(a,b){for(var c=0;b>c;c+=2)a.push(c);return a}),odd:na(function(a,b){for(var c=1;b>c;c+=2)a.push(c);return a}),lt:na(function(a,b,c){for(var d=0>c?c+b:c;--d>=0;)a.push(d);return a}),gt:na(function(a,b,c){for(var d=0>c?c+b:c;++d<b;)a.push(d);return a})}},d.pseudos.nth=d.pseudos.eq;for(b in{radio:!0,checkbox:!0,file:!0,password:!0,image:!0})d.pseudos[b]=la(b);for(b in{submit:!0,reset:!0})d.pseudos[b]=ma(b);function pa(){}pa.prototype=d.filters=d.pseudos,d.setFilters=new pa,g=fa.tokenize=function(a,b){var c,e,f,g,h,i,j,k=z[a+" "];if(k)return b?0:k.slice(0);h=a,i=[],j=d.preFilter;while(h){c&&!(e=R.exec(h))||(e&&(h=h.slice(e[0].length)||h),i.push(f=[])),c=!1,(e=S.exec(h))&&(c=e.shift(),f.push({value:c,type:e[0].replace(Q," ")}),h=h.slice(c.length));for(g in d.filter)!(e=W[g].exec(h))||j[g]&&!(e=j[g](e))||(c=e.shift(),f.push({value:c,type:g,matches:e}),h=h.slice(c.length));if(!c)break}return b?h.length:h?fa.error(a):z(a,i).slice(0)};function qa(a){for(var b=0,c=a.length,d="";c>b;b++)d+=a[b].value;return d}function ra(a,b,c){var d=b.dir,e=c&&"parentNode"===d,f=x++;return b.first?function(b,c,f){while(b=b[d])if(1===b.nodeType||e)return a(b,c,f)}:function(b,c,g){var h,i,j,k=[w,f];if(g){while(b=b[d])if((1===b.nodeType||e)&&a(b,c,g))return!0}else while(b=b[d])if(1===b.nodeType||e){if(j=b[u]||(b[u]={}),i=j[b.uniqueID]||(j[b.uniqueID]={}),(h=i[d])&&h[0]===w&&h[1]===f)return k[2]=h[2];if(i[d]=k,k[2]=a(b,c,g))return!0}}}function sa(a){return a.length>1?function(b,c,d){var e=a.length;while(e--)if(!a[e](b,c,d))return!1;return!0}:a[0]}function ta(a,b,c){for(var d=0,e=b.length;e>d;d++)fa(a,b[d],c);return c}function ua(a,b,c,d,e){for(var f,g=[],h=0,i=a.length,j=null!=b;i>h;h++)(f=a[h])&&(c&&!c(f,d,e)||(g.push(f),j&&b.push(h)));return g}function va(a,b,c,d,e,f){return d&&!d[u]&&(d=va(d)),e&&!e[u]&&(e=va(e,f)),ha(function(f,g,h,i){var j,k,l,m=[],n=[],o=g.length,p=f||ta(b||"*",h.nodeType?[h]:h,[]),q=!a||!f&&b?p:ua(p,m,a,h,i),r=c?e||(f?a:o||d)?[]:g:q;if(c&&c(q,r,h,i),d){j=ua(r,n),d(j,[],h,i),k=j.length;while(k--)(l=j[k])&&(r[n[k]]=!(q[n[k]]=l))}if(f){if(e||a){if(e){j=[],k=r.length;while(k--)(l=r[k])&&j.push(q[k]=l);e(null,r=[],j,i)}k=r.length;while(k--)(l=r[k])&&(j=e?J(f,l):m[k])>-1&&(f[j]=!(g[j]=l))}}else r=ua(r===g?r.splice(o,r.length):r),e?e(null,g,r,i):H.apply(g,r)})}function wa(a){for(var b,c,e,f=a.length,g=d.relative[a[0].type],h=g||d.relative[" "],i=g?1:0,k=ra(function(a){return a===b},h,!0),l=ra(function(a){return J(b,a)>-1},h,!0),m=[function(a,c,d){var e=!g&&(d||c!==j)||((b=c).nodeType?k(a,c,d):l(a,c,d));return b=null,e}];f>i;i++)if(c=d.relative[a[i].type])m=[ra(sa(m),c)];else{if(c=d.filter[a[i].type].apply(null,a[i].matches),c[u]){for(e=++i;f>e;e++)if(d.relative[a[e].type])break;return va(i>1&&sa(m),i>1&&qa(a.slice(0,i-1).concat({value:" "===a[i-2].type?"*":""})).replace(Q,"$1"),c,e>i&&wa(a.slice(i,e)),f>e&&wa(a=a.slice(e)),f>e&&qa(a))}m.push(c)}return sa(m)}function xa(a,b){var c=b.length>0,e=a.length>0,f=function(f,g,h,i,k){var l,o,q,r=0,s="0",t=f&&[],u=[],v=j,x=f||e&&d.find.TAG("*",k),y=w+=null==v?1:Math.random()||.1,z=x.length;for(k&&(j=g===n||g||k);s!==z&&null!=(l=x[s]);s++){if(e&&l){o=0,g||l.ownerDocument===n||(m(l),h=!p);while(q=a[o++])if(q(l,g||n,h)){i.push(l);break}k&&(w=y)}c&&((l=!q&&l)&&r--,f&&t.push(l))}if(r+=s,c&&s!==r){o=0;while(q=b[o++])q(t,u,g,h);if(f){if(r>0)while(s--)t[s]||u[s]||(u[s]=F.call(i));u=ua(u)}H.apply(i,u),k&&!f&&u.length>0&&r+b.length>1&&fa.uniqueSort(i)}return k&&(w=y,j=v),t};return c?ha(f):f}return h=fa.compile=function(a,b){var c,d=[],e=[],f=A[a+" "];if(!f){b||(b=g(a)),c=b.length;while(c--)f=wa(b[c]),f[u]?d.push(f):e.push(f);f=A(a,xa(e,d)),f.selector=a}return f},i=fa.select=function(a,b,e,f){var i,j,k,l,m,n="function"==typeof a&&a,o=!f&&g(a=n.selector||a);if(e=e||[],1===o.length){if(j=o[0]=o[0].slice(0),j.length>2&&"ID"===(k=j[0]).type&&c.getById&&9===b.nodeType&&p&&d.relative[j[1].type]){if(b=(d.find.ID(k.matches[0].replace(ba,ca),b)||[])[0],!b)return e;n&&(b=b.parentNode),a=a.slice(j.shift().value.length)}i=W.needsContext.test(a)?0:j.length;while(i--){if(k=j[i],d.relative[l=k.type])break;if((m=d.find[l])&&(f=m(k.matches[0].replace(ba,ca),_.test(j[0].type)&&oa(b.parentNode)||b))){if(j.splice(i,1),a=f.length&&qa(j),!a)return H.apply(e,f),e;break}}}return(n||h(a,o))(f,b,!p,e,!b||_.test(a)&&oa(b.parentNode)||b),e},c.sortStable=u.split("").sort(B).join("")===u,c.detectDuplicates=!!l,m(),c.sortDetached=ia(function(a){return 1&a.compareDocumentPosition(n.createElement("div"))}),ia(function(a){return a.innerHTML="<a href='#'></a>","#"===a.firstChild.getAttribute("href")})||ja("type|href|height|width",function(a,b,c){return c?void 0:a.getAttribute(b,"type"===b.toLowerCase()?1:2)}),c.attributes&&ia(function(a){return a.innerHTML="<input/>",a.firstChild.setAttribute("value",""),""===a.firstChild.getAttribute("value")})||ja("value",function(a,b,c){return c||"input"!==a.nodeName.toLowerCase()?void 0:a.defaultValue}),ia(function(a){return null==a.getAttribute("disabled")})||ja(K,function(a,b,c){var d;return c?void 0:a[b]===!0?b.toLowerCase():(d=a.getAttributeNode(b))&&d.specified?d.value:null}),fa}(a);n.find=t,n.expr=t.selectors,n.expr[":"]=n.expr.pseudos,n.uniqueSort=n.unique=t.uniqueSort,n.text=t.getText,n.isXMLDoc=t.isXML,n.contains=t.contains;var u=function(a,b,c){var d=[],e=void 0!==c;while((a=a[b])&&9!==a.nodeType)if(1===a.nodeType){if(e&&n(a).is(c))break;d.push(a)}return d},v=function(a,b){for(var c=[];a;a=a.nextSibling)1===a.nodeType&&a!==b&&c.push(a);return c},w=n.expr.match.needsContext,x=/^<([\w-]+)\s*\/?>(?:<\/\1>|)$/,y=/^.[^:#\[\.,]*$/;function z(a,b,c){if(n.isFunction(b))return n.grep(a,function(a,d){return!!b.call(a,d,a)!==c});if(b.nodeType)return n.grep(a,function(a){return a===b!==c});if("string"==typeof b){if(y.test(b))return n.filter(b,a,c);b=n.filter(b,a)}return n.grep(a,function(a){return n.inArray(a,b)>-1!==c})}n.filter=function(a,b,c){var d=b[0];return c&&(a=":not("+a+")"),1===b.length&&1===d.nodeType?n.find.matchesSelector(d,a)?[d]:[]:n.find.matches(a,n.grep(b,function(a){return 1===a.nodeType}))},n.fn.extend({find:function(a){var b,c=[],d=this,e=d.length;if("string"!=typeof a)return this.pushStack(n(a).filter(function(){for(b=0;e>b;b++)if(n.contains(d[b],this))return!0}));for(b=0;e>b;b++)n.find(a,d[b],c);return c=this.pushStack(e>1?n.unique(c):c),c.selector=this.selector?this.selector+" "+a:a,c},filter:function(a){return this.pushStack(z(this,a||[],!1))},not:function(a){return this.pushStack(z(this,a||[],!0))},is:function(a){return!!z(this,"string"==typeof a&&w.test(a)?n(a):a||[],!1).length}});var A,B=/^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/,C=n.fn.init=function(a,b,c){var e,f;if(!a)return this;if(c=c||A,"string"==typeof a){if(e="<"===a.charAt(0)&&">"===a.charAt(a.length-1)&&a.length>=3?[null,a,null]:B.exec(a),!e||!e[1]&&b)return!b||b.jquery?(b||c).find(a):this.constructor(b).find(a);if(e[1]){if(b=b instanceof n?b[0]:b,n.merge(this,n.parseHTML(e[1],b&&b.nodeType?b.ownerDocument||b:d,!0)),x.test(e[1])&&n.isPlainObject(b))for(e in b)n.isFunction(this[e])?this[e](b[e]):this.attr(e,b[e]);return this}if(f=d.getElementById(e[2]),f&&f.parentNode){if(f.id!==e[2])return A.find(a);this.length=1,this[0]=f}return this.context=d,this.selector=a,this}return a.nodeType?(this.context=this[0]=a,this.length=1,this):n.isFunction(a)?"undefined"!=typeof c.ready?c.ready(a):a(n):(void 0!==a.selector&&(this.selector=a.selector,this.context=a.context),n.makeArray(a,this))};C.prototype=n.fn,A=n(d);var D=/^(?:parents|prev(?:Until|All))/,E={children:!0,contents:!0,next:!0,prev:!0};n.fn.extend({has:function(a){var b,c=n(a,this),d=c.length;return this.filter(function(){for(b=0;d>b;b++)if(n.contains(this,c[b]))return!0})},closest:function(a,b){for(var c,d=0,e=this.length,f=[],g=w.test(a)||"string"!=typeof a?n(a,b||this.context):0;e>d;d++)for(c=this[d];c&&c!==b;c=c.parentNode)if(c.nodeType<11&&(g?g.index(c)>-1:1===c.nodeType&&n.find.matchesSelector(c,a))){f.push(c);break}return this.pushStack(f.length>1?n.uniqueSort(f):f)},index:function(a){return a?"string"==typeof a?n.inArray(this[0],n(a)):n.inArray(a.jquery?a[0]:a,this):this[0]&&this[0].parentNode?this.first().prevAll().length:-1},add:function(a,b){return this.pushStack(n.uniqueSort(n.merge(this.get(),n(a,b))))},addBack:function(a){return this.add(null==a?this.prevObject:this.prevObject.filter(a))}});function F(a,b){do a=a[b];while(a&&1!==a.nodeType);return a}n.each({parent:function(a){var b=a.parentNode;return b&&11!==b.nodeType?b:null},parents:function(a){return u(a,"parentNode")},parentsUntil:function(a,b,c){return u(a,"parentNode",c)},next:function(a){return F(a,"nextSibling")},prev:function(a){return F(a,"previousSibling")},nextAll:function(a){return u(a,"nextSibling")},prevAll:function(a){return u(a,"previousSibling")},nextUntil:function(a,b,c){return u(a,"nextSibling",c)},prevUntil:function(a,b,c){return u(a,"previousSibling",c)},siblings:function(a){return v((a.parentNode||{}).firstChild,a)},children:function(a){return v(a.firstChild)},contents:function(a){return n.nodeName(a,"iframe")?a.contentDocument||a.contentWindow.document:n.merge([],a.childNodes)}},function(a,b){n.fn[a]=function(c,d){var e=n.map(this,b,c);return"Until"!==a.slice(-5)&&(d=c),d&&"string"==typeof d&&(e=n.filter(d,e)),this.length>1&&(E[a]||(e=n.uniqueSort(e)),D.test(a)&&(e=e.reverse())),this.pushStack(e)}});var G=/\S+/g;function H(a){var b={};return n.each(a.match(G)||[],function(a,c){b[c]=!0}),b}n.Callbacks=function(a){a="string"==typeof a?H(a):n.extend({},a);var b,c,d,e,f=[],g=[],h=-1,i=function(){for(e=a.once,d=b=!0;g.length;h=-1){c=g.shift();while(++h<f.length)f[h].apply(c[0],c[1])===!1&&a.stopOnFalse&&(h=f.length,c=!1)}a.memory||(c=!1),b=!1,e&&(f=c?[]:"")},j={add:function(){return f&&(c&&!b&&(h=f.length-1,g.push(c)),function d(b){n.each(b,function(b,c){n.isFunction(c)?a.unique&&j.has(c)||f.push(c):c&&c.length&&"string"!==n.type(c)&&d(c)})}(arguments),c&&!b&&i()),this},remove:function(){return n.each(arguments,function(a,b){var c;while((c=n.inArray(b,f,c))>-1)f.splice(c,1),h>=c&&h--}),this},has:function(a){return a?n.inArray(a,f)>-1:f.length>0},empty:function(){return f&&(f=[]),this},disable:function(){return e=g=[],f=c="",this},disabled:function(){return!f},lock:function(){return e=!0,c||j.disable(),this},locked:function(){return!!e},fireWith:function(a,c){return e||(c=c||[],c=[a,c.slice?c.slice():c],g.push(c),b||i()),this},fire:function(){return j.fireWith(this,arguments),this},fired:function(){return!!d}};return j},n.extend({Deferred:function(a){var b=[["resolve","done",n.Callbacks("once memory"),"resolved"],["reject","fail",n.Callbacks("once memory"),"rejected"],["notify","progress",n.Callbacks("memory")]],c="pending",d={state:function(){return c},always:function(){return e.done(arguments).fail(arguments),this},then:function(){var a=arguments;return n.Deferred(function(c){n.each(b,function(b,f){var g=n.isFunction(a[b])&&a[b];e[f[1]](function(){var a=g&&g.apply(this,arguments);a&&n.isFunction(a.promise)?a.promise().progress(c.notify).done(c.resolve).fail(c.reject):c[f[0]+"With"](this===d?c.promise():this,g?[a]:arguments)})}),a=null}).promise()},promise:function(a){return null!=a?n.extend(a,d):d}},e={};return d.pipe=d.then,n.each(b,function(a,f){var g=f[2],h=f[3];d[f[1]]=g.add,h&&g.add(function(){c=h},b[1^a][2].disable,b[2][2].lock),e[f[0]]=function(){return e[f[0]+"With"](this===e?d:this,arguments),this},e[f[0]+"With"]=g.fireWith}),d.promise(e),a&&a.call(e,e),e},when:function(a){var b=0,c=e.call(arguments),d=c.length,f=1!==d||a&&n.isFunction(a.promise)?d:0,g=1===f?a:n.Deferred(),h=function(a,b,c){return function(d){b[a]=this,c[a]=arguments.length>1?e.call(arguments):d,c===i?g.notifyWith(b,c):--f||g.resolveWith(b,c)}},i,j,k;if(d>1)for(i=new Array(d),j=new Array(d),k=new Array(d);d>b;b++)c[b]&&n.isFunction(c[b].promise)?c[b].promise().progress(h(b,j,i)).done(h(b,k,c)).fail(g.reject):--f;return f||g.resolveWith(k,c),g.promise()}});var I;n.fn.ready=function(a){return n.ready.promise().done(a),this},n.extend({isReady:!1,readyWait:1,holdReady:function(a){a?n.readyWait++:n.ready(!0)},ready:function(a){(a===!0?--n.readyWait:n.isReady)||(n.isReady=!0,a!==!0&&--n.readyWait>0||(I.resolveWith(d,[n]),n.fn.triggerHandler&&(n(d).triggerHandler("ready"),n(d).off("ready"))))}});function J(){d.addEventListener?(d.removeEventListener("DOMContentLoaded",K),a.removeEventListener("load",K)):(d.detachEvent("onreadystatechange",K),a.detachEvent("onload",K))}function K(){(d.addEventListener||"load"===a.event.type||"complete"===d.readyState)&&(J(),n.ready())}n.ready.promise=function(b){if(!I)if(I=n.Deferred(),"complete"===d.readyState||"loading"!==d.readyState&&!d.documentElement.doScroll)a.setTimeout(n.ready);else if(d.addEventListener)d.addEventListener("DOMContentLoaded",K),a.addEventListener("load",K);else{d.attachEvent("onreadystatechange",K),a.attachEvent("onload",K);var c=!1;try{c=null==a.frameElement&&d.documentElement}catch(e){}c&&c.doScroll&&!function f(){if(!n.isReady){try{c.doScroll("left")}catch(b){return a.setTimeout(f,50)}J(),n.ready()}}()}return I.promise(b)},n.ready.promise();var L;for(L in n(l))break;l.ownFirst="0"===L,l.inlineBlockNeedsLayout=!1,n(function(){var a,b,c,e;c=d.getElementsByTagName("body")[0],c&&c.style&&(b=d.createElement("div"),e=d.createElement("div"),e.style.cssText="position:absolute;border:0;width:0;height:0;top:0;left:-9999px",c.appendChild(e).appendChild(b),"undefined"!=typeof b.style.zoom&&(b.style.cssText="display:inline;margin:0;border:0;padding:1px;width:1px;zoom:1",l.inlineBlockNeedsLayout=a=3===b.offsetWidth,a&&(c.style.zoom=1)),c.removeChild(e))}),function(){var a=d.createElement("div");l.deleteExpando=!0;try{delete a.test}catch(b){l.deleteExpando=!1}a=null}();var M=function(a){var b=n.noData[(a.nodeName+" ").toLowerCase()],c=+a.nodeType||1;return 1!==c&&9!==c?!1:!b||b!==!0&&a.getAttribute("classid")===b},N=/^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,O=/([A-Z])/g;function P(a,b,c){if(void 0===c&&1===a.nodeType){var d="data-"+b.replace(O,"-$1").toLowerCase();if(c=a.getAttribute(d),"string"==typeof c){try{c="true"===c?!0:"false"===c?!1:"null"===c?null:+c+""===c?+c:N.test(c)?n.parseJSON(c):c}catch(e){}n.data(a,b,c)}else c=void 0;
}return c}function Q(a){var b;for(b in a)if(("data"!==b||!n.isEmptyObject(a[b]))&&"toJSON"!==b)return!1;return!0}function R(a,b,d,e){if(M(a)){var f,g,h=n.expando,i=a.nodeType,j=i?n.cache:a,k=i?a[h]:a[h]&&h;if(k&&j[k]&&(e||j[k].data)||void 0!==d||"string"!=typeof b)return k||(k=i?a[h]=c.pop()||n.guid++:h),j[k]||(j[k]=i?{}:{toJSON:n.noop}),"object"!=typeof b&&"function"!=typeof b||(e?j[k]=n.extend(j[k],b):j[k].data=n.extend(j[k].data,b)),g=j[k],e||(g.data||(g.data={}),g=g.data),void 0!==d&&(g[n.camelCase(b)]=d),"string"==typeof b?(f=g[b],null==f&&(f=g[n.camelCase(b)])):f=g,f}}function S(a,b,c){if(M(a)){var d,e,f=a.nodeType,g=f?n.cache:a,h=f?a[n.expando]:n.expando;if(g[h]){if(b&&(d=c?g[h]:g[h].data)){n.isArray(b)?b=b.concat(n.map(b,n.camelCase)):b in d?b=[b]:(b=n.camelCase(b),b=b in d?[b]:b.split(" ")),e=b.length;while(e--)delete d[b[e]];if(c?!Q(d):!n.isEmptyObject(d))return}(c||(delete g[h].data,Q(g[h])))&&(f?n.cleanData([a],!0):l.deleteExpando||g!=g.window?delete g[h]:g[h]=void 0)}}}n.extend({cache:{},noData:{"applet ":!0,"embed ":!0,"object ":"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"},hasData:function(a){return a=a.nodeType?n.cache[a[n.expando]]:a[n.expando],!!a&&!Q(a)},data:function(a,b,c){return R(a,b,c)},removeData:function(a,b){return S(a,b)},_data:function(a,b,c){return R(a,b,c,!0)},_removeData:function(a,b){return S(a,b,!0)}}),n.fn.extend({data:function(a,b){var c,d,e,f=this[0],g=f&&f.attributes;if(void 0===a){if(this.length&&(e=n.data(f),1===f.nodeType&&!n._data(f,"parsedAttrs"))){c=g.length;while(c--)g[c]&&(d=g[c].name,0===d.indexOf("data-")&&(d=n.camelCase(d.slice(5)),P(f,d,e[d])));n._data(f,"parsedAttrs",!0)}return e}return"object"==typeof a?this.each(function(){n.data(this,a)}):arguments.length>1?this.each(function(){n.data(this,a,b)}):f?P(f,a,n.data(f,a)):void 0},removeData:function(a){return this.each(function(){n.removeData(this,a)})}}),n.extend({queue:function(a,b,c){var d;return a?(b=(b||"fx")+"queue",d=n._data(a,b),c&&(!d||n.isArray(c)?d=n._data(a,b,n.makeArray(c)):d.push(c)),d||[]):void 0},dequeue:function(a,b){b=b||"fx";var c=n.queue(a,b),d=c.length,e=c.shift(),f=n._queueHooks(a,b),g=function(){n.dequeue(a,b)};"inprogress"===e&&(e=c.shift(),d--),e&&("fx"===b&&c.unshift("inprogress"),delete f.stop,e.call(a,g,f)),!d&&f&&f.empty.fire()},_queueHooks:function(a,b){var c=b+"queueHooks";return n._data(a,c)||n._data(a,c,{empty:n.Callbacks("once memory").add(function(){n._removeData(a,b+"queue"),n._removeData(a,c)})})}}),n.fn.extend({queue:function(a,b){var c=2;return"string"!=typeof a&&(b=a,a="fx",c--),arguments.length<c?n.queue(this[0],a):void 0===b?this:this.each(function(){var c=n.queue(this,a,b);n._queueHooks(this,a),"fx"===a&&"inprogress"!==c[0]&&n.dequeue(this,a)})},dequeue:function(a){return this.each(function(){n.dequeue(this,a)})},clearQueue:function(a){return this.queue(a||"fx",[])},promise:function(a,b){var c,d=1,e=n.Deferred(),f=this,g=this.length,h=function(){--d||e.resolveWith(f,[f])};"string"!=typeof a&&(b=a,a=void 0),a=a||"fx";while(g--)c=n._data(f[g],a+"queueHooks"),c&&c.empty&&(d++,c.empty.add(h));return h(),e.promise(b)}}),function(){var a;l.shrinkWrapBlocks=function(){if(null!=a)return a;a=!1;var b,c,e;return c=d.getElementsByTagName("body")[0],c&&c.style?(b=d.createElement("div"),e=d.createElement("div"),e.style.cssText="position:absolute;border:0;width:0;height:0;top:0;left:-9999px",c.appendChild(e).appendChild(b),"undefined"!=typeof b.style.zoom&&(b.style.cssText="-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:1px;width:1px;zoom:1",b.appendChild(d.createElement("div")).style.width="5px",a=3!==b.offsetWidth),c.removeChild(e),a):void 0}}();var T=/[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,U=new RegExp("^(?:([+-])=|)("+T+")([a-z%]*)$","i"),V=["Top","Right","Bottom","Left"],W=function(a,b){return a=b||a,"none"===n.css(a,"display")||!n.contains(a.ownerDocument,a)};function X(a,b,c,d){var e,f=1,g=20,h=d?function(){return d.cur()}:function(){return n.css(a,b,"")},i=h(),j=c&&c[3]||(n.cssNumber[b]?"":"px"),k=(n.cssNumber[b]||"px"!==j&&+i)&&U.exec(n.css(a,b));if(k&&k[3]!==j){j=j||k[3],c=c||[],k=+i||1;do f=f||".5",k/=f,n.style(a,b,k+j);while(f!==(f=h()/i)&&1!==f&&--g)}return c&&(k=+k||+i||0,e=c[1]?k+(c[1]+1)*c[2]:+c[2],d&&(d.unit=j,d.start=k,d.end=e)),e}var Y=function(a,b,c,d,e,f,g){var h=0,i=a.length,j=null==c;if("object"===n.type(c)){e=!0;for(h in c)Y(a,b,h,c[h],!0,f,g)}else if(void 0!==d&&(e=!0,n.isFunction(d)||(g=!0),j&&(g?(b.call(a,d),b=null):(j=b,b=function(a,b,c){return j.call(n(a),c)})),b))for(;i>h;h++)b(a[h],c,g?d:d.call(a[h],h,b(a[h],c)));return e?a:j?b.call(a):i?b(a[0],c):f},Z=/^(?:checkbox|radio)$/i,$=/<([\w:-]+)/,_=/^$|\/(?:java|ecma)script/i,aa=/^\s+/,ba="abbr|article|aside|audio|bdi|canvas|data|datalist|details|dialog|figcaption|figure|footer|header|hgroup|main|mark|meter|nav|output|picture|progress|section|summary|template|time|video";function ca(a){var b=ba.split("|"),c=a.createDocumentFragment();if(c.createElement)while(b.length)c.createElement(b.pop());return c}!function(){var a=d.createElement("div"),b=d.createDocumentFragment(),c=d.createElement("input");a.innerHTML="  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>",l.leadingWhitespace=3===a.firstChild.nodeType,l.tbody=!a.getElementsByTagName("tbody").length,l.htmlSerialize=!!a.getElementsByTagName("link").length,l.html5Clone="<:nav></:nav>"!==d.createElement("nav").cloneNode(!0).outerHTML,c.type="checkbox",c.checked=!0,b.appendChild(c),l.appendChecked=c.checked,a.innerHTML="<textarea>x</textarea>",l.noCloneChecked=!!a.cloneNode(!0).lastChild.defaultValue,b.appendChild(a),c=d.createElement("input"),c.setAttribute("type","radio"),c.setAttribute("checked","checked"),c.setAttribute("name","t"),a.appendChild(c),l.checkClone=a.cloneNode(!0).cloneNode(!0).lastChild.checked,l.noCloneEvent=!!a.addEventListener,a[n.expando]=1,l.attributes=!a.getAttribute(n.expando)}();var da={option:[1,"<select multiple='multiple'>","</select>"],legend:[1,"<fieldset>","</fieldset>"],area:[1,"<map>","</map>"],param:[1,"<object>","</object>"],thead:[1,"<table>","</table>"],tr:[2,"<table><tbody>","</tbody></table>"],col:[2,"<table><tbody></tbody><colgroup>","</colgroup></table>"],td:[3,"<table><tbody><tr>","</tr></tbody></table>"],_default:l.htmlSerialize?[0,"",""]:[1,"X<div>","</div>"]};da.optgroup=da.option,da.tbody=da.tfoot=da.colgroup=da.caption=da.thead,da.th=da.td;function ea(a,b){var c,d,e=0,f="undefined"!=typeof a.getElementsByTagName?a.getElementsByTagName(b||"*"):"undefined"!=typeof a.querySelectorAll?a.querySelectorAll(b||"*"):void 0;if(!f)for(f=[],c=a.childNodes||a;null!=(d=c[e]);e++)!b||n.nodeName(d,b)?f.push(d):n.merge(f,ea(d,b));return void 0===b||b&&n.nodeName(a,b)?n.merge([a],f):f}function fa(a,b){for(var c,d=0;null!=(c=a[d]);d++)n._data(c,"globalEval",!b||n._data(b[d],"globalEval"))}var ga=/<|&#?\w+;/,ha=/<tbody/i;function ia(a){Z.test(a.type)&&(a.defaultChecked=a.checked)}function ja(a,b,c,d,e){for(var f,g,h,i,j,k,m,o=a.length,p=ca(b),q=[],r=0;o>r;r++)if(g=a[r],g||0===g)if("object"===n.type(g))n.merge(q,g.nodeType?[g]:g);else if(ga.test(g)){i=i||p.appendChild(b.createElement("div")),j=($.exec(g)||["",""])[1].toLowerCase(),m=da[j]||da._default,i.innerHTML=m[1]+n.htmlPrefilter(g)+m[2],f=m[0];while(f--)i=i.lastChild;if(!l.leadingWhitespace&&aa.test(g)&&q.push(b.createTextNode(aa.exec(g)[0])),!l.tbody){g="table"!==j||ha.test(g)?"<table>"!==m[1]||ha.test(g)?0:i:i.firstChild,f=g&&g.childNodes.length;while(f--)n.nodeName(k=g.childNodes[f],"tbody")&&!k.childNodes.length&&g.removeChild(k)}n.merge(q,i.childNodes),i.textContent="";while(i.firstChild)i.removeChild(i.firstChild);i=p.lastChild}else q.push(b.createTextNode(g));i&&p.removeChild(i),l.appendChecked||n.grep(ea(q,"input"),ia),r=0;while(g=q[r++])if(d&&n.inArray(g,d)>-1)e&&e.push(g);else if(h=n.contains(g.ownerDocument,g),i=ea(p.appendChild(g),"script"),h&&fa(i),c){f=0;while(g=i[f++])_.test(g.type||"")&&c.push(g)}return i=null,p}!function(){var b,c,e=d.createElement("div");for(b in{submit:!0,change:!0,focusin:!0})c="on"+b,(l[b]=c in a)||(e.setAttribute(c,"t"),l[b]=e.attributes[c].expando===!1);e=null}();var ka=/^(?:input|select|textarea)$/i,la=/^key/,ma=/^(?:mouse|pointer|contextmenu|drag|drop)|click/,na=/^(?:focusinfocus|focusoutblur)$/,oa=/^([^.]*)(?:\.(.+)|)/;function pa(){return!0}function qa(){return!1}function ra(){try{return d.activeElement}catch(a){}}function sa(a,b,c,d,e,f){var g,h;if("object"==typeof b){"string"!=typeof c&&(d=d||c,c=void 0);for(h in b)sa(a,h,c,d,b[h],f);return a}if(null==d&&null==e?(e=c,d=c=void 0):null==e&&("string"==typeof c?(e=d,d=void 0):(e=d,d=c,c=void 0)),e===!1)e=qa;else if(!e)return a;return 1===f&&(g=e,e=function(a){return n().off(a),g.apply(this,arguments)},e.guid=g.guid||(g.guid=n.guid++)),a.each(function(){n.event.add(this,b,e,d,c)})}n.event={global:{},add:function(a,b,c,d,e){var f,g,h,i,j,k,l,m,o,p,q,r=n._data(a);if(r){c.handler&&(i=c,c=i.handler,e=i.selector),c.guid||(c.guid=n.guid++),(g=r.events)||(g=r.events={}),(k=r.handle)||(k=r.handle=function(a){return"undefined"==typeof n||a&&n.event.triggered===a.type?void 0:n.event.dispatch.apply(k.elem,arguments)},k.elem=a),b=(b||"").match(G)||[""],h=b.length;while(h--)f=oa.exec(b[h])||[],o=q=f[1],p=(f[2]||"").split(".").sort(),o&&(j=n.event.special[o]||{},o=(e?j.delegateType:j.bindType)||o,j=n.event.special[o]||{},l=n.extend({type:o,origType:q,data:d,handler:c,guid:c.guid,selector:e,needsContext:e&&n.expr.match.needsContext.test(e),namespace:p.join(".")},i),(m=g[o])||(m=g[o]=[],m.delegateCount=0,j.setup&&j.setup.call(a,d,p,k)!==!1||(a.addEventListener?a.addEventListener(o,k,!1):a.attachEvent&&a.attachEvent("on"+o,k))),j.add&&(j.add.call(a,l),l.handler.guid||(l.handler.guid=c.guid)),e?m.splice(m.delegateCount++,0,l):m.push(l),n.event.global[o]=!0);a=null}},remove:function(a,b,c,d,e){var f,g,h,i,j,k,l,m,o,p,q,r=n.hasData(a)&&n._data(a);if(r&&(k=r.events)){b=(b||"").match(G)||[""],j=b.length;while(j--)if(h=oa.exec(b[j])||[],o=q=h[1],p=(h[2]||"").split(".").sort(),o){l=n.event.special[o]||{},o=(d?l.delegateType:l.bindType)||o,m=k[o]||[],h=h[2]&&new RegExp("(^|\\.)"+p.join("\\.(?:.*\\.|)")+"(\\.|$)"),i=f=m.length;while(f--)g=m[f],!e&&q!==g.origType||c&&c.guid!==g.guid||h&&!h.test(g.namespace)||d&&d!==g.selector&&("**"!==d||!g.selector)||(m.splice(f,1),g.selector&&m.delegateCount--,l.remove&&l.remove.call(a,g));i&&!m.length&&(l.teardown&&l.teardown.call(a,p,r.handle)!==!1||n.removeEvent(a,o,r.handle),delete k[o])}else for(o in k)n.event.remove(a,o+b[j],c,d,!0);n.isEmptyObject(k)&&(delete r.handle,n._removeData(a,"events"))}},trigger:function(b,c,e,f){var g,h,i,j,l,m,o,p=[e||d],q=k.call(b,"type")?b.type:b,r=k.call(b,"namespace")?b.namespace.split("."):[];if(i=m=e=e||d,3!==e.nodeType&&8!==e.nodeType&&!na.test(q+n.event.triggered)&&(q.indexOf(".")>-1&&(r=q.split("."),q=r.shift(),r.sort()),h=q.indexOf(":")<0&&"on"+q,b=b[n.expando]?b:new n.Event(q,"object"==typeof b&&b),b.isTrigger=f?2:3,b.namespace=r.join("."),b.rnamespace=b.namespace?new RegExp("(^|\\.)"+r.join("\\.(?:.*\\.|)")+"(\\.|$)"):null,b.result=void 0,b.target||(b.target=e),c=null==c?[b]:n.makeArray(c,[b]),l=n.event.special[q]||{},f||!l.trigger||l.trigger.apply(e,c)!==!1)){if(!f&&!l.noBubble&&!n.isWindow(e)){for(j=l.delegateType||q,na.test(j+q)||(i=i.parentNode);i;i=i.parentNode)p.push(i),m=i;m===(e.ownerDocument||d)&&p.push(m.defaultView||m.parentWindow||a)}o=0;while((i=p[o++])&&!b.isPropagationStopped())b.type=o>1?j:l.bindType||q,g=(n._data(i,"events")||{})[b.type]&&n._data(i,"handle"),g&&g.apply(i,c),g=h&&i[h],g&&g.apply&&M(i)&&(b.result=g.apply(i,c),b.result===!1&&b.preventDefault());if(b.type=q,!f&&!b.isDefaultPrevented()&&(!l._default||l._default.apply(p.pop(),c)===!1)&&M(e)&&h&&e[q]&&!n.isWindow(e)){m=e[h],m&&(e[h]=null),n.event.triggered=q;try{e[q]()}catch(s){}n.event.triggered=void 0,m&&(e[h]=m)}return b.result}},dispatch:function(a){a=n.event.fix(a);var b,c,d,f,g,h=[],i=e.call(arguments),j=(n._data(this,"events")||{})[a.type]||[],k=n.event.special[a.type]||{};if(i[0]=a,a.delegateTarget=this,!k.preDispatch||k.preDispatch.call(this,a)!==!1){h=n.event.handlers.call(this,a,j),b=0;while((f=h[b++])&&!a.isPropagationStopped()){a.currentTarget=f.elem,c=0;while((g=f.handlers[c++])&&!a.isImmediatePropagationStopped())a.rnamespace&&!a.rnamespace.test(g.namespace)||(a.handleObj=g,a.data=g.data,d=((n.event.special[g.origType]||{}).handle||g.handler).apply(f.elem,i),void 0!==d&&(a.result=d)===!1&&(a.preventDefault(),a.stopPropagation()))}return k.postDispatch&&k.postDispatch.call(this,a),a.result}},handlers:function(a,b){var c,d,e,f,g=[],h=b.delegateCount,i=a.target;if(h&&i.nodeType&&("click"!==a.type||isNaN(a.button)||a.button<1))for(;i!=this;i=i.parentNode||this)if(1===i.nodeType&&(i.disabled!==!0||"click"!==a.type)){for(d=[],c=0;h>c;c++)f=b[c],e=f.selector+" ",void 0===d[e]&&(d[e]=f.needsContext?n(e,this).index(i)>-1:n.find(e,this,null,[i]).length),d[e]&&d.push(f);d.length&&g.push({elem:i,handlers:d})}return h<b.length&&g.push({elem:this,handlers:b.slice(h)}),g},fix:function(a){if(a[n.expando])return a;var b,c,e,f=a.type,g=a,h=this.fixHooks[f];h||(this.fixHooks[f]=h=ma.test(f)?this.mouseHooks:la.test(f)?this.keyHooks:{}),e=h.props?this.props.concat(h.props):this.props,a=new n.Event(g),b=e.length;while(b--)c=e[b],a[c]=g[c];return a.target||(a.target=g.srcElement||d),3===a.target.nodeType&&(a.target=a.target.parentNode),a.metaKey=!!a.metaKey,h.filter?h.filter(a,g):a},props:"altKey bubbles cancelable ctrlKey currentTarget detail eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),fixHooks:{},keyHooks:{props:"char charCode key keyCode".split(" "),filter:function(a,b){return null==a.which&&(a.which=null!=b.charCode?b.charCode:b.keyCode),a}},mouseHooks:{props:"button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement".split(" "),filter:function(a,b){var c,e,f,g=b.button,h=b.fromElement;return null==a.pageX&&null!=b.clientX&&(e=a.target.ownerDocument||d,f=e.documentElement,c=e.body,a.pageX=b.clientX+(f&&f.scrollLeft||c&&c.scrollLeft||0)-(f&&f.clientLeft||c&&c.clientLeft||0),a.pageY=b.clientY+(f&&f.scrollTop||c&&c.scrollTop||0)-(f&&f.clientTop||c&&c.clientTop||0)),!a.relatedTarget&&h&&(a.relatedTarget=h===a.target?b.toElement:h),a.which||void 0===g||(a.which=1&g?1:2&g?3:4&g?2:0),a}},special:{load:{noBubble:!0},focus:{trigger:function(){if(this!==ra()&&this.focus)try{return this.focus(),!1}catch(a){}},delegateType:"focusin"},blur:{trigger:function(){return this===ra()&&this.blur?(this.blur(),!1):void 0},delegateType:"focusout"},click:{trigger:function(){return n.nodeName(this,"input")&&"checkbox"===this.type&&this.click?(this.click(),!1):void 0},_default:function(a){return n.nodeName(a.target,"a")}},beforeunload:{postDispatch:function(a){void 0!==a.result&&a.originalEvent&&(a.originalEvent.returnValue=a.result)}}},simulate:function(a,b,c){var d=n.extend(new n.Event,c,{type:a,isSimulated:!0});n.event.trigger(d,null,b),d.isDefaultPrevented()&&c.preventDefault()}},n.removeEvent=d.removeEventListener?function(a,b,c){a.removeEventListener&&a.removeEventListener(b,c)}:function(a,b,c){var d="on"+b;a.detachEvent&&("undefined"==typeof a[d]&&(a[d]=null),a.detachEvent(d,c))},n.Event=function(a,b){return this instanceof n.Event?(a&&a.type?(this.originalEvent=a,this.type=a.type,this.isDefaultPrevented=a.defaultPrevented||void 0===a.defaultPrevented&&a.returnValue===!1?pa:qa):this.type=a,b&&n.extend(this,b),this.timeStamp=a&&a.timeStamp||n.now(),void(this[n.expando]=!0)):new n.Event(a,b)},n.Event.prototype={constructor:n.Event,isDefaultPrevented:qa,isPropagationStopped:qa,isImmediatePropagationStopped:qa,preventDefault:function(){var a=this.originalEvent;this.isDefaultPrevented=pa,a&&(a.preventDefault?a.preventDefault():a.returnValue=!1)},stopPropagation:function(){var a=this.originalEvent;this.isPropagationStopped=pa,a&&!this.isSimulated&&(a.stopPropagation&&a.stopPropagation(),a.cancelBubble=!0)},stopImmediatePropagation:function(){var a=this.originalEvent;this.isImmediatePropagationStopped=pa,a&&a.stopImmediatePropagation&&a.stopImmediatePropagation(),this.stopPropagation()}},n.each({mouseenter:"mouseover",mouseleave:"mouseout",pointerenter:"pointerover",pointerleave:"pointerout"},function(a,b){n.event.special[a]={delegateType:b,bindType:b,handle:function(a){var c,d=this,e=a.relatedTarget,f=a.handleObj;return e&&(e===d||n.contains(d,e))||(a.type=f.origType,c=f.handler.apply(this,arguments),a.type=b),c}}}),l.submit||(n.event.special.submit={setup:function(){return n.nodeName(this,"form")?!1:void n.event.add(this,"click._submit keypress._submit",function(a){var b=a.target,c=n.nodeName(b,"input")||n.nodeName(b,"button")?n.prop(b,"form"):void 0;c&&!n._data(c,"submit")&&(n.event.add(c,"submit._submit",function(a){a._submitBubble=!0}),n._data(c,"submit",!0))})},postDispatch:function(a){a._submitBubble&&(delete a._submitBubble,this.parentNode&&!a.isTrigger&&n.event.simulate("submit",this.parentNode,a))},teardown:function(){return n.nodeName(this,"form")?!1:void n.event.remove(this,"._submit")}}),l.change||(n.event.special.change={setup:function(){return ka.test(this.nodeName)?("checkbox"!==this.type&&"radio"!==this.type||(n.event.add(this,"propertychange._change",function(a){"checked"===a.originalEvent.propertyName&&(this._justChanged=!0)}),n.event.add(this,"click._change",function(a){this._justChanged&&!a.isTrigger&&(this._justChanged=!1),n.event.simulate("change",this,a)})),!1):void n.event.add(this,"beforeactivate._change",function(a){var b=a.target;ka.test(b.nodeName)&&!n._data(b,"change")&&(n.event.add(b,"change._change",function(a){!this.parentNode||a.isSimulated||a.isTrigger||n.event.simulate("change",this.parentNode,a)}),n._data(b,"change",!0))})},handle:function(a){var b=a.target;return this!==b||a.isSimulated||a.isTrigger||"radio"!==b.type&&"checkbox"!==b.type?a.handleObj.handler.apply(this,arguments):void 0},teardown:function(){return n.event.remove(this,"._change"),!ka.test(this.nodeName)}}),l.focusin||n.each({focus:"focusin",blur:"focusout"},function(a,b){var c=function(a){n.event.simulate(b,a.target,n.event.fix(a))};n.event.special[b]={setup:function(){var d=this.ownerDocument||this,e=n._data(d,b);e||d.addEventListener(a,c,!0),n._data(d,b,(e||0)+1)},teardown:function(){var d=this.ownerDocument||this,e=n._data(d,b)-1;e?n._data(d,b,e):(d.removeEventListener(a,c,!0),n._removeData(d,b))}}}),n.fn.extend({on:function(a,b,c,d){return sa(this,a,b,c,d)},one:function(a,b,c,d){return sa(this,a,b,c,d,1)},off:function(a,b,c){var d,e;if(a&&a.preventDefault&&a.handleObj)return d=a.handleObj,n(a.delegateTarget).off(d.namespace?d.origType+"."+d.namespace:d.origType,d.selector,d.handler),this;if("object"==typeof a){for(e in a)this.off(e,b,a[e]);return this}return b!==!1&&"function"!=typeof b||(c=b,b=void 0),c===!1&&(c=qa),this.each(function(){n.event.remove(this,a,c,b)})},trigger:function(a,b){return this.each(function(){n.event.trigger(a,b,this)})},triggerHandler:function(a,b){var c=this[0];return c?n.event.trigger(a,b,c,!0):void 0}});var ta=/ jQuery\d+="(?:null|\d+)"/g,ua=new RegExp("<(?:"+ba+")[\\s/>]","i"),va=/<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:-]+)[^>]*)\/>/gi,wa=/<script|<style|<link/i,xa=/checked\s*(?:[^=]|=\s*.checked.)/i,ya=/^true\/(.*)/,za=/^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g,Aa=ca(d),Ba=Aa.appendChild(d.createElement("div"));function Ca(a,b){return n.nodeName(a,"table")&&n.nodeName(11!==b.nodeType?b:b.firstChild,"tr")?a.getElementsByTagName("tbody")[0]||a.appendChild(a.ownerDocument.createElement("tbody")):a}function Da(a){return a.type=(null!==n.find.attr(a,"type"))+"/"+a.type,a}function Ea(a){var b=ya.exec(a.type);return b?a.type=b[1]:a.removeAttribute("type"),a}function Fa(a,b){if(1===b.nodeType&&n.hasData(a)){var c,d,e,f=n._data(a),g=n._data(b,f),h=f.events;if(h){delete g.handle,g.events={};for(c in h)for(d=0,e=h[c].length;e>d;d++)n.event.add(b,c,h[c][d])}g.data&&(g.data=n.extend({},g.data))}}function Ga(a,b){var c,d,e;if(1===b.nodeType){if(c=b.nodeName.toLowerCase(),!l.noCloneEvent&&b[n.expando]){e=n._data(b);for(d in e.events)n.removeEvent(b,d,e.handle);b.removeAttribute(n.expando)}"script"===c&&b.text!==a.text?(Da(b).text=a.text,Ea(b)):"object"===c?(b.parentNode&&(b.outerHTML=a.outerHTML),l.html5Clone&&a.innerHTML&&!n.trim(b.innerHTML)&&(b.innerHTML=a.innerHTML)):"input"===c&&Z.test(a.type)?(b.defaultChecked=b.checked=a.checked,b.value!==a.value&&(b.value=a.value)):"option"===c?b.defaultSelected=b.selected=a.defaultSelected:"input"!==c&&"textarea"!==c||(b.defaultValue=a.defaultValue)}}function Ha(a,b,c,d){b=f.apply([],b);var e,g,h,i,j,k,m=0,o=a.length,p=o-1,q=b[0],r=n.isFunction(q);if(r||o>1&&"string"==typeof q&&!l.checkClone&&xa.test(q))return a.each(function(e){var f=a.eq(e);r&&(b[0]=q.call(this,e,f.html())),Ha(f,b,c,d)});if(o&&(k=ja(b,a[0].ownerDocument,!1,a,d),e=k.firstChild,1===k.childNodes.length&&(k=e),e||d)){for(i=n.map(ea(k,"script"),Da),h=i.length;o>m;m++)g=k,m!==p&&(g=n.clone(g,!0,!0),h&&n.merge(i,ea(g,"script"))),c.call(a[m],g,m);if(h)for(j=i[i.length-1].ownerDocument,n.map(i,Ea),m=0;h>m;m++)g=i[m],_.test(g.type||"")&&!n._data(g,"globalEval")&&n.contains(j,g)&&(g.src?n._evalUrl&&n._evalUrl(g.src):n.globalEval((g.text||g.textContent||g.innerHTML||"").replace(za,"")));k=e=null}return a}function Ia(a,b,c){for(var d,e=b?n.filter(b,a):a,f=0;null!=(d=e[f]);f++)c||1!==d.nodeType||n.cleanData(ea(d)),d.parentNode&&(c&&n.contains(d.ownerDocument,d)&&fa(ea(d,"script")),d.parentNode.removeChild(d));return a}n.extend({htmlPrefilter:function(a){return a.replace(va,"<$1></$2>")},clone:function(a,b,c){var d,e,f,g,h,i=n.contains(a.ownerDocument,a);if(l.html5Clone||n.isXMLDoc(a)||!ua.test("<"+a.nodeName+">")?f=a.cloneNode(!0):(Ba.innerHTML=a.outerHTML,Ba.removeChild(f=Ba.firstChild)),!(l.noCloneEvent&&l.noCloneChecked||1!==a.nodeType&&11!==a.nodeType||n.isXMLDoc(a)))for(d=ea(f),h=ea(a),g=0;null!=(e=h[g]);++g)d[g]&&Ga(e,d[g]);if(b)if(c)for(h=h||ea(a),d=d||ea(f),g=0;null!=(e=h[g]);g++)Fa(e,d[g]);else Fa(a,f);return d=ea(f,"script"),d.length>0&&fa(d,!i&&ea(a,"script")),d=h=e=null,f},cleanData:function(a,b){for(var d,e,f,g,h=0,i=n.expando,j=n.cache,k=l.attributes,m=n.event.special;null!=(d=a[h]);h++)if((b||M(d))&&(f=d[i],g=f&&j[f])){if(g.events)for(e in g.events)m[e]?n.event.remove(d,e):n.removeEvent(d,e,g.handle);j[f]&&(delete j[f],k||"undefined"==typeof d.removeAttribute?d[i]=void 0:d.removeAttribute(i),c.push(f))}}}),n.fn.extend({domManip:Ha,detach:function(a){return Ia(this,a,!0)},remove:function(a){return Ia(this,a)},text:function(a){return Y(this,function(a){return void 0===a?n.text(this):this.empty().append((this[0]&&this[0].ownerDocument||d).createTextNode(a))},null,a,arguments.length)},append:function(){return Ha(this,arguments,function(a){if(1===this.nodeType||11===this.nodeType||9===this.nodeType){var b=Ca(this,a);b.appendChild(a)}})},prepend:function(){return Ha(this,arguments,function(a){if(1===this.nodeType||11===this.nodeType||9===this.nodeType){var b=Ca(this,a);b.insertBefore(a,b.firstChild)}})},before:function(){return Ha(this,arguments,function(a){this.parentNode&&this.parentNode.insertBefore(a,this)})},after:function(){return Ha(this,arguments,function(a){this.parentNode&&this.parentNode.insertBefore(a,this.nextSibling)})},empty:function(){for(var a,b=0;null!=(a=this[b]);b++){1===a.nodeType&&n.cleanData(ea(a,!1));while(a.firstChild)a.removeChild(a.firstChild);a.options&&n.nodeName(a,"select")&&(a.options.length=0)}return this},clone:function(a,b){return a=null==a?!1:a,b=null==b?a:b,this.map(function(){return n.clone(this,a,b)})},html:function(a){return Y(this,function(a){var b=this[0]||{},c=0,d=this.length;if(void 0===a)return 1===b.nodeType?b.innerHTML.replace(ta,""):void 0;if("string"==typeof a&&!wa.test(a)&&(l.htmlSerialize||!ua.test(a))&&(l.leadingWhitespace||!aa.test(a))&&!da[($.exec(a)||["",""])[1].toLowerCase()]){a=n.htmlPrefilter(a);try{for(;d>c;c++)b=this[c]||{},1===b.nodeType&&(n.cleanData(ea(b,!1)),b.innerHTML=a);b=0}catch(e){}}b&&this.empty().append(a)},null,a,arguments.length)},replaceWith:function(){var a=[];return Ha(this,arguments,function(b){var c=this.parentNode;n.inArray(this,a)<0&&(n.cleanData(ea(this)),c&&c.replaceChild(b,this))},a)}}),n.each({appendTo:"append",prependTo:"prepend",insertBefore:"before",insertAfter:"after",replaceAll:"replaceWith"},function(a,b){n.fn[a]=function(a){for(var c,d=0,e=[],f=n(a),h=f.length-1;h>=d;d++)c=d===h?this:this.clone(!0),n(f[d])[b](c),g.apply(e,c.get());return this.pushStack(e)}});var Ja,Ka={HTML:"block",BODY:"block"};function La(a,b){var c=n(b.createElement(a)).appendTo(b.body),d=n.css(c[0],"display");return c.detach(),d}function Ma(a){var b=d,c=Ka[a];return c||(c=La(a,b),"none"!==c&&c||(Ja=(Ja||n("<iframe frameborder='0' width='0' height='0'/>")).appendTo(b.documentElement),b=(Ja[0].contentWindow||Ja[0].contentDocument).document,b.write(),b.close(),c=La(a,b),Ja.detach()),Ka[a]=c),c}var Na=/^margin/,Oa=new RegExp("^("+T+")(?!px)[a-z%]+$","i"),Pa=function(a,b,c,d){var e,f,g={};for(f in b)g[f]=a.style[f],a.style[f]=b[f];e=c.apply(a,d||[]);for(f in b)a.style[f]=g[f];return e},Qa=d.documentElement;!function(){var b,c,e,f,g,h,i=d.createElement("div"),j=d.createElement("div");if(j.style){j.style.cssText="float:left;opacity:.5",l.opacity="0.5"===j.style.opacity,l.cssFloat=!!j.style.cssFloat,j.style.backgroundClip="content-box",j.cloneNode(!0).style.backgroundClip="",l.clearCloneStyle="content-box"===j.style.backgroundClip,i=d.createElement("div"),i.style.cssText="border:0;width:8px;height:0;top:0;left:-9999px;padding:0;margin-top:1px;position:absolute",j.innerHTML="",i.appendChild(j),l.boxSizing=""===j.style.boxSizing||""===j.style.MozBoxSizing||""===j.style.WebkitBoxSizing,n.extend(l,{reliableHiddenOffsets:function(){return null==b&&k(),f},boxSizingReliable:function(){return null==b&&k(),e},pixelMarginRight:function(){return null==b&&k(),c},pixelPosition:function(){return null==b&&k(),b},reliableMarginRight:function(){return null==b&&k(),g},reliableMarginLeft:function(){return null==b&&k(),h}});function k(){var k,l,m=d.documentElement;m.appendChild(i),j.style.cssText="-webkit-box-sizing:border-box;box-sizing:border-box;position:relative;display:block;margin:auto;border:1px;padding:1px;top:1%;width:50%",b=e=h=!1,c=g=!0,a.getComputedStyle&&(l=a.getComputedStyle(j),b="1%"!==(l||{}).top,h="2px"===(l||{}).marginLeft,e="4px"===(l||{width:"4px"}).width,j.style.marginRight="50%",c="4px"===(l||{marginRight:"4px"}).marginRight,k=j.appendChild(d.createElement("div")),k.style.cssText=j.style.cssText="-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:0",k.style.marginRight=k.style.width="0",j.style.width="1px",g=!parseFloat((a.getComputedStyle(k)||{}).marginRight),j.removeChild(k)),j.style.display="none",f=0===j.getClientRects().length,f&&(j.style.display="",j.innerHTML="<table><tr><td></td><td>t</td></tr></table>",j.childNodes[0].style.borderCollapse="separate",k=j.getElementsByTagName("td"),k[0].style.cssText="margin:0;border:0;padding:0;display:none",f=0===k[0].offsetHeight,f&&(k[0].style.display="",k[1].style.display="none",f=0===k[0].offsetHeight)),m.removeChild(i)}}}();var Ra,Sa,Ta=/^(top|right|bottom|left)$/;a.getComputedStyle?(Ra=function(b){var c=b.ownerDocument.defaultView;return c&&c.opener||(c=a),c.getComputedStyle(b)},Sa=function(a,b,c){var d,e,f,g,h=a.style;return c=c||Ra(a),g=c?c.getPropertyValue(b)||c[b]:void 0,""!==g&&void 0!==g||n.contains(a.ownerDocument,a)||(g=n.style(a,b)),c&&!l.pixelMarginRight()&&Oa.test(g)&&Na.test(b)&&(d=h.width,e=h.minWidth,f=h.maxWidth,h.minWidth=h.maxWidth=h.width=g,g=c.width,h.width=d,h.minWidth=e,h.maxWidth=f),void 0===g?g:g+""}):Qa.currentStyle&&(Ra=function(a){return a.currentStyle},Sa=function(a,b,c){var d,e,f,g,h=a.style;return c=c||Ra(a),g=c?c[b]:void 0,null==g&&h&&h[b]&&(g=h[b]),Oa.test(g)&&!Ta.test(b)&&(d=h.left,e=a.runtimeStyle,f=e&&e.left,f&&(e.left=a.currentStyle.left),h.left="fontSize"===b?"1em":g,g=h.pixelLeft+"px",h.left=d,f&&(e.left=f)),void 0===g?g:g+""||"auto"});function Ua(a,b){return{get:function(){return a()?void delete this.get:(this.get=b).apply(this,arguments)}}}var Va=/alpha\([^)]*\)/i,Wa=/opacity\s*=\s*([^)]*)/i,Xa=/^(none|table(?!-c[ea]).+)/,Ya=new RegExp("^("+T+")(.*)$","i"),Za={position:"absolute",visibility:"hidden",display:"block"},$a={letterSpacing:"0",fontWeight:"400"},_a=["Webkit","O","Moz","ms"],ab=d.createElement("div").style;function bb(a){if(a in ab)return a;var b=a.charAt(0).toUpperCase()+a.slice(1),c=_a.length;while(c--)if(a=_a[c]+b,a in ab)return a}function cb(a,b){for(var c,d,e,f=[],g=0,h=a.length;h>g;g++)d=a[g],d.style&&(f[g]=n._data(d,"olddisplay"),c=d.style.display,b?(f[g]||"none"!==c||(d.style.display=""),""===d.style.display&&W(d)&&(f[g]=n._data(d,"olddisplay",Ma(d.nodeName)))):(e=W(d),(c&&"none"!==c||!e)&&n._data(d,"olddisplay",e?c:n.css(d,"display"))));for(g=0;h>g;g++)d=a[g],d.style&&(b&&"none"!==d.style.display&&""!==d.style.display||(d.style.display=b?f[g]||"":"none"));return a}function db(a,b,c){var d=Ya.exec(b);return d?Math.max(0,d[1]-(c||0))+(d[2]||"px"):b}function eb(a,b,c,d,e){for(var f=c===(d?"border":"content")?4:"width"===b?1:0,g=0;4>f;f+=2)"margin"===c&&(g+=n.css(a,c+V[f],!0,e)),d?("content"===c&&(g-=n.css(a,"padding"+V[f],!0,e)),"margin"!==c&&(g-=n.css(a,"border"+V[f]+"Width",!0,e))):(g+=n.css(a,"padding"+V[f],!0,e),"padding"!==c&&(g+=n.css(a,"border"+V[f]+"Width",!0,e)));return g}function fb(a,b,c){var d=!0,e="width"===b?a.offsetWidth:a.offsetHeight,f=Ra(a),g=l.boxSizing&&"border-box"===n.css(a,"boxSizing",!1,f);if(0>=e||null==e){if(e=Sa(a,b,f),(0>e||null==e)&&(e=a.style[b]),Oa.test(e))return e;d=g&&(l.boxSizingReliable()||e===a.style[b]),e=parseFloat(e)||0}return e+eb(a,b,c||(g?"border":"content"),d,f)+"px"}n.extend({cssHooks:{opacity:{get:function(a,b){if(b){var c=Sa(a,"opacity");return""===c?"1":c}}}},cssNumber:{animationIterationCount:!0,columnCount:!0,fillOpacity:!0,flexGrow:!0,flexShrink:!0,fontWeight:!0,lineHeight:!0,opacity:!0,order:!0,orphans:!0,widows:!0,zIndex:!0,zoom:!0},cssProps:{"float":l.cssFloat?"cssFloat":"styleFloat"},style:function(a,b,c,d){if(a&&3!==a.nodeType&&8!==a.nodeType&&a.style){var e,f,g,h=n.camelCase(b),i=a.style;if(b=n.cssProps[h]||(n.cssProps[h]=bb(h)||h),g=n.cssHooks[b]||n.cssHooks[h],void 0===c)return g&&"get"in g&&void 0!==(e=g.get(a,!1,d))?e:i[b];if(f=typeof c,"string"===f&&(e=U.exec(c))&&e[1]&&(c=X(a,b,e),f="number"),null!=c&&c===c&&("number"===f&&(c+=e&&e[3]||(n.cssNumber[h]?"":"px")),l.clearCloneStyle||""!==c||0!==b.indexOf("background")||(i[b]="inherit"),!(g&&"set"in g&&void 0===(c=g.set(a,c,d)))))try{i[b]=c}catch(j){}}},css:function(a,b,c,d){var e,f,g,h=n.camelCase(b);return b=n.cssProps[h]||(n.cssProps[h]=bb(h)||h),g=n.cssHooks[b]||n.cssHooks[h],g&&"get"in g&&(f=g.get(a,!0,c)),void 0===f&&(f=Sa(a,b,d)),"normal"===f&&b in $a&&(f=$a[b]),""===c||c?(e=parseFloat(f),c===!0||isFinite(e)?e||0:f):f}}),n.each(["height","width"],function(a,b){n.cssHooks[b]={get:function(a,c,d){return c?Xa.test(n.css(a,"display"))&&0===a.offsetWidth?Pa(a,Za,function(){return fb(a,b,d)}):fb(a,b,d):void 0},set:function(a,c,d){var e=d&&Ra(a);return db(a,c,d?eb(a,b,d,l.boxSizing&&"border-box"===n.css(a,"boxSizing",!1,e),e):0)}}}),l.opacity||(n.cssHooks.opacity={get:function(a,b){return Wa.test((b&&a.currentStyle?a.currentStyle.filter:a.style.filter)||"")?.01*parseFloat(RegExp.$1)+"":b?"1":""},set:function(a,b){var c=a.style,d=a.currentStyle,e=n.isNumeric(b)?"alpha(opacity="+100*b+")":"",f=d&&d.filter||c.filter||"";c.zoom=1,(b>=1||""===b)&&""===n.trim(f.replace(Va,""))&&c.removeAttribute&&(c.removeAttribute("filter"),""===b||d&&!d.filter)||(c.filter=Va.test(f)?f.replace(Va,e):f+" "+e)}}),n.cssHooks.marginRight=Ua(l.reliableMarginRight,function(a,b){return b?Pa(a,{display:"inline-block"},Sa,[a,"marginRight"]):void 0}),n.cssHooks.marginLeft=Ua(l.reliableMarginLeft,function(a,b){return b?(parseFloat(Sa(a,"marginLeft"))||(n.contains(a.ownerDocument,a)?a.getBoundingClientRect().left-Pa(a,{
marginLeft:0},function(){return a.getBoundingClientRect().left}):0))+"px":void 0}),n.each({margin:"",padding:"",border:"Width"},function(a,b){n.cssHooks[a+b]={expand:function(c){for(var d=0,e={},f="string"==typeof c?c.split(" "):[c];4>d;d++)e[a+V[d]+b]=f[d]||f[d-2]||f[0];return e}},Na.test(a)||(n.cssHooks[a+b].set=db)}),n.fn.extend({css:function(a,b){return Y(this,function(a,b,c){var d,e,f={},g=0;if(n.isArray(b)){for(d=Ra(a),e=b.length;e>g;g++)f[b[g]]=n.css(a,b[g],!1,d);return f}return void 0!==c?n.style(a,b,c):n.css(a,b)},a,b,arguments.length>1)},show:function(){return cb(this,!0)},hide:function(){return cb(this)},toggle:function(a){return"boolean"==typeof a?a?this.show():this.hide():this.each(function(){W(this)?n(this).show():n(this).hide()})}});function gb(a,b,c,d,e){return new gb.prototype.init(a,b,c,d,e)}n.Tween=gb,gb.prototype={constructor:gb,init:function(a,b,c,d,e,f){this.elem=a,this.prop=c,this.easing=e||n.easing._default,this.options=b,this.start=this.now=this.cur(),this.end=d,this.unit=f||(n.cssNumber[c]?"":"px")},cur:function(){var a=gb.propHooks[this.prop];return a&&a.get?a.get(this):gb.propHooks._default.get(this)},run:function(a){var b,c=gb.propHooks[this.prop];return this.options.duration?this.pos=b=n.easing[this.easing](a,this.options.duration*a,0,1,this.options.duration):this.pos=b=a,this.now=(this.end-this.start)*b+this.start,this.options.step&&this.options.step.call(this.elem,this.now,this),c&&c.set?c.set(this):gb.propHooks._default.set(this),this}},gb.prototype.init.prototype=gb.prototype,gb.propHooks={_default:{get:function(a){var b;return 1!==a.elem.nodeType||null!=a.elem[a.prop]&&null==a.elem.style[a.prop]?a.elem[a.prop]:(b=n.css(a.elem,a.prop,""),b&&"auto"!==b?b:0)},set:function(a){n.fx.step[a.prop]?n.fx.step[a.prop](a):1!==a.elem.nodeType||null==a.elem.style[n.cssProps[a.prop]]&&!n.cssHooks[a.prop]?a.elem[a.prop]=a.now:n.style(a.elem,a.prop,a.now+a.unit)}}},gb.propHooks.scrollTop=gb.propHooks.scrollLeft={set:function(a){a.elem.nodeType&&a.elem.parentNode&&(a.elem[a.prop]=a.now)}},n.easing={linear:function(a){return a},swing:function(a){return.5-Math.cos(a*Math.PI)/2},_default:"swing"},n.fx=gb.prototype.init,n.fx.step={};var hb,ib,jb=/^(?:toggle|show|hide)$/,kb=/queueHooks$/;function lb(){return a.setTimeout(function(){hb=void 0}),hb=n.now()}function mb(a,b){var c,d={height:a},e=0;for(b=b?1:0;4>e;e+=2-b)c=V[e],d["margin"+c]=d["padding"+c]=a;return b&&(d.opacity=d.width=a),d}function nb(a,b,c){for(var d,e=(qb.tweeners[b]||[]).concat(qb.tweeners["*"]),f=0,g=e.length;g>f;f++)if(d=e[f].call(c,b,a))return d}function ob(a,b,c){var d,e,f,g,h,i,j,k,m=this,o={},p=a.style,q=a.nodeType&&W(a),r=n._data(a,"fxshow");c.queue||(h=n._queueHooks(a,"fx"),null==h.unqueued&&(h.unqueued=0,i=h.empty.fire,h.empty.fire=function(){h.unqueued||i()}),h.unqueued++,m.always(function(){m.always(function(){h.unqueued--,n.queue(a,"fx").length||h.empty.fire()})})),1===a.nodeType&&("height"in b||"width"in b)&&(c.overflow=[p.overflow,p.overflowX,p.overflowY],j=n.css(a,"display"),k="none"===j?n._data(a,"olddisplay")||Ma(a.nodeName):j,"inline"===k&&"none"===n.css(a,"float")&&(l.inlineBlockNeedsLayout&&"inline"!==Ma(a.nodeName)?p.zoom=1:p.display="inline-block")),c.overflow&&(p.overflow="hidden",l.shrinkWrapBlocks()||m.always(function(){p.overflow=c.overflow[0],p.overflowX=c.overflow[1],p.overflowY=c.overflow[2]}));for(d in b)if(e=b[d],jb.exec(e)){if(delete b[d],f=f||"toggle"===e,e===(q?"hide":"show")){if("show"!==e||!r||void 0===r[d])continue;q=!0}o[d]=r&&r[d]||n.style(a,d)}else j=void 0;if(n.isEmptyObject(o))"inline"===("none"===j?Ma(a.nodeName):j)&&(p.display=j);else{r?"hidden"in r&&(q=r.hidden):r=n._data(a,"fxshow",{}),f&&(r.hidden=!q),q?n(a).show():m.done(function(){n(a).hide()}),m.done(function(){var b;n._removeData(a,"fxshow");for(b in o)n.style(a,b,o[b])});for(d in o)g=nb(q?r[d]:0,d,m),d in r||(r[d]=g.start,q&&(g.end=g.start,g.start="width"===d||"height"===d?1:0))}}function pb(a,b){var c,d,e,f,g;for(c in a)if(d=n.camelCase(c),e=b[d],f=a[c],n.isArray(f)&&(e=f[1],f=a[c]=f[0]),c!==d&&(a[d]=f,delete a[c]),g=n.cssHooks[d],g&&"expand"in g){f=g.expand(f),delete a[d];for(c in f)c in a||(a[c]=f[c],b[c]=e)}else b[d]=e}function qb(a,b,c){var d,e,f=0,g=qb.prefilters.length,h=n.Deferred().always(function(){delete i.elem}),i=function(){if(e)return!1;for(var b=hb||lb(),c=Math.max(0,j.startTime+j.duration-b),d=c/j.duration||0,f=1-d,g=0,i=j.tweens.length;i>g;g++)j.tweens[g].run(f);return h.notifyWith(a,[j,f,c]),1>f&&i?c:(h.resolveWith(a,[j]),!1)},j=h.promise({elem:a,props:n.extend({},b),opts:n.extend(!0,{specialEasing:{},easing:n.easing._default},c),originalProperties:b,originalOptions:c,startTime:hb||lb(),duration:c.duration,tweens:[],createTween:function(b,c){var d=n.Tween(a,j.opts,b,c,j.opts.specialEasing[b]||j.opts.easing);return j.tweens.push(d),d},stop:function(b){var c=0,d=b?j.tweens.length:0;if(e)return this;for(e=!0;d>c;c++)j.tweens[c].run(1);return b?(h.notifyWith(a,[j,1,0]),h.resolveWith(a,[j,b])):h.rejectWith(a,[j,b]),this}}),k=j.props;for(pb(k,j.opts.specialEasing);g>f;f++)if(d=qb.prefilters[f].call(j,a,k,j.opts))return n.isFunction(d.stop)&&(n._queueHooks(j.elem,j.opts.queue).stop=n.proxy(d.stop,d)),d;return n.map(k,nb,j),n.isFunction(j.opts.start)&&j.opts.start.call(a,j),n.fx.timer(n.extend(i,{elem:a,anim:j,queue:j.opts.queue})),j.progress(j.opts.progress).done(j.opts.done,j.opts.complete).fail(j.opts.fail).always(j.opts.always)}n.Animation=n.extend(qb,{tweeners:{"*":[function(a,b){var c=this.createTween(a,b);return X(c.elem,a,U.exec(b),c),c}]},tweener:function(a,b){n.isFunction(a)?(b=a,a=["*"]):a=a.match(G);for(var c,d=0,e=a.length;e>d;d++)c=a[d],qb.tweeners[c]=qb.tweeners[c]||[],qb.tweeners[c].unshift(b)},prefilters:[ob],prefilter:function(a,b){b?qb.prefilters.unshift(a):qb.prefilters.push(a)}}),n.speed=function(a,b,c){var d=a&&"object"==typeof a?n.extend({},a):{complete:c||!c&&b||n.isFunction(a)&&a,duration:a,easing:c&&b||b&&!n.isFunction(b)&&b};return d.duration=n.fx.off?0:"number"==typeof d.duration?d.duration:d.duration in n.fx.speeds?n.fx.speeds[d.duration]:n.fx.speeds._default,null!=d.queue&&d.queue!==!0||(d.queue="fx"),d.old=d.complete,d.complete=function(){n.isFunction(d.old)&&d.old.call(this),d.queue&&n.dequeue(this,d.queue)},d},n.fn.extend({fadeTo:function(a,b,c,d){return this.filter(W).css("opacity",0).show().end().animate({opacity:b},a,c,d)},animate:function(a,b,c,d){var e=n.isEmptyObject(a),f=n.speed(b,c,d),g=function(){var b=qb(this,n.extend({},a),f);(e||n._data(this,"finish"))&&b.stop(!0)};return g.finish=g,e||f.queue===!1?this.each(g):this.queue(f.queue,g)},stop:function(a,b,c){var d=function(a){var b=a.stop;delete a.stop,b(c)};return"string"!=typeof a&&(c=b,b=a,a=void 0),b&&a!==!1&&this.queue(a||"fx",[]),this.each(function(){var b=!0,e=null!=a&&a+"queueHooks",f=n.timers,g=n._data(this);if(e)g[e]&&g[e].stop&&d(g[e]);else for(e in g)g[e]&&g[e].stop&&kb.test(e)&&d(g[e]);for(e=f.length;e--;)f[e].elem!==this||null!=a&&f[e].queue!==a||(f[e].anim.stop(c),b=!1,f.splice(e,1));!b&&c||n.dequeue(this,a)})},finish:function(a){return a!==!1&&(a=a||"fx"),this.each(function(){var b,c=n._data(this),d=c[a+"queue"],e=c[a+"queueHooks"],f=n.timers,g=d?d.length:0;for(c.finish=!0,n.queue(this,a,[]),e&&e.stop&&e.stop.call(this,!0),b=f.length;b--;)f[b].elem===this&&f[b].queue===a&&(f[b].anim.stop(!0),f.splice(b,1));for(b=0;g>b;b++)d[b]&&d[b].finish&&d[b].finish.call(this);delete c.finish})}}),n.each(["toggle","show","hide"],function(a,b){var c=n.fn[b];n.fn[b]=function(a,d,e){return null==a||"boolean"==typeof a?c.apply(this,arguments):this.animate(mb(b,!0),a,d,e)}}),n.each({slideDown:mb("show"),slideUp:mb("hide"),slideToggle:mb("toggle"),fadeIn:{opacity:"show"},fadeOut:{opacity:"hide"},fadeToggle:{opacity:"toggle"}},function(a,b){n.fn[a]=function(a,c,d){return this.animate(b,a,c,d)}}),n.timers=[],n.fx.tick=function(){var a,b=n.timers,c=0;for(hb=n.now();c<b.length;c++)a=b[c],a()||b[c]!==a||b.splice(c--,1);b.length||n.fx.stop(),hb=void 0},n.fx.timer=function(a){n.timers.push(a),a()?n.fx.start():n.timers.pop()},n.fx.interval=13,n.fx.start=function(){ib||(ib=a.setInterval(n.fx.tick,n.fx.interval))},n.fx.stop=function(){a.clearInterval(ib),ib=null},n.fx.speeds={slow:600,fast:200,_default:400},n.fn.delay=function(b,c){return b=n.fx?n.fx.speeds[b]||b:b,c=c||"fx",this.queue(c,function(c,d){var e=a.setTimeout(c,b);d.stop=function(){a.clearTimeout(e)}})},function(){var a,b=d.createElement("input"),c=d.createElement("div"),e=d.createElement("select"),f=e.appendChild(d.createElement("option"));c=d.createElement("div"),c.setAttribute("className","t"),c.innerHTML="  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>",a=c.getElementsByTagName("a")[0],b.setAttribute("type","checkbox"),c.appendChild(b),a=c.getElementsByTagName("a")[0],a.style.cssText="top:1px",l.getSetAttribute="t"!==c.className,l.style=/top/.test(a.getAttribute("style")),l.hrefNormalized="/a"===a.getAttribute("href"),l.checkOn=!!b.value,l.optSelected=f.selected,l.enctype=!!d.createElement("form").enctype,e.disabled=!0,l.optDisabled=!f.disabled,b=d.createElement("input"),b.setAttribute("value",""),l.input=""===b.getAttribute("value"),b.value="t",b.setAttribute("type","radio"),l.radioValue="t"===b.value}();var rb=/\r/g,sb=/[\x20\t\r\n\f]+/g;n.fn.extend({val:function(a){var b,c,d,e=this[0];{if(arguments.length)return d=n.isFunction(a),this.each(function(c){var e;1===this.nodeType&&(e=d?a.call(this,c,n(this).val()):a,null==e?e="":"number"==typeof e?e+="":n.isArray(e)&&(e=n.map(e,function(a){return null==a?"":a+""})),b=n.valHooks[this.type]||n.valHooks[this.nodeName.toLowerCase()],b&&"set"in b&&void 0!==b.set(this,e,"value")||(this.value=e))});if(e)return b=n.valHooks[e.type]||n.valHooks[e.nodeName.toLowerCase()],b&&"get"in b&&void 0!==(c=b.get(e,"value"))?c:(c=e.value,"string"==typeof c?c.replace(rb,""):null==c?"":c)}}}),n.extend({valHooks:{option:{get:function(a){var b=n.find.attr(a,"value");return null!=b?b:n.trim(n.text(a)).replace(sb," ")}},select:{get:function(a){for(var b,c,d=a.options,e=a.selectedIndex,f="select-one"===a.type||0>e,g=f?null:[],h=f?e+1:d.length,i=0>e?h:f?e:0;h>i;i++)if(c=d[i],(c.selected||i===e)&&(l.optDisabled?!c.disabled:null===c.getAttribute("disabled"))&&(!c.parentNode.disabled||!n.nodeName(c.parentNode,"optgroup"))){if(b=n(c).val(),f)return b;g.push(b)}return g},set:function(a,b){var c,d,e=a.options,f=n.makeArray(b),g=e.length;while(g--)if(d=e[g],n.inArray(n.valHooks.option.get(d),f)>-1)try{d.selected=c=!0}catch(h){d.scrollHeight}else d.selected=!1;return c||(a.selectedIndex=-1),e}}}}),n.each(["radio","checkbox"],function(){n.valHooks[this]={set:function(a,b){return n.isArray(b)?a.checked=n.inArray(n(a).val(),b)>-1:void 0}},l.checkOn||(n.valHooks[this].get=function(a){return null===a.getAttribute("value")?"on":a.value})});var tb,ub,vb=n.expr.attrHandle,wb=/^(?:checked|selected)$/i,xb=l.getSetAttribute,yb=l.input;n.fn.extend({attr:function(a,b){return Y(this,n.attr,a,b,arguments.length>1)},removeAttr:function(a){return this.each(function(){n.removeAttr(this,a)})}}),n.extend({attr:function(a,b,c){var d,e,f=a.nodeType;if(3!==f&&8!==f&&2!==f)return"undefined"==typeof a.getAttribute?n.prop(a,b,c):(1===f&&n.isXMLDoc(a)||(b=b.toLowerCase(),e=n.attrHooks[b]||(n.expr.match.bool.test(b)?ub:tb)),void 0!==c?null===c?void n.removeAttr(a,b):e&&"set"in e&&void 0!==(d=e.set(a,c,b))?d:(a.setAttribute(b,c+""),c):e&&"get"in e&&null!==(d=e.get(a,b))?d:(d=n.find.attr(a,b),null==d?void 0:d))},attrHooks:{type:{set:function(a,b){if(!l.radioValue&&"radio"===b&&n.nodeName(a,"input")){var c=a.value;return a.setAttribute("type",b),c&&(a.value=c),b}}}},removeAttr:function(a,b){var c,d,e=0,f=b&&b.match(G);if(f&&1===a.nodeType)while(c=f[e++])d=n.propFix[c]||c,n.expr.match.bool.test(c)?yb&&xb||!wb.test(c)?a[d]=!1:a[n.camelCase("default-"+c)]=a[d]=!1:n.attr(a,c,""),a.removeAttribute(xb?c:d)}}),ub={set:function(a,b,c){return b===!1?n.removeAttr(a,c):yb&&xb||!wb.test(c)?a.setAttribute(!xb&&n.propFix[c]||c,c):a[n.camelCase("default-"+c)]=a[c]=!0,c}},n.each(n.expr.match.bool.source.match(/\w+/g),function(a,b){var c=vb[b]||n.find.attr;yb&&xb||!wb.test(b)?vb[b]=function(a,b,d){var e,f;return d||(f=vb[b],vb[b]=e,e=null!=c(a,b,d)?b.toLowerCase():null,vb[b]=f),e}:vb[b]=function(a,b,c){return c?void 0:a[n.camelCase("default-"+b)]?b.toLowerCase():null}}),yb&&xb||(n.attrHooks.value={set:function(a,b,c){return n.nodeName(a,"input")?void(a.defaultValue=b):tb&&tb.set(a,b,c)}}),xb||(tb={set:function(a,b,c){var d=a.getAttributeNode(c);return d||a.setAttributeNode(d=a.ownerDocument.createAttribute(c)),d.value=b+="","value"===c||b===a.getAttribute(c)?b:void 0}},vb.id=vb.name=vb.coords=function(a,b,c){var d;return c?void 0:(d=a.getAttributeNode(b))&&""!==d.value?d.value:null},n.valHooks.button={get:function(a,b){var c=a.getAttributeNode(b);return c&&c.specified?c.value:void 0},set:tb.set},n.attrHooks.contenteditable={set:function(a,b,c){tb.set(a,""===b?!1:b,c)}},n.each(["width","height"],function(a,b){n.attrHooks[b]={set:function(a,c){return""===c?(a.setAttribute(b,"auto"),c):void 0}}})),l.style||(n.attrHooks.style={get:function(a){return a.style.cssText||void 0},set:function(a,b){return a.style.cssText=b+""}});var zb=/^(?:input|select|textarea|button|object)$/i,Ab=/^(?:a|area)$/i;n.fn.extend({prop:function(a,b){return Y(this,n.prop,a,b,arguments.length>1)},removeProp:function(a){return a=n.propFix[a]||a,this.each(function(){try{this[a]=void 0,delete this[a]}catch(b){}})}}),n.extend({prop:function(a,b,c){var d,e,f=a.nodeType;if(3!==f&&8!==f&&2!==f)return 1===f&&n.isXMLDoc(a)||(b=n.propFix[b]||b,e=n.propHooks[b]),void 0!==c?e&&"set"in e&&void 0!==(d=e.set(a,c,b))?d:a[b]=c:e&&"get"in e&&null!==(d=e.get(a,b))?d:a[b]},propHooks:{tabIndex:{get:function(a){var b=n.find.attr(a,"tabindex");return b?parseInt(b,10):zb.test(a.nodeName)||Ab.test(a.nodeName)&&a.href?0:-1}}},propFix:{"for":"htmlFor","class":"className"}}),l.hrefNormalized||n.each(["href","src"],function(a,b){n.propHooks[b]={get:function(a){return a.getAttribute(b,4)}}}),l.optSelected||(n.propHooks.selected={get:function(a){var b=a.parentNode;return b&&(b.selectedIndex,b.parentNode&&b.parentNode.selectedIndex),null},set:function(a){var b=a.parentNode;b&&(b.selectedIndex,b.parentNode&&b.parentNode.selectedIndex)}}),n.each(["tabIndex","readOnly","maxLength","cellSpacing","cellPadding","rowSpan","colSpan","useMap","frameBorder","contentEditable"],function(){n.propFix[this.toLowerCase()]=this}),l.enctype||(n.propFix.enctype="encoding");var Bb=/[\t\r\n\f]/g;function Cb(a){return n.attr(a,"class")||""}n.fn.extend({addClass:function(a){var b,c,d,e,f,g,h,i=0;if(n.isFunction(a))return this.each(function(b){n(this).addClass(a.call(this,b,Cb(this)))});if("string"==typeof a&&a){b=a.match(G)||[];while(c=this[i++])if(e=Cb(c),d=1===c.nodeType&&(" "+e+" ").replace(Bb," ")){g=0;while(f=b[g++])d.indexOf(" "+f+" ")<0&&(d+=f+" ");h=n.trim(d),e!==h&&n.attr(c,"class",h)}}return this},removeClass:function(a){var b,c,d,e,f,g,h,i=0;if(n.isFunction(a))return this.each(function(b){n(this).removeClass(a.call(this,b,Cb(this)))});if(!arguments.length)return this.attr("class","");if("string"==typeof a&&a){b=a.match(G)||[];while(c=this[i++])if(e=Cb(c),d=1===c.nodeType&&(" "+e+" ").replace(Bb," ")){g=0;while(f=b[g++])while(d.indexOf(" "+f+" ")>-1)d=d.replace(" "+f+" "," ");h=n.trim(d),e!==h&&n.attr(c,"class",h)}}return this},toggleClass:function(a,b){var c=typeof a;return"boolean"==typeof b&&"string"===c?b?this.addClass(a):this.removeClass(a):n.isFunction(a)?this.each(function(c){n(this).toggleClass(a.call(this,c,Cb(this),b),b)}):this.each(function(){var b,d,e,f;if("string"===c){d=0,e=n(this),f=a.match(G)||[];while(b=f[d++])e.hasClass(b)?e.removeClass(b):e.addClass(b)}else void 0!==a&&"boolean"!==c||(b=Cb(this),b&&n._data(this,"__className__",b),n.attr(this,"class",b||a===!1?"":n._data(this,"__className__")||""))})},hasClass:function(a){var b,c,d=0;b=" "+a+" ";while(c=this[d++])if(1===c.nodeType&&(" "+Cb(c)+" ").replace(Bb," ").indexOf(b)>-1)return!0;return!1}}),n.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(" "),function(a,b){n.fn[b]=function(a,c){return arguments.length>0?this.on(b,null,a,c):this.trigger(b)}}),n.fn.extend({hover:function(a,b){return this.mouseenter(a).mouseleave(b||a)}});var Db=a.location,Eb=n.now(),Fb=/\?/,Gb=/(,)|(\[|{)|(}|])|"(?:[^"\\\r\n]|\\["\\\/bfnrt]|\\u[\da-fA-F]{4})*"\s*:?|true|false|null|-?(?!0\d)\d+(?:\.\d+|)(?:[eE][+-]?\d+|)/g;n.parseJSON=function(b){if(a.JSON&&a.JSON.parse)return a.JSON.parse(b+"");var c,d=null,e=n.trim(b+"");return e&&!n.trim(e.replace(Gb,function(a,b,e,f){return c&&b&&(d=0),0===d?a:(c=e||b,d+=!f-!e,"")}))?Function("return "+e)():n.error("Invalid JSON: "+b)},n.parseXML=function(b){var c,d;if(!b||"string"!=typeof b)return null;try{a.DOMParser?(d=new a.DOMParser,c=d.parseFromString(b,"text/xml")):(c=new a.ActiveXObject("Microsoft.XMLDOM"),c.async="false",c.loadXML(b))}catch(e){c=void 0}return c&&c.documentElement&&!c.getElementsByTagName("parsererror").length||n.error("Invalid XML: "+b),c};var Hb=/#.*$/,Ib=/([?&])_=[^&]*/,Jb=/^(.*?):[ \t]*([^\r\n]*)\r?$/gm,Kb=/^(?:about|app|app-storage|.+-extension|file|res|widget):$/,Lb=/^(?:GET|HEAD)$/,Mb=/^\/\//,Nb=/^([\w.+-]+:)(?:\/\/(?:[^\/?#]*@|)([^\/?#:]*)(?::(\d+)|)|)/,Ob={},Pb={},Qb="*/".concat("*"),Rb=Db.href,Sb=Nb.exec(Rb.toLowerCase())||[];function Tb(a){return function(b,c){"string"!=typeof b&&(c=b,b="*");var d,e=0,f=b.toLowerCase().match(G)||[];if(n.isFunction(c))while(d=f[e++])"+"===d.charAt(0)?(d=d.slice(1)||"*",(a[d]=a[d]||[]).unshift(c)):(a[d]=a[d]||[]).push(c)}}function Ub(a,b,c,d){var e={},f=a===Pb;function g(h){var i;return e[h]=!0,n.each(a[h]||[],function(a,h){var j=h(b,c,d);return"string"!=typeof j||f||e[j]?f?!(i=j):void 0:(b.dataTypes.unshift(j),g(j),!1)}),i}return g(b.dataTypes[0])||!e["*"]&&g("*")}function Vb(a,b){var c,d,e=n.ajaxSettings.flatOptions||{};for(d in b)void 0!==b[d]&&((e[d]?a:c||(c={}))[d]=b[d]);return c&&n.extend(!0,a,c),a}function Wb(a,b,c){var d,e,f,g,h=a.contents,i=a.dataTypes;while("*"===i[0])i.shift(),void 0===e&&(e=a.mimeType||b.getResponseHeader("Content-Type"));if(e)for(g in h)if(h[g]&&h[g].test(e)){i.unshift(g);break}if(i[0]in c)f=i[0];else{for(g in c){if(!i[0]||a.converters[g+" "+i[0]]){f=g;break}d||(d=g)}f=f||d}return f?(f!==i[0]&&i.unshift(f),c[f]):void 0}function Xb(a,b,c,d){var e,f,g,h,i,j={},k=a.dataTypes.slice();if(k[1])for(g in a.converters)j[g.toLowerCase()]=a.converters[g];f=k.shift();while(f)if(a.responseFields[f]&&(c[a.responseFields[f]]=b),!i&&d&&a.dataFilter&&(b=a.dataFilter(b,a.dataType)),i=f,f=k.shift())if("*"===f)f=i;else if("*"!==i&&i!==f){if(g=j[i+" "+f]||j["* "+f],!g)for(e in j)if(h=e.split(" "),h[1]===f&&(g=j[i+" "+h[0]]||j["* "+h[0]])){g===!0?g=j[e]:j[e]!==!0&&(f=h[0],k.unshift(h[1]));break}if(g!==!0)if(g&&a["throws"])b=g(b);else try{b=g(b)}catch(l){return{state:"parsererror",error:g?l:"No conversion from "+i+" to "+f}}}return{state:"success",data:b}}n.extend({active:0,lastModified:{},etag:{},ajaxSettings:{url:Rb,type:"GET",isLocal:Kb.test(Sb[1]),global:!0,processData:!0,async:!0,contentType:"application/x-www-form-urlencoded; charset=UTF-8",accepts:{"*":Qb,text:"text/plain",html:"text/html",xml:"application/xml, text/xml",json:"application/json, text/javascript"},contents:{xml:/\bxml\b/,html:/\bhtml/,json:/\bjson\b/},responseFields:{xml:"responseXML",text:"responseText",json:"responseJSON"},converters:{"* text":String,"text html":!0,"text json":n.parseJSON,"text xml":n.parseXML},flatOptions:{url:!0,context:!0}},ajaxSetup:function(a,b){return b?Vb(Vb(a,n.ajaxSettings),b):Vb(n.ajaxSettings,a)},ajaxPrefilter:Tb(Ob),ajaxTransport:Tb(Pb),ajax:function(b,c){"object"==typeof b&&(c=b,b=void 0),c=c||{};var d,e,f,g,h,i,j,k,l=n.ajaxSetup({},c),m=l.context||l,o=l.context&&(m.nodeType||m.jquery)?n(m):n.event,p=n.Deferred(),q=n.Callbacks("once memory"),r=l.statusCode||{},s={},t={},u=0,v="canceled",w={readyState:0,getResponseHeader:function(a){var b;if(2===u){if(!k){k={};while(b=Jb.exec(g))k[b[1].toLowerCase()]=b[2]}b=k[a.toLowerCase()]}return null==b?null:b},getAllResponseHeaders:function(){return 2===u?g:null},setRequestHeader:function(a,b){var c=a.toLowerCase();return u||(a=t[c]=t[c]||a,s[a]=b),this},overrideMimeType:function(a){return u||(l.mimeType=a),this},statusCode:function(a){var b;if(a)if(2>u)for(b in a)r[b]=[r[b],a[b]];else w.always(a[w.status]);return this},abort:function(a){var b=a||v;return j&&j.abort(b),y(0,b),this}};if(p.promise(w).complete=q.add,w.success=w.done,w.error=w.fail,l.url=((b||l.url||Rb)+"").replace(Hb,"").replace(Mb,Sb[1]+"//"),l.type=c.method||c.type||l.method||l.type,l.dataTypes=n.trim(l.dataType||"*").toLowerCase().match(G)||[""],null==l.crossDomain&&(d=Nb.exec(l.url.toLowerCase()),l.crossDomain=!(!d||d[1]===Sb[1]&&d[2]===Sb[2]&&(d[3]||("http:"===d[1]?"80":"443"))===(Sb[3]||("http:"===Sb[1]?"80":"443")))),l.data&&l.processData&&"string"!=typeof l.data&&(l.data=n.param(l.data,l.traditional)),Ub(Ob,l,c,w),2===u)return w;i=n.event&&l.global,i&&0===n.active++&&n.event.trigger("ajaxStart"),l.type=l.type.toUpperCase(),l.hasContent=!Lb.test(l.type),f=l.url,l.hasContent||(l.data&&(f=l.url+=(Fb.test(f)?"&":"?")+l.data,delete l.data),l.cache===!1&&(l.url=Ib.test(f)?f.replace(Ib,"$1_="+Eb++):f+(Fb.test(f)?"&":"?")+"_="+Eb++)),l.ifModified&&(n.lastModified[f]&&w.setRequestHeader("If-Modified-Since",n.lastModified[f]),n.etag[f]&&w.setRequestHeader("If-None-Match",n.etag[f])),(l.data&&l.hasContent&&l.contentType!==!1||c.contentType)&&w.setRequestHeader("Content-Type",l.contentType),w.setRequestHeader("Accept",l.dataTypes[0]&&l.accepts[l.dataTypes[0]]?l.accepts[l.dataTypes[0]]+("*"!==l.dataTypes[0]?", "+Qb+"; q=0.01":""):l.accepts["*"]);for(e in l.headers)w.setRequestHeader(e,l.headers[e]);if(l.beforeSend&&(l.beforeSend.call(m,w,l)===!1||2===u))return w.abort();v="abort";for(e in{success:1,error:1,complete:1})w[e](l[e]);if(j=Ub(Pb,l,c,w)){if(w.readyState=1,i&&o.trigger("ajaxSend",[w,l]),2===u)return w;l.async&&l.timeout>0&&(h=a.setTimeout(function(){w.abort("timeout")},l.timeout));try{u=1,j.send(s,y)}catch(x){if(!(2>u))throw x;y(-1,x)}}else y(-1,"No Transport");function y(b,c,d,e){var k,s,t,v,x,y=c;2!==u&&(u=2,h&&a.clearTimeout(h),j=void 0,g=e||"",w.readyState=b>0?4:0,k=b>=200&&300>b||304===b,d&&(v=Wb(l,w,d)),v=Xb(l,v,w,k),k?(l.ifModified&&(x=w.getResponseHeader("Last-Modified"),x&&(n.lastModified[f]=x),x=w.getResponseHeader("etag"),x&&(n.etag[f]=x)),204===b||"HEAD"===l.type?y="nocontent":304===b?y="notmodified":(y=v.state,s=v.data,t=v.error,k=!t)):(t=y,!b&&y||(y="error",0>b&&(b=0))),w.status=b,w.statusText=(c||y)+"",k?p.resolveWith(m,[s,y,w]):p.rejectWith(m,[w,y,t]),w.statusCode(r),r=void 0,i&&o.trigger(k?"ajaxSuccess":"ajaxError",[w,l,k?s:t]),q.fireWith(m,[w,y]),i&&(o.trigger("ajaxComplete",[w,l]),--n.active||n.event.trigger("ajaxStop")))}return w},getJSON:function(a,b,c){return n.get(a,b,c,"json")},getScript:function(a,b){return n.get(a,void 0,b,"script")}}),n.each(["get","post"],function(a,b){n[b]=function(a,c,d,e){return n.isFunction(c)&&(e=e||d,d=c,c=void 0),n.ajax(n.extend({url:a,type:b,dataType:e,data:c,success:d},n.isPlainObject(a)&&a))}}),n._evalUrl=function(a){return n.ajax({url:a,type:"GET",dataType:"script",cache:!0,async:!1,global:!1,"throws":!0})},n.fn.extend({wrapAll:function(a){if(n.isFunction(a))return this.each(function(b){n(this).wrapAll(a.call(this,b))});if(this[0]){var b=n(a,this[0].ownerDocument).eq(0).clone(!0);this[0].parentNode&&b.insertBefore(this[0]),b.map(function(){var a=this;while(a.firstChild&&1===a.firstChild.nodeType)a=a.firstChild;return a}).append(this)}return this},wrapInner:function(a){return n.isFunction(a)?this.each(function(b){n(this).wrapInner(a.call(this,b))}):this.each(function(){var b=n(this),c=b.contents();c.length?c.wrapAll(a):b.append(a)})},wrap:function(a){var b=n.isFunction(a);return this.each(function(c){n(this).wrapAll(b?a.call(this,c):a)})},unwrap:function(){return this.parent().each(function(){n.nodeName(this,"body")||n(this).replaceWith(this.childNodes)}).end()}});function Yb(a){return a.style&&a.style.display||n.css(a,"display")}function Zb(a){if(!n.contains(a.ownerDocument||d,a))return!0;while(a&&1===a.nodeType){if("none"===Yb(a)||"hidden"===a.type)return!0;a=a.parentNode}return!1}n.expr.filters.hidden=function(a){return l.reliableHiddenOffsets()?a.offsetWidth<=0&&a.offsetHeight<=0&&!a.getClientRects().length:Zb(a)},n.expr.filters.visible=function(a){return!n.expr.filters.hidden(a)};var $b=/%20/g,_b=/\[\]$/,ac=/\r?\n/g,bc=/^(?:submit|button|image|reset|file)$/i,cc=/^(?:input|select|textarea|keygen)/i;function dc(a,b,c,d){var e;if(n.isArray(b))n.each(b,function(b,e){c||_b.test(a)?d(a,e):dc(a+"["+("object"==typeof e&&null!=e?b:"")+"]",e,c,d)});else if(c||"object"!==n.type(b))d(a,b);else for(e in b)dc(a+"["+e+"]",b[e],c,d)}n.param=function(a,b){var c,d=[],e=function(a,b){b=n.isFunction(b)?b():null==b?"":b,d[d.length]=encodeURIComponent(a)+"="+encodeURIComponent(b)};if(void 0===b&&(b=n.ajaxSettings&&n.ajaxSettings.traditional),n.isArray(a)||a.jquery&&!n.isPlainObject(a))n.each(a,function(){e(this.name,this.value)});else for(c in a)dc(c,a[c],b,e);return d.join("&").replace($b,"+")},n.fn.extend({serialize:function(){return n.param(this.serializeArray())},serializeArray:function(){return this.map(function(){var a=n.prop(this,"elements");return a?n.makeArray(a):this}).filter(function(){var a=this.type;return this.name&&!n(this).is(":disabled")&&cc.test(this.nodeName)&&!bc.test(a)&&(this.checked||!Z.test(a))}).map(function(a,b){var c=n(this).val();return null==c?null:n.isArray(c)?n.map(c,function(a){return{name:b.name,value:a.replace(ac,"\r\n")}}):{name:b.name,value:c.replace(ac,"\r\n")}}).get()}}),n.ajaxSettings.xhr=void 0!==a.ActiveXObject?function(){return this.isLocal?ic():d.documentMode>8?hc():/^(get|post|head|put|delete|options)$/i.test(this.type)&&hc()||ic()}:hc;var ec=0,fc={},gc=n.ajaxSettings.xhr();a.attachEvent&&a.attachEvent("onunload",function(){for(var a in fc)fc[a](void 0,!0)}),l.cors=!!gc&&"withCredentials"in gc,gc=l.ajax=!!gc,gc&&n.ajaxTransport(function(b){if(!b.crossDomain||l.cors){var c;return{send:function(d,e){var f,g=b.xhr(),h=++ec;if(g.open(b.type,b.url,b.async,b.username,b.password),b.xhrFields)for(f in b.xhrFields)g[f]=b.xhrFields[f];b.mimeType&&g.overrideMimeType&&g.overrideMimeType(b.mimeType),b.crossDomain||d["X-Requested-With"]||(d["X-Requested-With"]="XMLHttpRequest");for(f in d)void 0!==d[f]&&g.setRequestHeader(f,d[f]+"");g.send(b.hasContent&&b.data||null),c=function(a,d){var f,i,j;if(c&&(d||4===g.readyState))if(delete fc[h],c=void 0,g.onreadystatechange=n.noop,d)4!==g.readyState&&g.abort();else{j={},f=g.status,"string"==typeof g.responseText&&(j.text=g.responseText);try{i=g.statusText}catch(k){i=""}f||!b.isLocal||b.crossDomain?1223===f&&(f=204):f=j.text?200:404}j&&e(f,i,j,g.getAllResponseHeaders())},b.async?4===g.readyState?a.setTimeout(c):g.onreadystatechange=fc[h]=c:c()},abort:function(){c&&c(void 0,!0)}}}});function hc(){try{return new a.XMLHttpRequest}catch(b){}}function ic(){try{return new a.ActiveXObject("Microsoft.XMLHTTP")}catch(b){}}n.ajaxSetup({accepts:{script:"text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"},contents:{script:/\b(?:java|ecma)script\b/},converters:{"text script":function(a){return n.globalEval(a),a}}}),n.ajaxPrefilter("script",function(a){void 0===a.cache&&(a.cache=!1),a.crossDomain&&(a.type="GET",a.global=!1)}),n.ajaxTransport("script",function(a){if(a.crossDomain){var b,c=d.head||n("head")[0]||d.documentElement;return{send:function(e,f){b=d.createElement("script"),b.async=!0,a.scriptCharset&&(b.charset=a.scriptCharset),b.src=a.url,b.onload=b.onreadystatechange=function(a,c){(c||!b.readyState||/loaded|complete/.test(b.readyState))&&(b.onload=b.onreadystatechange=null,b.parentNode&&b.parentNode.removeChild(b),b=null,c||f(200,"success"))},c.insertBefore(b,c.firstChild)},abort:function(){b&&b.onload(void 0,!0)}}}});var jc=[],kc=/(=)\?(?=&|$)|\?\?/;n.ajaxSetup({jsonp:"callback",jsonpCallback:function(){var a=jc.pop()||n.expando+"_"+Eb++;return this[a]=!0,a}}),n.ajaxPrefilter("json jsonp",function(b,c,d){var e,f,g,h=b.jsonp!==!1&&(kc.test(b.url)?"url":"string"==typeof b.data&&0===(b.contentType||"").indexOf("application/x-www-form-urlencoded")&&kc.test(b.data)&&"data");return h||"jsonp"===b.dataTypes[0]?(e=b.jsonpCallback=n.isFunction(b.jsonpCallback)?b.jsonpCallback():b.jsonpCallback,h?b[h]=b[h].replace(kc,"$1"+e):b.jsonp!==!1&&(b.url+=(Fb.test(b.url)?"&":"?")+b.jsonp+"="+e),b.converters["script json"]=function(){return g||n.error(e+" was not called"),g[0]},b.dataTypes[0]="json",f=a[e],a[e]=function(){g=arguments},d.always(function(){void 0===f?n(a).removeProp(e):a[e]=f,b[e]&&(b.jsonpCallback=c.jsonpCallback,jc.push(e)),g&&n.isFunction(f)&&f(g[0]),g=f=void 0}),"script"):void 0}),n.parseHTML=function(a,b,c){if(!a||"string"!=typeof a)return null;"boolean"==typeof b&&(c=b,b=!1),b=b||d;var e=x.exec(a),f=!c&&[];return e?[b.createElement(e[1])]:(e=ja([a],b,f),f&&f.length&&n(f).remove(),n.merge([],e.childNodes))};var lc=n.fn.load;n.fn.load=function(a,b,c){if("string"!=typeof a&&lc)return lc.apply(this,arguments);var d,e,f,g=this,h=a.indexOf(" ");return h>-1&&(d=n.trim(a.slice(h,a.length)),a=a.slice(0,h)),n.isFunction(b)?(c=b,b=void 0):b&&"object"==typeof b&&(e="POST"),g.length>0&&n.ajax({url:a,type:e||"GET",dataType:"html",data:b}).done(function(a){f=arguments,g.html(d?n("<div>").append(n.parseHTML(a)).find(d):a)}).always(c&&function(a,b){g.each(function(){c.apply(this,f||[a.responseText,b,a])})}),this},n.each(["ajaxStart","ajaxStop","ajaxComplete","ajaxError","ajaxSuccess","ajaxSend"],function(a,b){n.fn[b]=function(a){return this.on(b,a)}}),n.expr.filters.animated=function(a){return n.grep(n.timers,function(b){return a===b.elem}).length};function mc(a){return n.isWindow(a)?a:9===a.nodeType?a.defaultView||a.parentWindow:!1}n.offset={setOffset:function(a,b,c){var d,e,f,g,h,i,j,k=n.css(a,"position"),l=n(a),m={};"static"===k&&(a.style.position="relative"),h=l.offset(),f=n.css(a,"top"),i=n.css(a,"left"),j=("absolute"===k||"fixed"===k)&&n.inArray("auto",[f,i])>-1,j?(d=l.position(),g=d.top,e=d.left):(g=parseFloat(f)||0,e=parseFloat(i)||0),n.isFunction(b)&&(b=b.call(a,c,n.extend({},h))),null!=b.top&&(m.top=b.top-h.top+g),null!=b.left&&(m.left=b.left-h.left+e),"using"in b?b.using.call(a,m):l.css(m)}},n.fn.extend({offset:function(a){if(arguments.length)return void 0===a?this:this.each(function(b){n.offset.setOffset(this,a,b)});var b,c,d={top:0,left:0},e=this[0],f=e&&e.ownerDocument;if(f)return b=f.documentElement,n.contains(b,e)?("undefined"!=typeof e.getBoundingClientRect&&(d=e.getBoundingClientRect()),c=mc(f),{top:d.top+(c.pageYOffset||b.scrollTop)-(b.clientTop||0),left:d.left+(c.pageXOffset||b.scrollLeft)-(b.clientLeft||0)}):d},position:function(){if(this[0]){var a,b,c={top:0,left:0},d=this[0];return"fixed"===n.css(d,"position")?b=d.getBoundingClientRect():(a=this.offsetParent(),b=this.offset(),n.nodeName(a[0],"html")||(c=a.offset()),c.top+=n.css(a[0],"borderTopWidth",!0),c.left+=n.css(a[0],"borderLeftWidth",!0)),{top:b.top-c.top-n.css(d,"marginTop",!0),left:b.left-c.left-n.css(d,"marginLeft",!0)}}},offsetParent:function(){return this.map(function(){var a=this.offsetParent;while(a&&!n.nodeName(a,"html")&&"static"===n.css(a,"position"))a=a.offsetParent;return a||Qa})}}),n.each({scrollLeft:"pageXOffset",scrollTop:"pageYOffset"},function(a,b){var c=/Y/.test(b);n.fn[a]=function(d){return Y(this,function(a,d,e){var f=mc(a);return void 0===e?f?b in f?f[b]:f.document.documentElement[d]:a[d]:void(f?f.scrollTo(c?n(f).scrollLeft():e,c?e:n(f).scrollTop()):a[d]=e)},a,d,arguments.length,null)}}),n.each(["top","left"],function(a,b){n.cssHooks[b]=Ua(l.pixelPosition,function(a,c){return c?(c=Sa(a,b),Oa.test(c)?n(a).position()[b]+"px":c):void 0})}),n.each({Height:"height",Width:"width"},function(a,b){n.each({
padding:"inner"+a,content:b,"":"outer"+a},function(c,d){n.fn[d]=function(d,e){var f=arguments.length&&(c||"boolean"!=typeof d),g=c||(d===!0||e===!0?"margin":"border");return Y(this,function(b,c,d){var e;return n.isWindow(b)?b.document.documentElement["client"+a]:9===b.nodeType?(e=b.documentElement,Math.max(b.body["scroll"+a],e["scroll"+a],b.body["offset"+a],e["offset"+a],e["client"+a])):void 0===d?n.css(b,c,g):n.style(b,c,d,g)},b,f?d:void 0,f,null)}})}),n.fn.extend({bind:function(a,b,c){return this.on(a,null,b,c)},unbind:function(a,b){return this.off(a,null,b)},delegate:function(a,b,c,d){return this.on(b,a,c,d)},undelegate:function(a,b,c){return 1===arguments.length?this.off(a,"**"):this.off(b,a||"**",c)}}),n.fn.size=function(){return this.length},n.fn.andSelf=n.fn.addBack,"function"==typeof define&&define.amd&&define("jquery",[],function(){return n});var nc=a.jQuery,oc=a.$;return n.noConflict=function(b){return a.$===n&&(a.$=oc),b&&a.jQuery===n&&(a.jQuery=nc),n},b||(a.jQuery=a.$=n),n});

/*
Copyright 2012 Igor Vaynberg

Version: 3.3.1 Timestamp: Wed Feb 20 09:57:22 PST 2013

This software is licensed under the Apache License, Version 2.0 (the "Apache License") or the GNU
General Public License version 2 (the "GPL License"). You may choose either license to govern your
use of this software only upon the condition that you accept all of the terms of either the Apache
License or the GPL License.

You may obtain a copy of the Apache License and the GPL License at:

    http://www.apache.org/licenses/LICENSE-2.0
    http://www.gnu.org/licenses/gpl-2.0.html

Unless required by applicable law or agreed to in writing, software distributed under the
Apache License or the GPL Licesnse is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the Apache License and the GPL License for
the specific language governing permissions and limitations under the Apache License and the GPL License.
*/
 (function ($) {
 	if(typeof $.fn.each2 == "undefined"){
 		$.fn.extend({
 			/*
			* 4-10 times faster .each replacement
			* use it carefully, as it overrides jQuery context of element on each iteration
			*/
			each2 : function (c) {
				var j = $([0]), i = -1, l = this.length;
				while (
					++i < l
					&& (j.context = j[0] = this[i])
					&& c.call(j[0], i, j) !== false //"this"=DOM, i=index, j=jQuery object
				);
				return this;
			}
 		});
 	}
})(jQuery);

(function ($, undefined) {
    "use strict";
    /*global document, window, jQuery, console */

    if (window.Select2 !== undefined) {
        return;
    }

    var KEY, AbstractSelect2, SingleSelect2, MultiSelect2, nextUid, sizer,
        lastMousePosition, $document;

    KEY = {
        TAB: 9,
        ENTER: 13,
        ESC: 27,
        SPACE: 32,
        LEFT: 37,
        UP: 38,
        RIGHT: 39,
        DOWN: 40,
        SHIFT: 16,
        CTRL: 17,
        ALT: 18,
        PAGE_UP: 33,
        PAGE_DOWN: 34,
        HOME: 36,
        END: 35,
        BACKSPACE: 8,
        DELETE: 46,
        isArrow: function (k) {
            k = k.which ? k.which : k;
            switch (k) {
            case KEY.LEFT:
            case KEY.RIGHT:
            case KEY.UP:
            case KEY.DOWN:
                return true;
            }
            return false;
        },
        isControl: function (e) {
            var k = e.which;
            switch (k) {
            case KEY.SHIFT:
            case KEY.CTRL:
            case KEY.ALT:
                return true;
            }

            if (e.metaKey) return true;

            return false;
        },
        isFunctionKey: function (k) {
            k = k.which ? k.which : k;
            return k >= 112 && k <= 123;
        }
    };

    $document = $(document);

    nextUid=(function() { var counter=1; return function() { return counter++; }; }());

    function indexOf(value, array) {
        var i = 0, l = array.length;
        for (; i < l; i = i + 1) {
            if (equal(value, array[i])) return i;
        }
        return -1;
    }

    /**
     * Compares equality of a and b
     * @param a
     * @param b
     */
    function equal(a, b) {
        if (a === b) return true;
        if (a === undefined || b === undefined) return false;
        if (a === null || b === null) return false;
        if (a.constructor === String) return a === b+'';
        if (b.constructor === String) return b === a+'';
        return false;
    }

    /**
     * Splits the string into an array of values, trimming each value. An empty array is returned for nulls or empty
     * strings
     * @param string
     * @param separator
     */
    function splitVal(string, separator) {
        var val, i, l;
        if (string === null || string.length < 1) return [];
        val = string.split(separator);
        for (i = 0, l = val.length; i < l; i = i + 1) val[i] = $.trim(val[i]);
        return val;
    }

    function getSideBorderPadding(element) {
        return element.outerWidth(false) - element.width();
    }

    function installKeyUpChangeEvent(element) {
        var key="keyup-change-value";
        element.bind("keydown", function () {
            if ($.data(element, key) === undefined) {
                $.data(element, key, element.val());
            }
        });
        element.bind("keyup", function () {
            var val= $.data(element, key);
            if (val !== undefined && element.val() !== val) {
                $.removeData(element, key);
                element.trigger("keyup-change");
            }
        });
    }

    $document.bind("mousemove", function (e) {
        lastMousePosition = {x: e.pageX, y: e.pageY};
    });

    /**
     * filters mouse events so an event is fired only if the mouse moved.
     *
     * filters out mouse events that occur when mouse is stationary but
     * the elements under the pointer are scrolled.
     */
    function installFilteredMouseMove(element) {
	    element.bind("mousemove", function (e) {
            var lastpos = lastMousePosition;
            if (lastpos === undefined || lastpos.x !== e.pageX || lastpos.y !== e.pageY) {
                $(e.target).trigger("mousemove-filtered", e);
            }
        });
    }

    /**
     * Debounces a function. Returns a function that calls the original fn function only if no invocations have been made
     * within the last quietMillis milliseconds.
     *
     * @param quietMillis number of milliseconds to wait before invoking fn
     * @param fn function to be debounced
     * @param ctx object to be used as this reference within fn
     * @return debounced version of fn
     */
    function debounce(quietMillis, fn, ctx) {
        ctx = ctx || undefined;
        var timeout;
        return function () {
            var args = arguments;
            window.clearTimeout(timeout);
            timeout = window.setTimeout(function() {
                fn.apply(ctx, args);
            }, quietMillis);
        };
    }

    /**
     * A simple implementation of a thunk
     * @param formula function used to lazily initialize the thunk
     * @return {Function}
     */
    function thunk(formula) {
        var evaluated = false,
            value;
        return function() {
            if (evaluated === false) { value = formula(); evaluated = true; }
            return value;
        };
    };

    function installDebouncedScroll(threshold, element) {
        var notify = debounce(threshold, function (e) { element.trigger("scroll-debounced", e);});
        element.bind("scroll", function (e) {
            if (indexOf(e.target, element.get()) >= 0) notify(e);
        });
    }

    function focus($el) {
        if ($el[0] === document.activeElement) return;

        /* set the focus in a 0 timeout - that way the focus is set after the processing
            of the current event has finished - which seems like the only reliable way
            to set focus */
        window.setTimeout(function() {
            var el=$el[0], pos=$el.val().length, range;

            $el.focus();

            /* after the focus is set move the caret to the end, necessary when we val()
                just before setting focus */
            if(el.setSelectionRange)
            {
                el.setSelectionRange(pos, pos);
            }
            else if (el.createTextRange) {
                range = el.createTextRange();
                range.collapse(true);
                range.moveEnd('character', pos);
                range.moveStart('character', pos);
                range.select();
            }

        }, 0);
    }

    function killEvent(event) {
        event.preventDefault();
        event.stopPropagation();
    }
    function killEventImmediately(event) {
        event.preventDefault();
        event.stopImmediatePropagation();
    }

    function measureTextWidth(e) {
        if (!sizer){
        	var style = e[0].currentStyle || window.getComputedStyle(e[0], null);
        	sizer = $(document.createElement("div")).css({
	            position: "absolute",
	            left: "-10000px",
	            top: "-10000px",
	            display: "none",
	            fontSize: style.fontSize,
	            fontFamily: style.fontFamily,
	            fontStyle: style.fontStyle,
	            fontWeight: style.fontWeight,
	            letterSpacing: style.letterSpacing,
	            textTransform: style.textTransform,
	            whiteSpace: "nowrap"
	        });
            sizer.attr("class","select2-sizer");
        	$("body").append(sizer);
        }
        sizer.text(e.val());
        return sizer.width();
    }

    function syncCssClasses(dest, src, adapter) {
        var classes, replacements = [], adapted;

        classes = dest.attr("class");
        if (typeof classes === "string") {
            $(classes.split(" ")).each2(function() {
                if (this.indexOf("select2-") === 0) {
                    replacements.push(this);
                }
            });
        }
        classes = src.attr("class");
        if (typeof classes === "string") {
            $(classes.split(" ")).each2(function() {
                if (this.indexOf("select2-") !== 0) {
                    adapted = adapter(this);
                    if (typeof adapted === "string" && adapted.length > 0) {
                        replacements.push(this);
                    }
                }
            });
        }
        dest.attr("class", replacements.join(" "));
    }


    function markMatch(text, term, markup, escapeMarkup) {
        var match=text.toUpperCase().indexOf(term.toUpperCase()),
            tl=term.length;

        if (match<0) {
            markup.push(escapeMarkup(text));
            return;
        }

        markup.push(escapeMarkup(text.substring(0, match)));
        markup.push("<span class='select2-match'>");
        markup.push(escapeMarkup(text.substring(match, match + tl)));
        markup.push("</span>");
        markup.push(escapeMarkup(text.substring(match + tl, text.length)));
    }

    /**
     * Produces an ajax-based query function
     *
     * @param options object containing configuration paramters
     * @param options.transport function that will be used to execute the ajax request. must be compatible with parameters supported by $.ajax
     * @param options.url url for the data
     * @param options.data a function(searchTerm, pageNumber, context) that should return an object containing query string parameters for the above url.
     * @param options.dataType request data type: ajax, jsonp, other datatatypes supported by jQuery's $.ajax function or the transport function if specified
     * @param options.traditional a boolean flag that should be true if you wish to use the traditional style of param serialization for the ajax request
     * @param options.quietMillis (optional) milliseconds to wait before making the ajaxRequest, helps debounce the ajax function if invoked too often
     * @param options.results a function(remoteData, pageNumber) that converts data returned form the remote request to the format expected by Select2.
     *      The expected format is an object containing the following keys:
     *      results array of objects that will be used as choices
     *      more (optional) boolean indicating whether there are more results available
     *      Example: {results:[{id:1, text:'Red'},{id:2, text:'Blue'}], more:true}
     */
    function ajax(options) {
        var timeout, // current scheduled but not yet executed request
            requestSequence = 0, // sequence used to drop out-of-order responses
            handler = null,
            quietMillis = options.quietMillis || 100,
            ajaxUrl = options.url,
            self = this;

        return function (query) {
            window.clearTimeout(timeout);
            timeout = window.setTimeout(function () {
                requestSequence += 1; // increment the sequence
                var requestNumber = requestSequence, // this request's sequence number
                    data = options.data, // ajax data function
                    url = ajaxUrl, // ajax url string or function
                    transport = options.transport || $.ajax,
                    type = options.type || 'GET', // set type of request (GET or POST)
                    params = {};

                data = data ? data.call(self, query.term, query.page, query.context) : null;
                url = (typeof url === 'function') ? url.call(self, query.term, query.page, query.context) : url;

                if( null !== handler) { handler.abort(); }

                if (options.params) {
                    if ($.isFunction(options.params)) {
                        $.extend(params, options.params.call(self));
                    } else {
                        $.extend(params, options.params);
                    }
                }

                $.extend(params, {
                    url: url,
                    dataType: options.dataType,
                    data: data,
                    type: type,
                    cache: false,
                    success: function (data) {
                        if (requestNumber < requestSequence) {
                            return;
                        }
                        // TODO - replace query.page with query so users have access to term, page, etc.
                        var results = options.results(data, query.page);
                        query.callback(results);
                    }
                });
                handler = transport.call(self, params);
            }, quietMillis);
        };
    }

    /**
     * Produces a query function that works with a local array
     *
     * @param options object containing configuration parameters. The options parameter can either be an array or an
     * object.
     *
     * If the array form is used it is assumed that it contains objects with 'id' and 'text' keys.
     *
     * If the object form is used ti is assumed that it contains 'data' and 'text' keys. The 'data' key should contain
     * an array of objects that will be used as choices. These objects must contain at least an 'id' key. The 'text'
     * key can either be a String in which case it is expected that each element in the 'data' array has a key with the
     * value of 'text' which will be used to match choices. Alternatively, text can be a function(item) that can extract
     * the text.
     */
    function local(options) {
        var data = options, // data elements
            dataText,
            tmp,
            text = function (item) { return ""+item.text; }; // function used to retrieve the text portion of a data item that is matched against the search

		 if ($.isArray(data)) {
            tmp = data;
            data = { results: tmp };
        }

		 if ($.isFunction(data) === false) {
            tmp = data;
            data = function() { return tmp; };
        }

        var dataItem = data();
        if (dataItem.text) {
            text = dataItem.text;
            // if text is not a function we assume it to be a key name
            if (!$.isFunction(text)) {
                dataText = data.text; // we need to store this in a separate variable because in the next step data gets reset and data.text is no longer available
                text = function (item) { return item[dataText]; };
            }
        }

        return function (query) {
            var t = query.term, filtered = { results: [] }, process;
            if (t === "") {
                query.callback(data());
                return;
            }

            process = function(datum, collection) {
                var group, attr;
                datum = datum[0];
                if (datum.children) {
                    group = {};
                    for (attr in datum) {
                        if (datum.hasOwnProperty(attr)) group[attr]=datum[attr];
                    }
                    group.children=[];
                    $(datum.children).each2(function(i, childDatum) { process(childDatum, group.children); });
                    if (group.children.length || query.matcher(t, text(group), datum)) {
                        collection.push(group);
                    }
                } else {
                    if (query.matcher(t, text(datum), datum)) {
                        collection.push(datum);
                    }
                }
            };

            $(data().results).each2(function(i, datum) { process(datum, filtered.results); });
            query.callback(filtered);
        };
    }

    // TODO javadoc
    function tags(data) {
        var isFunc = $.isFunction(data);
        return function (query) {
            var t = query.term, filtered = {results: []};
            $(isFunc ? data() : data).each(function () {
                var isObject = this.text !== undefined,
                    text = isObject ? this.text : this;
                if (t === "" || query.matcher(t, text)) {
                    filtered.results.push(isObject ? this : {id: this, text: this});
                }
            });
            query.callback(filtered);
        };
    }

    /**
     * Checks if the formatter function should be used.
     *
     * Throws an error if it is not a function. Returns true if it should be used,
     * false if no formatting should be performed.
     *
     * @param formatter
     */
    function checkFormatter(formatter, formatterName) {
        if ($.isFunction(formatter)) return true;
        if (!formatter) return false;
        throw new Error("formatterName must be a function or a falsy value");
    }

    function evaluate(val) {
        return $.isFunction(val) ? val() : val;
    }

    function countResults(results) {
        var count = 0;
        $.each(results, function(i, item) {
            if (item.children) {
                count += countResults(item.children);
            } else {
                count++;
            }
        });
        return count;
    }

    /**
     * Default tokenizer. This function uses breaks the input on substring match of any string from the
     * opts.tokenSeparators array and uses opts.createSearchChoice to create the choice object. Both of those
     * two options have to be defined in order for the tokenizer to work.
     *
     * @param input text user has typed so far or pasted into the search field
     * @param selection currently selected choices
     * @param selectCallback function(choice) callback tho add the choice to selection
     * @param opts select2's opts
     * @return undefined/null to leave the current input unchanged, or a string to change the input to the returned value
     */
    function defaultTokenizer(input, selection, selectCallback, opts) {
        var original = input, // store the original so we can compare and know if we need to tell the search to update its text
            dupe = false, // check for whether a token we extracted represents a duplicate selected choice
            token, // token
            index, // position at which the separator was found
            i, l, // looping variables
            separator; // the matched separator

        if (!opts.createSearchChoice || !opts.tokenSeparators || opts.tokenSeparators.length < 1) return undefined;

        while (true) {
            index = -1;

            for (i = 0, l = opts.tokenSeparators.length; i < l; i++) {
                separator = opts.tokenSeparators[i];
                index = input.indexOf(separator);
                if (index >= 0) break;
            }

            if (index < 0) break; // did not find any token separator in the input string, bail

            token = input.substring(0, index);
            input = input.substring(index + separator.length);

            if (token.length > 0) {
                token = opts.createSearchChoice(token, selection);
                if (token !== undefined && token !== null && opts.id(token) !== undefined && opts.id(token) !== null) {
                    dupe = false;
                    for (i = 0, l = selection.length; i < l; i++) {
                        if (equal(opts.id(token), opts.id(selection[i]))) {
                            dupe = true; break;
                        }
                    }

                    if (!dupe) selectCallback(token);
                }
            }
        }

        if (original!==input) return input;
    }

    /**
     * Creates a new class
     *
     * @param superClass
     * @param methods
     */
    function clazz(SuperClass, methods) {
        var constructor = function () {};
        constructor.prototype = new SuperClass;
        constructor.prototype.constructor = constructor;
        constructor.prototype.parent = SuperClass.prototype;
        constructor.prototype = $.extend(constructor.prototype, methods);
        return constructor;
    }

    AbstractSelect2 = clazz(Object, {

        // abstract
        bind: function (func) {
            var self = this;
            return function () {
                func.apply(self, arguments);
            };
        },

        // abstract
        init: function (opts) {
            var results, search, resultsSelector = ".select2-results", mask;

            // prepare options
            this.opts = opts = this.prepareOpts(opts);

            this.id=opts.id;

            // destroy if called on an existing component
            if (opts.element.data("select2") !== undefined &&
                opts.element.data("select2") !== null) {
                this.destroy();
            }

            this.enabled=true;
            this.container = this.createContainer();

            this.containerId="s2id_"+(opts.element.attr("id") || "autogen"+nextUid());
            this.containerSelector="#"+this.containerId.replace(/([;&,\.\+\*\~':"\!\^#$%@\[\]\(\)=>\|])/g, '\\$1');
            this.container.attr("id", this.containerId);

            // cache the body so future lookups are cheap
            this.body = thunk(function() { return opts.element.closest("body"); });

            syncCssClasses(this.container, this.opts.element, this.opts.adaptContainerCssClass);

            this.container.css(evaluate(opts.containerCss));
            this.container.addClass(evaluate(opts.containerCssClass));

            this.elementTabIndex = this.opts.element.attr("tabIndex");

            // swap container for the element
            this.opts.element
                .data("select2", this)
                .addClass("select2-offscreen")
                .bind("focus.select2", function() { $(this).select2("focus"); })
                .attr("tabIndex", "-1")
                .before(this.container);
            this.container.data("select2", this);

            this.dropdown = this.container.find(".select2-drop");
            this.dropdown.addClass(evaluate(opts.dropdownCssClass));
            this.dropdown.data("select2", this);

            this.results = results = this.container.find(resultsSelector);
            this.search = search = this.container.find("input.select2-input");

            search.attr("tabIndex", this.elementTabIndex);

            this.resultsPage = 0;
            this.context = null;

            // initialize the container
            this.initContainer();

            installFilteredMouseMove(this.results);
            this.dropdown.delegate(resultsSelector, "mousemove-filtered touchstart touchmove touchend", this.bind(this.highlightUnderEvent));

            installDebouncedScroll(80, this.results);
            this.dropdown.delegate(resultsSelector, "scroll-debounced", this.bind(this.loadMoreIfNeeded));

            // if jquery.mousewheel plugin is installed we can prevent out-of-bounds scrolling of results via mousewheel
            if ($.fn.mousewheel) {
                results.mousewheel(function (e, delta, deltaX, deltaY) {
                    var top = results.scrollTop(), height;
                    if (deltaY > 0 && top - deltaY <= 0) {
                        results.scrollTop(0);
                        killEvent(e);
                    } else if (deltaY < 0 && results.get(0).scrollHeight - results.scrollTop() + deltaY <= results.height()) {
                        results.scrollTop(results.get(0).scrollHeight - results.height());
                        killEvent(e);
                    }
                });
            }

            installKeyUpChangeEvent(search);
            search.bind("keyup-change input paste", this.bind(this.updateResults));
            search.bind("focus", function () { search.addClass("select2-focused"); });
            search.bind("blur", function () { search.removeClass("select2-focused");});

            this.dropdown.delegate(resultsSelector, "mouseup", this.bind(function (e) {
                if ($(e.target).closest(".select2-result-selectable").length > 0) {
                    this.highlightUnderEvent(e);
                    this.selectHighlighted(e);
                }
            }));

            // trap all mouse events from leaving the dropdown. sometimes there may be a modal that is listening
            // for mouse events outside of itself so it can close itself. since the dropdown is now outside the select2's
            // dom it will trigger the popup close, which is not what we want
            this.dropdown.bind("click mouseup mousedown", function (e) { e.stopPropagation(); });

            if ($.isFunction(this.opts.initSelection)) {
                // initialize selection based on the current value of the source element
                this.initSelection();

                // if the user has provided a function that can set selection based on the value of the source element
                // we monitor the change event on the element and trigger it, allowing for two way synchronization
                this.monitorSource();
            }

            if (opts.element.is(":disabled") || opts.element.is("[readonly='readonly']")) this.disable();
        },

        // abstract
        destroy: function () {
            var select2 = this.opts.element.data("select2");

            if (this.propertyObserver) { delete this.propertyObserver; this.propertyObserver = null; }

            if (select2 !== undefined) {

                select2.container.remove();
                select2.dropdown.remove();
                select2.opts.element
                    .removeClass("select2-offscreen")
                    .removeData("select2")
                    .unbind(".select2")
                    .attr({"tabIndex": this.elementTabIndex})
                    .show();
            }
        },

        // abstract
        prepareOpts: function (opts) {
            var element, select, idKey, ajaxUrl;

            element = opts.element;

            if (element.get(0).tagName.toLowerCase() === "select") {
                this.select = select = opts.element;
            }

            if (select) {
                // these options are not allowed when attached to a select because they are picked up off the element itself
                $.each(["id", "multiple", "ajax", "query", "createSearchChoice", "initSelection", "data", "tags"], function () {
                    if (this in opts) {
                        throw new Error("Option '" + this + "' is not allowed for Select2 when attached to a <select> element.");
                    }
                });
            }

            opts = $.extend({}, {
                populateResults: function(container, results, query) {
                    var populate,  data, result, children, id=this.opts.id, self=this;

                    populate=function(results, container, depth) {

                        var i, l, result, selectable, disabled, compound, node, label, innerContainer, formatted;

                        results = opts.sortResults(results, container, query);

                        for (i = 0, l = results.length; i < l; i = i + 1) {

                            result=results[i];

                            disabled = (result.disabled === true);
                            selectable = (!disabled) && (id(result) !== undefined);

                            compound=result.children && result.children.length > 0;

                            node=$("<li></li>");
                            node.addClass("select2-results-dept-"+depth);
                            node.addClass("select2-result");
                            node.addClass(selectable ? "select2-result-selectable" : "select2-result-unselectable");
                            if (disabled) { node.addClass("select2-disabled"); }
                            if (compound) { node.addClass("select2-result-with-children"); }
                            node.addClass(self.opts.formatResultCssClass(result));

                            label=$(document.createElement("div"));
                            label.addClass("select2-result-label");

                            formatted=opts.formatResult(result, label, query, self.opts.escapeMarkup);
                            if (formatted!==undefined) {
                                label.html(formatted);
                            }

                            node.append(label);

                            if (compound) {

                                innerContainer=$("<ul></ul>");
                                innerContainer.addClass("select2-result-sub");
                                populate(result.children, innerContainer, depth+1);
                                node.append(innerContainer);
                            }

                            node.data("select2-data", result);
                            container.append(node);
                        }
                    };

                    populate(results, container, 0);
                }
            }, $.fn.select2.defaults, opts);

            if (typeof(opts.id) !== "function") {
                idKey = opts.id;
                opts.id = function (e) { return e[idKey]; };
            }

            if ($.isArray(opts.element.data("select2Tags"))) {
                if ("tags" in opts) {
                    throw "tags specified as both an attribute 'data-select2-tags' and in options of Select2 " + opts.element.attr("id");
                }
                opts.tags=opts.element.attr("data-select2-tags");
            }

            if (select) {
                opts.query = this.bind(function (query) {
                    var data = { results: [], more: false },
                        term = query.term,
                        children, firstChild, process;

                    process=function(element, collection) {
                        var group;
                        if (element.is("option")) {
                            if (query.matcher(term, element.text(), element)) {
                                collection.push({id:element.attr("value"), text:element.text(), element: element.get(), css: element.attr("class"), disabled: equal(element.attr("disabled"), "disabled") });
                            }
                        } else if (element.is("optgroup")) {
                            group={text:element.attr("label"), children:[], element: element.get(), css: element.attr("class")};
                            element.children().each2(function(i, elm) { process(elm, group.children); });
                            if (group.children.length>0) {
                                collection.push(group);
                            }
                        }
                    };

                    children=element.children();

                    // ignore the placeholder option if there is one
                    if (this.getPlaceholder() !== undefined && children.length > 0) {
                        firstChild = children[0];
                        if ($(firstChild).text() === "") {
                            children=children.not(firstChild);
                        }
                    }

                    children.each2(function(i, elm) { process(elm, data.results); });

                    query.callback(data);
                });
                // this is needed because inside val() we construct choices from options and there id is hardcoded
                opts.id=function(e) { return e.id; };
                opts.formatResultCssClass = function(data) { return data.css; };
            } else {
                if (!("query" in opts)) {

                    if ("ajax" in opts) {
                        ajaxUrl = opts.element.data("ajax-url");
                        if (ajaxUrl && ajaxUrl.length > 0) {
                            opts.ajax.url = ajaxUrl;
                        }
                        opts.query = ajax.call(opts.element, opts.ajax);
                    } else if ("data" in opts) {
                        opts.query = local(opts.data);
                    } else if ("tags" in opts) {
                        opts.query = tags(opts.tags);
                        if (opts.createSearchChoice === undefined) {
                            opts.createSearchChoice = function (term) { return {id: term, text: term}; };
                        }
                        if (opts.initSelection === undefined) {
                            opts.initSelection = function (element, callback) {
                                var data = [];
                                $(splitVal(element.val(), opts.separator)).each(function () {
                                    var id = this, text = this, tags=opts.tags;
                                    if ($.isFunction(tags)) tags=tags();
                                    $(tags).each(function() { if (equal(this.id, id)) { text = this.text; return false; } });
                                    data.push({id: id, text: text});
                                });

                                callback(data);
                            };
                        }
                    }
                }
            }
            if (typeof(opts.query) !== "function") {
                throw "query function not defined for Select2 " + opts.element.attr("id");
            }

            return opts;
        },

        /**
         * Monitor the original element for changes and update select2 accordingly
         */
        // abstract
        monitorSource: function () {
            var el = this.opts.element, sync;

            el.bind("change.select2", this.bind(function (e) {
                if (this.opts.element.data("select2-change-triggered") !== true) {
                    this.initSelection();
                }
            }));

            sync = this.bind(function () {

                var enabled, readonly, self = this;

                // sync enabled state

                enabled = this.opts.element.attr("disabled") !== "disabled";
                readonly = this.opts.element.attr("readonly") === "readonly";

                enabled = enabled && !readonly;

                if (this.enabled !== enabled) {
                    if (enabled) {
                        this.enable();
                    } else {
                        this.disable();
                    }
                }


                syncCssClasses(this.container, this.opts.element, this.opts.adaptContainerCssClass);
                this.container.addClass(evaluate(this.opts.containerCssClass));

                syncCssClasses(this.dropdown, this.opts.element, this.opts.adaptDropdownCssClass);
                this.dropdown.addClass(evaluate(this.opts.dropdownCssClass));

            });

            // mozilla and IE
            el.bind("propertychange.select2 DOMAttrModified.select2", sync);
            // safari and chrome
            if (typeof WebKitMutationObserver !== "undefined") {
                if (this.propertyObserver) { delete this.propertyObserver; this.propertyObserver = null; }
                this.propertyObserver = new WebKitMutationObserver(function (mutations) {
                    mutations.forEach(sync);
                });
                this.propertyObserver.observe(el.get(0), { attributes:true, subtree:false });
            }
        },

        /**
         * Triggers the change event on the source element
         */
        // abstract
        triggerChange: function (details) {

            details = details || {};
            details= $.extend({}, details, { type: "change", val: this.val() });
            // prevents recursive triggering
            this.opts.element.data("select2-change-triggered", true);
            this.opts.element.trigger(details);
            this.opts.element.data("select2-change-triggered", false);

            // some validation frameworks ignore the change event and listen instead to keyup, click for selects
            // so here we trigger the click event manually
            this.opts.element.click();

            // ValidationEngine ignorea the change event and listens instead to blur
            // so here we trigger the blur event manually if so desired
            if (this.opts.blurOnChange)
                this.opts.element.blur();
        },

        // abstract
        enable: function() {
            if (this.enabled) return;

            this.enabled=true;
            this.container.removeClass("select2-container-disabled");
            this.opts.element.removeAttr("disabled");
        },

        // abstract
        disable: function() {
            if (!this.enabled) return;

            this.close();

            this.enabled=false;
            this.container.addClass("select2-container-disabled");
            this.opts.element.attr("disabled", "disabled");
        },

        // abstract
        opened: function () {
            return this.container.hasClass("select2-dropdown-open");
        },

        // abstract
        positionDropdown: function() {
            var offset = this.container.offset(),
                height = this.container.outerHeight(false),
                width = this.container.outerWidth(false),
                dropHeight = this.dropdown.outerHeight(false),
	            viewPortRight = $(window).scrollLeft() + $(window).width(),
                viewportBottom = $(window).scrollTop() + $(window).height(),
                dropTop = offset.top + height,
                dropLeft = offset.left,
                enoughRoomBelow = dropTop + dropHeight <= viewportBottom,
                enoughRoomAbove = (offset.top - dropHeight) >= this.body().scrollTop(),
	            dropWidth = this.dropdown.outerWidth(false),
	            enoughRoomOnRight = dropLeft + dropWidth <= viewPortRight,
                aboveNow = this.dropdown.hasClass("select2-drop-above"),
                bodyOffset,
                above,
                css;

            //console.log("below/ droptop:", dropTop, "dropHeight", dropHeight, "sum", (dropTop+dropHeight)+" viewport bottom", viewportBottom, "enough?", enoughRoomBelow);
            //console.log("above/ offset.top", offset.top, "dropHeight", dropHeight, "top", (offset.top-dropHeight), "scrollTop", this.body().scrollTop(), "enough?", enoughRoomAbove);

            // fix positioning when body has an offset and is not position: static

            if (this.body().css('position') !== 'static') {
                bodyOffset = this.body().offset();
                dropTop -= bodyOffset.top;
                dropLeft -= bodyOffset.left;
            }

            // always prefer the current above/below alignment, unless there is not enough room

            if (aboveNow) {
                above = true;
                if (!enoughRoomAbove && enoughRoomBelow) above = false;
            } else {
                above = false;
                if (!enoughRoomBelow && enoughRoomAbove) above = true;
            }

            if (!enoughRoomOnRight) {
               dropLeft = offset.left + width - dropWidth;
            }

            if (above) {
                dropTop = offset.top - dropHeight;
                this.container.addClass("select2-drop-above");
                this.dropdown.addClass("select2-drop-above");
            }
            else {
                this.container.removeClass("select2-drop-above");
                this.dropdown.removeClass("select2-drop-above");
            }

            css = $.extend({
                top: dropTop,
                left: dropLeft,
                width: width
            }, evaluate(this.opts.dropdownCss));

            this.dropdown.css(css);
        },

        // abstract
        shouldOpen: function() {
            var event;

            if (this.opened()) return false;

            event = $.Event("opening");
            this.opts.element.trigger(event);
            return !event.isDefaultPrevented();
        },

        // abstract
        clearDropdownAlignmentPreference: function() {
            // clear the classes used to figure out the preference of where the dropdown should be opened
            this.container.removeClass("select2-drop-above");
            this.dropdown.removeClass("select2-drop-above");
        },

        /**
         * Opens the dropdown
         *
         * @return {Boolean} whether or not dropdown was opened. This method will return false if, for example,
         * the dropdown is already open, or if the 'open' event listener on the element called preventDefault().
         */
        // abstract
        open: function () {

            if (!this.shouldOpen()) return false;

            window.setTimeout(this.bind(this.opening), 1);

            return true;
        },

        /**
         * Performs the opening of the dropdown
         */
        // abstract
        opening: function() {
            var cid = this.containerId,
                scroll = "scroll." + cid,
                resize = "resize."+cid,
                orient = "orientationchange."+cid,
                mask;

            this.clearDropdownAlignmentPreference();

            this.container.addClass("select2-dropdown-open").addClass("select2-container-active");


            if(this.dropdown[0] !== this.body().children().last()[0]) {
                this.dropdown.detach().appendTo(this.body());
            }

            this.updateResults(true);

            // create the dropdown mask if doesnt already exist
            mask = $("#select2-drop-mask");
            if (mask.length == 0) {
                mask = $(document.createElement("div"));
                mask.attr("id","select2-drop-mask").attr("class","select2-drop-mask");
                mask.hide();
                mask.appendTo(this.body());
                mask.bind("mousedown touchstart", function (e) {
                    var dropdown = $("#select2-drop"), self;
                    if (dropdown.length > 0) {
                        self=dropdown.data("select2");
                        if (self.opts.selectOnBlur) {
                            self.selectHighlighted({noFocus: true});
                        }
                        self.close();
                    }
                });
            }

            // ensure the mask is always right before the dropdown
            if (this.dropdown.prev()[0] !== mask[0]) {
                this.dropdown.before(mask);
            }

            // move the global id to the correct dropdown
            $("#select2-drop").removeAttr("id");
            this.dropdown.attr("id", "select2-drop");

            // show the elements
            mask.css({
                width: document.documentElement.scrollWidth,
                height: document.documentElement.scrollHeight});
            mask.show();
            this.dropdown.show();
            this.positionDropdown();

            this.dropdown.addClass("select2-drop-active");
            this.ensureHighlightVisible();

            // attach listeners to events that can change the position of the container and thus require
            // the position of the dropdown to be updated as well so it does not come unglued from the container
            var that = this;
            this.container.parents().add(window).each(function () {
                $(this).bind(resize+" "+scroll+" "+orient, function (e) {
                    $("#select2-drop-mask").css({
                        width:document.documentElement.scrollWidth,
                        height:document.documentElement.scrollHeight});
                    that.positionDropdown();
                });
            });

            this.focusSearch();
        },

        // abstract
        close: function () {
            if (!this.opened()) return;

            var cid = this.containerId,
                scroll = "scroll." + cid,
                resize = "resize."+cid,
                orient = "orientationchange."+cid;

            // unbind event listeners
            this.container.parents().add(window).each(function () { $(this).unbind(scroll).unbind(resize).unbind(orient); });

            this.clearDropdownAlignmentPreference();

            $("#select2-drop-mask").hide();
            this.dropdown.removeAttr("id"); // only the active dropdown has the select2-drop id
            this.dropdown.hide();
            this.container.removeClass("select2-dropdown-open");
            this.results.empty();
            this.clearSearch();

            this.opts.element.trigger($.Event("close"));
        },

        // abstract
        clearSearch: function () {

        },

        //abstract
        getMaximumSelectionSize: function() {
            return evaluate(this.opts.maximumSelectionSize);
        },

        // abstract
        ensureHighlightVisible: function () {
            var results = this.results, children, index, child, hb, rb, y, more;

            index = this.highlight();

            if (index < 0) return;

            if (index == 0) {

                // if the first element is highlighted scroll all the way to the top,
                // that way any unselectable headers above it will also be scrolled
                // into view

                results.scrollTop(0);
                return;
            }

            children = this.findHighlightableChoices();

            child = $(children[index]);

            hb = child.offset().top + child.outerHeight(true);

            // if this is the last child lets also make sure select2-more-results is visible
            if (index === children.length - 1) {
                more = results.find("li.select2-more-results");
                if (more.length > 0) {
                    hb = more.offset().top + more.outerHeight(true);
                }
            }

            rb = results.offset().top + results.outerHeight(true);
            if (hb > rb) {
                results.scrollTop(results.scrollTop() + (hb - rb));
            }
            y = child.offset().top - results.offset().top;

            // make sure the top of the element is visible
            if (y < 0 && child.css('display') != 'none' ) {
                results.scrollTop(results.scrollTop() + y); // y is negative
            }
        },

        // abstract
        findHighlightableChoices: function() {
            var h=this.results.find(".select2-result-selectable:not(.select2-selected):not(.select2-disabled)");
            return this.results.find(".select2-result-selectable:not(.select2-selected):not(.select2-disabled)");
        },

        // abstract
        moveHighlight: function (delta) {
            var choices = this.findHighlightableChoices(),
                index = this.highlight();

            while (index > -1 && index < choices.length) {
                index += delta;
                var choice = $(choices[index]);
                if (choice.hasClass("select2-result-selectable") && !choice.hasClass("select2-disabled") && !choice.hasClass("select2-selected")) {
                    this.highlight(index);
                    break;
                }
            }
        },

        // abstract
        highlight: function (index) {
            var choices = this.findHighlightableChoices(),
                choice,
                data;

            if (arguments.length === 0) {
                return indexOf(choices.filter(".select2-highlighted")[0], choices.get());
            }

            if (index >= choices.length) index = choices.length - 1;
            if (index < 0) index = 0;

            this.results.find(".select2-highlighted").removeClass("select2-highlighted");

            choice = $(choices[index]);
            choice.addClass("select2-highlighted");

            this.ensureHighlightVisible();

            data = choice.data("select2-data");
            if (data) {
                this.opts.element.trigger({ type: "highlight", val: this.id(data), choice: data });
            }
        },

        // abstract
        countSelectableResults: function() {
            return this.findHighlightableChoices().length;
        },

        // abstract
        highlightUnderEvent: function (event) {
            var el = $(event.target).closest(".select2-result-selectable");
            if (el.length > 0 && !el.is(".select2-highlighted")) {
        		var choices = this.findHighlightableChoices();
                this.highlight(choices.index(el));
            } else if (el.length == 0) {
                // if we are over an unselectable item remove al highlights
                this.results.find(".select2-highlighted").removeClass("select2-highlighted");
            }
        },

        // abstract
        loadMoreIfNeeded: function () {
            var results = this.results,
                more = results.find("li.select2-more-results"),
                below, // pixels the element is below the scroll fold, below==0 is when the element is starting to be visible
                offset = -1, // index of first element without data
                page = this.resultsPage + 1,
                self=this,
                term=this.search.val(),
                context=this.context;

            if (more.length === 0) return;
            below = more.offset().top - results.offset().top - results.height();

            if (below <= this.opts.loadMorePadding) {
                more.addClass("select2-active");
                this.opts.query({
                        element: this.opts.element,
                        term: term,
                        page: page,
                        context: context,
                        matcher: this.opts.matcher,
                        callback: this.bind(function (data) {

                    // ignore a response if the select2 has been closed before it was received
                    if (!self.opened()) return;


                    self.opts.populateResults.call(this, results, data.results, {term: term, page: page, context:context});

                    if (data.more===true) {
                        more.detach().appendTo(results).text(self.opts.formatLoadMore(page+1));
                        window.setTimeout(function() { self.loadMoreIfNeeded(); }, 10);
                    } else {
                        more.remove();
                    }
                    self.positionDropdown();
                    self.resultsPage = page;
                    self.context = data.context;
                })});
            }
        },

        /**
         * Default tokenizer function which does nothing
         */
        tokenize: function() {

        },

        /**
         * @param initial whether or not this is the call to this method right after the dropdown has been opened
         */
        // abstract
        updateResults: function (initial) {
            var search = this.search, results = this.results, opts = this.opts, data, self=this, input;

            // if the search is currently hidden we do not alter the results
            if (initial !== true && (this.showSearchInput === false || !this.opened())) {
                return;
            }

            search.addClass("select2-active");

            function postRender() {
                results.scrollTop(0);
                search.removeClass("select2-active");
                self.positionDropdown();
            }

            function render(html) {
                results.html(html);
                postRender();
            }

            var maxSelSize = this.getMaximumSelectionSize();
            if (maxSelSize >=1) {
                data = this.data();
                if ($.isArray(data) && data.length >= maxSelSize && checkFormatter(opts.formatSelectionTooBig, "formatSelectionTooBig")) {
            	    render("<li class='select2-selection-limit'>" + opts.formatSelectionTooBig(maxSelSize) + "</li>");
            	    return;
                }
            }

            if (search.val().length < opts.minimumInputLength) {
                if (checkFormatter(opts.formatInputTooShort, "formatInputTooShort")) {
                    render("<li class='select2-no-results'>" + opts.formatInputTooShort(search.val(), opts.minimumInputLength) + "</li>");
                } else {
                    render("");
                }
                return;
            }
            else if (opts.formatSearching() && initial===true) {
                render("<li class='select2-searching'>" + opts.formatSearching() + "</li>");
            }

            if (opts.maximumInputLength && search.val().length > opts.maximumInputLength) {
                if (checkFormatter(opts.formatInputTooLong, "formatInputTooLong")) {
                    render("<li class='select2-no-results'>" + opts.formatInputTooLong(search.val(), opts.maximumInputLength) + "</li>");
                } else {
                    render("");
                }
                return;
            }

            // give the tokenizer a chance to pre-process the input
            input = this.tokenize();
            if (input != undefined && input != null) {
                search.val(input);
            }

            this.resultsPage = 1;

            opts.query({
                element: opts.element,
                    term: search.val(),
                    page: this.resultsPage,
                    context: null,
                    matcher: opts.matcher,
                    callback: this.bind(function (data) {
                var def; // default choice

                // ignore a response if the select2 has been closed before it was received
                if (!this.opened()) return;

                // save context, if any
                this.context = (data.context===undefined) ? null : data.context;
                // create a default choice and prepend it to the list
                if (this.opts.createSearchChoice && search.val() !== "") {
                    def = this.opts.createSearchChoice.call(null, search.val(), data.results);
                    if (def !== undefined && def !== null && self.id(def) !== undefined && self.id(def) !== null) {
                        if ($(data.results).filter(
                            function () {
                                return equal(self.id(this), self.id(def));
                            }).length === 0) {
                            data.results.unshift(def);
                        }
                    }
                }

                if (data.results.length === 0 && checkFormatter(opts.formatNoMatches, "formatNoMatches")) {
                    render("<li class='select2-no-results'>" + opts.formatNoMatches(search.val()) + "</li>");
                    return;
                }

                results.empty();
                self.opts.populateResults.call(this, results, data.results, {term: search.val(), page: this.resultsPage, context:null});

                if (data.more === true && checkFormatter(opts.formatLoadMore, "formatLoadMore")) {
                    results.append("<li class='select2-more-results'>" + self.opts.escapeMarkup(opts.formatLoadMore(this.resultsPage)) + "</li>");
                    window.setTimeout(function() { self.loadMoreIfNeeded(); }, 10);
                }

                this.postprocessResults(data, initial);

                postRender();
            })});
        },

        // abstract
        cancel: function () {
            this.close();
        },

        // abstract
        blur: function () {
            // if selectOnBlur == true, select the currently highlighted option
            if (this.opts.selectOnBlur)
                this.selectHighlighted({noFocus: true});

            this.close();
            this.container.removeClass("select2-container-active");
            // synonymous to .is(':focus'), which is available in jquery >= 1.6
            if (this.search[0] === document.activeElement) { this.search.blur(); }
            this.clearSearch();
            this.selection.find(".select2-search-choice-focus").removeClass("select2-search-choice-focus");
        },

        // abstract
        focusSearch: function () {
            focus(this.search);
        },

        // abstract
        selectHighlighted: function (options) {
            var index=this.highlight(),
                highlighted=this.results.find(".select2-highlighted"),
                data = highlighted.closest('.select2-result').data("select2-data");

            if (data) {
                this.highlight(index);
                this.onSelect(data, options);
            }
        },

        // abstract
        getPlaceholder: function () {
            return this.opts.element.attr("placeholder") ||
                this.opts.element.attr("data-placeholder") || // jquery 1.4 compat
                this.opts.element.data("placeholder") ||
                this.opts.placeholder;
        },

        /**
         * Get the desired width for the container element.  This is
         * derived first from option `width` passed to select2, then
         * the inline 'style' on the original element, and finally
         * falls back to the jQuery calculated element width.
         */
        // abstract
        initContainerWidth: function () {
            function resolveContainerWidth() {
                var style, attrs, matches, i, l;

                if (this.opts.width === "off") {
                    return null;
                } else if (this.opts.width === "element"){
                    return this.opts.element.outerWidth(false) === 0 ? 'auto' : this.opts.element.outerWidth(false) + 'px';
                } else if (this.opts.width === "copy" || this.opts.width === "resolve") {
                    // check if there is inline style on the element that contains width
                    style = this.opts.element.attr('style');
                    if (style !== undefined) {
                        attrs = style.split(';');
                        for (i = 0, l = attrs.length; i < l; i = i + 1) {
                            matches = attrs[i].replace(/\s/g, '')
                                .match(/width:(([-+]?([0-9]*\.)?[0-9]+)(px|em|ex|%|in|cm|mm|pt|pc))/);
                            if (matches !== null && matches.length >= 1)
                                return matches[1];
                        }
                    }

                    if (this.opts.width === "resolve") {
                        // next check if css('width') can resolve a width that is percent based, this is sometimes possible
                        // when attached to input type=hidden or elements hidden via css
                        style = this.opts.element.css('width');
                        if (style.indexOf("%") > 0) return style;

                        // finally, fallback on the calculated width of the element
                        return (this.opts.element.outerWidth(false) === 0 ? 'auto' : this.opts.element.outerWidth(false) + 'px');
                    }

                    return null;
                } else if ($.isFunction(this.opts.width)) {
                    return this.opts.width();
                } else {
                    return this.opts.width;
               }
            };

            var width = resolveContainerWidth.call(this);
            if (width !== null) {
                this.container.css("width", width);
            }
        }
    });

    SingleSelect2 = clazz(AbstractSelect2, {

        // single

		createContainer: function () {
            var container = $(document.createElement("div")).attr({
                "class": "select2-container"
            }).html([
                "<a href='javascript:void(0)' onclick='return false;' class='select2-choice' tabindex='-1'>",
                "   <span></span><abbr class='select2-search-choice-close' style='display:none;'></abbr>",
                "   <div><b></b></div>" ,
                "</a>",
                "<input class='select2-focusser select2-offscreen' type='text'/>",
                "<div class='select2-drop' style='display:none'>" ,
                "   <div class='select2-search'>" ,
                "       <input type='text' autocomplete='off' class='select2-input'/>" ,
                "   </div>" ,
                "   <ul class='select2-results'>" ,
                "   </ul>" ,
                "</div>"].join(""));
            return container;
        },

        // single
        disable: function() {
            if (!this.enabled) return;

            this.parent.disable.apply(this, arguments);

            this.focusser.attr("disabled", "disabled");
        },

        // single
        enable: function() {
            if (this.enabled) return;

            this.parent.enable.apply(this, arguments);

            this.focusser.removeAttr("disabled");
        },

        // single
        opening: function () {
            this.parent.opening.apply(this, arguments);
            this.focusser.attr("disabled", "disabled");

            this.opts.element.trigger($.Event("open"));
        },

        // single
        close: function () {
            if (!this.opened()) return;
            this.parent.close.apply(this, arguments);
            this.focusser.removeAttr("disabled");
            focus(this.focusser);
        },

        // single
        focus: function () {
            if (this.opened()) {
                this.close();
            } else {
                this.focusser.removeAttr("disabled");
                this.focusser.focus();
            }
        },

        // single
        isFocused: function () {
            return this.container.hasClass("select2-container-active");
        },

        // single
        cancel: function () {
            this.parent.cancel.apply(this, arguments);
            this.focusser.removeAttr("disabled");
            this.focusser.focus();
        },

        // single
        initContainer: function () {

            var selection,
                container = this.container,
                dropdown = this.dropdown,
                clickingInside = false;

            this.showSearch(this.opts.minimumResultsForSearch >= 0);

            this.selection = selection = container.find(".select2-choice");

            this.focusser = container.find(".select2-focusser");

            this.search.bind("keydown", this.bind(function (e) {
                if (!this.enabled) return;

                if (e.which === KEY.PAGE_UP || e.which === KEY.PAGE_DOWN) {
                    // prevent the page from scrolling
                    killEvent(e);
                    return;
                }

                switch (e.which) {
                    case KEY.UP:
                    case KEY.DOWN:
                        this.moveHighlight((e.which === KEY.UP) ? -1 : 1);
                        killEvent(e);
                        return;
                    case KEY.TAB:
                    case KEY.ENTER:
                        this.selectHighlighted();
                        killEvent(e);
                        return;
                    case KEY.ESC:
                        this.cancel(e);
                        killEvent(e);
                        return;
                }
            }));

            this.focusser.bind("keydown", this.bind(function (e) {
                if (!this.enabled) return;

                if (e.which === KEY.TAB || KEY.isControl(e) || KEY.isFunctionKey(e) || e.which === KEY.ESC) {
                    return;
                }

                if (this.opts.openOnEnter === false && e.which === KEY.ENTER) {
                    killEvent(e);
                    return;
                }

                if (e.which == KEY.DOWN || e.which == KEY.UP
                    || (e.which == KEY.ENTER && this.opts.openOnEnter)) {
                    this.open();
                    killEvent(e);
                    return;
                }

                if (e.which == KEY.DELETE || e.which == KEY.BACKSPACE) {
                    if (this.opts.allowClear) {
                        this.clear();
                    }
                    killEvent(e);
                    return;
                }
            }));


            installKeyUpChangeEvent(this.focusser);
            this.focusser.bind("keyup-change input", this.bind(function(e) {
                if (this.opened()) return;
                this.open();
                if (this.showSearchInput !== false) {
                    this.search.val(this.focusser.val());
                }
                this.focusser.val("");
                killEvent(e);
            }));

            selection.delegate("abbr", "mousedown", this.bind(function (e) {
                if (!this.enabled) return;
                this.clear();
                killEventImmediately(e);
                this.close();
                this.selection.focus();
            }));

            selection.bind("mousedown", this.bind(function (e) {
                clickingInside = true;

                if (this.opened()) {
                    this.close();
                } else if (this.enabled) {
                    this.open();
                }

                killEvent(e);

                clickingInside = false;
            }));

            dropdown.bind("mousedown", this.bind(function() { this.search.focus(); }));

            selection.bind("focus", this.bind(function(e) {
                killEvent(e);
            }));

            this.focusser.bind("focus", this.bind(function(){
                this.container.addClass("select2-container-active");
            })).bind("blur", this.bind(function() {
                if (!this.opened()) {
                    this.container.removeClass("select2-container-active");
                }
            }));
            this.search.bind("focus", this.bind(function(){
                this.container.addClass("select2-container-active");
            }))

            this.initContainerWidth();
            this.setPlaceholder();

        },

        // single
        clear: function() {
            var data=this.selection.data("select2-data");
            this.opts.element.val("");
            this.selection.find("span").empty();
            this.selection.removeData("select2-data");
            this.setPlaceholder();

            this.opts.element.trigger({ type: "removed", val: this.id(data), choice: data });
            this.triggerChange({removed:data});
        },

        /**
         * Sets selection based on source element's value
         */
        // single
        initSelection: function () {
            var selected;
            if (this.opts.element.val() === "" && this.opts.element.text() === "") {
                this.close();
                this.setPlaceholder();
            } else {
                var self = this;
                this.opts.initSelection.call(null, this.opts.element, function(selected){
                    if (selected !== undefined && selected !== null) {
                        self.updateSelection(selected);
                        self.close();
                        self.setPlaceholder();
                    }
                });
            }
        },

        // single
        prepareOpts: function () {
            var opts = this.parent.prepareOpts.apply(this, arguments);

            if (opts.element.get(0).tagName.toLowerCase() === "select") {
                // install the selection initializer
                opts.initSelection = function (element, callback) {
                    var selected = element.find(":selected");
                    // a single select box always has a value, no need to null check 'selected'
                    if ($.isFunction(callback))
                        callback({id: selected.attr("value"), text: selected.text(), element:selected});
                };
            } else if ("data" in opts) {
                // install default initSelection when applied to hidden input and data is local
                opts.initSelection = opts.initSelection || function (element, callback) {
                    var id = element.val();
                    //search in data by id
                    opts.query({
                        matcher: function(term, text, el){
                            return equal(id, opts.id(el));
                        },
                        callback: !$.isFunction(callback) ? $.noop : function(filtered) {
                            callback(filtered.results.length ? filtered.results[0] : null);
                        }
                    });
                };
            }

            return opts;
        },

        // single
        getPlaceholder: function() {
            // if a placeholder is specified on a single select without the first empty option ignore it
            if (this.select) {
                if (this.select.find("option").first().text() !== "") {
                    return undefined;
                }
            }

            return this.parent.getPlaceholder.apply(this, arguments);
        },

        // single
        setPlaceholder: function () {
            var placeholder = this.getPlaceholder();

            if (this.opts.element.val() === "" && placeholder !== undefined) {

                // check for a first blank option if attached to a select
                if (this.select && this.select.find("option:first").text() !== "") return;

                this.selection.find("span").html(this.opts.escapeMarkup(placeholder));

                this.selection.addClass("select2-default");

                this.selection.find("abbr").hide();
            }
        },

        // single
        postprocessResults: function (data, initial) {
            var selected = 0, self = this, showSearchInput = true;

            // find the selected element in the result list

            this.findHighlightableChoices().each2(function (i, elm) {
                if (equal(self.id(elm.data("select2-data")), self.opts.element.val())) {
                    selected = i;
                    return false;
                }
            });

            // and highlight it

            this.highlight(selected);

            // hide the search box if this is the first we got the results and there are a few of them

            if (initial === true) {
                var min=this.opts.minimumResultsForSearch;
                showSearchInput  = min < 0 ? false : countResults(data.results) >= min;
                this.showSearch(showSearchInput);
            }

        },

        // single
        showSearch: function(showSearchInput) {
            this.showSearchInput = showSearchInput;

            this.dropdown.find(".select2-search")[showSearchInput ? "removeClass" : "addClass"]("select2-search-hidden");
            //add "select2-with-searchbox" to the container if search box is shown
            $(this.dropdown, this.container)[showSearchInput ? "addClass" : "removeClass"]("select2-with-searchbox");
        },

        // single
        onSelect: function (data, options) {
            var old = this.opts.element.val();

            this.opts.element.val(this.id(data));
            this.updateSelection(data);

            this.opts.element.trigger({ type: "selected", val: this.id(data), choice: data });

            this.close();

            if (!options || !options.noFocus)
                this.selection.focus();

            if (!equal(old, this.id(data))) { this.triggerChange(); }
        },

        // single
        updateSelection: function (data) {

            var container=this.selection.find("span"), formatted;

            this.selection.data("select2-data", data);

            container.empty();
            formatted=this.opts.formatSelection(data, container);
            if (formatted !== undefined) {
                container.append(this.opts.escapeMarkup(formatted));
            }

            this.selection.removeClass("select2-default");

            if (this.opts.allowClear && this.getPlaceholder() !== undefined) {
                this.selection.find("abbr").show();
            }
        },

        // single
        val: function () {
            var val, triggerChange = false, data = null, self = this;

            if (arguments.length === 0) {
                return this.opts.element.val();
            }

            val = arguments[0];

            if (arguments.length > 1) {
                triggerChange = arguments[1];
            }

            if (this.select) {
                this.select
                    .val(val)
                    .find(":selected").each2(function (i, elm) {
                        data = {id: elm.attr("value"), text: elm.text()};
                        return false;
                    });
                this.updateSelection(data);
                this.setPlaceholder();
                if (triggerChange) {
                    this.triggerChange();
                }
            } else {
                if (this.opts.initSelection === undefined) {
                    throw new Error("cannot call val() if initSelection() is not defined");
                }
                // val is an id. !val is true for [undefined,null,'',0] - 0 is legal
                if (!val && val !== 0) {
                    this.clear();
                    if (triggerChange) {
                        this.triggerChange();
                    }
                    return;
                }
                this.opts.element.val(val);
                this.opts.initSelection(this.opts.element, function(data){
                    self.opts.element.val(!data ? "" : self.id(data));
                    self.updateSelection(data);
                    self.setPlaceholder();
                    if (triggerChange) {
                        self.triggerChange();
                    }
                });
            }
        },

        // single
        clearSearch: function () {
            this.search.val("");
            this.focusser.val("");
        },

        // single
        data: function(value) {
            var data;

            if (arguments.length === 0) {
                data = this.selection.data("select2-data");
                if (data == undefined) data = null;
                return data;
            } else {
                if (!value || value === "") {
                    this.clear();
                } else {
                    this.opts.element.val(!value ? "" : this.id(value));
                    this.updateSelection(value);
                }
            }
        }
    });

    MultiSelect2 = clazz(AbstractSelect2, {

        // multi
        createContainer: function () {
            var container = $(document.createElement("div")).attr({
                "class": "select2-container select2-container-multi"
            }).html([
                "    <ul class='select2-choices'>",
                //"<li class='select2-search-choice'><span>California</span><a href="javascript:void(0)" class="select2-search-choice-close"></a></li>" ,
                "  <li class='select2-search-field'>" ,
                "    <input type='text' autocomplete='off' class='select2-input'>" ,
                "  </li>" ,
                "</ul>" ,
                "<div class='select2-drop select2-drop-multi' style='display:none;'>" ,
                "   <ul class='select2-results'>" ,
                "   </ul>" ,
                "</div>"].join(""));
			return container;
        },

        // multi
        prepareOpts: function () {
            var opts = this.parent.prepareOpts.apply(this, arguments);

            // TODO validate placeholder is a string if specified

            if (opts.element.get(0).tagName.toLowerCase() === "select") {
                // install sthe selection initializer
                opts.initSelection = function (element, callback) {

                    var data = [];

                    element.find(":selected").each2(function (i, elm) {
                        data.push({id: elm.attr("value"), text: elm.text(), element: elm[0]});
                    });
                    callback(data);
                };
            } else if ("data" in opts) {
                // install default initSelection when applied to hidden input and data is local
                opts.initSelection = opts.initSelection || function (element, callback) {
                    var ids = splitVal(element.val(), opts.separator);
                    //search in data by array of ids
                    opts.query({
                        matcher: function(term, text, el){
                            return $.grep(ids, function(id) {
                                return equal(id, opts.id(el));
                            }).length;
                        },
                        callback: !$.isFunction(callback) ? $.noop : function(filtered) {
                            callback(filtered.results);
                        }
                    });
                };
            }

            return opts;
        },

        // multi
        initContainer: function () {

            var selector = ".select2-choices", selection;

            this.searchContainer = this.container.find(".select2-search-field");
            this.selection = selection = this.container.find(selector);

            this.search.bind("input paste", this.bind(function() {
                if (!this.enabled) return;
                if (!this.opened()) {
                    this.open();
                }
            }));

            this.search.bind("keydown", this.bind(function (e) {
                if (!this.enabled) return;

                if (e.which === KEY.BACKSPACE && this.search.val() === "") {
                    this.close();

                    var choices,
                        selected = selection.find(".select2-search-choice-focus");
                    if (selected.length > 0) {
                        this.unselect(selected.first());
                        this.search.width(10);
                        killEvent(e);
                        return;
                    }

                    choices = selection.find(".select2-search-choice:not(.select2-locked)");
                    if (choices.length > 0) {
                        choices.last().addClass("select2-search-choice-focus");
                    }
                } else {
                    selection.find(".select2-search-choice-focus").removeClass("select2-search-choice-focus");
                }

                if (this.opened()) {
                    switch (e.which) {
                    case KEY.UP:
                    case KEY.DOWN:
                        this.moveHighlight((e.which === KEY.UP) ? -1 : 1);
                        killEvent(e);
                        return;
                    case KEY.ENTER:
                    case KEY.TAB:
                        this.selectHighlighted();
                        killEvent(e);
                        return;
                    case KEY.ESC:
                        this.cancel(e);
                        killEvent(e);
                        return;
                    }
                }

                if (e.which === KEY.TAB || KEY.isControl(e) || KEY.isFunctionKey(e)
                 || e.which === KEY.BACKSPACE || e.which === KEY.ESC) {
                    return;
                }

                if (e.which === KEY.ENTER) {
                    if (this.opts.openOnEnter === false) {
                        return;
                    } else if (e.altKey || e.ctrlKey || e.shiftKey || e.metaKey) {
                        return;
                    }
                }

                this.open();

                if (e.which === KEY.PAGE_UP || e.which === KEY.PAGE_DOWN) {
                    // prevent the page from scrolling
                    killEvent(e);
                }
            }));

            this.search.bind("keyup", this.bind(this.resizeSearch));

            this.search.bind("blur", this.bind(function(e) {
                this.container.removeClass("select2-container-active");
                this.search.removeClass("select2-focused");
                if (!this.opened()) this.clearSearch();
                e.stopImmediatePropagation();
            }));

            this.container.delegate(selector, "mousedown", this.bind(function (e) {
                if (!this.enabled) return;
                if ($(e.target).closest(".select2-search-choice").length > 0) {
                    // clicked inside a select2 search choice, do not open
                    return;
                }
                this.clearPlaceholder();
                this.open();
                this.focusSearch();
                e.preventDefault();
            }));

            this.container.delegate(selector, "focus", this.bind(function () {
                if (!this.enabled) return;
                this.container.addClass("select2-container-active");
                this.dropdown.addClass("select2-drop-active");
                this.clearPlaceholder();
            }));

            this.initContainerWidth();

            // set the placeholder if necessary
            this.clearSearch();
        },

        // multi
        enable: function() {
            if (this.enabled) return;

            this.parent.enable.apply(this, arguments);

            this.search.removeAttr("disabled");
        },

        // multi
        disable: function() {
            if (!this.enabled) return;

            this.parent.disable.apply(this, arguments);

            this.search.attr("disabled", true);
        },

        // multi
        initSelection: function () {
            var data;
            if (this.opts.element.val() === "" && this.opts.element.text() === "") {
                this.updateSelection([]);
                this.close();
                // set the placeholder if necessary
                this.clearSearch();
            }
            if (this.select || this.opts.element.val() !== "") {
                var self = this;
                this.opts.initSelection.call(null, this.opts.element, function(data){
                    if (data !== undefined && data !== null) {
                        self.updateSelection(data);
                        self.close();
                        // set the placeholder if necessary
                        self.clearSearch();
                    }
                });
            }
        },

        // multi
        clearSearch: function () {
            var placeholder = this.getPlaceholder();

            if (placeholder !== undefined  && this.getVal().length === 0 && this.search.hasClass("select2-focused") === false) {
                this.search.val(placeholder).addClass("select2-default");
                // stretch the search box to full width of the container so as much of the placeholder is visible as possible
                this.resizeSearch();
            } else {
                this.search.val("").width(10);
            }
        },

        // multi
        clearPlaceholder: function () {
            if (this.search.hasClass("select2-default")) {
                this.search.val("").removeClass("select2-default");
            }
        },

        // multi
        opening: function () {
            this.parent.opening.apply(this, arguments);

            this.clearPlaceholder();
			this.resizeSearch();
            this.focusSearch();

            this.opts.element.trigger($.Event("open"));
        },

        // multi
        close: function () {
            if (!this.opened()) return;
            this.parent.close.apply(this, arguments);
        },

        // multi
        focus: function () {
            this.close();
            this.search.focus();
            this.opts.element.triggerHandler("focus");
        },

        // multi
        isFocused: function () {
            return this.search.hasClass("select2-focused");
        },

        // multi
        updateSelection: function (data) {
            var ids = [], filtered = [], self = this;

            // filter out duplicates
            $(data).each(function () {
                if (indexOf(self.id(this), ids) < 0) {
                    ids.push(self.id(this));
                    filtered.push(this);
                }
            });
            data = filtered;

            this.selection.find(".select2-search-choice").remove();
            $(data).each(function () {
                self.addSelectedChoice(this);
            });
            self.postprocessResults();
        },

        tokenize: function() {
            var input = this.search.val();
            input = this.opts.tokenizer(input, this.data(), this.bind(this.onSelect), this.opts);
            if (input != null && input != undefined) {
                this.search.val(input);
                if (input.length > 0) {
                    this.open();
                }
            }

        },

        // multi
        onSelect: function (data, options) {
            this.addSelectedChoice(data);

            this.opts.element.trigger({ type: "selected", val: this.id(data), choice: data });

            if (this.select || !this.opts.closeOnSelect) this.postprocessResults();

            if (this.opts.closeOnSelect) {
                this.close();
                this.search.width(10);
            } else {
                if (this.countSelectableResults()>0) {
                    this.search.width(10);
                    this.resizeSearch();
                    if (this.val().length >= this.getMaximumSelectionSize()) {
                        // if we reached max selection size repaint the results so choices
                        // are replaced with the max selection reached message
                        this.updateResults(true);
                    }
                    this.positionDropdown();
                } else {
                    // if nothing left to select close
                    this.close();
                    this.search.width(10);
                }
            }

            // since its not possible to select an element that has already been
            // added we do not need to check if this is a new element before firing change
            this.triggerChange({ added: data });

            if (!options || !options.noFocus)
                this.focusSearch();
        },

        // multi
        cancel: function () {
            this.close();
            this.focusSearch();
        },

        addSelectedChoice: function (data) {
            var enableChoice = !data.locked,
                enabledItem = $(
                    "<li class='select2-search-choice'>" +
                    "    <div></div>" +
                    "    <a href='#' onclick='return false;' class='select2-search-choice-close' tabindex='-1'></a>" +
                    "</li>"),
                disabledItem = $(
                    "<li class='select2-search-choice select2-locked'>" +
                    "<div></div>" +
                    "</li>");
            var choice = enableChoice ? enabledItem : disabledItem,
                id = this.id(data),
                val = this.getVal(),
                formatted;

            formatted=this.opts.formatSelection(data, choice.find("div"));
            if (formatted != undefined) {
                choice.find("div").replaceWith("<div>"+this.opts.escapeMarkup(formatted)+"</div>");
            }

            if(enableChoice){
              choice.find(".select2-search-choice-close")
                  .bind("mousedown", killEvent)
                  .bind("click dblclick", this.bind(function (e) {
                  if (!this.enabled) return;

                  $(e.target).closest(".select2-search-choice").fadeOut('fast', this.bind(function(){
                      this.unselect($(e.target));
                      this.selection.find(".select2-search-choice-focus").removeClass("select2-search-choice-focus");
                      this.close();
                      this.focusSearch();
                  })).dequeue();
                  killEvent(e);
              })).bind("focus", this.bind(function () {
                  if (!this.enabled) return;
                  this.container.addClass("select2-container-active");
                  this.dropdown.addClass("select2-drop-active");
              }));
            }

            choice.data("select2-data", data);
            choice.insertBefore(this.searchContainer);

            val.push(id);
            this.setVal(val);
        },

        // multi
        unselect: function (selected) {
            var val = this.getVal(),
                data,
                index;

            selected = selected.closest(".select2-search-choice");

            if (selected.length === 0) {
                throw "Invalid argument: " + selected + ". Must be .select2-search-choice";
            }

            data = selected.data("select2-data");

            if (!data) {
                // prevent a race condition when the 'x' is clicked really fast repeatedly the event can be queued
                // and invoked on an element already removed
                return;
            }

            index = indexOf(this.id(data), val);

            if (index >= 0) {
                val.splice(index, 1);
                this.setVal(val);
                if (this.select) this.postprocessResults();
            }
            selected.remove();

            this.opts.element.trigger({ type: "removed", val: this.id(data), choice: data });
            this.triggerChange({ removed: data });
        },

        // multi
        postprocessResults: function () {
            var val = this.getVal(),
                choices = this.results.find(".select2-result"),
                compound = this.results.find(".select2-result-with-children"),
                self = this;

            choices.each2(function (i, choice) {
                var id = self.id(choice.data("select2-data"));
                if (indexOf(id, val) >= 0) {
                    choice.addClass("select2-selected");
                    // mark all children of the selected parent as selected
                    choice.find(".select2-result-selectable").addClass("select2-selected");
                }
            });

            compound.each2(function(i, choice) {
                // hide an optgroup if it doesnt have any selectable children
                if (!choice.is('.select2-result-selectable')
                    && choice.find(".select2-result-selectable:not(.select2-selected)").length === 0) {
                    choice.addClass("select2-selected");
                }
            });

            if (this.highlight() == -1){
                self.highlight(0);
            }

        },

        // multi
        resizeSearch: function () {
            var minimumWidth, left, maxWidth, containerLeft, searchWidth,
            	sideBorderPadding = getSideBorderPadding(this.search);

            minimumWidth = measureTextWidth(this.search) + 10;

            left = this.search.offset().left;

            maxWidth = this.selection.width();
            containerLeft = this.selection.offset().left;

            searchWidth = maxWidth - (left - containerLeft) - sideBorderPadding;

            if (searchWidth < minimumWidth) {
                searchWidth = maxWidth - sideBorderPadding;
            }

            if (searchWidth < 40) {
                searchWidth = maxWidth - sideBorderPadding;
            }

            if (searchWidth <= 0) {
              searchWidth = minimumWidth;
            }

            this.search.width(searchWidth);
        },

        // multi
        getVal: function () {
            var val;
            if (this.select) {
                val = this.select.val();
                return val === null ? [] : val;
            } else {
                val = this.opts.element.val();
                return splitVal(val, this.opts.separator);
            }
        },

        // multi
        setVal: function (val) {
            var unique;
            if (this.select) {
                this.select.val(val);
            } else {
                unique = [];
                // filter out duplicates
                $(val).each(function () {
                    if (indexOf(this, unique) < 0) unique.push(this);
                });
                this.opts.element.val(unique.length === 0 ? "" : unique.join(this.opts.separator));
            }
        },

        // multi
        val: function () {
            var val, triggerChange = false, data = [], self=this;

            if (arguments.length === 0) {
                return this.getVal();
            }

            val = arguments[0];

            if (arguments.length > 1) {
                triggerChange = arguments[1];
            }

            // val is an id. !val is true for [undefined,null,'',0] - 0 is legal
            if (!val && val !== 0) {
                this.opts.element.val("");
                this.updateSelection([]);
                this.clearSearch();
                if (triggerChange) {
                    this.triggerChange();
                }
                return;
            }

            // val is a list of ids
            this.setVal(val);

            if (this.select) {
                this.opts.initSelection(this.select, this.bind(this.updateSelection));
                if (triggerChange) {
                    this.triggerChange();
                }
            } else {
                if (this.opts.initSelection === undefined) {
                    throw new Error("val() cannot be called if initSelection() is not defined");
                }

                this.opts.initSelection(this.opts.element, function(data){
                    var ids=$(data).map(self.id);
                    self.setVal(ids);
                    self.updateSelection(data);
                    self.clearSearch();
                    if (triggerChange) {
                        self.triggerChange();
                    }
                });
            }
            this.clearSearch();
        },

        // multi
        onSortStart: function() {
            if (this.select) {
                throw new Error("Sorting of elements is not supported when attached to <select>. Attach to <input type='hidden'/> instead.");
            }

            // collapse search field into 0 width so its container can be collapsed as well
            this.search.width(0);
            // hide the container
            this.searchContainer.hide();
        },

        // multi
        onSortEnd:function() {

            var val=[], self=this;

            // show search and move it to the end of the list
            this.searchContainer.show();
            // make sure the search container is the last item in the list
            this.searchContainer.appendTo(this.searchContainer.parent());
            // since we collapsed the width in dragStarted, we resize it here
            this.resizeSearch();

            // update selection

            this.selection.find(".select2-search-choice").each(function() {
                val.push(self.opts.id($(this).data("select2-data")));
            });
            this.setVal(val);
            this.triggerChange();
        },

        // multi
        data: function(values) {
            var self=this, ids;
            if (arguments.length === 0) {
                 return this.selection
                     .find(".select2-search-choice")
                     .map(function() { return $(this).data("select2-data"); })
                     .get();
            } else {
                if (!values) { values = []; }
                ids = $.map(values, function(e) { return self.opts.id(e); });
                this.setVal(ids);
                this.updateSelection(values);
                this.clearSearch();
            }
        }
    });

    $.fn.select2 = function () {

        var args = Array.prototype.slice.call(arguments, 0),
            opts,
            select2,
            value, multiple, allowedMethods = ["val", "destroy", "opened", "open", "close", "focus", "isFocused", "container", "onSortStart", "onSortEnd", "enable", "disable", "positionDropdown", "data"];

        this.each(function () {
            if (args.length === 0 || typeof(args[0]) === "object") {
                opts = args.length === 0 ? {} : $.extend({}, args[0]);
                opts.element = $(this);

                if (opts.element.get(0).tagName.toLowerCase() === "select") {
                    multiple = opts.element.attr("multiple");
                } else {
                    multiple = opts.multiple || false;
                    if ("tags" in opts) {opts.multiple = multiple = true;}
                }

                select2 = multiple ? new MultiSelect2() : new SingleSelect2();
                select2.init(opts);
            } else if (typeof(args[0]) === "string") {

                if (indexOf(args[0], allowedMethods) < 0) {
                    throw "Unknown method: " + args[0];
                }

                value = undefined;
                select2 = $(this).data("select2");
                if (select2 === undefined) return;
                if (args[0] === "container") {
                    value=select2.container;
                } else {
                    value = select2[args[0]].apply(select2, args.slice(1));
                }
                if (value !== undefined) {return false;}
            } else {
                throw "Invalid arguments to select2 plugin: " + args;
            }
        });
        return (value === undefined) ? this : value;
    };

    // plugin defaults, accessible to users
    $.fn.select2.defaults = {
        width: "copy",
        loadMorePadding: 0,
        closeOnSelect: true,
        openOnEnter: true,
        containerCss: {},
        dropdownCss: {},
        containerCssClass: "",
        dropdownCssClass: "",
        formatResult: function(result, container, query, escapeMarkup) {
            var markup=[];
            markMatch(result.text, query.term, markup, escapeMarkup);
            return markup.join("");
        },
        formatSelection: function (data, container) {
            return data ? data.text : undefined;
        },
        sortResults: function (results, container, query) {
            return results;
        },
        formatResultCssClass: function(data) {return undefined;},
        formatNoMatches: function () { return "No matches found"; },
        formatInputTooShort: function (input, min) { var n = min - input.length; return "Please enter " + n + " more character" + (n == 1? "" : "s"); },
        formatInputTooLong: function (input, max) { var n = input.length - max; return "Please enter " + n + " less character" + (n == 1? "" : "s"); },
        formatSelectionTooBig: function (limit) { return "You can only select " + limit + " item" + (limit == 1 ? "" : "s"); },
        formatLoadMore: function (pageNumber) { return "Loading more results..."; },
        formatSearching: function () { return "Searching..."; },
        minimumResultsForSearch: 0,
        minimumInputLength: 0,
        maximumInputLength: null,
        maximumSelectionSize: 0,
        id: function (e) { return e.id; },
        matcher: function(term, text) {
            return text.toUpperCase().indexOf(term.toUpperCase()) >= 0;
        },
        separator: ",",
        tokenSeparators: [],
        tokenizer: defaultTokenizer,
        escapeMarkup: function (markup) {
            var replace_map = {
                '\\': '&#92;',
                '&': '&amp;',
                '<': '&lt;',
                '>': '&gt;',
                '"': '&quot;',
                "'": '&apos;',
                "/": '&#47;'
            };

            return String(markup).replace(/[&<>"'/\\]/g, function (match) {
                    return replace_map[match[0]];
            });
        },
        blurOnChange: false,
        selectOnBlur: false,
        adaptContainerCssClass: function(c) { return c; },
        adaptDropdownCssClass: function(c) { return null; }
    };

    // exports
    window.Select2 = {
        query: {
            ajax: ajax,
            local: local,
            tags: tags
        }, util: {
            debounce: debounce,
            markMatch: markMatch
        }, "class": {
            "abstract": AbstractSelect2,
            "single": SingleSelect2,
            "multi": MultiSelect2
        }
    };

}(jQuery));

/*
 Script: jQuery.xdretroclock.js
 Plug-in for jQuery retro clock
 */

/*
 Author:
 Valeriy Chupurnov <leroy@xdan.ru>, <http://xdan.ru>
 License:
 LGPL - Lesser General Public License

 Posted <http://xdan.ru/project/xdretroclock/index.html>
 */
(function ($) {
    jQuery.fn.xdRetroClock = jQuery.fn.xdretroclock = function (options) {
        var settings = {
            digitImages: 1,
            digitWidth: 25,
            digitHeight: 30,
            showHour: true,
            showMinute: true,
            showSecond: true,
            showSeparator: false,
            am: false,
            tzoneOffset: 0,
            speedFlip: 100
        };

        var getmt = function (up) {
            return (-(((up) ? up - 1 : 9) * options.digitImages - 1) * options.digitHeight);
        }
        var div = function (val, by) {
            return (val - val % by) / by;
        }
        var current = {
            "h1": -1,
            "h2": -1,
            "m1": -1,
            "m2": -1,
            "s1": -1,
            "s2": -1
        };
        options = $.extend(settings, options);
        var setmargin = function ($box, mt, rec) {
            if (rec == 1) $box.css('margin-top', mt + 'px');
            $box.css('margin-top', (mt - options.digitHeight) + 'px');
            if (rec < options.digitImages)
                setTimeout(function () {
                    setmargin($box, mt - options.digitHeight, ++rec);
                }, options.speedFlip);
        }

        $.fn.time = function (time) {
            options.time = time;

        }

        var calcTime = function ($box) {
            var now = options.time;
            if (now === undefined) {
                now = new Date();
            }
            var h = (now.getHours() + options.tzoneOffset) % (options.am ? 12 : 24),
                h1 = div(h, 10),
                h2 = h % 10,
                m1 = div((now.getMinutes() + 1 ), 10),
                m2 = (now.getMinutes() + 1) % 10,
                s1 = div(now.getSeconds(), 10),
                s2 = now.getSeconds() % 10;
            if (options.showHour && h2 != current.h2) {
                setmargin($box.find('.hourRight div'), getmt(h2), 1);
                current.h2 = h2;
            }
            if (options.showHour && h1 != current.h1) {
                setmargin($box.find('.hourLeft div'), getmt(h1), 1);
                current.h1 = h1;
            }
            if (options.showMinute && m2 != current.m2) {
                setmargin($box.find('.minuteRight div'), getmt(m2), 1);
                current.m2 = m2;
            }
            if (options.showMinute && m1 != current.m1) {
                setmargin($box.find('.minuteLeft div'), getmt(m1), 1);
                current.m1 = m1;
            }

            if (options.showSecond && s2 != current.s2) {
                setmargin($box.find('.secondRight div'), getmt(s2), 1);
                current.s2 = s2;
            }
            if (options.showSecond && s1 != current.s1) {
                setmargin($box.find('.secondLeft div'), getmt(s1), 1);
                current.s1 = s1;
            }
        }
        return this.each(function () {
            var $box = $(this);
            if ($box.hasClass('retroclock') == false) {
                $box.addClass('retroclock');
            }

            if (options.showHour) {
                $box.append('<div class="xdgroup"><div class="xddigit hourLeft"><div></div></div><div class="xddigit hourRight"><div></div></div></div>');
            }
            if (options.showHour && (options.showMinute || options.showSecond) && options.showSeparator)
                $box.append('<div class="rcSeparator"></div>');
            if (options.showMinute) {
                $box.append('<div class="xdgroup"><div class="xddigit minuteLeft"><div></div></div><div class="xddigit minuteRight"><div></div></div></div>');
            }
            if (options.showMinute && options.showSecond && options.showSeparator)
                $box.append('<div class="rcSeparator"></div>');
            if (options.showSecond) {
                $box.append('<div class="xdgroup"><div class="xddigit secondLeft"><div></div></div><div class="xddigit secondRight"><div></div></div></div>');
            }
            // if( options.showHour || options.showMinute || options.showSecond )
            // 	$box.append('<div style="clear:both;float:none;"></div>');

            setInterval(function () {

                calcTime($box);
            }, 1000)
            calcTime($box);
        });
    }
})(jQuery);

/*
 * jQuery FlexSlider v2.7.2
 * Copyright 2012 WooThemes
 * Contributing Author: Tyler Smith
 */
;
(function ($) {

  var focused = true;

  //FlexSlider: Object Instance
  $.flexslider = function(el, options) {
    var slider = $(el);

    // making variables public

    //if rtl value was not passed and html is in rtl..enable it by default.
    if(typeof options.rtl=='undefined' && $('html').attr('dir')=='rtl'){
      options.rtl=true;
    }
    slider.vars = $.extend({}, $.flexslider.defaults, options);

    var namespace = slider.vars.namespace,
        msGesture = window.navigator && window.navigator.msPointerEnabled && window.MSGesture,
        touch = (( "ontouchstart" in window ) || msGesture || window.DocumentTouch && document instanceof DocumentTouch) && slider.vars.touch,
        // deprecating this idea, as devices are being released with both of these events
        eventType = "click touchend MSPointerUp keyup",
        watchedEvent = "",
        watchedEventClearTimer,
        vertical = slider.vars.direction === "vertical",
        reverse = slider.vars.reverse,
        carousel = (slider.vars.itemWidth > 0),
        fade = slider.vars.animation === "fade",
        asNav = slider.vars.asNavFor !== "",
        methods = {};

    // Store a reference to the slider object
    $.data(el, "flexslider", slider);

    // Private slider methods
    methods = {
      init: function() {
        slider.animating = false;
        // Get current slide and make sure it is a number
        slider.currentSlide = parseInt( ( slider.vars.startAt ? slider.vars.startAt : 0), 10 );
        if ( isNaN( slider.currentSlide ) ) { slider.currentSlide = 0; }
        slider.animatingTo = slider.currentSlide;
        slider.atEnd = (slider.currentSlide === 0 || slider.currentSlide === slider.last);
        slider.containerSelector = slider.vars.selector.substr(0,slider.vars.selector.search(' '));
        slider.slides = $(slider.vars.selector, slider);
        slider.container = $(slider.containerSelector, slider);
        slider.count = slider.slides.length;
        // SYNC:
        slider.syncExists = $(slider.vars.sync).length > 0;
        // SLIDE:
        if (slider.vars.animation === "slide") { slider.vars.animation = "swing"; }
        slider.prop = (vertical) ? "top" : ( slider.vars.rtl ? "marginRight" : "marginLeft" );
        slider.args = {};
        // SLIDESHOW:
        slider.manualPause = false;
        slider.stopped = false;
        //PAUSE WHEN INVISIBLE
        slider.started = false;
        slider.startTimeout = null;
        // TOUCH/USECSS:
        slider.transitions = !slider.vars.video && !fade && slider.vars.useCSS && (function() {
          var obj = document.createElement('div'),
              props = ['perspectiveProperty', 'WebkitPerspective', 'MozPerspective', 'OPerspective', 'msPerspective'];
          for (var i in props) {
            if ( obj.style[ props[i] ] !== undefined ) {
              slider.pfx = props[i].replace('Perspective','').toLowerCase();
              slider.prop = "-" + slider.pfx + "-transform";
              return true;
            }
          }
          return false;
        }());
        slider.isFirefox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1;
        slider.ensureAnimationEnd = '';
        // CONTROLSCONTAINER:
        if (slider.vars.controlsContainer !== "") slider.controlsContainer = $(slider.vars.controlsContainer).length > 0 && $(slider.vars.controlsContainer);
        // MANUAL:
        if (slider.vars.manualControls !== "") slider.manualControls = $(slider.vars.manualControls).length > 0 && $(slider.vars.manualControls);

        // CUSTOM DIRECTION NAV:
        if (slider.vars.customDirectionNav !== "") slider.customDirectionNav = $(slider.vars.customDirectionNav).length === 2 && $(slider.vars.customDirectionNav);

        // RANDOMIZE:
        if (slider.vars.randomize) {
          slider.slides.sort(function() { return (Math.round(Math.random())-0.5); });
          slider.container.empty().append(slider.slides);
        }

        slider.doMath();

        // INIT
        slider.setup("init");

        // CONTROLNAV:
        if (slider.vars.controlNav) { methods.controlNav.setup(); }

        // DIRECTIONNAV:
        if (slider.vars.directionNav) { methods.directionNav.setup(); }

        // KEYBOARD:
        if (slider.vars.keyboard && ($(slider.containerSelector).length === 1 || slider.vars.multipleKeyboard)) {
          $(document).bind('keyup', function(event) {
            var keycode = event.keyCode;
            if (!slider.animating && (keycode === 39 || keycode === 37)) {
              var target = (slider.vars.rtl?
                                ((keycode === 37) ? slider.getTarget('next') :
                                (keycode === 39) ? slider.getTarget('prev') : false)
                                :
                                ((keycode === 39) ? slider.getTarget('next') :
                                (keycode === 37) ? slider.getTarget('prev') : false)
                                )
                                ;
              slider.flexAnimate(target, slider.vars.pauseOnAction);
            }
          });
        }
        // MOUSEWHEEL:
        if (slider.vars.mousewheel) {
          slider.bind('mousewheel', function(event, delta, deltaX, deltaY) {
            event.preventDefault();
            var target = (delta < 0) ? slider.getTarget('next') : slider.getTarget('prev');
            slider.flexAnimate(target, slider.vars.pauseOnAction);
          });
        }

        // PAUSEPLAY
        if (slider.vars.pausePlay) { methods.pausePlay.setup(); }

        //PAUSE WHEN INVISIBLE
        if (slider.vars.slideshow && slider.vars.pauseInvisible) { methods.pauseInvisible.init(); }

        // SLIDSESHOW
        if (slider.vars.slideshow) {
          if (slider.vars.pauseOnHover) {
            slider.hover(function() {
              if (!slider.manualPlay && !slider.manualPause) { slider.pause(); }
            }, function() {
              if (!slider.manualPause && !slider.manualPlay && !slider.stopped) { slider.play(); }
            });
          }
          // initialize animation
          //If we're visible, or we don't use PageVisibility API
          if(!slider.vars.pauseInvisible || !methods.pauseInvisible.isHidden()) {
            (slider.vars.initDelay > 0) ? slider.startTimeout = setTimeout(slider.play, slider.vars.initDelay) : slider.play();
          }
        }

        // ASNAV:
        if (asNav) { methods.asNav.setup(); }

        // TOUCH
        if (touch && slider.vars.touch) { methods.touch(); }

        // FADE&&SMOOTHHEIGHT || SLIDE:
        if (!fade || (fade && slider.vars.smoothHeight)) { $(window).bind("resize orientationchange focus", methods.resize); }

        slider.find("img").attr("draggable", "false");

        // API: start() Callback
        setTimeout(function(){
          slider.vars.start(slider);
        }, 200);
      },
      asNav: {
        setup: function() {
          slider.asNav = true;
          slider.animatingTo = Math.floor(slider.currentSlide/slider.move);
          slider.currentItem = slider.currentSlide;
          slider.slides.removeClass(namespace + "active-slide").eq(slider.currentItem).addClass(namespace + "active-slide");
          if(!msGesture){
              slider.slides.on(eventType, function(e){
                e.preventDefault();
                var $slide = $(this),
                    target = $slide.index();
                var posFromX;
                if(slider.vars.rtl){
                  posFromX = -1*($slide.offset().right - $(slider).scrollLeft()); // Find position of slide relative to right of slider container
                }
                else
                {
                  posFromX = $slide.offset().left - $(slider).scrollLeft(); // Find position of slide relative to left of slider container
                }
                if( posFromX <= 0 && $slide.hasClass( namespace + 'active-slide' ) ) {
                  slider.flexAnimate(slider.getTarget("prev"), true);
                } else if (!$(slider.vars.asNavFor).data('flexslider').animating && !$slide.hasClass(namespace + "active-slide")) {
                  slider.direction = (slider.currentItem < target) ? "next" : "prev";
                  slider.flexAnimate(target, slider.vars.pauseOnAction, false, true, true);
                }
              });
          }else{
              el._slider = slider;
              slider.slides.each(function (){
                  var that = this;
                  that._gesture = new MSGesture();
                  that._gesture.target = that;
                  that.addEventListener("MSPointerDown", function (e){
                      e.preventDefault();
                      if(e.currentTarget._gesture) {
                        e.currentTarget._gesture.addPointer(e.pointerId);
                      }
                  }, false);
                  that.addEventListener("MSGestureTap", function (e){
                      e.preventDefault();
                      var $slide = $(this),
                          target = $slide.index();
                      if (!$(slider.vars.asNavFor).data('flexslider').animating && !$slide.hasClass('active')) {
                          slider.direction = (slider.currentItem < target) ? "next" : "prev";
                          slider.flexAnimate(target, slider.vars.pauseOnAction, false, true, true);
                      }
                  });
              });
          }
        }
      },
      controlNav: {
        setup: function() {
          if (!slider.manualControls) {
            methods.controlNav.setupPaging();
          } else { // MANUALCONTROLS:
            methods.controlNav.setupManual();
          }
        },
        setupPaging: function() {
          var type = (slider.vars.controlNav === "thumbnails") ? 'control-thumbs' : 'control-paging',
              j = 1,
              item,
              slide;

          slider.controlNavScaffold = $('<ol class="'+ namespace + 'control-nav ' + namespace + type + '"></ol>');

          if (slider.pagingCount > 1) {
            for (var i = 0; i < slider.pagingCount; i++) {
              slide = slider.slides.eq(i);

              if ( undefined === slide.attr( 'data-thumb-alt' ) ) { 
                slide.attr( 'data-thumb-alt', '' ); 
              }
              
              item = $( '<a></a>' ).attr( 'href', '#' ).text( j );
              if ( slider.vars.controlNav === "thumbnails" ) {
                item = $( '<img/>' ).attr( 'src', slide.attr( 'data-thumb' ) );
              }
              
              if ( '' !== slide.attr( 'data-thumb-alt' ) ) {
                item.attr( 'alt', slide.attr( 'data-thumb-alt' ) );
              }

              if ( 'thumbnails' === slider.vars.controlNav && true === slider.vars.thumbCaptions ) {
                var captn = slide.attr( 'data-thumbcaption' );
                if ( '' !== captn && undefined !== captn ) { 
                  var caption = $('<span></span>' ).addClass( namespace + 'caption' ).text( captn );
                  item.append( caption );
                }
              }
              
              var liElement = $( '<li>' );
              item.appendTo( liElement );
              liElement.append( '</li>' );

              slider.controlNavScaffold.append(liElement);
              j++;

            }
          }

          // CONTROLSCONTAINER:
          (slider.controlsContainer) ? $(slider.controlsContainer).append(slider.controlNavScaffold) : slider.append(slider.controlNavScaffold);
          methods.controlNav.set();

          methods.controlNav.active();

          slider.controlNavScaffold.delegate('a, img', eventType, function(event) {
            event.preventDefault();

            if (watchedEvent === "" || watchedEvent === event.type) {
              var $this = $(this),
                  target = slider.controlNav.index($this);

              if (!$this.hasClass(namespace + 'active')) {
                slider.direction = (target > slider.currentSlide) ? "next" : "prev";
                slider.flexAnimate(target, slider.vars.pauseOnAction);
              }
            }

            // setup flags to prevent event duplication
            if (watchedEvent === "") {
              watchedEvent = event.type;
            }
            methods.setToClearWatchedEvent();

          });
        },
        setupManual: function() {
          slider.controlNav = slider.manualControls;
          methods.controlNav.active();

          slider.controlNav.bind(eventType, function(event) {
            event.preventDefault();

            if (watchedEvent === "" || watchedEvent === event.type) {
              var $this = $(this),
                  target = slider.controlNav.index($this);

              if (!$this.hasClass(namespace + 'active')) {
                (target > slider.currentSlide) ? slider.direction = "next" : slider.direction = "prev";
                slider.flexAnimate(target, slider.vars.pauseOnAction);
              }
            }

            // setup flags to prevent event duplication
            if (watchedEvent === "") {
              watchedEvent = event.type;
            }
            methods.setToClearWatchedEvent();
          });
        },
        set: function() {
          var selector = (slider.vars.controlNav === "thumbnails") ? 'img' : 'a';
          slider.controlNav = $('.' + namespace + 'control-nav li ' + selector, (slider.controlsContainer) ? slider.controlsContainer : slider);
        },
        active: function() {
          slider.controlNav.removeClass(namespace + "active").eq(slider.animatingTo).addClass(namespace + "active");
        },
        update: function(action, pos) {
          if (slider.pagingCount > 1 && action === "add") {
            slider.controlNavScaffold.append($('<li><a href="#">' + slider.count + '</a></li>'));
          } else if (slider.pagingCount === 1) {
            slider.controlNavScaffold.find('li').remove();
          } else {
            slider.controlNav.eq(pos).closest('li').remove();
          }
          methods.controlNav.set();
          (slider.pagingCount > 1 && slider.pagingCount !== slider.controlNav.length) ? slider.update(pos, action) : methods.controlNav.active();
        }
      },
      directionNav: {
        setup: function() {
          var directionNavScaffold = $('<ul class="' + namespace + 'direction-nav"><li class="' + namespace + 'nav-prev"><a class="' + namespace + 'prev" href="#">' + slider.vars.prevText + '</a></li><li class="' + namespace + 'nav-next"><a class="' + namespace + 'next" href="#">' + slider.vars.nextText + '</a></li></ul>');

          // CUSTOM DIRECTION NAV:
          if (slider.customDirectionNav) {
            slider.directionNav = slider.customDirectionNav;
          // CONTROLSCONTAINER:
          } else if (slider.controlsContainer) {
            $(slider.controlsContainer).append(directionNavScaffold);
            slider.directionNav = $('.' + namespace + 'direction-nav li a', slider.controlsContainer);
          } else {
            slider.append(directionNavScaffold);
            slider.directionNav = $('.' + namespace + 'direction-nav li a', slider);
          }

          methods.directionNav.update();

          slider.directionNav.bind(eventType, function(event) {
            event.preventDefault();
            var target;

            if (watchedEvent === "" || watchedEvent === event.type) {
              target = ($(this).hasClass(namespace + 'next')) ? slider.getTarget('next') : slider.getTarget('prev');
              slider.flexAnimate(target, slider.vars.pauseOnAction);
            }

            // setup flags to prevent event duplication
            if (watchedEvent === "") {
              watchedEvent = event.type;
            }
            methods.setToClearWatchedEvent();
          });
        },
        update: function() {
          var disabledClass = namespace + 'disabled';
          if (slider.pagingCount === 1) {
            slider.directionNav.addClass(disabledClass).attr('tabindex', '-1');
          } else if (!slider.vars.animationLoop) {
            if (slider.animatingTo === 0) {
              slider.directionNav.removeClass(disabledClass).filter('.' + namespace + "prev").addClass(disabledClass).attr('tabindex', '-1');
            } else if (slider.animatingTo === slider.last) {
              slider.directionNav.removeClass(disabledClass).filter('.' + namespace + "next").addClass(disabledClass).attr('tabindex', '-1');
            } else {
              slider.directionNav.removeClass(disabledClass).removeAttr('tabindex');
            }
          } else {
            slider.directionNav.removeClass(disabledClass).removeAttr('tabindex');
          }
        }
      },
      pausePlay: {
        setup: function() {
          var pausePlayScaffold = $('<div class="' + namespace + 'pauseplay"><a href="#"></a></div>');

          // CONTROLSCONTAINER:
          if (slider.controlsContainer) {
            slider.controlsContainer.append(pausePlayScaffold);
            slider.pausePlay = $('.' + namespace + 'pauseplay a', slider.controlsContainer);
          } else {
            slider.append(pausePlayScaffold);
            slider.pausePlay = $('.' + namespace + 'pauseplay a', slider);
          }

          methods.pausePlay.update((slider.vars.slideshow) ? namespace + 'pause' : namespace + 'play');

          slider.pausePlay.bind(eventType, function(event) {
            event.preventDefault();

            if (watchedEvent === "" || watchedEvent === event.type) {
              if ($(this).hasClass(namespace + 'pause')) {
                slider.manualPause = true;
                slider.manualPlay = false;
                slider.pause();
              } else {
                slider.manualPause = false;
                slider.manualPlay = true;
                slider.play();
              }
            }

            // setup flags to prevent event duplication
            if (watchedEvent === "") {
              watchedEvent = event.type;
            }
            methods.setToClearWatchedEvent();
          });
        },
        update: function(state) {
          (state === "play") ? slider.pausePlay.removeClass(namespace + 'pause').addClass(namespace + 'play').html(slider.vars.playText) : slider.pausePlay.removeClass(namespace + 'play').addClass(namespace + 'pause').html(slider.vars.pauseText);
        }
      },
      touch: function() {
        var startX,
          startY,
          offset,
          cwidth,
          dx,
          startT,
          onTouchStart,
          onTouchMove,
          onTouchEnd,
          scrolling = false,
          localX = 0,
          localY = 0,
          accDx = 0;

        if(!msGesture){
            onTouchStart = function(e) {
              if (slider.animating) {
                e.preventDefault();
              } else if ( ( window.navigator.msPointerEnabled ) || e.touches.length === 1 ) {
                slider.pause();
                // CAROUSEL:
                cwidth = (vertical) ? slider.h : slider. w;
                startT = Number(new Date());
                // CAROUSEL:

                // Local vars for X and Y points.
                localX = e.touches[0].pageX;
                localY = e.touches[0].pageY;

                offset = (carousel && reverse && slider.animatingTo === slider.last) ? 0 :
                         (carousel && reverse) ? slider.limit - (((slider.itemW + slider.vars.itemMargin) * slider.move) * slider.animatingTo) :
                         (carousel && slider.currentSlide === slider.last) ? slider.limit :
                         (carousel) ? ((slider.itemW + slider.vars.itemMargin) * slider.move) * slider.currentSlide :
                         (reverse) ? (slider.last - slider.currentSlide + slider.cloneOffset) * cwidth : (slider.currentSlide + slider.cloneOffset) * cwidth;
                startX = (vertical) ? localY : localX;
                startY = (vertical) ? localX : localY;
                el.addEventListener('touchmove', onTouchMove, false);
                el.addEventListener('touchend', onTouchEnd, false);
              }
            };

            onTouchMove = function(e) {
              // Local vars for X and Y points.

              localX = e.touches[0].pageX;
              localY = e.touches[0].pageY;

              dx = (vertical) ? startX - localY : (slider.vars.rtl?-1:1)*(startX - localX);
              scrolling = (vertical) ? (Math.abs(dx) < Math.abs(localX - startY)) : (Math.abs(dx) < Math.abs(localY - startY));
              var fxms = 500;

              if ( ! scrolling || Number( new Date() ) - startT > fxms ) {
                e.preventDefault();
                if (!fade && slider.transitions) {
                  if (!slider.vars.animationLoop) {
                    dx = dx/((slider.currentSlide === 0 && dx < 0 || slider.currentSlide === slider.last && dx > 0) ? (Math.abs(dx)/cwidth+2) : 1);
                  }
                  slider.setProps(offset + dx, "setTouch");
                }
              }
            };

            onTouchEnd = function(e) {
              // finish the touch by undoing the touch session
              el.removeEventListener('touchmove', onTouchMove, false);

              if (slider.animatingTo === slider.currentSlide && !scrolling && !(dx === null)) {
                var updateDx = (reverse) ? -dx : dx,
                    target = (updateDx > 0) ? slider.getTarget('next') : slider.getTarget('prev');

                if (slider.canAdvance(target) && (Number(new Date()) - startT < 550 && Math.abs(updateDx) > 50 || Math.abs(updateDx) > cwidth/2)) {
                  slider.flexAnimate(target, slider.vars.pauseOnAction);
                } else {
                  if (!fade) { slider.flexAnimate(slider.currentSlide, slider.vars.pauseOnAction, true); }
                }
              }
              el.removeEventListener('touchend', onTouchEnd, false);

              startX = null;
              startY = null;
              dx = null;
              offset = null;
            };

            el.addEventListener('touchstart', onTouchStart, false);
        }else{
            el.style.msTouchAction = "none";
            el._gesture = new MSGesture();
            el._gesture.target = el;
            el.addEventListener("MSPointerDown", onMSPointerDown, false);
            el._slider = slider;
            el.addEventListener("MSGestureChange", onMSGestureChange, false);
            el.addEventListener("MSGestureEnd", onMSGestureEnd, false);

            function onMSPointerDown(e){
                e.stopPropagation();
                if (slider.animating) {
                    e.preventDefault();
                }else{
                    slider.pause();
                    el._gesture.addPointer(e.pointerId);
                    accDx = 0;
                    cwidth = (vertical) ? slider.h : slider. w;
                    startT = Number(new Date());
                    // CAROUSEL:

                    offset = (carousel && reverse && slider.animatingTo === slider.last) ? 0 :
                        (carousel && reverse) ? slider.limit - (((slider.itemW + slider.vars.itemMargin) * slider.move) * slider.animatingTo) :
                            (carousel && slider.currentSlide === slider.last) ? slider.limit :
                                (carousel) ? ((slider.itemW + slider.vars.itemMargin) * slider.move) * slider.currentSlide :
                                    (reverse) ? (slider.last - slider.currentSlide + slider.cloneOffset) * cwidth : (slider.currentSlide + slider.cloneOffset) * cwidth;
                }
            }

            function onMSGestureChange(e) {
                e.stopPropagation();
                var slider = e.target._slider;
                if(!slider){
                    return;
                }
                var transX = -e.translationX,
                    transY = -e.translationY;

                //Accumulate translations.
                accDx = accDx + ((vertical) ? transY : transX);
                dx = (slider.vars.rtl?-1:1)*accDx;
                scrolling = (vertical) ? (Math.abs(accDx) < Math.abs(-transX)) : (Math.abs(accDx) < Math.abs(-transY));

                if(e.detail === e.MSGESTURE_FLAG_INERTIA){
                    setImmediate(function (){
                        el._gesture.stop();
                    });

                    return;
                }

                if (!scrolling || Number(new Date()) - startT > 500) {
                    e.preventDefault();
                    if (!fade && slider.transitions) {
                        if (!slider.vars.animationLoop) {
                            dx = accDx / ((slider.currentSlide === 0 && accDx < 0 || slider.currentSlide === slider.last && accDx > 0) ? (Math.abs(accDx) / cwidth + 2) : 1);
                        }
                        slider.setProps(offset + dx, "setTouch");
                    }
                }
            }

            function onMSGestureEnd(e) {
                e.stopPropagation();
                var slider = e.target._slider;
                if(!slider){
                    return;
                }
                if (slider.animatingTo === slider.currentSlide && !scrolling && !(dx === null)) {
                    var updateDx = (reverse) ? -dx : dx,
                        target = (updateDx > 0) ? slider.getTarget('next') : slider.getTarget('prev');

                    if (slider.canAdvance(target) && (Number(new Date()) - startT < 550 && Math.abs(updateDx) > 50 || Math.abs(updateDx) > cwidth/2)) {
                        slider.flexAnimate(target, slider.vars.pauseOnAction);
                    } else {
                        if (!fade) { slider.flexAnimate(slider.currentSlide, slider.vars.pauseOnAction, true); }
                    }
                }

                startX = null;
                startY = null;
                dx = null;
                offset = null;
                accDx = 0;
            }
        }
      },
      resize: function() {
        if (!slider.animating && slider.is(':visible')) {
          if (!carousel) { slider.doMath(); }

          if (fade) {
            // SMOOTH HEIGHT:
            methods.smoothHeight();
          } else if (carousel) { //CAROUSEL:
            slider.slides.width(slider.computedW);
            slider.update(slider.pagingCount);
            slider.setProps();
          }
          else if (vertical) { //VERTICAL:
            slider.viewport.height(slider.h);
            slider.setProps(slider.h, "setTotal");
          } else {
            // SMOOTH HEIGHT:
            if (slider.vars.smoothHeight) { methods.smoothHeight(); }
            slider.newSlides.width(slider.computedW);
            slider.setProps(slider.computedW, "setTotal");
          }
        }
      },
      smoothHeight: function(dur) {
        if (!vertical || fade) {
          var $obj = (fade) ? slider : slider.viewport;
          (dur) ? $obj.animate({"height": slider.slides.eq(slider.animatingTo).innerHeight()}, dur) : $obj.innerHeight(slider.slides.eq(slider.animatingTo).innerHeight());
        }
      },
      sync: function(action) {
        var $obj = $(slider.vars.sync).data("flexslider"),
            target = slider.animatingTo;

        switch (action) {
          case "animate": $obj.flexAnimate(target, slider.vars.pauseOnAction, false, true); break;
          case "play": if (!$obj.playing && !$obj.asNav) { $obj.play(); } break;
          case "pause": $obj.pause(); break;
        }
      },
      uniqueID: function($clone) {
        // Append _clone to current level and children elements with id attributes
        $clone.filter( '[id]' ).add($clone.find( '[id]' )).each(function() {
          var $this = $(this);
          $this.attr( 'id', $this.attr( 'id' ) + '_clone' );
        });
        return $clone;
      },
      pauseInvisible: {
        visProp: null,
        init: function() {
          var visProp = methods.pauseInvisible.getHiddenProp();
          if (visProp) {
            var evtname = visProp.replace(/[H|h]idden/,'') + 'visibilitychange';
            document.addEventListener(evtname, function() {
              if (methods.pauseInvisible.isHidden()) {
                if(slider.startTimeout) {
                  clearTimeout(slider.startTimeout); //If clock is ticking, stop timer and prevent from starting while invisible
                } else {
                  slider.pause(); //Or just pause
                }
              }
              else {
                if(slider.started) {
                  slider.play(); //Initiated before, just play
                } else {
                  if (slider.vars.initDelay > 0) {
                    setTimeout(slider.play, slider.vars.initDelay);
                  } else {
                    slider.play(); //Didn't init before: simply init or wait for it
                  }
                }
              }
            });
          }
        },
        isHidden: function() {
          var prop = methods.pauseInvisible.getHiddenProp();
          if (!prop) {
            return false;
          }
          return document[prop];
        },
        getHiddenProp: function() {
          var prefixes = ['webkit','moz','ms','o'];
          // if 'hidden' is natively supported just return it
          if ('hidden' in document) {
            return 'hidden';
          }
          // otherwise loop over all the known prefixes until we find one
          for ( var i = 0; i < prefixes.length; i++ ) {
              if ((prefixes[i] + 'Hidden') in document) {
                return prefixes[i] + 'Hidden';
              }
          }
          // otherwise it's not supported
          return null;
        }
      },
      setToClearWatchedEvent: function() {
        clearTimeout(watchedEventClearTimer);
        watchedEventClearTimer = setTimeout(function() {
          watchedEvent = "";
        }, 3000);
      }
    };

    // public methods
    slider.flexAnimate = function(target, pause, override, withSync, fromNav) {
      if (!slider.vars.animationLoop && target !== slider.currentSlide) {
        slider.direction = (target > slider.currentSlide) ? "next" : "prev";
      }

      if (asNav && slider.pagingCount === 1) slider.direction = (slider.currentItem < target) ? "next" : "prev";

      if (!slider.animating && (slider.canAdvance(target, fromNav) || override) && slider.is(":visible")) {
        if (asNav && withSync) {
          var master = $(slider.vars.asNavFor).data('flexslider');
          slider.atEnd = target === 0 || target === slider.count - 1;
          master.flexAnimate(target, true, false, true, fromNav);
          slider.direction = (slider.currentItem < target) ? "next" : "prev";
          master.direction = slider.direction;

          if (Math.ceil((target + 1)/slider.visible) - 1 !== slider.currentSlide && target !== 0) {
            slider.currentItem = target;
            slider.slides.removeClass(namespace + "active-slide").eq(target).addClass(namespace + "active-slide");
            target = Math.floor(target/slider.visible);
          } else {
            slider.currentItem = target;
            slider.slides.removeClass(namespace + "active-slide").eq(target).addClass(namespace + "active-slide");
            return false;
          }
        }

        slider.animating = true;
        slider.animatingTo = target;

        // SLIDESHOW:
        if (pause) { slider.pause(); }

        // API: before() animation Callback
        slider.vars.before(slider);

        // SYNC:
        if (slider.syncExists && !fromNav) { methods.sync("animate"); }

        // CONTROLNAV
        if (slider.vars.controlNav) { methods.controlNav.active(); }

        // !CAROUSEL:
        // CANDIDATE: slide active class (for add/remove slide)
        if (!carousel) { slider.slides.removeClass(namespace + 'active-slide').eq(target).addClass(namespace + 'active-slide'); }

        // INFINITE LOOP:
        // CANDIDATE: atEnd
        slider.atEnd = target === 0 || target === slider.last;

        // DIRECTIONNAV:
        if (slider.vars.directionNav) { methods.directionNav.update(); }

        if (target === slider.last) {
          // API: end() of cycle Callback
          slider.vars.end(slider);
          // SLIDESHOW && !INFINITE LOOP:
          if (!slider.vars.animationLoop) { slider.pause(); }
        }

        // SLIDE:
        if (!fade) {
          var dimension = (vertical) ? slider.slides.filter(':first').height() : slider.computedW,
              margin, slideString, calcNext;

          // INFINITE LOOP / REVERSE:
          if (carousel) {
            margin = slider.vars.itemMargin;
            calcNext = ((slider.itemW + margin) * slider.move) * slider.animatingTo;
            slideString = (calcNext > slider.limit && slider.visible !== 1) ? slider.limit : calcNext;
          } else if (slider.currentSlide === 0 && target === slider.count - 1 && slider.vars.animationLoop && slider.direction !== "next") {
            slideString = (reverse) ? (slider.count + slider.cloneOffset) * dimension : 0;
          } else if (slider.currentSlide === slider.last && target === 0 && slider.vars.animationLoop && slider.direction !== "prev") {
            slideString = (reverse) ? 0 : (slider.count + 1) * dimension;
          } else {
            slideString = (reverse) ? ((slider.count - 1) - target + slider.cloneOffset) * dimension : (target + slider.cloneOffset) * dimension;
          }
          slider.setProps(slideString, "", slider.vars.animationSpeed);
          if (slider.transitions) {
            if (!slider.vars.animationLoop || !slider.atEnd) {
              slider.animating = false;
              slider.currentSlide = slider.animatingTo;
            }

            // Unbind previous transitionEnd events and re-bind new transitionEnd event
            slider.container.unbind("webkitTransitionEnd transitionend");
            slider.container.bind("webkitTransitionEnd transitionend", function() {
              clearTimeout(slider.ensureAnimationEnd);
              slider.wrapup(dimension);
            });

            // Insurance for the ever-so-fickle transitionEnd event
            clearTimeout(slider.ensureAnimationEnd);
            slider.ensureAnimationEnd = setTimeout(function() {
              slider.wrapup(dimension);
            }, slider.vars.animationSpeed + 100);

          } else {
            slider.container.animate(slider.args, slider.vars.animationSpeed, slider.vars.easing, function(){
              slider.wrapup(dimension);
            });
          }
        } else { // FADE:
          if (!touch) {
            slider.slides.eq(slider.currentSlide).css({"zIndex": 1}).animate({"opacity": 0}, slider.vars.animationSpeed, slider.vars.easing);
            slider.slides.eq(target).css({"zIndex": 2}).animate({"opacity": 1}, slider.vars.animationSpeed, slider.vars.easing, slider.wrapup);
          } else {
            slider.slides.eq(slider.currentSlide).css({ "opacity": 0, "zIndex": 1 });
            slider.slides.eq(target).css({ "opacity": 1, "zIndex": 2 });
            slider.wrapup(dimension);
          }
        }
        // SMOOTH HEIGHT:
        if (slider.vars.smoothHeight) { methods.smoothHeight(slider.vars.animationSpeed); }
      }
    };
    slider.wrapup = function(dimension) {
      // SLIDE:
      if (!fade && !carousel) {
        if (slider.currentSlide === 0 && slider.animatingTo === slider.last && slider.vars.animationLoop) {
          slider.setProps(dimension, "jumpEnd");
        } else if (slider.currentSlide === slider.last && slider.animatingTo === 0 && slider.vars.animationLoop) {
          slider.setProps(dimension, "jumpStart");
        }
      }
      slider.animating = false;
      slider.currentSlide = slider.animatingTo;
      // API: after() animation Callback
      slider.vars.after(slider);
    };

    // SLIDESHOW:
    slider.animateSlides = function() {
      if (!slider.animating && focused ) { slider.flexAnimate(slider.getTarget("next")); }
    };
    // SLIDESHOW:
    slider.pause = function() {
      clearInterval(slider.animatedSlides);
      slider.animatedSlides = null;
      slider.playing = false;
      // PAUSEPLAY:
      if (slider.vars.pausePlay) { methods.pausePlay.update("play"); }
      // SYNC:
      if (slider.syncExists) { methods.sync("pause"); }
    };
    // SLIDESHOW:
    slider.play = function() {
      if (slider.playing) { clearInterval(slider.animatedSlides); }
      slider.animatedSlides = slider.animatedSlides || setInterval(slider.animateSlides, slider.vars.slideshowSpeed);
      slider.started = slider.playing = true;
      // PAUSEPLAY:
      if (slider.vars.pausePlay) { methods.pausePlay.update("pause"); }
      // SYNC:
      if (slider.syncExists) { methods.sync("play"); }
    };
    // STOP:
    slider.stop = function () {
      slider.pause();
      slider.stopped = true;
    };
    slider.canAdvance = function(target, fromNav) {
      // ASNAV:
      var last = (asNav) ? slider.pagingCount - 1 : slider.last;
      return (fromNav) ? true :
             (asNav && slider.currentItem === slider.count - 1 && target === 0 && slider.direction === "prev") ? true :
             (asNav && slider.currentItem === 0 && target === slider.pagingCount - 1 && slider.direction !== "next") ? false :
             (target === slider.currentSlide && !asNav) ? false :
             (slider.vars.animationLoop) ? true :
             (slider.atEnd && slider.currentSlide === 0 && target === last && slider.direction !== "next") ? false :
             (slider.atEnd && slider.currentSlide === last && target === 0 && slider.direction === "next") ? false :
             true;
    };
    slider.getTarget = function(dir) {
      slider.direction = dir;
      if (dir === "next") {
        return (slider.currentSlide === slider.last) ? 0 : slider.currentSlide + 1;
      } else {
        return (slider.currentSlide === 0) ? slider.last : slider.currentSlide - 1;
      }
    };

    // SLIDE:
    slider.setProps = function(pos, special, dur) {
      var target = (function() {
        var posCheck = (pos) ? pos : ((slider.itemW + slider.vars.itemMargin) * slider.move) * slider.animatingTo,
            posCalc = (function() {
              if (carousel) {
                return (special === "setTouch") ? pos :
                       (reverse && slider.animatingTo === slider.last) ? 0 :
                       (reverse) ? slider.limit - (((slider.itemW + slider.vars.itemMargin) * slider.move) * slider.animatingTo) :
                       (slider.animatingTo === slider.last) ? slider.limit : posCheck;
              } else {
                switch (special) {
                  case "setTotal": return (reverse) ? ((slider.count - 1) - slider.currentSlide + slider.cloneOffset) * pos : (slider.currentSlide + slider.cloneOffset) * pos;
                  case "setTouch": return (reverse) ? pos : pos;
                  case "jumpEnd": return (reverse) ? pos : slider.count * pos;
                  case "jumpStart": return (reverse) ? slider.count * pos : pos;
                  default: return pos;
                }
              }
            }());

            return (posCalc * ((slider.vars.rtl)?1:-1)) + "px";
          }());

      if (slider.transitions) {
        if (slider.isFirefox) {
          target = (vertical) ? "translate3d(0," + target + ",0)" : "translate3d(" + (parseInt(target)+'px') + ",0,0)";
        } else {
          target = (vertical) ? "translate3d(0," + target + ",0)" : "translate3d(" + ((slider.vars.rtl?-1:1)*parseInt(target)+'px') + ",0,0)";
        }
        dur = (dur !== undefined) ? (dur/1000) + "s" : "0s";
        slider.container.css("-" + slider.pfx + "-transition-duration", dur);
         slider.container.css("transition-duration", dur);
      }

      slider.args[slider.prop] = target;
      if (slider.transitions || dur === undefined) { slider.container.css(slider.args); }

      slider.container.css('transform',target);
    };

    slider.setup = function(type) {
      // SLIDE:
      if (!fade) {
        var sliderOffset, arr;

        if (type === "init") {
          slider.viewport = $('<div class="' + namespace + 'viewport"></div>').css({"overflow": "hidden", "position": "relative"}).appendTo(slider).append(slider.container);
          // INFINITE LOOP:
          slider.cloneCount = 0;
          slider.cloneOffset = 0;
          // REVERSE:
          if (reverse) {
            arr = $.makeArray(slider.slides).reverse();
            slider.slides = $(arr);
            slider.container.empty().append(slider.slides);
          }
        }
        // INFINITE LOOP && !CAROUSEL:
        if (slider.vars.animationLoop && !carousel) {
          slider.cloneCount = 2;
          slider.cloneOffset = 1;
          // clear out old clones
          if (type !== "init") { slider.container.find('.clone').remove(); }
          slider.container.append(methods.uniqueID(slider.slides.first().clone().addClass('clone')).attr('aria-hidden', 'true'))
                          .prepend(methods.uniqueID(slider.slides.last().clone().addClass('clone')).attr('aria-hidden', 'true'));
        }
        slider.newSlides = $(slider.vars.selector, slider);

        sliderOffset = (reverse) ? slider.count - 1 - slider.currentSlide + slider.cloneOffset : slider.currentSlide + slider.cloneOffset;
        // VERTICAL:
        if (vertical && !carousel) {
          slider.container.height((slider.count + slider.cloneCount) * 200 + "%").css("position", "absolute").width("100%");
          setTimeout(function(){
            slider.newSlides.css({"display": "block"});
            slider.doMath();
            slider.viewport.height(slider.h);
            slider.setProps(sliderOffset * slider.h, "init");
          }, (type === "init") ? 100 : 0);
        } else {
          slider.container.width((slider.count + slider.cloneCount) * 200 + "%");
          slider.setProps(sliderOffset * slider.computedW, "init");
          setTimeout(function(){
            slider.doMath();
          if(slider.vars.rtl){
            if (slider.isFirefox) {
              slider.newSlides.css({"width": slider.computedW, "marginRight" : slider.computedM, "float": "right", "display": "block"});
            } else {
              slider.newSlides.css({"width": slider.computedW, "marginRight" : slider.computedM, "float": "left", "display": "block"});
            }
              
           }
            else{
              slider.newSlides.css({"width": slider.computedW, "marginRight" : slider.computedM, "float": "left", "display": "block"});
            }
            // SMOOTH HEIGHT:
            if (slider.vars.smoothHeight) { methods.smoothHeight(); }
          }, (type === "init") ? 100 : 0);
        }
      } else { // FADE:
        if(slider.vars.rtl){
          slider.slides.css({"width": "100%", "float": 'right', "marginLeft": "-100%", "position": "relative"});
        }
        else{
          slider.slides.css({"width": "100%", "float": 'left', "marginRight": "-100%", "position": "relative"});
        }
        if (type === "init") {
          if (!touch) {
            //slider.slides.eq(slider.currentSlide).fadeIn(slider.vars.animationSpeed, slider.vars.easing);
            if (slider.vars.fadeFirstSlide == false) {
              slider.slides.css({ "opacity": 0, "display": "block", "zIndex": 1 }).eq(slider.currentSlide).css({"zIndex": 2}).css({"opacity": 1});
            } else {
              slider.slides.css({ "opacity": 0, "display": "block", "zIndex": 1 }).eq(slider.currentSlide).css({"zIndex": 2}).animate({"opacity": 1},slider.vars.animationSpeed,slider.vars.easing);
            }
          } else {
            slider.slides.css({ "opacity": 0, "display": "block", "webkitTransition": "opacity " + slider.vars.animationSpeed / 1000 + "s ease", "zIndex": 1 }).eq(slider.currentSlide).css({ "opacity": 1, "zIndex": 2});
          }
        }
        // SMOOTH HEIGHT:
        if (slider.vars.smoothHeight) { methods.smoothHeight(); }
      }
      // !CAROUSEL:
      // CANDIDATE: active slide
      if (!carousel) { slider.slides.removeClass(namespace + "active-slide").eq(slider.currentSlide).addClass(namespace + "active-slide"); }

      //FlexSlider: init() Callback
      slider.vars.init(slider);
    };

    slider.doMath = function() {
      var slide = slider.slides.first(),
          slideMargin = slider.vars.itemMargin,
          minItems = slider.vars.minItems,
          maxItems = slider.vars.maxItems;

      slider.w = (slider.viewport===undefined) ? slider.width() : slider.viewport.width();
      if (slider.isFirefox) { slider.w = slider.width(); }
      slider.h = slide.height();
      slider.boxPadding = slide.outerWidth() - slide.width();

      // CAROUSEL:
      if (carousel) {
        slider.itemT = slider.vars.itemWidth + slideMargin;
        slider.itemM = slideMargin;
        slider.minW = (minItems) ? minItems * slider.itemT : slider.w;
        slider.maxW = (maxItems) ? (maxItems * slider.itemT) - slideMargin : slider.w;
        slider.itemW = (slider.minW > slider.w) ? (slider.w - (slideMargin * (minItems - 1)))/minItems :
                       (slider.maxW < slider.w) ? (slider.w - (slideMargin * (maxItems - 1)))/maxItems :
                       (slider.vars.itemWidth > slider.w) ? slider.w : slider.vars.itemWidth;

        slider.visible = Math.floor(slider.w/(slider.itemW));
        slider.move = (slider.vars.move > 0 && slider.vars.move < slider.visible ) ? slider.vars.move : slider.visible;
        slider.pagingCount = Math.ceil(((slider.count - slider.visible)/slider.move) + 1);
        slider.last =  slider.pagingCount - 1;
        slider.limit = (slider.pagingCount === 1) ? 0 :
                       (slider.vars.itemWidth > slider.w) ? (slider.itemW * (slider.count - 1)) + (slideMargin * (slider.count - 1)) : ((slider.itemW + slideMargin) * slider.count) - slider.w - slideMargin;
      } else {
        slider.itemW = slider.w;
        slider.itemM = slideMargin;
        slider.pagingCount = slider.count;
        slider.last = slider.count - 1;
      }
      slider.computedW = slider.itemW - slider.boxPadding;
      slider.computedM = slider.itemM;
    };

    slider.update = function(pos, action) {
      slider.doMath();

      // update currentSlide and slider.animatingTo if necessary
      if (!carousel) {
        if (pos < slider.currentSlide) {
          slider.currentSlide += 1;
        } else if (pos <= slider.currentSlide && pos !== 0) {
          slider.currentSlide -= 1;
        }
        slider.animatingTo = slider.currentSlide;
      }

      // update controlNav
      if (slider.vars.controlNav && !slider.manualControls) {
        if ((action === "add" && !carousel) || slider.pagingCount > slider.controlNav.length) {
          methods.controlNav.update("add");
        } else if ((action === "remove" && !carousel) || slider.pagingCount < slider.controlNav.length) {
          if (carousel && slider.currentSlide > slider.last) {
            slider.currentSlide -= 1;
            slider.animatingTo -= 1;
          }
          methods.controlNav.update("remove", slider.last);
        }
      }
      // update directionNav
      if (slider.vars.directionNav) { methods.directionNav.update(); }

    };

    slider.addSlide = function(obj, pos) {
      var $obj = $(obj);

      slider.count += 1;
      slider.last = slider.count - 1;

      // append new slide
      if (vertical && reverse) {
        (pos !== undefined) ? slider.slides.eq(slider.count - pos).after($obj) : slider.container.prepend($obj);
      } else {
        (pos !== undefined) ? slider.slides.eq(pos).before($obj) : slider.container.append($obj);
      }

      // update currentSlide, animatingTo, controlNav, and directionNav
      slider.update(pos, "add");

      // update slider.slides
      slider.slides = $(slider.vars.selector + ':not(.clone)', slider);
      // re-setup the slider to accomdate new slide
      slider.setup();

      //FlexSlider: added() Callback
      slider.vars.added(slider);
    };
    slider.removeSlide = function(obj) {
      var pos = (isNaN(obj)) ? slider.slides.index($(obj)) : obj;

      // update count
      slider.count -= 1;
      slider.last = slider.count - 1;

      // remove slide
      if (isNaN(obj)) {
        $(obj, slider.slides).remove();
      } else {
        (vertical && reverse) ? slider.slides.eq(slider.last).remove() : slider.slides.eq(obj).remove();
      }

      // update currentSlide, animatingTo, controlNav, and directionNav
      slider.doMath();
      slider.update(pos, "remove");

      // update slider.slides
      slider.slides = $(slider.vars.selector + ':not(.clone)', slider);
      // re-setup the slider to accomdate new slide
      slider.setup();

      // FlexSlider: removed() Callback
      slider.vars.removed(slider);
    };

    //FlexSlider: Initialize
    methods.init();
  };

  // Ensure the slider isn't focussed if the window loses focus.
  $( window ).blur( function ( e ) {
    focused = false;
  }).focus( function ( e ) {
    focused = true;
  });

  //FlexSlider: Default Settings
  $.flexslider.defaults = {
    namespace: "flex-",             //{NEW} String: Prefix string attached to the class of every element generated by the plugin
    selector: ".slides > li",       //{NEW} Selector: Must match a simple pattern. '{container} > {slide}' -- Ignore pattern at your own peril
    animation: "fade",              //String: Select your animation type, "fade" or "slide"
    easing: "swing",                //{NEW} String: Determines the easing method used in jQuery transitions. jQuery easing plugin is supported!
    direction: "horizontal",        //String: Select the sliding direction, "horizontal" or "vertical"
    reverse: false,                 //{NEW} Boolean: Reverse the animation direction
    animationLoop: true,            //Boolean: Should the animation loop? If false, directionNav will received "disable" classes at either end
    smoothHeight: false,            //{NEW} Boolean: Allow height of the slider to animate smoothly in horizontal mode
    startAt: 0,                     //Integer: The slide that the slider should start on. Array notation (0 = first slide)
    slideshow: true,                //Boolean: Animate slider automatically
    slideshowSpeed: 7000,           //Integer: Set the speed of the slideshow cycling, in milliseconds
    animationSpeed: 600,            //Integer: Set the speed of animations, in milliseconds
    initDelay: 0,                   //{NEW} Integer: Set an initialization delay, in milliseconds
    randomize: false,               //Boolean: Randomize slide order
    fadeFirstSlide: true,           //Boolean: Fade in the first slide when animation type is "fade"
    thumbCaptions: false,           //Boolean: Whether or not to put captions on thumbnails when using the "thumbnails" controlNav.

    // Usability features
    pauseOnAction: true,            //Boolean: Pause the slideshow when interacting with control elements, highly recommended.
    pauseOnHover: false,            //Boolean: Pause the slideshow when hovering over slider, then resume when no longer hovering
    pauseInvisible: true,       //{NEW} Boolean: Pause the slideshow when tab is invisible, resume when visible. Provides better UX, lower CPU usage.
    useCSS: true,                   //{NEW} Boolean: Slider will use CSS3 transitions if available
    touch: true,                    //{NEW} Boolean: Allow touch swipe navigation of the slider on touch-enabled devices
    video: false,                   //{NEW} Boolean: If using video in the slider, will prevent CSS3 3D Transforms to avoid graphical glitches

    // Primary Controls
    controlNav: true,               //Boolean: Create navigation for paging control of each slide? Note: Leave true for manualControls usage
    directionNav: true,             //Boolean: Create navigation for previous/next navigation? (true/false)
    prevText: "Previous",           //String: Set the text for the "previous" directionNav item
    nextText: "Next",               //String: Set the text for the "next" directionNav item

    // Secondary Navigation
    keyboard: true,                 //Boolean: Allow slider navigating via keyboard left/right keys
    multipleKeyboard: false,        //{NEW} Boolean: Allow keyboard navigation to affect multiple sliders. Default behavior cuts out keyboard navigation with more than one slider present.
    mousewheel: false,              //{UPDATED} Boolean: Requires jquery.mousewheel.js (https://github.com/brandonaaron/jquery-mousewheel) - Allows slider navigating via mousewheel
    pausePlay: false,               //Boolean: Create pause/play dynamic element
    pauseText: "Pause",             //String: Set the text for the "pause" pausePlay item
    playText: "Play",               //String: Set the text for the "play" pausePlay item

    // Special properties
    controlsContainer: "",          //{UPDATED} jQuery Object/Selector: Declare which container the navigation elements should be appended too. Default container is the FlexSlider element. Example use would be $(".flexslider-container"). Property is ignored if given element is not found.
    manualControls: "",             //{UPDATED} jQuery Object/Selector: Declare custom control navigation. Examples would be $(".flex-control-nav li") or "#tabs-nav li img", etc. The number of elements in your controlNav should match the number of slides/tabs.
    customDirectionNav: "",         //{NEW} jQuery Object/Selector: Custom prev / next button. Must be two jQuery elements. In order to make the events work they have to have the classes "prev" and "next" (plus namespace)
    sync: "",                       //{NEW} Selector: Mirror the actions performed on this slider with another slider. Use with care.
    asNavFor: "",                   //{NEW} Selector: Internal property exposed for turning the slider into a thumbnail navigation for another slider

    // Carousel Options
    itemWidth: 0,                   //{NEW} Integer: Box-model width of individual carousel items, including horizontal borders and padding.
    itemMargin: 0,                  //{NEW} Integer: Margin between carousel items.
    minItems: 1,                    //{NEW} Integer: Minimum number of carousel items that should be visible. Items will resize fluidly when below this.
    maxItems: 0,                    //{NEW} Integer: Maxmimum number of carousel items that should be visible. Items will resize fluidly when above this limit.
    move: 0,                        //{NEW} Integer: Number of carousel items that should move on animation. If 0, slider will move all visible items.
    allowOneSlide: true,           //{NEW} Boolean: Whether or not to allow a slider comprised of a single slide

    // Browser Specific
    isFirefox: false,             // {NEW} Boolean: Set to true when Firefox is the browser used.

    // Callback API
    start: function(){},            //Callback: function(slider) - Fires when the slider loads the first slide
    before: function(){},           //Callback: function(slider) - Fires asynchronously with each slider animation
    after: function(){},            //Callback: function(slider) - Fires after each slider animation completes
    end: function(){},              //Callback: function(slider) - Fires when the slider reaches the last slide (asynchronous)
    added: function(){},            //{NEW} Callback: function(slider) - Fires after a slide is added
    removed: function(){},           //{NEW} Callback: function(slider) - Fires after a slide is removed
    init: function() {},             //{NEW} Callback: function(slider) - Fires after the slider is initially setup
  rtl: false             //{NEW} Boolean: Whether or not to enable RTL mode
  };

  //FlexSlider: Plugin Function
  $.fn.flexslider = function(options) {
    if (options === undefined) { options = {}; }

    if (typeof options === "object") {
      return this.each(function() {
        var $this = $(this),
            selector = (options.selector) ? options.selector : ".slides > li",
            $slides = $this.find(selector);

      if ( ( $slides.length === 1 && options.allowOneSlide === false ) || $slides.length === 0 ) {
          $slides.fadeIn(400);
          if (options.start) { options.start($this); }
        } else if ($this.data('flexslider') === undefined) {
          new $.flexslider(this, options);
        }
      });
    } else {
      // Helper strings to quickly perform functions on the slider
      var $slider = $(this).data('flexslider');
      switch (options) {
        case "play": $slider.play(); break;
        case "pause": $slider.pause(); break;
        case "stop": $slider.stop(); break;
        case "next": $slider.flexAnimate($slider.getTarget("next"), true); break;
        case "prev":
        case "previous": $slider.flexAnimate($slider.getTarget("prev"), true); break;
        default: if (typeof options === "number") { $slider.flexAnimate(options, true); }
      }
    }
  };
})(jQuery);

"undefined"===typeof jwplayer&&(jwplayer=function(){if(jwplayer.api)return jwplayer.api.selectPlayer.apply(this,arguments)},jwplayer.version="6.12.4956",jwplayer.vid=document.createElement("video"),jwplayer.audio=document.createElement("audio"),jwplayer.source=document.createElement("source"),function(){var f={},d=Array.prototype,l=Object.prototype,b=d.slice,j=d.concat,c=l.toString,g=l.hasOwnProperty,k=d.map,a=d.forEach,e=d.filter,r=d.every,q=d.some,h=d.indexOf,l=Array.isArray,p=Object.keys,m=function(a){if(a instanceof
    m)return a;if(!(this instanceof m))return new m(a)},w=m.each=m.forEach=function(e,b,h){if(null==e)return e;if(a&&e.forEach===a)e.forEach(b,h);else if(e.length===+e.length)for(var c=0,p=e.length;c<p;c++){if(b.call(h,e[c],c,e)===f)return}else for(var d=m.keys(e),c=0,p=d.length;c<p;c++)if(b.call(h,e[d[c]],d[c],e)===f)return;return e};m.map=m.collect=function(a,e,b){var h=[];if(null==a)return h;if(k&&a.map===k)return a.map(e,b);w(a,function(a,c,p){h.push(e.call(b,a,c,p))});return h};m.find=m.detect=function(a,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 e,b){var h;u(a,function(a,c,p){if(e.call(b,a,c,p))return h=a,!0});return h};m.filter=m.select=function(a,b,h){var c=[];if(null==a)return c;if(e&&a.filter===e)return a.filter(b,h);w(a,function(a,e,p){b.call(h,a,e,p)&&c.push(a)});return c};m.every=m.all=function(a,e,b){e||(e=m.identity);var h=!0;if(null==a)return h;if(r&&a.every===r)return a.every(e,b);w(a,function(a,c,p){if(!(h=h&&e.call(b,a,c,p)))return f});return!!h};var u=m.some=m.any=function(a,e,b){e||(e=m.identity);var h=!1;if(null==a)return h;
    if(q&&a.some===q)return a.some(e,b);w(a,function(a,c,p){if(h||(h=e.call(b,a,c,p)))return f});return!!h};m.size=function(a){return null==a?0:a.length===+a.length?a.length:m.keys(a).length};m.after=function(a,e){return function(){if(1>--a)return e.apply(this,arguments)}};m.sortedIndex=function(a,e,h,b){h=null==h?m.identity:m.isFunction(h)?h:m.property(h);e=h.call(b,e);for(var c=0,p=a.length;c<p;){var d=c+p>>>1;h.call(b,a[d])<e?c=d+1:p=d}return c};m.find=m.detect=function(a,e,h){var b;u(a,function(a,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        c,p){if(e.call(h,a,c,p))return b=a,!0});return b};u=m.some=m.any=function(a,e,h){e||(e=m.identity);var b=!1;if(null==a)return b;if(q&&a.some===q)return a.some(e,h);w(a,function(a,c,p){if(b||(b=e.call(h,a,c,p)))return f});return!!b};m.contains=m.include=function(a,e){if(null==a)return!1;a.length!==+a.length&&(a=m.values(a));return 0<=m.indexOf(a,e)};m.where=function(a,e){return m.filter(a,m.matches(e))};m.difference=function(a){var e=j.apply(d,b.call(arguments,1));return m.filter(a,function(a){return!m.contains(e,
    a)})};m.without=function(a){return m.difference(a,b.call(arguments,1))};m.indexOf=function(a,e,b){if(null==a)return-1;var c=0,p=a.length;if(b)if("number"==typeof b)c=0>b?Math.max(0,p+b):b;else return c=m.sortedIndex(a,e),a[c]===e?c:-1;if(h&&a.indexOf===h)return a.indexOf(e,b);for(;c<p;c++)if(a[c]===e)return c;return-1};m.partial=function(a){var e=b.call(arguments,1);return function(){for(var b=0,h=e.slice(),c=0,p=h.length;c<p;c++)h[c]===m&&(h[c]=arguments[b++]);for(;b<arguments.length;)h.push(arguments[b++]);
    return a.apply(this,h)}};m.memoize=function(a,e){var b={};e||(e=m.identity);return function(){var h=e.apply(this,arguments);return m.has(b,h)?b[h]:b[h]=a.apply(this,arguments)}};m.delay=function(a,e){var h=b.call(arguments,2);return setTimeout(function(){return a.apply(null,h)},e)};m.defer=function(a){return m.delay.apply(m,[a,1].concat(b.call(arguments,1)))};m.keys=function(a){if(!m.isObject(a))return[];if(p)return p(a);var e=[],b;for(b in a)m.has(a,b)&&e.push(b);return e};m.pick=function(a){var e=
{},h=j.apply(d,b.call(arguments,1));w(h,function(b){b in a&&(e[b]=a[b])});return e};m.isArray=l||function(a){return"[object Array]"==c.call(a)};m.isObject=function(a){return a===Object(a)};w("Arguments Function String Number Date RegExp".split(" "),function(a){m["is"+a]=function(e){return c.call(e)=="[object "+a+"]"}});m.isArguments(arguments)||(m.isArguments=function(a){return!(!a||!m.has(a,"callee"))});"function"!==typeof/./&&(m.isFunction=function(a){return"function"===typeof a});m.isFinite=function(a){return isFinite(a)&&
    !isNaN(parseFloat(a))};m.isNaN=function(a){return m.isNumber(a)&&a!=+a};m.isBoolean=function(a){return!0===a||!1===a||"[object Boolean]"==c.call(a)};m.isNull=function(a){return null===a};m.isUndefined=function(a){return void 0===a};m.has=function(a,e){return g.call(a,e)};m.identity=function(a){return a};m.constant=function(a){return function(){return a}};m.property=function(a){return function(e){return e[a]}};m.matches=function(a){return function(e){if(e===a)return!0;for(var b in a)if(a[b]!==e[b])return!1;
    return!0}};m.result=function(a,e){if(null!=a){var b=a[e];return m.isFunction(b)?b.call(a):b}};this._=m}.call(jwplayer),function(f){function d(a){return function(){return g(a)}}function l(a,e,c,d,h){return function(){var p,g;if(h)c(a);else{try{if(p=a.responseXML)if(g=p.firstChild,p.lastChild&&"parsererror"===p.lastChild.nodeName){d&&d("Invalid XML",e,a);return}}catch(k){}if(p&&g)return c(a);(p=b.parseXML(a.responseText))&&p.firstChild?(a=b.extend({},a,{responseXML:p}),c(a)):d&&d(a.responseText?"Invalid XML":
    e,e,a)}}}var b=f.utils={},j=f._;b.exists=function(a){switch(typeof a){case "string":return 0<a.length;case "object":return null!==a;case "undefined":return!1}return!0};b.styleDimension=function(a){return a+(0<a.toString().indexOf("%")?"":"px")};b.getAbsolutePath=function(a,e){b.exists(e)||(e=document.location.href);if(b.exists(a)){var c;if(b.exists(a)){c=a.indexOf("://");var d=a.indexOf("?");c=0<c&&(0>d||d>c)}else c=void 0;if(c)return a;c=e.substring(0,e.indexOf("://")+3);var d=e.substring(c.length,
    e.indexOf("/",c.length+1)),h;0===a.indexOf("/")?h=a.split("/"):(h=e.split("?")[0],h=h.substring(c.length+d.length+1,h.lastIndexOf("/")),h=h.split("/").concat(a.split("/")));for(var p=[],g=0;g<h.length;g++)h[g]&&(b.exists(h[g])&&"."!==h[g])&&(".."===h[g]?p.pop():p.push(h[g]));return c+d+"/"+p.join("/")}};b.extend=function(){var a=Array.prototype.slice.call(arguments,0);if(1<a.length){for(var e=a[0],c=function(a,b){void 0!==b&&null!==b&&(e[a]=b)},d=1;d<a.length;d++)b.foreach(a[d],c);return e}return null};
    var c=window.console=window.console||{log:function(){}};b.log=function(){var a=Array.prototype.slice.call(arguments,0);"object"===typeof c.log?c.log(a):c.log.apply(c,a)};var g=j.memoize(function(a){return null!==navigator.userAgent.toLowerCase().match(a)});b.isFF=d(/firefox/i);b.isChrome=d(/chrome/i);b.isIPod=d(/iP(hone|od)/i);b.isIPad=d(/iPad/i);b.isSafari602=d(/Macintosh.*Mac OS X 10_8.*6\.0\.\d* Safari/i);b.isIETrident=function(a){return a?(a=parseFloat(a).toFixed(1),g(RegExp("trident/.+rv:\\s*"+
        a,"i"))):g(/trident/i)};b.isMSIE=function(a){return a?(a=parseFloat(a).toFixed(1),g(RegExp("msie\\s*"+a,"i"))):g(/msie/i)};b.isIE=function(a){return a?(a=parseFloat(a).toFixed(1),11<=a?b.isIETrident(a):b.isMSIE(a)):b.isMSIE()||b.isIETrident()};b.isSafari=function(){return g(/safari/i)&&!g(/chrome/i)&&!g(/chromium/i)&&!g(/android/i)};b.isIOS=function(a){return a?g(RegExp("iP(hone|ad|od).+\\sOS\\s"+a,"i")):g(/iP(hone|ad|od)/i)};b.isAndroidNative=function(a){return b.isAndroid(a,!0)};b.isAndroid=function(a,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   e){return e&&g(/chrome\/[123456789]/i)&&!g(/chrome\/18/)?!1:a?(b.isInt(a)&&!/\./.test(a)&&(a=""+a+"."),g(RegExp("Android\\s*"+a,"i"))):g(/Android/i)};b.isMobile=function(){return b.isIOS()||b.isAndroid()};b.isIframe=function(){return window.frameElement&&"IFRAME"===window.frameElement.nodeName};b.saveCookie=function(a,e){document.cookie="jwplayer."+a+"\x3d"+e+"; path\x3d/"};b.getCookies=function(){for(var a={},e=document.cookie.split("; "),b=0;b<e.length;b++){var c=e[b].split("\x3d");0===c[0].indexOf("jwplayer.")&&
    (a[c[0].substring(9,c[0].length)]=c[1])}return a};b.isInt=function(a){return 0===parseFloat(a)%1};b.typeOf=function(a){if(null===a)return"null";var e=typeof a;return"object"===e&&j.isArray(a)?"array":e};b.translateEventResponse=function(a,e){var c=b.extend({},e);if(a===f.events.JWPLAYER_FULLSCREEN&&!c.fullscreen)c.fullscreen="true"===c.message,delete c.message;else if("object"===typeof c.data){var d=c.data;delete c.data;c=b.extend(c,d)}else"object"===typeof c.metadata&&b.deepReplaceKeyName(c.metadata,
        ["__dot__","__spc__","__dsh__","__default__"],["."," ","-","default"]);b.foreach(["position","duration","offset"],function(a,e){c[e]&&(c[e]=Math.round(1E3*c[e])/1E3)});return c};b.flashVersion=function(){if(b.isAndroid())return 0;var a=navigator.plugins,e;try{if("undefined"!==a&&(e=a["Shockwave Flash"]))return parseInt(e.description.replace(/\D+(\d+)\..*/,"$1"),10)}catch(c){}if("undefined"!==typeof window.ActiveXObject)try{if(e=new window.ActiveXObject("ShockwaveFlash.ShockwaveFlash"))return parseInt(e.GetVariable("$version").split(" ")[1].split(",")[0],
        10)}catch(d){}return 0};b.getScriptPath=function(a){for(var e=document.getElementsByTagName("script"),c=0;c<e.length;c++){var b=e[c].src;if(b&&0<=b.indexOf(a))return b.substr(0,b.indexOf(a))}return""};b.deepReplaceKeyName=function(a,e,c){switch(f.utils.typeOf(a)){case "array":for(var d=0;d<a.length;d++)a[d]=f.utils.deepReplaceKeyName(a[d],e,c);break;case "object":b.foreach(a,function(b,d){var g;if(e instanceof Array&&c instanceof Array){if(e.length!==c.length)return;g=e}else g=[e];for(var k=b,j=0;j<
    g.length;j++)k=k.replace(RegExp(e[j],"g"),c[j]);a[k]=f.utils.deepReplaceKeyName(d,e,c);b!==k&&delete a[b]})}return a};var k=b.pluginPathType={ABSOLUTE:0,RELATIVE:1,CDN:2};b.getPluginPathType=function(a){if("string"===typeof a){a=a.split("?")[0];var e=a.indexOf("://");if(0<e)return k.ABSOLUTE;var c=a.indexOf("/");a=b.extension(a);return 0>e&&0>c&&(!a||!isNaN(a))?k.CDN:k.RELATIVE}};b.getPluginName=function(a){return a.replace(/^(.*\/)?([^-]*)-?.*\.(swf|js)$/,"$2")};b.getPluginVersion=function(a){return a.replace(/[^-]*-?([^\.]*).*$/,
        "$1")};b.isYouTube=function(a,e){return"youtube"===e||/^(http|\/\/).*(youtube\.com|youtu\.be)\/.+/.test(a)};b.youTubeID=function(a){try{return/v[=\/]([^?&]*)|youtu\.be\/([^?]*)|^([\w-]*)$/i.exec(a).slice(1).join("").replace("?","")}catch(e){return""}};b.isRtmp=function(a,e){return 0===a.indexOf("rtmp")||"rtmp"===e};b.foreach=function(a,e){var c,d;for(c in a)"function"===b.typeOf(a.hasOwnProperty)?a.hasOwnProperty(c)&&(d=a[c],e(c,d)):(d=a[c],e(c,d))};b.isHTTPS=function(){return 0===window.location.href.indexOf("https")};
    b.repo=function(){var a="http://p.jwpcdn.com/"+f.version.split(/\W/).splice(0,2).join("/")+"/";try{b.isHTTPS()&&(a=a.replace("http://","https://ssl."))}catch(c){}return a};b.versionCheck=function(a){a=("0"+a).split(/\W/);var c=f.version.split(/\W/),b=parseFloat(a[0]),d=parseFloat(c[0]);return b>d||b===d&&parseFloat("0"+a[1])>parseFloat(c[1])?!1:!0};b.ajax=function(a,c,d,g){var h,p=!1;0<a.indexOf("#")&&(a=a.replace(/#.*$/,""));if(a&&0<=a.indexOf("://")&&a.split("/")[2]!==window.location.href.split("/")[2]&&
        b.exists(window.XDomainRequest))h=new window.XDomainRequest,h.onload=l(h,a,c,d,g),h.ontimeout=h.onprogress=function(){},h.timeout=5E3;else if(b.exists(window.XMLHttpRequest)){var k=h=new window.XMLHttpRequest,j=a;h.onreadystatechange=function(){if(4===k.readyState)switch(k.status){case 200:l(k,j,c,d,g)();break;case 404:d("File not found",j,k)}}}else return d&&d("",a,h),h;h.overrideMimeType&&h.overrideMimeType("text/xml");var f=a,t=h;h.onerror=function(){d("Error loading file",f,t)};try{h.open("GET",
        a,!0)}catch(n){p=!0}setTimeout(function(){if(p)d&&d(a,a,h);else try{h.send()}catch(c){d&&d(a,a,h)}},0);return h};b.parseXML=function(a){var c;try{if(window.DOMParser){if(c=(new window.DOMParser).parseFromString(a,"text/xml"),c.childNodes&&c.childNodes.length&&"parsererror"===c.childNodes[0].firstChild.nodeName)return}else c=new window.ActiveXObject("Microsoft.XMLDOM"),c.async="false",c.loadXML(a)}catch(b){return}return c};b.between=function(a,c,b){return Math.max(Math.min(a,b),c)};b.seconds=function(a){if(j.isNumber(a))return a;
        a=a.replace(",",".");var c=a.split(":"),b=0;"s"===a.slice(-1)?b=parseFloat(a):"m"===a.slice(-1)?b=60*parseFloat(a):"h"===a.slice(-1)?b=3600*parseFloat(a):1<c.length?(b=parseFloat(c[c.length-1]),b+=60*parseFloat(c[c.length-2]),3===c.length&&(b+=3600*parseFloat(c[c.length-3]))):b=parseFloat(a);return b};b.serialize=function(a){return null===a?null:"true"===a.toString().toLowerCase()?!0:"false"===a.toString().toLowerCase()?!1:isNaN(Number(a))||5<a.length||0===a.length?a:Number(a)};b.addClass=function(a,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               c){var d=j.isString(a.className)?a.className.split(" "):[],g=j.isArray(c)?c:c.split(" ");j.each(g,function(a){j.contains(d,a)||d.push(a)});a.className=b.trim(d.join(" "))};b.removeClass=function(a,c){var d=j.isString(a.className)?a.className.split(" "):[],g=j.isArray(c)?c:c.split(" ");a.className=b.trim(j.difference(d,g).join(" "))};b.emptyElement=function(a){for(;a.firstChild;)a.removeChild(a.firstChild)};b.indexOf=j.indexOf;b.noop=function(){};b.canCast=function(){var a=f.cast;return!(!a||!j.isFunction(a.available)||
    !a.available())}}(jwplayer),function(f){function d(a){var c=document.createElement("style");a&&c.appendChild(document.createTextNode(a));c.type="text/css";document.getElementsByTagName("head")[0].appendChild(c);return c}function l(a,b,e){if(!c.exists(b))return"";e=e?" !important":"";return"string"===typeof b&&isNaN(b)?/png|gif|jpe?g/i.test(b)&&0>b.indexOf("url")?"url("+b+")":b+e:0===b||"z-index"===a||"opacity"===a?""+b+e:/color/i.test(a)?"#"+c.pad(b.toString(16).replace(/^0x/i,""),6)+e:Math.ceil(b)+
"px"+e}function b(a,c){for(var b=0;b<a.length;b++){var e=a[b],d,g;if(void 0!==e&&null!==e)for(d in c){g=d;g=g.split("-");for(var k=1;k<g.length;k++)g[k]=g[k].charAt(0).toUpperCase()+g[k].slice(1);g=g.join("");e.style[g]!==c[d]&&(e.style[g]=c[d])}}}function j(c){var b=g[c].sheet,e,d,k;if(b){e=b.cssRules;d=r[c];k=c;var j=a[k];k+=" { ";for(var f in j)k+=f+": "+j[f]+"; ";k+="}";if(void 0!==d&&d<e.length&&e[d].selectorText===c){if(k===e[d].cssText)return;b.deleteRule(d)}else d=e.length,r[c]=d;try{b.insertRule(k,
    d)}catch(l){}}}var c=f.utils,g={},k,a={},e=null,r={};c.cssKeyframes=function(a,c){var b=g.keyframes;b||(b=d(),g.keyframes=b);var b=b.sheet,e="@keyframes "+a+" { "+c+" }";try{b.insertRule(e,b.cssRules.length)}catch(k){}e=e.replace(/(keyframes|transform)/g,"-webkit-$1");try{b.insertRule(e,b.cssRules.length)}catch(j){}};var q=c.css=function(c,b,f){a[c]||(a[c]={});var r=a[c];f=f||!1;var u=!1,q,n;for(q in b)n=l(q,b[q],f),""!==n?n!==r[q]&&(r[q]=n,u=!0):void 0!==r[q]&&(delete r[q],u=!0);if(u){if(!g[c]){b=
    k&&k.sheet&&k.sheet.cssRules&&k.sheet.cssRules.length||0;if(!k||5E4<b)k=d();g[c]=k}null!==e?e.styleSheets[c]=a[c]:j(c)}};q.style=function(a,c,d){if(!(void 0===a||null===a)){void 0===a.length&&(a=[a]);var g={},k;for(k in c)g[k]=l(k,c[k]);if(null!==e&&!d){c=(c=a.__cssRules)||{};for(var j in g)c[j]=g[j];a.__cssRules=c;0>f._.indexOf(e.elements,a)&&e.elements.push(a)}else b(a,g)}};q.block=function(a){null===e&&(e={id:a,styleSheets:{},elements:[]})};q.unblock=function(a){if(e&&(!a||e.id===a)){for(var c in e.styleSheets)j(c);
    for(a=0;a<e.elements.length;a++)c=e.elements[a],b(c,c.__cssRules);e=null}};c.clearCss=function(c){for(var b in a)0<=b.indexOf(c)&&delete a[b];for(var e in g)0<=e.indexOf(c)&&j(e)};c.transform=function(a,c){var b={};c=c||"";b.transform=c;b["-webkit-transform"]=c;b["-ms-transform"]=c;b["-moz-transform"]=c;b["-o-transform"]=c;"string"===typeof a?q(a,b):q.style(a,b)};c.dragStyle=function(a,c){q(a,{"-webkit-user-select":c,"-moz-user-select":c,"-ms-user-select":c,"-webkit-user-drag":c,"user-select":c,"user-drag":c})};
    c.transitionStyle=function(a,c){navigator.userAgent.match(/5\.\d(\.\d)? safari/i)||q(a,{"-webkit-transition":c,"-moz-transition":c,"-o-transition":c,transition:c})};c.rotate=function(a,b){c.transform(a,"rotate("+b+"deg)")};c.rgbHex=function(a){a=String(a).replace("#","");3===a.length&&(a=a[0]+a[0]+a[1]+a[1]+a[2]+a[2]);return"#"+a.substr(-6)};c.hexToRgba=function(a,c){var b="rgb",e=[parseInt(a.substr(1,2),16),parseInt(a.substr(3,2),16),parseInt(a.substr(5,2),16)];void 0!==c&&100!==c&&(b+="a",e.push(c/
        100));return b+"("+e.join(",")+")"}}(jwplayer),function(f){var d=jwplayer._,l=f.foreach,b={mp4:"video/mp4",ogg:"video/ogg",oga:"audio/ogg",vorbis:"audio/ogg",webm:"video/webm",aac:"audio/mp4",mp3:"audio/mpeg",hls:"application/vnd.apple.mpegurl"},j={mp4:b.mp4,f4v:b.mp4,m4v:b.mp4,mov:b.mp4,m4a:b.aac,f4a:b.aac,aac:b.aac,mp3:b.mp3,ogv:b.ogg,ogg:b.ogg,oga:b.vorbis,vorbis:b.vorbis,webm:b.webm,m3u8:b.hls,m3u:b.hls,hls:b.hls},c=f.extensionmap={};l(j,function(b,d){c[b]={html5:d}});l({flv:"video",f4v:"video",
    mov:"video",m4a:"video",m4v:"video",mp4:"video",aac:"video",f4a:"video",mp3:"sound",smil:"rtmp",m3u8:"hls",hls:"hls"},function(b,d){c[b]||(c[b]={});c[b].flash=d});c.types=b;c.mimeType=function(c){var k;d.find(b,function(a,b){if(a===c)return k=b,!0});return k};c.extType=function(b){return c.mimeType(j[b])}}(jwplayer.utils),function(f){var d=f.loaderstatus={NEW:0,LOADING:1,ERROR:2,COMPLETE:3};f.scriptloader=function(l){function b(a){k=d.ERROR;g.sendEvent(c.ERROR,a)}function j(a){k=d.COMPLETE;g.sendEvent(c.COMPLETE,
    a)}var c=jwplayer.events,g=f.extend(this,new c.eventdispatcher),k=d.NEW;this.load=function(){if(k===d.NEW){var a=f.scriptloader.loaders[l];if(a&&(k=a.getStatus(),2>k)){a.addEventListener(c.ERROR,b);a.addEventListener(c.COMPLETE,j);return}var e=document.getElementsByTagName("head")[0]||document.documentElement,g=document.createElement("script"),q=!1;g.onload=g.onreadystatechange=function(a){if(!q&&(!this.readyState||"loaded"===this.readyState||"complete"===this.readyState))q=!0,j(a),g.onload=g.onreadystatechange=
    null,e&&g.parentNode&&e.removeChild(g)};g.onerror=b;g.src=l;e.insertBefore(g,e.firstChild);k=d.LOADING;f.scriptloader.loaders[l]=this}};this.getStatus=function(){return k}};f.scriptloader.loaders={}}(jwplayer.utils),function(f){f.trim=function(d){return d.replace(/^\s+|\s+$/g,"")};f.pad=function(d,f,b){for(b||(b="0");d.length<f;)d=b+d;return d};f.xmlAttribute=function(d,f){for(var b=0;b<d.attributes.length;b++)if(d.attributes[b].name&&d.attributes[b].name.toLowerCase()===f.toLowerCase())return d.attributes[b].value.toString();
    return""};f.extension=function(d){if(!d||"rtmp"===d.substr(0,4))return"";var f;f=-1<d.indexOf("(format\x3dm3u8-")?"m3u8":!1;if(f)return f;d=d.substring(d.lastIndexOf("/")+1,d.length).split("?")[0].split("#")[0];if(-1<d.lastIndexOf("."))return d.substr(d.lastIndexOf(".")+1,d.length).toLowerCase()};f.stringToColor=function(d){d=d.replace(/(#|0x)?([0-9A-F]{3,6})$/gi,"$2");3===d.length&&(d=d.charAt(0)+d.charAt(0)+d.charAt(1)+d.charAt(1)+d.charAt(2)+d.charAt(2));return parseInt(d,16)}}(jwplayer.utils),
    function(f){var d="touchmove",l="touchstart";f.touch=function(b){function j(b){b.type===l?(a=!0,r=g(h.DRAG_START,b)):b.type===d?a&&(q||(c(h.DRAG_START,b,r),q=!0),c(h.DRAG,b)):(a&&(q?c(h.DRAG_END,b):(b.cancelBubble=!0,c(h.TAP,b))),a=q=!1,r=null)}function c(a,c,b){if(e[a]&&(c.preventManipulation&&c.preventManipulation(),c.preventDefault&&c.preventDefault(),c=b?b:g(a,c)))e[a](c)}function g(a,c){var b=null;c.touches&&c.touches.length?b=c.touches[0]:c.changedTouches&&c.changedTouches.length&&(b=c.changedTouches[0]);
        if(!b)return null;var e=k.getBoundingClientRect(),b={type:a,target:k,x:b.pageX-window.pageXOffset-e.left,y:b.pageY,deltaX:0,deltaY:0};a!==h.TAP&&r&&(b.deltaX=b.x-r.x,b.deltaY=b.y-r.y);return b}var k=b,a=!1,e={},r=null,q=!1,h=f.touchEvents;document.addEventListener(d,j);document.addEventListener("touchend",function(b){a&&q&&c(h.DRAG_END,b);a=q=!1;r=null});document.addEventListener("touchcancel",j);b.addEventListener(l,j);b.addEventListener("touchend",j);this.addEventListener=function(a,c){e[a]=c};
        this.removeEventListener=function(a){delete e[a]};return this}}(jwplayer.utils),function(f){f.touchEvents={DRAG:"jwplayerDrag",DRAG_START:"jwplayerDragStart",DRAG_END:"jwplayerDragEnd",TAP:"jwplayerTap"}}(jwplayer.utils),function(f){f.key=function(d){var l,b,j;this.edition=function(){return j&&j.getTime()<(new Date).getTime()?"invalid":l};this.token=function(){return b};f.exists(d)||(d="");try{d=f.tea.decrypt(d,"36QXq4W@GSBV^teR");var c=d.split("/");(l=c[0])?/^(free|pro|premium|enterprise|ads)$/i.test(l)?
    (b=c[1],c[2]&&0<parseInt(c[2])&&(j=new Date,j.setTime(String(c[2])))):l="invalid":l="free"}catch(g){l="invalid"}}}(jwplayer.utils),function(f){var d=f.tea={};d.encrypt=function(j,c){if(0==j.length)return"";var g=d.strToLongs(b.encode(j));1>=g.length&&(g[1]=0);for(var k=d.strToLongs(b.encode(c).slice(0,16)),a=g.length,e=g[a-1],f=g[0],q,h=Math.floor(6+52/a),p=0;0<h--;){p+=2654435769;q=p>>>2&3;for(var m=0;m<a;m++)f=g[(m+1)%a],e=(e>>>5^f<<2)+(f>>>3^e<<4)^(p^f)+(k[m&3^q]^e),e=g[m]+=e}g=d.longsToStr(g);
    return l.encode(g)};d.decrypt=function(j,c){if(0==j.length)return"";for(var g=d.strToLongs(l.decode(j)),k=d.strToLongs(b.encode(c).slice(0,16)),a=g.length,e=g[a-1],f=g[0],q,h=2654435769*Math.floor(6+52/a);0!=h;){q=h>>>2&3;for(var p=a-1;0<=p;p--)e=g[0<p?p-1:a-1],e=(e>>>5^f<<2)+(f>>>3^e<<4)^(h^f)+(k[p&3^q]^e),f=g[p]-=e;h-=2654435769}g=d.longsToStr(g);g=g.replace(/\0+$/,"");return b.decode(g)};d.strToLongs=function(b){for(var c=Array(Math.ceil(b.length/4)),d=0;d<c.length;d++)c[d]=b.charCodeAt(4*d)+(b.charCodeAt(4*
        d+1)<<8)+(b.charCodeAt(4*d+2)<<16)+(b.charCodeAt(4*d+3)<<24);return c};d.longsToStr=function(b){for(var c=Array(b.length),d=0;d<b.length;d++)c[d]=String.fromCharCode(b[d]&255,b[d]>>>8&255,b[d]>>>16&255,b[d]>>>24&255);return c.join("")};var l={code:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/\x3d",encode:function(d,c){var g,k,a,e,f=[],q="",h,p,m=l.code;p=("undefined"==typeof c?0:c)?b.encode(d):d;h=p.length%3;if(0<h)for(;3>h++;)q+="\x3d",p+="\x00";for(h=0;h<p.length;h+=3)g=p.charCodeAt(h),
    k=p.charCodeAt(h+1),a=p.charCodeAt(h+2),e=g<<16|k<<8|a,g=e>>18&63,k=e>>12&63,a=e>>6&63,e&=63,f[h/3]=m.charAt(g)+m.charAt(k)+m.charAt(a)+m.charAt(e);f=f.join("");return f=f.slice(0,f.length-q.length)+q},decode:function(d,c){c="undefined"==typeof c?!1:c;var g,k,a,e,f,q=[],h,p=l.code;h=c?b.decode(d):d;for(var m=0;m<h.length;m+=4)g=p.indexOf(h.charAt(m)),k=p.indexOf(h.charAt(m+1)),e=p.indexOf(h.charAt(m+2)),f=p.indexOf(h.charAt(m+3)),a=g<<18|k<<12|e<<6|f,g=a>>>16&255,k=a>>>8&255,a&=255,q[m/4]=String.fromCharCode(g,
    k,a),64==f&&(q[m/4]=String.fromCharCode(g,k)),64==e&&(q[m/4]=String.fromCharCode(g));e=q.join("");return c?b.decode(e):e}},b={encode:function(b){b=b.replace(/[\u0080-\u07ff]/g,function(c){c=c.charCodeAt(0);return String.fromCharCode(192|c>>6,128|c&63)});return b=b.replace(/[\u0800-\uffff]/g,function(c){c=c.charCodeAt(0);return String.fromCharCode(224|c>>12,128|c>>6&63,128|c&63)})},decode:function(b){b=b.replace(/[\u00e0-\u00ef][\u0080-\u00bf][\u0080-\u00bf]/g,function(c){c=(c.charCodeAt(0)&15)<<12|
    (c.charCodeAt(1)&63)<<6|c.charCodeAt(2)&63;return String.fromCharCode(c)});return b=b.replace(/[\u00c0-\u00df][\u0080-\u00bf]/g,function(c){c=(c.charCodeAt(0)&31)<<6|c.charCodeAt(1)&63;return String.fromCharCode(c)})}}}(jwplayer.utils),function(f){f.events={COMPLETE:"COMPLETE",ERROR:"ERROR",API_READY:"jwplayerAPIReady",JWPLAYER_READY:"jwplayerReady",JWPLAYER_FULLSCREEN:"jwplayerFullscreen",JWPLAYER_RESIZE:"jwplayerResize",JWPLAYER_ERROR:"jwplayerError",JWPLAYER_SETUP_ERROR:"jwplayerSetupError",JWPLAYER_MEDIA_BEFOREPLAY:"jwplayerMediaBeforePlay",
    JWPLAYER_MEDIA_BEFORECOMPLETE:"jwplayerMediaBeforeComplete",JWPLAYER_MEDIA_BUFFER:"jwplayerMediaBuffer",JWPLAYER_MEDIA_BUFFER_FULL:"jwplayerMediaBufferFull",JWPLAYER_MEDIA_ERROR:"jwplayerMediaError",JWPLAYER_MEDIA_LOADED:"jwplayerMediaLoaded",JWPLAYER_MEDIA_COMPLETE:"jwplayerMediaComplete",JWPLAYER_MEDIA_SEEK:"jwplayerMediaSeek",JWPLAYER_MEDIA_TIME:"jwplayerMediaTime",JWPLAYER_MEDIA_VOLUME:"jwplayerMediaVolume",JWPLAYER_MEDIA_META:"jwplayerMediaMeta",JWPLAYER_MEDIA_MUTE:"jwplayerMediaMute",JWPLAYER_AUDIO_TRACKS:"jwplayerAudioTracks",
    JWPLAYER_AUDIO_TRACK_CHANGED:"jwplayerAudioTrackChanged",JWPLAYER_MEDIA_LEVELS:"jwplayerMediaLevels",JWPLAYER_MEDIA_LEVEL_CHANGED:"jwplayerMediaLevelChanged",JWPLAYER_CAPTIONS_CHANGED:"jwplayerCaptionsChanged",JWPLAYER_CAPTIONS_LIST:"jwplayerCaptionsList",JWPLAYER_CAPTIONS_LOADED:"jwplayerCaptionsLoaded",JWPLAYER_PLAYER_STATE:"jwplayerPlayerState",state:{BUFFERING:"BUFFERING",IDLE:"IDLE",PAUSED:"PAUSED",PLAYING:"PLAYING"},JWPLAYER_PLAYLIST_LOADED:"jwplayerPlaylistLoaded",JWPLAYER_PLAYLIST_ITEM:"jwplayerPlaylistItem",
    JWPLAYER_PLAYLIST_COMPLETE:"jwplayerPlaylistComplete",JWPLAYER_DISPLAY_CLICK:"jwplayerViewClick",JWPLAYER_PROVIDER_CLICK:"jwplayerProviderClick",JWPLAYER_VIEW_TAB_FOCUS:"jwplayerViewTabFocus",JWPLAYER_CONTROLS:"jwplayerViewControls",JWPLAYER_USER_ACTION:"jwplayerUserAction",JWPLAYER_INSTREAM_CLICK:"jwplayerInstreamClicked",JWPLAYER_INSTREAM_DESTROYED:"jwplayerInstreamDestroyed",JWPLAYER_AD_TIME:"jwplayerAdTime",JWPLAYER_AD_ERROR:"jwplayerAdError",JWPLAYER_AD_CLICK:"jwplayerAdClicked",JWPLAYER_AD_COMPLETE:"jwplayerAdComplete",
    JWPLAYER_AD_IMPRESSION:"jwplayerAdImpression",JWPLAYER_AD_COMPANIONS:"jwplayerAdCompanions",JWPLAYER_AD_SKIPPED:"jwplayerAdSkipped",JWPLAYER_AD_PLAY:"jwplayerAdPlay",JWPLAYER_AD_PAUSE:"jwplayerAdPause",JWPLAYER_AD_META:"jwplayerAdMeta",JWPLAYER_CAST_AVAILABLE:"jwplayerCastAvailable",JWPLAYER_CAST_SESSION:"jwplayerCastSession",JWPLAYER_CAST_AD_CHANGED:"jwplayerCastAdChanged"}}(jwplayer),function(f){var d=f.utils;f.events.eventdispatcher=function(l,b){function j(c,a,b){if(c)for(var g=0;g<c.length;g++){var f=
    c[g];if(f){null!==f.count&&0===--f.count&&delete c[g];try{f.listener(a)}catch(h){d.log('Error handling "'+b+'" event listener ['+g+"]: "+h.toString(),f.listener,a)}}}}var c,g;this.resetEventListeners=function(){c={};g=[]};this.resetEventListeners();this.addEventListener=function(b,a,e){try{d.exists(c[b])||(c[b]=[]),"string"===d.typeOf(a)&&(a=(new Function("return "+a))()),c[b].push({listener:a,count:e||null})}catch(g){d.log("error",g)}return!1};this.removeEventListener=function(b,a){var e;if(c[b]){try{if(void 0===
    a){c[b]=[];return}for(e=0;e<c[b].length;e++)if(c[b][e].listener.toString()===a.toString()){c[b].splice(e,1);break}}catch(g){d.log("error",g)}return!1}};this.addGlobalListener=function(b,a){try{"string"===d.typeOf(b)&&(b=(new Function("return "+b))()),g.push({listener:b,count:a||null})}catch(c){d.log("error",c)}return!1};this.removeGlobalListener=function(b){if(b){try{for(var a=g.length;a--;)g[a].listener.toString()===b.toString()&&g.splice(a,1)}catch(c){d.log("error",c)}return!1}};this.sendEvent=
    function(k,a){d.exists(a)||(a={});d.extend(a,{id:l,version:f.version,type:k});b&&d.log(k,a);j(c[k],a,k);j(g,a,k)}}}(window.jwplayer),function(f){var d={},l={};f.plugins=function(){};f.plugins.loadPlugins=function(b,j){l[b]=new f.plugins.pluginloader(new f.plugins.model(d),j);return l[b]};f.plugins.registerPlugin=function(b,j,c,g){var k=f.utils.getPluginName(b);d[k]||(d[k]=new f.plugins.plugin(b));d[k].registerPlugin(b,j,c,g)}}(jwplayer),function(f){f.plugins.model=function(d){this.addPlugin=function(l){var b=
    f.utils.getPluginName(l);d[b]||(d[b]=new f.plugins.plugin(l));return d[b]};this.getPlugins=function(){return d}}}(jwplayer),function(f){var d=jwplayer.utils,l=jwplayer.events;f.pluginmodes={FLASH:0,JAVASCRIPT:1,HYBRID:2};f.plugin=function(b){function j(){switch(d.getPluginPathType(b)){case d.pluginPathType.ABSOLUTE:return b;case d.pluginPathType.RELATIVE:return d.getAbsolutePath(b,window.location.href)}}function c(){q=setTimeout(function(){k=d.loaderstatus.COMPLETE;h.sendEvent(l.COMPLETE)},1E3)}function g(){k=
    d.loaderstatus.ERROR;h.sendEvent(l.ERROR,{url:b})}var k=d.loaderstatus.NEW,a,e,r,q,h=new l.eventdispatcher;d.extend(this,h);this.load=function(){if(k===d.loaderstatus.NEW)if(0<b.lastIndexOf(".swf"))a=b,k=d.loaderstatus.COMPLETE,h.sendEvent(l.COMPLETE);else if(d.getPluginPathType(b)===d.pluginPathType.CDN)k=d.loaderstatus.COMPLETE,h.sendEvent(l.COMPLETE);else{k=d.loaderstatus.LOADING;var e=new d.scriptloader(j());e.addEventListener(l.COMPLETE,c);e.addEventListener(l.ERROR,g);e.load()}};this.registerPlugin=
    function(b,c,g,f){q&&(clearTimeout(q),q=void 0);r=c;g&&f?(a=f,e=g):"string"===typeof g?a=g:"function"===typeof g?e=g:!g&&!f&&(a=b);k=d.loaderstatus.COMPLETE;h.sendEvent(l.COMPLETE)};this.getStatus=function(){return k};this.getPluginName=function(){return d.getPluginName(b)};this.getFlashPath=function(){if(a)switch(d.getPluginPathType(a)){case d.pluginPathType.ABSOLUTE:return a;case d.pluginPathType.RELATIVE:return 0<b.lastIndexOf(".swf")?d.getAbsolutePath(a,window.location.href):d.getAbsolutePath(a,
    j())}return null};this.getJS=function(){return e};this.getTarget=function(){return r};this.getPluginmode=function(){if("undefined"!==typeof a&&"undefined"!==typeof e)return f.pluginmodes.HYBRID;if("undefined"!==typeof a)return f.pluginmodes.FLASH;if("undefined"!==typeof e)return f.pluginmodes.JAVASCRIPT};this.getNewInstance=function(a,b,c){return new e(a,b,c)};this.getURL=function(){return b}}}(jwplayer.plugins),function(f){var d=f.utils,l=f.events,b=f._,j=d.foreach;f.plugins.pluginloader=function(c,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           g){function f(){q||(q=!0,r=d.loaderstatus.COMPLETE,u.sendEvent(l.COMPLETE))}function a(){if(!w&&((!h||0===b.keys(h).length)&&f(),!q)){var a=c.getPlugins();m=b.after(p,f);d.foreach(h,function(b){b=d.getPluginName(b);var c=a[b];b=c.getJS();var e=c.getTarget(),c=c.getStatus();c===d.loaderstatus.LOADING||c===d.loaderstatus.NEW||(b&&!d.versionCheck(e)&&u.sendEvent(l.ERROR,{message:"Incompatible player version"}),m())})}}function e(b){w||(u.sendEvent(l.ERROR,{message:"File not found"}),b.url&&d.log("File not found",
    b.url),a())}var r=d.loaderstatus.NEW,q=!1,h=g,p=b.size(h),m,w=!1,u=new l.eventdispatcher;d.extend(this,u);this.setupPlugins=function(a,b,e){var g={length:0,plugins:{}},f=0,h={},k=c.getPlugins();j(b.plugins,function(c,j){var p=d.getPluginName(c),l=k[p],r=l.getFlashPath(),m=l.getJS(),u=l.getURL();r&&(g.plugins[r]=d.extend({},j),g.plugins[r].pluginmode=l.getPluginmode(),g.length++);try{if(m&&b.plugins&&b.plugins[u]){var q=document.createElement("div");q.id=a.id+"_"+p;q.style.position="absolute";q.style.top=
    0;q.style.zIndex=f+10;h[p]=l.getNewInstance(a,d.extend({},b.plugins[u]),q);f++;a.onReady(e(h[p],q,!0));a.onResize(e(h[p],q))}}catch(w){d.log("ERROR: Failed to load "+p+".")}});a.plugins=h;return g};this.load=function(){if(!(d.exists(g)&&"object"!==d.typeOf(g))){r=d.loaderstatus.LOADING;j(g,function(b){d.exists(b)&&(b=c.addPlugin(b),b.addEventListener(l.COMPLETE,a),b.addEventListener(l.ERROR,e))});var b=c.getPlugins();j(b,function(a,b){b.load()})}a()};this.destroy=function(){w=!0;u&&(u.resetEventListeners(),
    u=null)};this.pluginFailed=e;this.getStatus=function(){return r}}}(jwplayer),function(f){f.parsers={localName:function(d){return d?d.localName?d.localName:d.baseName?d.baseName:"":""},textContent:function(d){return d?d.textContent?f.utils.trim(d.textContent):d.text?f.utils.trim(d.text):"":""},getChildNode:function(d,f){return d.childNodes[f]},numChildren:function(d){return d.childNodes?d.childNodes.length:0}}}(jwplayer),function(f){var d=f.parsers;(d.jwparser=function(){}).parseEntry=function(l,b){for(var j=
    [],c=[],g=f.utils.xmlAttribute,k=0;k<l.childNodes.length;k++){var a=l.childNodes[k];if("jwplayer"===a.prefix){var e=d.localName(a);"source"===e?(delete b.sources,j.push({file:g(a,"file"),"default":g(a,"default"),label:g(a,"label"),type:g(a,"type")})):"track"===e?(delete b.tracks,c.push({file:g(a,"file"),"default":g(a,"default"),kind:g(a,"kind"),label:g(a,"label")})):(b[e]=f.utils.serialize(d.textContent(a)),"file"===e&&b.sources&&delete b.sources)}b.file||(b.file=b.link)}if(j.length){b.sources=[];
    for(k=0;k<j.length;k++)0<j[k].file.length&&(j[k]["default"]="true"===j[k]["default"]?!0:!1,j[k].label.length||delete j[k].label,b.sources.push(j[k]))}if(c.length){b.tracks=[];for(k=0;k<c.length;k++)0<c[k].file.length&&(c[k]["default"]="true"===c[k]["default"]?!0:!1,c[k].kind=!c[k].kind.length?"captions":c[k].kind,c[k].label.length||delete c[k].label,b.tracks.push(c[k]))}return b}}(jwplayer),function(f){var d=jwplayer.utils,l=d.xmlAttribute,b=f.localName,j=f.textContent,c=f.numChildren,g=f.mediaparser=
    function(){};g.parseGroup=function(f,a){var e,r,q=[];for(r=0;r<c(f);r++)if(e=f.childNodes[r],"media"===e.prefix&&b(e))switch(b(e).toLowerCase()){case "content":l(e,"duration")&&(a.duration=d.seconds(l(e,"duration")));0<c(e)&&(a=g.parseGroup(e,a));l(e,"url")&&(a.sources||(a.sources=[]),a.sources.push({file:l(e,"url"),type:l(e,"type"),width:l(e,"width"),label:l(e,"label")}));break;case "title":a.title=j(e);break;case "description":a.description=j(e);break;case "guid":a.mediaid=j(e);break;case "thumbnail":a.image||
(a.image=l(e,"url"));break;case "group":g.parseGroup(e,a);break;case "subtitle":var h={};h.file=l(e,"url");h.kind="captions";if(0<l(e,"lang").length){var p=h;e=l(e,"lang");var m={zh:"Chinese",nl:"Dutch",en:"English",fr:"French",de:"German",it:"Italian",ja:"Japanese",pt:"Portuguese",ru:"Russian",es:"Spanish"};e=m[e]?m[e]:e;p.label=e}q.push(h)}a.hasOwnProperty("tracks")||(a.tracks=[]);for(r=0;r<q.length;r++)a.tracks.push(q[r]);return a}}(jwplayer.parsers),function(f){function d(c){for(var a={},e=0;e<
c.childNodes.length;e++){var d=c.childNodes[e],j=g(d);if(j)switch(j.toLowerCase()){case "enclosure":a.file=l.xmlAttribute(d,"url");break;case "title":a.title=b(d);break;case "guid":a.mediaid=b(d);break;case "pubdate":a.date=b(d);break;case "description":a.description=b(d);break;case "link":a.link=b(d);break;case "category":a.tags=a.tags?a.tags+b(d):b(d)}}a=f.mediaparser.parseGroup(c,a);a=f.jwparser.parseEntry(c,a);return new jwplayer.playlist.item(a)}var l=jwplayer.utils,b=f.textContent,j=f.getChildNode,
    c=f.numChildren,g=f.localName;f.rssparser={};f.rssparser.parse=function(b){for(var a=[],e=0;e<c(b);e++){var f=j(b,e);if("channel"===g(f).toLowerCase())for(var l=0;l<c(f);l++){var h=j(f,l);"item"===g(h).toLowerCase()&&a.push(d(h))}}return a}}(jwplayer.parsers),function(f){var d=f.utils,l=f._;f.playlist=function(b){var c=[];b=l.isArray(b)?b:[b];l.each(b,function(b){c.push(new f.playlist.item(b))});return c};f.playlist.filterPlaylist=function(f,c){var g=[];l.each(f,function(f){f=d.extend({},f);f.sources=
    b(f.sources,!1,c);if(f.sources.length){for(var a=0;a<f.sources.length;a++)f.sources[a].label=f.sources[a].label||a.toString();g.push(f)}});return g};var b=f.playlist.filterSources=function(b,c,g){var k,a=[],e=c?f.embed.flashCanPlay:f.embed.html5CanPlay;if(b)return l.each(b,function(b){if(!b||!b.file)b=void 0;else{var c=d.trim(""+b.file),f=b.type;f||(f=d.extension(c),f=d.extensionmap.extType(f));b=d.extend({},b,{file:c,type:f})}b&&e(b.file,b.type,g)&&(k=k||b.type,b.type===k&&a.push(b))}),a}}(jwplayer),
    function(f){var d=f.item=function(l){var b=jwplayer.utils,j=b.extend({},d.defaults,l),c,g;j.tracks=l&&b.exists(l.tracks)?l.tracks:[];0===j.sources.length&&(j.sources=[new f.source(j)]);for(c=0;c<j.sources.length;c++)g=j.sources[c]["default"],j.sources[c]["default"]=g?"true"===g.toString():!1,j.sources[c]=new f.source(j.sources[c]);if(j.captions&&!b.exists(l.tracks)){for(l=0;l<j.captions.length;l++)j.tracks.push(j.captions[l]);delete j.captions}for(c=0;c<j.tracks.length;c++)j.tracks[c]=new f.track(j.tracks[c]);
        return j};d.defaults={description:void 0,image:void 0,mediaid:void 0,title:void 0,sources:[],tracks:[]}}(jwplayer.playlist),function(f){var d=f.utils,l=f.events,b=f.parsers;f.playlist.loader=function(){function j(a){try{var c=a.responseXML.childNodes;a="";for(var d=0;d<c.length&&!(a=c[d],8!==a.nodeType);d++);"xml"===b.localName(a)&&(a=a.nextSibling);if("rss"!==b.localName(a))g("Not a valid RSS feed");else{var j=new f.playlist(b.rssparser.parse(a));k.sendEvent(l.JWPLAYER_PLAYLIST_LOADED,{playlist:j})}}catch(h){g()}}
    function c(a){g(a.match(/invalid/i)?"Not a valid RSS feed":"")}function g(a){k.sendEvent(l.JWPLAYER_ERROR,{message:a?a:"Error loading file"})}var k=new l.eventdispatcher;d.extend(this,k);this.load=function(a){d.ajax(a,j,c)}}}(jwplayer),function(f){var d=jwplayer.utils,l={file:void 0,label:void 0,type:void 0,"default":void 0};f.source=function(b){var f=d.extend({},l);d.foreach(l,function(c){d.exists(b[c])&&(f[c]=b[c],delete b[c])});f.type&&0<f.type.indexOf("/")&&(f.type=d.extensionmap.mimeType(f.type));
    "m3u8"===f.type&&(f.type="hls");"smil"===f.type&&(f.type="rtmp");return f}}(jwplayer.playlist),function(f){var d=jwplayer.utils,l={file:void 0,label:void 0,kind:"captions","default":!1};f.track=function(b){var f=d.extend({},l);b||(b={});d.foreach(l,function(c){d.exists(b[c])&&(f[c]=b[c],delete b[c])});return f}}(jwplayer.playlist),function(f){function d(b,c,a){var e=b.style;e.backgroundColor="#000";e.color="#FFF";e.width=l.styleDimension(a.width);e.height=l.styleDimension(a.height);e.display="table";
    e.opacity=1;a=document.createElement("p");e=a.style;e.verticalAlign="middle";e.textAlign="center";e.display="table-cell";e.font="15px/20px Arial, Helvetica, sans-serif";a.innerHTML=c.replace(":",":\x3cbr\x3e");b.innerHTML="";b.appendChild(a)}var l=f.utils,b=f.events,j=f._,c=f.embed=function(g){function k(){if(!z){var e=p.playlist;if(j.isArray(e)){if(0===e.length){r();return}if(1===e.length&&(!e[0].sources||0===e[0].sources.length||!e[0].sources[0].file)){r();return}}if(!x)if(j.isString(e))v=new f.playlist.loader,
    v.addEventListener(b.JWPLAYER_PLAYLIST_LOADED,function(a){p.playlist=a.playlist;x=!1;k()}),v.addEventListener(b.JWPLAYER_ERROR,function(a){x=!1;r(a)}),x=!0,v.load(p.playlist);else if(n.getStatus()===l.loaderstatus.COMPLETE){for(e=0;e<p.modes.length;e++){var d=p.modes[e],h=d.type;if(h&&c[h]){var m=l.extend({},p);B=new c[h](F,d,m,n,g);if(B.supportsConfig())return B.addEventListener(b.ERROR,a),B.embed(),l.css("object.jwswf, .jwplayer:focus",{outline:"none"}),l.css(".jw-tab-focus:focus",{outline:"solid 2px #0B7EF4"}),
    g}}p.fallback?(e="No suitable players found and fallback enabled",q(e,!0),l.log(e),new c.download(F,p,r)):(e="No suitable players found and fallback disabled",q(e,!1),l.log(e))}}}function a(a){h(u+a.message)}function e(a){g.dispatchEvent(b.JWPLAYER_ERROR,{message:"Could not load plugin: "+a.message})}function r(a){a&&a.message?h("Error loading playlist: "+a.message):h(u+"No playable sources found")}function q(a,c){clearTimeout(y);y=setTimeout(function(){g.dispatchEvent(b.JWPLAYER_SETUP_ERROR,{message:a,
    fallback:c})},0)}function h(a){z||(t.parentNode.replaceChild(F,t),p.fallback?(z=!0,d(F,a,p),q(a,!0)):q(a,!1))}var p=new c.config(g.config),m=p.width,w=p.height,u="Error loading player: ",t=document.getElementById(g.id),n=f.plugins.loadPlugins(g.id,p.plugins),v,x=!1,z=!1,B=null,y=-1;p.id=g.id;p.aspectratio?g.config.aspectratio=p.aspectratio:delete g.config.aspectratio;var E=g;l.foreach(p.events,function(a,b){var c=E[a];"function"===typeof c&&c.call(E,b)});var F=document.createElement("div");F.id=t.id;
    F.style.width=0<m.toString().indexOf("%")?m:m+"px";F.style.height=0<w.toString().indexOf("%")?w:w+"px";this.embed=function(){z||(n.addEventListener(b.COMPLETE,k),n.addEventListener(b.ERROR,e),n.load())};this.destroy=function(){B&&(B.destroy(),B=null);n&&(n.destroy(),n=null);v&&(v.resetEventListeners(),v=null)};this.errorScreen=h;return this};f.embed.errorScreen=d}(jwplayer),function(f){function d(c){if(c.playlist)for(var d=0;d<c.playlist.length;d++)c.playlist[d]=new j(c.playlist[d]);else{var f={};
    b.foreach(j.defaults,function(a){l(c,f,a)});f.sources||(c.levels?(f.sources=c.levels,delete c.levels):(d={},l(c,d,"file"),l(c,d,"type"),f.sources=d.file?[d]:[]));c.playlist=[new j(f)]}}function l(c,d,f){b.exists(c[f])&&(d[f]=c[f],delete c[f])}var b=f.utils,j=f.playlist.item;(f.embed.config=function(c){var g={fallback:!0,height:270,primary:"html5",width:480,base:c.base?c.base:b.getScriptPath("jwplayer.js"),aspectratio:""};c=b.extend({},g,f.defaults,c);var g={type:"html5",src:c.base+"jwplayer.html5.js"},
    k={type:"flash",src:c.base+"jwplayer.flash.swf"};c.modes="flash"===c.primary?[k,g]:[g,k];c.listbar&&(c.playlistsize=c.listbar.size,c.playlistposition=c.listbar.position,c.playlistlayout=c.listbar.layout);c.flashplayer&&(k.src=c.flashplayer);c.html5player&&(g.src=c.html5player);d(c);k=c.aspectratio;if("string"!==typeof k||!b.exists(k))g=0;else{var a=k.indexOf(":");-1===a?g=0:(g=parseFloat(k.substr(0,a)),k=parseFloat(k.substr(a+1)),g=0>=g||0>=k?0:100*(k/g)+"%")}-1===c.width.toString().indexOf("%")?
    delete c.aspectratio:g?c.aspectratio=g:delete c.aspectratio;return c}).addConfig=function(c,f){d(f);return b.extend(c,f)}}(jwplayer),function(f){function d(b,d){function c(a,b){f[k].style[a]=b}for(var f=document.querySelectorAll(b),k=0;k<f.length;k++)l.foreach(d,c)}var l=f.utils;f.embed.download=function(b,f,c){function g(a,b,c){a=document.createElement(a);b&&(a.className="jwdownload"+b);c&&c.appendChild(a);return a}var k=l.extend({},f),a,e=k.width?k.width:480,r=k.height?k.height:320,q;f=f.logo?f.logo:
{prefix:l.repo(),file:"logo.png",margin:10};var h,p,m,w,u,t;p=k.playlist;k=["mp4","aac","mp3"];if(p&&p.length){m=p[0];w=m.sources;for(p=0;p<w.length;p++)u=w[p],u.file&&(t=u.type||l.extensionmap.extType(l.extension(u.file)),0<=l.indexOf(k,t)?(a=u.file,q=m.image):l.isYouTube(u.file,t)&&(h=u.file));a?(c=a,b&&(a=g("a","display",b),g("div","icon",a),g("div","logo",a),c&&a.setAttribute("href",l.getAbsolutePath(c))),c="#"+b.id+" .jwdownload",b.style.width="",b.style.height="",d(c+"display",{width:l.styleDimension(Math.max(320,
    e)),height:l.styleDimension(Math.max(180,r)),background:"black center no-repeat "+(q?"url("+q+")":""),backgroundSize:"contain",position:"relative",border:"none",display:"block"}),d(c+"display div",{position:"absolute",width:"100%",height:"100%"}),d(c+"logo",{top:f.margin+"px",right:f.margin+"px",background:"top right no-repeat url("+f.prefix+f.file+")"}),d(c+"icon",{background:"center no-repeat url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAA8CAYAAAA6/NlyAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAgNJREFUeNrs28lqwkAYB/CZqNVDDj2r6FN41QeIy8Fe+gj6BL275Q08u9FbT8ZdwVfotSBYEPUkxFOoks4EKiJdaDuTjMn3wWBO0V/+sySR8SNSqVRKIR8qaXHkzlqS9jCfzzWcTCYp9hF5o+59sVjsiRzcegSckFzcjT+ruN80TeSlAjCAAXzdJSGPFXRpAAMYwACGZQkSdhG4WCzehMNhqV6vG6vVSrirKVEw66YoSqDb7cqlUilE8JjHd/y1MQefVzqdDmiaJpfLZWHgXMHn8F6vJ1cqlVAkEsGuAn83J4gAd2RZymQygX6/L1erVQt+9ZPWb+CDwcCC2zXGJaewl/DhcHhK3DVj+KfKZrMWvFarcYNLomAv4aPRSFZVlTlcSPA5fDweW/BoNIqFnKV53JvncjkLns/n/cLdS+92O7RYLLgsKfv9/t8XlDn4eDyiw+HA9Jyz2eyt0+kY2+3WFC5hluej0Ha7zQQq9PPwdDq1Et1sNsx/nFBgCqWJ8oAK1aUptNVqcYWewE4nahfU0YQnk4ntUEfGMIU2m01HoLaCKbTRaDgKtaVLk9tBYaBcE/6Artdr4RZ5TB6/dC+9iIe/WgAMYADDpAUJAxjAAAYwgGFZgoS/AtNNTF7Z2bL0BYPBV3Jw5xFwwWcYxgtBP5OkE8i9G7aWGOOCruvauwADALMLMEbKf4SdAAAAAElFTkSuQmCC)"})):
    h?(f=h,b=g("iframe","",b),b.src="http://www.youtube.com/embed/"+l.youTubeID(f),b.width=e,b.height=r,b.style.border="none"):c()}}}(jwplayer),function(f){var d=f.utils,l=f.events,b={};(f.embed.flash=function(c,g,k,a,e){function r(a,b,c){var e=document.createElement("param");e.setAttribute("name",b);e.setAttribute("value",c);a.appendChild(e)}function q(a,b,c){return function(){try{c&&document.getElementById(e.id+"_wrapper").appendChild(b);var d=document.getElementById(e.id).getPluginConfig("display");
    "function"===typeof a.resize&&a.resize(d.width,d.height);b.style.left=d.x;b.style.top=d.h}catch(f){}}}function h(a){if(!a)return{};var b={},c=[];d.foreach(a,function(a,e){var f=d.getPluginName(a);c.push(a);d.foreach(e,function(a,c){b[f+"."+a]=c})});b.plugins=c.join(",");return b}var p=new f.events.eventdispatcher,m=d.flashVersion();d.extend(this,p);this.embed=function(){k.id=e.id;if(10>m)return p.sendEvent(l.ERROR,{message:"Flash version must be 10.0 or greater"}),!1;var f,j,t=c.id,n=e.config.listbar,
    v=d.extend({},k);f=document.getElementById(e.id);f.parentNode.replaceChild(c,f);if(t+"_wrapper"===c.parentNode.id)f=document.getElementById(t+"_wrapper");else{f=document.createElement("div");j=document.createElement("div");j.style.display="none";j.id=t+"_aspect";f.id=t+"_wrapper";f.style.position="relative";f.style.display="block";f.style.width=d.styleDimension(v.width);f.style.height=d.styleDimension(v.height);if(e.config.aspectratio){var x=parseFloat(e.config.aspectratio);j.style.display="block";
    j.style.marginTop=e.config.aspectratio;f.style.height="auto";f.style.display="inline-block";n&&("bottom"===n.position?j.style.paddingBottom=n.size+"px":"right"===n.position&&(j.style.marginBottom=-1*n.size*(x/100)+"px"))}c.parentNode.replaceChild(f,c);f.appendChild(c);f.appendChild(j)}n=a.setupPlugins(e,v,q);0<n.length?d.extend(v,h(n.plugins)):delete v.plugins;"undefined"!==typeof v["dock.position"]&&"false"===v["dock.position"].toString().toLowerCase()&&(v.dock=v["dock.position"],delete v["dock.position"]);
    n=v.wmode||(v.height&&40>=v.height?"transparent":"opaque");j="height width modes events primary base fallback volume".split(" ");for(x=0;x<j.length;x++)delete v[j[x]];j=d.getCookies();d.foreach(j,function(a,b){"undefined"===typeof v[a]&&(v[a]=b)});j=window.location.href.split("/");j.splice(j.length-1,1);j=j.join("/");v.base=j+"/";b[t]=v;d.isMSIE()?(c.outerHTML='\x3cobject classid\x3d"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width\x3d"100%" height\x3d"100%" id\x3d"'+t+'" name\x3d"'+t+'" tabindex\x3d"0"\x3e\x3cparam name\x3d"movie" value\x3d"'+
        g.src+'"\x3e\x3cparam name\x3d"allowfullscreen" value\x3d"true"\x3e\x3cparam name\x3d"allowscriptaccess" value\x3d"always"\x3e\x3cparam name\x3d"seamlesstabbing" value\x3d"true"\x3e\x3cparam name\x3d"wmode" value\x3d"'+n+'"\x3e\x3cparam name\x3d"bgcolor" value\x3d"#000000"\x3e\x3c/object\x3e',f=f.getElementsByTagName("object")[0],f.style.outline="none"):(f=document.createElement("object"),f.setAttribute("type","application/x-shockwave-flash"),f.setAttribute("data",g.src),f.setAttribute("width","100%"),
        f.setAttribute("height","100%"),f.setAttribute("bgcolor","#000000"),f.setAttribute("id",t),f.setAttribute("name",t),f.className="jwswf",r(f,"allowfullscreen","true"),r(f,"allowscriptaccess","always"),r(f,"seamlesstabbing","true"),r(f,"wmode",n),c.parentNode.replaceChild(f,c));e.config.aspectratio&&(f.style.position="absolute",f.style.left="0");e.container=f;e.setPlayer(f,"flash")};this.supportsConfig=function(){if(m)if(k){if("string"===d.typeOf(k.playlist))return!0;try{var a=k.playlist[0].sources;
    if("undefined"===typeof a)return!0;for(var b=0;b<a.length;b++)if(a[b].file&&j(a[b].file,a[b].type))return!0}catch(c){}}else return!0;return!1};this.destroy=function(){}}).getVars=function(c){return b[c]};var j=f.embed.flashCanPlay=function(b,f){if(d.isYouTube(b,f)||d.isRtmp(b,f)||"hls"===f)return!0;var k=d.extensionmap[f?f:d.extension(b)];return!k?!1:!!k.flash}}(jwplayer),function(f){var d=f.utils,l=d.extensionmap,b=f.events,j;f.embed.html5=function(c,g,k,a,e){function l(a,b,e){return function(){try{var d=
    document.querySelector("#"+c.id+" .jwmain");e&&d.appendChild(b);"function"===typeof a.resize&&(a.resize(d.clientWidth,d.clientHeight),setTimeout(function(){a.resize(d.clientWidth,d.clientHeight)},400));b.left=d.style.left;b.top=d.style.top}catch(f){}}}var q=new b.eventdispatcher;d.extend(this,q);this.embed=function(){if(f.html5){a.setupPlugins(e,k,l);d.emptyElement(c);var b=f.utils.extend({},k);delete b.volume;b=new f.html5.player(b);e.setPlayer(b,"html5")}else this.loadEmbedder()};this.loadEmbedder=
    function(){j=j||new d.scriptloader(g.src);j.addEventListener(b.ERROR,this.loadError);j.addEventListener(b.COMPLETE,this.embed);j.load()};this.loadError=function(a){this.sendEvent(a.type,{message:"HTML5 player not found"})};this.supportsConfig=function(){if(f.vid.canPlayType)try{if("string"===d.typeOf(k.playlist))return!0;for(var a=k.playlist[0].sources,b=0;b<a.length;b++)if(f.embed.html5CanPlay(a[b].file,a[b].type,k.androidhls))return!0}catch(c){}return!1};this.destroy=function(){j&&(j.resetEventListeners(),
    j=null)}};f.embed.html5CanPlay=function(b,g,k){if(null!==navigator.userAgent.match(/BlackBerry/i)||d.isIE(9))return!1;if(d.isYouTube(b,g))return!0;var a=d.extension(b);g=g||l.extType(a);if("hls"===g)if(k){k=d.isAndroidNative;if(k(2)||k(3)||k("4.0"))return!1;if(d.isAndroid())return!0}else if(d.isAndroid())return!1;if(d.isRtmp(b,g))return!1;b=l[g]||l[a];if(!b||b.flash&&!b.html5)return!1;var e;a:if(b=b.html5){try{e=!!f.vid.canPlayType(b);break a}catch(j){}e=!1}else e=!0;return e}}(jwplayer),function(f){var d=
    f.embed,l=f.embed.html5CanPlay,b=f.utils,j=f._,c=/\.(js|swf)$/;f.cast=f.cast||{};f.embed=b.extend(function(g){function k(){v="Adobe SiteCatalyst Error: Could not find Media Module"}var a=b.repo(),e=b.extend({},f.defaults),j=b.extend({},e,g.config),l=g.config,h=j.plugins,p=j.analytics,m=a+"jwpsrv.js",w=a+"sharing.js",u=a+"related.js",t=a+"gapro.js",e=f.key?f.key:e.key,n=(new f.utils.key(e)).edition(),v,h=h?h:{};"ads"==n&&j.advertising&&(c.test(j.advertising.client)?h[j.advertising.client]=j.advertising:
    h[a+j.advertising.client+".js"]=j.advertising);delete l.advertising;l.key=e;j.analytics&&c.test(j.analytics.client)&&(m=j.analytics.client);delete l.analytics;p&&!("ads"===n||"enterprise"===n)&&delete p.enabled;if("free"==n||!p||!1!==p.enabled)h[m]=p?p:{};delete h.sharing;delete h.related;switch(n){case "ads":case "enterprise":if(l.sitecatalyst)try{window.s&&window.s.hasOwnProperty("Media")?new f.embed.sitecatalyst(g):k()}catch(x){k()}case "premium":j.related&&(c.test(j.related.client)&&(u=j.related.client),
    h[u]=j.related),j.ga&&(c.test(j.ga.client)&&(t=j.ga.client),h[t]=j.ga);case "pro":j.sharing&&(c.test(j.sharing.client)&&(w=j.sharing.client),h[w]=j.sharing),j.skin&&(l.skin=j.skin.replace(/^(beelden|bekle|five|glow|modieus|roundster|stormtrooper|vapor)$/i,b.repo()+"skins/$1.xml"))}l.plugins=h;g.config=l;g=new d(g);v&&g.errorScreen(v);return g},f.embed);f.embed.html5CanPlay=function(b,c){var a;var d={file:b,type:c};a=f.html5&&f.html5.chooseProvider?f.html5.chooseProvider(d)!==f.html5.VideoProvider:
    j.any(f.unregisteredProviders,function(a){return a.supports(d)});return a?!0:l.apply(this,arguments)}}(jwplayer),function(f){var d=jwplayer.utils;f.sitecatalyst=function(f){function b(b){a.debug&&d.log(b)}function j(a){a=a.split("/");a=a[a.length-1];a=a.split("?");return a[0]}function c(){if(!p){p=!0;var a=k.getPosition();b("stop: "+r+" : "+a);s.Media.stop(r,a)}}function g(){m||(c(),m=!0,b("close: "+r),s.Media.close(r),w=!0,h=0)}var k=f,a=d.extend({},k.config.sitecatalyst),e={onPlay:function(){if(!w){var a=
    k.getPosition();p=!1;b("play: "+r+" : "+a);s.Media.play(r,a)}},onPause:c,onBuffer:c,onIdle:g,onPlaylistItem:function(b){try{w=!0;g();h=0;var c;if(a.mediaName)c=a.mediaName;else{var e=k.getPlaylistItem(b.index);c=e.title?e.title:e.file?j(e.file):e.sources&&e.sources.length?j(e.sources[0].file):""}r=c;q=a.playerName?a.playerName:k.id}catch(f){d.log(f)}},onTime:function(){if(w){var a=k.getDuration();if(-1==a)return;m=p=w=!1;b("open: "+r+" : "+a+" : "+q);s.Media.open(r,a,q);b("play: "+r+" : 0");s.Media.play(r,
    0)}a=k.getPosition();if(3<=Math.abs(a-h)){var c=h;b("seek: "+c+" to "+a);b("stop: "+r+" : "+c);s.Media.stop(r,c);b("play: "+r+" : "+a);s.Media.play(r,a)}h=a},onComplete:g},r,q,h,p=!0,m=!0,w;d.foreach(e,function(a){k[a](e[a])})}}(jwplayer.embed),function(f){function d(b,d){b[d]&&(b[d]=l.getAbsolutePath(b[d]))}var l=f.utils,b=f._,j=window.location.href;f.cast.setupCastConfig=function(b,g){var k=l.extend({},b.config);k.cast=l.extend({pageUrl:j},g);for(var a="base autostart controls fallback fullscreen width height mobilecontrols modes playlistlayout playlistposition playlistsize primary stretching sharing related ga skin logo listbar events".split(" "),
                                                                                                                                                                                                                                                                                                                                                                                                                                                                             e=a.length;e--;)delete k[a[e]];a=k.plugins;delete k.plugins;for(var r in a)if(a.hasOwnProperty(r)){var q=a[r];if(q.client&&(/[\.\/]/.test(q.client)&&d(q,"client"),-1<q.client.indexOf("vast"))){e=k;q=l.extend({},q);q.client="vast";delete q.companiondiv;if(q.schedule){var h=void 0;for(h in q.schedule)q.schedule.hasOwnProperty(h)&&d(q.schedule[h].ad||q.schedule[h],"tag")}d(q,"tag");e.advertising=q}}b.position&&(k.position=b.position);0<b.item&&(k.item=b.item);k.captionLabel=l.getCookies().captionLabel;
    k.playerVersion=f.version;return k};f.cast.setupFlashCastConfig=function(c){var d=c.config;d.playlist=c.getPlaylist();var j;(j=b.find(d.plugins,function(a,b){return 0<b.indexOf("vast.js")}))?(j.client="vast",j={advertising:j}):j={};d=b.pick(d,"id captionLabel cast key playlist repeat".split(" "));d.cast.pageUrl=window.location.href;l.extend(d,{playerVersion:f.version,captionLabel:l.getCookies().captionLabel,volume:c.getVolume(),mute:c.getMute(),id:c.id,position:c.getPosition(),item:c.getPlaylistIndex()},
    j);return d}}(window.jwplayer),function(f,d){function l(a,b){a[b]&&(a[b]=j.getAbsolutePath(a[b]))}var b=d.cast,j=d.utils,c=d.events,g=c.state,k={};b.NS="urn:x-cast:com.longtailvideo.jwplayer";b.debug=!1;b.availability=null;b.available=function(a){a=a||b.availability;var c=f.chrome,d="available";c.cast&&c.cast.ReceiverAvailability&&(d=c.cast.ReceiverAvailability.AVAILABLE);return a===d};b.controller=function(a,e){var r,q;function h(a,c){b.log("send command",a,c);var d={command:a};void 0!==c&&(d.args=
    c);A.sendMessage(b.NS,d,N,function(a){b.log("error message",a);"Invalid namespace"===a.description&&x()})}function p(a){a=!!b.available(a.availability);M.available!==a&&(M.available=a,t(c.JWPLAYER_CAST_AVAILABLE))}function m(a){b.log("existing session",a);!A&&!H&&(H=a.session,H.addMessageListener(b.NS,w))}function w(f,g){var h=JSON.parse(g);if(!h)throw"Message not proper JSON";if(h.reconcile){H.removeMessageListener(b.NS,w);var j=h.diff,k=H;if(!j.id||!h.appid||!h.pageUrl)j.id=d().id,h.appid=G.appid,
    h.pageUrl=T,H=A=null;j.id===a.id&&(h.appid===G.appid&&h.pageUrl===T)&&(A||(a.jwInstreamState()&&a.jwInstreamDestroy(!0),B(k),e.sendEvent(c.JWPLAYER_PLAYER_STATE,{oldstate:j.oldstate,newstate:j.newstate})),P(h));H=null}}function u(a){M.active=!!a;a=M;var b;b=A&&A.receiver?A.receiver.friendlyName:"";a.deviceName=b;t(c.JWPLAYER_CAST_SESSION,{})}function t(a){var b=j.extend({},M);e.sendEvent(a,b)}function n(a){var c=f.chrome;a.code!==c.cast.ErrorCode.CANCEL&&(b.log("Cast Session Error:",a,A),a.code===
c.cast.ErrorCode.SESSION_ERROR&&x())}function v(a){var c=f.chrome;a.code!==c.cast.ErrorCode.CANCEL&&(b.log("Cast Session Error:",a,A),a.code===c.cast.ErrorCode.SESSION_ERROR&&x())}function x(){A?(F(),A.stop(E,z),Q({item:0}),P({type:"state",diff:{buffer:0,position:0,duration:0}})):E()}function z(a){b.error("Cast Session Stop error:",a,A);E()}function B(k){b.log("Session started:",k);if(A)x(),O=k;else{A=k;A.addMessageListener(b.NS,J);A.addUpdateListener(y);a.jwPause(!0);a.jwSetFullscreen(!1);L=e.getVideo();
    r=e.volume;q=e.mute;D=new b.provider(h);D.init();e.setVideoProvider(D);a.jwPlay=function(a){!1===a?D.pause():D.play()};a.jwPause=function(b){a.jwPlay(!!b)};a.jwLoad=function(a){"number"===j.typeOf(a)&&e.setItem(a);D.load(a)};a.jwPlaylistItem=function(a){"number"===j.typeOf(a)&&e.setItem(a);D.playlistItem(a)};a.jwPlaylistNext=function(){a.jwPlaylistItem(e.item+1)};a.jwPlaylistPrev=function(){a.jwPlaylistItem(e.item-1)};a.jwSetVolume=function(a){j.exists(a)&&(a=Math.min(Math.max(0,a),100)|0,R(a)&&(a=
        Math.max(0,Math.min(a/100,1)),A.setReceiverVolumeLevel(a,K,function(a){b.error("set volume error",a);K()})))};a.jwSetMute=function(a){j.exists(a)||(a=!I.mute);S(a)&&A.setReceiverMuted(!!a,K,function(a){b.error("set muted error",a);K()})};a.jwGetVolume=function(){return I.volume|0};a.jwGetMute=function(){return!!I.mute};a.jwIsBeforePlay=function(){return!1};var p=a.jwSetCurrentCaptions;a.jwSetCurrentCaptions=function(a){p(a);D.sendCommand("caption",a)};a.jwSkipAd=function(a){C&&(C.skipAd(a),a=C.getAdModel(),
        a.complete=!0,e.sendEvent(c.JWPLAYER_CAST_AD_CHANGED,a))};a.jwClickAd=function(b){if(C&&300<C.timeSinceClick()&&(C.clickAd(b),e.state!==g.PAUSED)){var h={tag:b.tag};b.sequence&&(h.sequence=b.sequence);b.podcount&&(h.podcount=b.podcount);d(a.id).dispatchEvent(c.JWPLAYER_AD_CLICK,h);f.open(b.clickthrough)}};a.jwPlayAd=a.jwPauseAd=a.jwSetControls=a.jwForceState=a.jwReleaseState=a.jwSetFullscreen=a.jwDetachMedia=a.jwAttachMedia=N;var l=d(a.id).plugins;l.vast&&l.vast.jwPauseAd!==N&&(U={jwPlayAd:l.vast.jwPlayAd,
        jwPauseAd:l.vast.jwPauseAd},l.vast.jwPlayAd=l.vast.jwPauseAd=N);K();u(!0);k!==H&&(D.setup(d.cast.setupCastConfig(e,G),e),d.cast.loader.sendDummyMedia(k))}}function y(a){b.log("Cast Session status",a);a?K():(D.sendEvent(c.JWPLAYER_PLAYER_STATE,{oldstate:e.state,newstate:g.BUFFERING}),E())}function E(){b.log("_sessionStopped");A&&(F(),A=null);if(L){delete a.jwSkipAd;delete a.jwClickAd;a.initializeAPI();var f=d(a.id).plugins;f.vast&&j.extend(f.vast,U);e.volume=r;e.mute=q;e.setVideoProvider(L);e.duration=
    0;D&&(D.destroy(),D=null);C&&(C.destroy(),C=null);e.state!==g.IDLE?j.isIPad()||j.isIPod()?(a.jwStop(!0),L.sendEvent(c.JWPLAYER_PLAYER_STATE,{oldstate:g.BUFFERING,newstate:g.IDLE})):(e.state=g.IDLE,a.jwPlay(!0),a.jwSeek(e.position)):L.sendEvent(c.JWPLAYER_PLAYER_STATE,{oldstate:g.BUFFERING,newstate:g.IDLE});L=null}u(!1);null!==O&&(B(O),O=null)}function F(){A.removeUpdateListener(y);A.removeMessageListener(b.NS,J)}function J(a,b){var c=JSON.parse(b);if(!c)throw"Message not proper JSON";P(c)}function P(d){if("state"===
    d.type){if(C&&(d.diff.newstate||d.diff.position))C.destroy(),C=null,e.setVideoProvider(D),e.sendEvent(c.JWPLAYER_CAST_AD_CHANGED,{done:!0});D.updateModel(d.diff,d.type);Q(d.diff)}else if("ad"===d.type){null===C&&(C=new b.adprovider(b.NS,A),C.init(),e.setVideoProvider(C));C.updateModel(d.diff,d.type);var f=C.getAdModel();d.diff.clickthrough&&(f.onClick=a.jwClickAd);d.diff.skipoffset&&(f.onSkipAd=a.jwSkipAd);e.sendEvent(c.JWPLAYER_CAST_AD_CHANGED,f);d.diff.complete&&C.resetAdModel()}else"connection"===
d.type?!0===d.closed&&x():b.error("received unhandled message",d.type,d)}function Q(a){void 0!==a.item&&e.item!==a.item&&(e.item=a.item,e.sendEvent(c.JWPLAYER_PLAYLIST_ITEM,{index:e.item}))}function K(){if(A&&A.receiver){var a=A.receiver.volume;if(a){var b=100*a.level|0;S(!!a.muted);R(b)}}}function R(a){var b=I.volume!==a;b&&(I.volume=a,D.sendEvent(c.JWPLAYER_MEDIA_VOLUME,{volume:a}));return b}function S(a){var b=I.mute!==a;b&&(I.mute=a,D.sendEvent(c.JWPLAYER_MEDIA_MUTE,{mute:a}));return b}function N(){}
    var A=null,M={available:!1,active:!1,deviceName:""},I={volume:null,mute:null},T=j.getAbsolutePath(f.location.href),G,D=null,C=null,L=null;r=e.volume;q=e.mute;var H=null,O=null,U=null;G=j.extend({},k,e.cast);l(G,"loadscreen");l(G,"endscreen");l(G,"logo");if(G.appid&&(!f.cast||!f.cast.receiver))b.loader.addEventListener("availability",p),b.loader.addEventListener("session",m),b.loader.initialize(G.appid);this.startCasting=function(){A||a.jwInstreamState()||f.chrome.cast.requestSession(B,n)};this.stopCasting=
        x;this.openExtension=function(){a.jwInstreamState()||f.chrome.cast.requestSession(B,v)}};b.log=function(){if(b.debug){var a=Array.prototype.slice.call(arguments,0);console.log.apply(console,a)}};b.error=function(){var a=Array.prototype.slice.call(arguments,0);console.error.apply(console,a)}}(window,jwplayer),function(f){function d(a){h.log("existing session",a);!y&&!x&&(x=a.session,x.addMessageListener(h.NS,l))}function l(a,b){var c=JSON.parse(b),d=x;if(!c)throw"Message not proper JSON";if(c.reconcile){x.removeMessageListener(h.NS,
    l);c.receiverFriendlyName=x.receiver.friendlyName;if(!c.pageUrl||!c.diff.id||!c.appid)c.pageUrl=B,c.diff.id=f().id,c.appid=v,x=y=null;t[c.diff.id]&&(v===c.appid&&c.pageUrl===B)&&(u=c.diff.id,v=c.appid,r(u,"jwInstreamDestroy"),g(d),r(u,n.message,c),x=null)}}function b(){y&&(y.removeUpdateListener(e),y.removeMessageListener(h.NS,k),y.stop(j.bind(this),a.bind(this)),y=null);r(u,n.cleanup)}function j(){"undefined"!==typeof z&&null!==z&&(g(z),z=null)}function c(a,b){y.sendMessage(h.NS,{command:a,args:b},
    p.noop,function(a){h.error("message send error",a)})}function g(a){var d=f(u);y?(b(),z=a):(y=a,y.addMessageListener(h.NS,k),y.addUpdateListener(e),d=f.cast.setupFlashCastConfig(d),x!==y&&(c("setup",d),f.cast.loader.sendDummyMedia(y)),r(u,n.connected,{receiverFriendlyName:a.receiver.friendlyName}))}function k(a,b){if(b){var c=JSON.parse(b);if(!c)throw"Message not proper JSON";r(u,n.message,c)}}function a(a){r(u,n.error,a||{})}function e(a){a?q():b()}function r(a,b,c){c?f(a).callInternal(b,c):f(a).callInternal(b)}
    function q(){if(y&&y.receiver){var a=y.receiver.volume;if(a){var b=100*a.level|0,a=!!a.muted;f(u).setVolume(b).setMute(a)}}}var h=f.cast,p=f.utils,m=f._,w=window.chrome||{},u,t={},n={},v,x,z,B=p.getAbsolutePath(window.location.href),y;h.NS="urn:x-cast:com.longtailvideo.jwplayer";h.flash={checkAvailability:function(a,b,c){n=c;v=b;t[a]=!0;h.loader.addEventListener("availability",function(b){"available"===b.availability&&r(a,n.available,b)});h.loader.addEventListener("session",d);h.loader.initialize(b)},
        startCasting:function(b){u=b;w.cast.requestSession(g.bind(this),a.bind(this))},stopCasting:b,openExtension:function(b){u=b;w.cast.requestSession(g.bind(this),a.bind(this))},mute:function(a){y.setReceiverMuted(a,q,function(a){h.error("set muted error",a)})},volume:function(a){a=Math.max(0,Math.min(a/100,1));y.setReceiverVolumeLevel(a,q,function(a){h.error("set volume error",a)})},messageReceiver:c,canCastItem:function(a){return m.any(a.levels,function(a){return f.embed.html5CanPlay(a.file,a.type)||
            "hls"===a.type})}}}(window.jwplayer),function(f,d){function l(a){var b=new d.cast.media.MediaInfo("");b.contentType="video/mp4";b=new d.cast.media.LoadRequest(b);b.autoplay=!1;a.loadMedia(b)}function b(){d&&d.cast&&d.cast.isAvailable&&!e.apiConfig?(e.apiConfig=new d.cast.ApiConfig(new d.cast.SessionRequest(m),k,a,d.cast.AutoJoinPolicy.ORIGIN_SCOPED),d.cast.initialize(e.apiConfig,g,c)):15>p++&&setTimeout(b,1E3)}function j(){h&&(h.resetEventListeners(),h=null)}function c(){e.apiConfig=null}function g(){}
    function k(a){w.sendEvent("session",{session:a});a.sendMessage(e.NS,{whoami:1});0===a.media.length&&l(a)}function a(a){e.availability=a;w.sendEvent("availability",{availability:a})}window.chrome=d;var e=f.cast,r=f.utils,q=f.events,h,p=0,m=null,w=r.extend({initialize:function(a){m=a;null!==e.availability?w.sendEvent("availability",{availability:e.availability}):d&&d.cast?b():h||(h=new r.scriptloader("https://www.gstatic.com/cv/js/sender/v1/cast_sender.js"),h.addEventListener(q.ERROR,j),h.addEventListener(q.COMPLETE,
        b),h.load())}},new q.eventdispatcher("cast.loader"));f.cast.loader=w;f.cast.loader.sendDummyMedia=l}(window.jwplayer,window.chrome||{}),function(f,d){var l=[],b=f.utils,j=f.events,c=f._,g=0,k=j.state,a="getBuffer getCaptionsList getControls getCurrentCaptions getCurrentQuality getCurrentAudioTrack getDuration getFullscreen getHeight getLockState getMute getPlaylistIndex getSafeRegion getPosition getQualityLevels getState getVolume getWidth isBeforeComplete isBeforePlay releaseState".split(" "),e=
    "playlistNext stop forceState playlistPrev seek setCurrentCaptions setControls setCurrentQuality setVolume setCurrentAudioTrack".split(" "),r={onBufferChange:j.JWPLAYER_MEDIA_BUFFER,onBufferFull:j.JWPLAYER_MEDIA_BUFFER_FULL,onError:j.JWPLAYER_ERROR,onSetupError:j.JWPLAYER_SETUP_ERROR,onFullscreen:j.JWPLAYER_FULLSCREEN,onMeta:j.JWPLAYER_MEDIA_META,onMute:j.JWPLAYER_MEDIA_MUTE,onPlaylist:j.JWPLAYER_PLAYLIST_LOADED,onPlaylistItem:j.JWPLAYER_PLAYLIST_ITEM,onPlaylistComplete:j.JWPLAYER_PLAYLIST_COMPLETE,
    onReady:j.API_READY,onResize:j.JWPLAYER_RESIZE,onComplete:j.JWPLAYER_MEDIA_COMPLETE,onSeek:j.JWPLAYER_MEDIA_SEEK,onTime:j.JWPLAYER_MEDIA_TIME,onVolume:j.JWPLAYER_MEDIA_VOLUME,onBeforePlay:j.JWPLAYER_MEDIA_BEFOREPLAY,onBeforeComplete:j.JWPLAYER_MEDIA_BEFORECOMPLETE,onDisplayClick:j.JWPLAYER_DISPLAY_CLICK,onControls:j.JWPLAYER_CONTROLS,onQualityLevels:j.JWPLAYER_MEDIA_LEVELS,onQualityChange:j.JWPLAYER_MEDIA_LEVEL_CHANGED,onCaptionsList:j.JWPLAYER_CAPTIONS_LIST,onCaptionsChange:j.JWPLAYER_CAPTIONS_CHANGED,
    onAdError:j.JWPLAYER_AD_ERROR,onAdClick:j.JWPLAYER_AD_CLICK,onAdImpression:j.JWPLAYER_AD_IMPRESSION,onAdTime:j.JWPLAYER_AD_TIME,onAdComplete:j.JWPLAYER_AD_COMPLETE,onAdCompanions:j.JWPLAYER_AD_COMPANIONS,onAdSkipped:j.JWPLAYER_AD_SKIPPED,onAdPlay:j.JWPLAYER_AD_PLAY,onAdPause:j.JWPLAYER_AD_PAUSE,onAdMeta:j.JWPLAYER_AD_META,onCast:j.JWPLAYER_CAST_SESSION,onAudioTrackChange:j.JWPLAYER_AUDIO_TRACK_CHANGED,onAudioTracks:j.JWPLAYER_AUDIO_TRACKS},q={onBuffer:k.BUFFERING,onPause:k.PAUSED,onPlay:k.PLAYING,
    onIdle:k.IDLE};f.api=function(h){function g(a,c){b.foreach(a,function(a,b){n[a]=function(a){return c(b,a)}})}function m(a,b){var c="jw"+b.charAt(0).toUpperCase()+b.slice(1);n[b]=function(){var b=t.apply(this,[c].concat(Array.prototype.slice.call(arguments,0)));return a?n:b}}function w(a,c){try{a.jwAddEventListener(c,'function(dat) { jwplayer("'+n.id+'").dispatchEvent("'+c+'", dat); }')}catch(d){if("flash"===n.renderingMode){var e=document.createElement("a");e.href=z.data;e.protocol!==location.protocol&&
b.log("Warning: Your site ["+location.protocol+"] and JWPlayer ["+e.protocol+"] are hosted using different protocols")}b.log("Could not add internal listener")}}function u(a,b){v[a]||(v[a]=[],z&&B&&w(z,a));v[a].push(b);return n}function t(){if(B){if(z){var a=Array.prototype.slice.call(arguments,0),b=a.shift();if("function"===typeof z[b]){switch(a.length){case 6:return z[b](a[0],a[1],a[2],a[3],a[4],a[5]);case 5:return z[b](a[0],a[1],a[2],a[3],a[4]);case 4:return z[b](a[0],a[1],a[2],a[3]);case 3:return z[b](a[0],
    a[1],a[2]);case 2:return z[b](a[0],a[1]);case 1:return z[b](a[0])}return z[b]()}}return null}y.push(arguments)}var n=this,v={},x={},z,B=!1,y=[],E,F={},J={};n.container=h;n.id=h.id;n.setup=function(a){if(f.embed){f.api.destroyPlayer(n.id);var b=new f.api(n.container);f.api.addPlayer(b);b.config=a;b._embedder=new f.embed(b);b._embedder.embed();return b}return n};n.getContainer=function(){return n.container};n.addButton=function(a,c,d,e){try{J[e]=d,t("jwDockAddButton",a,c,'jwplayer("'+n.id+'").callback("'+
    e+'")',e)}catch(f){b.log("Could not add dock button"+f.message)}};n.removeButton=function(a){t("jwDockRemoveButton",a)};n.callback=function(a){if(J[a])J[a]()};n.getMeta=function(){return n.getItemMeta()};n.getPlaylist=function(){var a=t("jwGetPlaylist");"flash"===n.renderingMode&&b.deepReplaceKeyName(a,["__dot__","__spc__","__dsh__","__default__"],["."," ","-","default"]);return a};n.getPlaylistItem=function(a){b.exists(a)||(a=n.getPlaylistIndex());return n.getPlaylist()[a]};n.getRenderingMode=function(){return n.renderingMode};
    n.setFullscreen=function(a){b.exists(a)?t("jwSetFullscreen",a):t("jwSetFullscreen",!t("jwGetFullscreen"));return n};n.setMute=function(a){b.exists(a)?t("jwSetMute",a):t("jwSetMute",!t("jwGetMute"));return n};n.lock=function(){return n};n.unlock=function(){return n};n.load=function(a){t("jwInstreamDestroy");f(n.id).plugins.googima&&t("jwDestroyGoogima");t("jwLoad",a);return n};n.playlistItem=function(a){t("jwPlaylistItem",parseInt(a,10));return n};n.resize=function(a,c){if("flash"!==n.renderingMode)t("jwResize",
        a,c);else{var d=document.getElementById(n.id+"_wrapper"),e=document.getElementById(n.id+"_aspect");e&&(e.style.display="none");d&&(d.style.display="block",d.style.width=b.styleDimension(a),d.style.height=b.styleDimension(c))}return n};n.play=function(a){if(a!==d)return t("jwPlay",a),n;a=n.getState();var b=E&&E.getState();b&&(b===k.IDLE||b===k.PLAYING||b===k.BUFFERING?t("jwInstreamPause"):t("jwInstreamPlay"));a===k.PLAYING||a===k.BUFFERING?t("jwPause"):t("jwPlay");return n};n.pause=function(a){a===
    d?(a=n.getState(),a===k.PLAYING||a===k.BUFFERING?t("jwPause"):t("jwPlay")):t("jwPause",a);return n};n.createInstream=function(){return new f.api.instream(this,z)};n.setInstream=function(a){return E=a};n.loadInstream=function(a,b){E=n.setInstream(n.createInstream()).init(b);E.loadItem(a);return E};n.destroyPlayer=function(){B=!0;t("jwPlayerDestroy")};n.playAd=function(a){var b=f(n.id).plugins;b.vast?b.vast.jwPlayAd(a):t("jwPlayAd",a)};n.pauseAd=function(){var a=f(n.id).plugins;a.vast?a.vast.jwPauseAd():
        t("jwPauseAd")};g(q,function(a,b){x[a]||(x[a]=[],u(j.JWPLAYER_PLAYER_STATE,function(b){var c=b.newstate;b=b.oldstate;if(c===a){var d=x[c];if(d)for(var e=0;e<d.length;e++){var f=d[e];"function"===typeof f&&f.call(this,{oldstate:b,newstate:c})}}}));x[a].push(b);return n});g(r,u);b.foreach(a,function(a,b){m(!1,b)});b.foreach(e,function(a,b){m(!0,b)});n.remove=function(){this._embedder&&this._embedder.destroy&&this._embedder.destroy();y=[];var a=1<c.size(c.where(l,{id:n.id}));a||b.clearCss("#"+n.id);
        var d=document.getElementById(n.id+("flash"===n.renderingMode?"_wrapper":""));if(d){if("html5"===n.renderingMode)n.destroyPlayer();else if(b.isMSIE(8)){var e=document.getElementById(n.id);if(e&&e.parentNode){e.style.display="none";for(var f in e)"function"===typeof e[f]&&(e[f]=null);e.parentNode.removeChild(e)}}a||(a=document.createElement("div"),a.id=n.id,d.parentNode.replaceChild(a,d))}l=c.filter(l,function(a){return a.uniqueId!==n.uniqueId})};n.registerPlugin=function(a,b,c,d){f.plugins.registerPlugin(a,
        b,c,d)};n.setPlayer=function(a,b){z=a;n.renderingMode=b};n.detachMedia=function(){if("html5"===n.renderingMode)return t("jwDetachMedia")};n.attachMedia=function(a){if("html5"===n.renderingMode)return t("jwAttachMedia",a)};n.getAudioTracks=function(){return t("jwGetAudioTracks")};n.removeEventListener=function(a,b){var c=v[a];if(c)for(var d=c.length;d--;)c[d]===b&&c.splice(d,1)};n.dispatchEvent=function(a,c){var d=v[a];if(d)for(var d=d.slice(0),e=b.translateEventResponse(a,c),f=0;f<d.length;f++){var h=
        d[f];if("function"===typeof h)try{a===j.JWPLAYER_PLAYLIST_LOADED&&b.deepReplaceKeyName(e.playlist,["__dot__","__spc__","__dsh__","__default__"],["."," ","-","default"]),h.call(this,e)}catch(g){b.log("There was an error calling back an event handler",g)}}};n.dispatchInstreamEvent=function(a){E&&E.dispatchEvent(a,arguments)};n.callInternal=t;n.playerReady=function(a){B=!0;z||n.setPlayer(document.getElementById(a.id));n.container=document.getElementById(n.id);b.foreach(v,function(a){w(z,a)});u(j.JWPLAYER_PLAYLIST_ITEM,
        function(){F={}});u(j.JWPLAYER_MEDIA_META,function(a){b.extend(F,a.metadata)});u(j.JWPLAYER_VIEW_TAB_FOCUS,function(a){var c=n.getContainer();!0===a.hasFocus?b.addClass(c,"jw-tab-focus"):b.removeClass(c,"jw-tab-focus")});for(n.dispatchEvent(j.API_READY);0<y.length;)t.apply(n,y.shift())};n.getItemMeta=function(){return F};return n};f.playerReady=function(a){var b=f.api.playerById(a.id);b||(b=f.api.selectPlayer(a.id));b.playerReady(a)};f.api.selectPlayer=function(a){var c;b.exists(a)||(a=0);a.nodeType?
    c=a:"string"===typeof a&&(c=document.getElementById(a));return c?(a=f.api.playerById(c.id))?a:new f.api(c):"number"===typeof a?l[a]:null};f.api.playerById=function(a){for(var b=0;b<l.length;b++)if(l[b].id===a)return l[b];return null};f.api.addPlayer=function(a){for(var b=0;b<l.length;b++)if(l[b]===a)return a;g++;a.uniqueId=g;l.push(a);return a};f.api.destroyPlayer=function(a){a=c.where(l,{id:a});c.each(a,c.partial(c.result,c,"remove"))}}(window.jwplayer),function(f){var d=f.events,l=f.utils,b=d.state;
    f.api.instream=function(f,c){function g(a,b){r[a]||(r[a]=[],c.jwInstreamAddEventListener(a,'function(dat) { jwplayer("'+f.id+'").dispatchInstreamEvent("'+a+'", dat); }'));r[a].push(b);return this}function k(a,b){q[a]||(q[a]=[],g(d.JWPLAYER_PLAYER_STATE,function(b){var c=b.newstate,d=b.oldstate;if(c===a){var e=q[c];if(e)for(var f=0;f<e.length;f++){var g=e[f];"function"===typeof g&&g.call(this,{oldstate:d,newstate:c,type:b.type})}}}));q[a].push(b);return this}var a,e,r={},q={},h=this;h.type="instream";
        h.init=function(){f.callInternal("jwInitInstream");return h};h.loadItem=function(b,c){a=b;e=c||{};"array"===l.typeOf(b)?f.callInternal("jwLoadArrayInstream",a,e):f.callInternal("jwLoadItemInstream",a,e)};h.removeEvents=function(){r=q={}};h.removeEventListener=function(a,b){var c=r[a];if(c)for(var d=c.length;d--;)c[d]===b&&c.splice(d,1)};h.dispatchEvent=function(a,b){var c=r[a];if(c)for(var c=c.slice(0),d=l.translateEventResponse(a,b[1]),e=0;e<c.length;e++){var f=c[e];"function"===typeof f&&f.call(this,
            d)}};h.onError=function(a){return g(d.JWPLAYER_ERROR,a)};h.onMediaError=function(a){return g(d.JWPLAYER_MEDIA_ERROR,a)};h.onFullscreen=function(a){return g(d.JWPLAYER_FULLSCREEN,a)};h.onMeta=function(a){return g(d.JWPLAYER_MEDIA_META,a)};h.onMute=function(a){return g(d.JWPLAYER_MEDIA_MUTE,a)};h.onComplete=function(a){return g(d.JWPLAYER_MEDIA_COMPLETE,a)};h.onPlaylistComplete=function(a){return g(d.JWPLAYER_PLAYLIST_COMPLETE,a)};h.onPlaylistItem=function(a){return g(d.JWPLAYER_PLAYLIST_ITEM,a)};h.onTime=
            function(a){return g(d.JWPLAYER_MEDIA_TIME,a)};h.onBuffer=function(a){return k(b.BUFFERING,a)};h.onPause=function(a){return k(b.PAUSED,a)};h.onPlay=function(a){return k(b.PLAYING,a)};h.onIdle=function(a){return k(b.IDLE,a)};h.onClick=function(a){return g(d.JWPLAYER_INSTREAM_CLICK,a)};h.onInstreamDestroyed=function(a){return g(d.JWPLAYER_INSTREAM_DESTROYED,a)};h.onAdSkipped=function(a){return g(d.JWPLAYER_AD_SKIPPED,a)};h.play=function(a){c.jwInstreamPlay(a)};h.pause=function(a){c.jwInstreamPause(a)};
        h.hide=function(){f.callInternal("jwInstreamHide")};h.destroy=function(){h.removeEvents();f.callInternal("jwInstreamDestroy")};h.setText=function(a){c.jwInstreamSetText(a?a:"")};h.getState=function(){return c.jwInstreamState()};h.setClick=function(a){c.jwInstreamClick&&c.jwInstreamClick(a)}}}(jwplayer),function(f){var d=f.api,l=d.selectPlayer,b=f._;d.selectPlayer=function(b){var c=l.apply(this,arguments);return c?c:{registerPlugin:function(b,c,a){"jwpsrv"!==b&&f.plugins.registerPlugin(b,c,a)}}};f.unregisteredProviders=
    [];d.registerProvider=function(d){f&&f.html5&&b.isFunction(f.html5.registerProvider)?f.html5.registerProvider(d):f.unregisteredProviders.push(d)}}(jwplayer));
(function(d){d.html5={};d.html5.version="6.12.4956";d=d.utils.css;var l=".jwplayer ;div;span;a;img;ul;li;video".split(";").join(", .jwplayer ");d(l+", .jwclick",{margin:0,padding:0,border:0,color:"#000000","font-size":"100%",font:"inherit","vertical-align":"baseline","background-color":"transparent","text-align":"left",direction:"ltr","line-height":20,"-webkit-tap-highlight-color":"rgba(255, 255, 255, 0)"});d(".jwplayer ,.jwplayer *",{"box-sizing":"content-box"});d(".jwplayer * button,.jwplayer * input,.jwplayer * select,.jwplayer * textarea",
    {"box-sizing":"border-box"});d(".jwplayer ul",{"list-style":"none"});d(".jwplayer .jwcontrols",{"pointer-events":"none"});d(".jwplayer.jw-user-inactive .jwcontrols",{"pointer-events":"all"});d(".jwplayer .jwcontrols .jwdockbuttons, .jwplayer .jwcontrols .jwcontrolbar, .jwplayer .jwcontrols .jwskip, .jwplayer .jwcontrols .jwdisplayIcon, .jwplayer .jwcontrols .jwpreview, .jwplayer .jwcontrols .jwlogo",{"pointer-events":"all"})})(jwplayer);
(function(d){var l=document;d.parseDimension=function(a){return"string"===typeof a?""===a?0:-1<a.lastIndexOf("%")?a:parseInt(a.replace("px",""),10):a};d.timeFormat=function(a){if(0<a){var c=Math.floor(a/3600),f=Math.floor((a-3600*c)/60);a=Math.floor(a%60);return(c?c+":":"")+(10>f?"0":"")+f+":"+(10>a?"0":"")+a}return"00:00"};d.bounds=function(a){var c={left:0,right:0,width:0,height:0,top:0,bottom:0};if(!a||!l.body.contains(a))return c;if(a.getBoundingClientRect){a=a.getBoundingClientRect(a);var f=
    window.pageYOffset,e=window.pageXOffset;if(!a.width&&!a.height&&!a.left&&!a.top)return c;c.left=a.left+e;c.right=a.right+e;c.top=a.top+f;c.bottom=a.bottom+f;c.width=a.right-a.left;c.height=a.bottom-a.top}else{c.width=a.offsetWidth|0;c.height=a.offsetHeight|0;do c.left+=a.offsetLeft|0,c.top+=a.offsetTop|0;while(a=a.offsetParent);c.right=c.left+c.width;c.bottom=c.top+c.height}return c};d.empty=function(a){if(a)for(;0<a.childElementCount;)a.removeChild(a.children[0])}})(jwplayer.utils);
(function(d){var l=d.stretching={NONE:"none",FILL:"fill",UNIFORM:"uniform",EXACTFIT:"exactfit"};d.scale=function(a,c,f,e,h){var j="";c=c||1;f=f||1;e|=0;h|=0;if(1!==c||1!==f)j="scale("+c+", "+f+")";if(e||h)j="translate("+e+"px, "+h+"px)";d.transform(a,j)};d.stretch=function(a,c,f,e,h,j){if(!c||!f||!e||!h||!j)return!1;a=a||l.UNIFORM;var b=2*Math.ceil(f/2)/h,m=2*Math.ceil(e/2)/j,g="video"===c.tagName.toLowerCase(),k=!1,q="jw"+a.toLowerCase();switch(a.toLowerCase()){case l.FILL:b>m?m=b:b=m;k=!0;break;
    case l.NONE:b=m=1;case l.EXACTFIT:k=!0;break;default:b>m?0.95<h*m/f?(k=!0,q="jwexactfit"):(h*=m,j*=m):0.95<j*b/e?(k=!0,q="jwexactfit"):(h*=b,j*=b),k&&(b=2*Math.ceil(f/2)/h,m=2*Math.ceil(e/2)/j)}g?(a={left:"",right:"",width:"",height:""},k?(f<h&&(a.left=a.right=Math.ceil((f-h)/2)),e<j&&(a.top=a.bottom=Math.ceil((e-j)/2)),a.width=h,a.height=j,d.scale(c,b,m,0,0)):(k=!1,d.transform(c)),d.css.style(c,a)):c.className=c.className.replace(/\s*jw(none|exactfit|uniform|fill)/g,"")+" "+q;return k}})(jwplayer.utils);
(function(d){d.dfxp=function(){var d=jwplayer.utils.seconds;this.parse=function(a){var c=[{begin:0,text:""}];a=a.replace(/^\s+/,"").replace(/\s+$/,"");var f=a.split("\x3c/p\x3e"),e=a.split("\x3c/tt:p\x3e"),h=[];for(a=0;a<f.length;a++)0<=f[a].indexOf("\x3cp")&&(f[a]=f[a].substr(f[a].indexOf("\x3cp")+2).replace(/^\s+/,"").replace(/\s+$/,""),h.push(f[a]));for(a=0;a<e.length;a++)0<=e[a].indexOf("\x3ctt:p")&&(e[a]=e[a].substr(e[a].indexOf("\x3ctt:p")+5).replace(/^\s+/,"").replace(/\s+$/,""),h.push(e[a]));
    f=h;for(a=0;a<f.length;a++){e=f[a];h={};try{var j=e.indexOf('begin\x3d"'),e=e.substr(j+7),j=e.indexOf('" end\x3d"');h.begin=d(e.substr(0,j));e=e.substr(j+7);j=e.indexOf('"');h.end=d(e.substr(0,j));j=e.indexOf('"\x3e');e=e.substr(j+2);h.text=e}catch(b){}e=h;e.text&&(c.push(e),e.end&&(c.push({begin:e.end,text:""}),delete e.end))}if(1<c.length)return c;throw{message:"Invalid DFXP file:"};}}})(jwplayer.parsers);
(function(d){d.srt=function(){var d=jwplayer.utils,a=d.seconds;this.parse=function(c,f){var e=f?[]:[{begin:0,text:""}];c=d.trim(c);var h=c.split("\r\n\r\n");1===h.length&&(h=c.split("\n\n"));for(var j=0;j<h.length;j++)if("WEBVTT"!==h[j]){var b,m=h[j];b={};var g=m.split("\r\n");1===g.length&&(g=m.split("\n"));try{m=1;0<g[0].indexOf(" --\x3e ")&&(m=0);var k=g[m].indexOf(" --\x3e ");0<k&&(b.begin=a(g[m].substr(0,k)),b.end=a(g[m].substr(k+5)));if(g[m+1]){b.text=g[m+1];for(m+=2;m<g.length;m++)b.text+=
    "\x3cbr/\x3e"+g[m]}}catch(q){}b.text&&(e.push(b),b.end&&!f&&(e.push({begin:b.end,text:""}),delete b.end))}if(1<e.length)return e;throw{message:"Invalid SRT file"};}}})(jwplayer.parsers);
(function(d){var l=d.utils.noop,a=d.events,c=d._.constant(!1);d.html5.DefaultProvider={supports:c,play:l,load:l,stop:l,volume:l,mute:l,seek:l,seekDrag:l,resize:l,remove:l,destroy:l,setVisibility:l,setFullscreen:c,getFullscreen:l,getContainer:l,setContainer:c,isAudioFile:c,supportsFullscreen:c,getQualityLevels:l,getCurrentQuality:l,setCurrentQuality:l,getAudioTracks:l,getCurrentAudioTrack:l,setCurrentAudioTrack:l,checkComplete:l,setControls:l,attachMedia:l,detachMedia:l,setState:function(c){if(c!==
    this.state){var e=this.state||a.state.IDLE;this.state=c;this.sendEvent(a.JWPLAYER_PLAYER_STATE,{oldstate:e,newstate:c})}}}})(jwplayer);(function(d){d.html5.chooseProvider=function(l){return d._.isObject(l)&&d.html5.YoutubeProvider.supports(l)?d.html5.YoutubeProvider:d.html5.VideoProvider}})(jwplayer);
(function(d){function l(l){function s(){}function r(a){E(a);Y&&(t.state===e.PLAYING&&!T)&&(K=Math.floor(10*I.currentTime)/10,a&&(N=!0),t.sendEvent(f.JWPLAYER_MEDIA_TIME,{position:K,duration:H}))}function n(){t.sendEvent(f.JWPLAYER_MEDIA_META,{duration:I.duration,height:I.videoHeight,width:I.videoWidth})}function y(a){Y&&(N||(N=!0,u()),"loadedmetadata"===a.type&&(I.muted&&(I.muted=!1,I.muted=!0),n()))}function E(){N&&(0<S&&!k)&&(b?setTimeout(function(){0<S&&t.seek(S)},200):t.seek(S))}function u(){P||
(P=!0,t.sendEvent(f.JWPLAYER_MEDIA_BUFFER_FULL))}function p(b){Y&&!T&&(I.paused?I.currentTime===I.duration&&3<I.duration||t.pause():(!a.isFF()||!("play"===b.type&&t.state===e.BUFFERING))&&t.setState(e.PLAYING))}function z(){Y&&(T||t.setState(e.BUFFERING))}function w(b){var c;if("array"===a.typeOf(b)&&0<b.length){c=[];for(var e=0;e<b.length;e++){var f=b[e],g={};g.label=f.label&&f.label?f.label?f.label:0:e;c[e]=g}}return c}function A(b,c){J=da[ea];h(W);W=setInterval(F,100);S=0;I.src!==J.file||m||g?
    (m||t.setState(e.BUFFERING),P=N=!1,H=c?c:-1,I.src=J.file,I.load()):(0===b&&(S=-1,t.seek(b)),n(),I.play());K=I.currentTime;m&&u();a.isIOS()&&t.getFullScreen()&&(I.controls=!0);0<b&&t.seek(b)}function F(){if(Y){var a;a=I.buffered;a=!a||!I.duration||0===a.length?0:a.end(a.length-1)/I.duration;1<=a&&h(W);a!==Z&&(Z=a,t.sendEvent(f.JWPLAYER_MEDIA_BUFFER,{bufferPercent:Math.round(100*Z)}))}}function L(a){t.sendEvent("fullscreenchange",{target:a.target,jwstate:ra})}this.state=e.IDLE;var G=new d.events.eventdispatcher("provider."+
    this.name);a.extend(this,G);var t=this,x={abort:s,canplay:y,canplaythrough:s,click:function(){t.sendEvent(f.JWPLAYER_PROVIDER_CLICK)},durationchange:function(){if(Y){var a=Math.floor(10*I.duration)/10;H!==a&&(H=a);k&&(0<S&&a>S)&&t.seek(S);r()}},emptied:s,ended:function(){Y&&t.state!==e.IDLE&&(h(W),ea=-1,va=!0,t.sendEvent(f.JWPLAYER_MEDIA_BEFORECOMPLETE),Y&&(t.setState(e.IDLE),va=!1,t.sendEvent(f.JWPLAYER_MEDIA_COMPLETE)))},error:function(){Y&&(a.log("Error playing media: %o %s",I.error,I.src||J.file),
    t.sendEvent(f.JWPLAYER_MEDIA_ERROR,{message:"Error loading media: File could not be played"}),t.setState(e.IDLE))},loadeddata:s,loadedmetadata:y,loadstart:s,pause:p,play:p,playing:p,progress:E,ratechange:s,readystatechange:s,seeked:function(){!T&&t.state!==e.PAUSED&&t.setState(e.PLAYING)},seeking:b?z:s,stalled:s,suspend:s,timeupdate:r,volumechange:function(){t.sendEvent(f.JWPLAYER_MEDIA_VOLUME,{volume:Math.round(100*I.volume)});t.sendEvent(f.JWPLAYER_MEDIA_MUTE,{mute:I.muted})},waiting:z,webkitbeginfullscreen:function(b){ra=
    !0;L(b);a.isIOS()&&(I.controls=!1)},webkitendfullscreen:function(b){ra=!1;L(b);a.isIOS()&&(I.controls=!1)}},M,J,H,K,N=!1,P,S=0,T=!1,D,W=-1,Z=-1,Y=!1,da,ea=-1,va=!1,ra=!1;this.sendEvent=function(){Y&&G.sendEvent.apply(this,arguments)};var I=document.getElementById(l).querySelector("video"),Va=I=I||document.createElement("video");a.foreach(x,function(a,b){Va.addEventListener(a,b,!1)});q||(I.controls=!0,I.controls=!1);I.setAttribute("x-webkit-airplay","allow");I.setAttribute("webkit-playsinline","");
    Y=!0;this.stop=function(){Y&&(h(W),I.removeAttribute("src"),b||I.load(),ea=-1,this.setState(e.IDLE))};this.destroy=function(){var b=I;a.foreach(x,function(a,c){b.removeEventListener(a,c,!1)});this.remove()};this.load=function(b){if(Y){da=b.sources;0>ea&&(ea=0);if(da)for(var c=a.getCookies().qualityLabel,e=0;e<da.length;e++)if(da[e]["default"]&&(ea=e),c&&da[e].label===c){ea=e;break}(c=w(da))&&t.sendEvent(f.JWPLAYER_MEDIA_LEVELS,{levels:c,currentQuality:ea});A(b.starttime||0,b.duration)}};this.play=
        function(){Y&&!T&&I.play()};this.pause=function(){Y&&(I.pause(),this.setState(e.PAUSED))};this.seekDrag=function(a){Y&&((T=a)?I.pause():I.play())};this.seek=function(a){if(Y)if(!T&&0===S&&this.sendEvent(f.JWPLAYER_MEDIA_SEEK,{position:K,offset:a}),N){S=0;try{I.currentTime=a}catch(b){S=a}}else S=a};this.volume=function(b){a.exists(b)&&(I.volume=Math.min(Math.max(0,b/100),1),D=100*I.volume)};this.mute=function(b){a.exists(b)||(b=!I.muted);b?(D=100*I.volume,I.muted=!0):(this.volume(D),I.muted=!1)};this.setState=
        function(a){a===e.PAUSED&&this.state===e.IDLE||T||j.setState.apply(this,arguments)};this.checkComplete=function(){return va};this.detachMedia=function(){h(W);Y=!1;return I};this.attachMedia=function(a){Y=!0;a||(N=!1);va&&(this.setState(e.IDLE),this.sendEvent(f.JWPLAYER_MEDIA_COMPLETE),va=!1)};this.setContainer=function(a){M=a;a.appendChild(I)};this.getContainer=function(){return M};this.remove=function(){I&&(I.removeAttribute("src"),b||I.load());h(W);ea=-1;M===I.parentNode&&M.removeChild(I)};this.setVisibility=
        function(b){b||k?a.css.style(M,{visibility:"visible",opacity:1}):a.css.style(M,{visibility:"",opacity:0})};this.resize=function(b,c,e){return a.stretch(e,I,b,c,I.videoWidth,I.videoHeight)};this.setControls=function(a){I.controls=!!a};this.supportsFullscreen=c.constant(!0);this.setFullScreen=function(a){if(a=!!a){try{var b=I.webkitEnterFullscreen||I.webkitEnterFullScreen;b&&b.apply(I)}catch(c){return!1}return t.getFullScreen()}(b=I.webkitExitFullscreen||I.webkitExitFullScreen)&&b.apply(I);return a};
    t.getFullScreen=function(){return ra||!!I.webkitDisplayingFullscreen};this.isAudioFile=function(){if(!da)return!1;var a=da[0].type;return"oga"===a||"aac"===a||"mp3"===a||"vorbis"===a};this.setCurrentQuality=function(b){if(ea!==b&&(b=parseInt(b,10),0<=b&&da&&da.length>b)){ea=b;a.saveCookie("qualityLabel",da[b].label);this.sendEvent(f.JWPLAYER_MEDIA_LEVEL_CHANGED,{currentQuality:b,levels:w(da)});b=Math.floor(10*I.currentTime)/10;var c=Math.floor(10*I.duration)/10;0>=c&&(c=H);A(b,c)}};this.getCurrentQuality=
        function(){return ea};this.getQualityLevels=function(){return w(da)}}var a=d.utils,c=d._,f=d.events,e=f.state,h=window.clearInterval,j=d.html5.DefaultProvider,b=a.isMSIE(),m=a.isMobile(),g=a.isSafari(),k=a.isAndroidNative(),q=a.isIOS(7),s=function(){};s.prototype=j;l.prototype=new s;l.supports=c.constant(!0);d.html5.VideoProvider=l})(jwplayer);
(function(d){function l(g){function l(){window.YT&&window.YT.loaded?(t=window.YT,s()):setTimeout(l,100)}function m(){j=null}function s(){var a;if(a=t)a=M&&M.parentNode,a||(K||(d(g).onReady(s),K=!0),a=!1);a&&N&&N.apply(G)}function v(){if(x&&x.getPlayerState){var a=x.getPlayerState();null!==a&&(void 0!==a&&a!==T)&&u({data:a});var b=t.PlayerState;a===b.PLAYING?(r(),G.sendEvent(f.JWPLAYER_MEDIA_TIME,{position:C(x.getCurrentTime()),duration:x.getDuration()})):a===b.BUFFERING&&r()}}function C(a){return Math.round(10*
        a)/10}function r(){var a=0;x&&x.getVideoLoadedFraction&&(a=Math.round(100*x.getVideoLoadedFraction()));H!==a&&(H=a,G.sendEvent(f.JWPLAYER_MEDIA_BUFFER,{bufferPercent:a}))}function n(){G.sendEvent(f.JWPLAYER_MEDIA_META,{duration:x.getDuration(),width:M.clientWidth,height:M.clientHeight})}function y(){var a=arguments,b=a.length-1;return function(){for(var c=b,e=a[b].apply(this,arguments);c--;)e=a[c].call(this,e);return e}}function E(){P&&(P.apply(G),P=null)}function u(a){var b=t.PlayerState;T=a.data;
    switch(T){case b.ENDED:G.state!==e.IDLE&&(W=!0,G.sendEvent(f.JWPLAYER_MEDIA_BEFORECOMPLETE),G.setState(e.IDLE),W=!1,G.sendEvent(f.JWPLAYER_MEDIA_COMPLETE));break;case b.PLAYING:c.isFunction(x.unloadModule)&&x.unloadModule("captions");Z=!1;n();G.sendEvent(f.JWPLAYER_MEDIA_LEVELS,{levels:G.getQualityLevels(),currentQuality:G.getCurrentQuality()});G.setState(e.PLAYING);break;case b.PAUSED:G.setState(e.PAUSED);break;case b.BUFFERING:G.setState(e.BUFFERING);break;case b.CUED:G.setState(e.IDLE)}}function p(){T!==
t.PlayerState.ENDED&&G.play();G.sendEvent(f.JWPLAYER_MEDIA_LEVEL_CHANGED,{currentQuality:G.getCurrentQuality(),levels:G.getQualityLevels()})}function z(){G.sendEvent(f.JWPLAYER_MEDIA_ERROR,{message:"Error loading YouTube: Video could not be played"})}function w(){b&&(G.setVisibility(!0),a.css("#"+g+" .jwcontrols",{display:"none"}))}function A(){clearInterval(S);if(x&&x.stopVideo)try{x.stopVideo(),x.clearVideo()}catch(a){}}function F(b){P=null;var c=a.youTubeID(b.sources[0].file);b.image||(b.image=
    "//i.ytimg.com/vi/"+c+"/0.jpg");G.setVisibility(!0);if(!t||!x)N=function(){if(!c)throw"invalid Youtube ID";if(M.parentNode){var b={height:"100%",width:"100%",videoId:c,playerVars:a.extend({html5:1,autoplay:0,controls:0,showinfo:0,rel:0,modestbranding:0,playsinline:1,origin:location.protocol+"//"+location.hostname},void 0),events:{onReady:E,onStateChange:u,onPlaybackQualityChange:p,onError:z}};G.setVisibility(!0);x=new t.Player(M,b);M=x.getIframe();N=null;w();L()}},s();else if(x.getPlayerState)if(x.getVideoData().video_id!==
    c){Z?(A(),x.cueVideoById(c)):x.loadVideoById(c);var e=x.getPlayerState(),f=t.PlayerState;(e===f.UNSTARTED||e===f.CUED)&&w()}else 0<x.getCurrentTime()&&x.seekTo(0),n();else e=function(){L();G.load(b)},P=P?y(e,P):e}function L(){x&&x.getVolume&&(G.sendEvent(f.JWPLAYER_MEDIA_VOLUME,{volume:Math.round(x.getVolume())}),G.sendEvent(f.JWPLAYER_MEDIA_MUTE,{mute:x.isMuted()}))}this.state=e.IDLE;var G=a.extend(this,new d.events.eventdispatcher("provider."+this.name)),t=window.YT,x=null,M=document.createElement("div"),
    J,H=-1,K=!1,N=null,P=null,S=-1,T=-1,D,W=!1,Z=b;this.setState=function(b){clearInterval(S);b!==e.IDLE&&(S=setInterval(v,250),b===e.PLAYING?a.css("#"+g+" .jwcontrols",{display:""}):b===e.BUFFERING&&r());h.setState.apply(this,arguments)};!t&&j&&(j.addEventListener(f.COMPLETE,l),j.addEventListener(f.ERROR,m),j.load());M.id=g+"_youtube";this.init=function(a){F(a)};this.destroy=function(){this.remove();J=M=t=G=null};this.load=function(a){this.setState(e.BUFFERING);F(a);G.play()};this.stop=function(){A();
    this.setState(e.IDLE)};this.play=function(){Z||(x&&x.playVideo?x.playVideo():P=P?y(this.play,P):this.play)};this.pause=function(){Z||x.pauseVideo&&x.pauseVideo()};this.seek=function(a){Z||x.seekTo&&x.seekTo(a)};this.volume=function(b){x&&x.getVolume&&a.exists(b)&&(D=Math.min(Math.max(0,b),100),x.setVolume(D))};this.mute=function(b){x&&x.getVolume&&(a.exists(b)||(b=!x.isMuted()),b?(D=x.getVolume(),x.mute()):(this.volume(D),x.unMute()))};this.detachMedia=function(){return document.createElement("video")};
    this.attachMedia=function(){W&&(this.setState(e.IDLE),this.sendEvent(f.JWPLAYER_MEDIA_COMPLETE),W=!1)};this.setContainer=function(a){J=a;a.appendChild(M);this.setVisibility(!0)};this.getContainer=function(){return J};this.supportsFullscreen=function(){return!(!J||!J.requestFullscreen&&!J.requestFullScreen&&!J.webkitRequestFullscreen&&!J.webkitRequestFullScreen&&!J.webkitEnterFullscreen&&!J.webkitEnterFullScreen&&!J.mozRequestFullScreen&&!J.msRequestFullscreen)};this.remove=function(){A();M&&(J&&J===
    M.parentNode)&&J.removeChild(M);N=P=x=null};this.setVisibility=function(c){c?(a.css.style(M,{display:"block"}),a.css.style(J,{visibility:"visible",opacity:1})):b||a.css.style(J,{opacity:0})};this.resize=function(b,c,e){return a.stretch(e,M,b,c,M.clientWidth,M.clientHeight)};this.checkComplete=function(){return W};this.getCurrentQuality=function(){if(x){if(x.getAvailableQualityLevels){var a=x.getPlaybackQuality();return x.getAvailableQualityLevels().indexOf(a)}return-1}};this.getQualityLevels=function(){if(x){if(!c.isFunction(x.getAvailableQualityLevels))return[];
        var a=x.getAvailableQualityLevels();return 2===a.length&&c.contains(a,"auto")?{label:c.without(a,"auto")}:c.map(a,function(a){return{label:a}}).reverse()}};this.setCurrentQuality=function(a){if(x&&x.getAvailableQualityLevels){var b=x.getAvailableQualityLevels();b.length&&x.setPlaybackQuality(b[b.length-a-1])}}}var a=d.utils,c=d._,f=d.events,e=f.state,h=d.html5.DefaultProvider,j=new a.scriptloader(window.location.protocol+"//www.youtube.com/iframe_api"),b=a.isMobile();window.onYouTubeIframeAPIReady=
    function(){j=null};var m=function(){};m.prototype=h;l.prototype=new m;l.supports=function(b){return a.isYouTube(b.file,b.type)};d.html5.YoutubeProvider=l})(jwplayer);
(function(d){var l=d.utils,a=l.css,c=d.events,f=80,e=30;d.html5.adskipbutton=function(h,j,b,d){function g(a){0>E||(a=b.replace(/xx/gi,Math.ceil(E-a)),s(a))}function k(a,b){if("number"===l.typeOf(z))E=z;else if("%"===z.slice(-1)){var c=parseFloat(z.slice(0,-1));b&&!isNaN(c)&&(E=b*c/100)}else"string"===l.typeOf(z)?E=l.seconds(z):isNaN(z)||(E=z)}function q(){u&&L.sendEvent(c.JWPLAYER_AD_SKIPPED)}function s(a){a=a||d;var b=y.getContext("2d");b.clearRect(0,0,f,e);C(b,0,0,f,e,5,!0,!1,!1);C(b,0,0,f,e,5,
    !1,!0,!1);b.fillStyle="#979797";b.globalAlpha=1;var c=y.height/2,g=y.width/2;b.textAlign="center";b.font="Bold 12px Sans-Serif";a===d&&(g-=w.width,b.drawImage(w,y.width-(y.width-b.measureText(d).width)/2-4,(e-w.height)/2));b.fillText(a,g,c+4)}function v(a){a=a||d;var b=y.getContext("2d");b.clearRect(0,0,f,e);C(b,0,0,f,e,5,!0,!1,!0);C(b,0,0,f,e,5,!1,!0,!0);b.fillStyle="#FFFFFF";b.globalAlpha=1;var c=y.height/2,g=y.width/2;b.textAlign="center";b.font="Bold 12px Sans-Serif";a===d&&(g-=w.width,b.drawImage(A,
    y.width-(y.width-b.measureText(d).width)/2-4,(e-w.height)/2));b.fillText(a,g,c+4)}function C(a,b,c,e,f,g,h,j,r){"undefined"===typeof j&&(j=!0);"undefined"===typeof g&&(g=5);a.beginPath();a.moveTo(b+g,c);a.lineTo(b+e-g,c);a.quadraticCurveTo(b+e,c,b+e,c+g);a.lineTo(b+e,c+f-g);a.quadraticCurveTo(b+e,c+f,b+e-g,c+f);a.lineTo(b+g,c+f);a.quadraticCurveTo(b,c+f,b,c+f-g);a.lineTo(b,c+g);a.quadraticCurveTo(b,c,b+g,c);a.closePath();j&&(a.strokeStyle="white",a.globalAlpha=r?1:0.25,a.stroke());h&&(a.fillStyle=
    "#000000",a.globalAlpha=0.5,a.fill())}function r(a,b){var c=document.createElement(a);b&&(c.className=b);return c}var n,y,E=-1,u=!1,p,z=0,w,A,F=!1,L=l.extend(this,new c.eventdispatcher);L.updateSkipTime=function(b,c){k(b,c);0<=E&&(a.style(n,{visibility:p?"visible":"hidden"}),0<E-b?(g(b),u&&(u=!1,n.style.cursor="default")):u||(u||(u=!0,n.style.cursor="pointer"),F?v():s()))};this.reset=function(a){u=!1;z=a;k(0,0);g(0)};L.show=function(){p=!0;0<E&&a.style(n,{visibility:"visible"})};L.hide=function(){p=
    !1;a.style(n,{visibility:"hidden"})};this.element=function(){return n};w=new Image;w.src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAkAAAAICAYAAAArzdW1AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA3NpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNS1jMDE0IDc5LjE1MTQ4MSwgMjAxMy8wMy8xMy0xMjowOToxNSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDo0ODkzMWI3Ny04YjE5LTQzYzMtOGM2Ni0wYzdkODNmZTllNDYiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6RDI0OTcxRkE0OEM2MTFFM0I4MTREM0ZBQTFCNDE3NTgiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6RDI0OTcxRjk0OEM2MTFFM0I4MTREM0ZBQTFCNDE3NTgiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIChNYWNpbnRvc2gpIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6NDA5ZGQxNDktNzdkMi00M2E3LWJjYWYtOTRjZmM2MWNkZDI0IiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOjQ4OTMxYjc3LThiMTktNDNjMy04YzY2LTBjN2Q4M2ZlOWU0NiIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PqAZXX0AAABYSURBVHjafI2BCcAwCAQ/kr3ScRwjW+g2SSezCi0kYHpwKLy8JCLDbWaGTM+MAFzuVNXhNiTQsh+PS9QhZ7o9JuFMeUVNwjsamDma4K+3oy1cqX/hxyPAAAQwNKV27g9PAAAAAElFTkSuQmCC";
    w.className="jwskipimage jwskipout";A=new Image;A.src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAkAAAAICAYAAAArzdW1AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA3NpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNS1jMDE0IDc5LjE1MTQ4MSwgMjAxMy8wMy8xMy0xMjowOToxNSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDo0ODkzMWI3Ny04YjE5LTQzYzMtOGM2Ni0wYzdkODNmZTllNDYiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6RDI0OTcxRkU0OEM2MTFFM0I4MTREM0ZBQTFCNDE3NTgiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6RDI0OTcxRkQ0OEM2MTFFM0I4MTREM0ZBQTFCNDE3NTgiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIChNYWNpbnRvc2gpIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6NDA5ZGQxNDktNzdkMi00M2E3LWJjYWYtOTRjZmM2MWNkZDI0IiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOjQ4OTMxYjc3LThiMTktNDNjMy04YzY2LTBjN2Q4M2ZlOWU0NiIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PvgIj/QAAABYSURBVHjadI6BCcAgDAS/0jmyih2tm2lHSRZJX6hQQ3w4FP49LKraSHV3ZLDzAuAi3cwaqUhSfvft+EweznHneUdTzPGRmp5hEJFhAo3LaCnjn7blzCvAAH9YOSCL5RZKAAAAAElFTkSuQmCC";
    A.className="jwskipimage jwskipover";n=r("div","jwskip");n.id=h+"_skipcontainer";y=r("canvas");n.appendChild(y);L.width=y.width=f;L.height=y.height=e;n.appendChild(A);n.appendChild(w);a.style(n,{visibility:"hidden",bottom:j});n.addEventListener("mouseover",function(){F=!0;u&&v()});n.addEventListener("mouseout",function(){F=!1;u&&s()});l.isMobile()?(new l.touch(n)).addEventListener(l.touchEvents.TAP,q):n.addEventListener("click",q)};a(".jwskip",{position:"absolute","float":"right",display:"inline-block",
    width:f,height:e,right:10});a(".jwskipimage",{position:"relative",display:"none"})})(window.jwplayer);
(function(d){var l=d.html5,a=d.utils,c=d.events,f=c.state,e=d.parsers,h=a.css,j=a.isAndroid(4,!0),b="playing";l.captions=function(h,g){function l(b){a.log("CAPTIONS("+b+")")}function q(a){(H=a.fullscreen)?(s(),setTimeout(s,500)):n(!0)}function s(){var a=z.offsetHeight,b=z.offsetWidth;0!==a&&0!==b&&F.resize(b,Math.round(0.94*a))}function v(b,c){a.ajax(b,function(a){var b=a.responseXML?a.responseXML.firstChild:null;x++;if(b){"xml"===e.localName(b)&&(b=b.nextSibling);for(;b.nodeType===b.COMMENT_NODE;)b=
    b.nextSibling}b=b&&"tt"===e.localName(b)?new d.parsers.dfxp:new d.parsers.srt;try{var f=b.parse(a.responseText);G<t.length&&(t[c].data=f);n(!1)}catch(g){l(g.message+": "+t[c].file)}x===t.length&&(0<M&&(E(M),M=-1),r())},C,!0)}function C(a){x++;l(a);x===t.length&&(0<M&&(E(M),M=-1),r())}function r(){for(var a=[],b=0;b<t.length;b++)a.push(t[b]);K.sendEvent(c.JWPLAYER_CAPTIONS_LOADED,{captionData:a})}function n(a){t.length?L===b&&0<J?(F.show(),H?q({fullscreen:!0}):(y(),a&&setTimeout(y,500))):F.hide():
    F.hide()}function y(){F.resize()}function E(a){0<a?(G=a-1,J=Math.floor(a),G>=t.length||(t[G].data?F.populate(t[G].data):x===t.length?(l("file not loaded: "+t[G].file),0!==J&&u(c.JWPLAYER_CAPTIONS_CHANGED,t,0),J=0):M=a,n(!1))):(J=0,n(!1))}function u(a,b,c){K.sendEvent(a,{type:a,tracks:b,track:c})}function p(){for(var a=[{label:"Off"}],b=0;b<t.length;b++)a.push({label:t[b].label});return a}var z,w={back:!0,color:"#FFFFFF",fontSize:15,fontFamily:"Arial,sans-serif",fontOpacity:100,backgroundColor:"#000",
    backgroundOpacity:100,edgeStyle:null,windowColor:"#FFFFFF",windowOpacity:0},A={fontStyle:"normal",fontWeight:"normal",textDecoration:"none"},F,L,G,t=[],x=0,M=-1,J=0,H=!1,K=new c.eventdispatcher;a.extend(this,K);this.element=function(){return z};this.getCaptionsList=function(){return p()};this.getCurrentCaptions=function(){return J};this.setCurrentCaptions=function(b){0<=b&&(J!==b&&b<=t.length)&&(E(b),b=p(),a.saveCookie("captionLabel",b[J].label),u(c.JWPLAYER_CAPTIONS_CHANGED,b,J))};z=document.createElement("div");
    z.id=h.id+"_caption";z.className="jwcaptions";h.jwAddEventListener(c.JWPLAYER_PLAYER_STATE,function(a){switch(a.newstate){case f.IDLE:L="idle";n(!1);break;case f.PLAYING:L=b,n(!1)}});h.jwAddEventListener(c.JWPLAYER_PLAYLIST_ITEM,function(){G=0;t=[];F.update(0);x=0;for(var b=h.jwGetPlaylist()[h.jwGetPlaylistIndex()].tracks,e=[],f=0,g="",r=0,g="",f=0;f<b.length;f++)g=b[f].kind.toLowerCase(),("captions"===g||"subtitles"===g)&&e.push(b[f]);J=0;if(!j){for(f=0;f<e.length;f++)if(g=e[f].file)e[f].label||
    (e[f].label=f.toString()),t.push(e[f]),v(t[f].file,f);for(f=0;f<t.length;f++)if(t[f]["default"]){r=f+1;break}b=a.getCookies();if(g=b.captionLabel){b=p();for(f=0;f<b.length;f++)if(g===b[f].label){r=f;break}}0<r&&E(r);n(!1);u(c.JWPLAYER_CAPTIONS_LIST,p(),J)}});h.jwAddEventListener(c.JWPLAYER_MEDIA_ERROR,l);h.jwAddEventListener(c.JWPLAYER_ERROR,l);h.jwAddEventListener(c.JWPLAYER_READY,function(){a.foreach(w,function(a,b){g&&(void 0!==g[a]?b=g[a]:void 0!==g[a.toLowerCase()]&&(b=g[a.toLowerCase()]));A[a]=
        b});F=new d.html5.captions.renderer(A,z);n(!1)});h.jwAddEventListener(c.JWPLAYER_MEDIA_TIME,function(a){F.update(a.position)});h.jwAddEventListener(c.JWPLAYER_FULLSCREEN,q);h.jwAddEventListener(c.JWPLAYER_RESIZE,function(){n(!1)})};h(".jwcaptions",{position:"absolute",cursor:"pointer",width:"100%",height:"100%",overflow:"hidden"})})(jwplayer);
(function(d){var l=d.utils,a=l.css.style;d.html5.captions.renderer=function(c,f){function e(b){b=b||"";C="hidden";a(g,{visibility:C});q.innerHTML=b;b.length&&(C="visible",setTimeout(h,16))}function h(){if("visible"===C){var b=g.clientWidth,e=Math.pow(b/400,0.6),f=c.fontSize*e;a(q,{maxWidth:b+"px",fontSize:Math.round(f)+"px",lineHeight:Math.round(1.4*f)+"px",padding:Math.round(1*e)+"px "+Math.round(8*e)+"px"});c.windowOpacity&&a(k,{padding:Math.round(5*e)+"px",borderRadius:Math.round(5*e)+"px"});a(g,
    {visibility:C})}}function j(){for(var a=-1,b=0;b<d.length;b++)if(d[b].begin<=v&&(b===d.length-1||d[b+1].begin>=v)){a=b;break}-1===a?e(""):a!==s&&(s=a,e(d[b].text))}function b(a,b,c){c=l.hexToRgba("#000000",c);"dropshadow"===a?b.textShadow="0 2px 1px "+c:"raised"===a?b.textShadow="0 0 5px "+c+", 0 1px 5px "+c+", 0 2px 5px "+c:"depressed"===a?b.textShadow="0 -2px 1px "+c:"uniform"===a&&(b.textShadow="-2px 0 1px "+c+",2px 0 1px "+c+",0 -2px 1px "+c+",0 2px 1px "+c+",-1px 1px 1px "+c+",1px 1px 1px "+
    c+",1px -1px 1px "+c+",1px 1px 1px "+c)}var d,g,k,q,s,v,C="visible",r=-1;this.hide=function(){clearInterval(r);a(g,{display:"none"})};this.populate=function(a){s=-1;d=a;j()};this.resize=function(){h()};this.show=function(){a(g,{display:"block"});h();clearInterval(r);r=setInterval(h,250)};this.update=function(a){v=a;d&&j()};var n=c.fontOpacity,y=c.windowOpacity,E=c.edgeStyle,u=c.backgroundColor,p={display:"inline-block"},z={color:l.hexToRgba(l.rgbHex(c.color),n),display:"inline-block",fontFamily:c.fontFamily,
    fontStyle:c.fontStyle,fontWeight:c.fontWeight,textAlign:"center",textDecoration:c.textDecoration,wordWrap:"break-word"};y&&(p.backgroundColor=l.hexToRgba(l.rgbHex(c.windowColor),y));b(E,z,n);c.back?z.backgroundColor=l.hexToRgba(l.rgbHex(u),c.backgroundOpacity):null===E&&b("uniform",z);g=document.createElement("div");k=document.createElement("div");q=document.createElement("span");a(g,{display:"block",height:"auto",position:"absolute",bottom:"20px",textAlign:"center",width:"100%"});a(k,p);a(q,z);k.appendChild(q);
    g.appendChild(k);f.appendChild(g)}})(jwplayer);
(function(d,l,a){function c(a){return a?parseInt(a.width,10)+"px "+parseInt(a.height,10)+"px":"0 0"}var f=d.jwplayer,e=f.html5,h=f.utils,j=f._,b=f.events,m=b.state,g=h.css,k=h.transitionStyle,q=h.isMobile(),s=h.isAndroid(4,!0),v=d.top!==d.self,C="button",r="text",n="slider",y={display:"none"},E={display:"block"},u={display:""};e.controlbar=function(p,k){function w(a,b,c){return{name:a,type:b,className:c}}function A(a){g.block(aa);var b=a.duration===Number.POSITIVE_INFINITY,c=0===a.duration&&0!==a.position&&
    h.isSafari()&&!q;b||c?(X.setText(p.jwGetPlaylist()[p.jwGetPlaylistIndex()].title||"Live broadcast"),D(!1)):(B.elapsed&&(b=h.timeFormat(a.position),B.elapsed.innerHTML=b),B.duration&&(b=h.timeFormat(a.duration),B.duration.innerHTML=b),0<a.duration?Fa(a.position/a.duration):Fa(0),sa=a.duration,ma||X.setText())}function F(){var a=p.jwGetMute();Oa=p.jwGetVolume()/100;Z("mute",a||0===Oa);Ha(a?0:Oa)}function L(){g.style([B.hd,B.cc],y);ga();wa()}function G(a){Pa=Math.floor(a.currentQuality);B.hd&&(B.hd.querySelector("button").className=
    2===ia.length&&0===Pa?"off":"");oa&&0<=Pa&&oa.setActive(a.currentQuality)}function t(a){fa&&(Ba=Math.floor(a.track),B.cc&&(B.cc.querySelector("button").className=2===fa.length&&0===Ba?"off":""),pa&&0<=Ba&&pa.setActive(a.track))}function x(a){B.cast&&(h.canCast()?h.addClass(B.cast,"jwcancast"):h.removeClass(B.cast,"jwcancast"));M(a||Qa)}function M(a){Qa=a;Z("cast",a.active);wa()}function J(){$=h.extend({},ca,V.getComponentSettings("controlbar"),k);Ia=U("background").height;var a=ta?0:$.margin;g.style(R,
    {height:Ia,bottom:a,left:a,right:a,"max-width":ta?"":$.maxwidth});g(H(".jwtext"),{font:$.fontsize+"px/"+U("background").height+"px "+$.font,color:$.fontcolor,"font-weight":$.fontweight});g(H(".jwoverlay"),{bottom:Ia})}function H(a){return"#"+aa+(a?" "+a:"")}function K(){return l.createElement("span")}function N(a,b,e,f,j){var r=K(),d=U(a);f=f?" left center":" center";var l=c(d);r.className="jw"+a;r.innerHTML="\x26nbsp;";if(d&&d.src)return e=e?{background:'url("'+d.src+'") repeat-x '+f,"background-size":l,
    height:j?d.height:""}:{background:'url("'+d.src+'") no-repeat'+f,"background-size":l,width:d.width,height:j?d.height:""},r.skin=d,g(H((j?".jwvertical ":"")+".jw"+a),h.extend(e,b)),B[a]=r}function P(a,b,e,f){b&&b.src&&(g(a,{width:b.width,background:"url("+b.src+") no-repeat center","background-size":c(b)}),e.src&&!q&&g(a+":hover,"+a+".off:hover",{background:"url("+e.src+") no-repeat center","background-size":c(e)}),f&&f.src&&g(a+".off",{background:"url("+f.src+") no-repeat center","background-size":c(f)}))}
    function S(a){return function(c){rb[a]&&(rb[a](),q&&X.sendEvent(b.JWPLAYER_USER_ACTION));c.preventDefault&&c.preventDefault()}}function T(b){h.foreach(ib,function(c,e){c!==b&&("cc"===c&&(clearTimeout(Ja),Ja=a),"hd"===c&&(clearTimeout(na),na=a),e.hide())})}function D(b){R&&B.alt&&(b===a&&(b=R.parentNode&&320<=R.parentNode.clientWidth),b&&!ma?g.style(Wa,u):g.style(Wa,y))}function W(){!ta&&!ma&&(g.block(aa),qa.show(),Ka("volume",qa),T("volume"))}function Z(a,b){j.isBoolean(b)||(b=!eb[a]);B[a]&&(b?h.addClass(B[a],
        "jwtoggle"):h.removeClass(B[a],"jwtoggle"),h.addClass(B[a],"jwtoggling"),setTimeout(function(){h.removeClass(B[a],"jwtoggling")},100));eb[a]=b}function Y(){2<ia.length&&(ha&&(clearTimeout(ha),ha=a),g.block(aa),oa.show(),Ka("hd",oa),T("hd"))}function da(){fa&&2<fa.length&&(jb&&(clearTimeout(jb),jb=a),g.block(aa),pa.show(),Ka("cc",pa),T("cc"))}function ea(b){0<=b&&b<ia.length&&(p.jwSetCurrentQuality(b),clearTimeout(na),na=a,oa.hide())}function va(b){0<=b&&b<fa.length&&(p.jwSetCurrentCaptions(b),clearTimeout(Ja),
        Ja=a,pa.hide())}function ra(){2===fa.length&&va((Ba+1)%2)}function I(){2===ia.length&&ea((Pa+1)%2)}function Va(a){a.preventDefault();l.onselectstart=function(){return!1}}function La(a){Aa();Ca=a;d.addEventListener("mouseup",Da,!1);d.addEventListener("mousemove",Da,!1)}function Aa(){d.removeEventListener("mouseup",Da);d.removeEventListener("mousemove",Da);Ca=null}function Ma(){B.timeRail.className="jwrail";p.jwGetState()!==m.IDLE&&(p.jwSeekDrag(!0),La("time"),kb(),X.sendEvent(b.JWPLAYER_USER_ACTION))}
    function Na(a){if(Ca){var c=B[Ca].querySelector(".jwrail"),c=h.bounds(c),c=a.x/c.width;100<c&&(c=100);a.type===h.touchEvents.DRAG_END?(p.jwSeekDrag(!1),B.timeRail.className="jwrail",Aa(),fb.time(c),Xa()):(Fa(c),a=(new Date).getTime(),500<a-lb&&(lb=a,fb.time(c)));X.sendEvent(b.JWPLAYER_USER_ACTION)}}function ob(a){var c=B.time.querySelector(".jwrail"),c=h.bounds(c);a=a.x/c.width;100<a&&(a=100);p.jwGetState()!==m.IDLE&&(fb.time(a),X.sendEvent(b.JWPLAYER_USER_ACTION))}function pb(a){return function(b){b.button||
    (B[a+"Rail"].className="jwrail","time"===a?p.jwGetState()!==m.IDLE&&(p.jwSeekDrag(!0),La(a)):La(a))}}function Da(a){if(Ca&&!a.button){var b=B[Ca].querySelector(".jwrail"),c=h.bounds(b),b=Ca,c=xa()?B[b].vertical?(100*c.bottom-a.pageY)/(100*c.height):(a.pageX-100*c.left)/(100*c.width):B[b].vertical?(c.bottom-a.pageY)/c.height:(a.pageX-c.left)/c.width;"mouseup"===a.type?("time"===b&&p.jwSeekDrag(!1),B[b+"Rail"].className="jwrail",Aa(),fb[b.replace("H","")](c)):("time"===Ca?Fa(c):Ha(c),a=(new Date).getTime(),
    500<a-lb&&(lb=a,fb[Ca.replace("H","")](c)));return!1}}function kb(a){a&&ab.apply(this,arguments);ka&&(sa&&!ta&&!q)&&(g.block(aa),ka.show(),Ka("time",ka))}function Xa(){ka&&ka.hide()}function ab(a){ya=h.bounds(R);if((ja=h.bounds(Ya))&&0!==ja.width){var b;xa()?(a=a.pageX?a.pageX-100*ja.left:a.x,b=100*ja.width):(a=a.pageX?a.pageX-ja.left:a.x,b=ja.width);ka.positionX(Math.round(a));qb(sa*a/b)}}function Za(){h.foreach(gb,function(a,b){var c={};"%"===b.position.toString().slice(-1)?c.left=b.position:0<
    sa?(c.left=(100*b.position/sa).toFixed(2)+"%",c.display=null):(c.left=0,c.display="none");g.style(b.element,c)})}function Q(){jb=setTimeout(pa.hide,500)}function Ga(){ha=setTimeout(oa.hide,500)}function la(a,b,c,e){if(!q){var f=a.element();b.appendChild(f);b.addEventListener("mousemove",c,!1);e?b.addEventListener("mouseout",e,!1):b.addEventListener("mouseout",a.hide,!1);g.style(f,{left:"50%"})}}function hb(c,e,f,g){if(q){var j=c.element();e.appendChild(j);(new h.touch(e)).addEventListener(h.touchEvents.TAP,
        function(){var e=f;"cc"===g?(2===fa.length&&(e=ra),Ja?(clearTimeout(Ja),Ja=a,c.hide()):(Ja=setTimeout(function(){c.hide();Ja=a},4E3),e()),X.sendEvent(b.JWPLAYER_USER_ACTION)):"hd"===g&&(2===ia.length&&(e=I),na?(clearTimeout(na),na=a,c.hide()):(na=setTimeout(function(){c.hide();na=a},4E3),e()),X.sendEvent(b.JWPLAYER_USER_ACTION))})}}function Ra(a){var b=K();b.className="jwgroup jw"+a;Ea[a]=b;if(Sa[a]){var b=Sa[a],f=Ea[a];if(b&&0<b.elements.length)for(var j=0;j<b.elements.length;j++){var d;a:{d=b.elements[j];
        var k=a;switch(d.type){case r:k=void 0;d=d.name;var k={},p=U(("alt"===d?"elapsed":d)+"Background");if(p.src){var t=K();t.id=aa+"_"+d;"elapsed"===d||"duration"===d?(t.className="jwtext jw"+d+" jwhidden",Wa.push(t)):t.className="jwtext jw"+d;k.background="url("+p.src+") repeat-x center";k["background-size"]=c(U("background"));g.style(t,k);t.innerHTML="alt"!==d?"00:00":"";k=B[d]=t}else k=null;d=k;break a;case C:if("blank"!==d.name){d=d.name;p=k;if(!U(d+"Button").src||q&&("mute"===d||0===d.indexOf("volume"))||
            s&&/hd|cc/.test(d))d=null;else{var k=K(),t=K(),v=void 0,v=ba,m=N(v.name);m||(m=K(),m.className="jwblankDivider");v.className&&(m.className+=" "+v.className);v=m;m=l.createElement("button");k.className="jw"+d;"left"===p?(k.appendChild(t),k.appendChild(v)):(k.appendChild(v),k.appendChild(t));q?"hd"!==d&&"cc"!==d&&(new h.touch(m)).addEventListener(h.touchEvents.TAP,S(d)):m.addEventListener("click",S(d),!1);m.innerHTML="\x26nbsp;";m.tabIndex=-1;m.setAttribute("type","button");t.appendChild(m);p=U(d+"Button");
            t=U(d+"ButtonOver");v=U(d+"ButtonOff");P(H(".jw"+d+" button"),p,t,v);(p=xb[d])&&P(H(".jw"+d+".jwtoggle button"),U(p+"Button"),U(p+"ButtonOver"));eb[d]?h.addClass(k,"jwtoggle"):h.removeClass(k,"jwtoggle");d=B[d]=k}break a}break;case n:k=void 0;v=d.name;if(q&&0===v.indexOf("volume"))k=void 0;else{d=K();var t="volume"===v,L=v+("time"===v?"Slider":"")+"Cap",p=t?"Top":"Left",k=t?"Bottom":"Right",m=N(L+p,null,!1,!1,t),w=N(L+k,null,!1,!1,t),ha;ha=v;var u=t,x=p,G=k,z=K(),Y=["Rail","Buffer","Progress"],E=
            void 0,W=void 0;z.className="jwrail";for(var A=0;A<Y.length;A++){var W="time"===ha?"Slider":"",M=ha+W+Y[A],D=N(M,null,!u,0===ha.indexOf("volume"),u),J=N(M+"Cap"+x,null,!1,!1,u),F=N(M+"Cap"+G,null,!1,!1,u),na=U(M+"Cap"+x),I=U(M+"Cap"+G);if(D){var T=K();T.className="jwrailgroup "+Y[A];J&&T.appendChild(J);T.appendChild(D);F&&(T.appendChild(F),F.className+=" jwcap"+(u?"Bottom":"Right"));g(H(".jwrailgroup."+Y[A]),{"min-width":u?"":na.width+I.width});T.capSize=u?na.height+I.height:na.width+I.width;g(H("."+
            D.className),{left:u?"":na.width,right:u?"":I.width,top:u?na.height:"",bottom:u?I.height:"",height:u?"auto":""});2===A&&(E=T);2===A&&!u?(D=K(),D.className="jwprogressOverflow",D.appendChild(T),B[M]=D,z.appendChild(D)):(B[M]=T,z.appendChild(T))}}if(x=N(ha+W+"Thumb",null,!1,!1,u))g(H("."+x.className),{opacity:"time"===ha?0:1,"margin-top":u?x.skin.height/-2:""}),x.className+=" jwthumb",(u&&E?E:z).appendChild(x);q?(u=new h.touch(z),u.addEventListener(h.touchEvents.DRAG_START,Ma),u.addEventListener(h.touchEvents.DRAG,
            Na),u.addEventListener(h.touchEvents.DRAG_END,Na),u.addEventListener(h.touchEvents.TAP,ob)):(E=ha,"volume"===E&&!u&&(E+="H"),z.addEventListener("mousedown",pb(E),!1));"time"===ha&&!q&&(z.addEventListener("mousemove",kb,!1),z.addEventListener("mouseout",Xa,!1));ha=B[ha+"Rail"]=z;z=U(L+p);L=U(L+p);d.className="jwslider jw"+v;m&&d.appendChild(m);d.appendChild(ha);w&&(t&&(w.className+=" jwcapBottom"),d.appendChild(w));g(H(".jw"+v+" .jwrail"),{left:t?"":z.width,right:t?"":L.width,top:t?z.height:"",bottom:t?
            L.height:"",width:t?"100%":"",height:t?"auto":""});B[v]=d;d.vertical=t;"time"===v?(ka=new e.overlay(aa+"_timetooltip",V),Ta=new e.thumbs(aa+"_thumb"),Ua=l.createElement("div"),Ua.className="jwoverlaytext",bb=l.createElement("div"),k=Ta.element(),bb.appendChild(k),bb.appendChild(Ua),ka.setContents(bb),Ya=ha,qb(0),k=ka.element(),ha.appendChild(k),B.timeSliderRail||g.style(B.time,y),B.timeSliderThumb&&g.style(B.timeSliderThumb,{"margin-left":U("timeSliderThumb").width/-2}),k=U("timeSliderCue"),p={"z-index":1},
            k&&k.src?(N("timeSliderCue"),p["margin-left"]=k.width/-2):p.display="none",g(H(".jwtimeSliderCue"),p),$a(0),Fa(0),Fa(0),$a(0)):0===v.indexOf("volume")&&(v=d,m="volume"+(t?"":"H"),w=t?"vertical":"horizontal",g(H(".jw"+m+".jw"+w),{width:U(m+"Rail",t).width+(t?0:U(m+"Cap"+p).width+U(m+"RailCap"+p).width+U(m+"RailCap"+k).width+U(m+"Cap"+k).width),height:t?U(m+"Cap"+p).height+U(m+"Rail").height+U(m+"RailCap"+p).height+U(m+"RailCap"+k).height+U(m+"Cap"+k).height:""}),v.className+=" jw"+w);k=d}d=k;break a}d=
            void 0}d&&("volume"===b.elements[j].name&&d.vertical?(qa=new e.overlay(aa+"_volumeOverlay",V),qa.setContents(d)):f.appendChild(d))}}}function xa(){return v&&h.isIE()&&p.jwGetFullscreen()}function wa(){clearTimeout(sb);sb=setTimeout(X.redraw,0)}function ga(){!mb&&1<p.jwGetPlaylist().length&&(!l.querySelector("#"+p.id+" .jwplaylist")||p.jwGetFullscreen())?(g.style(B.next,u),g.style(B.prev,u)):(g.style(B.next,y),g.style(B.prev,y))}function Ka(a,b){ya||(ya=h.bounds(R));b.constrainX(ya,!0)}function $a(a){B.timeSliderBuffer&&
    (a=Math.min(Math.max(0,a),1),g.style(B.timeSliderBuffer,{width:(100*a).toFixed(1)+"%",opacity:0<a?1:0}))}function za(a,b){if(B[a]){var c=B[a].vertical,e=a+("time"===a?"Slider":""),f=100*Math.min(Math.max(0,b),1)+"%",d=B[e+"Progress"],e=B[e+"Thumb"],h;d&&(h={},c?(h.height=f,h.bottom=0):h.width=f,"volume"!==a&&(h.opacity=0<b||Ca?1:0),g.style(d,h));e&&(h={},c?h.top=0:h.left=f,g.style(e,h))}}function Ha(a){za("volume",a);za("volumeH",a)}function Fa(a){za("time",a)}function U(b){var c="controlbar",e=b;
        0===b.indexOf("volume")&&(0===b.indexOf("volumeH")?e=b.replace("volumeH","volume"):c="tooltip");return(b=V.getSkinElement(c,e))?b:{width:0,height:0,src:"",image:a,ready:!1}}function ua(a){a=(new f.parsers.srt).parse(a.responseText,!0);if(!j.isArray(a))return O("Invalid data");X.addCues(a)}function O(a){h.log("Cues failed to load: "+a)}k=k||{};var V,ba=w("divider","divider"),ca={margin:8,maxwidth:800,font:"Arial,sans-serif",fontsize:11,fontcolor:15658734,fontweight:"bold",layout:{left:{position:"left",
        elements:[w("play",C),w("prev",C),w("next",C),w("elapsed",r)]},center:{position:"center",elements:[w("time",n),w("alt",r)]},right:{position:"right",elements:[w("duration",r),w("hd",C),w("cc",C),w("mute",C),w("volume",n),w("volumeH",n),w("cast",C),w("fullscreen",C)]}}},$,Sa,B,Ia,R,aa,sa,ia=[],Pa,fa,Ba,Oa,Qa={},qa,ya,Ya,ja,ka,bb,Ta,Ua,ha,na,oa,jb,Ja,pa,sb,cb=-1,ta=!1,ma=!1,mb=!1,nb=!1,Ca=null,lb=0,gb=[],db,xb={play:"pause",mute:"unmute",cast:"casting",fullscreen:"normalscreen"},eb={play:!1,mute:!1,
        cast:!1,fullscreen:k.fullscreen||!1},rb={play:function(){eb.play?p.jwPause():p.jwPlay()},mute:function(){var a=!eb.mute;p.jwSetMute(a);!a&&0===Oa&&p.jwSetVolume(20);F()},fullscreen:function(){p.jwSetFullscreen()},next:function(){p.jwPlaylistNext()},prev:function(){p.jwPlaylistPrev()},hd:I,cc:ra,cast:function(){Qa.active?p.jwOpenExtension():p.jwStartCasting()}},fb={time:function(a){db?(a=db.position,a="%"===a.toString().slice(-1)?sa*parseFloat(a.slice(0,-1))/100:parseFloat(a)):a*=sa;p.jwSeek(a)},volume:function(a){Ha(a);
        0.1>a&&(a=0);0.9<a&&(a=1);p.jwSetVolume(100*a)}},ib={},Wa=[],X=h.extend(this,new b.eventdispatcher),qb,tb,yb=function(a){g.style(ka.element(),{width:a});Ka("time",ka)};qb=function(a){var b=Ta.updateTimeline(a,yb);if(db){if((a=db.text)&&a!==tb)tb=a,g.style(ka.element(),{width:32<a.length?160:""})}else a=h.timeFormat(a),b||g.style(ka.element(),{width:""});Ua.innerHTML!==a&&(Ua.innerHTML=a);Ka("time",ka)};X.setText=function(a){g.block(aa);var b=B.alt,c=B.time;B.timeSliderRail?g.style(c,a?y:E):g.style(c,
        y);b&&(g.style(b,a?E:y),b.innerHTML=a||"");wa()};var Ea={};X.redraw=function(a){g.block(aa);a&&X.visible&&X.show(!0);J();var b=v&&h.isMSIE();a=Qa.active;g.style(B.fullscreen,{display:ta||a||nb||b?"none":""});g.style(B.volumeH,{display:ta||ma?"block":"none"});(b=Math.floor($.maxwidth))&&R.parentNode&&h.isIE()&&(!ta&&R.parentNode.clientWidth>b+2*Math.floor($.margin)?g.style(R,{width:b}):g.style(R,{width:""}));qa&&g.style(qa.element(),{display:!ta&&!ma?"block":"none"});g.style(B.hd,{display:!ta&&!a&&
    !ma&&1<ia.length&&oa?"":"none"});g.style(B.cc,{display:!ta&&!ma&&fa&&1<fa.length&&pa?"":"none"});Za();g.unblock(aa);X.visible&&(a=U("capLeft"),b=U("capRight"),a=xa()?{left:Math.round(h.parseDimension(62*Ea.left.offsetWidth)+a.width),right:Math.round(h.parseDimension(86*Ea.right.offsetWidth)+b.width)}:{left:Math.round(h.parseDimension(Ea.left.offsetWidth)+a.width),right:Math.round(h.parseDimension(Ea.right.offsetWidth)+b.width)},g.style(Ea.center,a))};X.audioMode=function(b){b!==a&&b!==ta&&(ta=!!b,
        wa());return ta};X.instreamMode=function(b){b!==a&&b!==ma&&(ma=!!b,g.style(B.cast,ma?y:u));return ma};X.adMode=function(a){if(j.isBoolean(a)&&a!==mb){if(mb=a){var b=Wa,c=j.indexOf(b,B.elapsed);-1<c&&b.splice(c,1);b=Wa;c=j.indexOf(b,B.duration);-1<c&&b.splice(c,1)}else b=Wa,c=B.elapsed,-1===j.indexOf(b,c)&&b.push(c),b=Wa,c=B.duration,-1===j.indexOf(b,c)&&b.push(c);g.style([B.cast,B.elapsed,B.duration],a?y:u);ga()}return mb};X.hideFullscreen=function(b){b!==a&&b!==nb&&(nb=!!b,wa());return nb};X.element=
        function(){return R};X.margin=function(){return parseInt($.margin,10)};X.height=function(){return Ia};X.show=function(a){if(!X.visible||a)X.visible=!0,g.style(R,{display:"inline-block"}),ya=h.bounds(R),D(),g.block(aa),F(),wa(),clearTimeout(cb),cb=-1,cb=setTimeout(function(){g.style(R,{opacity:1})},0)};X.showTemp=function(){this.visible||(R.style.opacity=0,R.style.display="inline-block")};X.hideTemp=function(){this.visible||(R.style.display="none")};X.addCues=function(a){h.foreach(a,function(a,b){if(b.text){var c=
        b.begin,e=b.text;if(/^[\d\.]+%?$/.test(c.toString())){var f=N("timeSliderCue"),d=B.timeSliderRail,g={position:c,text:e,element:f};f&&d&&(d.appendChild(f),f.addEventListener("mouseover",function(){db=g},!1),f.addEventListener("mouseout",function(){db=null},!1),gb.push(g))}Za()}})};X.hide=function(){if(X.visible&&(!ma||!q||!p.jwGetControls()))X.visible=!1,g.style(R,{opacity:0}),clearTimeout(cb),cb=-1,cb=setTimeout(function(){g.style(R,{display:"none"})},250)};B={};aa=p.id+"_controlbar";sa=0;R=K();R.id=
        aa;R.className="jwcontrolbar";V=p.skin;Sa=V.getComponentLayout("controlbar");Sa||(Sa=ca.layout);h.clearCss(H());g.block(aa+"build");J();var ub=N("capLeft"),vb=N("capRight"),wb=N("background",{position:"absolute",left:U("capLeft").width,right:U("capRight").width,"background-repeat":"repeat-x"},!0);wb&&R.appendChild(wb);ub&&R.appendChild(ub);Ra("left");Ra("center");Ra("right");R.appendChild(Ea.left);R.appendChild(Ea.center);R.appendChild(Ea.right);B.hd&&(oa=new e.menu("hd",aa+"_hd",V,ea),q?hb(oa,B.hd,
        Y,"hd"):la(oa,B.hd,Y,Ga),ib.hd=oa);B.cc&&(pa=new e.menu("cc",aa+"_cc",V,va),q?hb(pa,B.cc,da,"cc"):la(pa,B.cc,da,Q),ib.cc=pa);B.mute&&(B.volume&&B.volume.vertical)&&(qa=new e.overlay(aa+"_volumeoverlay",V),qa.setContents(B.volume),la(qa,B.mute,W),ib.volume=qa);g.style(Ea.right,{right:U("capRight").width});vb&&R.appendChild(vb);g.unblock(aa+"build");p.jwAddEventListener(b.JWPLAYER_MEDIA_TIME,A);p.jwAddEventListener(b.JWPLAYER_PLAYER_STATE,function(a){switch(a.newstate){case m.BUFFERING:case m.PLAYING:B.timeSliderThumb&&
    g.style(B.timeSliderThumb,{opacity:1});Z("play",!0);break;case m.PAUSED:Ca||Z("play",!1);break;case m.IDLE:Z("play",!1),B.timeSliderThumb&&g.style(B.timeSliderThumb,{opacity:0}),B.timeRail&&(B.timeRail.className="jwrail"),$a(0),A({position:0,duration:0})}});p.jwAddEventListener(b.JWPLAYER_PLAYLIST_ITEM,function(a){if(!ma){a=p.jwGetPlaylist()[a.index].tracks;var b=!1,c=B.timeSliderRail;h.foreach(gb,function(a,b){c.removeChild(b.element)});gb.length=0;if(j.isArray(a)&&!q)for(var e=0;e<a.length;e++)if(!b&&
        (a[e].file&&a[e].kind&&"thumbnails"===a[e].kind.toLowerCase())&&(Ta.load(a[e].file),b=!0),a[e].file&&a[e].kind&&"chapters"===a[e].kind.toLowerCase()){var f=a[e].file;f?h.ajax(f,ua,O,!0):gb.length=0}b||Ta.load()}});p.jwAddEventListener(b.JWPLAYER_MEDIA_MUTE,F);p.jwAddEventListener(b.JWPLAYER_MEDIA_VOLUME,F);p.jwAddEventListener(b.JWPLAYER_MEDIA_BUFFER,function(a){$a(a.bufferPercent/100)});p.jwAddEventListener(b.JWPLAYER_FULLSCREEN,function(a){Z("fullscreen",a.fullscreen);ga();X.visible&&X.show(!0)});
    p.jwAddEventListener(b.JWPLAYER_PLAYLIST_LOADED,L);p.jwAddEventListener(b.JWPLAYER_MEDIA_LEVELS,function(a){ia=a.levels||[];if(!ma&&1<ia.length&&oa){g.style(B.hd,u);oa.clearOptions();for(var b=0;b<ia.length;b++)oa.addOption(ia[b].label,b);G(a)}else g.style(B.hd,y);wa()});p.jwAddEventListener(b.JWPLAYER_MEDIA_LEVEL_CHANGED,G);p.jwAddEventListener(b.JWPLAYER_CAPTIONS_LIST,function(a){fa=a.tracks;if(!ma&&fa&&1<fa.length&&pa){g.style(B.cc,u);pa.clearOptions();for(var b=0;b<fa.length;b++)pa.addOption(fa[b].label,
        b);t(a)}else g.style(B.cc,y);wa()});p.jwAddEventListener(b.JWPLAYER_CAPTIONS_CHANGED,t);p.jwAddEventListener(b.JWPLAYER_RESIZE,function(){ya=h.bounds(R);0<ya.width&&X.show(!0)});p.jwAddEventListener(b.JWPLAYER_CAST_AVAILABLE,x);p.jwAddEventListener(b.JWPLAYER_CAST_SESSION,M);q||(R.addEventListener("mouseover",function(){d.addEventListener("mousedown",Va,!1)},!1),R.addEventListener("mouseout",function(){d.removeEventListener("mousedown",Va);l.onselectstart=null},!1));setTimeout(F,0);L();X.visible=
        !1;x()};g("span.jwcontrolbar",{position:"absolute",margin:"auto",opacity:0,display:"none"});g("span.jwcontrolbar span",{height:"100%"});h.dragStyle("span.jwcontrolbar span","none");g("span.jwcontrolbar .jwgroup",{display:"inline"});g("span.jwcontrolbar span, span.jwcontrolbar .jwgroup button,span.jwcontrolbar .jwleft",{position:"relative","float":"left"});g("span.jwcontrolbar .jwright",{position:"relative","float":"right"});g("span.jwcontrolbar .jwcenter",{position:"absolute"});g("span.jwcontrolbar button",
    {display:"inline-block",height:"100%",border:"none",cursor:"pointer"});g("span.jwcontrolbar .jwcapRight,span.jwcontrolbar .jwtimeSliderCapRight,span.jwcontrolbar .jwvolumeCapRight",{right:0,position:"absolute"});g("span.jwcontrolbar .jwcapBottom",{bottom:0,position:"absolute"});g("span.jwcontrolbar .jwtime",{position:"absolute",height:"100%",width:"100%",left:0});g("span.jwcontrolbar .jwthumb",{position:"absolute",height:"100%",cursor:"pointer"});g("span.jwcontrolbar .jwrail",{position:"absolute",
    cursor:"pointer"});g("span.jwcontrolbar .jwrailgroup",{position:"absolute",width:"100%"});g("span.jwcontrolbar .jwrailgroup span",{position:"absolute"});g("span.jwcontrolbar .jwdivider+.jwdivider",{display:"none"});g("span.jwcontrolbar .jwtext",{padding:"0 5px","text-align":"center"});g("span.jwcontrolbar .jwcast",{display:"none"});g("span.jwcontrolbar .jwcast.jwcancast",{display:"block"});g("span.jwcontrolbar .jwalt",{display:"none",overflow:"hidden"});g("span.jwcontrolbar .jwalt",{position:"absolute",
    left:0,right:0,"text-align":"left"},!0);g("span.jwcontrolbar .jwoverlaytext",{padding:3,"text-align":"center"});g("span.jwcontrolbar .jwvertical *",{display:"block"});g("span.jwcontrolbar .jwvertical .jwvolumeProgress",{height:"auto"},!0);g("span.jwcontrolbar .jwprogressOverflow",{position:"absolute",overflow:"hidden"});k("span.jwcontrolbar","opacity .25s, background .25s, visibility .25s");k("span.jwcontrolbar button","opacity .25s, background .25s, visibility .25s");k("span.jwcontrolbar .jwtoggling",
    "none")})(window,document);
(function(d){var l=d.utils,a=d.events,c=a.state,f=d.playlist;d.html5.controller=function(e,h){function j(){return e.getVideo()}function b(a){F.sendEvent(a.type,a)}function m(c){k(!0);switch(l.typeOf(c)){case "string":var g=new f.loader;g.addEventListener(a.JWPLAYER_PLAYLIST_LOADED,function(a){m(a.playlist)});g.addEventListener(a.JWPLAYER_ERROR,function(a){m([]);a.message="Could not load playlist: "+a.message;b(a)});g.load(c);break;case "object":case "array":e.setPlaylist(new d.playlist(c));break;
    case "number":e.setItem(c)}}function g(b){if(l.exists(b)&&!b)return q();try{0<=E&&(m(E),E=-1);if(!u&&(u=!0,F.sendEvent(a.JWPLAYER_MEDIA_BEFOREPLAY),u=!1,w)){w=!1;p=null;return}if(e.state===c.IDLE){if(0===e.playlist.length)return!1;j().load(e.playlist[e.item])}else e.state===c.PAUSED&&j().play();return!0}catch(f){F.sendEvent(a.JWPLAYER_ERROR,f),p=null}return!1}function k(b){p=null;try{return j().stop(),b||(z=!0),u&&(w=!0),!0}catch(c){F.sendEvent(a.JWPLAYER_ERROR,c)}return!1}function q(b){p=null;if(l.exists(b)&&
    !b)return g();switch(e.state){case c.PLAYING:case c.BUFFERING:try{j().pause()}catch(f){return F.sendEvent(a.JWPLAYER_ERROR,f),!1}break;default:u&&(w=!0)}return!0}function s(a){l.css.block(e.id+"_next");m(a);g();l.css.unblock(e.id+"_next")}function v(){s(e.item+1)}function C(){e.state===c.IDLE&&(z?z=!1:(p=C,e.repeat?v():e.item===e.playlist.length-1?(E=0,k(!0),setTimeout(function(){F.sendEvent(a.JWPLAYER_PLAYLIST_COMPLETE)},0)):v()))}function r(a){return function(){var b=Array.prototype.slice.call(arguments,
    0);y?n(a,b):A.push({method:a,arguments:b})}}function n(a,b){a.apply(this,b)}var y=!1,E=-1,u=!1,p,z=!1,w,A=[],F=l.extend(this,new a.eventdispatcher(e.id,e.config.debug));this.play=r(g);this.pause=r(q);this.seek=r(function(a){e.state!==c.PLAYING&&g(!0);j().seek(a)});this.stop=function(){e.state===c.IDLE&&(z=!0);r(k)()};this.load=r(m);this.next=r(v);this.prev=r(function(){s(e.item-1)});this.item=r(s);this.setVolume=r(e.setVolume);this.setMute=r(e.setMute);this.setFullscreen=r(function(a){h.fullscreen(a)});
    this.detachMedia=function(){try{return e.getVideo().detachMedia()}catch(a){l.log("Error calling detachMedia",a)}return null};this.attachMedia=function(a){try{e.getVideo().attachMedia(a)}catch(b){l.log("Error calling detachMedia",b);return}"function"===typeof p&&p()};this.setCurrentQuality=r(function(a){j().setCurrentQuality(a)});this.getCurrentQuality=function(){return j()?j().getCurrentQuality():-1};this.getQualityLevels=function(){return j()?j().getQualityLevels():null};this.setCurrentAudioTrack=
        function(a){j().setCurrentAudioTrack(a)};this.getCurrentAudioTrack=function(){return j()?j().getCurrentAudioTrack():-1};this.getAudioTracks=function(){return j()?j().getAudioTracks():null};this.setCurrentCaptions=r(function(a){h.setCurrentCaptions(a)});this.getCurrentCaptions=function(){return h.getCurrentCaptions()};this.getCaptionsList=function(){return h.getCaptionsList()};this.checkBeforePlay=function(){return u};this.playerReady=function(a){if(!y){h.completeSetup();F.sendEvent(a.type,a);d.utils.exists(d.playerReady)&&
    d.playerReady(a);e.addGlobalListener(b);h.addGlobalListener(b);F.sendEvent(d.events.JWPLAYER_PLAYLIST_LOADED,{playlist:d(e.id).getPlaylist()});F.sendEvent(d.events.JWPLAYER_PLAYLIST_ITEM,{index:e.item});m();e.autostart&&!l.isMobile()&&g();for(y=!0;0<A.length;)a=A.shift(),n(a.method,a.arguments)}};e.addEventListener(a.JWPLAYER_MEDIA_BUFFER_FULL,function(){j().play()});e.addEventListener(a.JWPLAYER_MEDIA_COMPLETE,function(){setTimeout(C,25)});e.addEventListener(a.JWPLAYER_MEDIA_ERROR,function(b){b=
        l.extend({},b);b.type=a.JWPLAYER_ERROR;F.sendEvent(b.type,b)})}})(jwplayer);(function(d){var l;d.html5.defaultskin=function(){return l=l||d.utils.parseXML('\x3c?xml version\x3d"1.0" ?\x3e\x3cskin author\x3d"JW Player" name\x3d"Six" target\x3d"6.7" version\x3d"3.0"\x3e\x3ccomponents\x3e\x3ccomponent name\x3d"controlbar"\x3e\x3csettings\x3e\x3csetting name\x3d"margin" value\x3d"10"/\x3e\x3csetting name\x3d"maxwidth" value\x3d"800"/\x3e\x3csetting name\x3d"fontsize" value\x3d"11"/\x3e\x3csetting name\x3d"fontweight" value\x3d"normal"/\x3e\x3csetting name\x3d"fontcase" value\x3d"normal"/\x3e\x3csetting name\x3d"fontcolor" value\x3d"0xd2d2d2"/\x3e\x3c/settings\x3e\x3celements\x3e\x3celement name\x3d"background" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAAeCAYAAADtlXTHAAAANklEQVR4AWMUFRW/x2RiYqLI9O3bNwam////MzAxAAGcAImBWf9RuRAxnFyEUQgDCLKATLCDAFb+JfgLDLOxAAAAAElFTkSuQmCC"/\x3e\x3celement name\x3d"capLeft" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAeCAYAAAARgF8NAAAAr0lEQVR4AWNhAAJRUXEFIFUOxNZAzMOABFiAkkpAeh0fH5+IgoKCKBsQoCgA4lJeXl5ReXl5qb9//zJ8+/aNAV2Btbi4uOifP39gYhgKeFiBAEjjUAAFlCn4/5+gCf9pbwVhNwxhKxAm/KdDZA16E778/v37DwsLKwsuBUdfvXopISUlLYpLQc+vX78snz17yigqKibAAgQoCuTlFe4+fPggCKio9OnTJzZAMW5kBQAEFD9DdqDrQQAAAABJRU5ErkJggg\x3d\x3d"/\x3e\x3celement name\x3d"capRight" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAeCAYAAAARgF8NAAAArklEQVR4Ad2TMQrCQBBF/y5rYykEa++QxibRK3gr0dt4BPUSLiTbKMYUSlgt3IFxyogJsRHFB6/7/A+7jIqiYYZnvLgV56IzcRyPUOMuOOcGVVWNAcxUmk4ZNZRS0Fojz/O9936lkmTCaICIgrV2Z9CCMaYHoK/RQWfAMHcEAP7QxPsNAP/BBDN/+7N+uoEoEIBba0NRHM8A1i8vSUJZni4hhAOAZdPxXsWNuBCzB0E+V9jBVxF8AAAAAElFTkSuQmCC"/\x3e\x3celement name\x3d"playButton" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAeCAQAAACcJxZuAAAAtElEQVR4AWOgLRgFnAyiDPwMzMRrkHuwuCSdQZ14Tbpv9v/cf2UN8ZoMHu5/uP/l/h9EazK4sx8Cn+7/RpQmg+v74RBo11eCmgwu7keFd/d/wavJ4PR+THhj/6f9N1ZODWTgxKLhyH7scMvK3iCsGvbtx4Tz1oZn4HTSjv2ocObakAy8nt60HwGnrA3KIBisa/dD4IS1/lDFBJLGiv0r9ves9YUpJpz4Ji72hiomNXnTH4wCAAxXpSnKMgKaAAAAAElFTkSuQmCC"/\x3e\x3celement name\x3d"playButtonOver" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAeCAQAAACcJxZuAAAAtElEQVR4AWOgLRgFPAwyDCIMLMRr0Hhws6SLwYR4TTZv/v/8f+UZ8ZocHv5/+P/l/x9Ea3K48x8Cn/7/RpQmh+v/4RBo11eCmhwu/keFd/9/wavJ4fR/THjj/6f/Nx5OzWHgwaLhyH/scMuj3lysGvb9x4Tznod343TSjv+ocObzkG68nt70HwGnPA/qJhisa/9D4ITn/lDFBJLGiv8r/vc894UpJpz4Jt7yhiomNXnTH4wCAHC8wQF60KqlAAAAAElFTkSuQmCC"/\x3e\x3celement name\x3d"pauseButton" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAeCAQAAACcJxZuAAAAYElEQVR4AWOgNRgFPAwqDAZAqAJkofPhgBFJg8r/2VDBVIY7GHwoYEG24RmchcnHpoHhDxDj4WNq+I0m+ZvqGn6hSf6iuoafaJI/SbaB7hroHw9f/sBZ6HzSkzdtwSgAADNtJoABsotOAAAAAElFTkSuQmCC"/\x3e\x3celement name\x3d"pauseButtonOver" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAeCAQAAACcJxZuAAAAWklEQVR4AWOgNRgFAgwGDA5AaABkofOxAoP/UMBggMGHAxZkG57BWeh87BoY/gAxHj6mht9okr+pruEXmuQvqmv4iSb5k2Qb6K6B/vHw4Q+chc4nPXnTFowCADYgMi8+iyldAAAAAElFTkSuQmCC"/\x3e\x3celement name\x3d"prevButton" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAAeCAQAAACLBYanAAAAmElEQVR4AWMYMDAKeBgkgBgGmBn4GUQZONEVqfzfz6ACV6Bekv5gMYMcuiKDR/sZDGAKrqz5sf/lfgZdDEW39jPYQxR82/94/y0gZDDAUHR+f3rpjZWf99/efx4CsSk6sj+pbMvKI/vhEJuiXWDrQjNmr921HwyxKVoPd3hAxsS16/evx+JwleUoQeCbMRkRBIQDk/5gFAAAvD5I9xunLg8AAAAASUVORK5CYII\x3d"/\x3e\x3celement name\x3d"prevButtonOver" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAAeCAQAAACLBYanAAAAmUlEQVR4AWMYMDAKBBgUgBgGWBhEGGQYeNAVGfz/z2AAV2BS0vXgJoMGuiKHR/8ZHGAKrjz78f/lfwYbDEW3/jOEQBR8+//4/y0gZHDAUHT+f/qcGw8//7/9/zwEYlN05H/S3C2PjvyHQ2yKdoGtC+2e/XzXfzDEpmg93OEB3ROfr/+/HovDDZajBIFv9+RbDBpEByb9wSgAAHeuVc8xgA8jAAAAAElFTkSuQmCC"/\x3e\x3celement name\x3d"nextButton" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABEAAAAeCAQAAABgMj2kAAAAlUlEQVR4AWOgAxgFnAyiDPwMzHA+D4MEEKMAuQeLS9IZ1OHKVP7vZ1BBVaL7cv+P/VfWwJUZPNrPYICqxODW/lv7H+//BlNmfwtTyfn9EHh7/+f9N1aml57HVHJkPwJuWZlUdgRTya79EDh7bWgGyKJdGEp01+9fv3/i2oAMmHPXYyiRm7zYNwPZ08vBniYcdDQHowAA/MZI93f1cSkAAAAASUVORK5CYII\x3d"/\x3e\x3celement name\x3d"nextButtonOver" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABEAAAAeCAQAAABgMj2kAAAAlUlEQVR4AWOgAxgFPAwyDCIMLHC+AIMCEKMAjQc3S7oYTODKDP7/ZzBAVWLz8v+P/1eewZU5PPrP4ICqxOHW/1v/H///BlMWcgtTyfn/EHj7/+f/Nx6mzzmPqeTIfwTc8ihp7hFMJbv+Q+Ds56HdIIt2YSixWf9//f+JzwO6Yc5dj6FEY/It325kTy8He5pw0NEcjAIAWP9Vz4mR7dgAAAAASUVORK5CYII\x3d"/\x3e\x3celement name\x3d"elapsedBackground" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAYAAAAeCAYAAAAPSW++AAAAD0lEQVQoU2NgGAWjYKQAAALuAAGL6/H9AAAAAElFTkSuQmCC"/\x3e\x3celement name\x3d"durationBackground" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAYAAAAeCAYAAAAPSW++AAAAD0lEQVQoU2NgGAWjYKQAAALuAAGL6/H9AAAAAElFTkSuQmCC"/\x3e\x3celement name\x3d"timeSliderCapLeft" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAMAAAAeCAYAAADpYKT6AAAAFElEQVR42mP4//8/AwwzjHIGhgMAcFgNAkNCQTAAAAAASUVORK5CYII\x3d"/\x3e\x3celement name\x3d"timeSliderCapRight" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAMAAAAeCAYAAADpYKT6AAAAFElEQVR42mP4//8/AwwzjHIGhgMAcFgNAkNCQTAAAAAASUVORK5CYII\x3d"/\x3e\x3celement name\x3d"timeSliderRail" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAAeCAYAAADtlXTHAAAALklEQVQI12NgIBmIior/ZxIVFWNgAgI4wcjAxMgI4zIyMkJYYMUM////5yXJCgBxnwX/1bpOMAAAAABJRU5ErkJggg\x3d\x3d"/\x3e\x3celement name\x3d"timeSliderRailCapLeft" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAeCAYAAADkftS9AAAAnUlEQVR42t3NSwrCMBSF4TsQBHHaaklJKRTalKZJ+lAXoTPBDTlyUYprKo6PN4F2D3rgm/yQG/rfRdHuwp5smsNdCImiKKFUAx/OaSpR1xpNYwKK4/2rLBXa1s1CnIxxsLZbhGhtD+eGBSWJePt7fX9YUFXVVylzdN2IYTgGBGCVZfmDQWuDcTyB/ACsOdz8Kf7jQ/P8C7ZhW/rlfQGDz0pa/ncctQAAAABJRU5ErkJggg\x3d\x3d"/\x3e\x3celement name\x3d"timeSliderRailCapRight" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAeCAYAAADkftS9AAAAn0lEQVR42t3MTwqCQBTH8bcIgmirJYoiCOowzh8ds0PULjpRqw5VdCZr/WueMJfwC5/NezOP1lcUHWbv5V0o1LYSVVUjTXP4xYM4KTWYEB2ybFlcSSmLoK4F4vj4JmN6BFpbHs5krUNgzMDDLw3DCQHfTZL0Q85NYH0/Is9LNI240Tie0XUaRVGyJ4AN+Rs//qKUuQPYEgdg7+2WF2voDzqVSl5A2koAAAAAAElFTkSuQmCC"/\x3e\x3celement name\x3d"timeSliderBuffer" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAAeCAYAAADtlXTHAAAAKElEQVQI12NgIA/IyMj9Z2JhYWFgAgIGJkZGRhDBwMDEwMAI5TKQDwCHIAF/C8ws/gAAAABJRU5ErkJggg\x3d\x3d"/\x3e\x3celement name\x3d"timeSliderBufferCapLeft" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAeCAYAAADkftS9AAAAY0lEQVR42uXJyxGAIAxFUfrgI5CgzajdqlWxQffxaeiCzJyZ5MYMNtb6zTl/OhfuP2BZQ4h1mpLEmOWPCMd3pESSM2vE0YiKdBqJuDEXUT0yzydIp7GUZYMKAhr7Y4cLHjPGvMB5JcRMsOVwAAAAAElFTkSuQmCC"/\x3e\x3celement name\x3d"timeSliderBufferCapRight" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAeCAYAAADkftS9AAAAYElEQVQoz+WLyxGAIAwF6YM/CdqMlCtdcRHvMSIw9sCb2ctuIsQaU8pUpfQppT6mdC6QtZ6McYUPUpMhIHkP9EYOuUmASAOOV5OIkQYAWLvc6Mf3HuNOncKkIW8mT7HOHpUUJcPzmTX0AAAAAElFTkSuQmCC"/\x3e\x3celement name\x3d"timeSliderProgress" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAAeCAQAAABHnLxMAAAAH0lEQVQI12NgIAT+/2e6x8D0k4HpOxj9AJM/CWpjAACWQgi68LWdTgAAAABJRU5ErkJggg\x3d\x3d"/\x3e\x3celement name\x3d"timeSliderProgressCapLeft" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAeCAQAAABOdxw2AAAARUlEQVQYV2NkgANG+jP/+zJkMtgCmf99vi38KPQTJPpq6xsvqIKznxh4ocwjCOaebQyeUOZmX4YFDEJQw9b4QQ2DAfoyAVkTEmC7RwxJAAAAAElFTkSuQmCC"/\x3e\x3celement name\x3d"timeSliderProgressCapRight" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAeCAQAAABOdxw2AAAASklEQVQYV8XLIRKAMAxE0R4QbhrXoQqJxWJxCGZqaKs/m1yi+80TSUqzRmNjCd48jMoqXnhvEU+iTzyImrgT+UFG1exv1q2YY95+oTIxx/xENX8AAAAASUVORK5CYII\x3d"/\x3e\x3celement name\x3d"timeSliderThumb" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAeCAQAAACP8FaaAAABMElEQVR4AeWSv0rzYBjFfy1NlU5RKC3dCjqZDwRXEapOuuik+BfbNLdUeg86pHSrm1Z3G3w7VAdbB+sNFFKIZ1FCjTjL95wQOOd3IC/vE/6vSZEmQ5Z5KUtGLhWjshYLbHCIKx2wLmcp/cJzOFTb/vtoGk7D8bDtc4GjNP2J/+ENzFv0FBnpORpHA4OnVBWwKFANTD96jKkfBYYqRVFyVC5bCr/pqsWmKDZHd8Okwv2IY1HyuL0wqRCE1EUp/lR4mFAT1XNym/iJ7pBTCpBnp5l4yGaLXVFsVqh1zCzuGGoiNuQoUcG7NjPYU1oSxVKrzDZuw+++BtPe5Oal4eOypdQWRVfNoswa+5xTl87YkysrjW3DpsQyDquSw5KcjXB83TlFeYoU9LbltO7ff5i/Mh+pOuncDFLYKwAAAABJRU5ErkJggg\x3d\x3d"/\x3e\x3celement name\x3d"timeSliderCue" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAeCAYAAAAl+Z4RAAAAcUlEQVQ4y2NgGAWjYBTgBaKi4llAfASKs0jWbGNj96S1tf03CIPYJBkCsrW6uu53bm7+fxAGsUFiJBmQlpbxOzMz5z8Ig9hAsaMkecHIyORJUlLq78TElN8gNlAsm9RwyAbZCsSHgDhzNFmNglGAHwAAo/gvURVBmFAAAAAASUVORK5CYII\x3d"/\x3e\x3celement name\x3d"hdButtonOff" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB0AAAAeCAYAAADQBxWhAAABf0lEQVR42u2VvUoDQRSFA0awMIVCsv+z/1oE8yOE9MYmtb2P4AspSOyECFZqtU9gbZvK6CNoNZ6zMMuSQpxdEAJbHC737pz59mbmblpSyn9XA22gDXRLod2uMYfWkKwh+uc60LVtO9J1RWXBn4N1oNL3QxkEEcwuzYybOWMh07QJ4xqK/ryuBQ3DWEZRoowdx3FfhAgkI3NVp7IsO5xMpnPDsFae59NHvzaURgWlWpblPEOSkbmqQzfQK2DT8fj0HB0rrz40jlOqgA4Go1m/f3LJWIYC8uQ4nkSX94vF3S5qX8qrDU2SlCqgOMMrAK4Zy1B27nlCIj4i34G+lbcC9ChXuSNeFEbmpZe5RZdv+BU4ZjM8V159aJoe5yp3JIS/eaZcv7dcPhzghc6Qr3DZlLc6FOelRoTn9OvI4DKxw2rQXs/84KzRyLPhTSSQGzIyV2OBdYzIYz4rgKxjn88/Q4fD0QUNNT6BBL5zH50Pfhvahzo1RH+7+WtroA10O6E/bVCWtAEB8p4AAAAASUVORK5CYII\x3d"/\x3e\x3celement name\x3d"hdButton" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB0AAAAeCAQAAAB6Dt0qAAABPUlEQVR4Ae2SsUrDUBiF/0EFfYK8Rl4g5BUUHGILRWghUHAQHJzaUcjSgB1EtCApliDoUApSKggZRFSUQsVAawspElz1OunxhwtZcm0Ht9LzQfLByVluLs145lkkjXQyyPwTg3uNv0tFKzuR+MAkIlF2eJyKPhBjRBMZYyBIp1SMEV6nMgIZlIoZQkJuIw7RiMll36XN5e31k0AkramYdiGhQjPsohlSgT13GTy8WXurR0mrmt5BQla+ZJ/mS2SxF8+GT7joLRRvvmWrnAaQULbi1R4rHmXZi/VhAO9laev6R7bKaQcSsv3+Lfw+2ey548B/t/Yz3pVs1dMWJORW4xaqfEzsfEwrO2te5ytpFVPjHJJntPnZ5jc708M9muwS1c/Ra8LHNGrKK6FlnENRxyQOPjcc0v5z/Wc68/wCXWlzVKUYIC4AAAAASUVORK5CYII\x3d"/\x3e\x3celement name\x3d"ccButtonOff" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB0AAAAeCAYAAADQBxWhAAABzUlEQVR42u1Uu0oDQRQVTCMopMjmtZvdJPswKCQbC6tYCEqMBDUGrf2NCDF+gmXEyiZWiTb+gMTGxtrGwmh8IOKjUoLjueNGfCBk10rYC4eZOey5Z+7M3O1zww033Og5BCGQA9oAcw6uz9kxbYfDIpMk2TGg58Z2TJmixFg0GueIRBQWDIZ5BX5/kIli5AcfCIS6PIH0nLdlGoupLB7XmCxHyegymTSXa7UdoVBYHBVFqQEDMjozzfRCvd7w5fNzKfD74ElHevumEHKEQiJD4nmYz4JvwWirWt30YiO36fTYNKotgj8Hv1GprPvAP1obtm+qqjqBhC/l8toAkh18uqs7rK8ZY/0Yj8AT90o80LG09k01TQe48Bnw4O6asqzw5DjGXVR2Qt9iPLb4Dh07NnGvqhq0jkwNQvehTCYSI0tIeIWqtq1jfAA/bhiJFcxvcPzVUmlVwPwJVZLWvqmuD3MgGYlbGHPN5qE3m52JYU0PifhTGEwRn8lMaFjvYVNdrXNT7BjGX1tGkvgL/dYyxMv0vTNTahH02ocY1cBEpTbgeL8z41eeNKSn6+jZNJUyiyT4y28Q+gvK07MpWsEDDAJDzsH1nj433HDjX8YbqHFYmhICTLsAAAAASUVORK5CYII\x3d"/\x3e\x3celement name\x3d"ccButton" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB0AAAAeCAQAAAB6Dt0qAAABWElEQVR4AWMY5mAUsDJIMBgy2DE44IR2QHkJoDoMINHQ/eTbl//44JNvDd1AzRjA8N63p/+f4IVP/9/7BrQZA9g9/H+fIHz4H+hsDOBw6z8EnvqZsJ6vznDCkke3/h/9Hr2ap9Z08oqnMFkGByxaL/+HwMiVafNufFl+hWvmiR+BC/IX3/yy4Bz/nJN/wbLYtZ75D4In/3GV7n56/v+1/zd/H/rGkHPgJYh94/fp/2B57FqP/AfBg/84SlY/O/L/8P+JLze/Z8je8PrI/0P/Jrza+Rcsj13r3v8guO9/+LKEhZu+9lzmn7zrl++c9BWbv7WfE5iy/S9YHrvWbf8hcP+P0FVsVSo9y57s+L/vm/9ytiqtvhVANlgWq1a79f8hcDPQR9eBAbIHyN7y/yyQfQnEhkCskWM4/9uq/4TgfKxJQiK6e/a3pf/xwZlfo4AJkZLkP6zBKAAAGMt/2TouFxQAAAAASUVORK5CYII\x3d"/\x3e\x3celement name\x3d"muteButton" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABoAAAAeCAQAAACY0sZTAAABZ0lEQVR4AWMYjGAUMDEwMzCSpoUxju+kDQMXAW1AaRYGdiCGsFjchd/OWmELFMGrhd1a4UUTAy+QzXLSdKMhA1+Z/tuF0qIMTLjdz9tp+27ly/0M4kBbWGdqv1/gJcMgdLz6YAA2u9gYhBgkGGR2pH3ZfWf/1f0Mshdsk8UZBDYlXMthEJhqfbuVgQ9Tk9D//SD4dv/F/eeBkEHuaNjjegYBT/k78xiEOcWuLWIQxtQkcWI/MmSQYhC/shioUPjUAhB5cgFWTQf3I0MGaQ6JwyBNIofBmsAkpvN27UeGDPI349dXMghEKu2byyAsKLZ/IYMQzoBoTNm4e8v+LcCA2GBoKsQgcDFjcRqDwBr7dU0MfLiDnCfaavHKdaAgZ2ZgXWd4cZ6eJIPQ5YYZXgzseCNXQ35GPSRyt+lVaTLwTTA9NJdTmIGJ2GTEzMCSKPZifoklpj14jTDj6jJj4CI5nYOzxkCCUQAAMVp+znQAUSsAAAAASUVORK5CYII\x3d"/\x3e\x3celement name\x3d"muteButtonOver" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABoAAAAeCAQAAACY0sZTAAABfUlEQVR4AWMYjGAUsDJwMLCQpoXRTnZZIoM0AzMBZQzcDCIMXEAWC5Dk0tZ6fK0uFyiCBzAziCh5Xd7PoAJkc64I7QxhUPWLf/yQ3xjoTByAjUExrvzB+5f/GewYOBn4cgOf3ddxYNDftH1OCza7BBgMGBwYfCas/fjnzv+r/xn8NiXYGTJoTZ25ZymDTn7W8UMMapiaDP6Dwdv/F/+fB0KGgJXtF3YyaGp7XLrLYMhqce4hgyGmJocT/5EhgxuD7ZknDEYMJgcfMBgzGB8AkZiaDv5HhgzuLPa7nwBNN90N1gQmMZ236z8yZAjcN3H+JgZNM+8tQOdxWm17yGCAMyBSV6//s+X/lv8Mvv2BChoM2hsXd89n0GnKn7+PQRV3kCvYlsx6v+4/gy0DOwNvU8SJO1LWDAb791bUMgjji1xhMc/u3QzKoMid6hPtxaCakrbzDqsBAytxyYgZmFQ5bfXu3Q1Lx7QHrxHykgWRDFJAA0gCLAzsQC0DCUYBAC3AlmbNhvr6AAAAAElFTkSuQmCC"/\x3e\x3celement name\x3d"unmuteButton" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABoAAAAeCAQAAACY0sZTAAAAiklEQVR4AWMYWWAUMDKwMLADMUla2K0VnjUx8BKvhYmBt83m3cp3+xnEiFHOxiDEIMEgsz3l6+5H++/sB7KJAEL/94Pgu/1X918GQuI0SZzcjwSJ1XRgPxIk1nnb9iNBoCYSAqI6ZdXOtfvXAjWREuQ84VZzVi4DBjmJkassN7GegZe8ZDQSwSgAAJ/LQok1XVtuAAAAAElFTkSuQmCC"/\x3e\x3celement name\x3d"unmuteButtonOver" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABoAAAAeCAQAAACY0sZTAAAAjUlEQVR4AWMYWWAUMDJwM4gwcJGihZlBRMnr0l4GZeK1sDEoxpQ+eP/uP4MVMcoFGAwYHBh8+ld/+vPo/53/QDYRwOA/GLz7f/X/ZSAkTpPDyf9IkFhNB/4jQWKdt+0/EgRqIiEgElct/7P2/1qgJlKCXMG6eNL7Zf8ZLEmLXGFhj5bdDMrkJaORCEYBAOZEUGMjl+JZAAAAAElFTkSuQmCC"/\x3e\x3celement name\x3d"castButton" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAQCAYAAAAWGF8bAAABuUlEQVR42mNggAA2IBYCYgkKsBDUHDAQevr06X5KMdRQMJDYvXs3SECLTNdpQfVLwA3cuXMnigCJAEO/xPbt2ykyEF2/8NatW0ECwuQaCNUPNpAZiAVqamqsgTQXuQZu2rQJYqCXl5cQ0LkpjY2Nbuzs7BJQQ5lINXD9+vUQA8PDwyWPHz++4/Lly/uvXr26btmyZUkCAgKiQElWIGYk1sC1a9fCvczNwcEhHxER4T59+vTuEydO7APiqS4uLkpQQ4kycNWqVRADQ0JCxIAu7JgwYUI0CwuLWlpaWtDmzZu3AsVmqaurSwIVsRBj4IoVKyAGurm5iQKdO/fUqVP7Tp48Odfe3t4wNjbWG2jo3o0bN5YAFfES4XUJYFDBvQyKBBmgIX5r1qzZBHTZAh4eHrWOjo6GPXv27ARaqApVI4wvpyxZsgRiIDDsZM6cOTPT19fXLDIy0hvo2n3z5s1L8fT0tF66dOm+uXPnxldXV+vdunVrPz68aNEiSF4OCgqSBUU50GXTgQLSU6dOnbFt27YpIFfPnj17JdCCalA6JeBClNKGHYgFgZgfiDmhYcYL9SaI5iEyYsAAACZV+irLroZ6AAAAAElFTkSuQmCC"/\x3e\x3celement name\x3d"castButtonOver" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAQCAYAAAAWGF8bAAABuUlEQVR42mNggAAOIJYAYgUKsATUHDCQePr06X9KMdRQMFDYvXs3SMCCTNdZQPUrwA3cuXMnigCJAEO/wvbt2ykyEF2/1NatW0ECUuQaCNUPNpAFiEVramr8gTQfuQZu2rQJYqCXl5cE0LltjY2Ncezs7CAbeIGYmVQD169fDzEwPDxc8fjx498uX778/+rVqy+WLVvWLCAgIAOUZAdiRmINXLt2LdzL/BwcHFoRERHx06dP33nixIl/QHzcxcVFF2ooUQauWrUKYmBISIgs0IXbJkyYUMnCwmKclpaWt3nz5k9AsXPq6upKQEWsxBi4YsUKiIFubm4yQOdeOnXq1L+TJ09etLe3d4yNjU0BGvpn48aNs4GKBInwugIwqOBeBsWsGtCQjDVr1rwFuuwqDw+PcUdHx+o9e/Z8B1poBFUjiS+nLFmyBGIgMOxUzwCBr6+vR2RkZArQtf/mzZvX6unp6b906dJ/c+fOra+urra7devWf3x40aJFkLwcFBSkDopyoMtOAQVUpk6denrbtm3HQK6ePXv2I6AFS4BsMQIuRCltOIFYHIhFgJgHiIWgmBdKCxAZMWAAABFDD0iNkbKIAAAAAElFTkSuQmCC"/\x3e\x3celement name\x3d"castingButton" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAQCAYAAAAWGF8bAAAB60lEQVR42mNggAAOIJYAYgUKsATUHDCQ+E8FADUUDBRevXoFEnAAYgsoTSwGq4fqV4Ab+OLFC5CABZkus4DqRxj49OlTsAtBNKkYpg/ZQKmHDx+CBCxBNKkYZCCUBhvIDMQis2fP9gfSKjdv3vx07969/6RgkIFQGmwg35kzZ+omTpwYxcPDo6mmpmaybNmy6devX/9569at/8RgkIFQGmyg8Nu3b39++/bt/9evX1/u3r27lYuLSy87Ozvy1KlTz65du/afEAYZCKXBBvKKiIhol5WVpe3cuXMX0PB/z58/P+3u7m4dFxfnD3T9x0uXLv3Hh0EGQmmwgYJPnjzZvGTJkkpOTk6TysrKbKB3P718+fKKvLy8QUNDQ965c+f+48MgA6E02EChy5cv33z37t3/N2/eXA4ODnYrKipKuXr16s8LFy4sAsprAl1+6vTp0/9xYVA6hNIQLwOxWnFxcd7Zs2ffvn79+q6cnJz5ggULFj148OBXUFCQNVBeCYjN8eWU48ePww0Uef/+/en09HRfYESkAJ3+Z//+/f1OTk7uR44cAbG7qqurCeYgoFp4XhYDBSgwL14FpcNNmzYdunHjxkWQq4FevXb+/PmNQLY4EEsSW9pwQDWIAjEPKJJA4QoNCiEon5WBSAAAryiVoYy0dtoAAAAASUVORK5CYII\x3d"/\x3e\x3celement name\x3d"castingButtonOver" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAQCAYAAAAWGF8bAAAB60lEQVR42mNggAAOIJYAYgUKsATUHDCQ+E8FADUUDBRevXoFEnAAYgsoTSwGq4fqV4Ab+OLFC5CABZkus4DqRxj49OlTsAtBNKkYpg/ZQKmHDx+CBCxBNKkYZCCUBhvIDMQis2fP9gfSKjdv3vx07969/6RgkIFQGmwg35kzZ+omTpwYxcPDo6mmpmaybNmy6devX/9569at/8RgkIFQGmyg8Nu3b39++/bt/9evX1/u3r27lYuLSy87Ozvy1KlTz65du/afEAYZCKXBBvKKiIhol5WVpe3cuXMX0PB/z58/P+3u7m4dFxfnD3T9x0uXLv3Hh0EGQmmwgYJPnjzZvGTJkkpOTk6TysrKbKB3P718+fKKvLy8QUNDQ965c+f+48MgA6E02EChy5cv33z37t3/N2/eXA4ODnYrKipKuXr16s8LFy4sAsprAl1+6vTp0/9xYVA6hNIQLwOxWnFxcd7Zs2ffvn79+q6cnJz5ggULFj148OBXUFCQNVBeCYjN8eWU48ePww0Uef/+/en09HRfYESkAJ3+Z//+/f1OTk7uR44cAbG7qqurCeYgoFp4XhYDBSgwL14FpcNNmzYdunHjxkWQq4FevXb+/PmNQLY4EEsSW9pwQDWIAjEPKJJA4QoNCiEon5WBSAAAryiVoYy0dtoAAAAASUVORK5CYII\x3d"/\x3e\x3celement name\x3d"trackButton" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABwAAAAeCAYAAAA/xX6fAAAB3ElEQVR42u2VP0sCYRzHLwiFUm4oIcUGz4ZMsRqkhhan2hzyBWSvwMXhAsGlFxA46y2JeJpDIeEfDnV1UhdX/+Du5mS/LzyC2F09KDjdAx94nuf3fZ6PPj53CovFQtglgik0habwX+FasxDHhJfwM7xsDjUbcUZc6YB5G69wj7C7XK5AqVSSR6NRfj6f1wD6xWLxBTXKXNMazQhIeYX2SCQSnk6naqfTySYSiZgkSXcAfZpTUAuFQrHxeKwZwSu04NNPJhM1k8m80thHiMQ+A30fasPh8EMUxQiNw0SUeFrhgTjhER6pqio3Gg2FySzC74Y5H2WyyFL/Zpsj9Xa73Xw8Hn9m38aoiZSJIUv9+16vp63DKwz0+/2G2+1+pL6HONCRYc6DDLLUv2U3M7rJkQaazWY9l8u9z2azCo0lHaGEGjKtVquONezbbHSkF7TR52Aw0NrtNhYFdYRB1JCh7BfWYHP6TbVVeIX+arVaq1QqGmBHtd6ulnVk2Qth/SXA/eCf04NdK5fLGjASLuvIYo3RzeIROlOpVLpQKGiAxpc6+1wu68lk8g2XYxuh1eFwBGRZTiuK8m10aVBDhrI4Tus2QoFt4CROiUOdfQ5ZzfmXjEto/gGbQlO4c+EPA9e3TyseGL0AAAAASUVORK5CYII\x3d"/\x3e\x3celement name\x3d"trackButtonOver" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABwAAAAeCAYAAAA/xX6fAAAB3ElEQVR42u2VvUsCYRzHj4awhq5AF3Mol5bSFjMSstYabGusuaVbHBwEsf9DpMDBF4QGB8FBhSYnvQahIfTEtsIg6AWevt94hLCzDoWm+8EHfi/fe74+j/eiCCGU/0SxDW1D2/BPw5FwgGXgBzsSv+xxtgg2wZ4J7C9aNZwBS263O1QoFC673e79qwzm+Xz+ijNo9sUvQVOrhkuRSOS43+8bjUZDj0ajSa/Xe0SYo3fLWSAQSBqGIcZh1dDBX9/r9YxUKnWNOgicYFbCPMhZp9N5UFX1DPUx0EDiG6dgxYqhO5fLXVYqlVtp5lB+BntBaHRqkR9Mc6T+ZrN5r2nahdzNuHBCk6QW+Umr1RKjWDUM6br+4fF4zpGvgwUTM/bWqaEW+aG8M7VJjjRUrVbfM5nM3WAweEa9YWK4wRk1tVrtndfI3Ux0pNtY6LHdbot6vc7GronhLmfUQPvEa7g4/lPxHauGO+Vy+a1UKgkij2o09oZzauULYfQlYPnB38KD/VosFgUZZzicU4s6MO7OsmK4mkgkbrLZrCCowybrhIfzeDxe5c0xjeG8y+UKxWKxm3Q6/YLaZ7KOjzNqoOVxzk1j+GXKnYI1oJqso8rZqtQqExvaH2Db0Db8d8NP8a/SZovcDd8AAAAASUVORK5CYII\x3d"/\x3e\x3celement name\x3d"fullscreenButton" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABYAAAAeCAQAAACC7ibdAAAA5ElEQVR4Ae3KsUrzYBhH8RPIFAJ5O3/ig5COgVyHW7N09x7aXSrESafuHeLi0A6iGEX+Y3edLMqnpe7egfbFMZCMXfo762GH9gIijIx8W0rcMQ9tU/3oL9KOGXdYLOuNfOS0CrGLyVr/fZ1zMht9a6VXqV6JjFa9efmiZ43PDoqnCqMh8BGS4IjpT8vTMYY7NiIaooHhsNnovqRPTA9HSOCjwT6ro+Jy8qV3PZT0aJUt9VavdadbnY9IaJUv9KiF5jqZYIQd87V80/rfAEdAq/RKvht9VEPrmmNS8m0ZRkTAzuz9AlNJVl+tEWchAAAAAElFTkSuQmCC"/\x3e\x3celement name\x3d"fullscreenButtonOver" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABYAAAAeCAQAAACC7ibdAAAA5klEQVR4Ae3MIUzDUACE4b8VlU1FaQWEBPlQna+oxqHm0dTicShQcyWZwSBWEgohEIKcB8UKAZbhcZXHmsw1eZUz+357OdZow8HHkJItSwiwcodmUWuFpO852s2nzUJtZFh5mPNyrq+23nE4Lv4007templIsYon1ZtedXKzkz/XGDocXBw8QiICBqPq9JJ9ogODT4d/aIgw4+KhYkBAzBbe6qLD/NR7+UX5q089VsRYpVN9NHPd605nBSFWWaknlZroqMTg9Yyv1TZqto+JcLBKrtR2q+96aHCxCkjIlqUYfBzWZuMfAHJlDLF+xFEAAAAASUVORK5CYII\x3d"/\x3e\x3celement name\x3d"normalscreenButton" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABYAAAAeCAQAAACC7ibdAAAA50lEQVR4Ae3KsU6DUBhA4QMNAtsNFcJLyKBx8mXYmNxkculDuJG4OOOmcbr/QNS1xKaJqxJjTJpUk84KuHW4d+nY76yHvV1zxlx8AiZYeJeHBKgmX14wte1qXZ1l98VG/8iyJMQo+ZJVvdGddPohx8co7eRThvWmQOFa5ncZWtSnRwQ4GEVvMvQh62oW2+YDItK+BIW3PTt4KJJxiPrVyJnF39Wv/EdkmQlOsqd6IUOkGLmou+JVv0ifdfabfKVbaXVTt0KCUfhczmWur4rj7LFCYTRhelte5yiC8xgPbHuIj4sztrdbfxJjV3K8mZ7yAAAAAElFTkSuQmCC"/\x3e\x3celement name\x3d"normalscreenButtonOver" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABYAAAAeCAQAAACC7ibdAAAA7ElEQVR4Ae3Sr07DUBzF8e+daKaaiaYNAoH8uc43pK+AmsHimETxDAQBQZVkCQhAUFMBewkUCG4W/ib4haTykCYzmFszuc+xX3lYtw3HAEdEQsqQHvGekWKz6qFh3Jfbl9+Znta/WmrekBFU/GjRLvWuN11UJASVXh/yetVxjRH1xM/qNm+3D0lxBOVP6vaiTz8xBgSNyCkpKTBiHP84YoyiC8gZETSY2LfXCjlBjnRretk26kZJUISd1I+679YbJ7NqoTvd6Ly9FQVB2ay51pX262x65jGChoyPmoMKI901YujLMxKi1TnXa+MPEjlkhvYbWGMAAAAASUVORK5CYII\x3d"/\x3e\x3celement name\x3d"volumeCapLeft" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAMAAAAeCAYAAADpYKT6AAAAFElEQVR42mP4//8/AwwzjHIGhgMAcFgNAkNCQTAAAAAASUVORK5CYII\x3d"/\x3e\x3celement name\x3d"volumeCapRight" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAMAAAAeCAYAAADpYKT6AAAAFElEQVR42mP4//8/AwwzjHIGhgMAcFgNAkNCQTAAAAAASUVORK5CYII\x3d"/\x3e\x3celement name\x3d"volumeRail" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACoAAAAeCAYAAABaKIzgAAAASElEQVRYCe3BsQ3AMAwDQRIW4Cqlkf031AZKVkg6An8nAQCAH3zOPQpQe28lqJcS1FpLCcpWhJKsBGVbCaq7lcAzcwkAAHz0AE0SB2llBfTtAAAAAElFTkSuQmCC"/\x3e\x3celement name\x3d"volumeRailCapLeft" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAQAAAAeCAYAAAALvL+DAAAAeElEQVR42tWKQQqDMBBFB3cFt9oQQ0wniW51b5f2ti30ZLX1AN+ZQA/hhwfz/zw6eZrmmoWn8NUyCh9jLJzzoLY1L2sd+v6GEBikmh7MCTHmYvyYI1LKBeo69/Y+SBkKtCz3SaztPxKAal0fs5ry2Emjo3ARajpNDtqHL/b2HUUVAAAAAElFTkSuQmCC"/\x3e\x3celement name\x3d"volumeRailCapRight" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAQAAAAeCAYAAAALvL+DAAAAeUlEQVQYV9WKOw7CMBBEV3RItAmWYzlmbUMLfSjDbUHiZASFfpj1LTLSW+18RLarrjt+yZPUFoQQ4ZwHgw+5SEqKcTzB+4C+dy/JuUK1wAouVimlwlDNtvgxOMOIMWEYwrsFZtgu03S/Cp/Vmnl+3ADshOdA9s1sSn8goC/6ib5oHgAAAABJRU5ErkJggg\x3d\x3d"/\x3e\x3celement name\x3d"volumeProgress" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACoAAAAeCAQAAADwIURrAAAALElEQVRIx2NgGAWjYBSMRMD4/z/1DWW5TQOXsnwdMoZ+GyouHQWjYBSMTAAAnO8GxIQ7mhMAAAAASUVORK5CYII\x3d"/\x3e\x3celement name\x3d"volumeProgressCapLeft" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAQAAAAeCAQAAAChtXcIAAAANUlEQVQY02NkgAJGOjH+9zEkAxm/JrzJ/wYSufTxLx9Y6shHBghj10SGPKji9RMYkhjp6EIAcaIN1SJ2FnYAAAAASUVORK5CYII\x3d"/\x3e\x3celement name\x3d"volumeProgressCapRight" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAQAAAAeCAQAAAChtXcIAAAANklEQVQYV2NgoCP4//F/H5hx5/+z/78mABnn/5//f+kzkHHkPxCCGLv+A+FEIGP9p/UgFXQFAHkZGwN2fDIsAAAAAElFTkSuQmCC"/\x3e\x3c/elements\x3e\x3c/component\x3e\x3ccomponent name\x3d"display"\x3e\x3csettings\x3e\x3csetting name\x3d"bufferrotation" value\x3d"90"/\x3e\x3csetting name\x3d"bufferinterval" value\x3d"125"/\x3e\x3csetting name\x3d"fontcase" value\x3d"normal"/\x3e\x3csetting name\x3d"fontcolor" value\x3d"0xffffff"/\x3e\x3csetting name\x3d"fontsize" value\x3d"11"/\x3e\x3csetting name\x3d"fontweight" value\x3d"normal"/\x3e\x3c/settings\x3e\x3celements\x3e\x3celement name\x3d"background" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAA0CAYAAACQGfi1AAAAYklEQVR4Ae2VwQ2AMAwD/cgKVRbJuAyH+mOBfMMQyBKCuwWsxoaLtfKQkaiqtAZ0t5yEzMSMOUCa15+IAGZqgO+AFTFTSmZFnyyZv+kfjEYH+ABlIhz7Cx4n4GROtPd5ycgNe0AqrojABCoAAAAASUVORK5CYII\x3d"/\x3e\x3celement name\x3d"backgroundOver" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAA0CAYAAACQGfi1AAAAY0lEQVR4Ae2VsQ2AQAwDXWSFF91Pkf1rxkAZIm0YAllCcF7Aiu3/i7WOU0ZFZm6rQXfLaiCzYkbuC+b1EWHATM3iHbAiZkrJrIiSP/ObQjQ6gAcg8w/AsV/w2AEmE1HVVTLqBmJaKtrlUvCnAAAAAElFTkSuQmCC"/\x3e\x3celement name\x3d"capLeft" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAA0CAYAAACHO2h8AAAA4UlEQVR4Ae2XwUoDMRRFT17GTscIMoWOqwF1WUSFIv6Autf/X5TuxG6FBkOeHfAHpk+GLnI+4HBzLzyI44/l8uoBeAVugJqRuIMA4L1t24+u685DCGci4hhJBdwPkr7vL3POLsaIqnKM6G2xaJuUksPAILquqtlMFayiuYhzYDMJIygi+2qonloi0CkTldXK/NOXXVYrZRs6UgyUjsrxL6d28sP2b4n0xJ62z1nVHbCutolx/4MRH8LFt6o+Nc28tqTyq9Xd5273RUrpVsSL915gvNCt188MbLebR+Dl2K/oL+WmRveI4jXNAAAAAElFTkSuQmCC"/\x3e\x3celement name\x3d"capLeftOver" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAA0CAYAAACHO2h8AAAA5ElEQVR4Ae2XMU7DQBBF346sIDAUDoqprNBCm4Im3IPcAE7EEbgId6BF6akQjheZGTYSF7DXQi7mSdM+zf4vjbSBP1arqy2wA26BUwZSJAHAY1VVT3VdX5RluZDEYBGwPUqaprlUVYkxYmaMEe2Wy+q873shgwK4KYrFiRnkis5EgkCeScjHRQNaw2xuG4HNYiNvzeufPmxvzcPOz8jIwDPy4++n9t8P22Qb2cye1qqahhAkt7W3GLvvKep/+Uyo/igYY0fW6+vXtv16/kgcDl2nagkYOmGzuePIfv9+DzyM/Yr+AujSfWZZzzLnAAAAAElFTkSuQmCC"/\x3e\x3celement name\x3d"capRight" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAA0CAYAAACHO2h8AAAA20lEQVR4Ae2XQUrEQBBFX4e29QJDVgFv4Cb7wSt4Ps8wLtw5B3A97mfmAFlkkbaZMpAynkBiBRGpd4Ci6j/4UGGzqR9ZjgBn4AV4A4ht29YsZJomzTnXXdfd9X2/A55iKYWlhJmU0nXTNAl4mIedwnZ7/4wBkcvH8Xh6jaqYiDFdAbcRFAtVFQJwU7ESPuh7zPrX3wj0T2zk1lz/+mG7NQ/bnpFixDPy8veq/dViW20j/W+drTOAmK2JXEbgbDrt628bhqEA+x+dpjMiMuY8lFLed8DB+orugQPAJ8i7bEsKl1PuAAAAAElFTkSuQmCC"/\x3e\x3celement name\x3d"capRightOver" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAA0CAYAAACHO2h8AAAA2UlEQVR4Ae3XwUkEMRTG8X8eIaLgwYXF0xRgKYsVWIIVrR1sI3uwANkSvMxhDhOzRoZ5pgOZSZiDvF8Bjy/vgwdx+/3jO8tdgQtwAs4A7nB4/mShuYgx5r7v4zAMR+DNp5RYyjknIYTbrutugNcy7ENYQVUpoZimSXa7h3vgxatSxfsQgCcPdZNEnAB3QiM26G/V9bdPBLp9ImvN6t9y2daaLbtiR0ol25Edfzu1mx62Zon0v91sVZ2Bq1Ap5+8f4FL1tLkYC+C06mla5CLGcUzp6wicm31FfwHzmG90m7lXIAAAAABJRU5ErkJggg\x3d\x3d"/\x3e\x3celement name\x3d"bufferIcon" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACQAAAA0CAQAAABI31KIAAABGElEQVR4Ae3Rr0pEQRSA8Zl1b1uDQTAt4j8QES1qURZvEf8lfYJVsfoAisYFq9mgyfUFVptgMtk3CAaD6DN8HoYbFhk9w9x0Yc6XDsv8LrNj0vgnTZo05LzzyR7m/wxafQC+sDHQENkv6DsG2uFV2i62nDc+2C82SybVwqAX+tIzxlOdzBUEPTnosTy0wgM9lryQpS7pVwutetAiN3RZU481mJYaf0PX9KR7rALNMCtNaVC3PLTALXesYpSGlatFVDFonnNOmfQeGKHFOqNhUIcr6cwLtdiVNkIgy6WDLrxQ7qBNrApJy0J1mCu2CY6k4qKMCbJFM/TPHvzeASfS8cBvtbhXazvosPzzN2lL4/GQXoISlKAqQz+eXnU2Tp6C2QAAAABJRU5ErkJggg\x3d\x3d"/\x3e\x3celement name\x3d"bufferIconOver" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACQAAAA0CAQAAABI31KIAAABGElEQVR4Ae3Rr0pEQRSA8Zl1b1uDQTAt4j8QES1qURZvEf8lfYJVsfoAisYFq9mgyfUFVptgMtk3CAaD6DN8HoYbFhk9w9x0Yc6XDsv8LrNj0vgnTZo05LzzyR7m/wxafQC+sDHQENkv6DsG2uFV2i62nDc+2C82SybVwqAX+tIzxlOdzBUEPTnosTy0wgM9lryQpS7pVwutetAiN3RZU481mJYaf0PX9KR7rALNMCtNaVC3PLTALXesYpSGlatFVDFonnNOmfQeGKHFOqNhUIcr6cwLtdiVNkIgy6WDLrxQ7qBNrApJy0J1mCu2CY6k4qKMCbJFM/TPHvzeASfS8cBvtbhXazvosPzzN2lL4/GQXoISlKAqQz+eXnU2Tp6C2QAAAABJRU5ErkJggg\x3d\x3d"/\x3e\x3celement name\x3d"errorIcon" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACQAAAA0CAQAAABI31KIAAAB3ElEQVR42u2Tv0sCYRzGv5WFJIVgkEVLSy1ObWGDUE0OgdRYtBZC/QENFv0DDTW0FEYJGkgEBUZCEFxYlJpnEMSpUxpBNAkiT++rlb+uvNOpuOcz3Pt+j3vgeN8PkRYtWv5Z2qmb0d58kXl7ZXuFzM3W6E3jybfUW+8E6ZupaaXB3ZNnPGPnlAbZruF02ebTuRRSSOds89TVaE0bWYJiEhIjiaBIFjZpKKaF1TSePknDuUamRmo6dKPRzCNKRDO6UepQW9NCAxseCXHGlHvKzZ8SNjw0wN6oSqfFIWXvwSE72YsrKWtxkEHdsQ/5hRjuCpCNbMVVDEdXNKzmGhhnlqT8DYrwoq+1lJ9ZIqNyu0aERAhXn/Cir3UIQoJGlJpndm2KuPyGF5V2IlxbyszTmybi7xcowYvK9/H3/sn65hXsEnBeBi8q3wuKzGN2PeQCKIcff+Xkoa55zK4zMYCTCubcs+7KSQBn3DzdL3Ytrt3iuIpXRvXsFs516vnFruuMH8oI/Whewa4gDmsY8435aqfBH81jdoWzXtTi8Dm8cvOwrHkFu/zwyJDBi+yc/aCMecyuUH4f6rjOTy9Xm9cXiRxgTyX7iESor7LIQENk5XdYFVb2lYG0aNHyF/MB+x5LQiE6gt8AAAAASUVORK5CYII\x3d"/\x3e\x3celement name\x3d"errorIconOver" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACQAAAA0CAQAAABI31KIAAAB3ElEQVR42u2Tv0sCYRzGv5WFJIVgkEVLSy1ObWGDUE0OgdRYtBZC/QENFv0DDTW0FEYJGkgEBUZCEFxYlJpnEMSpUxpBNAkiT++rlb+uvNOpuOcz3Pt+j3vgeN8PkRYtWv5Z2qmb0d58kXl7ZXuFzM3W6E3jybfUW+8E6ZupaaXB3ZNnPGPnlAbZruF02ebTuRRSSOds89TVaE0bWYJiEhIjiaBIFjZpKKaF1TSePknDuUamRmo6dKPRzCNKRDO6UepQW9NCAxseCXHGlHvKzZ8SNjw0wN6oSqfFIWXvwSE72YsrKWtxkEHdsQ/5hRjuCpCNbMVVDEdXNKzmGhhnlqT8DYrwoq+1lJ9ZIqNyu0aERAhXn/Cir3UIQoJGlJpndm2KuPyGF5V2IlxbyszTmybi7xcowYvK9/H3/sn65hXsEnBeBi8q3wuKzGN2PeQCKIcff+Xkoa55zK4zMYCTCubcs+7KSQBn3DzdL3Ytrt3iuIpXRvXsFs516vnFruuMH8oI/Whewa4gDmsY8435aqfBH81jdoWzXtTi8Dm8cvOwrHkFu/zwyJDBi+yc/aCMecyuUH4f6rjOTy9Xm9cXiRxgTyX7iESor7LIQENk5XdYFVb2lYG0aNHyF/MB+x5LQiE6gt8AAAAASUVORK5CYII\x3d"/\x3e\x3celement name\x3d"playIcon" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACQAAAA0CAQAAABI31KIAAABHUlEQVR4Ae2Vu0oDQRRAB2xSWVmmtQncLzFREUUsnW/wJ0SCWgQV8TUQBBEsjlgIFoJFCsFCCT5QgwZFtPGtncUWIcTZnd2pAnNOf2Bn5t5VgUCge8mpPtWrevxD+cbi1KTq948VXvjlbMM/Jk2aPPPjHZM7Ip88Y3JLy0e+M8fkmnYfMsbkkk7v+Uodkzr/2+AzVUxOsXvDh3NMToj3inenmByT7AVviTGp4WadV85XK0WVs4SOcHd3rVyyhg5xc91M6NhPOyDZFTOuEw97n3iXzZh2uv497C6YUe38ILFQMSM61Yjs0Om8Gdaph3abdmfNkM60RrZoWTaDOvNi2yRyxpQsETcKVapMm6JHJCI/tzTgEfH4QXYxgUDgD+1pwmmFlV3oAAAAAElFTkSuQmCC"/\x3e\x3celement name\x3d"playIconOver" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACQAAAA0CAQAAABI31KIAAABHklEQVR4Ae2VvUpDQRBGt7BMaekD5AEsU0zvL6KI76CdL6FDUItgIYJNEERIoVgIFoKFhWChBBNRYwwZRBv/tfostgghuXf37lSBPac/cHd35ppIJDK45MyIGTZDRk2+UVteNaP6WOEVf7hu62PUQgsv+FXHqAnrszJGD+go+AmO0R26bQfGqI5en/CdOUZV9LeBr0wxukKy9/j0jtEl0r3Fh1eMLuC2hndnjM7hZxVvuHksLZpcQugM/h42i0uJoVP4uSMLnPppJ3C7LfPsPOxjpLslc+x1/UdIdlNm2ftBHqC/JZnhTCNSQa8bMs2Zh3Yf3a7JFAetkT10LMokBy+2XVhZJgIjlkIZZazIuCJiya/Xx9QR/Q8yEokMFv9/Ax7UXjl24wAAAABJRU5ErkJggg\x3d\x3d"/\x3e\x3celement name\x3d"replayIcon" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACQAAAA0CAQAAABI31KIAAADOElEQVR4Ae2VUWhbVRjH/0nqdk0m0eTGITVZNsmiZCLTlooNPoWlbk27lzmGSIeyh7YgFSYaGO2yDZk4GMi65kG9d6kkbfCuyf1bqZmmlsYxCK51KwxkrpM4qBRla18cIngvw0qgN7ea1/z+L4fDn4/vO+c730G9NGjQQIALj8CKumn+afjIQWyDHRbUxTO/8w/Ojux9Bc0Q6gn27B3eoRZM5Zm2l7EVm/5bMAsEiPAjiFiFun7hXa5MjJ7Y1gI3mjYaxA5vZzSdmJeWlfvqz/xHFd7jr5+fP+rYgU0wpQlibE8peV+9yyVWeJuLVapwleU4tsCEh9B8sn8lt8SbBprJvHUEXrOMmuCVj61o9h81fXEhEY/GHAf09QOVlaF3N4fgNDsjCzxnBn7jDU3T2TfexE64IeC5G9Q1lz/7/vY2iBs5aHtndCm/wAXmUtvb8ShsD/pogdf46bm2CJ7Qr16THY87t0Iwzsf77ch1/sBCdmcYjrVuaZ4813UAPjwMC3SXsztS+ujqWTxp1E9CV8ct9Sq/56EeOGGpemtb1t6a9bXdq7nbvKV2dRjlJKaOl1lm+gICsME47x1jsu5LHYeIdfEXpCu8wsE43KiFezCu+woS/FiX4KxSYon7YhBQC2FfTPfNKghiXUIldYYzdLfChlpYxRbd952KkEGgr9Uii3z6JbNAnhbd941hoOBF5RIv8WC3SWmbuzt130XD0vyfSFOc4gfvwIVauD48qvs+Njxs8URikpOckmtevw2Br2Tdd9Lw+oVIR15VeZl91Q1Z3UXOvp7LVJlXI4YNaYHvdHKCE7ye3fXvE6l2OHaFr43rntNJ+IxHrj0czeQVFjifCrbDCRuqi3IG2+dTBSrM5MNR2GuOkcMD48xymotZrcAAXBBghQ0C3Aj09Sxmp5nlOA8PwAOLyWDrPZbhGL/kMufkkff2xx5rferFQ/vPx+fkZW13jBn2D8KrOc1H7av9ci7NNIu8yVX+xT95T1sVqe/J+dffhldzYUPD/4U9Q8lR9TNWa5RDyeej8BhkY/Qd7Y72Jk5Jw4qkSuqwckrqTbTuhc/44zb/IEOagtpK/N8fdoMGDf4G6kd7103/csoAAAAASUVORK5CYII\x3d"/\x3e\x3celement name\x3d"replayIconOver" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACQAAAA0CAQAAABI31KIAAADTElEQVR4Ae2VX2xTZRjGH1iBzDMrU6lxLdOFhLJ/CepwTWCJiUSTDTdilikxJmAo2GlJ9I7EsCgkw6jRG5ALtZNJy7QDiwxK0dZllSypssqatCHIMKdzM4uEnUUrtj2P57uAULNzOtltf8/Nl3OevHnf73u/70WJxVKiRAWqcD/KsGjsvyScb6EBZizFoth4nX9zJNn6KtZCwhLcNU9NcpJasPw3o80vogbl/y/YUkiwoRHNcMsUSvMGlX/6zz3SCiuWLzSIGXVbnN5gXJ7566b6K29J5ix///PwMWk9ylGUZVj93M5o6qZ6g9OUeY0TBZI5x9ggKlGEFbDvP6Jkp3lFR8PX93yEOpQXy6a2L6Bo9suaTv/2tv/ZPdLey7ylWKZnYEULLFhWbG+q3/f8waSmiPLKB3gSVkh4OkmhsdyHkZoO2Bay0eYtzulcggl+PVXTiYdggmBjgpf42XjzDqwRRy+OAo/eVwNJP5+675Pj/JkhZW0XVt7uFvvQePte1ONezSFclo4d0fjFH7FOr9Ol9l1X1Yv8idt6Ybmj6SRUofL2XSt76Zm57DVeVdt36eVkO3o2xhi9k9gAE/TzXn88LXxHz8KGeWkMyaMc5T4/rDDCus8vfCEZjZgXx0gmyijb3JBghNTmFr6RDByYl5ZofpjDfKANJhhR9mCr8P2QR4tOoG/zYYa57vligVa1Ct93uoEcJzLneZ4vvIEKGHFPx+vCd0K3tMZP5SCDfNeLKhjx8HvHhO8T3c22vRMc4hCDaTQZFGdC07m08O3XPX5p8+6AeooX2F3QkAUsgaW79wJPMaBu3g1Jr9XqD6ZO8iTHlYY7rkhBmJUNXZdmhedgCvX6w8C8yenLDTLE+JS9ExaY/lOUxd4ZnwpxkL7cJifMhs/Ids8Av2SEE4pWYBOqIKEMJlTAiqbu3gklov0d4HYPqo2H03LUugI+HucZznAs/fFXW92VbWu2bnvzsH8sPcMz2h8fXzuNWs1Z/KntOtKX9dLLMK9wjnlmOautwhTf+nIvf446zYUFPf5P7OxJ9atfsFD97Ek97kS1TjZ64+gxpyt4QD6U8age9VDmgOwKbnChXn9wFxuQDrRocmir1ai4y+lfokSJfwEhAcqxd5L4JgAAAABJRU5ErkJggg\x3d\x3d"/\x3e\x3c/elements\x3e\x3c/component\x3e\x3ccomponent name\x3d"dock"\x3e\x3csettings\x3e\x3csetting name\x3d"iconalpha" value\x3d"1"/\x3e\x3csetting name\x3d"iconalphaactive" value\x3d"1"/\x3e\x3csetting name\x3d"iconalphaover" value\x3d"1"/\x3e\x3c/settings\x3e\x3celements\x3e\x3celement name\x3d"button" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAgCAYAAABpRpp6AAAAxklEQVR4Ae2YsQ3CMBBF7+yIximQSERSMgYNI1AxJgswAaMkLREpEnQ2Z6Chooqwpf+k65+evhtzXW8LIjrp7fUcpcmod9U7v2Sbpjm2bVtaa5kSRERC13V13/ePIpatqk05zzOHEChFWImOKnyIwk7EMyXMJyTrOUOZAeGlKd4byUtYCZjEN9gwCuPRYRKYBCbx18JLJ0bh3IQJk/gFHh0Ko3BWwqOID8YYpoTx3ofoap0r18y0WymspCo7DLf7NE2X7L5bnyz7UgI6sO7WAAAAAElFTkSuQmCC"/\x3e\x3celement name\x3d"buttonOver" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAgCAYAAABpRpp6AAAAzklEQVR4Ae2YMU7FMBAFx04osQvyRQIX4nfcgRZOAxW3oMqRkhKbBkWyjVfiCiD7a0dKPxq9dZHxdLq9Al6AB8DRJl/ACryOwPM8z0/LsvhhGCwNklLK27bd7fv+LcLnabrxx3HYUgotYoyx4liFH0XYpZQtDfMb0orrSGeo8L8Il9Jd4dL5JFRYN6xHp5PQSegkLuwd/uPEWrg3YXQSenRaWAtfVOGYUs62QsPkiriK8Brj571z3ot0q7IxhgB8iPBbCMHU7wxcN/679f0HQzRYj4Eg/3AAAAAASUVORK5CYII\x3d"/\x3e\x3celement name\x3d"buttonActive" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAgCAYAAABpRpp6AAAAwUlEQVR4Ae2YsQ3CMBBFD8e0CVESUcFMpGMKapgAKvagymKWiF3RxMe/IUDn6J70I5dPX98u4odhvyWiG3JCdqSTiEzI3eNz7fv+0nVdW1WVI4VkEEI4IB8RHjXLCg6II4TPXmbgADOTZhwQV0+F4ekPmDBzcQ2zTcKEC9+wXTqbhE3CJrGyd5jpp1jDxb0SNgm7dNawNbyqhudlydkBUkwG4irCU0rzsa6bVqt0BinFN44vEX7EGDfIiHOj/Hfr8wvCZ0/Xf6TpeQAAAABJRU5ErkJggg\x3d\x3d"/\x3e\x3celement name\x3d"divider" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAQAAAAgCAYAAAA1zNleAAAAD0lEQVQoU2NgGAWjADcAAAIgAAEeEYatAAAAAElFTkSuQmCC"/\x3e\x3c/elements\x3e\x3c/component\x3e\x3ccomponent name\x3d"playlist"\x3e\x3csettings\x3e\x3csetting name\x3d"backgroundcolor" value\x3d"0x3c3c3e"/\x3e\x3csetting name\x3d"fontcolor" value\x3d"0x848489"/\x3e\x3csetting name\x3d"fontsize" value\x3d"11"/\x3e\x3csetting name\x3d"fontweight" value\x3d"normal"/\x3e\x3csetting name\x3d"activecolor" value\x3d"0xb2b2b6"/\x3e\x3csetting name\x3d"overcolor" value\x3d"0xb2b2b6"/\x3e\x3csetting name\x3d"titlecolor" value\x3d"0xb9b9be"/\x3e\x3csetting name\x3d"titlesize" value\x3d"12"/\x3e\x3csetting name\x3d"titleweight" value\x3d"bold"/\x3e\x3csetting name\x3d"titleactivecolor" value\x3d"0xececf4"/\x3e\x3csetting name\x3d"titleovercolor" value\x3d"0xececf4"/\x3e\x3c/settings\x3e\x3celements\x3e\x3celement name\x3d"item" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAABMAQMAAAASt2oTAAAAA1BMVEU8PD44mUV6AAAAFklEQVR4AWMYMmAUjIJRMApGwSgYBQAHuAABIqNCjAAAAABJRU5ErkJggg\x3d\x3d"/\x3e\x3celement name\x3d"itemActive" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAABMAQMAAAASt2oTAAAAA1BMVEUvLzHXqQRQAAAAFklEQVR4AWMYMmAUjIJRMApGwSgYBQAHuAABIqNCjAAAAABJRU5ErkJggg\x3d\x3d"/\x3e\x3celement name\x3d"itemImage" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAAA2CAMAAAAPkWzgAAAAk1BMVEU0NDcVFRcWFhgXFxknJyozMzYyMjUlJSgrKy4jIyYZGRssLC8YGBobGx0kJCcuLjAiIiQaGhwjIyUpKSwkJCYaGh0nJykiIiUgICIwMDMqKi0cHB8lJScdHSAtLTAuLjEdHR8VFRgxMTQvLzIvLzEoKCsZGRwqKiwbGx4gICMoKCofHyImJigmJikhISMeHiAhISRWJqoOAAAA/klEQVR4Ae3VNYLDMBQG4X8kme2QwwzLfP/TbeO0qfQ6zQW+coRxQqYl4HEJSEACEvA8NQamRkCoF40kNUxMgC3gc0lrtiZAB1BKuSOPDIzcXroB0EtL3hQXuIHLNboDC+aRgRnQ6GUAjtBEBmrgdcwA/OCyuMATraOvBiB3HBQTOJ8KZp5QwwXoA3xFBdrVjpPnHVgBfQfjqMChZSoAugDMwCsqUMFeAHwEwMFnXKDkshGAz5YAEOIC2fpbAqhUAMDG4AcO3HUAahkAHYykOQATC6Bsf7M7UNotswLwmR2wAviTHVAAHA2BMXCWIaDC7642wIMSkIAEJCABxv0D1B4Kmtm5dvAAAAAASUVORK5CYII\x3d"/\x3e\x3celement name\x3d"divider" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAANIAAAABCAIAAAAkUWeUAAAAEUlEQVR42mPQ1zccRaOIzggAmuR1T+nadMkAAAAASUVORK5CYII\x3d"/\x3e\x3celement name\x3d"sliderRail" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAABCAYAAADErm6rAAAAHklEQVQI12NgIABERcX/Kymp/FdWVkXBIDGQHCH9AAmVCvfMHD66AAAAAElFTkSuQmCC"/\x3e\x3celement name\x3d"sliderCapTop" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAKCAYAAACuaZ5oAAAAEUlEQVQoU2NgGAWjYBQMfQAAA8oAAZphnjsAAAAASUVORK5CYII\x3d"/\x3e\x3celement name\x3d"sliderCapBottom" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAKCAYAAACuaZ5oAAAAEUlEQVQoU2NgGAWjYBQMfQAAA8oAAZphnjsAAAAASUVORK5CYII\x3d"/\x3e\x3celement name\x3d"sliderRailCapTop" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAECAYAAACUY/8YAAAAX0lEQVR42q2P4QqAIAyEewktLUy3pKevVwvpAdZO+q9Qgw+OO25jQ88YM2blUAp4dW71epfvyuXcLCGsFWh4yD4fsHY6vV8kRpKUGFQND9kfHxQsJNqEOYOq4Wl2t/oPXdoiX8vd60IAAAAASUVORK5CYII\x3d"/\x3e\x3celement name\x3d"sliderRailCapBottom" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAECAYAAACUY/8YAAAAXElEQVQY02NgIADExCQ+KSmp/FdWVkXBIDGg3BcGSoG0tMxGWVl5DAtAYiA5ii2wsbE1ALr0A8hAkKtBGMQGiYHkKLbg////TK6uboYg1wIN/QzCIDZIDCRHSD8AB2YrZ5n2CLAAAAAASUVORK5CYII\x3d"/\x3e\x3celement name\x3d"sliderThumb" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAABCAAAAADhxTF3AAAAAnRSTlMA/1uRIrUAAAAUSURBVHjaY/oPA49unT+yaz2cCwAcKhapymVMMwAAAABJRU5ErkJggg\x3d\x3d"/\x3e\x3celement name\x3d"sliderThumbCapBottom" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAECAQAAAA+ajeTAAAAMElEQVQI12NgwACPPt76f/7/kf+7/q//yEAMeNQH19DHQBy41Xf+/ZH3u4hVjh8AAJAYGojU8tLHAAAAAElFTkSuQmCC"/\x3e\x3celement name\x3d"sliderThumbCapTop" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAECAQAAAA+ajeTAAAANUlEQVQI12NgoAbY2rf+49KPs/uIVH54wrH/h/7v+L/y//QJRGm4/PHa/7NALdv+L/6MKQsAZV8ZczFGWjAAAAAASUVORK5CYII\x3d"/\x3e\x3c/elements\x3e\x3c/component\x3e\x3ccomponent name\x3d"tooltip"\x3e\x3csettings\x3e\x3csetting name\x3d"fontcase" value\x3d"normal"/\x3e\x3csetting name\x3d"fontcolor" value\x3d"0xacacac"/\x3e\x3csetting name\x3d"fontsize" value\x3d"11"/\x3e\x3csetting name\x3d"fontweight" value\x3d"normal"/\x3e\x3csetting name\x3d"activecolor" value\x3d"0xffffff"/\x3e\x3csetting name\x3d"overcolor" value\x3d"0xffffff"/\x3e\x3c/settings\x3e\x3celements\x3e\x3celement name\x3d"background" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAwAAAACCAYAAABsfz2XAAAAEUlEQVR4AWOwtnV8RgomWQMAWvcm6W7AcF8AAAAASUVORK5CYII\x3d"/\x3e\x3celement name\x3d"arrow" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAwAAAADCAYAAACnI+4yAAAAEklEQVR42mP4//8/AymYgeYaABssa5WUTzsyAAAAAElFTkSuQmCC"/\x3e\x3celement name\x3d"capTop" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAwAAAAECAYAAAC6Jt6KAAAAHUlEQVR42mMUFRU/wUACYHR1935GkgZrW0faagAAqHQGCWgiU9QAAAAASUVORK5CYII\x3d"/\x3e\x3celement name\x3d"capBottom" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAwAAAAECAYAAAC6Jt6KAAAAGElEQVR42mOwtnV8RgpmoL0GUVHxE6RgAO7IRsl4Cw8cAAAAAElFTkSuQmCC"/\x3e\x3celement name\x3d"capLeft" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAcAAAACCAYAAACUn8ZgAAAAFklEQVR42mMQFRU/YW3r+AwbZsAnCQBUPRWHq8l/fAAAAABJRU5ErkJggg\x3d\x3d"/\x3e\x3celement name\x3d"capRight" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAcAAAACCAYAAACUn8ZgAAAAFklEQVR42mOwtnV8hg2LioqfYMAnCQBwXRWHw2Rr1wAAAABJRU5ErkJggg\x3d\x3d"/\x3e\x3celement name\x3d"capTopLeft" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAcAAAAECAYAAABCxiV9AAAAPklEQVR4XmMQFRVnBeIiIN4FxCeQMQOQU6ijq3/VycXjiau79zNkDJLcZWvv9MTGzumZta0jCgZJnkAXhPEBnhkmTDF7/FAAAAAASUVORK5CYII\x3d"/\x3e\x3celement name\x3d"capTopRight" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAcAAAAECAYAAABCxiV9AAAAPklEQVR42mMQFRU/gYZ3A3ERELMyuLp7P0PGTi4eT3R09a8CJbMYrG0dnyFjGzunZ7b2Tk+AkrswJGEYZAUA8XwmRnLnEVMAAAAASUVORK5CYII\x3d"/\x3e\x3celement name\x3d"capBottomLeft" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAcAAAAECAYAAABCxiV9AAAAMUlEQVR4AWMQFRU/YW3r+AwbBknusrSye4JLslBdQ/uqpbX9E2ySrEBcBMS7QVYgYwAWViWcql/T2AAAAABJRU5ErkJggg\x3d\x3d"/\x3e\x3celement name\x3d"capBottomRight" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAcAAAAECAYAAABCxiV9AAAANUlEQVR42mOwtnV8hg2LioqfYMAmYWll9wQouQtD0tLa/om6hvZVoGQ2A0g7Gt4NxEVAzAoAZzolltlSH50AAAAASUVORK5CYII\x3d"/\x3e\x3celement name\x3d"menuOption" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAARCAYAAADkIz3lAAAAcklEQVQoz2NgGLFAVFRcDoh3AfFnKC2HVaGYmMQeSUnp/7Kycv9BNJB/AJeJn+XlFf8rKir/V1BQ+g/k/8SqEGjKPhkZuf/Kyqr/QTSQfwirQm9vX3WQYqCVX0G0p6e3BlaF////ZwJiLiDmgdJMwzr2ANEWKw6VGUzBAAAAAElFTkSuQmCC"/\x3e\x3celement name\x3d"menuOptionOver" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAARCAYAAADkIz3lAAAAcklEQVQoz2NgGLFAVFRcDoh3AfFnKC2HVaGYmMQeSUnp/7Kycv9BNJB/AJeJn+XlFf8rKir/V1BQ+g/k/8SqEGjKPhkZuf/Kyqr/QTSQfwirQm9vX3WQYqCVX0G0p6e3BlaF////ZwJiLiDmgdJMwzr2ANEWKw6VGUzBAAAAAElFTkSuQmCC"/\x3e\x3celement name\x3d"menuOptionActive" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAARCAQAAABOKvVuAAAAdElEQVR4AWOgJ5BhcGQIBWIZhJCsW+6jS7+/P7rklssgBxN0un/59f+n/1//f3SVwQUmGPrs+6P/IPj8N0M4TNBl/+Vr/0Hw4FUGN5igkm3ursvnf+y6bJ/LoAwTZGZQY/BgCANiNSCbASHMwcANxMy09DcAxqMsxkMxUYIAAAAASUVORK5CYII\x3d"/\x3e\x3celement name\x3d"volumeCapTop" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA4AAAAFCAYAAAB1j90SAAAAE0lEQVR42mP4//8/AzmYYQRoBADgm9EvDrkmuwAAAABJRU5ErkJggg\x3d\x3d"/\x3e\x3celement name\x3d"volumeCapBottom" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA4AAAAFCAYAAAB1j90SAAAAE0lEQVR42mP4//8/AzmYYQRoBADgm9EvDrkmuwAAAABJRU5ErkJggg\x3d\x3d"/\x3e\x3celement name\x3d"volumeRailCapTop" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA4AAAAECAYAAAC+0w63AAAAXklEQVR42n2NWwqAIBRE3YSmJT4KafW1tZAWMN2RPkSojwPDPO5VAFSP1lMRDqG+UJexN4524bJ2hvehQU2P2efQGHs6tyCEhBhzg5oes7+PlcWUVuS8Nah5QLK77z7Bcm/CZuJM1AAAAABJRU5ErkJggg\x3d\x3d"/\x3e\x3celement name\x3d"volumeRailCapBottom" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA4AAAAECAYAAAC+0w63AAAAWklEQVQI12NgQAJiYhKfVFXV/6upaaBgkBhQ7gsDLiAtLbNRXl4RQyNIDCSHU6ONja0B0OQPIIUgW0AYxAaJgeRwavz//z+Tq6ubIch0oOLPIAxig8RAcshqARVfK+sjJ8UzAAAAAElFTkSuQmCC"/\x3e\x3celement name\x3d"volumeRail" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA4AAAA0CAYAAAC6qQkaAAAAXklEQVR42mP5//8/AwyIiUn85+bmZmBkZGRABiA1X79+ZXj16gVcgoUBDaBrwiWGoZFYMCg0MpKnkZFxCPlxVONw0MjIyDgaOCM7AdC7lBuNjtGiY1TjqMbRwooijQBUhw3jnmCdzgAAAABJRU5ErkJggg\x3d\x3d"/\x3e\x3celement name\x3d"volumeProgress" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA4AAAA0CAAAAACfwlbGAAAAAnRSTlMA/1uRIrUAAAAmSURBVHgBY/gPBPdunT+yaw2IBeY+BHHXwbmPQNz1w5w7yh3lAgBeJpPWLirUWgAAAABJRU5ErkJggg\x3d\x3d"/\x3e\x3celement name\x3d"volumeProgressCapTop" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA4AAAAECAQAAAAU2sY8AAAANElEQVQI12NgIA5s7Vv/cenH2X1YpA5POPb/0P8d/1f+nz4BQ/Lyx2v/zwKlt/1f/BkmBgDJshlzy7m4BgAAAABJRU5ErkJggg\x3d\x3d"/\x3e\x3celement name\x3d"volumeProgressCapBottom" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA4AAAAECAQAAAAU2sY8AAAAL0lEQVQI12NggIJHH2/9P///yP9d/9d/ZkAHjybCJScyYIJbE85/OvJp1wQG4gAADBkams/Cpm0AAAAASUVORK5CYII\x3d"/\x3e\x3celement name\x3d"volumeThumb" src\x3d"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA4AAAAQCAQAAACMnYaxAAAA/klEQVR4AYXQoW7CUBjF8f9IYWkgq2l2k8llrmJBTOBxsyQlJENs4236CDhEywNUIEGh12WZuYDC4W9A3B2zhTVLds8VJ+fnPv5/FzQIaHGptNQaWn4ooM0DA56VgVpbi1hEk2vSvNjbozu6vc0LUi1NCQFXDBflwW/9p7L1B78oGRJJCOnN8o3/OMvGz3J6EiLStdX0K2tLKiFm8n6qY3XiVYL5C98cLxL90dLWcWkZSYjpZ0Uds4K+hIg7nqblOU1LxlojCDF0GWfz1a5ylVvtsrmoi5EQ0OGGhEdNE2WslmjpSND5VAy3mu6VRM1o0fm+Dx8SEWOUWC3UIvoCCFqphCwr/x8AAAAASUVORK5CYII\x3d"/\x3e\x3c/elements\x3e\x3c/component\x3e\x3c/components\x3e\x3c/skin\x3e')}})(jwplayer);
(function(d){var l=d.html5,a=d.utils,c=d.events,f=c.state,e=a.css,h=a.isMobile(),j=".jwpreview",b={showicons:!0,bufferrotation:45,bufferinterval:100,fontcolor:"#ccc",overcolor:"#fff",fontsize:15,fontweight:""};l.display=function(d,g){function k(b){if(D&&(d.jwGetControls()||d.jwGetState()===f.PLAYING))D(b);else if((!h||!d.jwGetControls())&&T.sendEvent(c.JWPLAYER_DISPLAY_CLICK),d.jwGetControls()){var e=(new Date).getTime();W&&500>e-W?(d.jwSetFullscreen(),W=void 0):W=(new Date).getTime();var g=a.bounds(u.parentNode.querySelector(".jwcontrolbar")),
    j=a.bounds(u),e=g.left-10-j.left,r=g.left+30-j.left,k=j.bottom-40,l=j.bottom,p=g.right-30-j.left,g=g.right+10-j.left;if(h&&!(b.x>=e&&b.x<=r&&b.y>=k&&b.y<=l)){if(b.x>=p&&b.x<=g&&b.y>=k&&b.y<=l){d.jwSetFullscreen();return}T.sendEvent(c.JWPLAYER_DISPLAY_CLICK);if(J)return}switch(d.jwGetState()){case f.PLAYING:case f.BUFFERING:d.jwPause();break;default:d.jwPlay()}}}function q(a,b){S.showicons&&(a||b?(K.setRotation("buffer"===a?parseInt(S.bufferrotation,10):0,parseInt(S.bufferinterval,10)),K.setIcon(a),
    K.setText(b)):K.hide())}function s(a){A!==a?(A&&E(j,!1),(A=a)?(a=new Image,a.addEventListener("load",r,!1),a.src=A):(e("#"+u.id+" "+j,{"background-image":""}),E(j,!1),F=L=0)):A&&!J&&E(j,!0);C(d.jwGetState())}function v(a){clearTimeout(Z);Z=setTimeout(function(){C(a.newstate)},100)}function C(a){a=N?N:d?d.jwGetState():f.IDLE;if(a!==P)switch(P=a,K&&K.setRotation(0),a){case f.IDLE:!x&&!M&&(A&&!G&&E(j,!0),a=!0,d._model&&!1===d._model.config.displaytitle&&(a=!1),q("play",w&&a?w.title:""));break;case f.BUFFERING:x=
    !1;t.error&&t.error.setText();M=!1;q("buffer");break;case f.PLAYING:q();break;case f.PAUSED:q("play")}}function r(){F=this.width;L=this.height;C(d.jwGetState());y();A&&e("#"+u.id+" "+j,{"background-image":"url("+A+")"})}function n(a){x=!0;q("error",a.message)}function y(){0<u.clientWidth*u.clientHeight&&a.stretch(d.jwGetStretching(),p,u.clientWidth,u.clientHeight,F,L)}function E(a,b){e("#"+u.id+" "+a,{opacity:b?1:0,visibility:b?"visible":"hidden"})}var u,p,z,w,A,F,L,G=!1,t={},x=!1,M=!1,J,H,K,N,P,
    S=a.extend({},b,d.skin.getComponentSettings("display"),g),T=new c.eventdispatcher,D,W;a.extend(this,T);this.clickHandler=k;var Z;this.forceState=function(a){N=a;C(a);this.show()};this.releaseState=function(a){N=null;C(a);this.show()};this.hidePreview=function(a){G=a;E(j,!a);a&&(J=!0)};this.setHiding=function(){J=!0};this.element=function(){return u};this.redraw=y;this.show=function(a){if(K&&(a||(N?N:d?d.jwGetState():f.IDLE)!==f.PLAYING))clearTimeout(H),H=void 0,u.style.display="block",K.show(),J=
    !1};this.hide=function(){K&&(K.hide(),J=!0)};this.setAlternateClickHandler=function(a){D=a};this.revertAlternateClickHandler=function(){D=null};u=document.createElement("div");u.id=d.id+"_display";u.className="jwdisplay";p=document.createElement("div");p.className="jwpreview jw"+d.jwGetStretching();u.appendChild(p);d.jwAddEventListener(c.JWPLAYER_PLAYER_STATE,v);d.jwAddEventListener(c.JWPLAYER_PLAYLIST_ITEM,function(){x=!1;t.error&&t.error.setText();var a=(w=d.jwGetPlaylist()[d.jwGetPlaylistIndex()])?
    w.image:"";P=void 0;s(a)});d.jwAddEventListener(c.JWPLAYER_PLAYLIST_COMPLETE,function(){M=!0;q("replay");var a=d.jwGetPlaylist()[0];s(a.image)});d.jwAddEventListener(c.JWPLAYER_MEDIA_ERROR,n);d.jwAddEventListener(c.JWPLAYER_ERROR,n);d.jwAddEventListener(c.JWPLAYER_PROVIDER_CLICK,k);h?(z=new a.touch(u),z.addEventListener(a.touchEvents.TAP,k)):u.addEventListener("click",k,!1);z={font:S.fontweight+" "+S.fontsize+"px/"+(parseInt(S.fontsize,10)+3)+"px Arial, Helvetica, sans-serif",color:S.fontcolor};K=
    new l.displayicon(u.id+"_button",d,z,{color:S.overcolor});u.appendChild(K.element());v({newstate:f.IDLE})};e(".jwdisplay",{position:"absolute",width:"100%",height:"100%",overflow:"hidden"});e(".jwdisplay "+j,{position:"absolute",width:"100%",height:"100%",background:"#000 no-repeat center",overflow:"hidden",opacity:0});a.transitionStyle(".jwdisplay, .jwdisplay *","opacity .25s, color .25s")})(jwplayer);
(function(d){var l=d.utils,a=l.css,c=document,f="none",e="100%";d.html5.displayicon=function(h,j,b,m){function g(a,b,e,f){var d=c.createElement("div");d.className=a;b&&b.appendChild(d);y&&k(a,"."+a,e,f);return d}function k(b,c,e,f){var d=q(b);"replayIcon"===b&&!d.src&&(d=q("playIcon"));d.overSrc&&(f=l.extend({},f),f["background-image"]="url("+d.overSrc+")");d.src?(e=l.extend({},e),0<b.indexOf("Icon")&&(G=d.width|0),e.width=d.width,e["background-image"]="url("+d.src+")",e["background-size"]=d.width+
    "px "+d.height+"px",e["float"]="none",a.style(y,{display:"table"})):a.style(y,{display:"none"});e&&a("#"+n+" .jwdisplay "+c,e);f&&a("#"+n+" .jwdisplay:hover "+c,f);L=d}function q(a){var b=r.getSkinElement("display",a);a=r.getSkinElement("display",a+"Over");return b?(b.overSrc=a&&a.src?a.src:"",b):{src:"",overSrc:"",width:0,height:0}}function s(){var b=z||0===G;a.style(w,{display:w.innerHTML&&b?"":f});x=b?30:0;v()}function v(){clearTimeout(t);0<x--&&(t=setTimeout(v,33));var b="px "+e,c=Math.ceil(Math.max(L.width,
    l.bounds(y).width-p.width-u.width)),b={"background-size":[u.width+b,c+b,p.width+b].join(", ")};y.parentNode&&(b.left=1===y.parentNode.clientWidth%2?"0.5px":"");a.style(y,b)}function C(){H=(H+J)%360;l.rotate(A,H)}var r=j.skin,n=j.id,y,E,u,p,z,w,A,F={},L,G=0,t=-1,x=0;j instanceof d.html5.instream&&(n=n.replace("_instream",""));this.element=function(){return y};this.setText=function(a){var b=w.style;w.innerHTML=a?a.replace(":",":\x3cbr\x3e"):"";b.height="0";b.display="block";if(a)for(;2<Math.floor(w.scrollHeight/
    c.defaultView.getComputedStyle(w,null).lineHeight.replace("px",""));)w.innerHTML=w.innerHTML.replace(/(.*) .*$/,"$1...");b.height="";b.display="";s()};this.setIcon=function(a){var b=F[a];b||(b=g("jwicon"),b.id=y.id+"_"+a,F[a]=b);k(a+"Icon","#"+b.id);y.contains(A)?y.replaceChild(b,A):y.appendChild(b);A=b};var M,J=0,H;this.setRotation=function(a,b){clearInterval(M);H=0;J=a|0;0===J?C():M=setInterval(C,b)};var K=this.hide=function(){y.style.opacity=0;y.style.cursor=""};this.show=function(){y.style.opacity=
    1;y.style.cursor="pointer"};y=g("jwdisplayIcon");y.id=h;E=q("background");u=q("capLeft");p=q("capRight");z=0<u.width*p.width;var N={"background-image":"url("+u.src+"), url("+E.src+"), url("+p.src+")","background-position":"left,center,right","background-repeat":"no-repeat",padding:"0 "+p.width+"px 0 "+u.width+"px",height:E.height,"margin-top":E.height/-2};a("#"+h,N);l.isMobile()||(E.overSrc&&(N["background-image"]="url("+u.overSrc+"), url("+E.overSrc+"), url("+p.overSrc+")"),a(".jw-tab-focus #"+h+
    ", #"+n+" .jwdisplay:hover "+("#"+h),N));w=g("jwtext",y,b,m);A=g("jwicon",y);j.jwAddEventListener(d.events.JWPLAYER_RESIZE,v);K();s()};a(".jwplayer .jwdisplayIcon",{display:"table",position:"relative","margin-left":"auto","margin-right":"auto",top:"50%","float":"none"});a(".jwplayer .jwdisplayIcon div",{position:"relative",display:"table-cell","vertical-align":"middle","background-repeat":"no-repeat","background-position":"center"});a(".jwplayer .jwdisplayIcon div",{"vertical-align":"middle"},!0);
    a(".jwplayer .jwdisplayIcon .jwtext",{color:"#fff",padding:"0 1px","max-width":"300px","overflow-y":"hidden","text-align":"center","-webkit-user-select":f,"-moz-user-select":f,"-ms-user-select":f,"user-select":f})})(jwplayer);
(function(d){var l=d.html5,a=d.utils,c=a.css,f=a.bounds,e=window.top!==window.self,h=".jwdockbuttons";l.dock=function(d,b){function m(a){return!a||!a.src?{}:{background:"url("+a.src+") center","background-size":a.width+"px "+a.height+"px"}}function g(b,e){var d=s(b);c(k("."+b),a.extend(m(d),{width:d.width}));return q("div",b,e)}function k(a){return"#"+r+" "+(a?a:"")}function q(a,b,c){a=document.createElement(a);b&&(a.className=b);c&&c.appendChild(a);return a}function s(a){return(a=n.getSkinElement("dock",
    a))?a:{width:0,height:0,src:""}}function v(){c(h+" .capLeft, "+h+" .capRight",{display:y?"block":"none"})}var C=a.extend({},{iconalpha:0.75,iconalphaactive:0.5,iconalphaover:1,margin:8},b),r=d.id+"_dock",n=d.skin,y=0,E={},u={},p,z,w,A=this;A.redraw=function(){f(p)};A.element=function(){return p};A.offset=function(a){c(k(),{"margin-left":a})};A.hide=function(){A.visible&&(A.visible=!1,p.style.opacity=0,clearTimeout(w),w=setTimeout(function(){p.style.display="none"},250))};A.showTemp=function(){A.visible||
(p.style.opacity=0,p.style.display="block")};A.hideTemp=function(){A.visible||(p.style.display="none")};A.show=function(){!A.visible&&y&&(A.visible=!0,p.style.display="block",clearTimeout(w),w=setTimeout(function(){p.style.opacity=1},0))};A.addButton=function(b,g,h,k){if(!E[k]){var w=q("div","divider",z),m=q("div","button",z),s=q("div",null,m);s.id=r+"_"+k;s.innerHTML="\x26nbsp;";c("#"+s.id,{"background-image":b});"string"===typeof h&&(h=new Function(h));a.isMobile()?(new a.touch(m)).addEventListener(a.touchEvents.TAP,
    function(a){h(a)}):m.addEventListener("click",function(a){h(a);a.preventDefault()});E[k]={element:m,label:g,divider:w,icon:s};if(g){var C=new l.overlay(s.id+"_tooltip",n,!0);b=q("div");b.id=s.id+"_label";b.innerHTML=g;c("#"+b.id,{padding:3});C.setContents(b);if(!a.isMobile()){var L;m.addEventListener("mouseover",function(){clearTimeout(L);var b=u[k],g,h;g=f(E[k].icon);b.offsetX(0);h=f(p);e&&a.isIE()&&d.jwGetFullscreen()?c("#"+b.element().id,{left:100*g.left+50+100*g.width/2}):c("#"+b.element().id,
    {left:g.left-h.left+g.width/2});g=f(b.element());h.left>g.left&&b.offsetX(h.left-g.left+8);C.show();a.foreach(u,function(a,b){a!==k&&b.hide()})},!1);m.addEventListener("mouseout",function(){L=setTimeout(C.hide,100)},!1);p.appendChild(C.element());u[k]=C}}y++;v()}};A.removeButton=function(a){if(E[a]){z.removeChild(E[a].element);z.removeChild(E[a].divider);var b=document.getElementById(""+r+"_"+a+"_tooltip");b&&p.removeChild(b);delete E[a];y--;v()}};A.numButtons=function(){return y};A.visible=!1;p=
    q("div","jwdock");z=q("div","jwdockbuttons");p.appendChild(z);p.id=r;var F=s("button"),L=s("buttonOver"),G=s("buttonActive");F&&(c(k(),{height:F.height,padding:C.margin}),c(h,{height:F.height}),c(k("div.button"),a.extend(m(F),{width:F.width,cursor:"pointer",border:"none"})),c(k("div.button:hover"),m(L)),c(k("div.button:active"),m(G)),c(k("div.button\x3ediv"),{opacity:C.iconalpha}),c(k("div.button:hover\x3ediv"),{opacity:C.iconalphaover}),c(k("div.button:active\x3ediv"),{opacity:C.iconalphaactive}),
    c(k(".jwoverlay"),{top:C.margin+F.height}),g("capLeft",z),g("capRight",z),g("divider"));setTimeout(function(){f(p)})};c(".jwdock",{opacity:0,display:"none"});c(".jwdock \x3e *",{height:"100%","float":"left"});c(".jwdock \x3e .jwoverlay",{height:"auto","float":"none","z-index":99});c(h+" div.button",{position:"relative"});c(h+" \x3e *",{height:"100%","float":"left"});c(h+" .divider",{display:"none"});c(h+" div.button ~ .divider",{display:"block"});c(h+" .capLeft, "+h+" .capRight",{display:"none"});
    c(h+" .capRight",{"float":"right"});c(h+" div.button \x3e div",{left:0,right:0,top:0,bottom:0,margin:5,position:"absolute","background-position":"center","background-repeat":"no-repeat"});a.transitionStyle(".jwdock","background .25s, opacity .25s");a.transitionStyle(".jwdock .jwoverlay","opacity .25s");a.transitionStyle(h+" div.button div","opacity .25s")})(jwplayer);
(function(d){var l=d.html5,a=d.utils,c=d._,f=d.events,e=f.state,h=d.playlist;l.instream=function(j,b,m,g){function k(a){E(a.type,a);H&&j.jwInstreamDestroy(!1,D)}function q(a){if(a.newstate!==H.state)switch(H.state=a.newstate,H.state){case e.PLAYING:D.jwInstreamPlay();break;case e.PAUSED:D.jwInstreamPause()}}function s(a){E(a.type,a);n()}function v(a){E(a.type,a)}function C(a){b.sendEvent(a.type,a);E(f.JWPLAYER_FULLSCREEN,{fullscreen:a.jwstate})}function r(){P&&P.releaseState(D.jwGetState());K.play()}
    function n(){if(w&&A+1<w.length){A++;var b=w[A];z=new h.item(b);H.setPlaylist([b]);var c;F&&(c=F[A]);L=a.extend(p,c);K.load(H.playlist[0]);G.reset(L.skipoffset||-1);T=setTimeout(function(){E(f.JWPLAYER_PLAYLIST_ITEM,{index:A},!0)},0)}else T=setTimeout(function(){E(f.JWPLAYER_PLAYLIST_COMPLETE,{},!0);j.jwInstreamDestroy(!0,D)},0)}function y(a){a.width&&a.height&&(P&&P.releaseState(D.jwGetState()),m.resizeMedia())}function E(a,b){b=b||{};p.tag&&!b.tag&&(b.tag=p.tag);D.sendEvent(a,b)}function u(){N&&
    N.redraw();P&&P.redraw()}var p={controlbarseekable:"never",controlbarpausable:!0,controlbarstoppable:!0,loadingmessage:"Loading ad",playlistclickable:!0,skipoffset:null,tag:null},z,w,A=0,F,L={controlbarseekable:"never",controlbarpausable:!1,controlbarstoppable:!1},G,t,x,M,J,H,K,N,P,S,T=-1,D=a.extend(this,new f.eventdispatcher);j.jwAddEventListener(f.JWPLAYER_RESIZE,u);j.jwAddEventListener(f.JWPLAYER_FULLSCREEN,function(b){v(b);H&&(u(),!b.fullscreen&&a.isIPad()&&(H.state===e.PAUSED?P.show(!0):H.state===
    e.PLAYING&&P.hide()))});D.init=function(){t=g.detachMedia();K=new (d.html5.chooseProvider({}))(b.id);K.addGlobalListener(v);K.addEventListener(f.JWPLAYER_MEDIA_META,y);K.addEventListener(f.JWPLAYER_MEDIA_COMPLETE,n);K.addEventListener(f.JWPLAYER_MEDIA_BUFFER_FULL,r);K.addEventListener(f.JWPLAYER_MEDIA_ERROR,k);K.addEventListener(f.JWPLAYER_PLAYER_STATE,q);K.addEventListener(f.JWPLAYER_MEDIA_TIME,function(a){G&&G.updateSkipTime(a.position,a.duration)});K.attachMedia();K.mute(b.mute);K.volume(b.volume);
        H=new l.model({},K);H.setVolume(b.volume);H.setFullscreen(b.fullscreen);H.setMute(b.mute);H.addEventListener("fullscreenchange",C);J=b.playlist[b.item];x=t.currentTime;g.checkBeforePlay()||0===x&&!b.getVideo().checkComplete()?(x=0,M=e.PLAYING):M=b.getVideo().checkComplete()?e.IDLE:j.jwGetState()===e.IDLE?e.IDLE:e.PLAYING;M===e.PLAYING&&t.pause();P=new l.display(D);P.forceState(e.BUFFERING);S=document.createElement("div");S.id=D.id+"_instream_container";a.css.style(S,{width:"100%",height:"100%"});
        S.appendChild(P.element());N=new l.controlbar(D,{fullscreen:b.fullscreen});N.instreamMode(!0);S.appendChild(N.element());j.jwGetControls()?(N.show(),P.show()):(N.hide(),P.hide());m.setupInstream(S,N,P,H);u();D.jwInstreamSetText(p.loadingmessage)};D.load=function(b,d){if(a.isAndroid(2.3))k({type:f.JWPLAYER_ERROR,message:"Error loading instream: Cannot play instream on Android 2.3"});else{E(f.JWPLAYER_PLAYLIST_ITEM,{index:A},!0);var g=10+a.bounds(S.parentNode).bottom-a.bounds(N.element()).top;c.isArray(b)&&
    (d&&(F=d,d=d[A]),w=b,b=w[A]);L=a.extend(p,d);z=new h.item(b);H.setPlaylist([b]);G=new l.adskipbutton(j.id,g,L.skipMessage,L.skipText);G.addEventListener(f.JWPLAYER_AD_SKIPPED,s);G.reset(L.skipoffset||-1);j.jwGetControls()?G.show():G.hide();g=G.element();S.appendChild(g);H.addEventListener(f.JWPLAYER_ERROR,k);P.setAlternateClickHandler(function(a){a=a||{};a.hasControls=!!j.jwGetControls();E(f.JWPLAYER_INSTREAM_CLICK,a);H.state===e.PAUSED?a.hasControls&&D.jwInstreamPlay():D.jwInstreamPause()});a.isMSIE()&&
    t.parentElement.addEventListener("click",P.clickHandler);m.addEventListener(f.JWPLAYER_AD_SKIPPED,s);K.load(H.playlist[0])}};D.jwInstreamDestroy=function(c){if(H){H.removeEventListener("fullscreenchange",C);clearTimeout(T);T=-1;K.detachMedia();g.attachMedia();if(M!==e.IDLE){var d=a.extend({},J);d.starttime=x;b.getVideo().load(d)}else b.getVideo().stop();D.resetEventListeners();K.resetEventListeners();H.resetEventListeners();if(N)try{N.element().parentNode.removeChild(N.element())}catch(h){}P&&(t&&
    t.parentElement&&t.parentElement.removeEventListener("click",P.clickHandler),P.revertAlternateClickHandler());E(f.JWPLAYER_INSTREAM_DESTROYED,{reason:c?"complete":"destroyed"},!0);M===e.PLAYING&&t.play();m.destroyInstream(K.isAudioFile());H=null}};D.jwInstreamAddEventListener=function(a,b){D.addEventListener(a,b)};D.jwInstreamRemoveEventListener=function(a,b){D.removeEventListener(a,b)};D.jwInstreamPlay=function(){K.play(!0);b.state=e.PLAYING;P.show()};D.jwInstreamPause=function(){K.pause(!0);b.state=
        e.PAUSED;j.jwGetControls()&&(P.show(),N.show())};D.jwInstreamSeek=function(a){K.seek(a)};D.jwInstreamSetText=function(a){N.setText(a)};D.jwInstreamState=function(){return H.state};D.setControls=function(a){a?G.show():G.hide()};D.jwPlay=function(){"true"===L.controlbarpausable.toString().toLowerCase()&&D.jwInstreamPlay()};D.jwPause=function(){"true"===L.controlbarpausable.toString().toLowerCase()&&D.jwInstreamPause()};D.jwStop=function(){"true"===L.controlbarstoppable.toString().toLowerCase()&&(j.jwInstreamDestroy(!1,
        D),j.jwStop())};D.jwSeek=function(a){switch(L.controlbarseekable.toLowerCase()){case "always":D.jwInstreamSeek(a);break;case "backwards":H.position>a&&D.jwInstreamSeek(a)}};D.jwSeekDrag=function(a){H.seekDrag(a)};D.jwGetPosition=function(){};D.jwGetDuration=function(){};D.jwGetWidth=j.jwGetWidth;D.jwGetHeight=j.jwGetHeight;D.jwGetFullscreen=j.jwGetFullscreen;D.jwSetFullscreen=j.jwSetFullscreen;D.jwGetVolume=function(){return b.volume};D.jwSetVolume=function(a){H.setVolume(a);j.jwSetVolume(a)};D.jwGetMute=
        function(){return b.mute};D.jwSetMute=function(a){H.setMute(a);j.jwSetMute(a)};D.jwGetState=function(){return!H?e.IDLE:H.state};D.jwGetPlaylist=function(){return[z]};D.jwGetPlaylistIndex=function(){return 0};D.jwGetStretching=function(){return b.config.stretching};D.jwAddEventListener=function(a,b){D.addEventListener(a,b)};D.jwRemoveEventListener=function(a,b){D.removeEventListener(a,b)};D.jwSetCurrentQuality=function(){};D.jwGetQualityLevels=function(){return[]};D.jwGetControls=function(){return j.jwGetControls()};
    D.skin=j.skin;D.id=j.id+"_instream";return D}})(window.jwplayer);
(function(d){var l=d.utils,a=l.css,c=d.events.state,f=d.html5.logo=function(e,h){function j(a){l.exists(a)&&a.stopPropagation&&a.stopPropagation();if(!s||!g.link)b.jwGetState()===c.IDLE||b.jwGetState()===c.PAUSED?b.jwPlay():b.jwPause();s&&g.link&&(b.jwPause(),b.jwSetFullscreen(!1),window.open(g.link,g.linktarget))}var b=e,m=b.id+"_logo",g,k,q=f.defaults,s=!1;this.resize=function(){};this.element=function(){return k};this.offset=function(b){a("#"+m+" ",{"margin-bottom":b})};this.position=function(){return g.position};
    this.margin=function(){return parseInt(g.margin,10)};this.hide=function(a){if(g.hide||a)s=!1,k.style.visibility="hidden",k.style.opacity=0};this.show=function(){s=!0;k.style.visibility="visible";k.style.opacity=1};var v="o";b.edition&&(v=b.edition(),v="pro"===v?"p":"premium"===v?"r":"ads"===v?"a":"free"===v?"f":"o");if("o"===v||"f"===v)q.link="http://www.longtailvideo.com/jwpabout/?a\x3dl\x26v\x3d"+d.version+"\x26m\x3dh\x26e\x3d"+v;g=l.extend({},q,h);g.hide="true"===g.hide.toString();k=document.createElement("img");
    k.className="jwlogo";k.id=m;if(g.file){var q=/(\w+)-(\w+)/.exec(g.position),v={},C=g.margin;3===q.length?(v[q[1]]=C,v[q[2]]=C):v.top=v.right=C;a("#"+m+" ",v);k.src=(g.prefix?g.prefix:"")+g.file;l.isMobile()?(new l.touch(k)).addEventListener(l.touchEvents.TAP,j):k.onclick=j}else k.style.display="none";return this};f.defaults={prefix:l.repo(),file:"logo.png",linktarget:"_top",margin:8,hide:!1,position:"top-right"};a(".jwlogo",{cursor:"pointer",position:"absolute"})})(jwplayer);
(function(d){var l=d.html5,a=d.utils,c=a.css;l.menu=function(d,e,h,j){function b(a){return!a||!a.src?{}:{background:"url("+a.src+") no-repeat left","background-size":a.width+"px "+a.height+"px"}}function m(a,b){return function(){r(a);q&&q(b)}}function g(a,b){var c=document.createElement("div");a&&(c.className=a);b&&b.appendChild(c);return c}function k(a){return(a=h.getSkinElement("tooltip",a))?a:{width:0,height:0,src:void 0}}var q=j,s=new l.overlay(e+"_overlay",h);j=a.extend({fontcase:void 0,fontcolor:"#cccccc",
    fontsize:11,fontweight:void 0,activecolor:"#ffffff",overcolor:"#ffffff"},h.getComponentSettings("tooltip"));var v,C=[];this.element=function(){return s.element()};this.addOption=function(b,c){var d=g("jwoption",v);d.id=e+"_option_"+c;d.innerHTML=b;a.isMobile()?(new a.touch(d)).addEventListener(a.touchEvents.TAP,m(C.length,c)):d.addEventListener("click",m(C.length,c));C.push(d)};this.clearOptions=function(){for(;0<C.length;)v.removeChild(C.pop())};var r=this.setActive=function(a){for(var b=0;b<C.length;b++){var c=
    C[b];c.className=c.className.replace(" active","");b===a&&(c.className+=" active")}};this.show=s.show;this.hide=s.hide;this.offsetX=s.offsetX;this.positionX=s.positionX;this.constrainX=s.constrainX;v=g("jwmenu");v.id=e;var n=k("menuTop"+d);d=k("menuOption");var y=k("menuOptionOver"),E=k("menuOptionActive");if(n&&n.image){var u=new Image;u.src=n.src;u.width=n.width;u.height=n.height;v.appendChild(u)}d&&(n="#"+e+" .jwoption",c(n,a.extend(b(d),{height:d.height,color:j.fontcolor,"padding-left":d.width,
    font:j.fontweight+" "+j.fontsize+"px Arial,Helvetica,sans-serif","line-height":d.height,"text-transform":"upper"===j.fontcase?"uppercase":void 0})),c(n+":hover",a.extend(b(y),{color:j.overcolor})),c(n+".active",a.extend(b(E),{color:j.activecolor})));s.setContents(v)};c("."+"jwmenu jwoption".replace(/ /g," ."),{cursor:"pointer","white-space":"nowrap",position:"relative"})})(jwplayer);
(function(d){var l=d.html5,a=d.utils,c=d.events;l.model=function(f,e){function h(a){var b=s[a.type];if(b&&b.length){for(var c=!1,d=0;d<b.length;d++){var e=b[d].split("-\x3e"),f=e[0],e=e[1]||f;j[e]!==a[f]&&(j[e]=a[f],c=!0)}c&&j.sendEvent(a.type,a)}else j.sendEvent(a.type,a)}var j=this,b,m=a.getCookies(),g={controlbar:{},display:{}},k=a.noop,q={autostart:!1,controls:!0,fullscreen:!1,height:320,mobilecontrols:!1,mute:!1,playlist:[],playlistposition:"none",playlistsize:180,playlistlayout:"extended",repeat:!1,
    stretching:a.stretching.UNIFORM,width:480,volume:90},s={};s[c.JWPLAYER_MEDIA_MUTE]=["mute"];s[c.JWPLAYER_MEDIA_VOLUME]=["volume"];s[c.JWPLAYER_PLAYER_STATE]=["newstate-\x3estate"];s[c.JWPLAYER_MEDIA_BUFFER]=["bufferPercent-\x3ebuffer"];s[c.JWPLAYER_MEDIA_TIME]=["position","duration"];j.setVideoProvider=function(a){if(b){b.removeGlobalListener(h);var c=b.getContainer();c&&(b.remove(),a.setContainer(c))}b=a;b.volume(j.volume);b.mute(j.mute);b.addGlobalListener(h)};j.destroy=function(){b&&(b.removeGlobalListener(h),
    b.destroy())};j.getVideo=function(){return b};j.seekDrag=function(a){b.seekDrag(a)};j.setFullscreen=function(a){a=!!a;a!==j.fullscreen&&(j.fullscreen=a,j.sendEvent(c.JWPLAYER_FULLSCREEN,{fullscreen:a}))};j.setPlaylist=function(a){j.playlist=d.playlist.filterPlaylist(a,j.androidhls);0===j.playlist.length?j.sendEvent(c.JWPLAYER_ERROR,{message:"Error loading playlist: No playable sources found"}):(j.sendEvent(c.JWPLAYER_PLAYLIST_LOADED,{playlist:d(j.id).getPlaylist()}),j.item=-1,j.setItem(0))};j.setItem=
    function(a){var b=!1;a===j.playlist.length||-1>a?(a=0,b=!0):a=-1===a||a>j.playlist.length?j.playlist.length-1:a;if(b||a!==j.item)j.item=a,j.sendEvent(c.JWPLAYER_PLAYLIST_ITEM,{index:j.item}),b=j.playlist[a],a=l.chooseProvider(b&&b.sources&&b.sources[0]),k instanceof a||(k=e||new a(j.id),j.setVideoProvider(k)),k.init&&k.init(b)};j.setVolume=function(e){j.mute&&0<e&&j.setMute(!1);e=Math.round(e);j.mute||a.saveCookie("volume",e);h({type:c.JWPLAYER_MEDIA_VOLUME,volume:e});b.volume(e)};j.setMute=function(e){a.exists(e)||
(e=!j.mute);a.saveCookie("mute",e);h({type:c.JWPLAYER_MEDIA_MUTE,mute:e});b.mute(e)};j.componentConfig=function(a){return g[a]};a.extend(j,new c.eventdispatcher);var v=j,C=a.extend({},q,m,f);a.foreach(C,function(b,c){C[b]=a.serialize(c)});v.config=C;a.extend(j,{id:f.id,state:c.state.IDLE,duration:-1,position:0,buffer:0},j.config);j.playlist=[];j.setItem(0)}})(jwplayer);
(function(d){var l=d.utils,a=l.css,c=l.transitionStyle,f="top",e="bottom",h="right",j="left",b={fontcase:void 0,fontcolor:"#ffffff",fontsize:12,fontweight:void 0,activecolor:"#ffffff",overcolor:"#ffffff"};d.html5.overlay=function(c,d,k){function q(a){return"#"+y+(a?" ."+a:"")}function s(a,b){var c=document.createElement("div");a&&(c.className=a);b&&b.appendChild(c);return c}function v(b,c){var e;e=(e=E.getSkinElement("tooltip",b))?e:{width:0,height:0,src:"",image:void 0,ready:!1};var d=s(c,p);a.style(d,
    C(e));return[d,e]}function C(a){return{background:"url("+a.src+") center","background-size":a.width+"px "+a.height+"px"}}function r(b,c){c||(c="");var d=v("cap"+b+c,"jwborder jw"+b+(c?c:"")),g=d[0],d=d[1],k=l.extend(C(d),{width:b===j||c===j||b===h||c===h?d.width:void 0,height:b===f||c===f||b===e||c===e?d.height:void 0});k[b]=b===e&&!u||b===f&&u?w.height:0;c&&(k[c]=0);a.style(g,k);g={};k={};d={left:d.width,right:d.width,top:(u?w.height:0)+d.height,bottom:(u?0:w.height)+d.height};c&&(g[c]=d[c],g[b]=
    0,k[b]=d[b],k[c]=0,a(q("jw"+b),g),a(q("jw"+c),k),F[b]=d[b],F[c]=d[c])}var n=this,y=c,E=d,u=k,p,z,w,A;c=l.extend({},b,E.getComponentSettings("tooltip"));var F={};n.element=function(){return p};n.setContents=function(a){l.empty(z);z.appendChild(a)};n.positionX=function(b){a.style(p,{left:Math.round(b)})};n.constrainX=function(b,c){if(n.showing&&0!==b.width&&n.offsetX(0)){c&&a.unblock();var d=l.bounds(p);0!==d.width&&(d.right>b.right?n.offsetX(b.right-d.right):d.left<b.left&&n.offsetX(b.left-d.left))}};
    n.offsetX=function(b){b=Math.round(b);var c=p.clientWidth;0!==c&&(a.style(p,{"margin-left":Math.round(-c/2)+b}),a.style(A,{"margin-left":Math.round(-w.width/2)-b}));return c};n.borderWidth=function(){return F.left};n.show=function(){n.showing=!0;a.style(p,{opacity:1,visibility:"visible"})};n.hide=function(){n.showing=!1;a.style(p,{opacity:0,visibility:"hidden"})};p=s(".jwoverlay".replace(".",""));p.id=y;d=v("arrow","jwarrow");A=d[0];w=d[1];a.style(A,{position:"absolute",bottom:u?void 0:0,top:u?0:
        void 0,width:w.width,height:w.height,left:"50%"});r(f,j);r(e,j);r(f,h);r(e,h);r(j);r(h);r(f);r(e);d=v("background","jwback");a.style(d[0],{left:F.left,right:F.right,top:F.top,bottom:F.bottom});z=s("jwcontents",p);a(q("jwcontents")+" *",{color:c.fontcolor,font:c.fontweight+" "+c.fontsize+"px Arial,Helvetica,sans-serif","text-transform":"upper"===c.fontcase?"uppercase":void 0});u&&l.transform(q("jwarrow"),"rotate(180deg)");a.style(p,{padding:F.top+1+"px "+F.right+"px "+(F.bottom+1)+"px "+F.left+"px"});
    n.showing=!1};a(".jwoverlay",{position:"absolute",visibility:"hidden",opacity:0});a(".jwoverlay .jwcontents",{position:"relative","z-index":1});a(".jwoverlay .jwborder",{position:"absolute","background-size":"100% 100%"},!0);a(".jwoverlay .jwback",{position:"absolute","background-size":"100% 100%"});c(".jwoverlay","opacity .25s, visibility .25s")})(jwplayer);
(function(d){var l=d.html5,a=d.utils;l.player=function(c){function f(){for(var a=m.playlist,b=[],c=0;c<a.length;c++)b.push(e(a[c]));return b}function e(b){var c={description:b.description,file:b.file,image:b.image,mediaid:b.mediaid,title:b.title};a.foreach(b,function(a,b){c[a]=b});c.sources=[];c.tracks=[];0<b.sources.length&&a.foreach(b.sources,function(a,b){c.sources.push({file:b.file,type:b.type?b.type:void 0,label:b.label,"default":b["default"]?!0:!1})});0<b.tracks.length&&a.foreach(b.tracks,function(a,
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             b){c.tracks.push({file:b.file,kind:b.kind?b.kind:void 0,label:b.label,"default":b["default"]?!0:!1})});!b.file&&0<b.sources.length&&(c.file=b.sources[0].file);return c}function h(){b.jwPlay=k.play;b.jwPause=k.pause;b.jwStop=k.stop;b.jwSeek=k.seek;b.jwSetVolume=k.setVolume;b.jwSetMute=k.setMute;b.jwLoad=k.load;b.jwPlaylistNext=k.next;b.jwPlaylistPrev=k.prev;b.jwPlaylistItem=k.item;b.jwSetFullscreen=k.setFullscreen;b.jwResize=g.resize;b.jwSeekDrag=m.seekDrag;b.jwGetQualityLevels=k.getQualityLevels;
    b.jwGetCurrentQuality=k.getCurrentQuality;b.jwSetCurrentQuality=k.setCurrentQuality;b.jwGetAudioTracks=k.getAudioTracks;b.jwGetCurrentAudioTrack=k.getCurrentAudioTrack;b.jwSetCurrentAudioTrack=k.setCurrentAudioTrack;b.jwGetCaptionsList=k.getCaptionsList;b.jwGetCurrentCaptions=k.getCurrentCaptions;b.jwSetCurrentCaptions=k.setCurrentCaptions;b.jwGetSafeRegion=g.getSafeRegion;b.jwForceState=g.forceState;b.jwReleaseState=g.releaseState;b.jwGetPlaylistIndex=j("item");b.jwGetPosition=j("position");b.jwGetDuration=
        j("duration");b.jwGetBuffer=j("buffer");b.jwGetWidth=j("width");b.jwGetHeight=j("height");b.jwGetFullscreen=j("fullscreen");b.jwGetVolume=j("volume");b.jwGetMute=j("mute");b.jwGetState=j("state");b.jwGetStretching=j("stretching");b.jwGetPlaylist=f;b.jwGetControls=j("controls");b.jwDetachMedia=k.detachMedia;b.jwAttachMedia=k.attachMedia;b.jwPlayAd=function(a){var c=d(b.id).plugins;c.vast&&c.vast.jwPlayAd(a)};b.jwPauseAd=function(){var a=d(b.id).plugins;a.googima&&a.googima.jwPauseAd()};b.jwDestroyGoogima=
        function(){var a=d(b.id).plugins;a.googima&&a.googima.jwDestroyGoogima()};b.jwInitInstream=function(){b.jwInstreamDestroy();s=new l.instream(b,m,g,k);s.init()};b.jwLoadItemInstream=function(a,b){if(!s)throw"Instream player undefined";s.load(a,b)};b.jwLoadArrayInstream=function(a,b){if(!s)throw"Instream player undefined";s.load(a,b)};b.jwSetControls=function(a){g.setControls(a);s&&s.setControls(a)};b.jwInstreamPlay=function(){s&&s.jwInstreamPlay()};b.jwInstreamPause=function(){s&&s.jwInstreamPause()};
    b.jwInstreamState=function(){return s?s.jwInstreamState():""};b.jwInstreamDestroy=function(a,b){if(b=b||s)b.jwInstreamDestroy(a||!1),b===s&&(s=void 0)};b.jwInstreamAddEventListener=function(a,b){s&&s.jwInstreamAddEventListener(a,b)};b.jwInstreamRemoveEventListener=function(a,b){s&&s.jwInstreamRemoveEventListener(a,b)};b.jwPlayerDestroy=function(){k&&k.stop();g&&g.destroy();m&&m.destroy();q&&(q.resetEventListeners(),q.destroy())};b.jwInstreamSetText=function(a){s&&s.jwInstreamSetText(a)};b.jwIsBeforePlay=
        function(){return k.checkBeforePlay()};b.jwIsBeforeComplete=function(){return m.getVideo().checkComplete()};b.jwSetCues=g.addCues;b.jwAddEventListener=k.addEventListener;b.jwRemoveEventListener=k.removeEventListener;b.jwDockAddButton=g.addButton;b.jwDockRemoveButton=g.removeButton}function j(a){return function(){return m[a]}}var b=this,m,g,k,q,s;m=new l.model(c);b.id=m.id;b._model=m;a.css.block(b.id);g=new l.view(b,m);k=new l.controller(m,g);h();b.initializeAPI=h;q=new l.setup(m,g);q.addEventListener(d.events.JWPLAYER_READY,
    function(c){k.playerReady(c);a.css.unblock(b.id)});q.addEventListener(d.events.JWPLAYER_ERROR,function(c){a.css.unblock(b.id);d(b.id).dispatchEvent(d.events.JWPLAYER_SETUP_ERROR,c)});q.start()}})(window.jwplayer);
(function(d){var l={size:180,backgroundcolor:"#333333",fontcolor:"#999999",overcolor:"#CCCCCC",activecolor:"#CCCCCC",titlecolor:"#CCCCCC",titleovercolor:"#FFFFFF",titleactivecolor:"#FFFFFF",fontweight:"normal",titleweight:"normal",fontsize:11,titlesize:13},a=d.html5,c=d.events,f=d.utils,e=f.css,h=f.isMobile();a.playlistcomponent=function(d,b){function m(a){return"#"+C.id+(a?" ."+a:"")}function g(a,b){var c=document.createElement(a);b&&(c.className=b);return c}function k(a){return function(){u=a;q.jwPlaylistItem(a);
    q.jwPlay(!0)}}var q=d,s=q.skin,v=f.extend({},l,q.skin.getComponentSettings("playlist"),b),C,r,n,y,E=-1,u,p,z=76,w={background:void 0,divider:void 0,item:void 0,itemOver:void 0,itemImage:void 0,itemActive:void 0},A,F=this;F.element=function(){return C};F.redraw=function(){p&&p.redraw()};F.show=function(){f.show(C)};F.hide=function(){f.hide(C)};C=g("div","jwplaylist");C.id=q.id+"_jwplayer_playlistcomponent";A="basic"===q._model.playlistlayout;r=g("div","jwlistcontainer");C.appendChild(r);f.foreach(w,
    function(a){w[a]=s.getSkinElement("playlist",a)});A&&(z=32);w.divider&&(z+=w.divider.height);var L=0,G=0,t=0;f.clearCss(m());e(m(),{"background-color":v.backgroundcolor});e(m("jwlist"),{"background-image":w.background?" url("+w.background.src+")":""});e(m("jwlist *"),{color:v.fontcolor,font:v.fontweight+" "+v.fontsize+"px Arial, Helvetica, sans-serif"});w.itemImage?(L=(z-w.itemImage.height)/2+"px ",G=w.itemImage.width,t=w.itemImage.height):(G=4*z/3,t=z);w.divider&&e(m("jwplaylistdivider"),{"background-image":"url("+
w.divider.src+")","background-size":"100% "+w.divider.height+"px",width:"100%",height:w.divider.height});e(m("jwplaylistimg"),{height:t,width:G,margin:L?L+"0 "+L+L:"0 5px 0 0"});e(m("jwlist li"),{"background-image":w.item?"url("+w.item.src+")":"",height:z,overflow:"hidden","background-size":"100% "+z+"px",cursor:"pointer"});L={overflow:"hidden"};""!==v.activecolor&&(L.color=v.activecolor);w.itemActive&&(L["background-image"]="url("+w.itemActive.src+")");e(m("jwlist li.active"),L);e(m("jwlist li.active .jwtitle"),
    {color:v.titleactivecolor});e(m("jwlist li.active .jwdescription"),{color:v.activecolor});L={overflow:"hidden"};""!==v.overcolor&&(L.color=v.overcolor);w.itemOver&&(L["background-image"]="url("+w.itemOver.src+")");h||(e(m("jwlist li:hover"),L),e(m("jwlist li:hover .jwtitle"),{color:v.titleovercolor}),e(m("jwlist li:hover .jwdescription"),{color:v.overcolor}));e(m("jwtextwrapper"),{height:z,position:"relative"});e(m("jwtitle"),{overflow:"hidden",display:"inline-block",height:A?z:20,color:v.titlecolor,
    "font-size":v.titlesize,"font-weight":v.titleweight,"margin-top":A?"0 10px":10,"margin-left":10,"margin-right":10,"line-height":A?z:20});e(m("jwdescription"),{display:"block","font-size":v.fontsize,"line-height":18,"margin-left":10,"margin-right":10,overflow:"hidden",height:36,position:"relative"});q.jwAddEventListener(c.JWPLAYER_PLAYLIST_LOADED,function(){r.innerHTML="";for(var b=q.jwGetPlaylist(),c=[],d=0;d<b.length;d++)b[d]["ova.hidden"]||c.push(b[d]);if(n=c){b=g("ul","jwlist");b.id=C.id+"_ul"+
    Math.round(1E7*Math.random());y=b;for(b=0;b<n.length;b++){var j=b,c=n[j],d=g("li","jwitem"),l=void 0;d.id=y.id+"_item_"+j;0<j?(l=g("div","jwplaylistdivider"),d.appendChild(l)):(j=w.divider?w.divider.height:0,d.style.height=z-j+"px",d.style["background-size"]="100% "+(z-j)+"px");j=g("div","jwplaylistimg jwfill");l=void 0;c["playlist.image"]&&w.itemImage?l=c["playlist.image"]:c.image&&w.itemImage?l=c.image:w.itemImage&&(l=w.itemImage.src);l&&!A&&(e("#"+d.id+" .jwplaylistimg",{"background-image":l}),
    d.appendChild(j));j=g("div","jwtextwrapper");l=g("span","jwtitle");l.innerHTML=c&&c.title?c.title:"";j.appendChild(l);c.description&&!A&&(l=g("span","jwdescription"),l.innerHTML=c.description,j.appendChild(l));d.appendChild(j);c=d;h?(new f.touch(c)).addEventListener(f.touchEvents.TAP,k(b)):c.onclick=k(b);y.appendChild(c)}E=q.jwGetPlaylistIndex();r.appendChild(y);p=new a.playlistslider(C.id+"_slider",q.skin,C,y)}});q.jwAddEventListener(c.JWPLAYER_PLAYLIST_ITEM,function(a){0<=E&&(document.getElementById(y.id+
    "_item_"+E).className="jwitem",E=a.index);document.getElementById(y.id+"_item_"+a.index).className="jwitem active";a=q.jwGetPlaylistIndex();a!==u&&(u=-1,p&&p.visible()&&p.thumbPosition(a/(q.jwGetPlaylist().length-1)))});q.jwAddEventListener(c.JWPLAYER_RESIZE,function(){F.redraw()});return this};e(".jwplaylist",{position:"absolute",width:"100%",height:"100%"});f.dragStyle(".jwplaylist","none");e(".jwplaylist .jwplaylistimg",{position:"relative",width:"100%","float":"left",margin:"0 5px 0 0",background:"#000",
    overflow:"hidden"});e(".jwplaylist .jwlist",{position:"absolute",width:"100%","list-style":"none",margin:0,padding:0,overflow:"hidden"});e(".jwplaylist .jwlistcontainer",{position:"absolute",overflow:"hidden",width:"100%",height:"100%"});e(".jwplaylist .jwlist li",{width:"100%"});e(".jwplaylist .jwtextwrapper",{overflow:"hidden"});e(".jwplaylist .jwplaylistdivider",{position:"absolute"});h&&f.transitionStyle(".jwplaylist .jwlist","top .35s")})(jwplayer);
(function(d){function l(){var a=[],b;for(b=0;b<arguments.length;b++)a.push(".jwplaylist ."+arguments[b]);return a.join(",")}var a=jwplayer.utils,c=a.touchEvents,f=a.css,e=document,h=window;d.playlistslider=function(d,b,l,g){function k(a){return"#"+z.id+(a?" ."+a:"")}function q(a,b,c,d){var g=e.createElement("div");a&&(g.className=a,b&&f(k(a),{"background-image":b.src?b.src:void 0,"background-repeat":d?"repeat-y":"no-repeat",height:d?void 0:b.height}));c&&c.appendChild(g);return g}function s(a){return(a=
    u.getSkinElement("playlist",a))?a:{width:0,height:0,src:void 0}}function v(a){if(x)return a=a?a:h.event,Z(L-(a.detail?-1*a.detail:a.wheelDelta/40)/10),a.stopPropagation&&a.stopPropagation(),a.preventDefault?a.preventDefault():a.returnValue=!1,a.cancelBubble=!0,a.cancel=!0,!1}function C(a){0===a.button&&(F=!0);e.onselectstart=function(){return!1};h.addEventListener("mousemove",n,!1);h.addEventListener("mouseup",E,!1)}function r(a){Z(L-2*a.deltaY/p.clientHeight)}function n(b){if(F||"click"===b.type){var c=
    a.bounds(w),d=A.clientHeight/2;Z((b.pageY-c.top-d)/(c.height-d-d))}}function y(a){return function(b){0<b.button||(Z(L+0.05*a),G=setTimeout(function(){t=setInterval(function(){Z(L+0.05*a)},50)},500))}}function E(){F=!1;h.removeEventListener("mousemove",n);h.removeEventListener("mouseup",E);e.onselectstart=void 0;clearTimeout(G);clearInterval(t)}var u=b,p=g,z,w,A,F,L=0,G,t;b=a.isMobile();var x=!0,M,J,H,K,N,P,S,T,D;this.element=function(){return z};this.visible=function(){return x};var W=this.redraw=
    function(){clearTimeout(D);D=setTimeout(function(){if(p&&p.clientHeight){var a=p.parentNode.clientHeight/p.clientHeight;0>a&&(a=0);1<a?x=!1:(x=!0,f(k("jwthumb"),{height:Math.max(w.clientHeight*a,N.height+P.height)}));f(k(),{visibility:x?"visible":"hidden"});p&&(p.style.width=x?p.parentElement.clientWidth-H.width+"px":"")}else D=setTimeout(W,10)},0)},Z=this.thumbPosition=function(a){isNaN(a)&&(a=0);L=Math.max(0,Math.min(1,a));f(k("jwthumb"),{top:S+(w.clientHeight-A.clientHeight)*L});g&&(g.style.top=
    Math.min(0,z.clientHeight-g.scrollHeight)*L+"px")};z=q("jwslider",null,l);z.id=d;d=new a.touch(p);b?d.addEventListener(c.DRAG,r):(z.addEventListener("mousedown",C,!1),z.addEventListener("click",n,!1));M=s("sliderCapTop");J=s("sliderCapBottom");H=s("sliderRail");d=s("sliderRailCapTop");l=s("sliderRailCapBottom");K=s("sliderThumb");N=s("sliderThumbCapTop");P=s("sliderThumbCapBottom");S=M.height;T=J.height;f(k(),{width:H.width});f(k("jwrail"),{top:S,bottom:T});f(k("jwthumb"),{top:S});M=q("jwslidertop",
    M,z);J=q("jwsliderbottom",J,z);w=q("jwrail",null,z);A=q("jwthumb",null,z);b||(M.addEventListener("mousedown",y(-1),!1),J.addEventListener("mousedown",y(1),!1));q("jwrailtop",d,w);q("jwrailback",H,w,!0);q("jwrailbottom",l,w);f(k("jwrailback"),{top:d.height,bottom:l.height});q("jwthumbtop",N,A);q("jwthumbback",K,A,!0);q("jwthumbbottom",P,A);f(k("jwthumbback"),{top:N.height,bottom:P.height});W();p&&!b&&(p.addEventListener("mousewheel",v,!1),p.addEventListener("DOMMouseScroll",v,!1));return this};f(l("jwslider"),
    {position:"absolute",height:"100%",visibility:"hidden",right:0,top:0,cursor:"pointer","z-index":1,overflow:"hidden"});f(l("jwslider")+" *",{position:"absolute",width:"100%","background-position":"center","background-size":"100% 100%",overflow:"hidden"});f(l("jwslidertop","jwrailtop","jwthumbtop"),{top:0});f(l("jwsliderbottom","jwrailbottom","jwthumbbottom"),{bottom:0})})(jwplayer.html5);
(function(d){var l=jwplayer.utils,a=l.css,c=document,f="none";d.rightclick=function(a,h){function j(a){var b=c.createElement("div");b.className=a.replace(".","");return b}function b(){k||(q.style.display=f)}var m,g=l.extend({aboutlink:"http://www.longtailvideo.com/jwpabout/?a\x3dr\x26v\x3d"+d.version+"\x26m\x3dh\x26e\x3do",abouttext:"About JW Player "+d.version+"..."},h),k=!1,q,s;this.element=function(){return q};this.destroy=function(){c.removeEventListener("mousedown",b,!1)};m=c.getElementById(a.id);
    q=j(".jwclick");q.id=a.id+"_menu";q.style.display=f;m.oncontextmenu=function(a){var b,c;k||(a=a||window.event,b=a.target||a.srcElement,c=l.bounds(m),b=l.bounds(b),q.style.display=f,q.style.left=(a.offsetX?a.offsetX:a.layerX)+b.left-c.left+"px",q.style.top=(a.offsetY?a.offsetY:a.layerY)+b.top-c.top+"px",q.style.display="block",a.preventDefault())};q.onmouseover=function(){k=!0};q.onmouseout=function(){k=!1};c.addEventListener("mousedown",b,!1);s=j(".jwclick_item");s.innerHTML=g.abouttext;s.onclick=
        function(){window.top.location=g.aboutlink};q.appendChild(s);m.appendChild(q)};a(".jwclick",{"background-color":"#FFF","-webkit-border-radius":5,"-moz-border-radius":5,"border-radius":5,height:"auto",border:"1px solid #bcbcbc","font-family":"'MS Sans Serif', 'Geneva', sans-serif","font-size":10,width:320,"-webkit-box-shadow":"5px 5px 7px rgba(0,0,0,.10), 0px 1px 0px rgba(255,255,255,.3) inset","-moz-box-shadow":"5px 5px 7px rgba(0,0,0,.10), 0px 1px 0px rgba(255,255,255,.3) inset","box-shadow":"5px 5px 7px rgba(0,0,0,.10), 0px 1px 0px rgba(255,255,255,.3) inset",
    position:"absolute","z-index":999},!0);a(".jwclick div",{padding:"8px 21px",margin:"0px","background-color":"#FFF",border:"none","font-family":"'MS Sans Serif', 'Geneva', sans-serif","font-size":10,color:"inherit"},!0);a(".jwclick_item",{padding:"8px 21px","text-align":"left",cursor:"pointer"},!0);a(".jwclick_item:hover",{"background-color":"#595959",color:"#FFF"},!0);a(".jwclick_item a",{"text-decoration":f,color:"#000"},!0);a(".jwclick hr",{width:"100%",padding:0,margin:0,border:"1px #e9e9e9 solid"},
    !0)})(jwplayer.html5);
(function(d){var l=d.html5,a=d.utils,c=d._,f=d.events;l.setup=function(e,h){function j(){if(!this.cancelled)for(var a=0;a<z.length;a++){var b=z[a];c.all(c.map(b.depends,c.property("complete")))&&(z.splice(a,1),b.method(),c.defer(j))}}function b(a){a.complete=!0;0<z.length&&!C&&c.defer(j)}function m(){b(n)}function g(a){k("Error loading skin: "+a)}function k(a){C=!0;v.sendEvent(f.JWPLAYER_ERROR,{message:a});q.setupError(a)}var q=h,s,v=new f.eventdispatcher,C=!1,r={method:function(){e.edition&&"invalid"===
e.edition()?k("Error setting up player: Invalid license key"):b(r)},depends:[]},n={method:function(){s=new l.skin;s.load(e.config.skin,m,g)},depends:[r]},y={method:function(){var c=a.typeOf(e.config.playlist);"array"===c?(c=new d.playlist(e.config.playlist),e.setPlaylist(c),0===e.playlist.length||0===e.playlist[0].sources.length?k("Error loading playlist: No playable sources found"):b(y)):k("Playlist type not supported: "+c)},depends:[r]},E={method:function(){q.setup(s);b(E)},depends:[y,n]},u={method:function(){b(u)},
    depends:[E,y]},p={method:function(){this.cancelled||(v.sendEvent(f.JWPLAYER_READY),b(p))},depends:[u]},z=[r,n,y,E,u,p];this.start=function(){c.defer(j)};this.destroy=function(){this.cancelled=!0};a.extend(this,v)}})(jwplayer);
(function(d){d.skin=function(){var l={},a=!1;this.load=function(c,f,e){new d.skinloader(c,function(c){a=!0;l=c;"function"===typeof f&&f()},function(a){"function"===typeof e&&e(a)})};this.getSkinElement=function(c,d){c=c.toLowerCase();d=d.toLowerCase();if(a)try{return l[c].elements[d]}catch(e){jwplayer.utils.log("No such skin component / element: ",[c,d])}return null};this.getComponentSettings=function(c){c=c.toLowerCase();return a&&l&&l[c]?l[c].settings:null};this.getComponentLayout=function(c){c=
    c.toLowerCase();if(a){var d=l[c].layout;if(d&&(d.left||d.right||d.center))return l[c].layout}return null}}})(jwplayer.html5);
(function(d){var l=jwplayer.utils,a="Skin formatting error";d.skinloader=function(c,f,e){function h(c){q=c;l.ajax(l.getAbsolutePath(r),function(c){try{l.exists(c.responseXML)&&b(c.responseXML)}catch(d){v(a)}},function(a){v(a)})}function j(a,b){return a?a.getElementsByTagName(b):null}function b(a){var b=j(a,"skin")[0];a=j(b,"component");var c=b.getAttribute("target"),b=parseFloat(b.getAttribute("pixelratio"));0<b&&(y=b);l.versionCheck(c)||v("Incompatible player version");if(0===a.length)s(q);else for(c=
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             0;c<a.length;c++){var d=k(a[c].getAttribute("name")),b={settings:{},elements:{},layout:{}},e=j(j(a[c],"elements")[0],"element");q[d]=b;for(var f=0;f<e.length;f++)m(e[f],d);if((d=j(a[c],"settings")[0])&&0<d.childNodes.length){d=j(d,"setting");for(e=0;e<d.length;e++){var f=d[e].getAttribute("name"),h=d[e].getAttribute("value");/color$/.test(f)&&(h=l.stringToColor(h));b.settings[k(f)]=h}}if((d=j(a[c],"layout")[0])&&0<d.childNodes.length){d=j(d,"group");for(e=0;e<d.length;e++){h=d[e];f={elements:[]};
    b.layout[k(h.getAttribute("position"))]=f;for(var n=0;n<h.attributes.length;n++){var r=h.attributes[n];f[r.name]=r.value}h=j(h,"*");for(n=0;n<h.length;n++){r=h[n];f.elements.push({type:r.tagName});for(var t=0;t<r.attributes.length;t++){var x=r.attributes[t];f.elements[n][k(x.name)]=x.value}l.exists(f.elements[n].name)||(f.elements[n].name=r.tagName)}}}C=!1;g()}}function m(a,b){b=k(b);var c=new Image,d=k(a.getAttribute("name")),e=a.getAttribute("src");if(0!==e.indexOf("data:image/png;base64,"))var f=
    l.getAbsolutePath(r),e=[f.substr(0,f.lastIndexOf("/")),b,e].join("/");q[b].elements[d]={height:0,width:0,src:"",ready:!1,image:c};c.onload=function(){var a=b,e=q[k(a)]?q[k(a)].elements[k(d)]:null;e?(e.height=Math.round(c.height/y*n),e.width=Math.round(c.width/y*n),e.src=c.src,e.ready=!0,g()):l.log("Loaded an image for a missing element: "+a+"."+d)};c.onerror=function(){v("Skin image not found: "+this.src)};c.src=e}function g(){var a=!0,b;for(b in q)if("properties"!==b&&q.hasOwnProperty(b)){var c=
    q[b].elements,d;for(d in c)c.hasOwnProperty(d)&&(a&=(q[k(b)]?q[k(b)].elements[k(d)]:null).ready)}a&&(C||s(q))}function k(a){return a?a.toLowerCase():""}var q={},s=f,v=e,C=!0,r=c,n=(jwplayer.utils.isMobile(),1),y=1;"string"!==typeof r||""===r?b(d.defaultskin()):"xml"!==l.extension(r)?v("Skin not a valid file type"):new d.skinloader("",h,v)}})(jwplayer.html5);
(function(d){var l=d.utils,a=d.events,c=l.css;d.html5.thumbs=function(f){function e(a){k=null;try{a=(new d.parsers.srt).parse(a.responseText,!0)}catch(b){h(b.message);return}if("array"!==l.typeOf(a))return h("Invalid data");m=a}function h(a){k=null;l.log("Thumbnails could not be loaded: "+a)}function j(a,d,e){a.onload=null;d.width||(d.width=a.width,d.height=a.height);d["background-image"]=a.src;c.style(b,d);e&&e(d.width)}var b,m,g,k,q,s={},v,C=new a.eventdispatcher;l.extend(this,C);b=document.createElement("div");
    b.id=f;this.load=function(a){c.style(b,{display:"none"});k&&(k.onload=null,k.onreadystatechange=null,k.onerror=null,k.abort&&k.abort(),k=null);v&&(v.onload=null);a?(g=a.split("?")[0].split("/").slice(0,-1).join("/"),k=l.ajax(a,e,h,!0)):(m=q=v=null,s={})};this.element=function(){return b};this.updateTimeline=function(a,b){if(m){for(var c=0;c<m.length&&a>m[c].end;)c++;c===m.length&&c--;c=m[c].text;a:{var d=c;if(d&&d!==q){q=d;0>d.indexOf("://")&&(d=g?g+"/"+d:d);var e={display:"block",margin:"0 auto",
        "background-position":"0 0",width:0,height:0};if(0<d.indexOf("#xywh"))try{var f=/(.+)\#xywh=(\d+),(\d+),(\d+),(\d+)/.exec(d),d=f[1];e["background-position"]=-1*f[2]+"px "+-1*f[3]+"px";e.width=f[4];e.height=f[5]}catch(k){h("Could not parse thumbnail");break a}var l=s[d];l?j(l,e,b):(l=new Image,l.onload=function(){j(l,e,b)},s[d]=l,l.src=d);v&&(v.onload=null);v=l}}return c}}}})(jwplayer);
(function(d){var l=d.jwplayer,a=l.html5,c=l.utils,f=l.events,e=f.state,h=c.css,j=c.bounds,b=c.isMobile(),m=c.isIPad(),g=c.isIPod(),k="aspectMode",q=["fullscreenchange","webkitfullscreenchange","mozfullscreenchange","MSFullscreenChange"],s="hidden",v="none",C="block";a.view=function(r,n){function y(a){a=c.between(n.position+a,0,this.getDuration());this.seek(a)}function E(a){a=c.between(this.getVolume()+a,0,100);this.setVolume(a)}function u(a){var b;b=a.ctrlKey||a.metaKey?!1:n.controls?!0:!1;if(!b)return!0;
    O.adMode()||(ea(),x());b=l(r.id);switch(a.keyCode){case 27:b.setFullscreen(!1);break;case 13:case 32:b.play();break;case 37:O.adMode()||y.call(b,-5);break;case 39:O.adMode()||y.call(b,5);break;case 38:E.call(b,10);break;case 40:E.call(b,-10);break;case 77:b.setMute();break;case 70:b.setFullscreen();break;default:if(48<=a.keyCode&&59>=a.keyCode){var c=(a.keyCode-48)/10*b.getDuration();b.seek(c)}}if(/13|32|37|38|39|40/.test(a.keyCode))return a.preventDefault(),!1}function p(){Ya=!0;ja.sendEvent(f.JWPLAYER_VIEW_TAB_FOCUS,
    {hasFocus:!1})}function z(){var a=!Ya;Ya=!1;a&&ja.sendEvent(f.JWPLAYER_VIEW_TAB_FOCUS,{hasFocus:!0});O.adMode()||(ea(),x())}function w(){Ya=!1;ja.sendEvent(f.JWPLAYER_VIEW_TAB_FOCUS,{hasFocus:!1})}function A(){var a=j(Q),c=Math.round(a.width),e=Math.round(a.height);if(document.body.contains(Q)){if(c&&e&&(c!==Ka||e!==$a))Ka=c,$a=e,V&&V.redraw(),clearTimeout(Ba),Ba=setTimeout(W,50),ja.sendEvent(f.JWPLAYER_RESIZE,{width:c,height:e})}else d.removeEventListener("resize",A),b&&d.removeEventListener("orientationchange",
    A);return a}function F(a){a&&(a.element().addEventListener("mousemove",J,!1),a.element().addEventListener("mouseout",H,!1))}function L(){}function G(){clearTimeout(xa);xa=setTimeout(Aa,wa)}function t(a,b){var c=document.createElement(a);b&&(c.className=b);return c}function x(){clearTimeout(xa);xa=setTimeout(Aa,wa)}function M(){clearTimeout(xa);var a=r.jwGetState();if(a===e.PLAYING||a===e.PAUSED||ua)Ma(),Oa||(xa=setTimeout(Aa,wa))}function J(){clearTimeout(xa);Oa=!0}function H(){Oa=!1}function K(a){ja.sendEvent(a.type,
    a)}function N(a){if(a.done)P();else{if(!a.complete){O.adMode()||(O.instreamMode(!0),O.adMode(!0),O.show(!0));O.setText(a.message);var b=a.onClick;void 0!==b&&V.setAlternateClickHandler(function(){b(a)});void 0!==a.onSkipAd&&ba&&ba.setSkipoffset(a,a.onSkipAd)}ba&&ba.adChanged(a)}}function P(){O.setText("");O.adMode(!1);O.instreamMode(!1);O.show(!0);ba&&(ba.adsEnded(),ba.setState(r.jwGetState()));V.revertAlternateClickHandler()}function S(a,b,d){var e=Q.className,f,g,j=r.id+"_view";h.block(j);if(d=
        !!d)e=e.replace(/\s*aspectMode/,""),Q.className!==e&&(Q.className=e),h.style(Q,{display:C},d);c.exists(a)&&c.exists(b)&&(n.width=a,n.height=b);d={width:a};-1===e.indexOf(k)&&(d.height=b);h.style(Q,d,!0);V&&V.redraw();O&&O.redraw(!0);$&&($.offset(O&&0<=$.position().indexOf("bottom")?O.height()+O.margin():0),setTimeout(function(){ca&&ca.offset("top-left"===$.position()?$.element().clientWidth+$.margin():0)},500));R=T(b);O&&(R?(O.audioMode(!0),Ma(),V.hidePreview(!0),V&&V.hide(),Na(!1)):(O.audioMode(!1),
    Xa(r.jwGetState())));$&&R&&Va();Q.style.backgroundColor=R?"transparent":"#000";f=n.playlistsize;g=n.playlistposition;if(Ia&&f&&("right"===g||"bottom"===g))Ia.redraw(),e={display:C},d={},e[g]=0,d[g]=f,"right"===g?e.width=f:e.height=f,h.style(Ra,e),h.style(Ga,d);W(a,b);h.unblock(j)}function T(a){if(n.aspectratio)return!1;if(l._.isNumber(a))return D(a);if(l._.isString(a)&&-1<a.indexOf("%"))return!1;a=j(Q);return D(a.height)}function D(a){if(!a)return!1;"bottom"===n.playlistposition&&(a-=n.playlistsize);
    return 40>=a}function W(a,b){if(!a||isNaN(Number(a))){if(!ga)return;a=ga.clientWidth}if(!b||isNaN(Number(b))){if(!ga)return;b=ga.clientHeight}c.isMSIE(9)&&(document.all&&!d.atob)&&(a=b="100%");n.getVideo().resize(a,b,n.stretching)&&(clearTimeout(Ba),Ba=setTimeout(W,250))}function Z(a){void 0!==a.jwstate?a=a.jwstate:ya?(a=document.fullscreenElement||document.webkitCurrentFullScreenElement||document.mozFullScreenElement||document.msFullscreenElement,a=!!(a&&a.id===r.id)):a=ua?U.getVideo().getFullScreen():
    n.getVideo().getFullScreen();ya?Y(Q,a):da(a)}function Y(a,b){c.removeClass(a,"jwfullscreen");b?(c.addClass(a,"jwfullscreen"),h.style(document.body,{"overflow-y":s}),x()):h.style(document.body,{"overflow-y":""});O&&O.redraw();V&&V.redraw();ca&&ca.redraw();W();da(b)}function da(a){n.setFullscreen(a);U&&U.setFullscreen(a);a?(clearTimeout(Ba),Ba=setTimeout(W,200)):m&&r.jwGetState()===e.PAUSED&&setTimeout(La,500)}function ea(){O&&n.controls&&(ua?Ha.show():O.show())}function va(){!0!==ia&&(O&&!R&&!n.getVideo().isAudioFile())&&
(ua&&Ha.hide(),O.hide())}function ra(){ca&&(!R&&n.controls)&&ca.show()}function I(){ca&&(!Pa&&!n.getVideo().isAudioFile())&&ca.hide()}function Va(){$&&(!n.getVideo().isAudioFile()||R)&&$.hide(R)}function La(){V&&n.controls&&!R&&(!g||r.jwGetState()===e.IDLE)&&V.show();(!b||!n.fullscreen)&&n.getVideo().setControls(!1)}function Aa(){clearTimeout(xa);if(!0!==ia){sa=!1;var a=r.jwGetState();(!n.controls||a!==e.PAUSED)&&va();n.controls||I();a!==e.IDLE&&a!==e.PAUSED&&(I(),Va());c.addClass(Q,"jw-user-inactive")}}
    function Ma(){if(!1!==ia){sa=!0;if(n.controls||R)ea(),ra();Sa.hide&&$&&!R&&$.show();c.removeClass(Q,"jw-user-inactive")}}function Na(a){a=a&&!R;n.getVideo().setVisibility(a)}function ob(){Pa=!0;ka(!1);n.controls&&ra()}function pb(){ba&&ba.setState(r.jwGetState())}function Da(a){Pa=!1;clearTimeout(Ta);Ta=setTimeout(function(){Xa(a.newstate)},100)}function kb(){va()}function Xa(a){if(n.getVideo().isCaster)V&&(V.show(),V.hidePreview(!1)),h.style(ga,{visibility:"visible",opacity:1}),O&&(O.show(),O.hideFullscreen(!0));
    else{switch(a){case e.PLAYING:ia=!0!==n.getVideo().isCaster?null:!0;(ua?U:n).getVideo().isAudioFile()?(Na(!1),V.hidePreview(R),V.setHiding(!0),O&&(Ma(),O.hideFullscreen(!0)),ra()):(Na(!0),W(),V.hidePreview(!0),O&&O.hideFullscreen(!n.getVideo().supportsFullscreen()));break;case e.IDLE:Na(!1);R||(V.hidePreview(!1),La(),ra(),O&&O.hideFullscreen(!1));break;case e.BUFFERING:La();Aa();b&&Na(!0);break;case e.PAUSED:La(),Ma()}$&&!R&&$.show()}}function ab(a){return"#"+r.id+(a?" ."+a:"")}function Za(a,b){h(a,
        {display:b?C:v})}var Q,Ga,la,hb,Ra,xa=-1,wa=b?4E3:2E3,ga,Ka,$a,za,Ha,Fa,U,ua=!1,O,V,ba,ca,$,Sa=c.extend({},n.componentConfig("logo")),B,Ia,R,aa=!1,sa=!1,ia=null,Pa,fa,Ba=-1,Oa=!1,Qa,qa,ya=!1,Ya=!1,ja=c.extend(this,new f.eventdispatcher);this.getCurrentCaptions=function(){return B.getCurrentCaptions()};this.setCurrentCaptions=function(a){B.setCurrentCaptions(a)};this.getCaptionsList=function(){return B.getCaptionsList()};this.setup=function(j){if(!aa){r.skin=j;Ga=t("span","jwmain");Ga.id=r.id+"_view";
        ga=t("span","jwvideo");ga.id=r.id+"_media";la=t("span","jwcontrols");za=t("span","jwinstream");Ra=t("span","jwplaylistcontainer");hb=t("span","jwaspect");j=n.componentConfig("controlbar");var s=n.componentConfig("display");B=new a.captions(r,n.captions);B.addEventListener(f.JWPLAYER_CAPTIONS_LIST,K);B.addEventListener(f.JWPLAYER_CAPTIONS_CHANGED,K);B.addEventListener(f.JWPLAYER_CAPTIONS_LOADED,L);la.appendChild(B.element());V=new a.display(r,s);V.addEventListener(f.JWPLAYER_DISPLAY_CLICK,function(a){K(a);
            b?sa?Aa():Ma():Da({newstate:r.jwGetState()});sa&&x()});la.appendChild(V.element());$=new a.logo(r,Sa);la.appendChild($.element());ca=new a.dock(r,n.componentConfig("dock"));la.appendChild(ca.element());r.edition&&!b?fa=new a.rightclick(r,{abouttext:n.abouttext,aboutlink:n.aboutlink}):b||(fa=new a.rightclick(r,{}));n.playlistsize&&(n.playlistposition&&n.playlistposition!==v)&&(Ia=new a.playlistcomponent(r,{}),Ra.appendChild(Ia.element()));O=new a.controlbar(r,j);O.addEventListener(f.JWPLAYER_USER_ACTION,
            x);la.appendChild(O.element());g&&va();c.canCast()&&ja.forceControls(!0);Q.onmousedown=p;Q.onfocusin=z;Q.addEventListener("focus",z);Q.onfocusout=w;Q.addEventListener("blur",w);Q.addEventListener("keydown",u);Ga.appendChild(ga);Ga.appendChild(la);Ga.appendChild(za);Q.appendChild(Ga);Q.appendChild(hb);Q.appendChild(Ra);n.getVideo().setContainer(ga);n.addEventListener("fullscreenchange",Z);for(j=q.length;j--;)document.addEventListener(q[j],Z,!1);d.removeEventListener("resize",A);d.addEventListener("resize",
            A,!1);b&&(d.removeEventListener("orientationchange",A),d.addEventListener("orientationchange",A,!1));l(r.id).onAdPlay(function(){O.adMode(!0);Xa(e.PLAYING);x()});l(r.id).onAdSkipped(function(){O.adMode(!1)});l(r.id).onAdComplete(function(){O.adMode(!1)});l(r.id).onAdError(function(){O.adMode(!1)});r.jwAddEventListener(f.JWPLAYER_PLAYER_STATE,Da);r.jwAddEventListener(f.JWPLAYER_MEDIA_ERROR,kb);r.jwAddEventListener(f.JWPLAYER_PLAYLIST_COMPLETE,ob);r.jwAddEventListener(f.JWPLAYER_PLAYLIST_ITEM,pb);r.jwAddEventListener(f.JWPLAYER_CAST_AVAILABLE,
            function(){c.canCast()?ja.forceControls(!0):ja.releaseControls()});r.jwAddEventListener(f.JWPLAYER_CAST_SESSION,function(a){ba||(ba=new l.html5.castDisplay(r.id),ba.statusDelegate=function(a){ba.setState(a.newstate)});a.active?(h.style(B.element(),{display:"none"}),ja.forceControls(!0),ba.setState("connecting").setName(a.deviceName).show(),r.jwAddEventListener(f.JWPLAYER_PLAYER_STATE,ba.statusDelegate),r.jwAddEventListener(f.JWPLAYER_CAST_AD_CHANGED,N)):(r.jwRemoveEventListener(f.JWPLAYER_PLAYER_STATE,
            ba.statusDelegate),r.jwRemoveEventListener(f.JWPLAYER_CAST_AD_CHANGED,N),ba.hide(),O.adMode()&&P(),h.style(B.element(),{display:null}),Da({newstate:r.jwGetState()}),A())});Da({newstate:e.IDLE});b||(la.addEventListener("mouseout",G,!1),la.addEventListener("mousemove",M,!1),c.isMSIE()&&(ga.addEventListener("mousemove",M,!1),ga.addEventListener("click",V.clickHandler)));F(O);F(ca);F($);h("#"+Q.id+"."+k+" .jwaspect",{"margin-top":n.aspectratio,display:C});j=c.exists(n.aspectratio)?parseFloat(n.aspectratio):
            100;s=n.playlistsize;h("#"+Q.id+".playlist-right .jwaspect",{"margin-bottom":-1*s*(j/100)+"px"});h("#"+Q.id+".playlist-right .jwplaylistcontainer",{width:s+"px",right:0,top:0,height:"100%"});h("#"+Q.id+".playlist-bottom .jwaspect",{"padding-bottom":s+"px"});h("#"+Q.id+".playlist-bottom .jwplaylistcontainer",{width:"100%",height:s+"px",bottom:0});h("#"+Q.id+".playlist-right .jwmain",{right:s+"px"});h("#"+Q.id+".playlist-bottom .jwmain",{bottom:s+"px"});setTimeout(function(){S(n.width,n.height)},0)}};
    var ka=this.fullscreen=function(a){c.exists(a)||(a=!n.fullscreen);a=!!a;a!==n.fullscreen&&(ya?(a?Qa.apply(Q):qa.apply(document),Y(Q,a)):c.isIE()?Y(Q,a):(U&&U.getVideo().setFullScreen(a),n.getVideo().setFullScreen(a)))};this.resize=function(a,b){S(a,b,!0);A()};this.resizeMedia=W;var bb=this.completeSetup=function(){h.style(Q,{opacity:1});d.addEventListener("beforeunload",function(){n.getVideo().isCaster||r.jwStop()})},Ta;this.setupInstream=function(a,b,c,d){h.unblock();Za(ab("jwinstream"),!0);Za(ab("jwcontrols"),
        !1);za.appendChild(a);Ha=b;Fa=c;U=d;Da({newstate:e.PLAYING});ua=!0;za.addEventListener("mousemove",M);za.addEventListener("mouseout",G)};this.destroyInstream=function(){h.unblock();Za(ab("jwinstream"),!1);Za(ab("jwcontrols"),!0);za.innerHTML="";za.removeEventListener("mousemove",M);za.removeEventListener("mouseout",G);ua=!1};this.setupError=function(a){aa=!0;l.embed.errorScreen(Q,a,n);bb()};this.addButton=function(a,b,c,d){ca&&(ca.addButton(a,b,c,d),r.jwGetState()===e.IDLE&&ra())};this.removeButton=
        function(a){ca&&ca.removeButton(a)};this.setControls=function(a){var b=!!a;b!==n.controls&&(n.controls=b,ua?a?(Ha.show(),Fa.show()):(Ha.hide(),Fa.hide()):b&&Da({newstate:r.jwGetState()}),b||(Aa(),V&&V.hide()),ja.sendEvent(f.JWPLAYER_CONTROLS,{controls:b}))};this.forceControls=function(a){ia=!!a;a?Ma():Aa()};this.releaseControls=function(){ia=null;Xa(r.jwGetState())};this.addCues=function(a){O&&O.addCues(a)};this.forceState=function(a){V.forceState(a)};this.releaseState=function(){V.releaseState(r.jwGetState())};
    this.getSafeRegion=function(a){var b={x:0,y:0,width:0,height:0};a=a||!c.exists(a);O.showTemp();ca.showTemp();var d=j(Ga),e=d.top,f=ua?j(document.getElementById(r.id+"_instream_controlbar")):j(O.element()),g=ua?!1:0<ca.numButtons(),h=0===$.position().indexOf("top"),k=j($.element());g&&n.controls&&(g=j(ca.element()),b.y=Math.max(0,g.bottom-e));h&&(b.y=Math.max(b.y,k.bottom-e));b.width=d.width;b.height=f.height&&a&&n.controls?(h?f.top:k.top)-e-b.y:d.height-b.y;O.hideTemp();ca.hideTemp();return b};this.destroy=
        function(){d.removeEventListener("resize",A);d.removeEventListener("orientationchange",A);for(var a=q.length;a--;)document.removeEventListener(q[a],Z,!1);n.removeEventListener("fullscreenchange",Z);Q.removeEventListener("keydown",u,!1);fa&&fa.destroy();ba&&(r.jwRemoveEventListener(f.JWPLAYER_PLAYER_STATE,ba.statusDelegate),ba.destroy(),ba=null);la&&(la.removeEventListener("mousemove",M),la.removeEventListener("mouseout",G));ga&&(ga.removeEventListener("mousemove",M),ga.removeEventListener("click",
            V.clickHandler));ua&&this.destroyInstream()};Q=t("div","jwplayer playlist-"+n.playlistposition);Q.id=r.id;Q.tabIndex=0;Qa=Q.requestFullscreen||Q.webkitRequestFullscreen||Q.webkitRequestFullScreen||Q.mozRequestFullScreen||Q.msRequestFullscreen;qa=document.exitFullscreen||document.webkitExitFullscreen||document.webkitCancelFullScreen||document.mozCancelFullScreen||document.msExitFullscreen;ya=Qa&&qa;n.aspectratio&&(h.style(Q,{display:"inline-block"}),Q.className=Q.className.replace("jwplayer","jwplayer "+
        k));var Ua=document.getElementById(r.id);Ua.parentNode.replaceChild(Q,Ua)};h(".jwplayer",{position:"relative",display:"block",opacity:0,"min-height":0,"-webkit-transition":"opacity .25s ease","-moz-transition":"opacity .25s ease","-o-transition":"opacity .25s ease"});h(".jwmain",{position:"absolute",left:0,right:0,top:0,bottom:0,"-webkit-transition":"opacity .25s ease","-moz-transition":"opacity .25s ease","-o-transition":"opacity .25s ease"});h(".jwvideo, .jwcontrols",{position:"absolute",height:"100%",
    width:"100%","-webkit-transition":"opacity .25s ease","-moz-transition":"opacity .25s ease","-o-transition":"opacity .25s ease"});h(".jwvideo",{overflow:s,visibility:s,opacity:0});h(".jwvideo video",{background:"transparent",height:"100%",width:"100%",position:"absolute",margin:"auto",right:0,left:0,top:0,bottom:0});h(".jwplaylistcontainer",{position:"absolute",height:"100%",width:"100%",display:v});h(".jwinstream",{position:"absolute",top:0,left:0,bottom:0,right:0,display:"none"});h(".jwaspect",
    {display:"none"});h(".jwplayer."+k,{height:"auto"});h(".jwplayer.jwfullscreen",{width:"100%",height:"100%",left:0,right:0,top:0,bottom:0,"z-index":1E3,margin:0,position:"fixed"},!0);h(".jwplayer.jwfullscreen.jw-user-inactive",{cursor:"none","-webkit-cursor-visibility":"auto-hide"});h(".jwplayer.jwfullscreen .jwmain",{left:0,right:0,top:0,bottom:0},!0);h(".jwplayer.jwfullscreen .jwplaylistcontainer",{display:v},!0);h(".jwplayer .jwuniform",{"background-size":"contain !important"});h(".jwplayer .jwfill",
    {"background-size":"cover !important","background-position":"center"});h(".jwplayer .jwexactfit",{"background-size":"100% 100% !important"})})(window);
(function(d,l){function a(a){return"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAA"+q[a]}function c(a,b){var c=l.createElement(a);b&&f(c,b);return c}function f(a,b){m.isArray(b)||(b=[b]);b=m.map(b,function(a){return!a?"":"jwcast-"+a.toLowerCase()});a.className=b.join(" ")}function e(a,b){b.join||(b=[b]);for(var c=0;c<b.length;c++)a.appendChild(b[c])}var h=d.utils,j=d.html5,b=d.events,m=d._,g=b.state,k=h.css,q={wheel:"DgAAAA4CAYAAACohjseAAACiUlEQVR42u3aP2sTYRzAcZ87Md6mhE5GhRqli0NC22yNKO1iaStSY+ggdKggal6BDXRoUuwbEG1LpE4B30LAxEGbKYgO7SVoUhJD04hOusRv4ZlCwP5LevfDgw9kCnzD5Z4/95xqtVqideNLTQzjKV4gCxtNtNwaqBBGCg3UkcYz3EUIV+F1W6AHj7CFb1hAEIbbb1GFByjjAyZgSvkPXkMGW7gt7SETwQ8swpL0FFV4jjpuShsmTiOFz7gobRxUWEceXokDfQKf0CdxJhNFFT6JU7Ur2MUtiXNRhXdYlDrZnkERZyUGerCNcanLpYfISV0PGtjEpNTAGyjBkBq4ggWpWxYmGghIDRzEDgypgTG8lbyrtoZ5yYFZ3JccWMKg5MCfGJAcuHf5/ge6xwX8lnyLDmCn/SEzJChwCKX2YSIqKDCKbPtAHxcUGAdNOhBPkBYUmAZNOhDXUYMSEKdQBU06EAp1BAUEBnWLgg4EXmJJQOASXnVa0YdRcfma0NAN4U6BCpu44+LASd2g0BYIPEbexYHvdQOfOwdaqLh063AcFVj73bq3XBRnoYiZ/b58ySDposAkMlD/DNT8aGLUBXGjaMJ/0Beg9/Dd4etEH2qIHOUVdgHnHRh3DgUkjnoIIYUNh0V6sYHXUIcO1Eyso4BLDoi7jC94A/O4DgIZWEYdYycYN4YalmF04yjXNJpIwOrxOJdAE9PdPoznRxZFTPUgbgI2svD38jjlLMrI61DjmFcFU/iICmZhnMSB2DOYg41tJBGAOuSPFkASZdiYg8cpR5pHsIIGqkgjjghC6Eef1o8QIphHGlU0sIYRGE4/lB7DKnL4il/Yu/5gFzZyWEUMwzC7sXUv2l9q1CPRZSGkLwAAAABJRU5ErkJggg\x3d\x3d",
        display:"UAAAAC4AQMAAACo6KcpAAAABlBMVEV6enp6enqEWMsmAAAAAXRSTlMAQObYZgAAAEdJREFUeF7t2bEJACAMRcGAg7j/Fo6VTkvbIKSRe/XBH+DHLlaHK0qN7yAIgiAIgiAIgiAIgiAIgiAIgiAIgg0PZHfzbuUjPCPnO5qQcE/AAAAAAElFTkSuQmCC",pause:"CoAAAA2CAQAAAAb3sMwAAAAMElEQVR4Ae3MMQEAMAzDsIY/6AxB9/aRfyvt7GX2Ph8UCoVCoVAo9AiFQqFQKBQKfdYvoctOjDeGAAAAAElFTkSuQmCC",play:"DYAAAA2BAMAAAB+a3fuAAAAFVBMVEX///////////////////////////9nSIHRAAAABnRSTlMAP79AwMFfxd6iAAAAX0lEQVR4Xn3JQQGAABAEoaliFiPYYftHMMHBl55uQw455JBDDjnkkEMOOeSQQw455JBDDjnkkEMOOeSQQ+5O3HffW6hQoUKFChUqVKhQoUKFChUqVKhQoUKFChUqVKgfWHsiYI6VycIAAAAASUVORK5CYII\x3d",
        replay:"DQAAAA8CAYAAAApK5mGAAADkklEQVRoBd3BW2iVBRwA8P/cWHMsv9QilLCITLCU0khpST6JCEXrQbKMCgrKFwsfZq/LMnRRIdkFvBQUvmShgg9iV02zB7FScyWlqNHNqbCJ7PKLkFHp952dnZ3tfOv3ixgGSLAVt8b/ARIX9WADJsVIhsR/daIV42MkQiJdO5ZjdIwkSBR2Ek+gJkYCJIpzEE2Rd0gMzB7MibxCojRbcEtUGsZgJu7HYixVuh6sx6QYLrgSD+Fd/GhodKIV42Ko4B68h07Dpx3NGB3lgnnYpbJOYFoMBm7ANpW3D3NjMPAgzqqsn7EIVVEqVGOtymrHMtTGYKAeWxSvB3vxIh7ANIzFNUpzAa0YF4OFWuxUnFNYjkmRAomB6cX7uDHKAdX4QP/asRRXRAFIFO8TzI5yQov+bcO1UQQk+ncITVFumIce2XqxHFVRJCSy/YolqIlyQwOOy9aNR2KAkLhcJ1agIYYKVsvWi6eiBEj8owfrMDEGAVVYiMcjDa7HBdlejhIhcdF2TI9BQiP2uOgsro5LYa1sX6M2SoQ6zItBwmRsdrnn498wDuel68aMqDBMQZd0v6Mu+mCJbBsiJ7BdtkXRB7ul68HNkRNolO3D+BvGoke6HZEz+Fa6c6gJNMn2WOQMmmW7K/CSbBMiZ3CbbM8EPpKuLXIIo3BWujcCh6TbEjmFr6TbGfhDulcip7BJugOBbulaIqfwlnRHQ7bnIqewVrpjgU7pVkZOYaN0hwOnpFsfOYWt0u0LfCnd55FT+EG6zYEN0p1BdeQMEnRLtzKwTLZZkTO4V7bFgTtka4mcwTrZrgtU47R0P6E6cgINOCfdkeiDjbItipzAs7K1Rh/Mle0gaqLC0IBTsk2PPhiFI7ItiwrDKtl2xaXwqGwdmBoVgrvRJdv8uBRq0CbbISQxzDARJ2TbG1kwX2GfoT6GCa7CN7J1Y0YUgk0K+wJjY4hhAg4o7LXoD8bjuMIOY1oMETTiuMIOoj6KgTvRobDzaEZtlAnq8QK6FHYGU2IgcB+69e97LEJNlAh1eBrH9K8DjVEKPIxuxTmJVZiFmugHajEHa/Cb4nRiQQwGmtBpYM7hU7yNFjSjGSuwDrvRYWD+RGOUA25Hm8rZj8lRThiDd9Br+PTgVdTFUMFcfGfo7cHMGA4YhYXYr/x2YQGqohIwG2vwi9Idw2pMjzzBVCzBm/gYR3EaXbiA02jDDryOJ3FTlNFfAO8ENqnn13UAAAAASUVORK5CYII\x3d"},
    s=!1,v=316/176;j.castDisplay=function(q){function m(){if(J){var a=J.element();a.parentNode&&a.parentNode.removeChild(a);J.resetEventListeners();J=null}}function n(){K&&(K.parentNode&&K.parentNode.removeChild(K),K=null)}function y(){H&&(H.parentNode&&H.parentNode.removeChild(H),H=null)}s||(k(".jwplayer .jwcast-display",{display:"none",position:"absolute",width:"100%",height:"100%","background-repeat":"no-repeat","background-size":"auto","background-position":"50% 50%","background-image":a("display")}),
    k(".jwplayer .jwcast-label",{position:"absolute",left:10,right:10,bottom:"50%","margin-bottom":100,"text-align":"center"}),k(".jwplayer .jwcast-label span",{"font-family":'"Karbon", "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif',"font-size":20,"font-weight":300,color:"#7a7a7a"}),k(".jwplayer span.jwcast-name",{color:"#ccc"}),k(".jwcast-button",{position:"absolute",width:"100%",height:"100%",opacity:0,"background-repeat":"no-repeat",
    "background-size":"auto","background-position":"50% 50%"}),k(".jwcast-wheel",{"background-image":a("wheel")}),k(".jwcast-pause",{"background-image":a("pause")}),k(".jwcast-play",{"background-image":a("play")}),k(".jwcast-replay",{"background-image":a("replay")}),k(".jwcast-paused .jwcast-play",{opacity:1}),k(".jwcast-playing .jwcast-pause",{opacity:1}),k(".jwcast-idle .jwcast-replay",{opacity:1}),h.cssKeyframes("spin","from {transform: rotate(0deg);} to {transform: rotate(360deg);}"),k(".jwcast-connecting .jwcast-wheel, .jwcast-buffering .jwcast-wheel",
    {opacity:1,"-webkit-animation":"spin 1.5s linear infinite",animation:"spin 1.5s linear infinite"}),k(".jwcast-companion",{position:"absolute","background-position":"50% 50%","background-size":"316px 176px","background-repeat":"no-repeat",top:0,left:0,right:0,bottom:4}),k(".jwplayer .jwcast-click-label",{"font-family":'"Karbon", "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif',"font-size":14,"font-weight":300,"text-align":"center",position:"absolute",
    left:10,right:10,top:"50%",color:"#ccc","margin-top":100,"-webkit-user-select":"none","user-select":"none",cursor:"pointer"}),k(".jwcast-paused .jwcast-click-label",{color:"#7a7a7a",cursor:"default"}),s=!0);var E=l.getElementById(q+"_display_button"),u=c("div","display"),p=c("div",["pause","button"]),z=c("div",["play","button"]),w=c("div",["replay","button"]),A=c("div",["wheel","button"]),F=c("div","label"),L=c("span"),G=c("span","name"),t="#"+q+"_display.jwdisplay",x=-1,M=null,J=null,H=null,K=null;
    e(u,[A,p,z,w,F]);e(F,[L,G]);E.parentNode.insertBefore(u,E);this.statusDelegate=null;this.setName=function(a){G.innerText=a||"Google Cast";return this};this.setState=function(a){var b="Casting to ";if(null===M)if("connecting"===a)b="Loading on ";else if(a!==g.IDLE){var c=d(q).getPlaylistItem().title||"";c&&(b=b.replace("to",c+" to"))}L.innerText=b;clearTimeout(x);a===g.IDLE&&(x=setTimeout(function(){f(u,["display","idle"])},3E3),a="");f(u,["display",a||""]);return this};this.show=function(){k(t+" .jwpreview",
        {"background-size":"316px 176px !important",opacity:0.6,"margin-top":-2});k(t+" .jwdisplayIcon",{display:"none !important"});k.style(u,{display:"block"});return this};this.hide=function(){h.clearCss(t+" .jwpreview");k(t+" .jwdisplayIcon",{display:""});k.style(u,{display:"none"});return this};this.setSkipoffset=function(a,c){if(null===J){var d=l.getElementById(q+"_controlbar"),e=10+h.bounds(u).bottom-h.bounds(d).top;J=new j.adskipbutton(q,e|0,a.skipMessage,a.skipText);J.addEventListener(b.JWPLAYER_AD_SKIPPED,
        function(){c(a)});J.reset(a.skipoffset||-1);J.show();d.parentNode.insertBefore(J.element(),d)}else J.reset(a.skipoffset||-1)};this.setCompanions=function(a){var b,d,f,g=Number.MAX_VALUE,h=null;for(d=a.length;d--;)if(b=a[d],b.width&&b.height&&b.source)switch(b.type){case "html":case "iframe":case "application/x-shockwave-flash":break;default:f=Math.abs(b.width/b.height-v),f<g&&(g=f,0.75>f&&(h=b))}(a=h)?(null===H&&(H=c("div","companion"),e(u,H)),a.width/a.height>v?(b=316,d=a.height*b/a.width):(d=176,
        b=a.width*d/a.height),k.style(H,{"background-image":a.source,"background-size":b+"px "+d+"px"})):y()};this.adChanged=function(a){if(a.complete)J&&J.reset(-1),M=null;else{J&&(void 0===a.skipoffset?m():(a.position||a.duration)&&J.updateSkipTime(a.position|0,a.duration|0));var b=a.tag+a.sequence;b!==M&&(k(t+" .jwpreview",{opacity:0}),a.companions?this.setCompanions(a.companions):y(),a.clickthrough?null===K&&(K=c("div","click-label"),K.innerText="Click here to learn more \x3e",e(u,K)):n(),M=b,this.setState(a.newstate))}};
    this.adsEnded=function(){m();y();n();k(t+" .jwpreview",{opacity:0.6});M=null};this.destroy=function(){this.hide();u.parentNode&&u.parentNode.removeChild(u)}}})(jwplayer,document);
(function(d){var l=jwplayer.utils.extend,a=d.logo;a.defaults.prefix="";a.defaults.file="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHoAAAAyCAMAAACkjD/XAAACnVBMVEUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAJCQkSEhIAAAAaGhoAAAAiIiIrKysAAAAxMTEAAAA4ODg+Pj4AAABEREQAAABJSUkAAABOTk5TU1NXV1dcXFxiYmJmZmZqamptbW1xcXF0dHR3d3d9fX2AgICHh4eKioqMjIyOjo6QkJCSkpKUlJSWlpaYmJidnZ2enp6ioqKjo6OlpaWmpqanp6epqamqqqqurq6vr6+wsLCxsbG0tLS1tbW2tra3t7e6urq7u7u8vLy9vb2+vr6/v7/AwMDCwsLFxcXFxcXHx8fIyMjJycnKysrNzc3Ozs7Ozs7Pz8/Pz8/Q0NDR0dHR0dHS0tLU1NTV1dXW1tbW1tbW1tbX19fX19fa2trb29vb29vc3Nzc3Nzf39/f39/f39/f39/g4ODh4eHj4+Pj4+Pk5OTk5OTk5OTk5OTl5eXn5+fn5+fn5+fn5+fn5+fo6Ojo6Ojq6urq6urq6urr6+vr6+vr6+vt7e3t7e3t7e3t7e3u7u7u7u7v7+/v7+/w8PDw8PDw8PDw8PDy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vL09PT09PT09PT09PT09PT09PT09PT29vb29vb29vb29vb29vb29vb29vb29vb39/f39/f39/f39/f39/f4+Pj4+Pj4+Pj5+fn5+fn5+fn5+fn5+fn5+fn5+fn6+vr6+vr6+vr6+vr6+vr6+vr8/Pz8/Pz8/Pz8/Pz8/Pz8/Pz9/f39/f39/f39/f39/f39/f39/f39/f39/f3+/v7+/v7+/v7+/v7+/v7+/v7+/v7+/v7///////////////9kpi5JAAAA33RSTlMAAQIDBAUGBwgJCgsMDQ4PEBESExQVFhYWFxcYGBgZGRoaGhsbHBwdHR4eHx8gISIiIyQmJicoKSoqKywtLi4uMDEyMjM0NTU2Njc5Ojo7Ozw9Pj5AQUJCQ0ZGSElKSktMTU5PUFFRUlRVVlZXWFpbXV5eX2BhYmVmZ2hpamtsbW5vcHFyc3R2d3h5enx9fn+BgoKDhIWGiYmKi4yNjo+QkZKTlJWWl5eYmZqbnJ2enp+goaKkpaamp6ipqqusra6vsLKzs7W2t7i5uru8vb6/wMHCwsPExcbHyMnJysvMVK8y+QAAB5FJREFUeNrFmP2f3EQdx8kmm2yy2WQzmZkjl3bJ2Rb12mtp8SiKiBUUxVKFVisIihV62CKCIoK0UvVK1bP07mitBeVJUVso0Duw1Xo9ET0f6JN47bV3u9+/xe83kyzr0+vlL7t8Xq9ubpLpvHfm+7i54P+UVkBp2gWdFpGNYtFA+NtALpYcxzZ1rSM0TSvgv5xse0wwu1joxDYLulE0dKTTSLcqfOvMQ1WzoHXAtCadsGXqBCsUnWDxNBzmlq51wLSuz0LmOcTWClZFfA1ghLUbrUwbdq396kAvK5s6HoFdlb8FuLONB66RlGnD5S8BwKkNoVMsFEw3XIOj97hmoX2updP5kml7jgLp/Ec8yzBKntwDMCnwa7TPtUrkWLrliW2gtC+0TdNhvdMAu1hJ19plYNcP0LGKiJp/HJTeEI5V8sjJ4PZ2mTp1rb7Pf5C5JbvCN0Cuha7jpE5WX9oeU6us8YlTUH8grFQC+QzkWuKVvdTJXuWO0Z5Nk2tNkWNdzgLed+4tdNWrkpPBI20ytVYwK+LrQLpPcHk3vIVm1ZCcDD7jt8fUGmYNoeLpJzKW+1vQYSjJyc72ZKbWSOqqhpn+99r/rn99WDDLbJViHZbJirkWtJDkZPArbhta2jFg7LdKV1ID9aWaz5CTzTD0pvB2aypB9xYPKtaUXEC7bKKjeA1dHyJTU+xbFgY/RiAKP2lYsm28RaJmAtfTs6c4xP9g0gycUqKpeDGLegZPl3MqTL6oWCdl9EIrOol20/U6zyzgVJzpeV6l7Dhl18VP1/N8v1r1vQoNSziH1nPKKMdBChbAiprheygfL65tZmxazguYXDoL8BcyqlhRb0W/M3Wy412YRTUd7SKEFIKzIBQ8DBhHewgSjkLB7GwS54wxwcoORqYQ+QyhFGA9VIYxnfCKq2VtE3k3wTB1taLx+FVCNTRyxnU4YQ/8WEY9M7PvkvJHsEsAam5srRRwH0YBhml14Zv7pRz62+LAD/jWE0vHINU6OUGXyc0Mt5GiLW/+6blV8eO4tY8B6t3qvBsZOnUy+HJgFaiuMELfhQ6RrAe4JZGvwxcFPLx69YZDZ1ciOrB03ayEd52vr0x6/zokhbxs+p5o7Oc3kfrkxFOrV392d+NWFaeaXvK652Cw+xTAo9cS5ar0vKcfy9BrgNRfMVN0SOh+gPfWtgN8L7kM6pcI2FSrJUtm7kc0KxlF2xcHd/1xWxxvmv1QLB9/5cJobDiKIxklcmI4ShJ5eJ/qOTSqU6/BBC4JN6boQSAN71Doi1Mnm+B0Rjlavgabo/GZ2V/LL8FRSehkkfzzYIouoqXf31jz3de7kq5DB6JP1a+vSUQnOXrRoujpn2XogumJpwCeBfhDV4qeAdK1QwqdOhkMqdAyyyk6HoHR3tmD4/UlI/DDBNFxHK1tDBDaNrHODU7KDzTW16Lr6nccHZGxHNt3Jao/RrSU8pPTeX+JPYj4NpAGkxsg16FoWP1xP5Bu8UwdYxSXJXRyJ0zeCtsegdsm4QsLBBwcHf3l+fF5hHbscnDh1LeSaGwvModnTl7ChVRuNiblxIkjR6bq+9+R9RzkO7cBadWCdZBroDaq/jgDqHMLMYtSr8jkpwl9aaOxF9bdDHsb9T5Ev/rkk6N398SIDj3X5zfDzi1bDpxdHNWWwcOchS27funeR+EOyTI0RcyKLIM20VPzyOObeh4LJsZ/hYnaRpgRsTwG9TPzLz5XhyOSDlzykDEKLsEYl08cG0W9eW+U4B1eZZmtY7J13PXCeHeg0MrPjlH8yLiJ/mYtfqIFvQVNTaez/cMrfwHHpJC7APZH0csAP5ARokPPwXyIoEjKaOnM7UIIOfKKrJEJvEAguhZHUY1sHb3vH1tCxyS0OvGtAL+/iMubQOlMXyKfA6U8i+I0PqWyecA3AmyVEmPhczxEdBUbOKwCsHsAtfNUDyZNdiNcLQld8cTYgQHScjExjNPvOf9RSsrZtt3uB3f2s0Dku35MyiY6z6LYjbMdx+HvO7pd11/egBtCvh7mFvs+P70Rl8L0yU8r7WROyXb5b77Dxemv+I7L82wmxoeY53U9+/K8HE1ZvBq4eGQfh1SNa0Keo5tZVCXwXs7KluUwIZjrMsrHTsB95f4B50JwztGURtHywsBjvGphtIUiFeb9Kn4pjzHXUOhmlXPI3Ug/5QH6BjS1uWpRRdLNku3YWPNw4RKVSSqfpKLq3k3bIZXMvFha+NjQqXqlhYxKa9EgFJGVqKCrqD2ZloJrql7Qgq4vw9DKfn0ahp73B+ln3hPQY/xKJEO1CC2P6T49UOP/fD+R5qphSBvAslttQb8YZr1os7/5ry0P8VDNoZK6T8pnZpdW4bb9ZWPQ2NPtlhxf/A5yPUApt+0/MP2uqy5nLkaKLyZycuOKCp13u9mWXXasol4staAPYyprN1p5CvkR1nD5pxz9jQDPu1Pvbii3yklQmr2U/LtDUr9Fngelp0NqwDsmirPtoLRWJdxOiQrp9Yr8XGiTk3XyxF2eFuw3+ju5aRJl1Yu+f+LMM1eiexc6/lK0QuWpYhkd3XT+UsfOXhd2WKpO6W/TO3BUO8H/BB7RwuB6W7b7AAAAAElFTkSuQmCC";d.logo=
    function(c,d){"free"==c.edition()?d=null:(a.defaults.file="",a.defaults.prefix="");l(this,new a(c,d))}})(jwplayer.html5);(function(d){var l=d.html5,a=l.model;l.model=function(c,f){var e=new d.utils.key(c.key),h=new a(c,f),j=h.componentConfig;h.edition=function(){return e.edition()};h.componentConfig=function(a){return"logo"==a?h.logo:j(a)};return h}})(jwplayer);
(function(d){var l=d.html5,a=l.player;l.player=function(c){c=new a(c);var f;f=c._model.edition();if("enterprise"===f||"ads"===f)f=new d.cast.controller(c,c._model),c.jwStartCasting=f.startCasting,c.jwStopCasting=f.stopCasting,c.jwOpenExtension=f.openExtension;return c};a.prototype.edition=function(){return this._model.edition()}})(jwplayer);
(function(d){function l(f){if(!a.isFunction(f.supports))throw{message:"Tried to register a provider with an invalid object"};var e=function(){};e.prototype=d.html5.DefaultProvider;f.prototype=new e;c.unshift(f)}var a=d._,c=[d.html5.YoutubeProvider,d.html5.VideoProvider];a.each(d.unregisteredProviders,l);delete d.unregisteredProviders;d.html5.chooseProvider=function(d){d=a.isObject(d)?d:{};return a.find(c,function(a){return a.supports(d)})};d.html5.registerProvider=l})(jwplayer);
(function(d){var l=jwplayer.utils.extend,a=d.rightclick;d.rightclick=function(c,f){if("free"==c.edition())f.aboutlink="http://www.longtailvideo.com/jwpabout/?a\x3dr\x26v\x3d"+d.version+"\x26m\x3dh\x26e\x3df",delete f.abouttext;else{if(!f.aboutlink){var e="http://www.longtailvideo.com/jwpabout/?a\x3dr\x26v\x3d"+d.version+"\x26m\x3dh\x26e\x3d",h=c.edition();f.aboutlink=e+("pro"==h?"p":"premium"==h?"r":"enterprise"==h?"e":"ads"==h?"a":"f")}f.abouttext?f.abouttext+=" ...":(e=c.edition(),e=e.charAt(0).toUpperCase()+
    e.substr(1),f.abouttext="About JW Player "+d.version+" ("+e+" edition)")}l(this,new a(c,f))}})(jwplayer.html5);
(function(d){var l=d.cast,a=d.utils;l.adprovider=function(c,f){function e(){m={message:g,position:0,duration:-1}}function h(b,d){var e={command:b};void 0!==d&&(e.args=d);f.sendMessage(c,e,a.noop,function(a){l.error("message send error",a)})}var j=new l.provider(c,f),b=a.extend(this,j),m,g="Loading ad",k=0;b.init=function(){j.init();e()};b.destroy=function(){j.destroy()};b.updateModel=function(a,b){(a.tag||a.newstate||a.sequence||a.companions)&&l.log("received ad change:",a);a.tag&&(m.tag&&a.tag!==
m.tag)&&(l.error("ad messages not received in order. new model:",a,"old model:",m),e());d.utils.extend(m,a);j.updateModel(a,b)};b.getAdModel=function(){var b=a.extend({},m);if(0<m.duration){var c=m,d=c.message.replace(/xx/gi,""+Math.min(c.duration|0,Math.ceil(c.duration-c.position)));c.podMessage&&1<c.podcount&&(d=c.podMessage.replace(/__AD_POD_CURRENT__/g,""+c.sequence).replace(/__AD_POD_LENGTH__/g,""+c.podcount)+d);b.message=d}else b.message=g;return b};b.resetAdModel=function(){e()};b.skipAd=function(a){h("skipAd",
    {tag:a.tag})};b.clickAd=function(a){k=(new Date).getTime();h("clickAd",{tag:a.tag})};b.timeSinceClick=function(){return(new Date).getTime()-k}}})(window.jwplayer);
(function(d){var l=d.cast,a=d.utils,c=d.events,f=d._,e=c.state;l.provider=function(d){function j(a){g.oldstate=g.newstate;g.newstate=a;b.sendEvent(c.JWPLAYER_PLAYER_STATE,{oldstate:g.oldstate,newstate:g.newstate})}var b=a.extend(this,new c.eventdispatcher("cast.provider")),m=-1,g={newstate:e.IDLE,oldstate:e.IDLE,buffer:0,position:0,duration:-1,audioMode:!1},k=document.createElement("div");k.className="jwcast-screen";k.onclick=function(){b.sendEvent(c.JWPLAYER_PROVIDER_CLICK)};b.isCaster=!0;b.init=
    function(){};b.destroy=function(){clearTimeout(m);_castSession=null};b.updateModel=function(a,d){a.newstate&&(g.newstate=a.newstate,g.oldstate=a.oldstate||g.oldstate,b.sendEvent(c.JWPLAYER_PLAYER_STATE,{oldstate:g.oldstate,newstate:g.newstate}));if("ad"!==d){if(void 0!==a.position||void 0!==a.duration)void 0!==a.position&&(g.position=a.position),void 0!==a.duration&&(g.duration=a.duration),b.sendEvent(c.JWPLAYER_MEDIA_TIME,{position:g.position,duration:g.duration});void 0!==a.buffer&&(g.buffer=a.buffer,
    b.sendEvent(c.JWPLAYER_MEDIA_BUFFER,{bufferPercent:g.buffer}))}};b.supportsFullscreen=function(){return!1};b.setup=function(a,b){b.state&&(g.newstate=b.state);void 0!==b.buffer&&(g.buffer=b.buffer);void 0!==a.position&&(g.position=a.position);void 0!==a.duration&&(g.duration=a.duration);j(e.BUFFERING);d("setup",a)};b.playlistItem=function(a){j(e.BUFFERING);d("item",a)};b.load=function(a){j(e.BUFFERING);d("load",a)};b.stop=function(){clearTimeout(m);m=setTimeout(function(){j(e.IDLE);d("stop")},0)};
    b.play=function(){d("play")};b.pause=function(){j(e.PAUSED);d("pause")};b.seek=function(a){j(e.BUFFERING);b.sendEvent(c.JWPLAYER_MEDIA_SEEK,{position:g.position,offset:a});d("seek",a)};b.audioMode=function(){return g.audioMode};b.sendCommand=function(a,b){d(a,b)};b.detachMedia=function(){l.error("detachMedia called while casting");return document.createElement("video")};b.attachMedia=function(){l.error("attachMedia called while casting")};var q;b.setContainer=function(a){a.appendChild(k);q=a};b.getContainer=
        function(){return q};b.remove=function(){q.removeChild(k)};b.volume=b.mute=b.setControls=b.setCurrentQuality=b.resize=b.seekDrag=b.addCaptions=b.resetCaptions=b.setVisibility=b.fsCaptions=a.noop;b.setFullScreen=b.getFullScreen=b.checkComplete=f.constant(!1);b.getWidth=b.getHeight=b.getCurrentQuality=f.constant(0);b.getQualityLevels=f.constant(["Auto"])};a.css(".jwplayer .jwcast-screen",{width:"100%",height:"100%"})})(window.jwplayer);
$(document).ready(function () {

    var player = $("#radio")[0];
    if (player) {
        let isPlaying = false;
        $(document).on("click", "#play", function () {
            if (player.paused) {
                player.play();
                $("#play").addClass('paused');
            } else {
                player.pause();
                $("#play").removeClass('paused');
            }
        });
        function rangeSlider(id, onDrag) {
            var range = document.getElementById(id),
                dragger = range.children[0],
                down = false,
                rangeWidth, rangeLeft;

            range.addEventListener("mousedown", function (e) {
                rangeWidth = this.offsetWidth;
                rangeLeft = e.layerX;
                down = true;
                updateDragger(rangeWidth, rangeLeft);
                return false;
            });

            range.addEventListener("mousemove", function (e) {
                if (down) {
                    rangeWidth = this.offsetWidth;
                    rangeLeft = e.layerX;
                    updateDragger(rangeWidth, rangeLeft);
                }
            });

            range.addEventListener("mouseup", function () {
                down = false;
            });

            function updateDragger(rangeWidth, rangeLeft) {
                dragger.style.width = rangeLeft + 'px';
                if (typeof onDrag == "function") {
                    onDrag(rangeLeft / rangeWidth);
                }
            }
        }

        rangeSlider('volume', function (value) {
            console.log(value);
            player.volume = value;
            // document.getElementById('test-result').innerHTML = value + '%';
        });

        if (Hls.isSupported()) {
            var hls = new Hls();
            hls.loadSource('https://hls-iptv.tattelecom.ru/hls/huzur/radio/azan.m3u8');
            hls.attachMedia(player);
        } else if (player.canPlayType('application/vnd.apple.mpegurl')) {
            player.src = 'https://hls-iptv.tattelecom.ru/hls/huzur/radio/azan.m3u8';
        }
    }

    $(".side-menu .button").click(function () {
        $('.side-menu').toggleClass('active');
        return false;
    });
    var initLastNewsCarousel = function () {
        var $el = $('#last-news'),
            w_height = $('#main-articles').height() - $el.parents('.panel_bordered').height() - $('#secular-articles').height(),
            magic = 62;
        $el.removeClass('hide')
            .jcarousel({
                vertical: true,
                scroll: 2
            });
        if (w_height > 0) {
            $el.parents('.jcarousel-clip').height(w_height - magic);
        }
    };

    // Flexslider
    //------------------------------
    var mainArticles = $('#mainSlider ').flexslider({
        touch: false,
        start: initLastNewsCarousel,
        slideshowSpeed: 5000
    });
    //  if ($('#mainSlider').length == 0) initLastNewsCarousel();

});
