$(function () {
    var validator = $('.ask_form').validate();

    $('.ask_form__message.success').hide();
    $('.ask_form__message.error').hide();

    $('.ask_form_opener').click(function (e) {
        e.preventDefault();
        var $this = $(this),
            askForm = $this.closest(':has(.ask_form)').find('.ask_form');
        $this.slideUp();
        askForm.slideDown();
        validator.resetForm();
    });
    $('.ask_form__hide').click(function (e) {
        e.preventDefault();
        var $this = $(this),
            askForm = $this.parents('.ask_form'),
            askFormOpener = $this.closest(':has(.ask_form_opener)').find('.ask_form_opener');
        askForm.slideUp();
        askFormOpener.slideDown();
    });

    $('.ask_form').ajaxForm({
        beforeSubmit: function checkFormData(formData, jqForm, options) {
            $('.ask_form__message.success').hide();
            $('.ask_form__message.error').hide();
            return true;
        },
        success: function showResponse(responseText, statusText) {

            if (responseText.code == 'ok') {
                $('.ask_form__message.success').show();
                validator.resetForm();
            } else {
                $('.ask_form__message.error').show();
            }
            return;
        }
    });
    if (window.location.hash == "#form") {
        $('.ask_form_opener').trigger("click");
    }

});