(function ($, undefined) {

    $(document).ready(function () {

        $('.sprite.sprite-search').click(function(event){
            event.preventDefault();
            $('.search-block-wrapper').toggleClass('active');
            $('.search-block__field').autocomplete({
                source    : function( request, response ) {
                    var matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( request.term ), "i"),
                        items = [];
                    jQuery.ajax({
                        type: 'GET',
                        async: false,
                        url: '/searchautocompeleter/',
                        data: {'q':  request.term},
                        success: function (data, textStatus, jqXHR) {
                            items = data;
                        }
                    });


                    var matcher = new RegExp("("+$.ui.autocomplete.escapeRegex(request.term )+")", "ig" );
                    response($.map(items, function(item) {
                        return {label: item.replace(matcher, "<b>$1</b>"), value: item};
                    }));
                },
                open          : function() {
                    var autocomplete = $(".ui-autocomplete");
                    var oldTop = autocomplete.offset().top,
                        oldLeft = autocomplete.offset().left,
                        newTop = oldTop + 5,
                        newLeft = oldLeft + 30;

                    autocomplete.css("top", newTop);
                    autocomplete.css("left", newLeft);
                }
            }).data('ui-autocomplete')._renderItem = function(ul, item) {
                return $( "<li></li>" )
                    .data( "item.autocomplete", item )
                    .append( $( "<a></a>" ).html(item.label) )
                    .appendTo( ul );
            };
        });

    });

})(jQuery);
