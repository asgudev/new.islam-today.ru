// iScroll
$(function() {

    try {

        $('[data-iscroll]').each(function() {
            var element = $(this)[0],
                params = {
                    scrollbars: 'custom',
                    mouseWheel: true
                };
            new IScroll(element, params);
        });

    } catch (e) {
        console.log(e);
    }

});


$(function() {
    $(document).on('click', '.js_show_comment', function(ev) {
        $('.koran-book__translation').show();
        $(this).addClass('active');
        $('.js_hide_comment').removeClass('active');

        $(this).empty();
        $(this).append('<span>показать</span>');
        $('.js_hide_comment').empty();
        $('.js_hide_comment').append('<a href="#" class="koran-book__nav__item__target link-dashed">скрыть</a>');

        ev.preventDefault();
    });

    $(document).on('click', '.js_hide_comment', function(ev) {
        $('.koran-book__translation').hide();
        $(this).addClass('active');
        $('.js_show_comment').removeClass('active');

        $(this).empty();
        $(this).append('<span>скрыть</span>');
        $('.js_show_comment').empty();
        $('.js_show_comment').append('<a href="#" class="koran-book__nav__item__target link-dashed">показать</a>');

        ev.preventDefault();
    });


    var player_init = function() {
        var player = playerFactory(),
            playerIndex = 0;

        $('[data-audio]').each(function(){
            var $this = $(this),
                id = 'player-' + (playerIndex++),
                file = $this.data('file');

            $this.attr('id', id);
            if (player) {
                player(id, file);
            }


        });

        function playerFactory() {
            var player = false;
            if ( checkFlashSupport() ) {
                player = Player.flash;
            } else
            if ( checkAudioSupport() ) {
                player = Player.html5;
            } else  {
                // not supported
            }
            return player;
        }

        function checkAudioSupport() {
            var audio  = document.createElement("audio"),
                isAudioSupported = (typeof audio.canPlayType === "function" && audio.canPlayType("audio/mpeg") !== "");
            return isAudioSupported;
        }

        function checkFlashSupport() {
            var isFlashInstalled = false;

            if (typeof(navigator.plugins)!="undefined"&&typeof(navigator.plugins["Shockwave Flash"])=="object") {
                isFlashInstalled = true;
            } else if (typeof window.ActiveXObject != "undefined") {
                try {
                    if (new ActiveXObject("ShockwaveFlash.ShockwaveFlash")) {
                        isFlashInstalled = true;
                    }
                } catch(e) {};
            }

            return isFlashInstalled;
        }
    };

    var comment_scroller_init = function() {
        $('[data-iscroll]').each(function() {
            var element = $(this)[0],
                params = {
                    scrollbars: 'custom',
                    mouseWheel: true
                };
            new IScroll(element, params);
        });
    };


    $(document).on('click', '.js_koran_audio_select', function(ev) {
        var koran_audio_author_id = $(this).data('id');

        $('.js_koran_audio_selected').html($(this).data('name'));
        $('.js_koran_audio_selected').data('author-id', koran_audio_author_id);

        $.each($('.js_audio_file'), function() {

            var page = $(this).data('page');

            $(this).empty();
            $(this).html('<div class="uppod uppod--style-540" data-audio data-file="/files/koran/audio/'+ koran_audio_author_id + '/' + page + '.mp3"></div>');

        });


        player_init();

        ev.preventDefault();
    });


    $(document).on('click', '.js_koran_author_select', function(ev) {
        var koran_author_id = $(this).data('value');

        $('.js_koran_author_selected').html($(this).data('name'));
        $('.js_koran_author_selected').data('author-id', koran_author_id);
        ev.preventDefault();

        jQuery.ajax({
            type: 'POST',
            async: false,
            url: '/koran/api/',
            data: {'koran_author_id':  koran_author_id},
            success: function (data, textStatus, jqXHR) {
                $('.koran_inner_content').html(data);

                comment_scroller_init();
                player_init();
                direction_check();
            }
        });
    });

    $(document).on('click', '.js_koran_surah_select', function(ev) {
        var koran_author_id = $('.js_koran_author_selected').data('author-id');
        var koran_surah_id = $(this).data('surah-id');

        $('.js_koran_surah_selected').html($(this).data('title'));
        $('.js_koran_surah_selected').data('surah-id', koran_surah_id);

        ev.preventDefault();

        jQuery.ajax({
            type: 'POST',
            async: false,
            url: '/koran/api/',
            data: {'koran_author_id':  koran_author_id, 'koran_surah_id': koran_surah_id},
            success: function (data, textStatus, jqXHR) {
                $('.koran_inner_content').html(data);

                comment_scroller_init();
                player_init();

                direction_check();
            }
        });
    });

    $(document).on('click', '.js_koran_juz_select', function(ev) {
        var koran_author_id = $('.js_koran_author_selected').data('author-id');
        var koran_juz_part = $(this).data('part');

        $('.js_koran_juz_selected').html($(this).data('part'));
        $('.js_koran_juz_selected').data('part', koran_juz_part);

        ev.preventDefault();

        jQuery.ajax({
            type: 'POST',
            async: false,
            url: '/koran/api/',
            data: {'koran_author_id':  koran_author_id, 'koran_juz_part': koran_juz_part},
            success: function (data, textStatus, jqXHR) {
                $('.koran_inner_content').html(data);

                comment_scroller_init();
                player_init();

                direction_check();
            }
        });
    });



    $(document).on('click', '.js_koran_ayah_select', function(ev) {
        var koran_author_id = $('.js_koran_author_selected').data('author-id');
        var koran_ayah = $(this).data('ayah');
        var koran_surah_id = $('.js_koran_surah_selected').data('surah-id');

        $('.js_koran_ayah_selected').html($(this).data('ayah'));
        $('.js_koran_ayah_selected').data('ayah', koran_ayah);

        ev.preventDefault();

        jQuery.ajax({
            type: 'POST',
            async: false,
            url: '/koran/api/',
            data: {'koran_author_id':  koran_author_id, 'koran_ayah': koran_ayah, 'koran_surah_id' : koran_surah_id},
            success: function (data, textStatus, jqXHR) {
                $('.koran_inner_content').html(data);

                comment_scroller_init();
                player_init();

                direction_check();
            }
        });
    });

    $(document).on('click', '.koran-book__direction__item--next', function(ev) {
        var koran_author_id = $('.js_koran_author_selected').data('author-id');
        var current_page = $(this).data('page');

        ev.preventDefault();

        jQuery.ajax({
            type: 'POST',
            async: false,
            url: '/koran/api/',
            data: {'koran_author_id':  koran_author_id, 'right_page': current_page - 1, 'left_page': current_page - 2},
            success: function (data, textStatus, jqXHR) {
                $('.koran_inner_content').html(data);

                comment_scroller_init();
                player_init();

                direction_check();
            }
        });
    });


    $(document).on('click', '.koran-book__direction__item--prev', function(ev) {
        var koran_author_id = $('.js_koran_author_selected').data('author-id');
        var current_page = $(this).data('page');

        ev.preventDefault();

        jQuery.ajax({
            type: 'POST',
            async: false,
            url: '/koran/api/',
            data: {'koran_author_id':  koran_author_id, 'right_page': current_page + 1, 'left_page': current_page + 2},
            success: function (data, textStatus, jqXHR) {
                $('.koran_inner_content').html(data);

                comment_scroller_init();
                player_init();

                direction_check();
            }
        });
    });


    var direction_check = function() {
        if ($('.koran-book__direction__item--next').data('page') == 1) {
            $('.koran-book__direction__item--next').hide();
        }

        if ($('.koran-book__direction__item--prev').data('page') == 605) {
            $('.koran-book__direction__item--prev').hide();
        }
    };


    direction_check();

});