var Player = (function() {

  var path = '/static/javascripts/plugins/uppod/';

  function html5(id, file) {
    return new Uppod({
      m:"audio",
      uid:id,
      comment:"only time",
      file: file,
      st:"uppodaudio"
    });
  }

  function flash(id, file) {
    var flashvars = {
      "uid":id,
      "comment":"only time",
      "st": path + "audio179-540.txt",
      "file": file
    };
    var params = {
      bgcolor:"#34383a",
      allowScriptAccess:"always",
      id:id,
      wmode:"opaque" // z-index fix
    };
    new swfobject.embedSWF(path + "uppod.swf", id, "450", "35", "9.0.115.0", false, flashvars, params);
  }

  return {
    flash: flash,
    html5: html5
  };

})();

$(function(){

  var player = playerFactory(),
    playerIndex = 0;

  $('[data-audio]').each(function(){
    var $this = $(this),
      id = 'player-' + (playerIndex++),
      file = $this.data('file');

    $this.attr('id', id);
    if (player) {
      player(id, file);
    }


  });

  function playerFactory() {
    var player = false;
    if ( checkFlashSupport() ) {
      player = Player.flash;
    } else
    if ( checkAudioSupport() ) {
      player = Player.html5;
    } else  {
      // not supported
    }
    return player;
  }

  function checkAudioSupport() {
    var audio  = document.createElement("audio"),
      isAudioSupported = (typeof audio.canPlayType === "function" && audio.canPlayType("audio/mpeg") !== "");
    return isAudioSupported;
  }

  function checkFlashSupport() {
    var isFlashInstalled = false;

    if (typeof(navigator.plugins)!="undefined"&&typeof(navigator.plugins["Shockwave Flash"])=="object") {
      isFlashInstalled = true;
    } else if (typeof window.ActiveXObject != "undefined") {
      try {
        if (new ActiveXObject("ShockwaveFlash.ShockwaveFlash")) {
          isFlashInstalled = true;
        }
      } catch(e) {};
    }

    return isFlashInstalled;
  }

});