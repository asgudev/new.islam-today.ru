//	Uppod.AJAX для плеера Uppod (http://uppod.ru/player/ajax/)  
//  Версия 2.0 для Flash и HTML5

	var uppod_instances = new Array();
	var uppod_instances_id = new Array();
	var uppod_embedtag = new Array();
	var uppod_html5_instances = new Array();
	
	// Настройки
	var uppod_play_next=1; // 1 - включать следующий плеер по окончании


	function uppodTheEnd(playerID) {
		if(uppod_play_next==1){
			if(uppod_instances_id[playerID]<uppod_instances.length-1){
				uppodObject(uppod_instances[uppod_instances_id[playerID]+1]).sendToUppod('play');
			}
			else{
				uppodObject(uppod_instances[0]).sendToUppod('play');
			}
		}
	}
	
	function uppodStopAll(playerID) {
		for(var i = 0;i<uppod_instances.length;i++) {
			try {
				if(uppod_instances[i] != playerID){
					uppodObject(uppod_instances[i]).sendToUppod("stop");
				}
			}
			catch( errorObject ) {
			}
		}
	}

	function uppodPlayers(){
		var objectID;
		var objectTags = document.getElementsByTagName("object");
		for(var i=0;i<objectTags.length;i++) {
			objectID = objectTags[i].id;
			if(objectTags[i].getElementsByTagName("embed").length>0){
				uppod_embedtag[i] = true;
			}
			if(objectID.indexOf("player") >-1&uppod_instances.indexOf(objectID)==-1) {
				uppod_instances[i] = objectID;
				uppod_instances_id[objectID]=i;
			}
		}
		if(typeof uppod_players !== 'undefined'){
			if(uppod_players.length>0){
				for(var i=0;i<uppod_players.length;i++) {
					if(uppod_players[i].uid){
						if(!uppod_html5_instances[uppod_players[i].uid]) {
							uppod_html5_instances[uppod_players[i].uid]=uppod_players[i];
							document.getElementById(uppod_players[i].uid).addEventListener("play",uppodHTML5OnPlay,false);
							uppod_play_next==1?document.getElementById(uppod_players[i].uid).addEventListener("end",uppodHTML5OnEnd,false):'';
						}
					}
				}
			}
		}
	}

	if(!Array.indexOf){ 
		Array.prototype.indexOf = function(obj){
		for(var i=0; i<this.length; i++){
			if(this[i]==obj){
				return i;
				}
			}
			return -1;
			}
	}
	function uppodInit(playerID) {}
	function uppodSend(playerID) {}
	var ap_uppodID = setInterval(uppodPlayers, 3000);
	setTimeout(uppodPlayers,1000);
	setTimeout(uppodPlayers,2000);
	function uppodObject(objectID) {
		if(uppod_embedtag[uppod_instances_id[objectID]]){
	    	var M$ =  navigator.appName.indexOf("Microsoft")!=-1
	   		return (M$ ? window : document)[objectID];
	    }else{
	    	return document.getElementById(objectID);
	    }
	}
	function uppodHTML5OnPlay(event) {
		if(event.target.id){
			for(var i=0;i<uppod_players.length;i++) {
				if(uppod_players[i].uid){
					if(uppod_players[i].uid!=event.target.id && uppod_players[i].getStatus()==1) {
						uppod_players[i].Pause();
					}
				}
			}
		}
	}
	function uppodHTML5OnEnd(event) {
		if(event.target.id){
			for(var i=0;i<uppod_players.length;i++) {
				if(uppod_players[i].uid){
					if(uppod_players[i].uid==event.target.id && i<uppod_players.length-1) {
						uppod_players[i+1].Play();
						break;
					}
				}
			}
		}
	}