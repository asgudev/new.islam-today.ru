;window.Modernizr=function(a,b,c){function D(a){j.cssText=a}function E(a,b){return D(n.join(a+";")+(b||""))}function F(a,b){return typeof a===b}function G(a,b){return!!~(""+a).indexOf(b)}function H(a,b){for(var d in a){var e=a[d];if(!G(e,"-")&&j[e]!==c)return b=="pfx"?e:!0}return!1}function I(a,b,d){for(var e in a){var f=b[a[e]];if(f!==c)return d===!1?a[e]:F(f,"function")?f.bind(d||b):f}return!1}function J(a,b,c){var d=a.charAt(0).toUpperCase()+a.slice(1),e=(a+" "+p.join(d+" ")+d).split(" ");return F(b,"string")||F(b,"undefined")?H(e,b):(e=(a+" "+q.join(d+" ")+d).split(" "),I(e,b,c))}function K(){e.input=function(c){for(var d=0,e=c.length;d<e;d++)u[c[d]]=c[d]in k;return u.list&&(u.list=!!b.createElement("datalist")&&!!a.HTMLDataListElement),u}("autocomplete autofocus list placeholder max min multiple pattern required step".split(" ")),e.inputtypes=function(a){for(var d=0,e,f,h,i=a.length;d<i;d++)k.setAttribute("type",f=a[d]),e=k.type!=="text",e&&(k.value=l,k.style.cssText="position:absolute;visibility:hidden;",/^range$/.test(f)&&k.style.WebkitAppearance!==c?(g.appendChild(k),h=b.defaultView,e=h.getComputedStyle&&h.getComputedStyle(k,null).WebkitAppearance!=="textfield"&&k.offsetHeight!==0,g.removeChild(k)):/^(search|tel)$/.test(f)||(/^(url|email)$/.test(f)?e=k.checkValidity&&k.checkValidity()===!1:e=k.value!=l)),t[a[d]]=!!e;return t}("search tel url email datetime date month week time datetime-local number range color".split(" "))}var d="2.6.2",e={},f=!0,g=b.documentElement,h="modernizr",i=b.createElement(h),j=i.style,k=b.createElement("input"),l=":)",m={}.toString,n=" -webkit- -moz- -o- -ms- ".split(" "),o="Webkit Moz O ms",p=o.split(" "),q=o.toLowerCase().split(" "),r={svg:"http://www.w3.org/2000/svg"},s={},t={},u={},v=[],w=v.slice,x,y=function(a,c,d,e){var f,i,j,k,l=b.createElement("div"),m=b.body,n=m||b.createElement("body");if(parseInt(d,10))while(d--)j=b.createElement("div"),j.id=e?e[d]:h+(d+1),l.appendChild(j);return f=["&#173;",'<style id="s',h,'">',a,"</style>"].join(""),l.id=h,(m?l:n).innerHTML+=f,n.appendChild(l),m||(n.style.background="",n.style.overflow="hidden",k=g.style.overflow,g.style.overflow="hidden",g.appendChild(n)),i=c(l,a),m?l.parentNode.removeChild(l):(n.parentNode.removeChild(n),g.style.overflow=k),!!i},z=function(b){var c=a.matchMedia||a.msMatchMedia;if(c)return c(b).matches;var d;return y("@media "+b+" { #"+h+" { position: absolute; } }",function(b){d=(a.getComputedStyle?getComputedStyle(b,null):b.currentStyle)["position"]=="absolute"}),d},A=function(){function d(d,e){e=e||b.createElement(a[d]||"div"),d="on"+d;var f=d in e;return f||(e.setAttribute||(e=b.createElement("div")),e.setAttribute&&e.removeAttribute&&(e.setAttribute(d,""),f=F(e[d],"function"),F(e[d],"undefined")||(e[d]=c),e.removeAttribute(d))),e=null,f}var a={select:"input",change:"input",submit:"form",reset:"form",error:"img",load:"img",abort:"img"};return d}(),B={}.hasOwnProperty,C;!F(B,"undefined")&&!F(B.call,"undefined")?C=function(a,b){return B.call(a,b)}:C=function(a,b){return b in a&&F(a.constructor.prototype[b],"undefined")},Function.prototype.bind||(Function.prototype.bind=function(b){var c=this;if(typeof c!="function")throw new TypeError;var d=w.call(arguments,1),e=function(){if(this instanceof e){var a=function(){};a.prototype=c.prototype;var f=new a,g=c.apply(f,d.concat(w.call(arguments)));return Object(g)===g?g:f}return c.apply(b,d.concat(w.call(arguments)))};return e}),s.flexbox=function(){return J("flexWrap")},s.canvas=function(){var a=b.createElement("canvas");return!!a.getContext&&!!a.getContext("2d")},s.canvastext=function(){return!!e.canvas&&!!F(b.createElement("canvas").getContext("2d").fillText,"function")},s.webgl=function(){return!!a.WebGLRenderingContext},s.touch=function(){var c;return"ontouchstart"in a||a.DocumentTouch&&b instanceof DocumentTouch?c=!0:y(["@media (",n.join("touch-enabled),("),h,")","{#modernizr{top:9px;position:absolute}}"].join(""),function(a){c=a.offsetTop===9}),c},s.geolocation=function(){return"geolocation"in navigator},s.postmessage=function(){return!!a.postMessage},s.websqldatabase=function(){return!!a.openDatabase},s.indexedDB=function(){return!!J("indexedDB",a)},s.hashchange=function(){return A("hashchange",a)&&(b.documentMode===c||b.documentMode>7)},s.history=function(){return!!a.history&&!!history.pushState},s.draganddrop=function(){var a=b.createElement("div");return"draggable"in a||"ondragstart"in a&&"ondrop"in a},s.websockets=function(){return"WebSocket"in a||"MozWebSocket"in a},s.rgba=function(){return D("background-color:rgba(150,255,150,.5)"),G(j.backgroundColor,"rgba")},s.hsla=function(){return D("background-color:hsla(120,40%,100%,.5)"),G(j.backgroundColor,"rgba")||G(j.backgroundColor,"hsla")},s.multiplebgs=function(){return D("background:url(https://),url(https://),red url(https://)"),/(url\s*\(.*?){3}/.test(j.background)},s.backgroundsize=function(){return J("backgroundSize")},s.borderimage=function(){return J("borderImage")},s.borderradius=function(){return J("borderRadius")},s.boxshadow=function(){return J("boxShadow")},s.textshadow=function(){return b.createElement("div").style.textShadow===""},s.opacity=function(){return E("opacity:.55"),/^0.55$/.test(j.opacity)},s.cssanimations=function(){return J("animationName")},s.csscolumns=function(){return J("columnCount")},s.cssgradients=function(){var a="background-image:",b="gradient(linear,left top,right bottom,from(#9f9),to(white));",c="linear-gradient(left top,#9f9, white);";return D((a+"-webkit- ".split(" ").join(b+a)+n.join(c+a)).slice(0,-a.length)),G(j.backgroundImage,"gradient")},s.cssreflections=function(){return J("boxReflect")},s.csstransforms=function(){return!!J("transform")},s.csstransforms3d=function(){var a=!!J("perspective");return a&&"webkitPerspective"in g.style&&y("@media (transform-3d),(-webkit-transform-3d){#modernizr{left:9px;position:absolute;height:3px;}}",function(b,c){a=b.offsetLeft===9&&b.offsetHeight===3}),a},s.csstransitions=function(){return J("transition")},s.fontface=function(){var a;return y('@font-face {font-family:"font";src:url("https://")}',function(c,d){var e=b.getElementById("smodernizr"),f=e.sheet||e.styleSheet,g=f?f.cssRules&&f.cssRules[0]?f.cssRules[0].cssText:f.cssText||"":"";a=/src/i.test(g)&&g.indexOf(d.split(" ")[0])===0}),a},s.generatedcontent=function(){var a;return y(["#",h,"{font:0/0 a}#",h,':after{content:"',l,'";visibility:hidden;font:3px/1 a}'].join(""),function(b){a=b.offsetHeight>=3}),a},s.video=function(){var a=b.createElement("video"),c=!1;try{if(c=!!a.canPlayType)c=new Boolean(c),c.ogg=a.canPlayType('video/ogg; codecs="theora"').replace(/^no$/,""),c.h264=a.canPlayType('video/mp4; codecs="avc1.42E01E"').replace(/^no$/,""),c.webm=a.canPlayType('video/webm; codecs="vp8, vorbis"').replace(/^no$/,"")}catch(d){}return c},s.audio=function(){var a=b.createElement("audio"),c=!1;try{if(c=!!a.canPlayType)c=new Boolean(c),c.ogg=a.canPlayType('audio/ogg; codecs="vorbis"').replace(/^no$/,""),c.mp3=a.canPlayType("audio/mpeg;").replace(/^no$/,""),c.wav=a.canPlayType('audio/wav; codecs="1"').replace(/^no$/,""),c.m4a=(a.canPlayType("audio/x-m4a;")||a.canPlayType("audio/aac;")).replace(/^no$/,"")}catch(d){}return c},s.localstorage=function(){try{return localStorage.setItem(h,h),localStorage.removeItem(h),!0}catch(a){return!1}},s.sessionstorage=function(){try{return sessionStorage.setItem(h,h),sessionStorage.removeItem(h),!0}catch(a){return!1}},s.webworkers=function(){return!!a.Worker},s.applicationcache=function(){return!!a.applicationCache},s.svg=function(){return!!b.createElementNS&&!!b.createElementNS(r.svg,"svg").createSVGRect},s.inlinesvg=function(){var a=b.createElement("div");return a.innerHTML="<svg/>",(a.firstChild&&a.firstChild.namespaceURI)==r.svg},s.smil=function(){return!!b.createElementNS&&/SVGAnimate/.test(m.call(b.createElementNS(r.svg,"animate")))},s.svgclippaths=function(){return!!b.createElementNS&&/SVGClipPath/.test(m.call(b.createElementNS(r.svg,"clipPath")))};for(var L in s)C(s,L)&&(x=L.toLowerCase(),e[x]=s[L](),v.push((e[x]?"":"no-")+x));return e.input||K(),e.addTest=function(a,b){if(typeof a=="object")for(var d in a)C(a,d)&&e.addTest(d,a[d]);else{a=a.toLowerCase();if(e[a]!==c)return e;b=typeof b=="function"?b():b,typeof f!="undefined"&&f&&(g.className+=" "+(b?"":"no-")+a),e[a]=b}return e},D(""),i=k=null,function(a,b){function k(a,b){var c=a.createElement("p"),d=a.getElementsByTagName("head")[0]||a.documentElement;return c.innerHTML="x<style>"+b+"</style>",d.insertBefore(c.lastChild,d.firstChild)}function l(){var a=r.elements;return typeof a=="string"?a.split(" "):a}function m(a){var b=i[a[g]];return b||(b={},h++,a[g]=h,i[h]=b),b}function n(a,c,f){c||(c=b);if(j)return c.createElement(a);f||(f=m(c));var g;return f.cache[a]?g=f.cache[a].cloneNode():e.test(a)?g=(f.cache[a]=f.createElem(a)).cloneNode():g=f.createElem(a),g.canHaveChildren&&!d.test(a)?f.frag.appendChild(g):g}function o(a,c){a||(a=b);if(j)return a.createDocumentFragment();c=c||m(a);var d=c.frag.cloneNode(),e=0,f=l(),g=f.length;for(;e<g;e++)d.createElement(f[e]);return d}function p(a,b){b.cache||(b.cache={},b.createElem=a.createElement,b.createFrag=a.createDocumentFragment,b.frag=b.createFrag()),a.createElement=function(c){return r.shivMethods?n(c,a,b):b.createElem(c)},a.createDocumentFragment=Function("h,f","return function(){var n=f.cloneNode(),c=n.createElement;h.shivMethods&&("+l().join().replace(/\w+/g,function(a){return b.createElem(a),b.frag.createElement(a),'c("'+a+'")'})+");return n}")(r,b.frag)}function q(a){a||(a=b);var c=m(a);return r.shivCSS&&!f&&!c.hasCSS&&(c.hasCSS=!!k(a,"article,aside,figcaption,figure,footer,header,hgroup,nav,section{display:block}mark{background:#FF0;color:#000}")),j||p(a,c),a}var c=a.html5||{},d=/^<|^(?:button|map|select|textarea|object|iframe|option|optgroup)$/i,e=/^(?:a|b|code|div|fieldset|h1|h2|h3|h4|h5|h6|i|label|li|ol|p|q|span|strong|style|table|tbody|td|th|tr|ul)$/i,f,g="_html5shiv",h=0,i={},j;(function(){try{var a=b.createElement("a");a.innerHTML="<xyz></xyz>",f="hidden"in a,j=a.childNodes.length==1||function(){b.createElement("a");var a=b.createDocumentFragment();return typeof a.cloneNode=="undefined"||typeof a.createDocumentFragment=="undefined"||typeof a.createElement=="undefined"}()}catch(c){f=!0,j=!0}})();var r={elements:c.elements||"abbr article aside audio bdi canvas data datalist details figcaption figure footer header hgroup mark meter nav output progress section summary time video",shivCSS:c.shivCSS!==!1,supportsUnknownElements:j,shivMethods:c.shivMethods!==!1,type:"default",shivDocument:q,createElement:n,createDocumentFragment:o};a.html5=r,q(b)}(this,b),e._version=d,e._prefixes=n,e._domPrefixes=q,e._cssomPrefixes=p,e.mq=z,e.hasEvent=A,e.testProp=function(a){return H([a])},e.testAllProps=J,e.testStyles=y,e.prefixed=function(a,b,c){return b?J(a,b,c):J(a,"pfx")},g.className=g.className.replace(/(^|\s)no-js(\s|$)/,"$1$2")+(f?" js "+v.join(" "):""),e}(this,this.document),function(a,b,c){function d(a){return"[object Function]"==o.call(a)}function e(a){return"string"==typeof a}function f(){}function g(a){return!a||"loaded"==a||"complete"==a||"uninitialized"==a}function h(){var a=p.shift();q=1,a?a.t?m(function(){("c"==a.t?B.injectCss:B.injectJs)(a.s,0,a.a,a.x,a.e,1)},0):(a(),h()):q=0}function i(a,c,d,e,f,i,j){function k(b){if(!o&&g(l.readyState)&&(u.r=o=1,!q&&h(),l.onload=l.onreadystatechange=null,b)){"img"!=a&&m(function(){t.removeChild(l)},50);for(var d in y[c])y[c].hasOwnProperty(d)&&y[c][d].onload()}}var j=j||B.errorTimeout,l=b.createElement(a),o=0,r=0,u={t:d,s:c,e:f,a:i,x:j};1===y[c]&&(r=1,y[c]=[]),"object"==a?l.data=c:(l.src=c,l.type=a),l.width=l.height="0",l.onerror=l.onload=l.onreadystatechange=function(){k.call(this,r)},p.splice(e,0,u),"img"!=a&&(r||2===y[c]?(t.insertBefore(l,s?null:n),m(k,j)):y[c].push(l))}function j(a,b,c,d,f){return q=0,b=b||"j",e(a)?i("c"==b?v:u,a,b,this.i++,c,d,f):(p.splice(this.i++,0,a),1==p.length&&h()),this}function k(){var a=B;return a.loader={load:j,i:0},a}var l=b.documentElement,m=a.setTimeout,n=b.getElementsByTagName("script")[0],o={}.toString,p=[],q=0,r="MozAppearance"in l.style,s=r&&!!b.createRange().compareNode,t=s?l:n.parentNode,l=a.opera&&"[object Opera]"==o.call(a.opera),l=!!b.attachEvent&&!l,u=r?"object":l?"script":"img",v=l?"script":u,w=Array.isArray||function(a){return"[object Array]"==o.call(a)},x=[],y={},z={timeout:function(a,b){return b.length&&(a.timeout=b[0]),a}},A,B;B=function(a){function b(a){var a=a.split("!"),b=x.length,c=a.pop(),d=a.length,c={url:c,origUrl:c,prefixes:a},e,f,g;for(f=0;f<d;f++)g=a[f].split("="),(e=z[g.shift()])&&(c=e(c,g));for(f=0;f<b;f++)c=x[f](c);return c}function g(a,e,f,g,h){var i=b(a),j=i.autoCallback;i.url.split(".").pop().split("?").shift(),i.bypass||(e&&(e=d(e)?e:e[a]||e[g]||e[a.split("/").pop().split("?")[0]]),i.instead?i.instead(a,e,f,g,h):(y[i.url]?i.noexec=!0:y[i.url]=1,f.load(i.url,i.forceCSS||!i.forceJS&&"stylesheets"==i.url.split(".").pop().split("?").shift()?"c":c,i.noexec,i.attrs,i.timeout),(d(e)||d(j))&&f.load(function(){k(),e&&e(i.origUrl,h,g),j&&j(i.origUrl,h,g),y[i.url]=2})))}function h(a,b){function c(a,c){if(a){if(e(a))c||(j=function(){var a=[].slice.call(arguments);k.apply(this,a),l()}),g(a,j,b,0,h);else if(Object(a)===a)for(n in m=function(){var b=0,c;for(c in a)a.hasOwnProperty(c)&&b++;return b}(),a)a.hasOwnProperty(n)&&(!c&&!--m&&(d(j)?j=function(){var a=[].slice.call(arguments);k.apply(this,a),l()}:j[n]=function(a){return function(){var b=[].slice.call(arguments);a&&a.apply(this,b),l()}}(k[n])),g(a[n],j,b,n,h))}else!c&&l()}var h=!!a.test,i=a.load||a.both,j=a.callback||f,k=j,l=a.complete||f,m,n;c(h?a.yep:a.nope,!!i),i&&c(i)}var i,j,l=this.yepnope.loader;if(e(a))g(a,0,l,0);else if(w(a))for(i=0;i<a.length;i++)j=a[i],e(j)?g(j,0,l,0):w(j)?B(j):Object(j)===j&&h(j,l);else Object(a)===a&&h(a,l)},B.addPrefix=function(a,b){z[a]=b},B.addFilter=function(a){x.push(a)},B.errorTimeout=1e4,null==b.readyState&&b.addEventListener&&(b.readyState="loading",b.addEventListener("DOMContentLoaded",A=function(){b.removeEventListener("DOMContentLoaded",A,0),b.readyState="complete"},0)),a.yepnope=k(),a.yepnope.executeStack=h,a.yepnope.injectJs=function(a,c,d,e,i,j){var k=b.createElement("script"),l,o,e=e||B.errorTimeout;k.src=a;for(o in d)k.setAttribute(o,d[o]);c=j?h:c||f,k.onreadystatechange=k.onload=function(){!l&&g(k.readyState)&&(l=1,c(),k.onload=k.onreadystatechange=null)},m(function(){l||(l=1,c(1))},e),i?k.onload():n.parentNode.insertBefore(k,n)},a.yepnope.injectCss=function(a,c,d,e,g,i){var e=b.createElement("link"),j,c=i?h:c||f;e.href=a,e.rel="stylesheet",e.type="text/stylesheets";for(j in d)e.setAttribute(j,d[j]);g||(n.parentNode.insertBefore(e,n),m(c,0))}}(this,document),Modernizr.load=function(){yepnope.apply(window,[].slice.call(arguments,0))};

/*!
 * jCarousel - Riding carousels with jQuery
 *   http://sorgalla.com/jcarousel/
 *
 * Copyright (c) 2006 Jan Sorgalla (http://sorgalla.com)
 * Dual licensed under the MIT (http://www.opensource.org/licenses/mit-license.php)
 * and GPL (http://www.opensource.org/licenses/gpl-license.php) licenses.
 *
 * Built on top of the jQuery library
 *   http://jquery.com
 *
 * Inspired by the "Carousel Component" by Bill Scott
 *   http://billwscott.com/carousel/
 */

(function(a){var b={vertical:!1,rtl:!1,start:1,offset:1,size:null,scroll:3,visible:null,animation:"normal",easing:"swing",auto:0,wrap:null,initCallback:null,setupCallback:null,reloadCallback:null,itemLoadCallback:null,itemFirstInCallback:null,itemFirstOutCallback:null,itemLastInCallback:null,itemLastOutCallback:null,itemVisibleInCallback:null,itemVisibleOutCallback:null,animationStepCallback:null,buttonNextHTML:"<div></div>",buttonPrevHTML:"<div></div>",buttonNextEvent:"click",buttonPrevEvent:"click",buttonNextCallback:null,buttonPrevCallback:null,itemFallbackDimension:null},c=!1;a(window).bind("load.jcarousel",function(){c=!0}),a.jcarousel=function(e,f){this.options=a.extend({},b,f||{}),this.locked=!1,this.autoStopped=!1,this.container=null,this.clip=null,this.list=null,this.buttonNext=null,this.buttonPrev=null,this.buttonNextState=null,this.buttonPrevState=null,f&&void 0!==f.rtl||(this.options.rtl="rtl"==(a(e).attr("dir")||a("html").attr("dir")||"").toLowerCase()),this.wh=this.options.vertical?"height":"width",this.lt=this.options.vertical?"top":this.options.rtl?"right":"left";for(var g="",h=e.className.split(" "),i=0;h.length>i;i++)if(-1!=h[i].indexOf("jcarousel-skin")){a(e).removeClass(h[i]),g=h[i];break}"UL"==e.nodeName.toUpperCase()||"OL"==e.nodeName.toUpperCase()?(this.list=a(e),this.clip=this.list.parents(".jcarousel-clip"),this.container=this.list.parents(".jcarousel-container")):(this.container=a(e),this.list=this.container.find("ul,ol").eq(0),this.clip=this.container.find(".jcarousel-clip")),0===this.clip.size()&&(this.clip=this.list.wrap("<div></div>").parent()),0===this.container.size()&&(this.container=this.clip.wrap("<div></div>").parent()),""!==g&&-1==this.container.parent()[0].className.indexOf("jcarousel-skin")&&this.container.wrap('<div class=" '+g+'"></div>'),this.buttonPrev=a(".jcarousel-prev",this.container),0===this.buttonPrev.size()&&null!==this.options.buttonPrevHTML&&(this.buttonPrev=a(this.options.buttonPrevHTML).appendTo(this.container)),this.buttonPrev.addClass(this.className("jcarousel-prev")),this.buttonNext=a(".jcarousel-next",this.container),0===this.buttonNext.size()&&null!==this.options.buttonNextHTML&&(this.buttonNext=a(this.options.buttonNextHTML).appendTo(this.container)),this.buttonNext.addClass(this.className("jcarousel-next")),this.clip.addClass(this.className("jcarousel-clip")).css({position:"relative"}),this.list.addClass(this.className("jcarousel-list")).css({overflow:"hidden",position:"relative",top:0,margin:0,padding:0}).css(this.options.rtl?"right":"left",0),this.container.addClass(this.className("jcarousel-container")).css({position:"relative"}),!this.options.vertical&&this.options.rtl&&this.container.addClass("jcarousel-direction-rtl").attr("dir","rtl");var j=null!==this.options.visible?Math.ceil(this.clipping()/this.options.visible):null,k=this.list.children("li"),l=this;if(k.size()>0){var m=0,n=this.options.offset;k.each(function(){l.format(this,n++),m+=l.dimension(this,j)}),this.list.css(this.wh,m+100+"px"),f&&void 0!==f.size||(this.options.size=k.size())}this.container.css("display","block"),this.buttonNext.css("display","block"),this.buttonPrev.css("display","block"),this.funcNext=function(){return l.next(),!1},this.funcPrev=function(){return l.prev(),!1},this.funcResize=function(){l.resizeTimer&&clearTimeout(l.resizeTimer),l.resizeTimer=setTimeout(function(){l.reload()},100)},null!==this.options.initCallback&&this.options.initCallback(this,"init"),!c&&d.isSafari()?(this.buttons(!1,!1),a(window).bind("load.jcarousel",function(){l.setup()})):this.setup()};var d=a.jcarousel;d.fn=d.prototype={jcarousel:"0.2.9"},d.fn.extend=d.extend=a.extend,d.fn.extend({setup:function(){if(this.first=null,this.last=null,this.prevFirst=null,this.prevLast=null,this.animating=!1,this.timer=null,this.resizeTimer=null,this.tail=null,this.inTail=!1,!this.locked){this.list.css(this.lt,this.pos(this.options.offset)+"px");var b=this.pos(this.options.start,!0);this.prevFirst=this.prevLast=null,this.animate(b,!1),a(window).unbind("resize.jcarousel",this.funcResize).bind("resize.jcarousel",this.funcResize),null!==this.options.setupCallback&&this.options.setupCallback(this)}},reset:function(){this.list.empty(),this.list.css(this.lt,"0px"),this.list.css(this.wh,"10px"),null!==this.options.initCallback&&this.options.initCallback(this,"reset"),this.setup()},reload:function(){if(null!==this.tail&&this.inTail&&this.list.css(this.lt,d.intval(this.list.css(this.lt))+this.tail),this.tail=null,this.inTail=!1,null!==this.options.reloadCallback&&this.options.reloadCallback(this),null!==this.options.visible){var a=this,b=Math.ceil(this.clipping()/this.options.visible),c=0,e=0;this.list.children("li").each(function(d){c+=a.dimension(this,b),a.first>d+1&&(e=c)}),this.list.css(this.wh,c+"px"),this.list.css(this.lt,-e+"px")}this.scroll(this.first,!1)},lock:function(){this.locked=!0,this.buttons()},unlock:function(){this.locked=!1,this.buttons()},size:function(a){return void 0!==a&&(this.options.size=a,this.locked||this.buttons()),this.options.size},has:function(a,b){void 0!==b&&b||(b=a),null!==this.options.size&&b>this.options.size&&(b=this.options.size);for(var c=a;b>=c;c++){var d=this.get(c);if(!d.length||d.hasClass("jcarousel-item-placeholder"))return!1}return!0},get:function(b){return a(">.jcarousel-item-"+b,this.list)},add:function(b,c){var e=this.get(b),f=0,g=a(c);if(0===e.length){var h,i=d.intval(b);for(e=this.create(b);;)if(h=this.get(--i),0>=i||h.length){0>=i?this.list.prepend(e):h.after(e);break}}else f=this.dimension(e);"LI"==g.get(0).nodeName.toUpperCase()?(e.replaceWith(g),e=g):e.empty().append(c),this.format(e.removeClass(this.className("jcarousel-item-placeholder")),b);var j=null!==this.options.visible?Math.ceil(this.clipping()/this.options.visible):null,k=this.dimension(e,j)-f;return b>0&&this.first>b&&this.list.css(this.lt,d.intval(this.list.css(this.lt))-k+"px"),this.list.css(this.wh,d.intval(this.list.css(this.wh))+k+"px"),e},remove:function(a){var b=this.get(a);if(b.length&&!(a>=this.first&&this.last>=a)){var c=this.dimension(b);this.first>a&&this.list.css(this.lt,d.intval(this.list.css(this.lt))+c+"px"),b.remove(),this.list.css(this.wh,d.intval(this.list.css(this.wh))-c+"px")}},next:function(){null===this.tail||this.inTail?this.scroll("both"!=this.options.wrap&&"last"!=this.options.wrap||null===this.options.size||this.last!=this.options.size?this.first+this.options.scroll:1):this.scrollTail(!1)},prev:function(){null!==this.tail&&this.inTail?this.scrollTail(!0):this.scroll("both"!=this.options.wrap&&"first"!=this.options.wrap||null===this.options.size||1!=this.first?this.first-this.options.scroll:this.options.size)},scrollTail:function(a){if(!this.locked&&!this.animating&&this.tail){this.pauseAuto();var b=d.intval(this.list.css(this.lt));b=a?b+this.tail:b-this.tail,this.inTail=!a,this.prevFirst=this.first,this.prevLast=this.last,this.animate(b)}},scroll:function(a,b){this.locked||this.animating||(this.pauseAuto(),this.animate(this.pos(a),b))},pos:function(a,b){var c=d.intval(this.list.css(this.lt));if(this.locked||this.animating)return c;"circular"!=this.options.wrap&&(a=1>a?1:this.options.size&&a>this.options.size?this.options.size:a);for(var m,e=this.first>a,f="circular"!=this.options.wrap&&1>=this.first?1:this.first,g=e?this.get(f):this.get(this.last),h=e?f:f-1,i=null,j=0,k=!1,l=0;e?--h>=a:a>++h;)i=this.get(h),k=!i.length,0===i.length&&(i=this.create(h).addClass(this.className("jcarousel-item-placeholder")),g[e?"before":"after"](i),null!==this.first&&"circular"==this.options.wrap&&null!==this.options.size&&(0>=h||h>this.options.size)&&(m=this.get(this.index(h)),m.length&&(i=this.add(h,m.clone(!0))))),g=i,l=this.dimension(i),k&&(j+=l),null!==this.first&&("circular"==this.options.wrap||h>=1&&(null===this.options.size||this.options.size>=h))&&(c=e?c+l:c-l);var n=this.clipping(),o=[],p=0,q=0;for(g=this.get(a-1),h=a;++p;){if(i=this.get(h),k=!i.length,0===i.length&&(i=this.create(h).addClass(this.className("jcarousel-item-placeholder")),0===g.length?this.list.prepend(i):g[e?"before":"after"](i),null!==this.first&&"circular"==this.options.wrap&&null!==this.options.size&&(0>=h||h>this.options.size)&&(m=this.get(this.index(h)),m.length&&(i=this.add(h,m.clone(!0))))),g=i,l=this.dimension(i),0===l)throw Error("jCarousel: No width/height set for items. This will cause an infinite loop. Aborting...");if("circular"!=this.options.wrap&&null!==this.options.size&&h>this.options.size?o.push(i):k&&(j+=l),q+=l,q>=n)break;h++}for(var r=0;o.length>r;r++)o[r].remove();j>0&&(this.list.css(this.wh,this.dimension(this.list)+j+"px"),e&&(c-=j,this.list.css(this.lt,d.intval(this.list.css(this.lt))-j+"px")));var s=a+p-1;if("circular"!=this.options.wrap&&this.options.size&&s>this.options.size&&(s=this.options.size),h>s)for(p=0,h=s,q=0;++p&&(i=this.get(h--),i.length)&&(q+=this.dimension(i),!(q>=n)););var t=s-p+1;if("circular"!=this.options.wrap&&1>t&&(t=1),this.inTail&&e&&(c+=this.tail,this.inTail=!1),this.tail=null,"circular"!=this.options.wrap&&s==this.options.size&&s-p+1>=1){var u=d.intval(this.get(s).css(this.options.vertical?"marginBottom":"marginRight"));q-u>n&&(this.tail=q-n-u)}for(b&&a===this.options.size&&this.tail&&(c-=this.tail,this.inTail=!0);a-->t;)c+=this.dimension(this.get(a));return this.prevFirst=this.first,this.prevLast=this.last,this.first=t,this.last=s,c},animate:function(b,c){if(!this.locked&&!this.animating){this.animating=!0;var d=this,e=function(){if(d.animating=!1,0===b&&d.list.css(d.lt,0),!d.autoStopped&&("circular"==d.options.wrap||"both"==d.options.wrap||"last"==d.options.wrap||null===d.options.size||d.last<d.options.size||d.last==d.options.size&&null!==d.tail&&!d.inTail)&&d.startAuto(),d.buttons(),d.notify("onAfterAnimation"),"circular"==d.options.wrap&&null!==d.options.size)for(var a=d.prevFirst;d.prevLast>=a;a++)null===a||a>=d.first&&d.last>=a||!(1>a||a>d.options.size)||d.remove(a)};if(this.notify("onBeforeAnimation"),this.options.animation&&c!==!1){var f=this.options.vertical?{top:b}:this.options.rtl?{right:b}:{left:b},g={duration:this.options.animation,easing:this.options.easing,complete:e};a.isFunction(this.options.animationStepCallback)&&(g.step=this.options.animationStepCallback),this.list.animate(f,g)}else this.list.css(this.lt,b+"px"),e()}},startAuto:function(a){if(void 0!==a&&(this.options.auto=a),0===this.options.auto)return this.stopAuto();if(null===this.timer){this.autoStopped=!1;var b=this;this.timer=window.setTimeout(function(){b.next()},1e3*this.options.auto)}},stopAuto:function(){this.pauseAuto(),this.autoStopped=!0},pauseAuto:function(){null!==this.timer&&(window.clearTimeout(this.timer),this.timer=null)},buttons:function(a,b){null==a&&(a=!this.locked&&0!==this.options.size&&(this.options.wrap&&"first"!=this.options.wrap||null===this.options.size||this.last<this.options.size),this.locked||this.options.wrap&&"first"!=this.options.wrap||null===this.options.size||!(this.last>=this.options.size)||(a=null!==this.tail&&!this.inTail)),null==b&&(b=!this.locked&&0!==this.options.size&&(this.options.wrap&&"last"!=this.options.wrap||this.first>1),this.locked||this.options.wrap&&"last"!=this.options.wrap||null===this.options.size||1!=this.first||(b=null!==this.tail&&this.inTail));var c=this;this.buttonNext.size()>0?(this.buttonNext.unbind(this.options.buttonNextEvent+".jcarousel",this.funcNext),a&&this.buttonNext.bind(this.options.buttonNextEvent+".jcarousel",this.funcNext),this.buttonNext[a?"removeClass":"addClass"](this.className("jcarousel-next-disabled")).attr("disabled",a?!1:!0),null!==this.options.buttonNextCallback&&this.buttonNext.data("jcarouselstate")!=a&&this.buttonNext.each(function(){c.options.buttonNextCallback(c,this,a)}).data("jcarouselstate",a)):null!==this.options.buttonNextCallback&&this.buttonNextState!=a&&this.options.buttonNextCallback(c,null,a),this.buttonPrev.size()>0?(this.buttonPrev.unbind(this.options.buttonPrevEvent+".jcarousel",this.funcPrev),b&&this.buttonPrev.bind(this.options.buttonPrevEvent+".jcarousel",this.funcPrev),this.buttonPrev[b?"removeClass":"addClass"](this.className("jcarousel-prev-disabled")).attr("disabled",b?!1:!0),null!==this.options.buttonPrevCallback&&this.buttonPrev.data("jcarouselstate")!=b&&this.buttonPrev.each(function(){c.options.buttonPrevCallback(c,this,b)}).data("jcarouselstate",b)):null!==this.options.buttonPrevCallback&&this.buttonPrevState!=b&&this.options.buttonPrevCallback(c,null,b),this.buttonNextState=a,this.buttonPrevState=b},notify:function(a){var b=null===this.prevFirst?"init":this.prevFirst<this.first?"next":"prev";this.callback("itemLoadCallback",a,b),this.prevFirst!==this.first&&(this.callback("itemFirstInCallback",a,b,this.first),this.callback("itemFirstOutCallback",a,b,this.prevFirst)),this.prevLast!==this.last&&(this.callback("itemLastInCallback",a,b,this.last),this.callback("itemLastOutCallback",a,b,this.prevLast)),this.callback("itemVisibleInCallback",a,b,this.first,this.last,this.prevFirst,this.prevLast),this.callback("itemVisibleOutCallback",a,b,this.prevFirst,this.prevLast,this.first,this.last)},callback:function(b,c,d,e,f,g,h){if(null!=this.options[b]&&("object"==typeof this.options[b]||"onAfterAnimation"==c)){var i="object"==typeof this.options[b]?this.options[b][c]:this.options[b];if(a.isFunction(i)){var j=this;if(void 0===e)i(j,d,c);else if(void 0===f)this.get(e).each(function(){i(j,this,e,d,c)});else for(var k=function(a){j.get(a).each(function(){i(j,this,a,d,c)})},l=e;f>=l;l++)null===l||l>=g&&h>=l||k(l)}}},create:function(a){return this.format("<li></li>",a)},format:function(b,c){b=a(b);for(var d=b.get(0).className.split(" "),e=0;d.length>e;e++)-1!=d[e].indexOf("jcarousel-")&&b.removeClass(d[e]);return b.addClass(this.className("jcarousel-item")).addClass(this.className("jcarousel-item-"+c)).css({"float":this.options.rtl?"right":"left","list-style":"none"}).attr("jcarouselindex",c),b},className:function(a){return a+" "+a+(this.options.vertical?"-vertical":"-horizontal")},dimension:function(b,c){var e=a(b);if(null==c)return this.options.vertical?e.innerHeight()+d.intval(e.css("margin-top"))+d.intval(e.css("margin-bottom"))+d.intval(e.css("border-top-width"))+d.intval(e.css("border-bottom-width"))||d.intval(this.options.itemFallbackDimension):e.innerWidth()+d.intval(e.css("margin-left"))+d.intval(e.css("margin-right"))+d.intval(e.css("border-left-width"))+d.intval(e.css("border-right-width"))||d.intval(this.options.itemFallbackDimension);var f=this.options.vertical?c-d.intval(e.css("marginTop"))-d.intval(e.css("marginBottom")):c-d.intval(e.css("marginLeft"))-d.intval(e.css("marginRight"));return a(e).css(this.wh,f+"px"),this.dimension(e)},clipping:function(){return this.options.vertical?this.clip[0].offsetHeight-d.intval(this.clip.css("borderTopWidth"))-d.intval(this.clip.css("borderBottomWidth")):this.clip[0].offsetWidth-d.intval(this.clip.css("borderLeftWidth"))-d.intval(this.clip.css("borderRightWidth"))},index:function(a,b){return null==b&&(b=this.options.size),Math.round(((a-1)/b-Math.floor((a-1)/b))*b)+1}}),d.extend({defaults:function(c){return a.extend(b,c||{})},intval:function(a){return a=parseInt(a,10),isNaN(a)?0:a},windowLoaded:function(){c=!0},isSafari:function(){var a=navigator.userAgent.toLowerCase(),b=/(chrome)[ \/]([\w.]+)/.exec(a)||/(webkit)[ \/]([\w.]+)/.exec(a)||[],c=b[1]||"";return"webkit"===c}}),a.fn.jcarousel=function(b){if("string"==typeof b){var c=a(this).data("jcarousel"),e=Array.prototype.slice.call(arguments,1);return c[b].apply(c,e)}return this.each(function(){var c=a(this).data("jcarousel");c?(b&&a.extend(c.options,b),c.reload()):a(this).data("jcarousel",new d(this,b))})}})(jQuery);

/*!
 * jQuery Transit - CSS3 transitions and transformations
 * (c) 2011-2012 Rico Sta. Cruz <rico@ricostacruz.com>
 * MIT Licensed.
 *
 * http://ricostacruz.com/jquery.transit
 * http://github.com/rstacruz/jquery.transit
 */
(function(k){k.transit={version:"0.9.9",propertyMap:{marginLeft:"margin",marginRight:"margin",marginBottom:"margin",marginTop:"margin",paddingLeft:"padding",paddingRight:"padding",paddingBottom:"padding",paddingTop:"padding"},enabled:true,useTransitionEnd:false};var d=document.createElement("div");var q={};function b(v){if(v in d.style){return v}var u=["Moz","Webkit","O","ms"];var r=v.charAt(0).toUpperCase()+v.substr(1);if(v in d.style){return v}for(var t=0;t<u.length;++t){var s=u[t]+r;if(s in d.style){return s}}}function e(){d.style[q.transform]="";d.style[q.transform]="rotateY(90deg)";return d.style[q.transform]!==""}var a=navigator.userAgent.toLowerCase().indexOf("chrome")>-1;q.transition=b("transition");q.transitionDelay=b("transitionDelay");q.transform=b("transform");q.transformOrigin=b("transformOrigin");q.transform3d=e();var i={transition:"transitionEnd",MozTransition:"transitionend",OTransition:"oTransitionEnd",WebkitTransition:"webkitTransitionEnd",msTransition:"MSTransitionEnd"};var f=q.transitionEnd=i[q.transition]||null;for(var p in q){if(q.hasOwnProperty(p)&&typeof k.support[p]==="undefined"){k.support[p]=q[p]}}d=null;k.cssEase={_default:"ease","in":"ease-in",out:"ease-out","in-out":"ease-in-out",snap:"cubic-bezier(0,1,.5,1)",easeOutCubic:"cubic-bezier(.215,.61,.355,1)",easeInOutCubic:"cubic-bezier(.645,.045,.355,1)",easeInCirc:"cubic-bezier(.6,.04,.98,.335)",easeOutCirc:"cubic-bezier(.075,.82,.165,1)",easeInOutCirc:"cubic-bezier(.785,.135,.15,.86)",easeInExpo:"cubic-bezier(.95,.05,.795,.035)",easeOutExpo:"cubic-bezier(.19,1,.22,1)",easeInOutExpo:"cubic-bezier(1,0,0,1)",easeInQuad:"cubic-bezier(.55,.085,.68,.53)",easeOutQuad:"cubic-bezier(.25,.46,.45,.94)",easeInOutQuad:"cubic-bezier(.455,.03,.515,.955)",easeInQuart:"cubic-bezier(.895,.03,.685,.22)",easeOutQuart:"cubic-bezier(.165,.84,.44,1)",easeInOutQuart:"cubic-bezier(.77,0,.175,1)",easeInQuint:"cubic-bezier(.755,.05,.855,.06)",easeOutQuint:"cubic-bezier(.23,1,.32,1)",easeInOutQuint:"cubic-bezier(.86,0,.07,1)",easeInSine:"cubic-bezier(.47,0,.745,.715)",easeOutSine:"cubic-bezier(.39,.575,.565,1)",easeInOutSine:"cubic-bezier(.445,.05,.55,.95)",easeInBack:"cubic-bezier(.6,-.28,.735,.045)",easeOutBack:"cubic-bezier(.175, .885,.32,1.275)",easeInOutBack:"cubic-bezier(.68,-.55,.265,1.55)"};k.cssHooks["transit:transform"]={get:function(r){return k(r).data("transform")||new j()},set:function(s,r){var t=r;if(!(t instanceof j)){t=new j(t)}if(q.transform==="WebkitTransform"&&!a){s.style[q.transform]=t.toString(true)}else{s.style[q.transform]=t.toString()}k(s).data("transform",t)}};k.cssHooks.transform={set:k.cssHooks["transit:transform"].set};if(k.fn.jquery<"1.8"){k.cssHooks.transformOrigin={get:function(r){return r.style[q.transformOrigin]},set:function(r,s){r.style[q.transformOrigin]=s}};k.cssHooks.transition={get:function(r){return r.style[q.transition]},set:function(r,s){r.style[q.transition]=s}}}n("scale");n("translate");n("rotate");n("rotateX");n("rotateY");n("rotate3d");n("perspective");n("skewX");n("skewY");n("x",true);n("y",true);function j(r){if(typeof r==="string"){this.parse(r)}return this}j.prototype={setFromString:function(t,s){var r=(typeof s==="string")?s.split(","):(s.constructor===Array)?s:[s];r.unshift(t);j.prototype.set.apply(this,r)},set:function(s){var r=Array.prototype.slice.apply(arguments,[1]);if(this.setter[s]){this.setter[s].apply(this,r)}else{this[s]=r.join(",")}},get:function(r){if(this.getter[r]){return this.getter[r].apply(this)}else{return this[r]||0}},setter:{rotate:function(r){this.rotate=o(r,"deg")},rotateX:function(r){this.rotateX=o(r,"deg")},rotateY:function(r){this.rotateY=o(r,"deg")},scale:function(r,s){if(s===undefined){s=r}this.scale=r+","+s},skewX:function(r){this.skewX=o(r,"deg")},skewY:function(r){this.skewY=o(r,"deg")},perspective:function(r){this.perspective=o(r,"px")},x:function(r){this.set("translate",r,null)},y:function(r){this.set("translate",null,r)},translate:function(r,s){if(this._translateX===undefined){this._translateX=0}if(this._translateY===undefined){this._translateY=0}if(r!==null&&r!==undefined){this._translateX=o(r,"px")}if(s!==null&&s!==undefined){this._translateY=o(s,"px")}this.translate=this._translateX+","+this._translateY}},getter:{x:function(){return this._translateX||0},y:function(){return this._translateY||0},scale:function(){var r=(this.scale||"1,1").split(",");if(r[0]){r[0]=parseFloat(r[0])}if(r[1]){r[1]=parseFloat(r[1])}return(r[0]===r[1])?r[0]:r},rotate3d:function(){var t=(this.rotate3d||"0,0,0,0deg").split(",");for(var r=0;r<=3;++r){if(t[r]){t[r]=parseFloat(t[r])}}if(t[3]){t[3]=o(t[3],"deg")}return t}},parse:function(s){var r=this;s.replace(/([a-zA-Z0-9]+)\((.*?)\)/g,function(t,v,u){r.setFromString(v,u)})},toString:function(t){var s=[];for(var r in this){if(this.hasOwnProperty(r)){if((!q.transform3d)&&((r==="rotateX")||(r==="rotateY")||(r==="perspective")||(r==="transformOrigin"))){continue}if(r[0]!=="_"){if(t&&(r==="scale")){s.push(r+"3d("+this[r]+",1)")}else{if(t&&(r==="translate")){s.push(r+"3d("+this[r]+",0)")}else{s.push(r+"("+this[r]+")")}}}}}return s.join(" ")}};function m(s,r,t){if(r===true){s.queue(t)}else{if(r){s.queue(r,t)}else{t()}}}function h(s){var r=[];k.each(s,function(t){t=k.camelCase(t);t=k.transit.propertyMap[t]||k.cssProps[t]||t;t=c(t);if(k.inArray(t,r)===-1){r.push(t)}});return r}function g(s,v,x,r){var t=h(s);if(k.cssEase[x]){x=k.cssEase[x]}var w=""+l(v)+" "+x;if(parseInt(r,10)>0){w+=" "+l(r)}var u=[];k.each(t,function(z,y){u.push(y+" "+w)});return u.join(", ")}k.fn.transition=k.fn.transit=function(z,s,y,C){var D=this;var u=0;var w=true;if(typeof s==="function"){C=s;s=undefined}if(typeof y==="function"){C=y;y=undefined}if(typeof z.easing!=="undefined"){y=z.easing;delete z.easing}if(typeof z.duration!=="undefined"){s=z.duration;delete z.duration}if(typeof z.complete!=="undefined"){C=z.complete;delete z.complete}if(typeof z.queue!=="undefined"){w=z.queue;delete z.queue}if(typeof z.delay!=="undefined"){u=z.delay;delete z.delay}if(typeof s==="undefined"){s=k.fx.speeds._default}if(typeof y==="undefined"){y=k.cssEase._default}s=l(s);var E=g(z,s,y,u);var B=k.transit.enabled&&q.transition;var t=B?(parseInt(s,10)+parseInt(u,10)):0;if(t===0){var A=function(F){D.css(z);if(C){C.apply(D)}if(F){F()}};m(D,w,A);return D}var x={};var r=function(H){var G=false;var F=function(){if(G){D.unbind(f,F)}if(t>0){D.each(function(){this.style[q.transition]=(x[this]||null)})}if(typeof C==="function"){C.apply(D)}if(typeof H==="function"){H()}};if((t>0)&&(f)&&(k.transit.useTransitionEnd)){G=true;D.bind(f,F)}else{window.setTimeout(F,t)}D.each(function(){if(t>0){this.style[q.transition]=E}k(this).css(z)})};var v=function(F){this.offsetWidth;r(F)};m(D,w,v);return this};function n(s,r){if(!r){k.cssNumber[s]=true}k.transit.propertyMap[s]=q.transform;k.cssHooks[s]={get:function(v){var u=k(v).css("transit:transform");return u.get(s)},set:function(v,w){var u=k(v).css("transit:transform");u.setFromString(s,w);k(v).css({"transit:transform":u})}}}function c(r){return r.replace(/([A-Z])/g,function(s){return"-"+s.toLowerCase()})}function o(s,r){if((typeof s==="string")&&(!s.match(/^[\-0-9\.]+$/))){return s}else{return""+s+r}}function l(s){var r=s;if(k.fx.speeds[r]){r=k.fx.speeds[r]}return o(r,"ms")}k.transit.getTransitionValue=g})(jQuery);
// Generated by CoffeeScript 1.6.3
(function() {
  $.widget("ui.iSlider", {
    options: {
      width: 100,
      slides: [],
      template: false,
      templateHtml: '',
      speed: 5000,
      autoplay: true,
      debug: false,
      width: 100,
      height: 100,
      shadowSize: 4
    },
    totalWidth: 0,
    currentSlide: 0,
    slidesContainer: null,
    slideTimer: 0,
    parseTemplate: function(data) {
      var templateResult;
      templateResult = this.options.templateHtml;
      $.each(data, function(key, value) {
        templateResult = templateResult.replace(new RegExp("{" + key + "}", "gm"), value);
      });
      return templateResult;
    },
    createHtml: function(element) {
      var body, containerHeight, containerWidth, firstSlide, self;
      self = this;
      body = $('<div />').addClass('iSliderBody');
      containerWidth = self.options.width + 2;
      containerHeight = self.options.height + 2 + self.options.shadowSize;
      self.slidesContainer = $('<div />').addClass('iSlidesContainer');
      self.slidesContainer.css({
        "height": self.options.height + 25
      });
      $(body).append(self.slidesContainer);
      $(self.slidesContainer).append(self.getControls(self));
      $(element).append(body);
      firstSlide = self.options.slides[0];
      self.fillData(firstSlide);
    },
    getControls: function(parent) {
      var controls, next, prev;
      controls = $('<div />').addClass('iSliderControls');
      next = $('<a />').addClass('controlNext');
      next.on('click', function() {
        parent.stopTimer();
        parent.slide(true);
      });
      controls.append(next);
      prev = $('<a />').addClass('controlPrev');
      prev.on('click', function() {
        parent.stopTimer();
        parent.slide(false);
      });
      controls.append(prev);
      return controls;
    },
    addShadow: function(element, color) {
      $(element).css({
        '--webkit-box-shadow': "0 " + this.options.shadowSize + "px 0 " + color,
        '-moz-box-shadow': "0 " + this.options.shadowSize + "px 0 " + color,
        'box-shadow': "0 " + this.options.shadowSize + "px 0 " + color
      });
    },
    _create: function() {
      var el, o, self;
      self = this;
      o = self.options;
      el = self.element;
      self.createHtml(el);
      self.startTimer();
    },
    startTimer: function() {
      var self;
      self = this;
      clearInterval(self.slideTimer);
      self.slideTimer = setInterval(function() {
        return self.slide();
      }, self.options.speed);
    },
    stopTimer: function() {
      var self;
      self = this;
      clearInterval(self.slideTimer);
    },
    slide: function(next) {
      var direction, nextSlideObject, self;
      if (next == null) {
        next = true;
      }
      self = this;
      if (next === true) {
        self.setNextSlide();
        direction = 1;
      } else {
        self.setPrevSlide();
        direction = -1;
      }
      nextSlideObject = self.options.slides[self.currentSlide];
      self.switchData(nextSlideObject, direction);
    },
    setNextSlide: function() {
      var next, self;
      self = this;
      next = self.currentSlide + 1;
      next = next === self.options.slides.length ? 0 : next;
      self.currentSlide = next;
      return next;
    },
    setPrevSlide: function() {
      var next, self;
      self = this;
      next = self.currentSlide - 1;
      next = next === -1 ? self.options.slides.length - 1 : next;
      self.currentSlide = next;
      return next;
    },
    switchData: function(slide, direction) {
      var antisign, self, sign;
      self = this;
      sign = direction > 0 ? "+" : "-";
      antisign = direction > 0 ? "-" : "+";
      $(self.slidesContainer).transition({
        perspective: '800px',
        rotateY: "" + sign + "=90deg"
      }, 250, 'easeInBack', function() {
        $('.iSlide', this).remove();
        self.fillData(slide);
        this.transition({
          perspective: '800px',
          rotateY: "" + sign + "=270deg"
        }, 750, 'easeOutBack', function() {
          return this.transition({
            perspective: '800px',
            rotateY: "" + antisign + "=360deg"
          }, 1, 'ease', function() {
            return self.startTimer();
          });
        });
      });
    },
    fillData: function(slide) {
      var self, slideBody;
      self = this;
      slideBody = $(self.parseTemplate(slide)).addClass('iSlide');
      slideBody.css({
        'width': self.options.width,
        'height': self.options.height
      });
      self.addShadow(slideBody, slide.shadow);
      self.slidesContainer.append(slideBody);
      self.totalWidth += $(slideBody).width();
    },
    rotateContainer: function() {},
    destroy: function() {
      this.element.next().remove();
    },
    _setOption: function(option, value) {
      var el;
      $.Widget.prototype._setOption.apply(this, arguments);
      el = this.element;
    }
  });

  $(document).ready(function() {
    $('#slider_01').iSlider({
      templateHtml: "<a rel=\"noindex, nofollow\" href=\"http://{link}\" target=\"_blank\" class=\"slideBody\" style=\"background-image: url('{background}'); border-color:{borderColor}; \">\n  <div class=\"title\" style=\"color:{shadow}\">{title}</div>\n  <div class=\"link\" style=\"background-color:{shadow}\">{link}</div>\n</a>",
      debug: false,
      width: 200,
      height: 295,
      slides: [
        {
          background: '/static/banners/img/bg_10.png',
          title: 'Халяль<br/>справочник России',
          link: 'HALALGID.com',
          borderColor: '#819F71',
          shadow: '#1C4302'
        }, {
          background: '/static/banners/img/bg_02.png',
          title: 'Образовательный<br/>портал',
          link: 'BAYTALHIKMA.huzur.ru',
          borderColor: '#AE8F6F',
          shadow: '#8F3F1D'
        }, {
          background: '/static/banners/img/bg_03.png',
          title: 'Электронная<br/>онлайн-библиотека',
          link: 'DARUL-KUTUB.com',
          borderColor: '#AD8E6F',
          shadow: '#8E3E1B'
        }, /*{
          background: '/static/banners/img/bg_04.png',
          title: 'Мусульманский<br/>женский портал',
          link: 'www.ANNISA-TODAY.ru',
          borderColor: '#9BBF9A',
          shadow: '#418845'
        }, */{
          background: '/static/banners/img/bg_05.png',
          title: 'Время намаза',
          link: 'NAMAZ-TIME.huzur.ru',
          borderColor: '#C89182',
          shadow: '#842515'
        }, 
{
          background: '/static/banners/radio.jpg',
          title: 'ИНТЕРНЕТ-РАДИО<br/>"АЗАН"',
          link: 'www.RADIOAZAN.ru',
          borderColor: '#fff',//483220
          shadow: '#f5923d',
          fontSize: 16
      },
 {
          background: '/static/banners/img/bg_07.png',
          title: 'МУСУЛЬМАНСКИЙ<br/>МОЛОДЕЖНЫЙ ПОРТАЛ',
          link: 'YOUNGMUSLIMS.huzur.ru',
          borderColor: '#448D48',
          shadow: '#448D48',
          fontSize: 16
      }, {
          background: '/static/banners/img/bg_01.png',
          title: 'ИНТЕРНЕТ-МАГАЗИН<br/>ИСЛАМСКИХ ТОВАРОВ',
//          link: 'www.SHOP.ISLAM-TODAY.com',
          link: 'www.HUZURSHOP.ru',
          borderColor: '#819F71',
          shadow: '#1C4302',
          fontSize: 14
      }, {
          background: '/static/banners/img/bg_09.png',
          title: 'ИЗДАТЕЛЬСКИЙ ДОМ<br/>ХУЗУР',
          link: 'www.HUZUR.ru',
          borderColor: '#215656',
          shadow: '#215656',
          fontSize: 14
      }
      ]
    });
//    $('#slider_02').iSlider({
//      templateHtml: "<a href=\"http://{link}\" target=\"_blank\" class=\"slideBody\" style=\"background-image: url('{background}'); border-color:{borderColor}; \">\n</a>",
//      debug: false,
//      width: 212,
//      height: 308,
//      speed: 15000,
//      slides: [
//        {
//            background: '/static/banners/img/maidenly/04.png',
//            title: 'Интернет-магазин мусульманской одежды',
//            link: 'maidenly.ru',
//            borderColor: '#604991',
//            shadow: '#604991'
//        }, {
//              background: '/static/banners/img/maidenly/05.jpg',
//              title: 'Интернет-магазин мусульманской одежды',
//              link: 'maidenly.ru/catalog/yubki/yubka_veil_uteplennaya_chernaya_na_koketke/',
//              borderColor: '#604991',
//              shadow: '#604991'
//          }, {
//              background: '/static/banners/img/maidenly/06.jpg',
//              title: 'Интернет-магазин мусульманской одежды',
//              link: 'maidenly.ru/catalog/platya/plate_veil_elegiya_nezhnosti/',
//              borderColor: '#604991',
//              shadow: '#604991'
//          }, {
//              background: '/static/banners/img/maidenly/07.jpg',
//              title: 'Интернет-магазин мусульманской одежды',
//              link: 'maidenly.ru/catalog/platki_i_palantiny/?SECTION_ID=245&SHOWALL_1=1',
//              borderColor: '#604991',
//              shadow: '#604991'
//        }
//      ]
//    });
    $('#slider_03').iSlider({
        templateHtml: "<a href=\"http://{link}\" target=\"_blank\" class=\"slideBody\" style=\"background-image: url('{background}'); border-color:{borderColor}; \">\n</a>",
        debug: false,
        width: 212,
        height: 308,
        speed: 15000,
        slides: [
            {
                background: '/static/banners/img/islamicbooks/01.png',
                title: 'Интернет-магазин мусульманских книг и дисков',
                link: 'islamicbooks.ru',
                borderColor: '#604991',
                shadow: '#604991'
            }, {
                background: '/static/banners/img/islamicbooks/2.png',
                title: 'Интернет-магазин мусульманских книг и дисков',
                link: 'islamicbooks.ru',
                borderColor: '#604991',
                shadow: '#604991'
            }, {
                background: '/static/banners/img/islamicbooks/3.png',
                title: 'Интернет-магазин мусульманских книг и дисков',
                link: 'islamicbooks.ru',
                borderColor: '#604991',
                shadow: '#604991'
            }, {
                background: '/static/banners/img/islamicbooks/41.png',
                title: 'Интернет-магазин мусульманских книг и дисков',
                link: 'islamicbooks.ru',
                borderColor: '#604991',
                shadow: '#604991'
            }, {
                background: '/static/banners/img/islamicbooks/5.jpg',
                title: 'Интернет-магазин мусульманских книг и дисков',
                link: 'islamicbooks.ru',
                borderColor: '#604991',
                shadow: '#604991'
            }
        ]
    });
});

}).call(this);

/*
//@ sourceMappingURL=islider.map
*/

/*
	Script: jQuery.xdretroclock.js
	Plug-in for jQuery retro clock
*/

/*
	Author:
		Valeriy Chupurnov <leroy@xdan.ru>, <http://xdan.ru>
	License:
		LGPL - Lesser General Public License

	Posted <http://xdan.ru/project/xdretroclock/index.html>
*/
(function($){
jQuery.fn.xdRetroClock = jQuery.fn.xdretroclock = function(options){
	var settings = {
		digitImages: 6,
		digitWidth: 32,
		digitHeight: 47,
		showHour:true,
		showMinute:true,
		showSecond:true,
        showSeparator:false,
		am:false,
		tzoneOffset:0,
		speedFlip:100
	};

	var getmt =  function (up){
		return (-(((up)?up-1:9)*options.digitImages-1)*options.digitHeight);
	}
	var div = function(val, by){
		return (val - val % by) / by;
	}
	var current = {
		"h1": -1,
		"h2": -1,
		"m1": -1,
		"m2": -1,
		"s1": -1,
		"s2": -1
	};
	options = $.extend(settings,options);
	var setmargin = function($box, mt, rec){
		if( rec==1 )$box.css('margin-top',mt+'px');
		$box.css('margin-top',(mt-options.digitHeight)+'px' );
		if( rec<options.digitImages )
			setTimeout(function(){
				setmargin($box,mt-options.digitHeight,++rec);	
			},options.speedFlip);
	}

    $.fn.time = function(time) {
        options.time = time;

    }

	var calcTime = function($box){
        var now = options.time;
        if (now === undefined || now === 0) {
            now = new Date();
        }
		var	h = (now.getHours()+options.tzoneOffset)%(options.am?12:24),
		h1 = div(h,10),
		h2 = h%10,
		m1 = div((now.getMinutes() + 1 ), 10),
		m2 = (now.getMinutes() + 1)  % 10,
		s1 = div(now.getSeconds() , 10),
		s2 = now.getSeconds() % 10;
		if( options.showHour && h2 != current.h2 ){
			setmargin($box.find('.hourRight div'),getmt(h2),1);
			current.h2 = h2;
		}
		if( options.showHour && h1 != current.h1){
			setmargin($box.find('.hourLeft div'),getmt(h1),1);
			current.h1 = h1;
		}
		if( options.showMinute && m2 != current.m2){
			setmargin($box.find('.minuteRight div'),getmt(m2),1);
			current.m2 = m2;
		}
		if( options.showMinute && m1 != current.m1){
			setmargin($box.find('.minuteLeft div'),getmt(m1),1);
			current.m1 = m1;
		}
		
	    if ( options.showSecond && s2 != current.s2){
			setmargin($box.find('.secondRight div'),getmt(s2),1);
			current.s2 = s2;
		}
		if ( options.showSecond && s1 != current.s1){
			setmargin($box.find('.secondLeft div'),getmt(s1),1);
			current.s1 = s1;
		}
	}
	return this.each(function(){
		var $box = $(this);
        if($box.hasClass('retroclock') == false) {
            $box.addClass('retroclock');
        }

		if( options.showHour ) {
            $box.append('<div class="xdgroup"><div class="xddigit hourLeft"><div></div></div><div class="xddigit hourRight"><div></div></div></div>');
        }
		if( options.showHour && (options.showMinute || options.showSecond) && options.showSeparator )
			$box.append('<div class="rcSeparator"></div>');
		if( options.showMinute ) {
            $box.append('<div class="xdgroup"><div class="xddigit minuteLeft"><div></div></div><div class="xddigit minuteRight"><div></div></div></div>');
        }
		if( options.showMinute && options.showSecond && options.showSeparator )
			$box.append('<div class="rcSeparator"></div>');
		if( options.showSecond ) {
            $box.append('<div class="xdgroup"><div class="xddigit secondLeft"><div></div></div><div class="xddigit secondRight"><div></div></div></div>');
        }
		if( options.showHour || options.showMinute || options.showSecond )
			$box.append('<div style="clear:both;float:none;"></div>');
		setInterval(function(){

			calcTime($box);
		},1000)
		calcTime($box);
	});
}
})(jQuery);

(function(e){"undefined"==typeof e.fn.each2&&e.fn.extend({each2:function(h){for(var n=e([0]),m=-1,w=this.length;++m<w&&(n.context=n[0]=this[m])&&!1!==h.call(n[0],m,n););return this}})})(jQuery);
(function(e,h){function n(a,b){for(var c=0,d=b.length;c<d;c+=1)if(m(a,b[c]))return c;return-1}function m(a,b){return a===b?!0:a===h||b===h||null===a||null===b?!1:a.constructor===String?a===b+"":b.constructor===String?b===a+"":!1}function w(a,b){var c,d,g;if(null===a||1>a.length)return[];c=a.split(b);d=0;for(g=c.length;d<g;d+=1)c[d]=e.trim(c[d]);return c}function D(a){a.bind("keydown",function(){e.data(a,"keyup-change-value")===h&&e.data(a,"keyup-change-value",a.val())});a.bind("keyup",function(){var b=
e.data(a,"keyup-change-value");b!==h&&a.val()!==b&&(e.removeData(a,"keyup-change-value"),a.trigger("keyup-change"))})}function E(a,b,c){c=c||h;var d;return function(){var g=arguments;window.clearTimeout(d);d=window.setTimeout(function(){b.apply(c,g)},a)}}function F(a){a[0]!==document.activeElement&&window.setTimeout(function(){var b=a[0],c=a.val().length;a.focus();b.setSelectionRange?b.setSelectionRange(c,c):b.createTextRange&&(b=b.createTextRange(),b.collapse(!0),b.moveEnd("character",c),b.moveStart("character",
c),b.select())},0)}function k(a){a.preventDefault();a.stopPropagation()}function z(a,b,c){var d,g=[],p;d=a.attr("class");"string"===typeof d&&e(d.split(" ")).each2(function(){0===this.indexOf("select2-")&&g.push(this)});d=b.attr("class");"string"===typeof d&&e(d.split(" ")).each2(function(){0!==this.indexOf("select2-")&&(p=c(this),"string"===typeof p&&0<p.length&&g.push(this))});a.attr("class",g.join(" "))}function G(a,b,c,d){var g=a.toUpperCase().indexOf(b.toUpperCase());b=b.length;0>g?c.push(d(a)):
(c.push(d(a.substring(0,g))),c.push("<span class='select2-match'>"),c.push(d(a.substring(g,g+b))),c.push("</span>"),c.push(d(a.substring(g+b,a.length))))}function H(a){var b,c=0,d=null,g=a.quietMillis||100,p=a.url,l=this;return function(j){window.clearTimeout(b);b=window.setTimeout(function(){var b=c+=1,g=a.data,x=p,h=a.transport||e.ajax,f=a.type||"GET",k={},g=g?g.call(l,j.term,j.page,j.context):null,x="function"===typeof x?x.call(l,j.term,j.page,j.context):x;null!==d&&d.abort();a.params&&(e.isFunction(a.params)?
e.extend(k,a.params.call(l)):e.extend(k,a.params));e.extend(k,{url:x,dataType:a.dataType,data:g,type:f,cache:!1,success:function(d){b<c||(d=a.results(d,j.page),j.callback(d))}});d=h.call(l,k)},g)}}function I(a){var b=a,c,d,g=function(a){return""+a.text};e.isArray(b)&&(d=b,b={results:d});!1===e.isFunction(b)&&(d=b,b=function(){return d});a=b();a.text&&(g=a.text,e.isFunction(g)||(c=b.text,g=function(a){return a[c]}));return function(a){var c=a.term,d={results:[]},h;""===c?a.callback(b()):(h=function(b,
d){var j,f;b=b[0];if(b.children){j={};for(f in b)b.hasOwnProperty(f)&&(j[f]=b[f]);j.children=[];e(b.children).each2(function(a,b){h(b,j.children)});(j.children.length||a.matcher(c,g(j),b))&&d.push(j)}else a.matcher(c,g(b),b)&&d.push(b)},e(b().results).each2(function(a,b){h(b,d.results)}),a.callback(d))}}function J(a){var b=e.isFunction(a);return function(c){var d=c.term,g={results:[]};e(b?a():a).each(function(){var a=this.text!==h,b=a?this.text:this;if(""===d||c.matcher(d,b))g.results.push(a?this:
{id:this,text:this})});c.callback(g)}}function y(a){if(e.isFunction(a))return!0;if(!a)return!1;throw Error("formatterName must be a function or a falsy value");}function r(a){return e.isFunction(a)?a():a}function K(a){var b=0;e.each(a,function(a,d){d.children?b+=K(d.children):b++});return b}function N(a,b,c,d){var g=a,e=!1,l,j,f,q;if(!d.createSearchChoice||!d.tokenSeparators||1>d.tokenSeparators.length)return h;for(;;){e=-1;j=0;for(f=d.tokenSeparators.length;j<f&&!(q=d.tokenSeparators[j],e=a.indexOf(q),
0<=e);j++);if(0>e)break;l=a.substring(0,e);a=a.substring(e+q.length);if(0<l.length&&(l=d.createSearchChoice(l,b),l!==h&&null!==l&&d.id(l)!==h&&null!==d.id(l))){e=!1;j=0;for(f=b.length;j<f;j++)if(m(d.id(l),d.id(b[j]))){e=!0;break}e||c(l)}}if(g!==a)return a}function A(a,b){var c=function(){};c.prototype=new a;c.prototype.constructor=c;c.prototype.parent=a.prototype;c.prototype=e.extend(c.prototype,b);return c}if(window.Select2===h){var f,u,B,C,L,v,M;f={TAB:9,ENTER:13,ESC:27,SPACE:32,LEFT:37,UP:38,RIGHT:39,
DOWN:40,SHIFT:16,CTRL:17,ALT:18,PAGE_UP:33,PAGE_DOWN:34,HOME:36,END:35,BACKSPACE:8,DELETE:46,isArrow:function(a){a=a.which?a.which:a;switch(a){case f.LEFT:case f.RIGHT:case f.UP:case f.DOWN:return!0}return!1},isControl:function(a){switch(a.which){case f.SHIFT:case f.CTRL:case f.ALT:return!0}return a.metaKey?!0:!1},isFunctionKey:function(a){a=a.which?a.which:a;return 112<=a&&123>=a}};u=e(document);var O=1;L=function(){return O++};u.bind("mousemove",function(a){M={x:a.pageX,y:a.pageY}});u=A(Object,
{bind:function(a){var b=this;return function(){a.apply(b,arguments)}},init:function(a){var b,c;this.opts=a=this.prepareOpts(a);this.id=a.id;a.element.data("select2")!==h&&null!==a.element.data("select2")&&this.destroy();this.enabled=!0;this.container=this.createContainer();this.containerId="s2id_"+(a.element.attr("id")||"autogen"+L());this.containerSelector="#"+this.containerId.replace(/([;&,\.\+\*\~':"\!\^#$%@\[\]\(\)=>\|])/g,"\\$1");this.container.attr("id",this.containerId);var d=!1,g;this.body=
function(){!1===d&&(g=a.element.closest("body"),d=!0);return g};z(this.container,this.opts.element,this.opts.adaptContainerCssClass);this.container.css(r(a.containerCss));this.container.addClass(r(a.containerCssClass));this.elementTabIndex=this.opts.element.attr("tabIndex");this.opts.element.data("select2",this).addClass("select2-offscreen").bind("focus.select2",function(){e(this).select2("focus")}).attr("tabIndex","-1").before(this.container);this.container.data("select2",this);this.dropdown=this.container.find(".select2-drop");
this.dropdown.addClass(r(a.dropdownCssClass));this.dropdown.data("select2",this);this.results=b=this.container.find(".select2-results");this.search=c=this.container.find("input.select2-input");c.attr("tabIndex",this.elementTabIndex);this.resultsPage=0;this.context=null;this.initContainer();this.results.bind("mousemove",function(a){var b=M;(b===h||b.x!==a.pageX||b.y!==a.pageY)&&e(a.target).trigger("mousemove-filtered",a)});this.dropdown.delegate(".select2-results","mousemove-filtered touchstart touchmove touchend",
this.bind(this.highlightUnderEvent));var p=this.results,l=E(80,function(a){p.trigger("scroll-debounced",a)});p.bind("scroll",function(a){0<=n(a.target,p.get())&&l(a)});this.dropdown.delegate(".select2-results","scroll-debounced",this.bind(this.loadMoreIfNeeded));e.fn.mousewheel&&b.mousewheel(function(a,c,d,g){c=b.scrollTop();0<g&&0>=c-g?(b.scrollTop(0),k(a)):0>g&&b.get(0).scrollHeight-b.scrollTop()+g<=b.height()&&(b.scrollTop(b.get(0).scrollHeight-b.height()),k(a))});D(c);c.bind("keyup-change input paste",
this.bind(this.updateResults));c.bind("focus",function(){c.addClass("select2-focused")});c.bind("blur",function(){c.removeClass("select2-focused")});this.dropdown.delegate(".select2-results","mouseup",this.bind(function(a){0<e(a.target).closest(".select2-result-selectable").length&&(this.highlightUnderEvent(a),this.selectHighlighted(a))}));this.dropdown.bind("click mouseup mousedown",function(a){a.stopPropagation()});e.isFunction(this.opts.initSelection)&&(this.initSelection(),this.monitorSource());
(a.element.is(":disabled")||a.element.is("[readonly='readonly']"))&&this.disable()},destroy:function(){var a=this.opts.element.data("select2");this.propertyObserver&&(delete this.propertyObserver,this.propertyObserver=null);a!==h&&(a.container.remove(),a.dropdown.remove(),a.opts.element.removeClass("select2-offscreen").removeData("select2").unbind(".select2").attr({tabIndex:this.elementTabIndex}).show())},prepareOpts:function(a){var b,c,d;b=a.element;"select"===b.get(0).tagName.toLowerCase()&&(this.select=
c=a.element);c&&e.each("id multiple ajax query createSearchChoice initSelection data tags".split(" "),function(){if(this in a)throw Error("Option '"+this+"' is not allowed for Select2 when attached to a <select> element.");});a=e.extend({},{populateResults:function(b,c,d){var j,f=this.opts.id,q=this;j=function(b,c,g){var p,k,m,n,r,t,s;b=a.sortResults(b,c,d);p=0;for(k=b.length;p<k;p+=1)m=b[p],r=!0===m.disabled,n=!r&&f(m)!==h,t=m.children&&0<m.children.length,s=e("<li></li>"),s.addClass("select2-results-dept-"+
g),s.addClass("select2-result"),s.addClass(n?"select2-result-selectable":"select2-result-unselectable"),r&&s.addClass("select2-disabled"),t&&s.addClass("select2-result-with-children"),s.addClass(q.opts.formatResultCssClass(m)),n=e(document.createElement("div")),n.addClass("select2-result-label"),r=a.formatResult(m,n,d,q.opts.escapeMarkup),r!==h&&n.html(r),s.append(n),t&&(t=e("<ul></ul>"),t.addClass("select2-result-sub"),j(m.children,t,g+1),s.append(t)),s.data("select2-data",m),c.append(s)};j(c,b,
0)}},e.fn.select2.defaults,a);"function"!==typeof a.id&&(d=a.id,a.id=function(a){return a[d]});if(e.isArray(a.element.data("select2Tags"))){if("tags"in a)throw"tags specified as both an attribute 'data-select2-tags' and in options of Select2 "+a.element.attr("id");a.tags=a.element.attr("data-select2-tags")}if(c)a.query=this.bind(function(a){var c={results:[],more:!1},d=a.term,j,f,q;q=function(b,c){var e;b.is("option")?a.matcher(d,b.text(),b)&&c.push({id:b.attr("value"),text:b.text(),element:b.get(),
css:b.attr("class"),disabled:m(b.attr("disabled"),"disabled")}):b.is("optgroup")&&(e={text:b.attr("label"),children:[],element:b.get(),css:b.attr("class")},b.children().each2(function(a,b){q(b,e.children)}),0<e.children.length&&c.push(e))};j=b.children();this.getPlaceholder()!==h&&0<j.length&&(f=j[0],""===e(f).text()&&(j=j.not(f)));j.each2(function(a,b){q(b,c.results)});a.callback(c)}),a.id=function(a){return a.id},a.formatResultCssClass=function(a){return a.css};else if(!("query"in a))if("ajax"in
a){if((c=a.element.data("ajax-url"))&&0<c.length)a.ajax.url=c;a.query=H.call(a.element,a.ajax)}else"data"in a?a.query=I(a.data):"tags"in a&&(a.query=J(a.tags),a.createSearchChoice===h&&(a.createSearchChoice=function(a){return{id:a,text:a}}),a.initSelection===h&&(a.initSelection=function(b,c){var d=[];e(w(b.val(),a.separator)).each(function(){var b=this,c=this,g=a.tags;e.isFunction(g)&&(g=g());e(g).each(function(){if(m(this.id,b))return c=this.text,!1});d.push({id:b,text:c})});c(d)}));if("function"!==
typeof a.query)throw"query function not defined for Select2 "+a.element.attr("id");return a},monitorSource:function(){var a=this.opts.element,b;a.bind("change.select2",this.bind(function(){!0!==this.opts.element.data("select2-change-triggered")&&this.initSelection()}));b=this.bind(function(){var a,b;a="disabled"!==this.opts.element.attr("disabled");b="readonly"===this.opts.element.attr("readonly");a=a&&!b;this.enabled!==a&&(a?this.enable():this.disable());z(this.container,this.opts.element,this.opts.adaptContainerCssClass);
this.container.addClass(r(this.opts.containerCssClass));z(this.dropdown,this.opts.element,this.opts.adaptDropdownCssClass);this.dropdown.addClass(r(this.opts.dropdownCssClass))});a.bind("propertychange.select2 DOMAttrModified.select2",b);"undefined"!==typeof WebKitMutationObserver&&(this.propertyObserver&&(delete this.propertyObserver,this.propertyObserver=null),this.propertyObserver=new WebKitMutationObserver(function(a){a.forEach(b)}),this.propertyObserver.observe(a.get(0),{attributes:!0,subtree:!1}))},
triggerChange:function(a){a=a||{};a=e.extend({},a,{type:"change",val:this.val()});this.opts.element.data("select2-change-triggered",!0);this.opts.element.trigger(a);this.opts.element.data("select2-change-triggered",!1);this.opts.element.click();this.opts.blurOnChange&&this.opts.element.blur()},enable:function(){this.enabled||(this.enabled=!0,this.container.removeClass("select2-container-disabled"),this.opts.element.removeAttr("disabled"))},disable:function(){this.enabled&&(this.close(),this.enabled=
!1,this.container.addClass("select2-container-disabled"),this.opts.element.attr("disabled","disabled"))},opened:function(){return this.container.hasClass("select2-dropdown-open")},positionDropdown:function(){var a=this.container.offset(),b=this.container.outerHeight(!1),c=this.container.outerWidth(!1),d=this.dropdown.outerHeight(!1),g=e(window).scrollLeft()+e(window).width(),p=e(window).scrollTop()+e(window).height(),b=a.top+b,f=a.left,p=b+d<=p,j=a.top-d>=this.body().scrollTop(),h=this.dropdown.outerWidth(!1),
g=f+h<=g,q=this.dropdown.hasClass("select2-drop-above"),k;"static"!==this.body().css("position")&&(k=this.body().offset(),b-=k.top,f-=k.left);q?(q=!0,!j&&p&&(q=!1)):(q=!1,!p&&j&&(q=!0));g||(f=a.left+c-h);q?(b=a.top-d,this.container.addClass("select2-drop-above"),this.dropdown.addClass("select2-drop-above")):(this.container.removeClass("select2-drop-above"),this.dropdown.removeClass("select2-drop-above"));a=e.extend({top:b,left:f,width:c},r(this.opts.dropdownCss));this.dropdown.css(a)},shouldOpen:function(){var a;
if(this.opened())return!1;a=e.Event("opening");this.opts.element.trigger(a);return!a.isDefaultPrevented()},clearDropdownAlignmentPreference:function(){this.container.removeClass("select2-drop-above");this.dropdown.removeClass("select2-drop-above")},open:function(){if(!this.shouldOpen())return!1;window.setTimeout(this.bind(this.opening),1);return!0},opening:function(){var a=this.containerId,b="scroll."+a,c="resize."+a,d="orientationchange."+a;this.clearDropdownAlignmentPreference();this.container.addClass("select2-dropdown-open").addClass("select2-container-active");
this.dropdown[0]!==this.body().children().last()[0]&&this.dropdown.detach().appendTo(this.body());this.updateResults(!0);a=e("#select2-drop-mask");0==a.length&&(a=e(document.createElement("div")),a.attr("id","select2-drop-mask").attr("class","select2-drop-mask"),a.hide(),a.appendTo(this.body()),a.bind("mousedown touchstart",function(){var a=e("#select2-drop");0<a.length&&(a=a.data("select2"),a.opts.selectOnBlur&&a.selectHighlighted({noFocus:!0}),a.close())}));this.dropdown.prev()[0]!==a[0]&&this.dropdown.before(a);
e("#select2-drop").removeAttr("id");this.dropdown.attr("id","select2-drop");a.css({width:document.documentElement.scrollWidth,height:document.documentElement.scrollHeight});a.show();this.dropdown.show();this.positionDropdown();this.dropdown.addClass("select2-drop-active");this.ensureHighlightVisible();var g=this;this.container.parents().add(window).each(function(){e(this).bind(c+" "+b+" "+d,function(){e("#select2-drop-mask").css({width:document.documentElement.scrollWidth,height:document.documentElement.scrollHeight});
g.positionDropdown()})});this.focusSearch()},close:function(){if(this.opened()){var a=this.containerId,b="scroll."+a,c="resize."+a,d="orientationchange."+a;this.container.parents().add(window).each(function(){e(this).unbind(b).unbind(c).unbind(d)});this.clearDropdownAlignmentPreference();e("#select2-drop-mask").hide();this.dropdown.removeAttr("id");this.dropdown.hide();this.container.removeClass("select2-dropdown-open");this.results.empty();this.clearSearch();this.opts.element.trigger(e.Event("close"))}},
clearSearch:function(){},getMaximumSelectionSize:function(){return r(this.opts.maximumSelectionSize)},ensureHighlightVisible:function(){var a=this.results,b,c,d,g;c=this.highlight();0>c||(0==c?a.scrollTop(0):(b=this.findHighlightableChoices(),d=e(b[c]),g=d.offset().top+d.outerHeight(!0),c===b.length-1&&(b=a.find("li.select2-more-results"),0<b.length&&(g=b.offset().top+b.outerHeight(!0))),b=a.offset().top+a.outerHeight(!0),g>b&&a.scrollTop(a.scrollTop()+(g-b)),g=d.offset().top-a.offset().top,0>g&&
"none"!=d.css("display")&&a.scrollTop(a.scrollTop()+g)))},findHighlightableChoices:function(){this.results.find(".select2-result-selectable:not(.select2-selected):not(.select2-disabled)");return this.results.find(".select2-result-selectable:not(.select2-selected):not(.select2-disabled)")},moveHighlight:function(a){for(var b=this.findHighlightableChoices(),c=this.highlight();-1<c&&c<b.length;){var c=c+a,d=e(b[c]);if(d.hasClass("select2-result-selectable")&&!d.hasClass("select2-disabled")&&!d.hasClass("select2-selected")){this.highlight(c);
break}}},highlight:function(a){var b=this.findHighlightableChoices();if(0===arguments.length)return n(b.filter(".select2-highlighted")[0],b.get());a>=b.length&&(a=b.length-1);0>a&&(a=0);this.results.find(".select2-highlighted").removeClass("select2-highlighted");b=e(b[a]);b.addClass("select2-highlighted");this.ensureHighlightVisible();(b=b.data("select2-data"))&&this.opts.element.trigger({type:"highlight",val:this.id(b),choice:b})},countSelectableResults:function(){return this.findHighlightableChoices().length},
highlightUnderEvent:function(a){a=e(a.target).closest(".select2-result-selectable");if(0<a.length&&!a.is(".select2-highlighted")){var b=this.findHighlightableChoices();this.highlight(b.index(a))}else 0==a.length&&this.results.find(".select2-highlighted").removeClass("select2-highlighted")},loadMoreIfNeeded:function(){var a=this.results,b=a.find("li.select2-more-results"),c,d=this.resultsPage+1,g=this,e=this.search.val(),f=this.context;0!==b.length&&(c=b.offset().top-a.offset().top-a.height(),c<=this.opts.loadMorePadding&&
(b.addClass("select2-active"),this.opts.query({element:this.opts.element,term:e,page:d,context:f,matcher:this.opts.matcher,callback:this.bind(function(c){g.opened()&&(g.opts.populateResults.call(this,a,c.results,{term:e,page:d,context:f}),!0===c.more?(b.detach().appendTo(a).text(g.opts.formatLoadMore(d+1)),window.setTimeout(function(){g.loadMoreIfNeeded()},10)):b.remove(),g.positionDropdown(),g.resultsPage=d,g.context=c.context)})})))},tokenize:function(){},updateResults:function(a){function b(){g.scrollTop(0);
d.removeClass("select2-active");j.positionDropdown()}function c(a){g.html(a);b()}var d=this.search,g=this.results,f=this.opts,l,j=this;if(!(!0!==a&&(!1===this.showSearchInput||!this.opened()))){d.addClass("select2-active");var k=this.getMaximumSelectionSize();if(1<=k&&(l=this.data(),e.isArray(l)&&l.length>=k&&y(f.formatSelectionTooBig,"formatSelectionTooBig"))){c("<li class='select2-selection-limit'>"+f.formatSelectionTooBig(k)+"</li>");return}d.val().length<f.minimumInputLength?y(f.formatInputTooShort,
"formatInputTooShort")?c("<li class='select2-no-results'>"+f.formatInputTooShort(d.val(),f.minimumInputLength)+"</li>"):c(""):(f.formatSearching()&&!0===a&&c("<li class='select2-searching'>"+f.formatSearching()+"</li>"),f.maximumInputLength&&d.val().length>f.maximumInputLength?y(f.formatInputTooLong,"formatInputTooLong")?c("<li class='select2-no-results'>"+f.formatInputTooLong(d.val(),f.maximumInputLength)+"</li>"):c(""):(l=this.tokenize(),l!=h&&null!=l&&d.val(l),this.resultsPage=1,f.query({element:f.element,
term:d.val(),page:this.resultsPage,context:null,matcher:f.matcher,callback:this.bind(function(l){var k;this.opened()&&(this.context=l.context===h?null:l.context,this.opts.createSearchChoice&&""!==d.val()&&(k=this.opts.createSearchChoice.call(null,d.val(),l.results),k!==h&&null!==k&&j.id(k)!==h&&null!==j.id(k)&&0===e(l.results).filter(function(){return m(j.id(this),j.id(k))}).length&&l.results.unshift(k)),0===l.results.length&&y(f.formatNoMatches,"formatNoMatches")?c("<li class='select2-no-results'>"+
f.formatNoMatches(d.val())+"</li>"):(g.empty(),j.opts.populateResults.call(this,g,l.results,{term:d.val(),page:this.resultsPage,context:null}),!0===l.more&&y(f.formatLoadMore,"formatLoadMore")&&(g.append("<li class='select2-more-results'>"+j.opts.escapeMarkup(f.formatLoadMore(this.resultsPage))+"</li>"),window.setTimeout(function(){j.loadMoreIfNeeded()},10)),this.postprocessResults(l,a),b()))})})))}},cancel:function(){this.close()},blur:function(){this.opts.selectOnBlur&&this.selectHighlighted({noFocus:!0});
this.close();this.container.removeClass("select2-container-active");this.search[0]===document.activeElement&&this.search.blur();this.clearSearch();this.selection.find(".select2-search-choice-focus").removeClass("select2-search-choice-focus")},focusSearch:function(){F(this.search)},selectHighlighted:function(a){var b=this.highlight(),c=this.results.find(".select2-highlighted").closest(".select2-result").data("select2-data");c&&(this.highlight(b),this.onSelect(c,a))},getPlaceholder:function(){return this.opts.element.attr("placeholder")||
this.opts.element.attr("data-placeholder")||this.opts.element.data("placeholder")||this.opts.placeholder},initContainerWidth:function(){var a=function(){var a,c,d,g;if("off"===this.opts.width)return null;if("element"===this.opts.width)return 0===this.opts.element.outerWidth(!1)?"auto":this.opts.element.outerWidth(!1)+"px";if("copy"===this.opts.width||"resolve"===this.opts.width){a=this.opts.element.attr("style");if(a!==h){a=a.split(";");d=0;for(g=a.length;d<g;d+=1)if(c=a[d].replace(/\s/g,"").match(/width:(([-+]?([0-9]*\.)?[0-9]+)(px|em|ex|%|in|cm|mm|pt|pc))/),
null!==c&&1<=c.length)return c[1]}return"resolve"===this.opts.width?(a=this.opts.element.css("width"),0<a.indexOf("%")?a:0===this.opts.element.outerWidth(!1)?"auto":this.opts.element.outerWidth(!1)+"px"):null}return e.isFunction(this.opts.width)?this.opts.width():this.opts.width}.call(this);null!==a&&this.container.css("width",a)}});B=A(u,{createContainer:function(){return e(document.createElement("div")).attr({"class":"select2-container"}).html("<a href='javascript:void(0)' onclick='return false;' class='select2-choice' tabindex='-1'>   <span></span><abbr class='select2-search-choice-close' style='display:none;'></abbr>   <div><b></b></div></a><input class='select2-focusser select2-offscreen' type='text'/><div class='select2-drop' style='display:none'>   <div class='select2-search'>       <input type='text' autocomplete='off' class='select2-input'/>   </div>   <ul class='select2-results'>   </ul></div>")},
disable:function(){this.enabled&&(this.parent.disable.apply(this,arguments),this.focusser.attr("disabled","disabled"))},enable:function(){this.enabled||(this.parent.enable.apply(this,arguments),this.focusser.removeAttr("disabled"))},opening:function(){this.parent.opening.apply(this,arguments);this.focusser.attr("disabled","disabled");this.opts.element.trigger(e.Event("open"))},close:function(){this.opened()&&(this.parent.close.apply(this,arguments),this.focusser.removeAttr("disabled"),F(this.focusser))},
focus:function(){this.opened()?this.close():(this.focusser.removeAttr("disabled"),this.focusser.focus())},isFocused:function(){return this.container.hasClass("select2-container-active")},cancel:function(){this.parent.cancel.apply(this,arguments);this.focusser.removeAttr("disabled");this.focusser.focus()},initContainer:function(){var a,b=this.container,c=this.dropdown;this.showSearch(0<=this.opts.minimumResultsForSearch);this.selection=a=b.find(".select2-choice");this.focusser=b.find(".select2-focusser");
this.search.bind("keydown",this.bind(function(a){if(this.enabled)if(a.which===f.PAGE_UP||a.which===f.PAGE_DOWN)k(a);else switch(a.which){case f.UP:case f.DOWN:this.moveHighlight(a.which===f.UP?-1:1);k(a);break;case f.TAB:case f.ENTER:this.selectHighlighted();k(a);break;case f.ESC:this.cancel(a),k(a)}}));this.focusser.bind("keydown",this.bind(function(a){if(this.enabled&&!(a.which===f.TAB||f.isControl(a)||f.isFunctionKey(a)||a.which===f.ESC))if(!1===this.opts.openOnEnter&&a.which===f.ENTER)k(a);else if(a.which==
f.DOWN||a.which==f.UP||a.which==f.ENTER&&this.opts.openOnEnter)this.open(),k(a);else if(a.which==f.DELETE||a.which==f.BACKSPACE)this.opts.allowClear&&this.clear(),k(a)}));D(this.focusser);this.focusser.bind("keyup-change input",this.bind(function(a){this.opened()||(this.open(),!1!==this.showSearchInput&&this.search.val(this.focusser.val()),this.focusser.val(""),k(a))}));a.delegate("abbr","mousedown",this.bind(function(a){this.enabled&&(this.clear(),a.preventDefault(),a.stopImmediatePropagation(),
this.close(),this.selection.focus())}));a.bind("mousedown",this.bind(function(a){this.opened()?this.close():this.enabled&&this.open();k(a)}));c.bind("mousedown",this.bind(function(){this.search.focus()}));a.bind("focus",this.bind(function(a){k(a)}));this.focusser.bind("focus",this.bind(function(){this.container.addClass("select2-container-active")})).bind("blur",this.bind(function(){this.opened()||this.container.removeClass("select2-container-active")}));this.search.bind("focus",this.bind(function(){this.container.addClass("select2-container-active")}));
this.initContainerWidth();this.setPlaceholder()},clear:function(){var a=this.selection.data("select2-data");this.opts.element.val("");this.selection.find("span").empty();this.selection.removeData("select2-data");this.setPlaceholder();this.opts.element.trigger({type:"removed",val:this.id(a),choice:a});this.triggerChange({removed:a})},initSelection:function(){if(""===this.opts.element.val()&&""===this.opts.element.text())this.close(),this.setPlaceholder();else{var a=this;this.opts.initSelection.call(null,
this.opts.element,function(b){b!==h&&null!==b&&(a.updateSelection(b),a.close(),a.setPlaceholder())})}},prepareOpts:function(){var a=this.parent.prepareOpts.apply(this,arguments);"select"===a.element.get(0).tagName.toLowerCase()?a.initSelection=function(a,c){var d=a.find(":selected");e.isFunction(c)&&c({id:d.attr("value"),text:d.text(),element:d})}:"data"in a&&(a.initSelection=a.initSelection||function(b,c){var d=b.val();a.query({matcher:function(b,c,e){return m(d,a.id(e))},callback:!e.isFunction(c)?
e.noop:function(a){c(a.results.length?a.results[0]:null)}})});return a},getPlaceholder:function(){return this.select&&""!==this.select.find("option").first().text()?h:this.parent.getPlaceholder.apply(this,arguments)},setPlaceholder:function(){var a=this.getPlaceholder();""===this.opts.element.val()&&a!==h&&!(this.select&&""!==this.select.find("option:first").text())&&(this.selection.find("span").html(this.opts.escapeMarkup(a)),this.selection.addClass("select2-default"),this.selection.find("abbr").hide())},
postprocessResults:function(a,b){var c=0,d=this,e=!0;this.findHighlightableChoices().each2(function(a,b){if(m(d.id(b.data("select2-data")),d.opts.element.val()))return c=a,!1});this.highlight(c);!0===b&&(e=this.opts.minimumResultsForSearch,e=0>e?!1:K(a.results)>=e,this.showSearch(e))},showSearch:function(a){this.showSearchInput=a;this.dropdown.find(".select2-search")[a?"removeClass":"addClass"]("select2-search-hidden");e(this.dropdown,this.container)[a?"addClass":"removeClass"]("select2-with-searchbox")},
onSelect:function(a,b){var c=this.opts.element.val();this.opts.element.val(this.id(a));this.updateSelection(a);this.opts.element.trigger({type:"selected",val:this.id(a),choice:a});this.close();(!b||!b.noFocus)&&this.selection.focus();m(c,this.id(a))||this.triggerChange()},updateSelection:function(a){var b=this.selection.find("span");this.selection.data("select2-data",a);b.empty();a=this.opts.formatSelection(a,b);a!==h&&b.append(this.opts.escapeMarkup(a));this.selection.removeClass("select2-default");
this.opts.allowClear&&this.getPlaceholder()!==h&&this.selection.find("abbr").show()},val:function(){var a,b=!1,c=null,d=this;if(0===arguments.length)return this.opts.element.val();a=arguments[0];1<arguments.length&&(b=arguments[1]);if(this.select)this.select.val(a).find(":selected").each2(function(a,b){c={id:b.attr("value"),text:b.text()};return!1}),this.updateSelection(c),this.setPlaceholder(),b&&this.triggerChange();else{if(this.opts.initSelection===h)throw Error("cannot call val() if initSelection() is not defined");
!a&&0!==a?(this.clear(),b&&this.triggerChange()):(this.opts.element.val(a),this.opts.initSelection(this.opts.element,function(a){d.opts.element.val(!a?"":d.id(a));d.updateSelection(a);d.setPlaceholder();b&&d.triggerChange()}))}},clearSearch:function(){this.search.val("");this.focusser.val("")},data:function(a){var b;if(0===arguments.length)return b=this.selection.data("select2-data"),b==h&&(b=null),b;!a||""===a?this.clear():(this.opts.element.val(!a?"":this.id(a)),this.updateSelection(a))}});C=A(u,
{createContainer:function(){return e(document.createElement("div")).attr({"class":"select2-container select2-container-multi"}).html("    <ul class='select2-choices'>  <li class='select2-search-field'>    <input type='text' autocomplete='off' class='select2-input'>  </li></ul><div class='select2-drop select2-drop-multi' style='display:none;'>   <ul class='select2-results'>   </ul></div>")},prepareOpts:function(){var a=this.parent.prepareOpts.apply(this,arguments);"select"===a.element.get(0).tagName.toLowerCase()?
a.initSelection=function(a,c){var d=[];a.find(":selected").each2(function(a,b){d.push({id:b.attr("value"),text:b.text(),element:b[0]})});c(d)}:"data"in a&&(a.initSelection=a.initSelection||function(b,c){var d=w(b.val(),a.separator);a.query({matcher:function(b,c,f){return e.grep(d,function(b){return m(b,a.id(f))}).length},callback:!e.isFunction(c)?e.noop:function(a){c(a.results)}})});return a},initContainer:function(){var a;this.searchContainer=this.container.find(".select2-search-field");this.selection=
a=this.container.find(".select2-choices");this.search.bind("input paste",this.bind(function(){this.enabled&&(this.opened()||this.open())}));this.search.bind("keydown",this.bind(function(b){if(this.enabled){if(b.which===f.BACKSPACE&&""===this.search.val()){this.close();var c;c=a.find(".select2-search-choice-focus");if(0<c.length){this.unselect(c.first());this.search.width(10);k(b);return}c=a.find(".select2-search-choice:not(.select2-locked)");0<c.length&&c.last().addClass("select2-search-choice-focus")}else a.find(".select2-search-choice-focus").removeClass("select2-search-choice-focus");
if(this.opened())switch(b.which){case f.UP:case f.DOWN:this.moveHighlight(b.which===f.UP?-1:1);k(b);return;case f.ENTER:case f.TAB:this.selectHighlighted();k(b);return;case f.ESC:this.cancel(b);k(b);return}b.which===f.TAB||f.isControl(b)||f.isFunctionKey(b)||b.which===f.BACKSPACE||b.which===f.ESC||b.which===f.ENTER&&(!1===this.opts.openOnEnter||b.altKey||b.ctrlKey||b.shiftKey||b.metaKey)||(this.open(),(b.which===f.PAGE_UP||b.which===f.PAGE_DOWN)&&k(b))}}));this.search.bind("keyup",this.bind(this.resizeSearch));
this.search.bind("blur",this.bind(function(a){this.container.removeClass("select2-container-active");this.search.removeClass("select2-focused");this.opened()||this.clearSearch();a.stopImmediatePropagation()}));this.container.delegate(".select2-choices","mousedown",this.bind(function(a){this.enabled&&!(0<e(a.target).closest(".select2-search-choice").length)&&(this.clearPlaceholder(),this.open(),this.focusSearch(),a.preventDefault())}));this.container.delegate(".select2-choices","focus",this.bind(function(){this.enabled&&
(this.container.addClass("select2-container-active"),this.dropdown.addClass("select2-drop-active"),this.clearPlaceholder())}));this.initContainerWidth();this.clearSearch()},enable:function(){this.enabled||(this.parent.enable.apply(this,arguments),this.search.removeAttr("disabled"))},disable:function(){this.enabled&&(this.parent.disable.apply(this,arguments),this.search.attr("disabled",!0))},initSelection:function(){""===this.opts.element.val()&&""===this.opts.element.text()&&(this.updateSelection([]),
this.close(),this.clearSearch());if(this.select||""!==this.opts.element.val()){var a=this;this.opts.initSelection.call(null,this.opts.element,function(b){b!==h&&null!==b&&(a.updateSelection(b),a.close(),a.clearSearch())})}},clearSearch:function(){var a=this.getPlaceholder();a!==h&&0===this.getVal().length&&!1===this.search.hasClass("select2-focused")?(this.search.val(a).addClass("select2-default"),this.resizeSearch()):this.search.val("").width(10)},clearPlaceholder:function(){this.search.hasClass("select2-default")&&
this.search.val("").removeClass("select2-default")},opening:function(){this.parent.opening.apply(this,arguments);this.clearPlaceholder();this.resizeSearch();this.focusSearch();this.opts.element.trigger(e.Event("open"))},close:function(){this.opened()&&this.parent.close.apply(this,arguments)},focus:function(){this.close();this.search.focus();this.opts.element.triggerHandler("focus")},isFocused:function(){return this.search.hasClass("select2-focused")},updateSelection:function(a){var b=[],c=[],d=this;
e(a).each(function(){0>n(d.id(this),b)&&(b.push(d.id(this)),c.push(this))});a=c;this.selection.find(".select2-search-choice").remove();e(a).each(function(){d.addSelectedChoice(this)});d.postprocessResults()},tokenize:function(){var a=this.search.val(),a=this.opts.tokenizer(a,this.data(),this.bind(this.onSelect),this.opts);null!=a&&a!=h&&(this.search.val(a),0<a.length&&this.open())},onSelect:function(a,b){this.addSelectedChoice(a);this.opts.element.trigger({type:"selected",val:this.id(a),choice:a});
(this.select||!this.opts.closeOnSelect)&&this.postprocessResults();this.opts.closeOnSelect?(this.close(),this.search.width(10)):0<this.countSelectableResults()?(this.search.width(10),this.resizeSearch(),this.val().length>=this.getMaximumSelectionSize()&&this.updateResults(!0),this.positionDropdown()):(this.close(),this.search.width(10));this.triggerChange({added:a});(!b||!b.noFocus)&&this.focusSearch()},cancel:function(){this.close();this.focusSearch()},addSelectedChoice:function(a){var b=!a.locked,
c=e("<li class='select2-search-choice'>    <div></div>    <a href='#' onclick='return false;' class='select2-search-choice-close' tabindex='-1'></a></li>"),d=e("<li class='select2-search-choice select2-locked'><div></div></li>"),c=b?c:d,d=this.id(a),g=this.getVal(),f;f=this.opts.formatSelection(a,c.find("div"));f!=h&&c.find("div").replaceWith("<div>"+this.opts.escapeMarkup(f)+"</div>");b&&c.find(".select2-search-choice-close").bind("mousedown",k).bind("click dblclick",this.bind(function(a){this.enabled&&
(e(a.target).closest(".select2-search-choice").fadeOut("fast",this.bind(function(){this.unselect(e(a.target));this.selection.find(".select2-search-choice-focus").removeClass("select2-search-choice-focus");this.close();this.focusSearch()})).dequeue(),k(a))})).bind("focus",this.bind(function(){this.enabled&&(this.container.addClass("select2-container-active"),this.dropdown.addClass("select2-drop-active"))}));c.data("select2-data",a);c.insertBefore(this.searchContainer);g.push(d);this.setVal(g)},unselect:function(a){var b=
this.getVal(),c,d;a=a.closest(".select2-search-choice");if(0===a.length)throw"Invalid argument: "+a+". Must be .select2-search-choice";if(c=a.data("select2-data"))d=n(this.id(c),b),0<=d&&(b.splice(d,1),this.setVal(b),this.select&&this.postprocessResults()),a.remove(),this.opts.element.trigger({type:"removed",val:this.id(c),choice:c}),this.triggerChange({removed:c})},postprocessResults:function(){var a=this.getVal(),b=this.results.find(".select2-result"),c=this.results.find(".select2-result-with-children"),
d=this;b.each2(function(b,c){var e=d.id(c.data("select2-data"));0<=n(e,a)&&(c.addClass("select2-selected"),c.find(".select2-result-selectable").addClass("select2-selected"))});c.each2(function(a,b){!b.is(".select2-result-selectable")&&0===b.find(".select2-result-selectable:not(.select2-selected)").length&&b.addClass("select2-selected")});-1==this.highlight()&&d.highlight(0)},resizeSearch:function(){var a,b,c,d,f=this.search.outerWidth(!1)-this.search.width();a=this.search;v||(c=a[0].currentStyle||
window.getComputedStyle(a[0],null),v=e(document.createElement("div")).css({position:"absolute",left:"-10000px",top:"-10000px",display:"none",fontSize:c.fontSize,fontFamily:c.fontFamily,fontStyle:c.fontStyle,fontWeight:c.fontWeight,letterSpacing:c.letterSpacing,textTransform:c.textTransform,whiteSpace:"nowrap"}),v.attr("class","select2-sizer"),e("body").append(v));v.text(a.val());a=v.width()+10;b=this.search.offset().left;c=this.selection.width();d=this.selection.offset().left;b=c-(b-d)-f;b<a&&(b=
c-f);40>b&&(b=c-f);0>=b&&(b=a);this.search.width(b)},getVal:function(){var a;if(this.select)return a=this.select.val(),null===a?[]:a;a=this.opts.element.val();return w(a,this.opts.separator)},setVal:function(a){var b;this.select?this.select.val(a):(b=[],e(a).each(function(){0>n(this,b)&&b.push(this)}),this.opts.element.val(0===b.length?"":b.join(this.opts.separator)))},val:function(){var a,b=!1,c=this;if(0===arguments.length)return this.getVal();a=arguments[0];1<arguments.length&&(b=arguments[1]);
if(!a&&0!==a)this.opts.element.val(""),this.updateSelection([]),this.clearSearch(),b&&this.triggerChange();else{this.setVal(a);if(this.select)this.opts.initSelection(this.select,this.bind(this.updateSelection)),b&&this.triggerChange();else{if(this.opts.initSelection===h)throw Error("val() cannot be called if initSelection() is not defined");this.opts.initSelection(this.opts.element,function(a){var f=e(a).map(c.id);c.setVal(f);c.updateSelection(a);c.clearSearch();b&&c.triggerChange()})}this.clearSearch()}},
onSortStart:function(){if(this.select)throw Error("Sorting of elements is not supported when attached to <select>. Attach to <input type='hidden'/> instead.");this.search.width(0);this.searchContainer.hide()},onSortEnd:function(){var a=[],b=this;this.searchContainer.show();this.searchContainer.appendTo(this.searchContainer.parent());this.resizeSearch();this.selection.find(".select2-search-choice").each(function(){a.push(b.opts.id(e(this).data("select2-data")))});this.setVal(a);this.triggerChange()},
data:function(a){var b=this,c;if(0===arguments.length)return this.selection.find(".select2-search-choice").map(function(){return e(this).data("select2-data")}).get();a||(a=[]);c=e.map(a,function(a){return b.opts.id(a)});this.setVal(c);this.updateSelection(a);this.clearSearch()}});e.fn.select2=function(){var a=Array.prototype.slice.call(arguments,0),b,c,d,f,k="val destroy opened open close focus isFocused container onSortStart onSortEnd enable disable positionDropdown data".split(" ");this.each(function(){if(0===
a.length||"object"===typeof a[0])b=0===a.length?{}:e.extend({},a[0]),b.element=e(this),"select"===b.element.get(0).tagName.toLowerCase()?f=b.element.attr("multiple"):(f=b.multiple||!1,"tags"in b&&(b.multiple=f=!0)),c=f?new C:new B,c.init(b);else if("string"===typeof a[0]){if(0>n(a[0],k))throw"Unknown method: "+a[0];d=h;c=e(this).data("select2");if(c!==h&&(d="container"===a[0]?c.container:c[a[0]].apply(c,a.slice(1)),d!==h))return!1}else throw"Invalid arguments to select2 plugin: "+a;});return d===
h?this:d};e.fn.select2.defaults={width:"copy",loadMorePadding:0,closeOnSelect:!0,openOnEnter:!0,containerCss:{},dropdownCss:{},containerCssClass:"",dropdownCssClass:"",formatResult:function(a,b,c,d){b=[];G(a.text,c.term,b,d);return b.join("")},formatSelection:function(a){return a?a.text:h},sortResults:function(a){return a},formatResultCssClass:function(){return h},formatNoMatches:function(){return"No matches found"},formatInputTooShort:function(a,b){var c=b-a.length;return"Please enter "+c+" more character"+
(1==c?"":"s")},formatInputTooLong:function(a,b){var c=a.length-b;return"Please enter "+c+" less character"+(1==c?"":"s")},formatSelectionTooBig:function(a){return"You can only select "+a+" item"+(1==a?"":"s")},formatLoadMore:function(){return"Loading more results..."},formatSearching:function(){return"Searching..."},minimumResultsForSearch:0,minimumInputLength:0,maximumInputLength:null,maximumSelectionSize:0,id:function(a){return a.id},matcher:function(a,b){return 0<=b.toUpperCase().indexOf(a.toUpperCase())},
separator:",",tokenSeparators:[],tokenizer:N,escapeMarkup:function(a){var b={"\\":"&#92;","&":"&amp;","<":"&lt;",">":"&gt;",'"':"&quot;","'":"&apos;","/":"&#47;"};return String(a).replace(/[&<>"'/\\]/g,function(a){return b[a[0]]})},blurOnChange:!1,selectOnBlur:!1,adaptContainerCssClass:function(a){return a},adaptDropdownCssClass:function(){return null}};window.Select2={query:{ajax:H,local:I,tags:J},util:{debounce:E,markMatch:G},"class":{"abstract":u,single:B,multi:C}}}})(jQuery);

/**
 * Select2 Spanish translation
 */
(function ($) {
    "use strict";

    $.extend($.fn.select2.defaults, {
        formatNoMatches: function () { return "Ничего не найдено."; },
        formatInputTooShort: function (input, min) { var n = min - input.length; return "Por favor adicione " + n + " caracter" + (n == 1? "" : "es"); },
        formatInputTooLong: function (input, max) { var n = input.length - max; return "Por favor elimine " + n + " caracter" + (n == 1? "" : "es"); },
        formatSelectionTooBig: function (limit) { return "Solo puede seleccionar " + limit + " elemento" + (limit == 1 ? "" : "s"); },
        //formatLoadMore: function (pageNumber) { return "Cargando más resultados..."; },
        formatSearching: function () { return "Идет поиск..."; }
    });
})(jQuery);
/*
 * jQuery FlexSlider v2.2.0
 * Copyright 2012 WooThemes
 * Contributing Author: Tyler Smith
 */
(function(e){e.flexslider=function(t,n){var r=e(t);r.vars=e.extend({},e.flexslider.defaults,n);var i=r.vars.namespace,s=window.navigator&&window.navigator.msPointerEnabled&&window.MSGesture,o=("ontouchstart"in window||s||window.DocumentTouch&&document instanceof DocumentTouch)&&r.vars.touch,u="click touchend MSPointerUp",a="",f,l=r.vars.direction==="vertical",c=r.vars.reverse,h=r.vars.itemWidth>0,p=r.vars.animation==="fade",d=r.vars.asNavFor!=="",v={},m=!0;e.data(t,"flexslider",r);v={init:function(){r.animating=!1;r.currentSlide=parseInt(r.vars.startAt?r.vars.startAt:0);isNaN(r.currentSlide)&&(r.currentSlide=0);r.animatingTo=r.currentSlide;r.atEnd=r.currentSlide===0||r.currentSlide===r.last;r.containerSelector=r.vars.selector.substr(0,r.vars.selector.search(" "));r.slides=e(r.vars.selector,r);r.container=e(r.containerSelector,r);r.count=r.slides.length;r.syncExists=e(r.vars.sync).length>0;r.vars.animation==="slide"&&(r.vars.animation="swing");r.prop=l?"top":"marginLeft";r.args={};r.manualPause=!1;r.stopped=!1;r.started=!1;r.startTimeout=null;r.transitions=!r.vars.video&&!p&&r.vars.useCSS&&function(){var e=document.createElement("div"),t=["perspectiveProperty","WebkitPerspective","MozPerspective","OPerspective","msPerspective"];for(var n in t)if(e.style[t[n]]!==undefined){r.pfx=t[n].replace("Perspective","").toLowerCase();r.prop="-"+r.pfx+"-transform";return!0}return!1}();r.vars.controlsContainer!==""&&(r.controlsContainer=e(r.vars.controlsContainer).length>0&&e(r.vars.controlsContainer));r.vars.manualControls!==""&&(r.manualControls=e(r.vars.manualControls).length>0&&e(r.vars.manualControls));if(r.vars.randomize){r.slides.sort(function(){return Math.round(Math.random())-.5});r.container.empty().append(r.slides)}r.doMath();r.setup("init");r.vars.controlNav&&v.controlNav.setup();r.vars.directionNav&&v.directionNav.setup();r.vars.keyboard&&(e(r.containerSelector).length===1||r.vars.multipleKeyboard)&&e(document).bind("keyup",function(e){var t=e.keyCode;if(!r.animating&&(t===39||t===37)){var n=t===39?r.getTarget("next"):t===37?r.getTarget("prev"):!1;r.flexAnimate(n,r.vars.pauseOnAction)}});r.vars.mousewheel&&r.bind("mousewheel",function(e,t,n,i){e.preventDefault();var s=t<0?r.getTarget("next"):r.getTarget("prev");r.flexAnimate(s,r.vars.pauseOnAction)});r.vars.pausePlay&&v.pausePlay.setup();r.vars.slideshow&&r.vars.pauseInvisible&&v.pauseInvisible.init();if(r.vars.slideshow){r.vars.pauseOnHover&&r.hover(function(){!r.manualPlay&&!r.manualPause&&r.pause()},function(){!r.manualPause&&!r.manualPlay&&!r.stopped&&r.play()});if(!r.vars.pauseInvisible||!v.pauseInvisible.isHidden())r.vars.initDelay>0?r.startTimeout=setTimeout(r.play,r.vars.initDelay):r.play()}d&&v.asNav.setup();o&&r.vars.touch&&v.touch();(!p||p&&r.vars.smoothHeight)&&e(window).bind("resize orientationchange focus",v.resize);r.find("img").attr("draggable","false");setTimeout(function(){r.vars.start(r)},200)},asNav:{setup:function(){r.asNav=!0;r.animatingTo=Math.floor(r.currentSlide/r.move);r.currentItem=r.currentSlide;r.slides.removeClass(i+"active-slide").eq(r.currentItem).addClass(i+"active-slide");if(!s)r.slides.click(function(t){t.preventDefault();var n=e(this),s=n.index(),o=n.offset().left-e(r).scrollLeft();if(o<=0&&n.hasClass(i+"active-slide"))r.flexAnimate(r.getTarget("prev"),!0);else if(!e(r.vars.asNavFor).data("flexslider").animating&&!n.hasClass(i+"active-slide")){r.direction=r.currentItem<s?"next":"prev";r.flexAnimate(s,r.vars.pauseOnAction,!1,!0,!0)}});else{t._slider=r;r.slides.each(function(){var t=this;t._gesture=new MSGesture;t._gesture.target=t;t.addEventListener("MSPointerDown",function(e){e.preventDefault();e.currentTarget._gesture&&e.currentTarget._gesture.addPointer(e.pointerId)},!1);t.addEventListener("MSGestureTap",function(t){t.preventDefault();var n=e(this),i=n.index();if(!e(r.vars.asNavFor).data("flexslider").animating&&!n.hasClass("active")){r.direction=r.currentItem<i?"next":"prev";r.flexAnimate(i,r.vars.pauseOnAction,!1,!0,!0)}})})}}},controlNav:{setup:function(){r.manualControls?v.controlNav.setupManual():v.controlNav.setupPaging()},setupPaging:function(){var t=r.vars.controlNav==="thumbnails"?"control-thumbs":"control-paging",n=1,s,o;r.controlNavScaffold=e('<ol class="'+i+"control-nav "+i+t+'"></ol>');if(r.pagingCount>1)for(var f=0;f<r.pagingCount;f++){o=r.slides.eq(f);s=r.vars.controlNav==="thumbnails"?'<img src="'+o.attr("data-thumb")+'"/>':"<a>"+n+"</a>";if("thumbnails"===r.vars.controlNav&&!0===r.vars.thumbCaptions){var l=o.attr("data-thumbcaption");""!=l&&undefined!=l&&(s+='<span class="'+i+'caption">'+l+"</span>")}r.controlNavScaffold.append("<li>"+s+"</li>");n++}r.controlsContainer?e(r.controlsContainer).append(r.controlNavScaffold):r.append(r.controlNavScaffold);v.controlNav.set();v.controlNav.active();r.controlNavScaffold.delegate("a, img",u,function(t){t.preventDefault();if(a===""||a===t.type){var n=e(this),s=r.controlNav.index(n);if(!n.hasClass(i+"active")){r.direction=s>r.currentSlide?"next":"prev";r.flexAnimate(s,r.vars.pauseOnAction)}}a===""&&(a=t.type);v.setToClearWatchedEvent()})},setupManual:function(){r.controlNav=r.manualControls;v.controlNav.active();r.controlNav.bind(u,function(t){t.preventDefault();if(a===""||a===t.type){var n=e(this),s=r.controlNav.index(n);if(!n.hasClass(i+"active")){s>r.currentSlide?r.direction="next":r.direction="prev";r.flexAnimate(s,r.vars.pauseOnAction)}}a===""&&(a=t.type);v.setToClearWatchedEvent()})},set:function(){var t=r.vars.controlNav==="thumbnails"?"img":"a";r.controlNav=e("."+i+"control-nav li "+t,r.controlsContainer?r.controlsContainer:r)},active:function(){r.controlNav.removeClass(i+"active").eq(r.animatingTo).addClass(i+"active")},update:function(t,n){r.pagingCount>1&&t==="add"?r.controlNavScaffold.append(e("<li><a>"+r.count+"</a></li>")):r.pagingCount===1?r.controlNavScaffold.find("li").remove():r.controlNav.eq(n).closest("li").remove();v.controlNav.set();r.pagingCount>1&&r.pagingCount!==r.controlNav.length?r.update(n,t):v.controlNav.active()}},directionNav:{setup:function(){var t=e('<ul class="'+i+'direction-nav"><li><a class="'+i+'prev" href="#">'+r.vars.prevText+'</a></li><li><a class="'+i+'next" href="#">'+r.vars.nextText+"</a></li></ul>");if(r.controlsContainer){e(r.controlsContainer).append(t);r.directionNav=e("."+i+"direction-nav li a",r.controlsContainer)}else{r.append(t);r.directionNav=e("."+i+"direction-nav li a",r)}v.directionNav.update();r.directionNav.bind(u,function(t){t.preventDefault();var n;if(a===""||a===t.type){n=e(this).hasClass(i+"next")?r.getTarget("next"):r.getTarget("prev");r.flexAnimate(n,r.vars.pauseOnAction)}a===""&&(a=t.type);v.setToClearWatchedEvent()})},update:function(){var e=i+"disabled";r.pagingCount===1?r.directionNav.addClass(e).attr("tabindex","-1"):r.vars.animationLoop?r.directionNav.removeClass(e).removeAttr("tabindex"):r.animatingTo===0?r.directionNav.removeClass(e).filter("."+i+"prev").addClass(e).attr("tabindex","-1"):r.animatingTo===r.last?r.directionNav.removeClass(e).filter("."+i+"next").addClass(e).attr("tabindex","-1"):r.directionNav.removeClass(e).removeAttr("tabindex")}},pausePlay:{setup:function(){var t=e('<div class="'+i+'pauseplay"><a></a></div>');if(r.controlsContainer){r.controlsContainer.append(t);r.pausePlay=e("."+i+"pauseplay a",r.controlsContainer)}else{r.append(t);r.pausePlay=e("."+i+"pauseplay a",r)}v.pausePlay.update(r.vars.slideshow?i+"pause":i+"play");r.pausePlay.bind(u,function(t){t.preventDefault();if(a===""||a===t.type)if(e(this).hasClass(i+"pause")){r.manualPause=!0;r.manualPlay=!1;r.pause()}else{r.manualPause=!1;r.manualPlay=!0;r.play()}a===""&&(a=t.type);v.setToClearWatchedEvent()})},update:function(e){e==="play"?r.pausePlay.removeClass(i+"pause").addClass(i+"play").html(r.vars.playText):r.pausePlay.removeClass(i+"play").addClass(i+"pause").html(r.vars.pauseText)}},touch:function(){var e,n,i,o,u,a,f=!1,d=0,v=0,m=0;if(!s){t.addEventListener("touchstart",g,!1);function g(s){if(r.animating)s.preventDefault();else if(window.navigator.msPointerEnabled||s.touches.length===1){r.pause();o=l?r.h:r.w;a=Number(new Date);d=s.touches[0].pageX;v=s.touches[0].pageY;i=h&&c&&r.animatingTo===r.last?0:h&&c?r.limit-(r.itemW+r.vars.itemMargin)*r.move*r.animatingTo:h&&r.currentSlide===r.last?r.limit:h?(r.itemW+r.vars.itemMargin)*r.move*r.currentSlide:c?(r.last-r.currentSlide+r.cloneOffset)*o:(r.currentSlide+r.cloneOffset)*o;e=l?v:d;n=l?d:v;t.addEventListener("touchmove",y,!1);t.addEventListener("touchend",b,!1)}}function y(t){d=t.touches[0].pageX;v=t.touches[0].pageY;u=l?e-v:e-d;f=l?Math.abs(u)<Math.abs(d-n):Math.abs(u)<Math.abs(v-n);var s=500;if(!f||Number(new Date)-a>s){t.preventDefault();if(!p&&r.transitions){r.vars.animationLoop||(u/=r.currentSlide===0&&u<0||r.currentSlide===r.last&&u>0?Math.abs(u)/o+2:1);r.setProps(i+u,"setTouch")}}}function b(s){t.removeEventListener("touchmove",y,!1);if(r.animatingTo===r.currentSlide&&!f&&u!==null){var l=c?-u:u,h=l>0?r.getTarget("next"):r.getTarget("prev");r.canAdvance(h)&&(Number(new Date)-a<550&&Math.abs(l)>50||Math.abs(l)>o/2)?r.flexAnimate(h,r.vars.pauseOnAction):p||r.flexAnimate(r.currentSlide,r.vars.pauseOnAction,!0)}t.removeEventListener("touchend",b,!1);e=null;n=null;u=null;i=null}}else{t.style.msTouchAction="none";t._gesture=new MSGesture;t._gesture.target=t;t.addEventListener("MSPointerDown",w,!1);t._slider=r;t.addEventListener("MSGestureChange",E,!1);t.addEventListener("MSGestureEnd",S,!1);function w(e){e.stopPropagation();if(r.animating)e.preventDefault();else{r.pause();t._gesture.addPointer(e.pointerId);m=0;o=l?r.h:r.w;a=Number(new Date);i=h&&c&&r.animatingTo===r.last?0:h&&c?r.limit-(r.itemW+r.vars.itemMargin)*r.move*r.animatingTo:h&&r.currentSlide===r.last?r.limit:h?(r.itemW+r.vars.itemMargin)*r.move*r.currentSlide:c?(r.last-r.currentSlide+r.cloneOffset)*o:(r.currentSlide+r.cloneOffset)*o}}function E(e){e.stopPropagation();var n=e.target._slider;if(!n)return;var r=-e.translationX,s=-e.translationY;m+=l?s:r;u=m;f=l?Math.abs(m)<Math.abs(-r):Math.abs(m)<Math.abs(-s);if(e.detail===e.MSGESTURE_FLAG_INERTIA){setImmediate(function(){t._gesture.stop()});return}if(!f||Number(new Date)-a>500){e.preventDefault();if(!p&&n.transitions){n.vars.animationLoop||(u=m/(n.currentSlide===0&&m<0||n.currentSlide===n.last&&m>0?Math.abs(m)/o+2:1));n.setProps(i+u,"setTouch")}}}function S(t){t.stopPropagation();var r=t.target._slider;if(!r)return;if(r.animatingTo===r.currentSlide&&!f&&u!==null){var s=c?-u:u,l=s>0?r.getTarget("next"):r.getTarget("prev");r.canAdvance(l)&&(Number(new Date)-a<550&&Math.abs(s)>50||Math.abs(s)>o/2)?r.flexAnimate(l,r.vars.pauseOnAction):p||r.flexAnimate(r.currentSlide,r.vars.pauseOnAction,!0)}e=null;n=null;u=null;i=null;m=0}}},resize:function(){if(!r.animating&&r.is(":visible")){h||r.doMath();if(p)v.smoothHeight();else if(h){r.slides.width(r.computedW);r.update(r.pagingCount);r.setProps()}else if(l){r.viewport.height(r.h);r.setProps(r.h,"setTotal")}else{r.vars.smoothHeight&&v.smoothHeight();r.newSlides.width(r.computedW);r.setProps(r.computedW,"setTotal")}}},smoothHeight:function(e){if(!l||p){var t=p?r:r.viewport;e?t.animate({height:r.slides.eq(r.animatingTo).height()},e):t.height(r.slides.eq(r.animatingTo).height())}},sync:function(t){var n=e(r.vars.sync).data("flexslider"),i=r.animatingTo;switch(t){case"animate":n.flexAnimate(i,r.vars.pauseOnAction,!1,!0);break;case"play":!n.playing&&!n.asNav&&n.play();break;case"pause":n.pause()}},pauseInvisible:{visProp:null,init:function(){var e=["webkit","moz","ms","o"];if("hidden"in document)return"hidden";for(var t=0;t<e.length;t++)e[t]+"Hidden"in document&&(v.pauseInvisible.visProp=e[t]+"Hidden");if(v.pauseInvisible.visProp){var n=v.pauseInvisible.visProp.replace(/[H|h]idden/,"")+"visibilitychange";document.addEventListener(n,function(){v.pauseInvisible.isHidden()?r.startTimeout?clearTimeout(r.startTimeout):r.pause():r.started?r.play():r.vars.initDelay>0?setTimeout(r.play,r.vars.initDelay):r.play()})}},isHidden:function(){return document[v.pauseInvisible.visProp]||!1}},setToClearWatchedEvent:function(){clearTimeout(f);f=setTimeout(function(){a=""},3e3)}};r.flexAnimate=function(t,n,s,u,a){!r.vars.animationLoop&&t!==r.currentSlide&&(r.direction=t>r.currentSlide?"next":"prev");d&&r.pagingCount===1&&(r.direction=r.currentItem<t?"next":"prev");if(!r.animating&&(r.canAdvance(t,a)||s)&&r.is(":visible")){if(d&&u){var f=e(r.vars.asNavFor).data("flexslider");r.atEnd=t===0||t===r.count-1;f.flexAnimate(t,!0,!1,!0,a);r.direction=r.currentItem<t?"next":"prev";f.direction=r.direction;if(Math.ceil((t+1)/r.visible)-1===r.currentSlide||t===0){r.currentItem=t;r.slides.removeClass(i+"active-slide").eq(t).addClass(i+"active-slide");return!1}r.currentItem=t;r.slides.removeClass(i+"active-slide").eq(t).addClass(i+"active-slide");t=Math.floor(t/r.visible)}r.animating=!0;r.animatingTo=t;n&&r.pause();r.vars.before(r);r.syncExists&&!a&&v.sync("animate");r.vars.controlNav&&v.controlNav.active();h||r.slides.removeClass(i+"active-slide").eq(t).addClass(i+"active-slide");r.atEnd=t===0||t===r.last;r.vars.directionNav&&v.directionNav.update();if(t===r.last){r.vars.end(r);r.vars.animationLoop||r.pause()}if(!p){var m=l?r.slides.filter(":first").height():r.computedW,g,y,b;if(h){g=r.vars.itemMargin;b=(r.itemW+g)*r.move*r.animatingTo;y=b>r.limit&&r.visible!==1?r.limit:b}else r.currentSlide===0&&t===r.count-1&&r.vars.animationLoop&&r.direction!=="next"?y=c?(r.count+r.cloneOffset)*m:0:r.currentSlide===r.last&&t===0&&r.vars.animationLoop&&r.direction!=="prev"?y=c?0:(r.count+1)*m:y=c?(r.count-1-t+r.cloneOffset)*m:(t+r.cloneOffset)*m;r.setProps(y,"",r.vars.animationSpeed);if(r.transitions){if(!r.vars.animationLoop||!r.atEnd){r.animating=!1;r.currentSlide=r.animatingTo}r.container.unbind("webkitTransitionEnd transitionend");r.container.bind("webkitTransitionEnd transitionend",function(){r.wrapup(m)})}else r.container.animate(r.args,r.vars.animationSpeed,r.vars.easing,function(){r.wrapup(m)})}else if(!o){r.slides.eq(r.currentSlide).css({zIndex:1}).animate({opacity:0},r.vars.animationSpeed,r.vars.easing);r.slides.eq(t).css({zIndex:2}).animate({opacity:1},r.vars.animationSpeed,r.vars.easing,r.wrapup)}else{r.slides.eq(r.currentSlide).css({opacity:0,zIndex:1});r.slides.eq(t).css({opacity:1,zIndex:2});r.wrapup(m)}r.vars.smoothHeight&&v.smoothHeight(r.vars.animationSpeed)}};r.wrapup=function(e){!p&&!h&&(r.currentSlide===0&&r.animatingTo===r.last&&r.vars.animationLoop?r.setProps(e,"jumpEnd"):r.currentSlide===r.last&&r.animatingTo===0&&r.vars.animationLoop&&r.setProps(e,"jumpStart"));r.animating=!1;r.currentSlide=r.animatingTo;r.vars.after(r)};r.animateSlides=function(){!r.animating&&m&&r.flexAnimate(r.getTarget("next"))};r.pause=function(){clearInterval(r.animatedSlides);r.animatedSlides=null;r.playing=!1;r.vars.pausePlay&&v.pausePlay.update("play");r.syncExists&&v.sync("pause")};r.play=function(){r.playing&&clearInterval(r.animatedSlides);r.animatedSlides=r.animatedSlides||setInterval(r.animateSlides,r.vars.slideshowSpeed);r.started=r.playing=!0;r.vars.pausePlay&&v.pausePlay.update("pause");r.syncExists&&v.sync("play")};r.stop=function(){r.pause();r.stopped=!0};r.canAdvance=function(e,t){var n=d?r.pagingCount-1:r.last;return t?!0:d&&r.currentItem===r.count-1&&e===0&&r.direction==="prev"?!0:d&&r.currentItem===0&&e===r.pagingCount-1&&r.direction!=="next"?!1:e===r.currentSlide&&!d?!1:r.vars.animationLoop?!0:r.atEnd&&r.currentSlide===0&&e===n&&r.direction!=="next"?!1:r.atEnd&&r.currentSlide===n&&e===0&&r.direction==="next"?!1:!0};r.getTarget=function(e){r.direction=e;return e==="next"?r.currentSlide===r.last?0:r.currentSlide+1:r.currentSlide===0?r.last:r.currentSlide-1};r.setProps=function(e,t,n){var i=function(){var n=e?e:(r.itemW+r.vars.itemMargin)*r.move*r.animatingTo,i=function(){if(h)return t==="setTouch"?e:c&&r.animatingTo===r.last?0:c?r.limit-(r.itemW+r.vars.itemMargin)*r.move*r.animatingTo:r.animatingTo===r.last?r.limit:n;switch(t){case"setTotal":return c?(r.count-1-r.currentSlide+r.cloneOffset)*e:(r.currentSlide+r.cloneOffset)*e;case"setTouch":return c?e:e;case"jumpEnd":return c?e:r.count*e;case"jumpStart":return c?r.count*e:e;default:return e}}();return i*-1+"px"}();if(r.transitions){i=l?"translate3d(0,"+i+",0)":"translate3d("+i+",0,0)";n=n!==undefined?n/1e3+"s":"0s";r.container.css("-"+r.pfx+"-transition-duration",n)}r.args[r.prop]=i;(r.transitions||n===undefined)&&r.container.css(r.args)};r.setup=function(t){if(!p){var n,s;if(t==="init"){r.viewport=e('<div class="'+i+'viewport"></div>').css({overflow:"hidden",position:"relative"}).appendTo(r).append(r.container);r.cloneCount=0;r.cloneOffset=0;if(c){s=e.makeArray(r.slides).reverse();r.slides=e(s);r.container.empty().append(r.slides)}}if(r.vars.animationLoop&&!h){r.cloneCount=2;r.cloneOffset=1;t!=="init"&&r.container.find(".clone").remove();r.container.append(r.slides.first().clone().addClass("clone").attr("aria-hidden","true")).prepend(r.slides.last().clone().addClass("clone").attr("aria-hidden","true"))}r.newSlides=e(r.vars.selector,r);n=c?r.count-1-r.currentSlide+r.cloneOffset:r.currentSlide+r.cloneOffset;if(l&&!h){r.container.height((r.count+r.cloneCount)*200+"%").css("position","absolute").width("100%");setTimeout(function(){r.newSlides.css({display:"block"});r.doMath();r.viewport.height(r.h);r.setProps(n*r.h,"init")},t==="init"?100:0)}else{r.container.width((r.count+r.cloneCount)*200+"%");r.setProps(n*r.computedW,"init");setTimeout(function(){r.doMath();r.newSlides.css({width:r.computedW,"float":"left",display:"block"});r.vars.smoothHeight&&v.smoothHeight()},t==="init"?100:0)}}else{r.slides.css({width:"100%","float":"left",marginRight:"-100%",position:"relative"});t==="init"&&(o?r.slides.css({opacity:0,display:"block",webkitTransition:"opacity "+r.vars.animationSpeed/1e3+"s ease",zIndex:1}).eq(r.currentSlide).css({opacity:1,zIndex:2}):r.slides.css({opacity:0,display:"block",zIndex:1}).eq(r.currentSlide).css({zIndex:2}).animate({opacity:1},r.vars.animationSpeed,r.vars.easing));r.vars.smoothHeight&&v.smoothHeight()}h||r.slides.removeClass(i+"active-slide").eq(r.currentSlide).addClass(i+"active-slide")};r.doMath=function(){var e=r.slides.first(),t=r.vars.itemMargin,n=r.vars.minItems,i=r.vars.maxItems;r.w=r.viewport===undefined?r.width():r.viewport.width();r.h=e.height();r.boxPadding=e.outerWidth()-e.width();if(h){r.itemT=r.vars.itemWidth+t;r.minW=n?n*r.itemT:r.w;r.maxW=i?i*r.itemT-t:r.w;r.itemW=r.minW>r.w?(r.w-t*(n-1))/n:r.maxW<r.w?(r.w-t*(i-1))/i:r.vars.itemWidth>r.w?r.w:r.vars.itemWidth;r.visible=Math.floor(r.w/r.itemW);r.move=r.vars.move>0&&r.vars.move<r.visible?r.vars.move:r.visible;r.pagingCount=Math.ceil((r.count-r.visible)/r.move+1);r.last=r.pagingCount-1;r.limit=r.pagingCount===1?0:r.vars.itemWidth>r.w?r.itemW*(r.count-1)+t*(r.count-1):(r.itemW+t)*r.count-r.w-t}else{r.itemW=r.w;r.pagingCount=r.count;r.last=r.count-1}r.computedW=r.itemW-r.boxPadding};r.update=function(e,t){r.doMath();if(!h){e<r.currentSlide?r.currentSlide+=1:e<=r.currentSlide&&e!==0&&(r.currentSlide-=1);r.animatingTo=r.currentSlide}if(r.vars.controlNav&&!r.manualControls)if(t==="add"&&!h||r.pagingCount>r.controlNav.length)v.controlNav.update("add");else if(t==="remove"&&!h||r.pagingCount<r.controlNav.length){if(h&&r.currentSlide>r.last){r.currentSlide-=1;r.animatingTo-=1}v.controlNav.update("remove",r.last)}r.vars.directionNav&&v.directionNav.update()};r.addSlide=function(t,n){var i=e(t);r.count+=1;r.last=r.count-1;l&&c?n!==undefined?r.slides.eq(r.count-n).after(i):r.container.prepend(i):n!==undefined?r.slides.eq(n).before(i):r.container.append(i);r.update(n,"add");r.slides=e(r.vars.selector+":not(.clone)",r);r.setup();r.vars.added(r)};r.removeSlide=function(t){var n=isNaN(t)?r.slides.index(e(t)):t;r.count-=1;r.last=r.count-1;isNaN(t)?e(t,r.slides).remove():l&&c?r.slides.eq(r.last).remove():r.slides.eq(t).remove();r.doMath();r.update(n,"remove");r.slides=e(r.vars.selector+":not(.clone)",r);r.setup();r.vars.removed(r)};v.init()};e(window).blur(function(e){focused=!1}).focus(function(e){focused=!0});e.flexslider.defaults={namespace:"flex-",selector:".slides > li",animation:"fade",easing:"swing",direction:"horizontal",reverse:!1,animationLoop:!0,smoothHeight:!1,startAt:0,slideshow:!0,slideshowSpeed:7e3,animationSpeed:600,initDelay:0,randomize:!1,thumbCaptions:!1,pauseOnAction:!0,pauseOnHover:!1,pauseInvisible:!0,useCSS:!0,touch:!0,video:!1,controlNav:!0,directionNav:!0,prevText:"Previous",nextText:"Next",keyboard:!0,multipleKeyboard:!1,mousewheel:!1,pausePlay:!1,pauseText:"Pause",playText:"Play",controlsContainer:"",manualControls:"",sync:"",asNavFor:"",itemWidth:0,itemMargin:0,minItems:1,maxItems:0,move:0,allowOneSlide:!0,start:function(){},before:function(){},after:function(){},end:function(){},added:function(){},removed:function(){}};e.fn.flexslider=function(t){t===undefined&&(t={});if(typeof t=="object")return this.each(function(){var n=e(this),r=t.selector?t.selector:".slides > li",i=n.find(r);if(i.length===1&&t.allowOneSlide===!0||i.length===0){i.fadeIn(400);t.start&&t.start(n)}else n.data("flexslider")===undefined&&new e.flexslider(this,t)});var n=e(this).data("flexslider");switch(t){case"play":n.play();break;case"pause":n.pause();break;case"stop":n.stop();break;case"next":n.flexAnimate(n.getTarget("next"),!0);break;case"prev":case"previous":n.flexAnimate(n.getTarget("prev"),!0);break;default:typeof t=="number"&&n.flexAnimate(t,!0)}}})(jQuery);
$(function(){

    $(window).scroll(function(){
        if ($(this).scrollTop() > 100) {
            $('.scrollup').fadeIn();
            $('.home').fadeIn();
        } else {
            $('.scrollup').fadeOut();
            $('.home').fadeOut();
        }
    });
    $('.scrollup').click(function(){
        $("html, body").animate({ scrollTop: 0 }, 600);
        return false;
    });

    $( ".calendar" ).datepicker({
        onSelect: function(date){
            location.href="/search/?date="+date+"&show=articles";
        },
        dateFormat: "yy-mm-dd",
        maxDate: new Date()
    });

    $( ".calendar" ).datepicker( "option", $.datepicker.regional['ru']);

    // Main Articles Slider and Last News Vertical Carousel
    //------------------------------------------------
    try {
        // jCarousel
        //------------------------------
        var initLastNewsCarousel = function() {
            var $el = $('#last-news'),
                w_height = $('#main-articles').height() - $el.parents('.panel_bordered').height() - $('#secular-articles').height(),
                magic = 62;
            $el.removeClass('hide')
                .jcarousel({
                    vertical: true,
                    scroll: 2
                });
            if (w_height > 0) {
                $el.parents('.jcarousel-clip').height(w_height - magic);
            }
        }

        // Flexslider
        //------------------------------
        var mainArticles = $('.flexslider--headline').flexslider({
            touch: false,
            start: initLastNewsCarousel,
            slideshowSpeed: 5000
        });
        if ($('.flexslider--headline').length == 0) initLastNewsCarousel();
    }
    catch (e) {
        console.log(e);
    }

    //---------

    try {

        var $gallery = $('#gallery'),
            $galleryPreview = $('#gallery-preview');

        $gallery.carousel({
            interval: 0
        });

        $galleryPreview.jcarousel();

        var galleryPreviewCarousel = $galleryPreview.data('jcarousel');

        $galleryPreview.find('li a').on('click', function(e){
            e.preventDefault();
            var $this = $(this),
                $item = $this.parents('li'),
                index = $item.index();

            $gallery.carousel(index);
        });

        $gallery.on('slid', function(e){
            var index = $gallery.find('.active').index();
            $galleryPreview.find('.active').removeClass('active');
            $galleryPreview.find('li:eq(' + index + ') a').addClass('active');
            galleryPreviewCarousel.scroll(index);
        });

    }
    catch (e) {
        console.log(e);
    }

    //---------


    try {

//        var validator = $('.ask_form').validate();
//
//        $('.ask_form_opener').click(function(e){
//            e.preventDefault();
//            var $this = $(this),
//                askForm = $this.closest(':has(.ask_form)').find('.ask_form');
//            $this.slideUp();
//            askForm.slideDown();
//            validator.resetForm();
//        });
//        $('.ask_form__hide').click(function(e){
//            e.preventDefault();
//            var $this = $(this),
//                askForm = $this.parents('.ask_form'),
//                askFormOpener = $this.closest(':has(.ask_form_opener)').find('.ask_form_opener');
//            askForm.slideUp();
//            askFormOpener.slideDown();
//        });
    }
    catch (e) {
        console.log(e);
    }



    $('[data-action=toggle]').click(function(e){
        e.preventDefault();

        var $this = $(this),
            $currentText = $this.find('.current_text'),
            $swapText = $this.find('.swap_text'),
            target = $this.data('target'),
            $div  = $this.closest(':has('+ target + ')').find(target);
        $div.toggleClass('toggle');

        var c = $currentText.html();
        $currentText.html($swapText.html());
        $swapText.html(c);
    });

    var firstItemHeight = $('.ap_list .panel_bordered.fetvy').innerHeight(),
        itemHeaderHeight = 0,//$('.ap_list .panel_bordered .panel_bordered__header').outerHeight(true),
        itemBodyPaddingMargin = 27,
        itemIndicatorHeight = $('.ap_list .panel_bordered .article_preview__indicators').outerHeight(true);

    $('.ap_list .panel_bordered').height(firstItemHeight);

    $('.ap_list .overflow_fade').css({
        'max-height': firstItemHeight - itemHeaderHeight - itemIndicatorHeight - itemBodyPaddingMargin
    });

    $('.overflow_fade').each(function(e){
        var $this = $(this),
            $gradient = $this.find('.gradient_fade'),
            $toggleButton = $this.closest(':has([data-action=toggle])').find('[data-action=toggle]');

        if ($this.height() < parseInt($this.css('max-height'))) {
            $toggleButton.hide();
            $gradient.hide();
        }
    });

    $('.overflow_fade--announce').each(function(){
        var $container = $(this),
            height = $container.height(),
            maxHeight = parseFloat($container.css('max-height'));

        if (height == maxHeight) {
            $container.addClass('overflow_fade--show');
        }
    });

    $('#linksbar-opener').click(function(e){
        e.preventDefault();
        $('#linksbar').slideToggle();
    });

    $('#text-opener').click(function(e){
        e.preventDefault();
        $('#text').slideToggle();
    });

    // Сервисы
    //------------------------------
    $('#services').on('click', '[data-action=toggle-service]', function(e){
        e.preventDefault();
        var $item = $(this).parents('.w-services__item');
        if (!$item.hasClass('opened')) {
            $item.parent().find('> .opened').removeClass('opened', 180);
        }
        $item.toggleClass('opened', 180);
    });

    $('[data-toggle=popover]').popover({
        html: true,
        trigger: 'click'
    });


    // Popover
//    var isVisible = false;
//
//    var hideAllPopovers = function() {
//        $('[data-toggle=popover]').each(function() {
//            $(this).popover('hide');
//        });
//    };
//
//    $('[data-toggle=popover]').popover({
//        html: true,
//        trigger: 'manual'
//    }).on('click', function(e) {
//            // if any other popovers are visible, hide them
//            if(isVisible) {
//                hideAllPopovers();
//            }
//
//            $(this).popover('show');
//
//            //handle clicking on the popover itself
//            $('.popover').off('click').on('click', function(e) {
//                e.stopPropagation(); // prevent event for bubbling up => will not get caught with document.onclick
//            });
//
//            isVisible = true;
//            e.stopPropagation();
//        });
//
//
//    $(document).on('click', function(e) {
//        hideAllPopovers();
//        isVisible = false;
//    });

    var azan_player = jwplayer("icecast");

    azan_player.setup({
        height: 26,
        width: 96,
        file: 'http://radioazan.ru:8888/se2',
        //file: 'http://sound.radioazan.ru:8888/stream',
        type: 'mp3',
        skin: '/static/javascripts/vendor/jwplayer/skin/islam-today-small.xml'
    });

/*    $(document).on("click", ".jwplay", function(){
        var url = (window.location != window.parent.location)
            ? document.referrer
            : document.location.href;
        $.ajax({
            url: "http://dev.asgu.org/visitor",
            type: "POST",
            data: { url : url }
        });
    });*/

});

(function ($, undefined) {

    $(document).ready(function () {

        $('.sprite.sprite-search').click(function(event){
            //event.preventDefault();
            $('.search-block-wrapper').toggleClass('active');
            $('.search-block__field').autocomplete({
                source: function( request, response ) {
                    var matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( request.term ), "i"),
                        items = [];
                    jQuery.ajax({
                        type: 'GET',
                        async: false,
                        url: '/searchautocompeleter/',
                        data: {'q':  request.term},
                        success: function (data, textStatus, jqXHR) {
                            items = data;
                        }
                    });


                    var matcher = new RegExp("("+$.ui.autocomplete.escapeRegex(request.term )+")", "ig" );
                    response($.map(items, function(item) {
                        return {label: item.replace(matcher, "<b>$1</b>"), value: item};
                    }));
                },
                open: function() {
                    var autocomplete = $(".ui-autocomplete");
                    var oldTop = autocomplete.offset().top,
                        oldLeft = autocomplete.offset().left,
                        newTop = oldTop + 5,
                        newLeft = oldLeft + 30;

                    autocomplete.css("top", newTop);
                    autocomplete.css("left", newLeft);
                }
            }).data('ui-autocomplete')._renderItem = function(ul, item) {
                return $( "<li></li>" )
                    .data( "item.autocomplete", item )
                    .append( $( "<a></a>" ).html(item.label) )
                    .appendTo( ul );
            };
        });

    });

})(jQuery);

(function ($, undefined) {

  $(document).ready(function () {

    setTimeout('foundMenuUls()',1000);
    $('.primary').children('li').bind('mouseenter mouseleave', function (e) {
      $(this).toggleClass('hover');
    });

  });

})(jQuery);

function foundMenuUls(){
    $('.primary > li:has(ul)').each(function () {
      var $this = $(this),
        elWidth = $this.width(),
        angleWidth = 18,
        elPositionLeft = $this.position().left,
        $angle = $('<div class="angle"/>');
      $angle.css({
        'left': elPositionLeft + elWidth / 2 - angleWidth / 2
      });
      $this.append($angle);
    });
};
$(function(){
    $(document).on('click', '.default_pagination li', function(){
        console.log($('a', this).length);
        if($('a', this).length){
            document.location.href = $('a', this).attr('href');
        }
    });
});

function addLink() {
	if ((document.activeElement.tagName == 'TEXTAREA') || (document.activeElement.tagName == 'INPUT')) return;
	var body_element = document.getElementsByTagName('body')[0];
	var selection;
	selection = window.getSelection();
	var pagelink = "<br /><br /> Подробнее: <a href='"+document.location.href+"'>"+document.location.href+"</a>";
	var copytext = selection + pagelink;
	var newdiv = document.createElement('div');
	newdiv.style.position='absolute';
	newdiv.style.left='-99999px';
	body_element.appendChild(newdiv);
	newdiv.innerHTML = copytext;
	selection.selectAllChildren(newdiv);
	window.setTimeout(function() {
		body_element.removeChild(newdiv);
	},0);
}
document.oncopy = addLink;


$(function(){
    $(document).on('click', '.town-select-caption', function() {
        $('.town-select-dropdown').show();
    }).on('click', '.town-select-dropdown li', function(){
        var url = document.location.href;
        if(url.indexOf('?') > 0){
            url += '&changeCity=' + $(this).data('id');
        }else{
            url += '?changeCity=' + $(this).data('id');
        }
        document.location.href = url;
    }).on('click', function(e){
        if($(e.target).closest('.town-select-container').length){
            return false;
        }else{
            $('.town-select-dropdown').hide();
        }
    })
})
