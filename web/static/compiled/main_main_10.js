$(function(){

    $(window).scroll(function(){
        if ($(this).scrollTop() > 100) {
            $('.scrollup').fadeIn();
            $('.home').fadeIn();
        } else {
            $('.scrollup').fadeOut();
            $('.home').fadeOut();
        }
    });
    $('.scrollup').click(function(){
        $("html, body").animate({ scrollTop: 0 }, 600);
        return false;
    });

    $('.primary > li:has(ul)').each(function(){
        var $this = $(this),
            elWidth = $this.width(),
            angleWidth = 18,
            elPositionLeft = $this.position().left,
            $angle = $('<div class="angle"/>');
            $angle.css({
            'left': elPositionLeft + elWidth / 2 - angleWidth / 2
        });
        $this.append($angle);
    });

    $('.primary:not(:has(.active))').find('li').hover(function(e){
        $(this).toggleClass('hover');
    }, function(e){
        $(this).toggleClass('hover');
    });

    $( ".calendar" ).datepicker({
        onSelect: function(date){
            location.href="/search/?date="+date+"&show=articles";
        },
        dateFormat: "yy-mm-dd",
        maxDate: new Date()
    });

    $( ".calendar" ).datepicker( "option", $.datepicker.regional['ru']);

    // Main Articles Slider and Last News Vertical Carousel
    //------------------------------------------------
    try {
        // jCarousel
        //------------------------------
        var initLastNewsCarousel = function() {
            var $el = $('#last-news'),
                w_height = $('#main-articles').height() - $el.parents('.panel_bordered').height() - $('#secular-articles').height(),
                magic = 62;
            $el.removeClass('hide')
                .jcarousel({
                    vertical: true,
                    scroll: 2
                });
            if (w_height > 0) {
                $el.parents('.jcarousel-clip').height(w_height - magic);
            }
        }

        // Flexslider
        //------------------------------
        var mainArticles = $('.flexslider--headline').flexslider({
            touch: false,
            start: initLastNewsCarousel,
            slideshowSpeed: 5000
        });
        if ($('.flexslider--headline').length == 0) initLastNewsCarousel();
    }
    catch (e) {
        console.log(e);
    }

    //---------

    try {

        var $gallery = $('#gallery'),
            $galleryPreview = $('#gallery-preview');

        $gallery.carousel({
            interval: 0
        });

        $galleryPreview.jcarousel();

        var galleryPreviewCarousel = $galleryPreview.data('jcarousel');

        $galleryPreview.find('li a').click(function(e){
            e.preventDefault();
            var $this = $(this),
                $item = $this.parents('li'),
                index = $item.index();

            $gallery.carousel(index);
        });

        $gallery.bind('slid', function(e){
            var index = $gallery.find('.active').index();
            $galleryPreview.find('.active').removeClass('active');

            $galleryPreview.find('li:eq(' + index + ') a').addClass('active');
            galleryPreviewCarousel.scroll(index);
        });

    }
    catch (e) {
        console.log(e);
    }

    //---------


    try {

//        var validator = $('.ask_form').validate();
//
//        $('.ask_form_opener').click(function(e){
//            e.preventDefault();
//            var $this = $(this),
//                askForm = $this.closest(':has(.ask_form)').find('.ask_form');
//            $this.slideUp();
//            askForm.slideDown();
//            validator.resetForm();
//        });
//        $('.ask_form__hide').click(function(e){
//            e.preventDefault();
//            var $this = $(this),
//                askForm = $this.parents('.ask_form'),
//                askFormOpener = $this.closest(':has(.ask_form_opener)').find('.ask_form_opener');
//            askForm.slideUp();
//            askFormOpener.slideDown();
//        });
    }
    catch (e) {
        console.log(e);
    }



    $('[data-action=toggle]').click(function(e){
        e.preventDefault();

        var $this = $(this),
            $currentText = $this.find('.current_text'),
            $swapText = $this.find('.swap_text'),
            target = $this.data('target'),
            $div  = $this.closest(':has('+ target + ')').find(target);
        $div.toggleClass('toggle');

        var c = $currentText.html();
        $currentText.html($swapText.html());
        $swapText.html(c);
    });

    var firstItemHeight = $('.ap_list .panel_bordered:first').innerHeight(),
        itemHeaderHeight = $('.ap_list .panel_bordered .panel_bordered__header').outerHeight(true),
        itemBodyPaddingMargin = 27,
        itemIndicatorHeight = $('.ap_list .panel_bordered .article_preview__indicators').outerHeight(true);

    $('.ap_list .panel_bordered').height(firstItemHeight);
    $('.ap_list .overflow_fade').css({
        'max-height': firstItemHeight - itemHeaderHeight - itemIndicatorHeight - itemBodyPaddingMargin
    });

    $('.overflow_fade').each(function(e){
        var $this = $(this),
            $gradient = $this.find('.gradient_fade'),
            $toggleButton = $this.closest(':has([data-action=toggle])').find('[data-action=toggle]');

        if ($this.height() < parseInt($this.css('max-height'))) {
            $toggleButton.hide();
            $gradient.hide();
        }
    });

    $('.overflow_fade--announce').each(function(){
        var $container = $(this),
            height = $container.height(),
            maxHeight = parseFloat($container.css('max-height'));

        if (height == maxHeight) {
            $container.addClass('overflow_fade--show');
        }
    });

    $('#linksbar-opener').click(function(e){
        e.preventDefault();
        $('#linksbar').slideToggle();
    });

    $('#text-opener').click(function(e){
        e.preventDefault();
        $('#text').slideToggle();
    });

    // Сервисы
    //------------------------------
    $('#services').on('click', '[data-action=toggle-service]', function(e){
        e.preventDefault();
        var $item = $(this).parents('.w-services__item');
        if (!$item.hasClass('opened')) {
            $item.parent().find('> .opened').removeClass('opened', 180);
        }
        $item.toggleClass('opened', 180);
    });

    $('[data-toggle=popover]').popover({
        html: true,
        trigger: 'click'
    });


    // Popover
//    var isVisible = false;
//
//    var hideAllPopovers = function() {
//        $('[data-toggle=popover]').each(function() {
//            $(this).popover('hide');
//        });
//    };
//
//    $('[data-toggle=popover]').popover({
//        html: true,
//        trigger: 'manual'
//    }).on('click', function(e) {
//            // if any other popovers are visible, hide them
//            if(isVisible) {
//                hideAllPopovers();
//            }
//
//            $(this).popover('show');
//
//            //handle clicking on the popover itself
//            $('.popover').off('click').on('click', function(e) {
//                e.stopPropagation(); // prevent event for bubbling up => will not get caught with document.onclick
//            });
//
//            isVisible = true;
//            e.stopPropagation();
//        });
//
//
//    $(document).on('click', function(e) {
//        hideAllPopovers();
//        isVisible = false;
//    });

    jwplayer("icecast").setup({
        height: 26,
        width: 96,
        file: 'http://radioazan.ru:8888/se2',
        type: 'mp3',
        skin: '/static/javascripts/vendor/jwplayer/skin/islam-today-small.xml'
    });

//    jQuery.ajax({
//        dataType: 'jsonp',
//        url: 'http://radioazan.dev.solidcube.ru/api/',
//        error: function (jqXHR, textStatus, errorThrown) {
//        },
//        success: callback
//    });
//
//    function callback(result) {
//        $.each(result, function(index, element) {
//            jQuery('.broadcasts').append('<div class="broadcasts__item"><div class="broadcasts__item__time">'+element.startAt+'</div>'+element.title+'</div>');
//        });
//    }
});
