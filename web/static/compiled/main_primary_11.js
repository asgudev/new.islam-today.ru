(function ($, undefined) {

  $(document).ready(function () {

    $('.primary > li:has(ul)').each(function () {
      var $this = $(this),
        elWidth = $this.width(),
        angleWidth = 18,
        elPositionLeft = $this.position().left,
        $angle = $('<div class="angle"/>');
      $angle.css({
        'left': elPositionLeft + elWidth / 2 - angleWidth / 2
      });
      $this.append($angle);
    });

    $('.primary').children('li').bind('mouseenter mouseleave', function (e) {
      $(this).toggleClass('hover');
    });

  });

})(jQuery);
